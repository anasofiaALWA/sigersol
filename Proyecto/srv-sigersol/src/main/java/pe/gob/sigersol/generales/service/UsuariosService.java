package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUsuarios_;
import pe.gob.sigersol.entities.dao.UsuariosDAO;

public class UsuariosService {
	private static Logger log = Logger.getLogger(UsuariosService.class);
	UsuariosDAO usuariosDAO = new UsuariosDAO();
	
	public ClsResultado obtener(){
		ClsResultado clsResultado = null;
		try {
			clsResultado = usuariosDAO.obtener();
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsUsuarios_ clsUsuario) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = usuariosDAO.insertar(clsUsuario);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsUsuarios_ clsUsuario) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = usuariosDAO.actualizar(clsUsuario);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
