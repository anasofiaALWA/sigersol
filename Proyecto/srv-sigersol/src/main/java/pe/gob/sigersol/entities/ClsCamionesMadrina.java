package pe.gob.sigersol.entities;

public class ClsCamionesMadrina {

	private Integer camiones_madrinas_id;
	private ClsTransferencia transferencia;
	private ClsTipoCombustible tipo_combustible;
	private Integer cantidad_camiones;
	private Integer recorrido_anual;
	private Integer cantidad_combustible;
	private Integer codigo_usuario;
	
	public Integer getCamiones_madrinas_id() {
		return camiones_madrinas_id;
	}
	public void setCamiones_madrinas_id(Integer camiones_madrinas_id) {
		this.camiones_madrinas_id = camiones_madrinas_id;
	}
	public ClsTransferencia getTransferencia() {
		return transferencia;
	}
	public void setTransferencia(ClsTransferencia transferencia) {
		this.transferencia = transferencia;
	}
	public ClsTipoCombustible getTipo_combustible() {
		return tipo_combustible;
	}
	public void setTipo_combustible(ClsTipoCombustible tipo_combustible) {
		this.tipo_combustible = tipo_combustible;
	}
	public Integer getCantidad_camiones() {
		return cantidad_camiones;
	}
	public void setCantidad_camiones(Integer cantidad_camiones) {
		this.cantidad_camiones = cantidad_camiones;
	}
	public Integer getRecorrido_anual() {
		return recorrido_anual;
	}
	public void setRecorrido_anual(Integer recorrido_anual) {
		this.recorrido_anual = recorrido_anual;
	}
	public Integer getCantidad_combustible() {
		return cantidad_combustible;
	}
	public void setCantidad_combustible(Integer cantidad_combustible) {
		this.cantidad_combustible = cantidad_combustible;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
