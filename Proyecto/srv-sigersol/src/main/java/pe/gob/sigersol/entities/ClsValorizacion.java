package pe.gob.sigersol.entities;

public class ClsValorizacion {
	
	private Integer valorizacion_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private Integer flag_acopio;
	private Integer flag_acopio2;
	private Integer flag_valorizacion;
	private Integer flag_valorizacion2;
	private Integer numero_acopio;
	private Integer numero_valorizacion;
	private Integer monto_incentivos;
	private String  residuo_inorganico_otro;
	private Integer enero_otro;
	private Integer febrero_otro;
	private Integer marzo_otro;
	private Integer abril_otro;
	private Integer mayo_otro;
	private Integer junio_otro;
	private Integer julio_otro;
	private Integer agosto_otro;
	private Integer setiembre_otro;
	private Integer octubre_otro;
	private Integer noviembre_otro;
	private Integer diciembre_otro;
	private Integer total_otro;
	private Integer cantidad_tratamiento;
	private Integer residuo_compost;
	private Integer residuo_humus;
	private Integer compostaje;
	private Integer lombricultura;
	private Integer biodigestion;
	private String descrip_otros;
	private Integer otros_actividades;
	private String descrip_productos;
	private Integer cantidad_productos;
	private Integer numero_ciclos;
	private Integer cantidad_residuos;
	private Integer cantidad_residuos1;
	private Integer cantidad_residuos2;
	private Integer cantidad_residuos3;
	private Integer cantidad_residuos4;
	private Integer cantidad_residuos5;
	private Integer codigo_usuario;
		
	public Integer getValorizacion_id() {
		return valorizacion_id;
	}
	public void setValorizacion_id(Integer valorizacion_id) {
		this.valorizacion_id = valorizacion_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public Integer getFlag_acopio() {
		return flag_acopio;
	}
	public void setFlag_acopio(Integer flag_acopio) {
		this.flag_acopio = flag_acopio;
	}
	public Integer getFlag_acopio2() {
		return flag_acopio2;
	}
	public void setFlag_acopio2(Integer flag_acopio2) {
		this.flag_acopio2 = flag_acopio2;
	}
	public Integer getFlag_valorizacion() {
		return flag_valorizacion;
	}
	public void setFlag_valorizacion(Integer flag_valorizacion) {
		this.flag_valorizacion = flag_valorizacion;
	}
	public Integer getFlag_valorizacion2() {
		return flag_valorizacion2;
	}
	public void setFlag_valorizacion2(Integer flag_valorizacion2) {
		this.flag_valorizacion2 = flag_valorizacion2;
	}
	public Integer getNumero_acopio() {
		return numero_acopio;
	}
	public void setNumero_acopio(Integer numero_acopio) {
		this.numero_acopio = numero_acopio;
	}
	public Integer getNumero_valorizacion() {
		return numero_valorizacion;
	}
	public void setNumero_valorizacion(Integer numero_valorizacion) {
		this.numero_valorizacion = numero_valorizacion;
	}
	public Integer getMonto_incentivos() {
		return monto_incentivos;
	}
	public void setMonto_incentivos(Integer monto_incentivos) {
		this.monto_incentivos = monto_incentivos;
	}
	public String getResiduo_inorganico_otro() {
		return residuo_inorganico_otro;
	}
	public void setResiduo_inorganico_otro(String residuo_inorganico_otro) {
		this.residuo_inorganico_otro = residuo_inorganico_otro;
	}
	public Integer getEnero_otro() {
		return enero_otro;
	}
	public void setEnero_otro(Integer enero_otro) {
		this.enero_otro = enero_otro;
	}
	public Integer getFebrero_otro() {
		return febrero_otro;
	}
	public void setFebrero_otro(Integer febrero_otro) {
		this.febrero_otro = febrero_otro;
	}
	public Integer getMarzo_otro() {
		return marzo_otro;
	}
	public void setMarzo_otro(Integer marzo_otro) {
		this.marzo_otro = marzo_otro;
	}
	public Integer getAbril_otro() {
		return abril_otro;
	}
	public void setAbril_otro(Integer abril_otro) {
		this.abril_otro = abril_otro;
	}
	public Integer getMayo_otro() {
		return mayo_otro;
	}
	public void setMayo_otro(Integer mayo_otro) {
		this.mayo_otro = mayo_otro;
	}
	public Integer getJunio_otro() {
		return junio_otro;
	}
	public void setJunio_otro(Integer junio_otro) {
		this.junio_otro = junio_otro;
	}
	public Integer getJulio_otro() {
		return julio_otro;
	}
	public void setJulio_otro(Integer julio_otro) {
		this.julio_otro = julio_otro;
	}
	public Integer getAgosto_otro() {
		return agosto_otro;
	}
	public void setAgosto_otro(Integer agosto_otro) {
		this.agosto_otro = agosto_otro;
	}
	public Integer getSetiembre_otro() {
		return setiembre_otro;
	}
	public void setSetiembre_otro(Integer setiembre_otro) {
		this.setiembre_otro = setiembre_otro;
	}
	public Integer getOctubre_otro() {
		return octubre_otro;
	}
	public void setOctubre_otro(Integer octubre_otro) {
		this.octubre_otro = octubre_otro;
	}
	public Integer getNoviembre_otro() {
		return noviembre_otro;
	}
	public void setNoviembre_otro(Integer noviembre_otro) {
		this.noviembre_otro = noviembre_otro;
	}
	public Integer getDiciembre_otro() {
		return diciembre_otro;
	}
	public void setDiciembre_otro(Integer diciembre_otro) {
		this.diciembre_otro = diciembre_otro;
	}
	public Integer getTotal_otro() {
		return total_otro;
	}
	public void setTotal_otro(Integer total_otro) {
		this.total_otro = total_otro;
	}
	public Integer getCantidad_tratamiento() {
		return cantidad_tratamiento;
	}
	public void setCantidad_tratamiento(Integer cantidad_tratamiento) {
		this.cantidad_tratamiento = cantidad_tratamiento;
	}
	public Integer getResiduo_compost() {
		return residuo_compost;
	}
	public void setResiduo_compost(Integer residuo_compost) {
		this.residuo_compost = residuo_compost;
	}
	public Integer getResiduo_humus() {
		return residuo_humus;
	}
	public void setResiduo_humus(Integer residuo_humus) {
		this.residuo_humus = residuo_humus;
	}
	public Integer getCompostaje() {
		return compostaje;
	}
	public void setCompostaje(Integer compostaje) {
		this.compostaje = compostaje;
	}
	public Integer getLombricultura() {
		return lombricultura;
	}
	public void setLombricultura(Integer lombricultura) {
		this.lombricultura = lombricultura;
	}
	public Integer getBiodigestion() {
		return biodigestion;
	}
	public void setBiodigestion(Integer biodigestion) {
		this.biodigestion = biodigestion;
	}
	public String getDescrip_otros() {
		return descrip_otros;
	}
	public void setDescrip_otros(String descrip_otros) {
		this.descrip_otros = descrip_otros;
	}
	public Integer getOtros_actividades() {
		return otros_actividades;
	}
	public void setOtros_actividades(Integer otros_actividades) {
		this.otros_actividades = otros_actividades;
	}
	public String getDescrip_productos() {
		return descrip_productos;
	}
	public void setDescrip_productos(String descrip_productos) {
		this.descrip_productos = descrip_productos;
	}
	public Integer getCantidad_productos() {
		return cantidad_productos;
	}
	public void setCantidad_productos(Integer cantidad_productos) {
		this.cantidad_productos = cantidad_productos;
	}
	public Integer getNumero_ciclos() {
		return numero_ciclos;
	}
	public void setNumero_ciclos(Integer numero_ciclos) {
		this.numero_ciclos = numero_ciclos;
	}
	public Integer getCantidad_residuos() {
		return cantidad_residuos;
	}
	public void setCantidad_residuos(Integer cantidad_residuos) {
		this.cantidad_residuos = cantidad_residuos;
	}
	public Integer getCantidad_residuos1() {
		return cantidad_residuos1;
	}
	public void setCantidad_residuos1(Integer cantidad_residuos1) {
		this.cantidad_residuos1 = cantidad_residuos1;
	}
	public Integer getCantidad_residuos2() {
		return cantidad_residuos2;
	}
	public void setCantidad_residuos2(Integer cantidad_residuos2) {
		this.cantidad_residuos2 = cantidad_residuos2;
	}
	public Integer getCantidad_residuos3() {
		return cantidad_residuos3;
	}
	public void setCantidad_residuos3(Integer cantidad_residuos3) {
		this.cantidad_residuos3 = cantidad_residuos3;
	}
	public Integer getCantidad_residuos4() {
		return cantidad_residuos4;
	}
	public void setCantidad_residuos4(Integer cantidad_residuos4) {
		this.cantidad_residuos4 = cantidad_residuos4;
	}
	public Integer getCantidad_residuos5() {
		return cantidad_residuos5;
	}
	public void setCantidad_residuos5(Integer cantidad_residuos5) {
		this.cantidad_residuos5 = cantidad_residuos5;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
