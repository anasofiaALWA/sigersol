package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.BarridoDAO;

public class BarridoService {

	private static Logger log = Logger.getLogger(BarridoService.class);
	BarridoDAO barridoDAO = new BarridoDAO();
	
	public ClsResultado obtener(ClsBarrido clsBarrido){
		ClsResultado clsResultado = null;
		try {
			clsResultado = barridoDAO.obtener(clsBarrido);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsBarrido clsBarrido) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = barridoDAO.insertar(clsBarrido);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsBarrido clsBarrido) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = barridoDAO.actualizar(clsBarrido);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
