package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsDocumento;
import pe.gob.sigersol.entities.ClsResultado;

public class DocumentoDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(DocumentoDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_documento = "{call SIGERSOLBL.SGR_SP_LIST_DOCUMENTO(?,?)}";
	private final String sgr_sp_ins_documento = "{call SIGERSOLBL.SGR_SP_INS_DOCUMENTO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_documento = "{call SIGERSOLBL.SGR_SP_UPD_DOCUMENTO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsDocumento clsDocumento) {
		ClsResultado clsResultado = new ClsResultado();
		ClsDocumento item = new ClsDocumento();
		con = null;
		String sql = this.sgr_sp_list_documento;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDocumento.getDocumento_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setDocumento_id(rs.getInt("DOCUMENTO_ID"));
					item.setUid_documento(rs.getString("UID_DOCUMENTO"));
					item.setNombre_archivo(rs.getString("NOMBRE_ARCHIVO"));
					item.setNumero_documento(rs.getString("NUMERO_DOCUMENTO"));
					item.setTipo_documento(rs.getString("TIPO_DOCUMENTO"));					
					item.setRazon_social(rs.getString("RAZON_SOCIAL"));
					item.setNombre_infraestructura(rs.getString("NOMBRE_INFRAESTRUCTURA"));
					item.setNombre_autoridad(rs.getString("NOMBRE_AUTORIDAD"));
					item.setFecha_emision(rs.getString("FECHA_EMISION"));
					item.setFecha_caducidad(rs.getString("FECHA_CADUCIDAD"));
					item.setInicio_permiso(rs.getString("INICIO_PERMISO"));
					item.setCulminacion_permiso(rs.getString("CULMINACION_PERMISO"));
					item.setNumero_licencia(rs.getString("NUMERO_LICENCIA"));
					item.setNumero_expediente(rs.getString("NUMERO_EXPEDIENTE"));
							
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsDocumento clsDocumento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_documento;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setString(1, clsDocumento.getUid_documento());
			cs.setString(2, clsDocumento.getNumero_documento());
			cs.setString(3, clsDocumento.getNombre_archivo());
			cs.setString(4, clsDocumento.getTipo_documento());
			cs.setString(5, clsDocumento.getRazon_social());
			cs.setString(6, clsDocumento.getNombre_infraestructura());
			cs.setString(7, clsDocumento.getNombre_autoridad());
			cs.setString(8, clsDocumento.getFecha_emision());
			cs.setString(9, clsDocumento.getFecha_caducidad());
			cs.setString(10, clsDocumento.getInicio_permiso());
			cs.setString(11, clsDocumento.getCulminacion_permiso());
			cs.setString(12, clsDocumento.getNumero_licencia());
			cs.setString(13, clsDocumento.getNumero_expediente());
			cs.setInt(14, clsDocumento.getCodigo_usuario());
			cs.registerOutParameter(15, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(15);
			
			log.info("DocumentoDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsDocumento clsDocumento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_documento;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDocumento.getDocumento_id());
			cs.setString(2, clsDocumento.getUid_documento());
			cs.setString(3, clsDocumento.getNumero_documento());
			cs.setString(4, clsDocumento.getNombre_archivo());
			cs.setString(5, clsDocumento.getTipo_documento());
			cs.setString(6, clsDocumento.getRazon_social());
			cs.setString(7, clsDocumento.getNombre_infraestructura());
			cs.setString(8, clsDocumento.getNombre_autoridad());
			cs.setString(9, clsDocumento.getFecha_emision());
			cs.setString(10, clsDocumento.getFecha_caducidad());
			cs.setString(11, clsDocumento.getInicio_permiso());
			cs.setString(12, clsDocumento.getCulminacion_permiso());
			cs.setString(13, clsDocumento.getNumero_licencia());
			cs.setString(14, clsDocumento.getNumero_expediente());
			cs.setInt(15, clsDocumento.getCodigo_usuario());
			cs.registerOutParameter(16, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(16);

			log.info("DocumentoDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
