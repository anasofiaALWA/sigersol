package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsOferta;
import pe.gob.sigersol.entities.ClsProgramaSegregacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoOferta;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.OfertaService;
import pe.gob.sigersol.generales.service.ProgramaSegregacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase OfertaRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de barrido permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/oferta")
@RequestScoped
public class OfertaRestService {

	private static Logger log = Logger.getLogger(OfertaRestService.class.getName());
	private ClsOferta clsOferta;
	private ClsTipoOferta clsTipoOferta;
	private ClsBarrido clsBarrido;
	private OfertaService ofertaService;
	private List<ClsOferta> listaOferta;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarOferta")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarOferta(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			ofertaService = new OfertaService();
			
			jsonEntradaListaOferta(paramJson);
			
			log.info("**Obtener Generacion No Domiciliar");
			
			clsResultado = ofertaService.listarOferta(clsOferta);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstOferta\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		ofertaService = new OfertaService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaOferta(paramJson);

			for (ClsOferta clsOferta : listaOferta) {
				clsResultado = ofertaService.insertar(clsOferta);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ofertaService = new OfertaService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaOferta(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsOferta clsOferta : listaOferta) {
			clsResultado = ofertaService.actualizar(clsOferta);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Oferta
	 * 
	 * */
	private void jsonEntradaOferta(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> OfertarRestService :	 jsonEntradaOferta" + jsonEntrada);

		JSONObject jsonObjectTipoOferta;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaOferta = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsOferta = new ClsOferta();
			clsTipoOferta = new ClsTipoOferta ();
			clsBarrido = new ClsBarrido();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("barrido_id") != null && innerObject.get("barrido_id").toString() != "") {
				clsBarrido.setBarrido_id(Integer.parseInt(innerObject.get("barrido_id").toString()));
			}
			
			clsOferta.setBarrido(clsBarrido);
			
			if (innerObject.get("tipo_oferta") != null && innerObject.get("tipo_oferta").toString() != "") {
				jsonObjectTipoOferta  = (JSONObject) innerObject.get("tipo_oferta");
				clsTipoOferta.setTipo_oferta_id(Integer.parseInt(jsonObjectTipoOferta.get("tipo_oferta_id").toString()));
			}
			clsOferta.setTipo_oferta(clsTipoOferta);
			
			if (innerObject.get("total_asfaltada") != null && innerObject.get("total_asfaltada") != "") {
				clsOferta.setTotal_asfaltada(Integer.parseInt(innerObject.get("total_asfaltada").toString()));
			} else {
				clsOferta.setTotal_asfaltada(0);
			}
			
			if (innerObject.get("total_no_asfaltada") != null && innerObject.get("total_no_asfaltada") != "") {
				clsOferta.setTotal_no_asfaltada(Integer.parseInt(innerObject.get("total_no_asfaltada").toString()));
			} else {
				clsOferta.setTotal_no_asfaltada(0);
			}
			
			if (innerObject.get("total_plazas") != null && innerObject.get("total_plazas").toString() != "") {
				clsOferta.setTotal_plazas(Integer.parseInt(innerObject.get("total_plazas").toString()));
			} else {
				clsOferta.setTotal_plazas(0);
			}
			
			if (innerObject.get("total_playas") != null && innerObject.get("total_playas").toString() != "") {
				clsOferta.setTotal_playas(Integer.parseInt(innerObject.get("total_playas").toString()));
			} else {
				clsOferta.setTotal_playas(0);
			}
			
			if (innerObject.get("total_otros") != null && innerObject.get("total_otros").toString() != "") {
				clsOferta.setTotal_otros(Integer.parseInt(innerObject.get("total_otros").toString()));
			} else {
				clsOferta.setTotal_otros(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsOferta.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaOferta.add(clsOferta);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
		
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaOferta(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectOferta;
		JSONParser jsonParser = new JSONParser();
		
		clsOferta = new ClsOferta();
		clsTipoOferta = new ClsTipoOferta();
		clsBarrido = new ClsBarrido();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectOferta = (JSONObject) jsonObjectPrincipal.get("Oferta");
			
			if (jsonObjectOferta.get("barrido_id") != null && jsonObjectOferta.get("barrido_id") != "" &&
					jsonObjectOferta.get("barrido_id") != " ") {
				clsBarrido.setBarrido_id(Integer.parseInt(jsonObjectOferta.get("barrido_id").toString()));
			}
			else
				clsBarrido.setBarrido_id(-1);
			
			clsOferta.setBarrido(clsBarrido);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
		
}
