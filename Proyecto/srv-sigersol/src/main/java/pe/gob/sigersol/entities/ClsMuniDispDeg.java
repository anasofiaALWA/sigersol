package pe.gob.sigersol.entities;

public class ClsMuniDispDeg {

	private Integer muni_disp_deg_id;
	private ClsDisposicionFinal disposicionFinal;
	private ClsMes mes;
	private Integer selec_degradada;
	private Integer cantidad;
	private Integer metodo_usado;
	private Integer codigo_usuario;
	
	public Integer getMuni_disp_deg_id() {
		return muni_disp_deg_id;
	}
	public void setMuni_disp_deg_id(Integer muni_disp_deg_id) {
		this.muni_disp_deg_id = muni_disp_deg_id;
	}
	public ClsDisposicionFinal getDisposicionFinal() {
		return disposicionFinal;
	}
	public void setDisposicionFinal(ClsDisposicionFinal disposicionFinal) {
		this.disposicionFinal = disposicionFinal;
	}
	public ClsMes getMes() {
		return mes;
	}
	public void setMes(ClsMes mes) {
		this.mes = mes;
	}
	public Integer getSelec_degradada() {
		return selec_degradada;
	}
	public void setSelec_degradada(Integer selec_degradada) {
		this.selec_degradada = selec_degradada;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getMetodo_usado() {
		return metodo_usado;
	}
	public void setMetodo_usado(Integer metodo_usado) {
		this.metodo_usado = metodo_usado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
