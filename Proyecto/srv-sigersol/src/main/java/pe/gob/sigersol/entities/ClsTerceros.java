package pe.gob.sigersol.entities;

import java.util.Date;

public class ClsTerceros {

	private Integer terceros_id;
	private ClsMunicipalidad municipalidad;
	private String num_registro;
	private String fecha_inicio_vigencia;
	private String fecha_fin_vigencia;
	private String razon_social;
	private String direccion;
	private String nombre_archivo;
	private String nombre_archivo_ref;
	private String uid_alfresco;
	private Integer codigo_usuario;
	
	public Integer getTerceros_id() {
		return terceros_id;
	}
	public void setTerceros_id(Integer terceros_id) {
		this.terceros_id = terceros_id;
	}
	public ClsMunicipalidad getMunicipalidad() {
		return municipalidad;
	}
	public void setMunicipalidad(ClsMunicipalidad municipalidad) {
		this.municipalidad = municipalidad;
	}
	public String getNum_registro() {
		return num_registro;
	}
	public void setNum_registro(String num_registro) {
		this.num_registro = num_registro;
	}
	public String getFecha_inicio_vigencia() {
		return fecha_inicio_vigencia;
	}
	public void setFecha_inicio_vigencia(String fecha_inicio_vigencia) {
		this.fecha_inicio_vigencia = fecha_inicio_vigencia;
	}
	public String getFecha_fin_vigencia() {
		return fecha_fin_vigencia;
	}
	public void setFecha_fin_vigencia(String fecha_fin_vigencia) {
		this.fecha_fin_vigencia = fecha_fin_vigencia;
	}
	public String getRazon_social() {
		return razon_social;
	}
	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNombre_archivo() {
		return nombre_archivo;
	}
	public void setNombre_archivo(String nombre_archivo) {
		this.nombre_archivo = nombre_archivo;
	}
	public String getNombre_archivo_ref() {
		return nombre_archivo_ref;
	}
	public void setNombre_archivo_ref(String nombre_archivo_ref) {
		this.nombre_archivo_ref = nombre_archivo_ref;
	}
	public String getUid_alfresco() {
		return uid_alfresco;
	}
	public void setUid_alfresco(String uid_alfresco) {
		this.uid_alfresco = uid_alfresco;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
