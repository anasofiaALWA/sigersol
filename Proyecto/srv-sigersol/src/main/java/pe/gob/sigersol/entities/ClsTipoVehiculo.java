package pe.gob.sigersol.entities;

public class ClsTipoVehiculo {
	
	private Integer tipo_vehiculo_id;
	private String nombre_tipo;
	
	public Integer getTipo_vehiculo_id() {
		return tipo_vehiculo_id;
	}
	public void setTipo_vehiculo_id(Integer tipo_vehiculo_id) {
		this.tipo_vehiculo_id = tipo_vehiculo_id;
	}
	public String getNombre_tipo() {
		return nombre_tipo;
	}
	public void setNombre_tipo(String nombre_tipo) {
		this.nombre_tipo = nombre_tipo;
	}

}
