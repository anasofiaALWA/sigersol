package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAreasDegradadasAbandonadas;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.AreasDegradadasAbandonadasService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase AreasDegradadasAbandonadasRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de DisposicionFinal, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/abandonadasX")
@RequestScoped
public class AreasDegradadasAbandonadasRestService {

	private static Logger log = Logger.getLogger(AreasDegradadasAbandonadasRestService.class.getName());
	private Integer accion = null;
	
	private ClsAreasDegradadasAbandonadas clsAreasDegradadasAbandonadas;
	private ClsDisposicionFinal clsDisposicionFinal;
	
	private AreasDegradadasAbandonadasService areasDegradadasAbandonadasService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			areasDegradadasAbandonadasService = new AreasDegradadasAbandonadasService();
			
			jsonEntradaAreasDegradadasAbandonadas(paramJson);
			
			log.info("**obtener areas Degradadas Abandonadas**");
			
			clsResultado = areasDegradadasAbandonadasService.obtener(clsAreasDegradadasAbandonadas);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"abandonadas\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		areasDegradadasAbandonadasService = new AreasDegradadasAbandonadasService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaAreasDegradadasAbandonadas(paramJson);	
			clsResultado = areasDegradadasAbandonadasService.insertar(clsAreasDegradadasAbandonadas);	
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		areasDegradadasAbandonadasService = new AreasDegradadasAbandonadasService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaAreasDegradadasAbandonadas(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = areasDegradadasAbandonadasService.actualizar(clsAreasDegradadasAbandonadas);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de AreasDegradadasAbandonadas  
	 * 
	 * */
	private void jsonEntradaAreasDegradadasAbandonadas(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AreasDegradadasAbandonadasRestService : jsonEntradaAreasDegradadasAbandonadas" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectAreasDegradadasAbandonadas;
		JSONParser jsonParser = new JSONParser();
		
		clsAreasDegradadasAbandonadas = new ClsAreasDegradadasAbandonadas();
		clsDisposicionFinal = new ClsDisposicionFinal();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectAreasDegradadasAbandonadas = (JSONObject) jsonObjectPrincipal.get("Abandonadas");
			
			if (jsonObjectAreasDegradadasAbandonadas.get("areas_degradadas_abandonadas_id") != null && jsonObjectAreasDegradadasAbandonadas.get("areas_degradadas_abandonadas_id") != "") {
				clsAreasDegradadasAbandonadas.setAreas_degradadas_abandonadas_id(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("areas_degradadas_abandonadas_id").toString()));
			} else {
				clsAreasDegradadasAbandonadas.setAreas_degradadas_abandonadas_id(-1);
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("disposicion_final_id") != null && jsonObjectAreasDegradadasAbandonadas.get("disposicion_final_id") != "") {
				clsDisposicionFinal.setDisposicion_final_id(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("disposicion_final_id").toString()));
			} else {
				clsDisposicionFinal.setDisposicion_final_id(-1);
			}
			
			clsAreasDegradadasAbandonadas.setDisposicion_final(clsDisposicionFinal);
			
			if (jsonObjectAreasDegradadasAbandonadas.get("flag_areas_abandonadas") != null && jsonObjectAreasDegradadasAbandonadas.get("flag_areas_abandonadas") != "") {
				clsAreasDegradadasAbandonadas.setFlag_areas_abandonadas(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("flag_areas_abandonadas").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("numero_areas") != null && jsonObjectAreasDegradadasAbandonadas.get("numero_areas") != "") {
				clsAreasDegradadasAbandonadas.setNumero_areas(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("numero_areas").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("anio_inicio") != null && jsonObjectAreasDegradadasAbandonadas.get("anio_inicio") != "") {
				clsAreasDegradadasAbandonadas.setAnio_inicio(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("anio_inicio").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("anio_cierre") != null && jsonObjectAreasDegradadasAbandonadas.get("anio_cierre") != "") {
				clsAreasDegradadasAbandonadas.setAnio_cierre(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("anio_cierre").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("area_total") != null && jsonObjectAreasDegradadasAbandonadas.get("area_total") != "") {
				clsAreasDegradadasAbandonadas.setArea_total(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("area_total").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("responsable") != null && jsonObjectAreasDegradadasAbandonadas.get("responsable") != "") {
				clsAreasDegradadasAbandonadas.setResponsable(jsonObjectAreasDegradadasAbandonadas.get("responsable").toString());
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("altura") != null && jsonObjectAreasDegradadasAbandonadas.get("altura") != "") {
				clsAreasDegradadasAbandonadas.setAltura(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("altura").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("cantidad") != null && jsonObjectAreasDegradadasAbandonadas.get("cantidad") != "") {
				clsAreasDegradadasAbandonadas.setCantidad(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("cantidad").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("metodo_usado") != null && jsonObjectAreasDegradadasAbandonadas.get("metodo_usado") != "") {
				clsAreasDegradadasAbandonadas.setMetodo_usado(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("metodo_usado").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("altura_napa") != null && jsonObjectAreasDegradadasAbandonadas.get("altura_napa") != "") {
				clsAreasDegradadasAbandonadas.setAltura_napa(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("altura_napa").toString()));
			}
			
			if (jsonObjectAreasDegradadasAbandonadas.get("cod_usuario") != null && jsonObjectAreasDegradadasAbandonadas.get("cod_usuario") != "") {
				clsAreasDegradadasAbandonadas.setCodigo_usuario(Integer.parseInt(jsonObjectAreasDegradadasAbandonadas.get("cod_usuario").toString()));
			}
			
			log.info("----------------------------------------------------------------------------------");
		
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
