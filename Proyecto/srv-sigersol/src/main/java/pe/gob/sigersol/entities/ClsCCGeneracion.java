package pe.gob.sigersol.entities;

import java.util.List;

public class ClsCCGeneracion {
	private Integer cc_generacionId;
	private Integer disposicionFinalId;
	private Integer anioInicio_sdf;
	private Double generacionTotal_sdf;
	private Integer codigo_estado;
	private Integer sesionId;
	private List<ClsCCGenComposicion> listComposicion;
	
	public Integer getCc_generacionId() {
		return cc_generacionId;
	}
	public void setCc_generacionId(Integer cc_generacionId) {
		this.cc_generacionId = cc_generacionId;
	}
	public Integer getDisposicionFinalId() {
		return disposicionFinalId;
	}
	public void setDisposicionFinalId(Integer disposicionFinalId) {
		this.disposicionFinalId = disposicionFinalId;
	}
	public Integer getAnioInicio_sdf() {
		return anioInicio_sdf;
	}
	public void setAnioInicio_sdf(Integer anioInicio_sdf) {
		this.anioInicio_sdf = anioInicio_sdf;
	}
	public Double getGeneracionTotal_sdf() {
		return generacionTotal_sdf;
	}
	public void setGeneracionTotal_sdf(Double generacionTotal_sdf) {
		this.generacionTotal_sdf = generacionTotal_sdf;
	}
	public Integer getCodigo_estado() {
		return codigo_estado;
	}
	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}
	public Integer getSesionId() {
		return sesionId;
	}
	public void setSesionId(Integer sesionId) {
		this.sesionId = sesionId;
	}
	public List<ClsCCGenComposicion> getListComposicion() {
		return listComposicion;
	}
	public void setListComposicion(List<ClsCCGenComposicion> listComposicion) {
		this.listComposicion = listComposicion;
	}
	
	

}
