package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsOfertaBarrido;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.OfertaBarridoDAO;

public class OfertaBarridoService {
	
	private static Logger log = Logger.getLogger(OfertaService.class);
	OfertaBarridoDAO ofertaBarridoDAO = new OfertaBarridoDAO();
	
	public ClsResultado obtener(ClsOfertaBarrido clsOfertaBarrido){
		ClsResultado clsResultado = null;
		try {
			clsResultado = ofertaBarridoDAO.obtener(clsOfertaBarrido);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsOfertaBarrido clsOfertaBarrido) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = ofertaBarridoDAO.insertar(clsOfertaBarrido);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsOfertaBarrido clsOfertaBarrido) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = ofertaBarridoDAO.actualizar(clsOfertaBarrido);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
