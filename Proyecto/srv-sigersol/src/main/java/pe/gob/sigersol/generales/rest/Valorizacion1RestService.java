package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.Valorizacion1Service;
import pe.gob.sigersol.generales.service.ValorizacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/valorizacion1")
@RequestScoped
public class Valorizacion1RestService {
	
	private static Logger log = Logger.getLogger(Valorizacion1RestService.class.getName());
	private ClsValorizacion clsValorizacion;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private Valorizacion1Service valorizacion1Service;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		valorizacion1Service = new Valorizacion1Service();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener generacion**");		
			jsonEntradaValorizacion(paramJson);	
			clsResultado = valorizacion1Service.obtener(clsValorizacion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"valorizacion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		valorizacion1Service = new Valorizacion1Service();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaValorizacion(paramJson);
			clsResultado = valorizacion1Service.insertar(clsValorizacion);			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		valorizacion1Service = new Valorizacion1Service();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaValorizacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = valorizacion1Service.actualizar(clsValorizacion);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Valorización  
	 * 
	 * */
	private void jsonEntradaValorizacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ValorizacionRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectValorizacion;
		JSONParser jsonParser = new JSONParser();
		
		clsValorizacion = new ClsValorizacion();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectValorizacion = (JSONObject) jsonObjectPrincipal.get("Valorizacion1");
			
			if (jsonObjectValorizacion.get("valorizacion_id") != null && jsonObjectValorizacion.get("valorizacion_id") != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectValorizacion.get("valorizacion_id").toString()));
			}
			
			if (jsonObjectValorizacion.get("ciclo_grs_id") != null && jsonObjectValorizacion.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectValorizacion.get("ciclo_grs_id").toString()));
			} 
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsValorizacion.setCicloGestionResiduos(clsCicloGestionResiduos);
			
			if (jsonObjectValorizacion.get("flag_acopio") != null && jsonObjectValorizacion.get("flag_acopio") != "") {
				clsValorizacion.setFlag_acopio(Integer.parseInt(jsonObjectValorizacion.get("flag_acopio").toString()));
			}
			
			if (jsonObjectValorizacion.get("flag_acopio2") != null && jsonObjectValorizacion.get("flag_acopio2") != "") {
				clsValorizacion.setFlag_acopio2(Integer.parseInt(jsonObjectValorizacion.get("flag_acopio2").toString()));
			}
			
			if (jsonObjectValorizacion.get("flag_valorizacion") != null && jsonObjectValorizacion.get("flag_valorizacion") != "") {
				clsValorizacion.setFlag_valorizacion(Integer.parseInt(jsonObjectValorizacion.get("flag_valorizacion").toString()));
			}
			
			if (jsonObjectValorizacion.get("flag_valorizacion2") != null && jsonObjectValorizacion.get("flag_valorizacion2") != "") {
				clsValorizacion.setFlag_valorizacion2(Integer.parseInt(jsonObjectValorizacion.get("flag_valorizacion2").toString()));
			}
			
			if (jsonObjectValorizacion.get("numero_acopio") != null && jsonObjectValorizacion.get("numero_acopio") != "") {
				clsValorizacion.setNumero_acopio(Integer.parseInt(jsonObjectValorizacion.get("numero_acopio").toString()));
			}
			
			if (jsonObjectValorizacion.get("numero_valorizacion") != null && jsonObjectValorizacion.get("numero_valorizacion") != "") {
				clsValorizacion.setNumero_valorizacion(Integer.parseInt(jsonObjectValorizacion.get("numero_valorizacion").toString()));
			}
			
			if (jsonObjectValorizacion.get("cod_usuario") != null && jsonObjectValorizacion.get("cod_usuario") != "") {
				clsValorizacion.setCodigo_usuario(Integer.parseInt(jsonObjectValorizacion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
