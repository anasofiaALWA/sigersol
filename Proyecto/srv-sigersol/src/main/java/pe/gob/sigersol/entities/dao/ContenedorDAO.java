package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsContenedor;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoContenedor;


public class ContenedorDAO extends GenericoDAO{
	
	private static Logger log = Logger.getLogger(ContenedorDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_contenedor= "{call SIGERSOLBL.SGR_SP_INS_CONTENEDOR(?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_contenedor = "{call SIGERSOLBL.SGR_SP_UPD_CONTENEDOR(?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_list_contenedor = "{call SIGERSOLBL.SGR_SP_LIST_CONTENEDOR(?,?)}";  
	
	public ClsResultado listarContenedor(ClsContenedor clsContenedor){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsContenedor> lista = new ArrayList<ClsContenedor>();
		con = null;
		String sql = this.sgr_sp_list_contenedor;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsContenedor.getAlmacenamiento().getAlmacenamiento_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsContenedor item = new ClsContenedor();
				ClsTipoContenedor tipoContenedor = new ClsTipoContenedor();
				
				try {
					tipoContenedor.setTipo_contenedor_id(rs.getInt("TIPO_CONTENEDOR_ID"));
					tipoContenedor.setTipo_contenedor(rs.getString("TIPO_CONTENEDOR"));
					tipoContenedor.setEstado_contenedor(rs.getString("ESTADO_CONTENEDOR"));
					item.setTipo_contenedor(tipoContenedor);
					if(clsContenedor.getAlmacenamiento().getAlmacenamiento_id() != -1){
						item.setCapacidad(rs.getInt("CAPACIDAD"));
						item.setCantidad(rs.getDouble("CANTIDAD"));
						item.setMercados(rs.getInt("MERCADOS"));
						item.setParques(rs.getInt("PARQUES"));
						item.setVias(rs.getInt("VIAS"));
						item.setOtros(rs.getInt("OTROS"));
					}else{
						item.setCapacidad(null);
						item.setCantidad(null);
						item.setMercados(null);
						item.setParques(null);
						item.setVias(null);
						item.setOtros(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsContenedor clsContenedor) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_contenedor;
		
		Integer cont_contenedor = 1;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsContenedor.getAlmacenamiento().getAlmacenamiento_id());
			cs.setInt(2, clsContenedor.getTipo_contenedor().getTipo_contenedor_id());
			cs.setInt(3, clsContenedor.getCapacidad());
			cs.setDouble(4, clsContenedor.getCantidad());
			cs.setInt(5, clsContenedor.getMercados());
			cs.setInt(6, clsContenedor.getParques());
			cs.setInt(7, clsContenedor.getVias());
			cs.setInt(8, clsContenedor.getOtros());
			cs.setInt(9, clsContenedor.getCodigo_usuario());
			cs.registerOutParameter(10, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(10);
			
			cont_contenedor++;
			
			log.info("ContenedorDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsContenedor clsContenedor) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_contenedor;

		Integer cont_contenedor = 1;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsContenedor.getAlmacenamiento().getAlmacenamiento_id());
			cs.setInt(2, clsContenedor.getTipo_contenedor().getTipo_contenedor_id());
			cs.setInt(3, clsContenedor.getCapacidad());
			cs.setDouble(4, clsContenedor.getCantidad());
			cs.setInt(5, clsContenedor.getMercados());
			cs.setInt(6, clsContenedor.getParques());
			cs.setInt(7, clsContenedor.getVias());
			cs.setInt(8, clsContenedor.getOtros());
			cs.setInt(9, clsContenedor.getCodigo_usuario());
			cs.registerOutParameter(10, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(10);

			cont_contenedor++;
			
			log.info("ContenedorDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
