package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAccionesEducacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoEventos;

public class AccionesEducacionDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(AccionesEducacionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_acciones_educacion = "{call SIGERSOLBL.SGR_SP_INS_ACCIONES_EDUCACION(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_acciones_educacion = "{call SIGERSOLBL.SGR_SP_UPD_ACCIONES_EDUCACION(?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_acciones_educacion = "{call SIGERSOLBL.SGR_SP_LIST_ACCIONES_EDUCACION(?,?)}";  
	
	public ClsResultado listarAccionesEducacion(ClsAccionesEducacion clsAccionesEducacion){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsAccionesEducacion> lista = new ArrayList<ClsAccionesEducacion>();
		con = null;
		String sql = this.sgr_sp_list_acciones_educacion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccionesEducacion.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsAccionesEducacion item = new ClsAccionesEducacion();
				ClsTipoEventos tipoEventos = new ClsTipoEventos();
				
				try {
					tipoEventos.setTipo_eventos_id(rs.getInt("TIPO_EVENTOS_ID"));
					tipoEventos.setNombre_eventos(rs.getString("NOMBRE_EVENTOS"));
					item.setTipo_eventos(tipoEventos);
					if(clsAccionesEducacion.getEducacion_ambiental().getEducacion_ambiental_id() == -1){
						item.setCapacitad_hombres(null);
						item.setCapacitad_mujeres(null);
					} else{
						item.setCapacitad_hombres(rs.getInt("CAPACITAD_HOMBRES"));
						item.setCapacitad_mujeres(rs.getInt("CAPACITAD_MUJERES"));
					}
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAccionesEducacion clsAccionesEducacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_acciones_educacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccionesEducacion.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.setInt(2, clsAccionesEducacion.getTipo_eventos().getTipo_eventos_id());
			cs.setInt(3, clsAccionesEducacion.getCapacitad_hombres());
			cs.setInt(4, clsAccionesEducacion.getCapacitad_mujeres());
			cs.setInt(5, clsAccionesEducacion.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("AccionesEducacionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsAccionesEducacion clsAccionesEducacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_acciones_educacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccionesEducacion.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.setInt(2, clsAccionesEducacion.getTipo_eventos().getTipo_eventos_id());
			cs.setInt(3, clsAccionesEducacion.getCapacitad_hombres());
			cs.setInt(4, clsAccionesEducacion.getCapacitad_mujeres());
			cs.setInt(5, clsAccionesEducacion.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("AccionesEducacionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
