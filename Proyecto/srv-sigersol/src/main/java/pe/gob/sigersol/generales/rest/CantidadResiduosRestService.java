package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCantidadResiduos;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduo;
import pe.gob.sigersol.generales.service.CantidadResiduosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/cantidadResiduosX")
@RequestScoped
public class CantidadResiduosRestService {
	
	private static Logger log = Logger.getLogger(CantidadResiduosRestService.class.getName());
	private ClsCantidadResiduos clsCantidadResiduos;
	private ClsRecRcdOm clsRecRcdOm;
	private ClsTipoResiduo clsTipoResiduo;
	private CantidadResiduosService cantidadResiduosService;
	private List<ClsCantidadResiduos> listaCantidadResiduos;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCantidadResiduos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCantidadResiduos(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			cantidadResiduosService = new CantidadResiduosService();
			
			jsonEntradaListaCantidadResiduos(paramJson);
			
			log.info("**Obtener Recoleccion Vehiculos");
			
			clsResultado = cantidadResiduosService.listarCantidadResiduos(clsCantidadResiduos);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCantidadResiduos\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		cantidadResiduosService = new CantidadResiduosService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaCantidadResiduos(paramJson);
	
			for (ClsCantidadResiduos clsCantidadResiduos : listaCantidadResiduos) {
				clsResultado = cantidadResiduosService.insertar(clsCantidadResiduos);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		cantidadResiduosService = new CantidadResiduosService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCantidadResiduos(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCantidadResiduos clsCantidadResiduos : listaCantidadResiduos) {
			clsResultado = cantidadResiduosService.actualizar(clsCantidadResiduos);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaCantidadResiduos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantidadResiduosRestService : jsonEntradaCantidadResiduos" + jsonEntrada);

		JSONObject jsonObjectTipoResiduos;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCantidadResiduos = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCantidadResiduos = new ClsCantidadResiduos();
			clsRecRcdOm = new ClsRecRcdOm();
			clsTipoResiduo = new ClsTipoResiduo();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("rec_rcd_om_id") != null && innerObject.get("rec_rcd_om_id").toString() != "") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(innerObject.get("rec_rcd_om_id").toString()));
			} 
			else {
				clsRecRcdOm.setRec_rcd_om_id(0);
			}
			
			clsCantidadResiduos.setRecRcdOm(clsRecRcdOm);
			
			if (innerObject.get("tipoResiduos") != null && innerObject.get("tipoResiduos").toString() != "") {
				jsonObjectTipoResiduos  = (JSONObject) innerObject.get("tipoResiduos");
				clsTipoResiduo.setTipo_residuos_id(Integer.parseInt(jsonObjectTipoResiduos.get("tipo_residuos_id").toString()));
			}
			clsCantidadResiduos.setTipoResiduos(clsTipoResiduo);
			
			if (innerObject.get("usuarioAtendido") != null && innerObject.get("usuarioAtendido").toString() != "") {
				clsCantidadResiduos.setUsuarioAtendido(Integer.parseInt(innerObject.get("usuarioAtendido").toString()));
			} else{
				clsCantidadResiduos.setUsuarioAtendido(0);
			}
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsCantidadResiduos.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else{
				clsCantidadResiduos.setCantidad(0);
			}
		
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCantidadResiduos.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCantidadResiduos.add(clsCantidadResiduos);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCantidadResiduos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantidadResiduosRestService : jsonEntradaListaCantidadResiduos" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectCantidadResiduos;
		JSONParser jsonParser = new JSONParser();
		
		clsCantidadResiduos = new ClsCantidadResiduos();
		clsRecRcdOm = new ClsRecRcdOm();
		clsTipoResiduo = new ClsTipoResiduo();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectCantidadResiduos = (JSONObject) jsonObjectPrincipal.get("CantidadResiduos");
			
			if (jsonObjectCantidadResiduos.get("rec_rcd_om_id") != null && jsonObjectCantidadResiduos.get("rec_rcd_om_id") != "" &&
					jsonObjectCantidadResiduos.get("rec_rcd_om_id") != " ") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(jsonObjectCantidadResiduos.get("rec_rcd_om_id").toString()));
			}
			else
				clsRecRcdOm.setRec_rcd_om_id(-1);
			
			clsCantidadResiduos.setRecRcdOm(clsRecRcdOm);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
