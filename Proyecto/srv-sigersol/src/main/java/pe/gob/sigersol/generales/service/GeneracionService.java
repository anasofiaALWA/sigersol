package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.GeneracionDAO;

public class GeneracionService {
	
	private static Logger log = Logger.getLogger(GeneracionService.class);
	GeneracionDAO generacionDAO = new GeneracionDAO();
	
	public ClsResultado obtener(ClsGeneracion clsGeneracion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = generacionDAO.obtener(clsGeneracion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsGeneracion clsGeneracion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = generacionDAO.insertar(clsGeneracion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsGeneracion clsGeneracion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = generacionDAO.actualizar(clsGeneracion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
