package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResSolidosEspeciales;
import pe.gob.sigersol.entities.ClsResultado;

public class ResSolidosEspecialesDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(ComposicionNDDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_list_rs_especiales = "{call SIGERSOLBL.SGR_SP_LIST_RS_ESPECIALES(?,?,?)}";
	private final String sgr_sp_ins_rs_especiales = "{call SIGERSOLBL.SGR_SP_INS_RS_ESPECIALES(?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_rs_especiales = "{call SIGERSOLBL.SGR_SP_UPD_RS_ESPECIALES(?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsResSolidosEspeciales clsResSolidosEspeciales) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsResSolidosEspeciales> lista = new ArrayList<ClsResSolidosEspeciales>();
		
		con = null;
		String sql = this.sgr_sp_list_rs_especiales;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResSolidosEspeciales.getTipors_id());
			cs.setInt(2, clsResSolidosEspeciales.getGeneracion().getGeneracion_id());
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);

			while (rs.next()) {
				ClsResSolidosEspeciales item = new ClsResSolidosEspeciales();
				ClsGeneracion generacion = new ClsGeneracion();
				try {
					//item.setResiduos_solidos_id(rs.getInt("RESIDUOS_SOLIDOS_ID"));
					item.setTipors_id(rs.getInt("TIPORS_ID"));
					item.setNombre(rs.getString("TIPORS_NOMBRE"));
					item.setInformacion(rs.getString("INFORMACION"));
					item.setPadre_id(rs.getInt("PADRE_ID"));
					item.setNivel(rs.getInt("NIVEL"));
					item.setTiene_hijos(rs.getInt("TIENE_HIJOS"));
					if(clsResSolidosEspeciales.getGeneracion().getGeneracion_id() != -1){
						item.setPorcentaje1(rs.getDouble("PORCENTAJE1"));
						item.setPorcentaje2(rs.getDouble("PORCENTAJE2"));
						item.setPorcentaje3(rs.getDouble("PORCENTAJE3"));
						item.setPorcentaje4(rs.getDouble("PORCENTAJE4"));
						item.setPorcentaje5(rs.getDouble("PORCENTAJE5"));
						item.setPorcentaje6(rs.getDouble("PORCENTAJE6"));
						item.setPorcentaje7(rs.getDouble("PORCENTAJE7"));
						generacion.setGeneracion_id(rs.getInt("GENERACION_ID"));
						item.setGeneracion(generacion);
					}
					else{
						item.setPorcentaje1(null);
						item.setPorcentaje2(null);
						item.setPorcentaje3(null);
						item.setPorcentaje4(null);
						item.setPorcentaje5(null);
						item.setPorcentaje6(null);
						item.setPorcentaje7(null);
						generacion.setGeneracion_id(-1);
						item.setGeneracion(generacion);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
				
			}
			
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsResSolidosEspeciales clsResSolidosEspeciales) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_rs_especiales;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResSolidosEspeciales.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsResSolidosEspeciales.getTipors_id());
			cs.setDouble(3, clsResSolidosEspeciales.getPorcentaje1());
			cs.setDouble(4, clsResSolidosEspeciales.getPorcentaje2());
			cs.setDouble(5, clsResSolidosEspeciales.getPorcentaje3());
			cs.setDouble(6, clsResSolidosEspeciales.getPorcentaje4());
			cs.setDouble(7, clsResSolidosEspeciales.getPorcentaje5());
			cs.setDouble(8, clsResSolidosEspeciales.getPorcentaje6());
			cs.setDouble(9, clsResSolidosEspeciales.getPorcentaje7());
			cs.setDouble(10, clsResSolidosEspeciales.getCod_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);
			
			log.info("ResSolidosEspecialesDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsResSolidosEspeciales clsResSolidosEspeciales) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_rs_especiales;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResSolidosEspeciales.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsResSolidosEspeciales.getTipors_id());
			cs.setDouble(3, clsResSolidosEspeciales.getPorcentaje1());
			cs.setDouble(4, clsResSolidosEspeciales.getPorcentaje2());
			cs.setDouble(5, clsResSolidosEspeciales.getPorcentaje3());
			cs.setDouble(6, clsResSolidosEspeciales.getPorcentaje4());
			cs.setDouble(7, clsResSolidosEspeciales.getPorcentaje5());
			cs.setDouble(8, clsResSolidosEspeciales.getPorcentaje6());
			cs.setDouble(9, clsResSolidosEspeciales.getPorcentaje7());
			cs.setInt(10, clsResSolidosEspeciales.getCod_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);

			log.info("ResSolidosEspecialesDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
