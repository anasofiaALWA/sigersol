package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CCGenComposicionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/ccGenComposicion")
@RequestScoped
public class CCGenComposicionRestService {
	
	private static Logger log = Logger.getLogger(CCGenComposicionRestService.class.getName());
	private CCGenComposicionService ccGenCompService;
	
	@POST
	@Path("/datosGenerales")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtenerGenComposicion(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		
		ccGenCompService = new CCGenComposicionService();
		Integer dispFinalId = jsonEntradaListComposicion(paramJson);
		clsResultado = ccGenCompService.obtenerGenComposicion(dispFinalId);
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		
		return lRespuesta.toJSon();
		
	}
	
	
	private  Integer jsonEntradaListComposicion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CCGenComposicionRestService :	jsonEntradaListComposicion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		Integer disposicionFinalId =-1;

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Filtro");

			if (jsonObject.get("DispFinalId") != null && jsonObject.get("DispFinalId") != "") {
				disposicionFinalId = Integer.parseInt(jsonObject.get("DispFinalId").toString());
			}			

		} catch (Exception e) {
			// TODO: handle exception
		}
		return disposicionFinalId;
	}
	

}
