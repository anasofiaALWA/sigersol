/**
 * 
 */
package pe.gob.sigersol.sesion.service;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.security.authentication.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.AuthResponse;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSesion;
import pe.gob.sigersol.entities.ClsUsuario;
import pe.gob.sigersol.entities.dao.SesionDAO;
import pe.gob.sigersol.util.ApplicationProperties;

/**
 * @author Alwa
 *
 */
public class SessionService {

	private static Logger log = Logger.getLogger(SessionService.class.getName());
	PasswordEncoder passwordEncoder;
	protected static Integer sistemId = Integer
			.parseInt(ApplicationProperties.getProperty(ApplicationProperties.ApplicationPropertiesEnum.SISTEMA_ID));
	protected static String appServer = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.APP_SERVER);
	protected static String UrlApp = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.LINK_APP);
	SesionDAO dao = new SesionDAO();

	public ClsResultado validarUsuario(String nombreUsuario, String contrasenha) {
		ClsResultado resultado = new ClsResultado();

		try {
			resultado = dao.minam_obtener_usuarioId_activo(nombreUsuario);
			if (resultado.isExito()) {
				// Validar contraseña de usuario
				ClsUsuario usuario = (ClsUsuario) resultado.getObjeto();
				String encrypted = passwordEncoder.encode(contrasenha);
				if (usuario.getContrasenha().equalsIgnoreCase(encrypted)) {
					//autenticado =true;
					
				
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return resultado;
	}

	public ClsResultado validarUsuarioTemp(String nombreUsuario, String contrasenha, HttpServletRequest request) {
		ClsResultado clsResultado = new ClsResultado();
		try {
			ClsResultado resultado = dao.validarLoginTemp(nombreUsuario, contrasenha);
			if (resultado.isExito()) {
				Integer userId = (Integer) resultado.getObjeto();
				clsResultado = dao.obtener_usuarioTemp(userId);
				createSession(request, userId);
			}
		} catch (Exception e) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);

		}

		return clsResultado;
	}

	public HttpSession createSession(HttpServletRequest request, Integer user_id) {
		HttpSession session = null;
		ClsSesion clsSesion = null;
		Integer sesionId = null;
		try {

			// crea objeto sesion

			clsSesion = new ClsSesion();
			clsSesion.setUsuario_id(user_id);
			clsSesion.setSistema_id(sistemId);
			clsSesion.setApp_server(appServer);
			clsSesion.setMac_adress(getMacAddr());
			clsSesion.setIp_adress(getClientIpAddr(request));
			clsSesion.setSistema_operativo(getClientOS(request));
			clsSesion.setLink_app(UrlApp);
			// inserta sesion en bd
			// ClsResultado sesionResult = dao.minam_registra_sesion(clsSesion);
			ClsResultado sesionResult = new ClsResultado();
			sesionResult.setExito(true);
			sesionResult.setObjeto(11);
			if (sesionResult.isExito()) {
				sesionId = (Integer) sesionResult.getObjeto();
				clsSesion.setSesion_id(sesionId);
				// crear HhhtpSesion
				log.info("session creada");
				AuthResponse loginInfo = new AuthResponse(clsSesion);
				session = request.getSession(true);
				session.setAttribute("sesion", loginInfo.getSesion());
				session.setAttribute("token", loginInfo.getToken());
			}
			// session.setMaxInactiveInterval(30);

		} catch (Exception e) {
			session = null;
		}

		return session;
	}

	private String getClientOS(HttpServletRequest request) {

		String browserDetails = request.getHeader("User-Agent");
		String userAgent = browserDetails;

		String os = "";

		// =================OS=======================
		if (userAgent.toLowerCase().indexOf("windows") >= 0) {
			os = "Windows";
		} else if (userAgent.toLowerCase().indexOf("mac") >= 0) {
			os = "Mac";
		} else if (userAgent.toLowerCase().indexOf("x11") >= 0) {
			os = "Unix";
		} else if (userAgent.toLowerCase().indexOf("android") >= 0) {
			os = "Android";
		} else if (userAgent.toLowerCase().indexOf("iphone") >= 0) {
			os = "IPhone";
		} else {
			os = "UnKnown, More-Info: " + userAgent;
		}
		log.info("OS >> " + os);
		return os;
	}

	private String getClientIpAddr(HttpServletRequest request) {

		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		log.info("IP" + ip);
		return ip;
	}

	private String getMacAddr() {

		InetAddress ip;
		String ipAddress = "";
		String macAddress = "";
		try {

			ip = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			ipAddress = ip.getHostAddress();
			macAddress = sb.toString();

		} catch (UnknownHostException e) {
			ipAddress = "";
			macAddress = "";
		} catch (SocketException e) {
			ipAddress = "";
			macAddress = "";
		}
		log.info("macAddress:" + macAddress);
		return macAddress;
	}

}
