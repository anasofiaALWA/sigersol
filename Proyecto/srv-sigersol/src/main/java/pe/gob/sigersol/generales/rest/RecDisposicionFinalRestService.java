package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsRecDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.RecDisposicionFinalService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase RecDisposicionFinalRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de recolección permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/recoleccionDf")
@RequestScoped
public class RecDisposicionFinalRestService {

	private static Logger log = Logger.getLogger(RecDisposicionFinalRestService.class.getName());
	
	private ClsRecDisposicionFinal clsRecDisposicionFinal;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private RecDisposicionFinalService recDisposicionFinalService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			recDisposicionFinalService = new RecDisposicionFinalService();
			
			jsonEntradaRecDisposicionFinal(paramJson);
			
			log.info("**obtener RecDisposicionFinal");
			
			clsResultado = recDisposicionFinalService.obtener(clsRecDisposicionFinal);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"recoleccion_df\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		recDisposicionFinalService = new RecDisposicionFinalService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			jsonEntradaRecDisposicionFinal(paramJson);
			
			clsResultado = recDisposicionFinalService.insertar(clsRecDisposicionFinal);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		recDisposicionFinalService = new RecDisposicionFinalService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaRecDisposicionFinal(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = recDisposicionFinalService.actualizar(clsRecDisposicionFinal);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de RecDisposicionFinal
	 * 
	 * */
	private void jsonEntradaRecDisposicionFinal(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> DisposicionFinalRestService : jsonEntradaDisposicionFinal" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectRecDisposicionFinal;
		JSONParser jsonParser = new JSONParser();
		
		clsRecDisposicionFinal = new ClsRecDisposicionFinal();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectRecDisposicionFinal = (JSONObject) jsonObjectPrincipal.get("RecoleccionDf");
			
			if (jsonObjectRecDisposicionFinal.get("rec_disposicion_final_id") != null && jsonObjectRecDisposicionFinal.get("rec_disposicion_final_id") != "") {
				clsRecDisposicionFinal.setRec_disposicion_final_id(Integer.parseInt(jsonObjectRecDisposicionFinal.get("rec_disposicion_final_id").toString()));
			} else {
				clsRecDisposicionFinal.setRec_disposicion_final_id(-1);
			}
			
			if (jsonObjectRecDisposicionFinal.get("ciclo_grs_id") != null && jsonObjectRecDisposicionFinal.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectRecDisposicionFinal.get("ciclo_grs_id").toString()));
			}
			
			clsRecDisposicionFinal.setCiclo_grs_id(clsCicloGestionResiduos);
			
			if (jsonObjectRecDisposicionFinal.get("adm_serv_id") != null && jsonObjectRecDisposicionFinal.get("adm_serv_id") != "") {
				clsRecDisposicionFinal.setAdm_serv_id(jsonObjectRecDisposicionFinal.get("adm_serv_id").toString());
			}
			
			if (jsonObjectRecDisposicionFinal.get("costo_anual") != null && jsonObjectRecDisposicionFinal.get("costo_anual") != "") {
				clsRecDisposicionFinal.setCosto_anual(Integer.parseInt(jsonObjectRecDisposicionFinal.get("costo_anual").toString()));
			}
			
			if (jsonObjectRecDisposicionFinal.get("fr1") != null && jsonObjectRecDisposicionFinal.get("fr1") != "") {
				clsRecDisposicionFinal.setFr1(Integer.parseInt(jsonObjectRecDisposicionFinal.get("fr1").toString()));
			}
			
			if (jsonObjectRecDisposicionFinal.get("fr2") != null && jsonObjectRecDisposicionFinal.get("fr2") != "") {
				clsRecDisposicionFinal.setFr2(Integer.parseInt(jsonObjectRecDisposicionFinal.get("fr2").toString()));
				
				clsRecDisposicionFinal.setFrecuencia(clsRecDisposicionFinal.getFr1().toString().concat("/").concat(clsRecDisposicionFinal.getFr2().toString()));
			}
			
			if (jsonObjectRecDisposicionFinal.get("cantidad_residuos") != null && jsonObjectRecDisposicionFinal.get("cantidad_residuos") != "") {
				clsRecDisposicionFinal.setCantidad_residuos(Integer.parseInt(jsonObjectRecDisposicionFinal.get("cantidad_residuos").toString()));
			}
			else{
				clsRecDisposicionFinal.setCantidad_residuos(0);
			}
			
			if (jsonObjectRecDisposicionFinal.get("otros_descripcion") != null && jsonObjectRecDisposicionFinal.get("otros_descripcion") != "") {
				clsRecDisposicionFinal.setOtros_descripcion(jsonObjectRecDisposicionFinal.get("otros_descripcion").toString());
			}
			else{
				clsRecDisposicionFinal.setOtros_descripcion("");
			}
			
			if (jsonObjectRecDisposicionFinal.get("otros_antig_promedio") != null && jsonObjectRecDisposicionFinal.get("otros_antig_promedio") != "") {
				clsRecDisposicionFinal.setOtros_antig_promedio(Integer.parseInt(jsonObjectRecDisposicionFinal.get("otros_antig_promedio").toString()));
			}
			else{
				clsRecDisposicionFinal.setOtros_antig_promedio(0);
			}
			
			if (jsonObjectRecDisposicionFinal.get("otros_capacidad") != null && jsonObjectRecDisposicionFinal.get("otros_capacidad") != "") {
				clsRecDisposicionFinal.setOtros_capacidad(Integer.parseInt(jsonObjectRecDisposicionFinal.get("otros_capacidad").toString()));
			}
			else{
				clsRecDisposicionFinal.setOtros_capacidad(0);
			}
			
			if (jsonObjectRecDisposicionFinal.get("otros_recoleccion_anual") != null && jsonObjectRecDisposicionFinal.get("otros_recoleccion_anual") != "") {
				clsRecDisposicionFinal.setOtros_recoleccion_anual(Integer.parseInt(jsonObjectRecDisposicionFinal.get("otros_recoleccion_anual").toString()));
			}
			else{
				clsRecDisposicionFinal.setOtros_recoleccion_anual(0);
			}
			
			if (jsonObjectRecDisposicionFinal.get("otros_numero_unidades") != null && jsonObjectRecDisposicionFinal.get("otros_numero_unidades") != "") {
				clsRecDisposicionFinal.setOtros_numero_unidades(Integer.parseInt(jsonObjectRecDisposicionFinal.get("otros_numero_unidades").toString()));
			}
			else{
				clsRecDisposicionFinal.setOtros_numero_unidades(0);
			}
			
			if (jsonObjectRecDisposicionFinal.get("cod_usuario") != null && jsonObjectRecDisposicionFinal.get("cod_usuario") != "") {
				clsRecDisposicionFinal.setCodigo_usuario(Integer.parseInt(jsonObjectRecDisposicionFinal.get("cod_usuario").toString()));
			}
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
