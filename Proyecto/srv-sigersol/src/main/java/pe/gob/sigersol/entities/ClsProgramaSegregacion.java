package pe.gob.sigersol.entities;

public class ClsProgramaSegregacion {

	private Integer programa_segregacion_id;
	private ClsValorizacion valorizacion;
	private Integer cantidad_recicladores;
	private Integer viviendas_participantes;
	private Integer caf_adm_propia;
	private Integer caf_tercerizado;
	private Integer caf_mixto;
	private Integer recicladores_hombre_25;
	private Integer recicladores_mujer_25;
	private Integer recicladores_hombre_25_65;
	private Integer recicladores_mujer_25_65;  
	private Integer recicladores_hombre_65;
	private Integer recicladores_mujer_65;
	private Integer codigo_usuario;
	
	public Integer getPrograma_segregacion_id() {
		return programa_segregacion_id;
	}
	public void setPrograma_segregacion_id(Integer programa_segregacion_id) {
		this.programa_segregacion_id = programa_segregacion_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public Integer getCantidad_recicladores() {
		return cantidad_recicladores;
	}
	public void setCantidad_recicladores(Integer cantidad_recicladores) {
		this.cantidad_recicladores = cantidad_recicladores;
	}
	public Integer getViviendas_participantes() {
		return viviendas_participantes;
	}
	public void setViviendas_participantes(Integer viviendas_participantes) {
		this.viviendas_participantes = viviendas_participantes;
	}
	public Integer getCaf_adm_propia() {
		return caf_adm_propia;
	}
	public void setCaf_adm_propia(Integer caf_adm_propia) {
		this.caf_adm_propia = caf_adm_propia;
	}
	public Integer getCaf_tercerizado() {
		return caf_tercerizado;
	}
	public void setCaf_tercerizado(Integer caf_tercerizado) {
		this.caf_tercerizado = caf_tercerizado;
	}
	public Integer getCaf_mixto() {
		return caf_mixto;
	}
	public void setCaf_mixto(Integer caf_mixto) {
		this.caf_mixto = caf_mixto;
	}
	public Integer getRecicladores_hombre_25() {
		return recicladores_hombre_25;
	}
	public void setRecicladores_hombre_25(Integer recicladores_hombre_25) {
		this.recicladores_hombre_25 = recicladores_hombre_25;
	}
	public Integer getRecicladores_mujer_25() {
		return recicladores_mujer_25;
	}
	public void setRecicladores_mujer_25(Integer recicladores_mujer_25) {
		this.recicladores_mujer_25 = recicladores_mujer_25;
	}
	public Integer getRecicladores_hombre_25_65() {
		return recicladores_hombre_25_65;
	}
	public void setRecicladores_hombre_25_65(Integer recicladores_hombre_25_65) {
		this.recicladores_hombre_25_65 = recicladores_hombre_25_65;
	}
	public Integer getRecicladores_mujer_25_65() {
		return recicladores_mujer_25_65;
	}
	public void setRecicladores_mujer_25_65(Integer recicladores_mujer_25_65) {
		this.recicladores_mujer_25_65 = recicladores_mujer_25_65;
	}
	public Integer getRecicladores_hombre_65() {
		return recicladores_hombre_65;
	}
	public void setRecicladores_hombre_65(Integer recicladores_hombre_65) {
		this.recicladores_hombre_65 = recicladores_hombre_65;
	}
	public Integer getRecicladores_mujer_65() {
		return recicladores_mujer_65;
	}
	public void setRecicladores_mujer_65(Integer recicladores_mujer_65) {
		this.recicladores_mujer_65 = recicladores_mujer_65;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
