package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCompMuniDf;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsResiduosMunicipales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CompMuniDfService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase CompMuniDfRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de disposicion final permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/compMuniDfX")
@RequestScoped
public class CompMuniDfRestService {

	private static Logger log = Logger.getLogger(CompMuniDfRestService.class.getName());
	private ClsCompMuniDf clsCompMuniDf;
	private ClsResiduosMunicipales clsResiduosMunicipales;
	private ClsDisposicionFinal clsDisposicionFinal;
	private CompMuniDfService compMuniDfService;
	private List<ClsCompMuniDf> listaCompMuniDf;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCompMuni")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCompMuni(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			compMuniDfService = new CompMuniDfService();
			
			jsonEntradaListaCompMuni(paramJson);
			
			log.info("**Obtener Composicion Municipales**");
			
			clsResultado = compMuniDfService.listarCompMuni(clsCompMuniDf);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCompMuni\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		compMuniDfService = new CompMuniDfService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			jsonEntradaCompMuni(paramJson);

			for (ClsCompMuniDf clsCompMuniDf : listaCompMuniDf) {
				clsResultado = compMuniDfService.insertar(clsCompMuniDf);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		compMuniDfService = new CompMuniDfService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCompMuni(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCompMuniDf clsCompMuniDf : listaCompMuniDf) {
			clsResultado = compMuniDfService.actualizar(clsCompMuniDf);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de CompMuniDf
	 * 
	 * */
	private void jsonEntradaCompMuni(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CompMuniDfRestService : jsonEntradaCompMuni" + jsonEntrada);

		JSONObject jsonObjectResMunicipales;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCompMuniDf = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCompMuniDf = new ClsCompMuniDf();
			clsResiduosMunicipales = new ClsResiduosMunicipales();
			clsDisposicionFinal = new ClsDisposicionFinal();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("disposicion_final_id") != null && innerObject.get("disposicion_final_id").toString() != "") {
				clsDisposicionFinal.setDisposicion_final_id(Integer.parseInt(innerObject.get("disposicion_final_id").toString()));
			}
			
			clsCompMuniDf.setDisposicionFinal(clsDisposicionFinal);
			
			if (innerObject.get("residuosMunicipales") != null && innerObject.get("residuosMunicipales").toString() != "") {
				jsonObjectResMunicipales  = (JSONObject) innerObject.get("residuosMunicipales");
				clsResiduosMunicipales.setResiduos_municipales_id(Integer.parseInt(jsonObjectResMunicipales.get("residuos_municipales_id").toString()));
			}
			clsCompMuniDf.setResiduosMunicipales(clsResiduosMunicipales);
			
			if (innerObject.get("porcentaje") != null && innerObject.get("porcentaje").toString() != "") {
				clsCompMuniDf.setPorcentaje(Integer.parseInt(innerObject.get("porcentaje").toString()));
			} else {
				clsCompMuniDf.setPorcentaje(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCompMuniDf.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCompMuniDf.add(clsCompMuniDf);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCompMuni(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaListaCompMuni" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectCompMuni;
		JSONParser jsonParser = new JSONParser();
		
		clsCompMuniDf = new ClsCompMuniDf();
		clsResiduosMunicipales = new ClsResiduosMunicipales();
		clsDisposicionFinal = new ClsDisposicionFinal();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectCompMuni = (JSONObject) jsonObjectPrincipal.get("CompMuniDf");
			
			if (jsonObjectCompMuni.get("disposicion_final_id") != null && jsonObjectCompMuni.get("disposicion_final_id") != "" &&
					jsonObjectCompMuni.get("disposicion_final_id") != " ") {
				clsDisposicionFinal.setDisposicion_final_id(Integer.parseInt(jsonObjectCompMuni.get("disposicion_final_id").toString()));
			}
			else
				clsDisposicionFinal.setDisposicion_final_id(-1);
			
			clsCompMuniDf.setDisposicionFinal(clsDisposicionFinal);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
