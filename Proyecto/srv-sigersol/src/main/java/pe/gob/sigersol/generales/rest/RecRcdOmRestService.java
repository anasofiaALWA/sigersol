package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.RecRcdOmService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase RecRcdOmRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de recolección permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/recoleccionRcd")
@RequestScoped
public class RecRcdOmRestService {

	private static Logger log = Logger.getLogger(SolicitudRestService.class.getName());
	
	private ClsRecRcdOm clsRecRcdOm;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private RecRcdOmService recRcdOmService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			recRcdOmService = new RecRcdOmService();
			
			jsonEntradaRecRcdOm(paramJson);
			
			log.info("**obtener RecRcdOm**");
			
			clsResultado = recRcdOmService.obtener(clsRecRcdOm);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"recoleccion_rcd\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		recRcdOmService = new RecRcdOmService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaRecRcdOm(paramJson);
			clsResultado = recRcdOmService.insertar(clsRecRcdOm);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		recRcdOmService = new RecRcdOmService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaRecRcdOm(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = recRcdOmService.actualizar(clsRecRcdOm);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de RecRcdOm
	 * 
	 * */
	private void jsonEntradaRecRcdOm(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> RecRcdOmRestService :jsonEntradaRecRcdOm" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectRecoleccionRcd;
		JSONParser jsonParser = new JSONParser();
		
		clsRecRcdOm = new ClsRecRcdOm();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectRecoleccionRcd = (JSONObject) jsonObjectPrincipal.get("RecoleccionRcd");
			
			if (jsonObjectRecoleccionRcd.get("rec_rcd_om_id") != null && jsonObjectRecoleccionRcd.get("rec_rcd_om_id") != "") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(jsonObjectRecoleccionRcd.get("rec_rcd_om_id").toString()));
			} else {
				clsRecRcdOm.setRec_rcd_om_id(-1);
			}
			
			if (jsonObjectRecoleccionRcd.get("ciclo_grs_id") != null && jsonObjectRecoleccionRcd.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectRecoleccionRcd.get("ciclo_grs_id").toString()));
			}
			
			clsRecRcdOm.setCiclo_grs_id(clsCicloGestionResiduos);
			
			if (jsonObjectRecoleccionRcd.get("flag_rcd") != null && jsonObjectRecoleccionRcd.get("flag_rcd") != "") {
				clsRecRcdOm.setFlag_rcd(Integer.parseInt(jsonObjectRecoleccionRcd.get("flag_rcd").toString()));
			}
			
			if (jsonObjectRecoleccionRcd.get("adm_serv_id") != null && jsonObjectRecoleccionRcd.get("adm_serv_id") != "") {
				clsRecRcdOm.setAdm_serv_id(jsonObjectRecoleccionRcd.get("adm_serv_id").toString());
			}
			
			if (jsonObjectRecoleccionRcd.get("costo_anual") != null && jsonObjectRecoleccionRcd.get("costo_anual") != "") {
				clsRecRcdOm.setCosto_anual(Integer.parseInt(jsonObjectRecoleccionRcd.get("costo_anual").toString()));
			}
			else
			{
				clsRecRcdOm.setCosto_anual(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("cantidad_residuos") != null && jsonObjectRecoleccionRcd.get("cantidad_residuos") != "") {
				clsRecRcdOm.setCantidad_residuos(Integer.parseInt(jsonObjectRecoleccionRcd.get("cantidad_residuos").toString()));
			}
			else{
				clsRecRcdOm.setCantidad_residuos(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("usuarios_atendidos") != null && jsonObjectRecoleccionRcd.get("usuarios_atendidos") != "") {
				clsRecRcdOm.setUsuarios_atendidos(Integer.parseInt(jsonObjectRecoleccionRcd.get("usuarios_atendidos").toString()));
			}
			else{
				clsRecRcdOm.setUsuarios_atendidos(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("otros_descripcion") != null && jsonObjectRecoleccionRcd.get("otros_descripcion") != "") {
				clsRecRcdOm.setOtros_descripcion(jsonObjectRecoleccionRcd.get("otros_descripcion").toString());
			}
			else{
				clsRecRcdOm.setOtros_descripcion("");
			}
			
			if (jsonObjectRecoleccionRcd.get("otros_antig_promedio") != null && jsonObjectRecoleccionRcd.get("otros_antig_promedio") != "") {
				clsRecRcdOm.setOtros_antig_promedio(Integer.parseInt(jsonObjectRecoleccionRcd.get("otros_antig_promedio").toString()));
			}
			else{
				clsRecRcdOm.setOtros_antig_promedio(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("otros_capacidad") != null && jsonObjectRecoleccionRcd.get("otros_capacidad") != "") {
				clsRecRcdOm.setOtros_capacidad(Integer.parseInt(jsonObjectRecoleccionRcd.get("otros_capacidad").toString()));
			}
			else{
				clsRecRcdOm.setOtros_capacidad(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("otros_recoleccion_anual") != null && jsonObjectRecoleccionRcd.get("otros_recoleccion_anual") != "") {
				clsRecRcdOm.setOtros_recoleccion_anual(Integer.parseInt(jsonObjectRecoleccionRcd.get("otros_recoleccion_anual").toString()));
			}
			else{
				clsRecRcdOm.setOtros_recoleccion_anual(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("otros_numero_unidades") != null && jsonObjectRecoleccionRcd.get("otros_numero_unidades") != "") {
				clsRecRcdOm.setOtros_numero_unidades(Integer.parseInt(jsonObjectRecoleccionRcd.get("otros_numero_unidades").toString()));
			}
			else{
				clsRecRcdOm.setOtros_numero_unidades(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("flag_municipal") != null && jsonObjectRecoleccionRcd.get("flag_municipal") != "") {
				clsRecRcdOm.setFlag_municipal(Integer.parseInt(jsonObjectRecoleccionRcd.get("flag_municipal").toString()));
			}
			else{
				clsRecRcdOm.setFlag_municipal(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("otro_munic_adm_serv_id") != null && jsonObjectRecoleccionRcd.get("otro_munic_adm_serv_id") != "") {
				clsRecRcdOm.setOtro_munic_adm_serv_id(jsonObjectRecoleccionRcd.get("otro_munic_adm_serv_id").toString());
			}
			else{
				clsRecRcdOm.setOtro_munic_adm_serv_id("");
			}
		
			if (jsonObjectRecoleccionRcd.get("otro_munic_costo") != null && jsonObjectRecoleccionRcd.get("otro_munic_costo") != "") {
				clsRecRcdOm.setOtro_munic_costo(Integer.parseInt(jsonObjectRecoleccionRcd.get("otro_munic_costo").toString()));
			}
			else{
				clsRecRcdOm.setOtro_munic_costo(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("descrip_residuos_otros") != null && jsonObjectRecoleccionRcd.get("descrip_residuos_otros") != "") {
				clsRecRcdOm.setDescrip_residuos_otros(jsonObjectRecoleccionRcd.get("descrip_residuos_otros").toString());
			}
			else{
				clsRecRcdOm.setDescrip_residuos_otros("");
			}
		
			if (jsonObjectRecoleccionRcd.get("usuario_atendido_otros") != null && jsonObjectRecoleccionRcd.get("usuario_atendido_otros") != "") {
				clsRecRcdOm.setUsuario_atendido_otros(Integer.parseInt(jsonObjectRecoleccionRcd.get("usuario_atendido_otros").toString()));
			}
			else{
				clsRecRcdOm.setUsuario_atendido_otros(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("cantidad_otros") != null && jsonObjectRecoleccionRcd.get("cantidad_otros") != "") {
				clsRecRcdOm.setCantidad_otros(Integer.parseInt(jsonObjectRecoleccionRcd.get("cantidad_otros").toString()));
			}
			else{
				clsRecRcdOm.setCantidad_otros(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("usuarios_rcd_om") != null && jsonObjectRecoleccionRcd.get("usuarios_rcd_om") != "") {
				clsRecRcdOm.setUsuarios_rcd_om(Integer.parseInt(jsonObjectRecoleccionRcd.get("usuarios_rcd_om").toString()));
			}
			else{
				clsRecRcdOm.setUsuarios_rcd_om(0);
			}
			
			if (jsonObjectRecoleccionRcd.get("cod_usuario") != null && jsonObjectRecoleccionRcd.get("cod_usuario") != "") {
				clsRecRcdOm.setCodigo_usuario(Integer.parseInt(jsonObjectRecoleccionRcd.get("cod_usuario").toString()));
			}
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
