package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsLugarAprovechamiento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.LugarAprovechamientoDAO;

public class LugarAprovechamientoService {

	private static Logger log = Logger.getLogger(LugarAprovechamientoService.class);
	LugarAprovechamientoDAO lugarAprovechamientoDAO = new LugarAprovechamientoDAO();
	
	public ClsResultado obtener(ClsLugarAprovechamiento clsLugarAprovechamiento){
		ClsResultado clsResultado = null;
		try {
			clsResultado = lugarAprovechamientoDAO.obtener(clsLugarAprovechamiento);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = lugarAprovechamientoDAO.insertar(clsLugarAprovechamiento);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = lugarAprovechamientoDAO.actualizar(clsLugarAprovechamiento);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizacion(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = lugarAprovechamientoDAO.actualizacion(clsLugarAprovechamiento);
		} catch (Exception e) {
			log.info("message actualizacion >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
