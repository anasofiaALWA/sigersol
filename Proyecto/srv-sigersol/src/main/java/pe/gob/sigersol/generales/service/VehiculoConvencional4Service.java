package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoConvencional;
import pe.gob.sigersol.entities.dao.VehiculoConvencional4DAO;

public class VehiculoConvencional4Service {

	private static Logger log = Logger.getLogger(VehiculoConvencional4Service.class);
	VehiculoConvencional4DAO vehiculoConvencional4DAO = new VehiculoConvencional4DAO();
	
	public ClsResultado listarVehiculoConvencional(ClsVehiculoConvencional clsVehiculoConvencional){
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional4DAO.listarVehiculoConvencional(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoConvencional clsVehiculoConvencional) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional4DAO.insertar(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsVehiculoConvencional clsVehiculoConvencional) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional4DAO.actualizar(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
