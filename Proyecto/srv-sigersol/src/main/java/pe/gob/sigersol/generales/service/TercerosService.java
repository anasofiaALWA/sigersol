package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTerceros;
import pe.gob.sigersol.entities.dao.TercerosDAO;

public class TercerosService {

	private static Logger log = Logger.getLogger(TercerosService.class);
	TercerosDAO tercerosDAO = new TercerosDAO();
	
	public ClsResultado obtener(ClsTerceros clsTerceros){
		ClsResultado clsResultado = null;
		try {
			clsResultado = tercerosDAO.obtener(clsTerceros);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTerceros clsTerceros) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = tercerosDAO.insertar(clsTerceros);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsTerceros clsTerceros) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = tercerosDAO.actualizar(clsTerceros);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
