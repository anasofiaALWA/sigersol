package pe.gob.sigersol.core.service;

import pe.gob.sigersol.core.bpm.ConexionBPM;

/**
 * @author lflorian
 */

public class GenericoService {

	public ConexionBPM abrirConexion(String _usuario, String _password, String deploymentUrlStr, String deploymentId,
			String processDefinitionId) {

		System.out.println("abrirConexion>> " + _usuario);
		ConexionBPM conexion;

		System.out.println("abrirConexion>> deploymentUrlStr: " + deploymentUrlStr + " deploymentId: " + deploymentId
				+ " processDefinitionId: " + processDefinitionId);

		conexion = new ConexionBPM(deploymentUrlStr, processDefinitionId, deploymentId, _usuario, _password);

		return conexion;
	}

}
