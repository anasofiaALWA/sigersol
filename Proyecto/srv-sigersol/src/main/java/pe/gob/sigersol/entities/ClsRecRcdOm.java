package pe.gob.sigersol.entities;

public class ClsRecRcdOm {

	private Integer rec_rcd_om_id;
	private ClsCicloGestionResiduos ciclo_grs_id;
	private Integer flag_rcd;
	private String adm_serv_id;
	private Integer costo_anual;
	private Integer cantidad_residuos;
	private Integer usuarios_atendidos;
	private String otros_descripcion;
	private Integer otros_antig_promedio;
	private Integer otros_capacidad;
	private Integer otros_recoleccion_anual;
	private Integer otros_numero_unidades;
	private Integer flag_municipal;
	private String otro_munic_adm_serv_id;
	private Integer otro_munic_costo;
	private String descrip_residuos_otros;
	private Integer usuario_atendido_otros;
	private Integer cantidad_otros;
	private Integer usuarios_rcd_om;
	private Double rcd_om;
	private Integer codigo_usuario;
	
	
	public Integer getRec_rcd_om_id() {
		return rec_rcd_om_id;
	}
	public void setRec_rcd_om_id(Integer rec_rcd_om_id) {
		this.rec_rcd_om_id = rec_rcd_om_id;
	}
	public ClsCicloGestionResiduos getCiclo_grs_id() {
		return ciclo_grs_id;
	}
	public void setCiclo_grs_id(ClsCicloGestionResiduos ciclo_grs_id) {
		this.ciclo_grs_id = ciclo_grs_id;
	}
	public Integer getFlag_rcd() {
		return flag_rcd;
	}
	public void setFlag_rcd(Integer flag_rcd) {
		this.flag_rcd = flag_rcd;
	}
	public String getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(String adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getCantidad_residuos() {
		return cantidad_residuos;
	}
	public void setCantidad_residuos(Integer cantidad_residuos) {
		this.cantidad_residuos = cantidad_residuos;
	}
	public Integer getUsuarios_atendidos() {
		return usuarios_atendidos;
	}
	public void setUsuarios_atendidos(Integer usuarios_atendidos) {
		this.usuarios_atendidos = usuarios_atendidos;
	}
	public String getOtros_descripcion() {
		return otros_descripcion;
	}
	public void setOtros_descripcion(String otros_descripcion) {
		this.otros_descripcion = otros_descripcion;
	}
	public Integer getOtros_antig_promedio() {
		return otros_antig_promedio;
	}
	public void setOtros_antig_promedio(Integer otros_antig_promedio) {
		this.otros_antig_promedio = otros_antig_promedio;
	}
	public Integer getOtros_capacidad() {
		return otros_capacidad;
	}
	public void setOtros_capacidad(Integer otros_capacidad) {
		this.otros_capacidad = otros_capacidad;
	}
	public Integer getOtros_recoleccion_anual() {
		return otros_recoleccion_anual;
	}
	public void setOtros_recoleccion_anual(Integer otros_recoleccion_anual) {
		this.otros_recoleccion_anual = otros_recoleccion_anual;
	}
	public Integer getOtros_numero_unidades() {
		return otros_numero_unidades;
	}
	public void setOtros_numero_unidades(Integer otros_numero_unidades) {
		this.otros_numero_unidades = otros_numero_unidades;
	}
	public Integer getFlag_municipal() {
		return flag_municipal;
	}
	public void setFlag_municipal(Integer flag_municipal) {
		this.flag_municipal = flag_municipal;
	}
	public String getOtro_munic_adm_serv_id() {
		return otro_munic_adm_serv_id;
	}
	public void setOtro_munic_adm_serv_id(String otro_munic_adm_serv_id) {
		this.otro_munic_adm_serv_id = otro_munic_adm_serv_id;
	}
	public Integer getOtro_munic_costo() {
		return otro_munic_costo;
	}
	public void setOtro_munic_costo(Integer otro_munic_costo) {
		this.otro_munic_costo = otro_munic_costo;
	}
	public String getDescrip_residuos_otros() {
		return descrip_residuos_otros;
	}
	public void setDescrip_residuos_otros(String descrip_residuos_otros) {
		this.descrip_residuos_otros = descrip_residuos_otros;
	}
	public Integer getUsuario_atendido_otros() {
		return usuario_atendido_otros;
	}
	public void setUsuario_atendido_otros(Integer usuario_atendido_otros) {
		this.usuario_atendido_otros = usuario_atendido_otros;
	}
	public Integer getCantidad_otros() {
		return cantidad_otros;
	}
	public void setCantidad_otros(Integer cantidad_otros) {
		this.cantidad_otros = cantidad_otros;
	}
	public Integer getUsuarios_rcd_om() {
		return usuarios_rcd_om;
	}
	public void setUsuarios_rcd_om(Integer usuarios_rcd_om) {
		this.usuarios_rcd_om = usuarios_rcd_om;
	}
	public Double getRcd_om() {
		return rcd_om;
	}
	public void setRcd_om(Double rcd_om) {
		this.rcd_om = rcd_om;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
