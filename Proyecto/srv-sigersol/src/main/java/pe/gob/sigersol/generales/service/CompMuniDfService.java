package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCompMuniDf;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CompMuniDfDAO;


public class CompMuniDfService {

	private static Logger log = Logger.getLogger(CompMuniDfService.class);
	CompMuniDfDAO compMuniDfDAO = new CompMuniDfDAO();
	
	public ClsResultado listarCompMuni(ClsCompMuniDf clsCompMuniDf){
		ClsResultado clsResultado = null;
		try {
			clsResultado = compMuniDfDAO.listarCompMuni(clsCompMuniDf);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCompMuniDf clsCompMuniDf) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = compMuniDfDAO.insertar(clsCompMuniDf);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCompMuniDf clsCompMuniDf) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = compMuniDfDAO.actualizar(clsCompMuniDf);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
