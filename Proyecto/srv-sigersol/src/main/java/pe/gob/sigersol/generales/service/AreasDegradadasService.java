package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAreasDegradadas;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AreasDegradadasDAO;

public class AreasDegradadasService {

	private static Logger log = Logger.getLogger(AreasDegradadasService.class);
	AreasDegradadasDAO areasDegradadasDAO = new AreasDegradadasDAO();
	
	public ClsResultado obtener(ClsDisposicionFinalAdm clsDisposicionFinalAdm){
		ClsResultado clsResultado = null;
		try {
			clsResultado = areasDegradadasDAO.obtener(clsDisposicionFinalAdm);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = areasDegradadasDAO.insertar(clsDisposicionFinalAdm);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = areasDegradadasDAO.actualizar(clsDisposicionFinalAdm);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
