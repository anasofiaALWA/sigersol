package pe.gob.sigersol.entities;

public class ClsDatosGenerales {

	private Integer datos_generales_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private String nombre_alcalde;
	private String direccion;
	private String telefono;
	private String fax;
	private String email;
	private String clasif_municipalidad;
	private String tipo_municipalidad;
	private String nombre_slp;
	private String responsable_slp;
	private String telefono_slp;
	private String celular_slp;
	private String email_slp;
	private String nombre_sigersol;
	private String telefono_sigersol;
	private String celular_sigersol;
	private String email_sigersol;
	private String direccion_sigersol;
	private Integer poblacion_urbana;
	private Integer poblacion_rural;
	private Integer total_viviendas;
    private Integer codigo_usuario;
	
    public Integer getDatos_generales_id() {
		return datos_generales_id;
	}
	public void setDatos_generales_id(Integer datos_generales_id) {
		this.datos_generales_id = datos_generales_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public String getNombre_alcalde() {
		return nombre_alcalde;
	}
	public void setNombre_alcalde(String nombre_alcalde) {
		this.nombre_alcalde = nombre_alcalde;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}	
	public String getClasif_municipalidad() {
		return clasif_municipalidad;
	}
	public void setClasif_municipalidad(String clasif_municipalidad) {
		this.clasif_municipalidad = clasif_municipalidad;
	}
	public String getTipo_municipalidad() {
		return tipo_municipalidad;
	}
	public void setTipo_municipalidad(String tipo_municipalidad) {
		this.tipo_municipalidad = tipo_municipalidad;
	}
	public String getNombre_slp() {
		return nombre_slp;
	}
	public void setNombre_slp(String nombre_slp) {
		this.nombre_slp = nombre_slp;
	}
	public String getResponsable_slp() {
		return responsable_slp;
	}
	public void setResponsable_slp(String responsable_slp) {
		this.responsable_slp = responsable_slp;
	}
	public String getTelefono_slp() {
		return telefono_slp;
	}
	public void setTelefono_slp(String telefono_slp) {
		this.telefono_slp = telefono_slp;
	}
	public String getCelular_slp() {
		return celular_slp;
	}
	public void setCelular_slp(String celular_slp) {
		this.celular_slp = celular_slp;
	}
	public String getEmail_slp() {
		return email_slp;
	}
	public void setEmail_slp(String email_slp) {
		this.email_slp = email_slp;
	}
	public String getNombre_sigersol() {
		return nombre_sigersol;
	}
	public void setNombre_sigersol(String nombre_sigersol) {
		this.nombre_sigersol = nombre_sigersol;
	}
	public String getTelefono_sigersol() {
		return telefono_sigersol;
	}
	public void setTelefono_sigersol(String telefono_sigersol) {
		this.telefono_sigersol = telefono_sigersol;
	}
	public String getCelular_sigersol() {
		return celular_sigersol;
	}
	public void setCelular_sigersol(String celular_sigersol) {
		this.celular_sigersol = celular_sigersol;
	}
	public String getEmail_sigersol() {
		return email_sigersol;
	}
	public void setEmail_sigersol(String email_sigersol) {
		this.email_sigersol = email_sigersol;
	}
	public String getDireccion_sigersol() {
		return direccion_sigersol;
	}
	public void setDireccion_sigersol(String direccion_sigersol) {
		this.direccion_sigersol = direccion_sigersol;
	}
	public Integer getPoblacion_urbana() {
		return poblacion_urbana;
	}
	public void setPoblacion_urbana(Integer poblacion_urbana) {
		this.poblacion_urbana = poblacion_urbana;
	}
	public Integer getPoblacion_rural() {
		return poblacion_rural;
	}
	public void setPoblacion_rural(Integer poblacion_rural) {
		this.poblacion_rural = poblacion_rural;
	}
	public Integer getTotal_viviendas() {
		return total_viviendas;
	}
	public void setTotal_viviendas(Integer total_viviendas) {
		this.total_viviendas = total_viviendas;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	} 
}
