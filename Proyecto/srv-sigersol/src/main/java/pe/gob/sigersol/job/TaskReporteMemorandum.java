//package pe.gob.sigersol.job;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.OutputStream;
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.jboss.logging.Logger;
//
//import pe.gob.sigersol.core.dao.GenericoDAO;
//
//
//import pe.gob.sigersol.parametros.service.UnicodeParametrosService;
//import pe.gob.sigersol.util.DateUtility;
//
//import net.sf.jasperreports.engine.JasperCompileManager;
//import net.sf.jasperreports.engine.JasperReport;
//import net.sf.jasperreports.engine.JasperRunManager;
//
//public class TaskReporteMemorandum implements Runnable {
//	UnicodeParametrosService unicodeparametros_service = new UnicodeParametrosService();
//	//MemorandumService memorandumService = new MemorandumService();
//	private static Logger log = Logger.getLogger(TaskReporteMemorandum.class.getName());
//
//	@Override
//	public void run() {
//		//List<ClsPC15> listaPC15 = memorandumService.obtener_memorandums();
//		reporteMemorandumFisico(listaPC15);
//	}
//
//	public String reporteMemorandumFisico(List<ClsPC15> listaPC15 /*, @Context HttpServletRequest request,
//			@Context HttpServletResponse response*/) {
//		File miDir = new File(".");
//		Connection connection = null;
//		byte[] reporte = null;
//		String filename = null;
//		String respuesta = "";
//		String rutaFile = "";
//		String nombre_Archivo;
//		// String pc = request.getRemoteHost();
//		String RUTA_IMG;
//
//		try {
//			// OBTENER LA CONEXION A BASE DE DATOS QUE USARA EL REPORTE
//			GenericoDAO genericodao = new GenericoDAO();
//			connection = genericodao.getConnection();
//
//			for (ClsPC15 clsPC15 : listaPC15) {
//				try {
//					// A
//					if (clsPC15.getTipo_persona().equals("A")) {
//						// rutaFile =
//						// request.getSession().getServletContext().getRealPath(
//						// unicodeparametros_service.obtener_parametros("PARAMETROS_REPORTE",
//						// "OO").trim());
//						rutaFile = miDir.getCanonicalPath()
//								+ unicodeparametros_service.obtener_parametros("PARAMETROS_REPORTE", "OOF").trim();
//						filename = unicodeparametros_service.obtener_parametros("PARAMETROS_REPORTE", "FICHERO_EXPORTF")
//								+ "reporteMemorandumOO" + "_"
//								+ DateUtility.getCurrentTimestamp_format("yyyyMMdd_HHmmss") + "_" + ".pdf";
//						// B
//					} else {
//						// rutaFile =
//						// request.getSession().getServletContext().getRealPath(
//						// unicodeparametros_service.obtener_parametros("PARAMETROS_REPORTE",
//						// "TCOS").trim());
//						rutaFile = miDir.getCanonicalPath()
//								+ unicodeparametros_service.obtener_parametros("PARAMETROS_REPORTE", "TCOSF").trim();
//						filename = unicodeparametros_service.obtener_parametros("PARAMETROS_REPORTE", "FICHERO_EXPORTF")
//								+ "reporteMemorandumTCOS" + "_"
//								+ DateUtility.getCurrentTimestamp_format("yyyyMMdd_HHmmss") + "_" + ".pdf";
//					}
//
//					JasperReport jasperReport = JasperCompileManager.compileReport(rutaFile);
//					log.info("Controller reporteResolucion DESPUES DE COMPILAR EL JASPERREPORT");
//
//					RUTA_IMG = unicodeparametros_service.obtener_parametros("PARAMETROS_REPORTE", "RUTA_IMG").trim();
//
//					Map parametros = new HashMap();
//					parametros.put("RUTA_IMG", RUTA_IMG);
//					parametros.put("P_RESOLUCION", clsPC15.getResolucion_id());
//					parametros.put("P_CIP", clsPC15.getCip());
//
//					reporte = JasperRunManager.runReportToPdf(jasperReport, parametros, connection);
////					nombre_Archivo = FuncionesBase.uploadDir.replace("\\", "/") + FuncionesBase.dirDeployed()
////							+ filename;
//					nombre_Archivo = filename;
//					// respuesta += filename + ",";
//					OutputStream out = new FileOutputStream(nombre_Archivo);
//					out.write(reporte);
//					out.close();
//
//					// Registro de auditoria funcional
//					// registraAuditoriaFuncional(pc, usuario_creacion, "Generar
//					// reporte memorandum físico",
//					// "memorandum/reporteMemorandumFisico",
//					// respuesta);
//					
//					memorandumService.cambiarRutaMemorandum(clsPC15.getCip(), nombre_Archivo);
//					
//					respuesta = "Reporte creado correctamente";
//					log.info("respuesta: " + respuesta);
//				} catch (Exception e) {
//					respuesta = "Error en la generación del reporte: " + e.getMessage();
//					log.error("ERROR! generar pdf reporte memorandum GENERACION ARCHIVO e:", e);
//					break;
//				}
//			}
//
//		} catch (Exception e) {
//			respuesta = "Error en la generación del reporte: " + e.getMessage();
//			log.error("ERROR! reporte memorandum ", e);
//		} finally {
//			if (connection != null) {
//				try {
//					connection.close();
//				} catch (SQLException e) {
//					respuesta = "Error en la generación del reporte: " + e.getMessage();
//					log.error("ERROR! reporte memorandum en cerrar conexion");
//				}
//			}
//		}
//		return respuesta;
//	}
//}
