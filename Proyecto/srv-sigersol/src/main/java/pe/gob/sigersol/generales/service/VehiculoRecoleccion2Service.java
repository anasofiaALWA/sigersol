package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoRecoleccion;
import pe.gob.sigersol.entities.dao.VehiculoRecoleccion2DAO;

public class VehiculoRecoleccion2Service {

	private static Logger log = Logger.getLogger(VehiculoRecoleccion2Service.class);
	VehiculoRecoleccion2DAO vehiculoRecoleccion2DAO = new VehiculoRecoleccion2DAO();
	
	public ClsResultado listarVehiculoRecoleccion(ClsVehiculoRecoleccion clsVehiculoRecoleccion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccion2DAO.listarVehiculoRecoleccion(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoRecoleccion clsVehiculoRecoleccion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccion2DAO.insertar(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsVehiculoRecoleccion clsVehiculoRecoleccion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccion2DAO.actualizar(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
