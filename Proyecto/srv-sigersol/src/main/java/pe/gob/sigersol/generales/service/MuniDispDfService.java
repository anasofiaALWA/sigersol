package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsMuniDispDf;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.MuniDispDfDAO;

public class MuniDispDfService {

	private static Logger log = Logger.getLogger(MuniDispDfService.class);
	MuniDispDfDAO muniDispDfDAO = new MuniDispDfDAO();
	
	public ClsResultado listarMuniDisp(ClsMuniDispDf clsMuniDispDf){
		ClsResultado clsResultado = null;
		try {
			clsResultado = muniDispDfDAO.listarMuniDisp(clsMuniDispDf);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsMuniDispDf clsMuniDispDf) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = muniDispDfDAO.insertar(clsMuniDispDf);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsMuniDispDf clsMuniDispDf) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = muniDispDfDAO.actualizar(clsMuniDispDf);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
