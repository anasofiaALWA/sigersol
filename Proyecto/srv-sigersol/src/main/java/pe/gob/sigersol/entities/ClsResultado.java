package pe.gob.sigersol.entities;
/**
 * 
 * @author Alwa
 * Clase destinada para seteo de campos comunes de resultado en transacciones CRUD
 *
 */
public class ClsResultado {
	private Object objeto; // Variable destinada a guardar el resultado de una transaccion (almacena datos y listas)
	private boolean exito;
	private String mensaje;
	

	public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

	public boolean isExito() {
		return exito;
	}

	public void setExito(boolean exito) {
		this.exito = exito;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	
}
