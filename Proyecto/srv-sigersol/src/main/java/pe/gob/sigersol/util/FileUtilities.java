package pe.gob.sigersol.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialException;

import org.jboss.logging.Logger;

public class FileUtilities {
	private static Logger log = Logger.getLogger(FileUtilities.class.getName());
	
	public static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();

		if (length > Integer.MAX_VALUE) {
			log.error("Archivo demasiado grande.");
		}

		// Create the byte array to hold the data
		byte[] bytes = new byte[(int) length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			is.close();
			throw new IOException("Could not completely read file " + file.getName());
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;
	}

	public static InputStream getInputStreamByBytes(byte[] fileByteArray) {
		InputStream is = new ByteArrayInputStream(fileByteArray);
		return is;
	}

	public static Blob getBlob(byte[] fileByteArray) throws SerialException, SQLException {
		Blob blob = new javax.sql.rowset.serial.SerialBlob(fileByteArray);
		System.out.println(blob.length());
		return blob;
	}
}
