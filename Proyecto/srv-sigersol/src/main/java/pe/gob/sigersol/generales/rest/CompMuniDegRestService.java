package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAreasDegradadas;
import pe.gob.sigersol.entities.ClsCompMuniDeg;
import pe.gob.sigersol.entities.ClsResiduosMunicipales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CompMuniDegService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase CompMuniDegRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de disposicion final permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/compMuniDegX")
@RequestScoped
public class CompMuniDegRestService {

	private static Logger log = Logger.getLogger(CompMuniDegRestService.class.getName());
	private ClsCompMuniDeg clsCompMuniDeg;
	private ClsResiduosMunicipales clsResiduosMunicipales;
	private ClsAreasDegradadas clsAreasDegradadas;
	private CompMuniDegService compMuniDegService;
	private List<ClsCompMuniDeg> listaCompMuniDeg;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCompMuni")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCompMuni(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			compMuniDegService = new CompMuniDegService();
			
			jsonEntradaListaTipoManejo(paramJson);
			
			log.info("**Obtener Composicion Municipales**");
			
			clsResultado = compMuniDegService.listarCompMuni(clsCompMuniDeg);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCompMuni\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		compMuniDegService = new CompMuniDegService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			jsonEntradaTipoManejo(paramJson);
		
			for (ClsCompMuniDeg clsCompMuniDeg : listaCompMuniDeg) {
				clsResultado = compMuniDegService.insertar(clsCompMuniDeg);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		compMuniDegService = new CompMuniDegService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaTipoManejo(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCompMuniDeg clsCompMuniDeg : listaCompMuniDeg) {
			clsResultado = compMuniDegService.actualizar(clsCompMuniDeg);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de CompMuniDeg
	 * 
	 * */
	private void jsonEntradaTipoManejo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CompMuniDfRestService : jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectCobertura;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCompMuniDeg = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCompMuniDeg = new ClsCompMuniDeg();
			clsResiduosMunicipales = new ClsResiduosMunicipales();
			clsAreasDegradadas = new ClsAreasDegradadas();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("areas_degradadas_id") != null && innerObject.get("areas_degradadas_id").toString() != "") {
				clsAreasDegradadas.setAreas_degradadas_id(Integer.parseInt(innerObject.get("areas_degradadas_id").toString()));
			}
			
			clsCompMuniDeg.setAreasDegradadas(clsAreasDegradadas);
			
			if (innerObject.get("residuosMunicipales") != null && innerObject.get("residuosMunicipales").toString() != "") {
				jsonObjectCobertura  = (JSONObject) innerObject.get("residuosMunicipales");
				clsResiduosMunicipales.setResiduos_municipales_id(Integer.parseInt(jsonObjectCobertura.get("residuos_municipales_id").toString()));
			}
			clsCompMuniDeg.setResiduosMunicipales(clsResiduosMunicipales);
			
			if (innerObject.get("porcentaje") != null && innerObject.get("porcentaje").toString() != "") {
				clsCompMuniDeg.setPorcentaje(Integer.parseInt(innerObject.get("porcentaje").toString()));
			} else {
				clsCompMuniDeg.setPorcentaje(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCompMuniDeg.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCompMuniDeg.add(clsCompMuniDeg);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaTipoManejo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectTipoManejo;
		JSONParser jsonParser = new JSONParser();
		
		clsCompMuniDeg = new ClsCompMuniDeg();
		clsResiduosMunicipales = new ClsResiduosMunicipales();
		clsAreasDegradadas = new ClsAreasDegradadas();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectTipoManejo = (JSONObject) jsonObjectPrincipal.get("CompMuniDeg");
			
			if (jsonObjectTipoManejo.get("areas_degradadas_id") != null && jsonObjectTipoManejo.get("areas_degradadas_id") != "" &&
					jsonObjectTipoManejo.get("areas_degradadas_id") != " ") {
				clsAreasDegradadas.setAreas_degradadas_id(Integer.parseInt(jsonObjectTipoManejo.get("areas_degradadas_id").toString()));
			}
			else
				clsAreasDegradadas.setAreas_degradadas_id(-1);
			
			clsCompMuniDeg.setAreasDegradadas(clsAreasDegradadas);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
