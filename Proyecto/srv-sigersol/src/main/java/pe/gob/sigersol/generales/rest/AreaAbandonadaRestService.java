package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAreaAbandonada;
import pe.gob.sigersol.entities.ClsCantVehiculos;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoVehiculo;
import pe.gob.sigersol.generales.service.AreaAbandonadaService;
import pe.gob.sigersol.generales.service.CantVehiculosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/areaAbandonada")
@RequestScoped
public class AreaAbandonadaRestService {

	private static Logger log = Logger.getLogger(AreaAbandonadaRestService.class.getName());
	private Integer accion = null;
	private ClsAreaAbandonada clsAreaAbandonada;
	private ClsMunicipalidad clsMunicipalidad;
	private AreaAbandonadaService areaAbandonadaService;
	private List<ClsAreaAbandonada> listaAreaAbandonada;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCantVehiculos(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			areaAbandonadaService = new AreaAbandonadaService();
			
			jsonEntradaListaCantVehiculos(paramJson);
			
			log.info("**Obtener Area abandonada**");
			
			clsResultado = areaAbandonadaService.listarAreaAbandonada(clsAreaAbandonada);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstAreaAbandonada\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		areaAbandonadaService = new AreaAbandonadaService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaCantVehiculos(paramJson);

			for (ClsAreaAbandonada clsAreaAbandonada : listaAreaAbandonada) {
				clsResultado = areaAbandonadaService.insertar(clsAreaAbandonada);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		areaAbandonadaService = new AreaAbandonadaService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCantVehiculos(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsAreaAbandonada clsAreaAbandonada : listaAreaAbandonada) {
			clsResultado = areaAbandonadaService.insertar(clsAreaAbandonada);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de CantVehiculos
	 * 
	 * */
	private void jsonEntradaCantVehiculos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantVehiculosRestService :	 jsonEntradaCantVehiculos" + jsonEntrada);

		JSONObject jsonObjectCobertura;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaAreaAbandonada = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsAreaAbandonada = new ClsAreaAbandonada();
			clsMunicipalidad = new ClsMunicipalidad();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("area_abandonadas_id") != null && innerObject.get("area_abandonadas_id").toString() != "") {
				clsAreaAbandonada.setArea_abandonadas_id(Integer.parseInt(innerObject.get("area_abandonadas_id").toString()));
			} else {
				clsAreaAbandonada.setArea_abandonadas_id(-1);
			}
			
			if (innerObject.get("municipalidad_id") != null && innerObject.get("municipalidad_id").toString() != "") {
				clsMunicipalidad.setMunicipalidad_id(Integer.parseInt(innerObject.get("municipalidad_id").toString()));
			}
			
			clsAreaAbandonada.setMunicipalidad(clsMunicipalidad);
			
			if (innerObject.get("anio_inicio") != null && innerObject.get("anio_inicio").toString() != "") {
				clsAreaAbandonada.setAnio_inicio(Integer.parseInt(innerObject.get("anio_inicio").toString()));
			}
			
			if (innerObject.get("anio_cierre") != null && innerObject.get("anio_cierre").toString() != "") {
				clsAreaAbandonada.setAnio_cierre(Integer.parseInt(innerObject.get("anio_cierre").toString()));
			} 
			
			if (innerObject.get("area_total") != null && innerObject.get("area_total").toString() != "") {
				clsAreaAbandonada.setArea_total(Integer.parseInt(innerObject.get("area_total").toString()));
			}
			
			if (innerObject.get("responsable") != null && innerObject.get("responsable").toString() != "") {
				clsAreaAbandonada.setResponsable(innerObject.get("responsable").toString());
			} 
			
			if (innerObject.get("altura_nf") != null && innerObject.get("altura_nf").toString() != "") {
				clsAreaAbandonada.setAltura_nf(Integer.parseInt(innerObject.get("altura_nf").toString()));
			} 
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsAreaAbandonada.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			}
			
			if (innerObject.get("metodo_usado") != null && innerObject.get("metodo_usado").toString() != "") {
				clsAreaAbandonada.setMetodo_usado(innerObject.get("metodo_usado").toString());
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsAreaAbandonada.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaAreaAbandonada.add(clsAreaAbandonada);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCantVehiculos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantVehiculosRestService :	 jsonEntradaListaCantVehiculos" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectTipoManejo;
		JSONParser jsonParser = new JSONParser();
		
		clsAreaAbandonada = new ClsAreaAbandonada();
		clsMunicipalidad = new ClsMunicipalidad();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectTipoManejo = (JSONObject) jsonObjectPrincipal.get("AreaAbandonada");
			
			if (jsonObjectTipoManejo.get("municipalidad_id") != null && jsonObjectTipoManejo.get("municipalidad_id") != "" &&
					jsonObjectTipoManejo.get("municipalidad_id") != " ") {
				clsMunicipalidad.setMunicipalidad_id(Integer.parseInt(jsonObjectTipoManejo.get("municipalidad_id").toString()));
			}
			else
				clsMunicipalidad.setMunicipalidad_id(-1);
			
			clsAreaAbandonada.setMunicipalidad(clsMunicipalidad);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
