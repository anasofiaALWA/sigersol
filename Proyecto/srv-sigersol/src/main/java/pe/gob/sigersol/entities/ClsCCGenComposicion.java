package pe.gob.sigersol.entities;

public class ClsCCGenComposicion {

	private Integer cc_genComposicion;
	private Integer cc_tipoResiduoId;
	private String cc_tipoResiduoDesc;
	private Double porcentaje;
	private Integer sesionId;
	
	public Integer getCc_genComposicion() {
		return cc_genComposicion;
	}
	public void setCc_genComposicion(Integer cc_genComposicion) {
		this.cc_genComposicion = cc_genComposicion;
	}
	public Integer getCc_tipoResiduoId() {
		return cc_tipoResiduoId;
	}
	public void setCc_tipoResiduoId(Integer cc_tipoResiduoId) {
		this.cc_tipoResiduoId = cc_tipoResiduoId;
	}
	public String getCc_tipoResiduoDesc() {
		return cc_tipoResiduoDesc;
	}
	public void setCc_tipoResiduoDesc(String cc_tipoResiduoDesc) {
		this.cc_tipoResiduoDesc = cc_tipoResiduoDesc;
	}
	public Double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public Integer getSesionId() {
		return sesionId;
	}
	public void setSesionId(Integer sesionId) {
		this.sesionId = sesionId;
	}
	
	
}
