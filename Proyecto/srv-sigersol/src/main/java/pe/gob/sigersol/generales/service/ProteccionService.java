package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsProteccion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ProteccionDAO;

public class ProteccionService {
	
	private static Logger log = Logger.getLogger(ProteccionService.class);
	ProteccionDAO proteccionDAO = new ProteccionDAO();
	
	public ClsResultado listarProteccion(ClsProteccion clsProteccion){
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = proteccionDAO.listarProteccion(clsProteccion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsProteccion clsProteccion) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = proteccionDAO.insertar(clsProteccion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsProteccion clsProteccion) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = proteccionDAO.actualizar(clsProteccion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
