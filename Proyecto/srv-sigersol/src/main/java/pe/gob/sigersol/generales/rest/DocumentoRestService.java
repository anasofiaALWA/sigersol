package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsDocumento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.DocumentoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/documentoX")
@RequestScoped
public class DocumentoRestService {

	private static Logger log = Logger.getLogger(DocumentoRestService.class.getName());
	private ClsDocumento clsDocumento;
	private DocumentoService documentoService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		documentoService = new DocumentoService();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener Documento**");		
			jsonEntradaDocumento(paramJson);	
			clsResultado = documentoService.obtener(clsDocumento);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"documento\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		documentoService = new DocumentoService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaDocumento(paramJson);
			clsResultado = documentoService.insertar(clsDocumento);			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		documentoService = new DocumentoService();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaDocumento(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = documentoService.actualizar(clsDocumento);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Valorización  
	 * 
	 * */
	private void jsonEntradaDocumento(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ValorizacionRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectDocumento;
		JSONParser jsonParser = new JSONParser();
		
		clsDocumento = new ClsDocumento();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectDocumento = (JSONObject) jsonObjectPrincipal.get("Documento");
			
			if (jsonObjectDocumento.get("documento_id") != null && jsonObjectDocumento.get("documento_id") != "") {
				clsDocumento.setDocumento_id(Integer.parseInt(jsonObjectDocumento.get("documento_id").toString()));
			}

			if (jsonObjectDocumento.get("uid_documento") != null && jsonObjectDocumento.get("uid_documento") != "") {
				clsDocumento.setUid_documento(jsonObjectDocumento.get("uid_documento").toString());
			}
			else {
				clsDocumento.setUid_documento("");
			}
			
			if (jsonObjectDocumento.get("nombre_archivo") != null && jsonObjectDocumento.get("nombre_archivo") != "") {
				clsDocumento.setNombre_archivo(jsonObjectDocumento.get("nombre_archivo").toString());
			}
			else {
				clsDocumento.setNombre_archivo("");
			}
			
			if (jsonObjectDocumento.get("numero_documento") != null && jsonObjectDocumento.get("numero_documento") != "") {
				clsDocumento.setNumero_documento(jsonObjectDocumento.get("numero_documento").toString());
			}
			else {
				clsDocumento.setNumero_documento("");
			}
			
			if (jsonObjectDocumento.get("tipo_documento") != null && jsonObjectDocumento.get("tipo_documento") != "") {
				clsDocumento.setTipo_documento(jsonObjectDocumento.get("tipo_documento").toString());
			}
			else {
				clsDocumento.setTipo_documento("");
			}
			
			if (jsonObjectDocumento.get("razon_social") != null && jsonObjectDocumento.get("razon_social") != "") {
				clsDocumento.setRazon_social(jsonObjectDocumento.get("razon_social").toString());
			}
			else {
				clsDocumento.setRazon_social("");
			}
			

			if (jsonObjectDocumento.get("nombre_infraestructura") != null && jsonObjectDocumento.get("nombre_infraestructura") != "") {
				clsDocumento.setNombre_infraestructura(jsonObjectDocumento.get("nombre_infraestructura").toString());
			}
			else {
				clsDocumento.setNombre_infraestructura("");
			}
			
			if (jsonObjectDocumento.get("fecha_emision") != null && jsonObjectDocumento.get("fecha_emision") != "") {
				clsDocumento.setFecha_emision(jsonObjectDocumento.get("fecha_emision").toString());
			}
			else{
				clsDocumento.setFecha_emision("");
			}
			
			if (jsonObjectDocumento.get("fecha_caducidad") != null && jsonObjectDocumento.get("fecha_caducidad") != "") {
				clsDocumento.setFecha_caducidad(jsonObjectDocumento.get("fecha_caducidad").toString());
			}
			else{
				clsDocumento.setFecha_caducidad("");
			}
			
			if (jsonObjectDocumento.get("inicio_permiso") != null && jsonObjectDocumento.get("inicio_permiso") != "") {
				clsDocumento.setInicio_permiso(jsonObjectDocumento.get("inicio_permiso").toString());
			}
			else{
				clsDocumento.setInicio_permiso("");
			}
			
			if (jsonObjectDocumento.get("culminacion_permiso") != null && jsonObjectDocumento.get("culminacion_permiso") != "") {
				clsDocumento.setCulminacion_permiso(jsonObjectDocumento.get("culminacion_permiso").toString());
			}
			else{
				clsDocumento.setCulminacion_permiso("");
			}
			
			if (jsonObjectDocumento.get("numero_licencia") != null && jsonObjectDocumento.get("numero_licencia") != "") {
				clsDocumento.setNumero_licencia(jsonObjectDocumento.get("numero_licencia").toString());
			}	
			else{
				clsDocumento.setNumero_licencia("");
			}
			
			if (jsonObjectDocumento.get("numero_expediente") != null && jsonObjectDocumento.get("numero_expediente") != "") {
				clsDocumento.setNumero_expediente(jsonObjectDocumento.get("numero_expediente").toString());
			}
			else{
				clsDocumento.setNumero_expediente("");
			}
			
			if (jsonObjectDocumento.get("cod_usuario") != null && jsonObjectDocumento.get("cod_usuario") != "") {
				clsDocumento.setCodigo_usuario(Integer.parseInt(jsonObjectDocumento.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
