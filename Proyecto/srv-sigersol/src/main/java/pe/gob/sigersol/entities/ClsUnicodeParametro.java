package pe.gob.sigersol.entities;

/**
 * @author
 */
public class ClsUnicodeParametro {

	private Integer unicode_parametro_id;
	private String nombre_tabla;
	private String codigo_parametro;
	private String valor_parametro;
	private Integer codigo_estado;
	private Integer editable;

	private String estado;

	public Integer getUnicode_parametro_id() {
		return unicode_parametro_id;
	}

	public void setUnicode_parametro_id(Integer unicode_parametro_id) {
		this.unicode_parametro_id = unicode_parametro_id;
	}

	public String getNombre_tabla() {
		return nombre_tabla;
	}

	public void setNombre_tabla(String nombre_tabla) {
		this.nombre_tabla = nombre_tabla;
	}

	public String getCodigo_parametro() {
		return codigo_parametro;
	}

	public void setCodigo_parametro(String codigo_parametro) {
		this.codigo_parametro = codigo_parametro;
	}

	public String getValor_parametro() {
		return valor_parametro;
	}

	public void setValor_parametro(String valor_parametro) {
		this.valor_parametro = valor_parametro;
	}

	public Integer getCodigo_estado() {
		return codigo_estado;
	}

	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String toJson() {
		String ljson = "{ }";
		return ljson.replace("null", "");
	}

	public Integer getEditable() {
		return editable;
	}

	public void setEditable(Integer editable) {
		this.editable = editable;
	}

	
	
}
