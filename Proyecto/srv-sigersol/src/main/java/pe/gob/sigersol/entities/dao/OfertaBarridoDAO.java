package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsOfertaBarrido;
import pe.gob.sigersol.entities.ClsProgramaSegregacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoOferta;

public class OfertaBarridoDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(OfertaBarridoDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_oferta = "{call SIGERSOLBL.SGR_SP_LIST_SGR_OFERTA(?,?)}";
	private final String sgr_sp_ins_oferta = "{call SIGERSOLBL.SGR_SP_INS_OFERTA(?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_oferta = "{call SIGERSOLBL.SGR_SP_UPD_OFERTA(?,?,?,?,?,?,?,?,?)}";
		
	public ClsResultado obtener(ClsOfertaBarrido clsOfertaBarrido) {
		ClsResultado clsResultado = new ClsResultado();
		ClsOfertaBarrido item = new ClsOfertaBarrido();
		ClsBarrido barrido = new ClsBarrido();
		con = null;
		String sql = this.sgr_sp_list_sgr_oferta;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOfertaBarrido.getBarrido().getBarrido_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				
				try {
					item.setOferta_id(rs.getInt("OFERTA_ID"));
					barrido.setBarrido_id(rs.getInt("BARRIDO_ID"));
					item.setBarrido(barrido);
					item.setTotal_asfaltada(rs.getInt("TOTAL_ASFALTADA"));
					item.setTotal_no_asfaltada(rs.getInt("TOTAL_NO_ASFALTADA"));
					item.setTotal_plazas(rs.getInt("TOTAL_PLAZAS"));
					item.setTotal_playas(rs.getInt("TOTAL_PLAYAS"));
					item.setTotal_otros(rs.getInt("TOTAL_OTROS"));
					
				} catch (Exception e) {

				}
	
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsOfertaBarrido clsOfertaBarrido) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_oferta;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOfertaBarrido.getBarrido().getBarrido_id());
			cs.setInt(2, clsOfertaBarrido.getTipoOferta().getTipo_oferta_id());
			cs.setInt(3, clsOfertaBarrido.getTotal_asfaltada());
			cs.setInt(4, clsOfertaBarrido.getTotal_no_asfaltada());
			cs.setInt(5, clsOfertaBarrido.getTotal_plazas());
			cs.setInt(6, clsOfertaBarrido.getTotal_playas());
			cs.setInt(7, clsOfertaBarrido.getTotal_otros());
			cs.setInt(8, clsOfertaBarrido.getCodigo_usuario());
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(9);
			
			log.info("OfertaDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsOfertaBarrido clsOfertaBarrido) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_oferta;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOfertaBarrido.getBarrido().getBarrido_id());
			cs.setInt(2, clsOfertaBarrido.getTipoOferta().getTipo_oferta_id());
			cs.setInt(3, clsOfertaBarrido.getTotal_asfaltada());
			cs.setInt(4, clsOfertaBarrido.getTotal_no_asfaltada());
			cs.setInt(5, clsOfertaBarrido.getTotal_plazas());
			cs.setInt(6, clsOfertaBarrido.getTotal_playas());
			cs.setInt(7, clsOfertaBarrido.getTotal_otros());
			cs.setInt(8, clsOfertaBarrido.getCodigo_usuario());
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(9);

			log.info("OfertaDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
