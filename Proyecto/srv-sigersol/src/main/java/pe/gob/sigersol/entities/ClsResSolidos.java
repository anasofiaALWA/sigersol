package pe.gob.sigersol.entities;

import java.util.List;

public class ClsResSolidos {

	private Integer residuos_solidos_id;
	private ClsGeneracion generacion;
	private Integer tipors_id;
	private String nombre;
	private String informacion;
	private Integer padre_id;
	private Integer nivel;
	private Integer tiene_hijos;
	private Double porcentaje;
	private List<ClsResSolidos> hijos;
	private Integer cod_usuario;
	
	public Integer getResiduos_solidos_id() {
		return residuos_solidos_id;
	}
	public void setResiduos_solidos_id(Integer residuos_solidos_id) {
		this.residuos_solidos_id = residuos_solidos_id;
	}
	public ClsGeneracion getGeneracion() {
		return generacion;
	}
	public void setGeneracion(ClsGeneracion generacion) {
		this.generacion = generacion;
	}
	public Integer getTipors_id() {
		return tipors_id;
	}
	public void setTipors_id(Integer tipors_id) {
		this.tipors_id = tipors_id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getInformacion() {
		return informacion;
	}
	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}
	public Integer getPadre_id() {
		return padre_id;
	}
	public void setPadre_id(Integer padre_id) {
		this.padre_id = padre_id;
	}
	public Integer getNivel() {
		return nivel;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	public Integer getTiene_hijos() {
		return tiene_hijos;
	}
	public void setTiene_hijos(Integer tiene_hijos) {
		this.tiene_hijos = tiene_hijos;
	}
	public Double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public List<ClsResSolidos> getHijos() {
		return hijos;
	}
	public void setHijos(List<ClsResSolidos> hijos) {
		this.hijos = hijos;
	}
	public Integer getCod_usuario() {
		return cod_usuario;
	}
	public void setCod_usuario(Integer cod_usuario) {
		this.cod_usuario = cod_usuario;
	}
}
