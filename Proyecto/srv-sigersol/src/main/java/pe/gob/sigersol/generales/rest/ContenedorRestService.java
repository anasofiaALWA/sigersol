package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAlmacenamiento;
import pe.gob.sigersol.entities.ClsContenedor;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoContenedor;
import pe.gob.sigersol.generales.service.ContenedorService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase ContenedorRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de almacenamiento permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/contenedor")
@RequestScoped
public class ContenedorRestService {

	private static Logger log = Logger.getLogger(ContenedorRestService.class.getName());
	private ClsContenedor clsContenedor;
	private ClsTipoContenedor clsTipoContenedor;
	private ClsAlmacenamiento clsAlmacenamiento;
	private ContenedorService contenedorService;
	private List<ClsContenedor> listaContenedor;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarContenedor")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarContenedor(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			contenedorService = new ContenedorService();
			
			jsonEntradaListaContenedor(paramJson);
			
			log.info("**Obtener Generacion No Domiciliar");
			
			clsResultado = contenedorService.listarContenedor(clsContenedor);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstContenedor\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		contenedorService = new ContenedorService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaNoDomiciliar(paramJson);
			
			for (ClsContenedor clsContenedor : listaContenedor) {
				clsResultado = contenedorService.insertar(clsContenedor);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		contenedorService = new ContenedorService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaNoDomiciliar(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsContenedor clsContenedor : listaContenedor) {
			clsResultado = contenedorService.actualizar(clsContenedor);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Contenedor
	 * 
	 * */
	private void jsonEntradaNoDomiciliar(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService : jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectTipoContenedor;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaContenedor = new ArrayList();
		
		//Integer contador = 1;
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsContenedor = new ClsContenedor();
			clsTipoContenedor = new ClsTipoContenedor();
			clsAlmacenamiento = new ClsAlmacenamiento();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("almacenamiento_id") != null && innerObject.get("almacenamiento_id").toString() != "") {
				clsAlmacenamiento.setAlmacenamiento_id(Integer.parseInt(innerObject.get("almacenamiento_id").toString()));
			}
			
			clsContenedor.setAlmacenamiento(clsAlmacenamiento);
			
			if (innerObject.get("tipo_contenedor") != null && innerObject.get("tipo_contenedor").toString() != "") {
				jsonObjectTipoContenedor  = (JSONObject) innerObject.get("tipo_contenedor");
				clsTipoContenedor.setTipo_contenedor_id(Integer.parseInt(jsonObjectTipoContenedor.get("tipo_contenedor_id").toString()));
			}
			clsContenedor.setTipo_contenedor(clsTipoContenedor);
			
			/*if(contador > 3)
				clsContenedor.setTipo("SOT");
			else
				clsContenedor.setTipo("SUP");
			
			if(contador == 1 ||  contador == 4)
				clsContenedor.setEstado("OBE");
			else if(contador == 2 || contador == 5)
				clsContenedor.setEstado("OME");
			else
				clsContenedor.setEstado("NO");*/
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsContenedor.setCantidad(Double.valueOf(innerObject.get("cantidad").toString()));
			} else {
				clsContenedor.setCantidad(0.0);
			}
			
			if (innerObject.get("capacidad") != null && innerObject.get("capacidad").toString() != "") {
				clsContenedor.setCapacidad(Integer.parseInt(innerObject.get("capacidad").toString()));
			} else {
				clsContenedor.setCapacidad(0);
			}
			
			if (innerObject.get("mercados") != null && innerObject.get("mercados").toString() != "") {
				clsContenedor.setMercados(Integer.parseInt(innerObject.get("mercados").toString()));
			} else {
				clsContenedor.setMercados(0);
			}
			
			if (innerObject.get("parques") != null && innerObject.get("parques").toString() != "") {
				clsContenedor.setParques(Integer.parseInt(innerObject.get("parques").toString()));
			} else {
				clsContenedor.setParques(0);
			}
			
			if (innerObject.get("vias") != null && innerObject.get("vias").toString() != "") {
				clsContenedor.setVias(Integer.parseInt(innerObject.get("vias").toString()));
			} else {
				clsContenedor.setVias(0);
			}
			
			if (innerObject.get("otros") != null && innerObject.get("otros").toString() != "") {
				clsContenedor.setOtros(Integer.parseInt(innerObject.get("otros").toString()));
			} else {
				clsContenedor.setOtros(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsContenedor.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			//contador++;
			listaContenedor.add(clsContenedor);	
		}
			
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaContenedor(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ContenedorRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectContenedor;
		JSONParser jsonParser = new JSONParser();
		
		clsContenedor = new ClsContenedor();
		clsTipoContenedor = new ClsTipoContenedor();
		clsAlmacenamiento = new ClsAlmacenamiento();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectContenedor = (JSONObject) jsonObjectPrincipal.get("Contenedor");
			
			if (jsonObjectContenedor.get("almacenamiento_id") != null && jsonObjectContenedor.get("almacenamiento_id") != "" &&
					jsonObjectContenedor.get("almacenamiento_id") != " ") {
				clsAlmacenamiento.setAlmacenamiento_id(Integer.parseInt(jsonObjectContenedor.get("almacenamiento_id").toString()));
			}
			else
				clsAlmacenamiento.setAlmacenamiento_id(-1);
			
			clsContenedor.setAlmacenamiento(clsAlmacenamiento);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
