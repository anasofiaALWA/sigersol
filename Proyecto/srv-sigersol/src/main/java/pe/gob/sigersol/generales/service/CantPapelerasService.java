package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCantPapeleras;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CantPapelerasDAO;

public class CantPapelerasService {

	private static Logger log = Logger.getLogger(CantPapelerasService.class);
	CantPapelerasDAO cantPapelerasDAO = new CantPapelerasDAO();
	
	public ClsResultado listarCantPapeleras(ClsCantPapeleras clsCantPapeleras){
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantPapelerasDAO.listarCantPapeleras(clsCantPapeleras);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCantPapeleras clsCantPapeleras) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantPapelerasDAO.insertar(clsCantPapeleras);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCantPapeleras clsCantPapeleras) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantPapelerasDAO.actualizar(clsCantPapeleras);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
