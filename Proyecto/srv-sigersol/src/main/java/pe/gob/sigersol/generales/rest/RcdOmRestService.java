package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoConstruccion;
import pe.gob.sigersol.generales.service.RcdOmService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/rcdOm")
@RequestScoped
public class RcdOmRestService {

	private static Logger log = Logger.getLogger(SolicitudRestService.class.getName());
	private ClsRcdOm clsRcdOm;
	private ClsTipoConstruccion clsTipoConstruccion;
	private ClsGeneracion clsGeneracion;
	private RcdOmService rcdOmService;
	private List<ClsRcdOm> listaRcdOm;

	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			rcdOmService = new RcdOmService();
			
			jsonEntradaListaRcdOm(paramJson);
			
			log.info("**Obtener RcdOm**");
			
			clsResultado = rcdOmService.obtener(clsRcdOm);
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstRcdOm\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		rcdOmService = new RcdOmService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaRcdOm(paramJson);

			// if (accion == 1) {
			// clsResultado = academicoService.insertar(clsAcademico);
			// } else {
			// clsResultado = academicoService.actualizar(clsAcademico);
			// }
			//
			
			for (ClsRcdOm clsRcdOm : listaRcdOm) {
				
				if(clsRcdOm.getTipo_construccion().getFlag_cantidad() == 1){
					clsResultado = rcdOmService.insertar(clsRcdOm);
				}
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		rcdOmService = new RcdOmService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaRcdOm(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsRcdOm clsRcdOm : listaRcdOm) {
			if(clsRcdOm.getTipo_construccion().getFlag_cantidad() == 1){
				clsResultado = rcdOmService.actualizar(clsRcdOm);
			}
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	
	private void jsonEntradaRcdOm(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> RcdOmRestService : jsonEntradaRcdOm" + jsonEntrada);

		JSONObject jsonObjectTipoEspeciales;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaRcdOm = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsRcdOm = new ClsRcdOm();
			clsTipoConstruccion = new ClsTipoConstruccion();
			clsGeneracion = new ClsGeneracion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
			}
			
			clsRcdOm.setGeneracion(clsGeneracion);
			
			if (innerObject.get("tipo_construccion") != null && innerObject.get("tipo_construccion").toString() != "") {
				jsonObjectTipoEspeciales  = (JSONObject) innerObject.get("tipo_construccion");
				clsTipoConstruccion.setTipo_construccion_id(Integer.parseInt(jsonObjectTipoEspeciales.get("tipo_construccion_id").toString()));
				clsTipoConstruccion.setFlag_cantidad(Integer.parseInt(jsonObjectTipoEspeciales.get("flag_cantidad").toString()));
			}
			clsRcdOm.setTipo_construccion(clsTipoConstruccion);
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsRcdOm.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else {
				clsRcdOm.setCantidad(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsRcdOm.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaRcdOm.add(clsRcdOm);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	private void jsonEntradaListaRcdOm(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> RcdOmRestService : jsonEntradaListaRcdOm" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResiduosEspeciales;
		JSONParser jsonParser = new JSONParser();
		
		clsRcdOm = new ClsRcdOm();
		clsTipoConstruccion = new ClsTipoConstruccion();
		clsGeneracion = new ClsGeneracion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResiduosEspeciales = (JSONObject) jsonObjectPrincipal.get("RcdOm");
			
			if (jsonObjectResiduosEspeciales.get("generacion_id") != null && jsonObjectResiduosEspeciales.get("generacion_id") != "" &&
					jsonObjectResiduosEspeciales.get("generacion_id") != " ") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(jsonObjectResiduosEspeciales.get("generacion_id").toString()));
			}
			else
				clsGeneracion.setGeneracion_id(-1);
			
			clsRcdOm.setGeneracion(clsGeneracion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
