package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResiduosCiclo;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;

public class ResiduosCicloDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(ResiduosCicloDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_residuos_ciclo = "{call SIGERSOLBL.SGR_SP_LIST_RESIDUOS_CICLO(?,?)}";
	private final String sgr_sp_ins_residuos_ciclo = "{call SIGERSOLBL.SGR_SP_INS_RESIDUOS_CICLO(?,?,?,?)}";
	private final String sgr_sp_upd_residuos_ciclo = "{call SIGERSOLBL.SGR_SP_UPD_RESIDUOS_CICLO(?,?,?,?,?)}";
	
	
	public ClsResultado obtener(ClsResiduosCiclo clsResiduosCiclo) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsResiduosCiclo> lista = new ArrayList<ClsResiduosCiclo>();
		con = null;
		String sql = this.sgr_sp_list_residuos_ciclo;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosCiclo.getValorizacion().getValorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsResiduosCiclo item = new ClsResiduosCiclo();
				ClsValorizacion valorizacion = new ClsValorizacion();
				
				try {
					item.setResiduos_ciclo_id(rs.getInt("RESIDUOS_CICLO_ID"));
					valorizacion.setValorizacion_id(rs.getInt("VALORIZACION_ID"));
					item.setValorizacion(valorizacion);
					item.setCantidad(rs.getInt("CANTIDAD"));
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}

	public ClsResultado insertar(ClsResiduosCiclo clsResiduosCiclo) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_residuos_ciclo;
	
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosCiclo.getValorizacion().getValorizacion_id());		
			cs.setInt(2, clsResiduosCiclo.getCantidad());
			cs.setInt(3, clsResiduosCiclo.getCodigo_usuario());
			cs.registerOutParameter(4, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(4);
			
			log.info("ResiduosCicloDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsResiduosCiclo clsResiduosCiclo) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_residuos_ciclo;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosCiclo.getResiduos_ciclo_id());
			cs.setInt(2, clsResiduosCiclo.getValorizacion().getValorizacion_id());		
			cs.setInt(3, clsResiduosCiclo.getCantidad());
			cs.setInt(4, clsResiduosCiclo.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);

			log.info("ResiduosCicloDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
