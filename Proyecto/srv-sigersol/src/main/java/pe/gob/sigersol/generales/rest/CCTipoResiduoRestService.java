package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CCTipoResiduoService;

import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/ccTipoResiduo")
@RequestScoped
public class CCTipoResiduoRestService {
	private static Logger log = Logger.getLogger(CCTipoResiduoRestService.class.getName());
	private CCTipoResiduoService tipoResService;

	@POST
	@Path("/listarDOC")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarDOC(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		tipoResService = new CCTipoResiduoService();
		clsResultado = tipoResService.listarDOC();
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();

	}

	@POST
	@Path("/listarTasaDesc")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarTasaDesc(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		tipoResService = new CCTipoResiduoService();

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		String ubigeoId = "";
		String condicionId = "";

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(paramJson);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Filtro");

			if (jsonObject.get("ubigeoId") != null && jsonObject.get("ubigeoId") != "") {
				ubigeoId = jsonObject.get("ubigeoId").toString();
			}
			if (jsonObject.get("condicionId") != null && jsonObject.get("condicionId") != "") {
				condicionId = jsonObject.get("condicionId").toString();
			}
			
			clsResultado = tipoResService.listarTasaDesc(ubigeoId, condicionId);

		} catch (Exception e) {
			log.info("Error al obtener Filtro >> " + e.getMessage());
		}

		
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();

	}

}
