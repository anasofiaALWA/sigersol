package pe.gob.sigersol.entities;

public class ClsCombustibleVehiculo {

	private Integer combustible_vehiculo_id;
	private ClsRecDisposicionFinal recDisposicionFinal;
	private ClsRecValorizacion recValorizacion;
	private ClsRecRcdOm recRcdOm;
	private ClsTipoCombustible tipo_combustible;
	private Integer consumo;
	private Integer costo;
	private Integer codigo_usuario;
	
	public Integer getCombustible_vehiculo_id() {
		return combustible_vehiculo_id;
	}
	public void setCombustible_vehiculo_id(Integer combustible_vehiculo_id) {
		this.combustible_vehiculo_id = combustible_vehiculo_id;
	}
	public ClsRecDisposicionFinal getRecDisposicionFinal() {
		return recDisposicionFinal;
	}
	public void setRecDisposicionFinal(ClsRecDisposicionFinal recDisposicionFinal) {
		this.recDisposicionFinal = recDisposicionFinal;
	}
	public ClsRecValorizacion getRecValorizacion() {
		return recValorizacion;
	}
	public void setRecValorizacion(ClsRecValorizacion recValorizacion) {
		this.recValorizacion = recValorizacion;
	}
	public ClsRecRcdOm getRecRcdOm() {
		return recRcdOm;
	}
	public void setRecRcdOm(ClsRecRcdOm recRcdOm) {
		this.recRcdOm = recRcdOm;
	}
	public ClsTipoCombustible getTipo_combustible() {
		return tipo_combustible;
	}
	public void setTipo_combustible(ClsTipoCombustible tipo_combustible) {
		this.tipo_combustible = tipo_combustible;
	}
	public Integer getConsumo() {
		return consumo;
	}
	public void setConsumo(Integer consumo) {
		this.consumo = consumo;
	}
	public Integer getCosto() {
		return costo;
	}
	public void setCosto(Integer costo) {
		this.costo = costo;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
