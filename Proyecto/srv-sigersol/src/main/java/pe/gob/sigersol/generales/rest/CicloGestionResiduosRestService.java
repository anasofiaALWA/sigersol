package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CicloGestionResiduosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase CicloGestionResiduosRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de CicloGestionResiduos, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/gestion")
@RequestScoped
public class CicloGestionResiduosRestService {

	private static Logger log = Logger.getLogger(CicloGestionResiduosRestService.class.getName());
	private Integer accion = null;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private CicloGestionResiduosService cicloGestionResiduosService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados
	 * anteriormente
	 * 
	 */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(@Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			log.info("**obtener Ciclo Gestion**");

			clsResultado = cicloGestionResiduosService.obtener();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales(
					"{\"cicloGRS\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite validar que las distintas etapas del ciclo hayan sido
	 * o no ingresadas
	 * 
	 */
	@POST
	@Path("/validar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String validar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		cicloGestionResiduosService = new CicloGestionResiduosService();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			log.info("**obtener Ciclo Gestion**");

			jsonEntradaGestion(paramJson);

			clsResultado = cicloGestionResiduosService.validar(clsCicloGestionResiduos);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales(
					"{\"cicloGRS\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		cicloGestionResiduosService = new CicloGestionResiduosService();

		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaGestion(paramJson);
			clsResultado = cicloGestionResiduosService.insertar(clsCicloGestionResiduos);
			idUsuario = (Integer) clsResultado.getObjeto();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		cicloGestionResiduosService = new CicloGestionResiduosService();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaGestion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = cicloGestionResiduosService.actualizar(clsCicloGestionResiduos);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}

	@POST
	@Path("/getUltAnioCicloGRS")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String getUltAnioCicloGRS(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {

		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		cicloGestionResiduosService = new CicloGestionResiduosService();

		ClsMunicipalidad clsMunicipalidad = jsonEntradaUbigeo(paramJson);

		clsResultado = cicloGestionResiduosService.getUltimoAnioCGR(clsMunicipalidad.getCodigo_ubigeo_distrito());

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad
	 * de CicloGestionResiduos
	 * 
	 */
	private void jsonEntradaGestion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CicloGestionResiduosRestService :	jsonEntradaGestion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectGestion;
		JSONParser jsonParser = new JSONParser();

		clsCicloGestionResiduos = new ClsCicloGestionResiduos();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectGestion = (JSONObject) jsonObjectPrincipal.get("Gestion");

			if (jsonObjectGestion.get("ciclo_grs_id") != null && jsonObjectGestion.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos
						.setCiclo_grs_id(Integer.parseInt(jsonObjectGestion.get("ciclo_grs_id").toString()));
			} else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);

			if (jsonObjectGestion.get("municipalidad_id") != null && jsonObjectGestion.get("municipalidad_id") != "") {
				clsCicloGestionResiduos
						.setMunicipalidad_id(Integer.parseInt(jsonObjectGestion.get("municipalidad_id").toString()));
			}
			if (jsonObjectGestion.get("anio") != null && jsonObjectGestion.get("anio") != "") {
				clsCicloGestionResiduos.setAnio(Integer.parseInt(jsonObjectGestion.get("anio").toString()));
			}
			if (jsonObjectGestion.get("generacion") != null && jsonObjectGestion.get("generacion") != "") {
				clsCicloGestionResiduos.setGeneracion(Integer.parseInt(jsonObjectGestion.get("generacion").toString()));
			} else {
				clsCicloGestionResiduos.setGeneracion(0);
			}
			if (jsonObjectGestion.get("almacenamiento") != null && jsonObjectGestion.get("almacenamiento") != "") {
				clsCicloGestionResiduos
						.setAlmacenamiento(Integer.parseInt(jsonObjectGestion.get("almacenamiento").toString()));
			} else {
				clsCicloGestionResiduos.setAlmacenamiento(0);
			}
			if (jsonObjectGestion.get("barrido") != null && jsonObjectGestion.get("barrido") != "") {
				clsCicloGestionResiduos.setBarrido(Integer.parseInt(jsonObjectGestion.get("barrido").toString()));
			} else {
				clsCicloGestionResiduos.setBarrido(0);
			}
			if (jsonObjectGestion.get("recoleccion") != null && jsonObjectGestion.get("recoleccion") != "") {
				clsCicloGestionResiduos
						.setRecoleccion(Integer.parseInt(jsonObjectGestion.get("recoleccion").toString()));
			} else {
				clsCicloGestionResiduos.setRecoleccion(0);
			}
			if (jsonObjectGestion.get("transferencia") != null && jsonObjectGestion.get("transferencia") != "") {
				clsCicloGestionResiduos
						.setTransferencia(Integer.parseInt(jsonObjectGestion.get("transferencia").toString()));
			} else {
				clsCicloGestionResiduos.setTransferencia(0);
			}
			if (jsonObjectGestion.get("valorizacion") != null && jsonObjectGestion.get("valorizacion") != "") {
				clsCicloGestionResiduos
						.setValorizacion(Integer.parseInt(jsonObjectGestion.get("valorizacion").toString()));
			} else {
				clsCicloGestionResiduos.setValorizacion(0);
			}
			if (jsonObjectGestion.get("disposicion_final") != null
					&& jsonObjectGestion.get("disposicion_final") != "") {
				clsCicloGestionResiduos
						.setDisposicion_final(Integer.parseInt(jsonObjectGestion.get("disposicion_final").toString()));
			} else {
				clsCicloGestionResiduos.setDisposicion_final(0);
			}
			if (jsonObjectGestion.get("reporte_firmado") != null && jsonObjectGestion.get("reporte_firmado") != "") {
				clsCicloGestionResiduos
						.setReporte_firmado(Integer.parseInt(jsonObjectGestion.get("reporte_firmado").toString()));
			} else {
				clsCicloGestionResiduos.setReporte_firmado(0);
			}
			if (jsonObjectGestion.get("flag_recoleccion") != null && jsonObjectGestion.get("flag_recoleccion") != "") {
				clsCicloGestionResiduos
						.setFlag_recoleccion(Integer.parseInt(jsonObjectGestion.get("flag_recoleccion").toString()));
			} else {
				clsCicloGestionResiduos.setFlag_recoleccion(0);
			}
			if (jsonObjectGestion.get("cod_usuario") != null && jsonObjectGestion.get("cod_usuario") != "") {
				clsCicloGestionResiduos
						.setCodigo_usuario(Integer.parseInt(jsonObjectGestion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	private ClsMunicipalidad jsonEntradaUbigeo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CicloGestionResiduosRestService :	jsonEntradaUbigeo" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		ClsMunicipalidad municipalidad = new ClsMunicipalidad();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Ubigeo");
			
			if (jsonObject.get("CodDist") != null && jsonObject.get("CodDist") != "") {
				municipalidad.setCodigo_ubigeo_distrito(jsonObject.get("CodDist").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return municipalidad;
	}

}
