package pe.gob.sigersol.entities;

public class ClsAccionPoi {

	private Integer accion_poi_id;
	private ClsEducacionAmbiental educacion_ambiental;
	private ClsAccionTipo accion_tipo;
	private String tipo_accion;
	private Integer cantidad_accion;
	private Integer presupuesto_asignado;
	private Integer codigo_usuario;
	
	public Integer getAccion_poi_id() {
		return accion_poi_id;
	}
	public void setAccion_poi_id(Integer accion_poi_id) {
		this.accion_poi_id = accion_poi_id;
	}
	public ClsEducacionAmbiental getEducacion_ambiental() {
		return educacion_ambiental;
	}
	public void setEducacion_ambiental(ClsEducacionAmbiental educacion_ambiental) {
		this.educacion_ambiental = educacion_ambiental;
	}
	public ClsAccionTipo getAccion_tipo() {
		return accion_tipo;
	}
	public void setAccion_tipo(ClsAccionTipo accion_tipo) {
		this.accion_tipo = accion_tipo;
	}
	public String getTipo_accion() {
		return tipo_accion;
	}
	public void setTipo_accion(String tipo_accion) {
		this.tipo_accion = tipo_accion;
	}
	public Integer getCantidad_accion() {
		return cantidad_accion;
	}
	public void setCantidad_accion(Integer cantidad_accion) {
		this.cantidad_accion = cantidad_accion;
	}
	public Integer getPresupuesto_asignado() {
		return presupuesto_asignado;
	}
	public void setPresupuesto_asignado(Integer presupuesto_asignado) {
		this.presupuesto_asignado = presupuesto_asignado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
