package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsMes;
import pe.gob.sigersol.entities.ClsMuniDispDeg;
import pe.gob.sigersol.entities.ClsResultado;

public class MuniDispDegDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(MuniDispDegDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_rs_dispuesto2 = "{call SIGERSOLBL.SGR_SP_INS_RS_DISPUESTO2(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_rs_dispuesto2 = "{call SIGERSOLBL.SGR_SP_UPD_RS_DISPUESTO2(?,?,?,?,?,?)}";
	private final String sgr_sp_list_rs_dispuesto2 = "{call SIGERSOLBL.SGR_SP_LIST_RS_DISPUESTO2(?,?)}";  
	
	public ClsResultado listarMuniDisp(ClsMuniDispDeg clsMuniDispDeg){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsMuniDispDeg> lista = new ArrayList<ClsMuniDispDeg>();
		con = null;
		String sql = this.sgr_sp_list_rs_dispuesto2;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsMuniDispDeg.getDisposicionFinal().getDisposicion_final_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsMuniDispDeg item = new ClsMuniDispDeg();
				ClsMes mes = new ClsMes();
				
				try {
					mes.setMes_id(rs.getInt("MES_ID"));
					mes.setNombre_mes(rs.getString("NOMBRE_MES"));
					item.setMes(mes);
					if(clsMuniDispDeg.getDisposicionFinal().getDisposicion_final_id() != -1){
						item.setCantidad(rs.getInt("CANTIDAD"));
						item.setMetodo_usado(rs.getInt("METODO_USADO"));
					}
					else{
						item.setCantidad(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsMuniDispDeg clsMuniDispDeg) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_rs_dispuesto2;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsMuniDispDeg.getDisposicionFinal().getDisposicion_final_id());
			cs.setInt(2, clsMuniDispDeg.getMes().getMes_id());
			cs.setInt(3, clsMuniDispDeg.getCantidad());
			cs.setInt(4, clsMuniDispDeg.getMetodo_usado());
			cs.setInt(5, clsMuniDispDeg.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("MuniDispDfDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsMuniDispDeg clsMuniDispDeg) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_rs_dispuesto2;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsMuniDispDeg.getDisposicionFinal().getDisposicion_final_id());
			cs.setInt(2, clsMuniDispDeg.getMes().getMes_id());
			cs.setInt(3, clsMuniDispDeg.getCantidad());
			cs.setInt(4, clsMuniDispDeg.getMetodo_usado());
			cs.setInt(5, clsMuniDispDeg.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("MuniDispDfDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
