package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsLugarAprovechamiento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.LugarAprovechamiento2Service;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/lugarAprovechamiento2")
@RequestScoped
public class LugarAprovechamiento2RestService {

	private static Logger log = Logger.getLogger(LugarAprovechamiento2RestService.class.getName());
	private ClsLugarAprovechamiento clsLugarAprovechamiento;
	private ClsValorizacion clsValorizacion;
	private LugarAprovechamiento2Service lugarAprovechamiento2Service;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		lugarAprovechamiento2Service = new LugarAprovechamiento2Service();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener Planta de Valorizacion**");		
			jsonEntradaValorizacion(paramJson);	
			clsResultado = lugarAprovechamiento2Service.obtener(clsLugarAprovechamiento);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"planta\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		lugarAprovechamiento2Service = new LugarAprovechamiento2Service();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaValorizacion(paramJson);
			clsResultado = lugarAprovechamiento2Service.insertar(clsLugarAprovechamiento);			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		lugarAprovechamiento2Service = new LugarAprovechamiento2Service();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaValorizacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = lugarAprovechamiento2Service.actualizar(clsLugarAprovechamiento);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Valorización  
	 * 
	 * */
	private void jsonEntradaValorizacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ValorizacionRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectValorizacion;
		JSONParser jsonParser = new JSONParser();
		
		clsLugarAprovechamiento = new ClsLugarAprovechamiento();
		clsValorizacion = new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectValorizacion = (JSONObject) jsonObjectPrincipal.get("LugarAprovechamiento2");
			
			if (jsonObjectValorizacion.get("lugar_aprovechamiento_id") != null && jsonObjectValorizacion.get("lugar_aprovechamiento_id") != "") {
				clsLugarAprovechamiento.setLugar_aprovechamiento_id(Integer.parseInt(jsonObjectValorizacion.get("lugar_aprovechamiento_id").toString()));
			}
			
			if (jsonObjectValorizacion.get("valorizacion_id") != null && jsonObjectValorizacion.get("valorizacion_id") != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectValorizacion.get("valorizacion_id").toString()));
			} 
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsLugarAprovechamiento.setValorizacion(clsValorizacion);
			
			if (jsonObjectValorizacion.get("adm_propia") != null && jsonObjectValorizacion.get("adm_propia") != "") {
				clsLugarAprovechamiento.setAdm_propia(Integer.parseInt(jsonObjectValorizacion.get("adm_propia").toString()));
			}
			
			if (jsonObjectValorizacion.get("tercerizado") != null && jsonObjectValorizacion.get("tercerizado") != "") {
				clsLugarAprovechamiento.setTercerizado(Integer.parseInt(jsonObjectValorizacion.get("tercerizado").toString()));
			}
			
			if (jsonObjectValorizacion.get("mixto") != null && jsonObjectValorizacion.get("mixto") != "") {
				clsLugarAprovechamiento.setMixto(Integer.parseInt(jsonObjectValorizacion.get("mixto").toString()));
			}

			if (jsonObjectValorizacion.get("zona_coordenada") != null && jsonObjectValorizacion.get("zona_coordenada") != "") {
				clsLugarAprovechamiento.setZona_coordenada(jsonObjectValorizacion.get("zona_coordenada").toString());
			}
			else {
				clsLugarAprovechamiento.setZona_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("norte_coordenada") != null && jsonObjectValorizacion.get("norte_coordenada") != "") {
				clsLugarAprovechamiento.setNorte_coordenada(jsonObjectValorizacion.get("norte_coordenada").toString());
			}
			else{
				clsLugarAprovechamiento.setNorte_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("sur_coordenada") != null && jsonObjectValorizacion.get("sur_coordenada") != "") {
				clsLugarAprovechamiento.setSur_coordenada(jsonObjectValorizacion.get("sur_coordenada").toString());
			}
			else{
				clsLugarAprovechamiento.setSur_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("div_politica_dep") != null && jsonObjectValorizacion.get("div_politica_dep") != "") {
				clsLugarAprovechamiento.setDiv_politica_dep(jsonObjectValorizacion.get("div_politica_dep").toString());
			}
			else{
				clsLugarAprovechamiento.setDiv_politica_dep("");
			}
			
			if (jsonObjectValorizacion.get("div_politica_prov") != null && jsonObjectValorizacion.get("div_politica_prov") != "") {
				clsLugarAprovechamiento.setDiv_politica_prov(jsonObjectValorizacion.get("div_politica_prov").toString());
			}
			else{
				clsLugarAprovechamiento.setDiv_politica_prov("");
			}
			
			if (jsonObjectValorizacion.get("div_politica_dist") != null && jsonObjectValorizacion.get("div_politica_dist") != "") {
				clsLugarAprovechamiento.setDiv_politica_dist(jsonObjectValorizacion.get("div_politica_dist").toString());
			}
			else{
				clsLugarAprovechamiento.setDiv_politica_dist("");
			}
			
			if (jsonObjectValorizacion.get("direccion_iga") != null && jsonObjectValorizacion.get("direccion_iga") != "") {
				clsLugarAprovechamiento.setDireccion_iga(jsonObjectValorizacion.get("direccion_iga").toString());
			}
			else{
				clsLugarAprovechamiento.setDireccion_iga("");
			}
			
			if (jsonObjectValorizacion.get("referencia_iga") != null && jsonObjectValorizacion.get("referencia_iga") != "") {
				clsLugarAprovechamiento.setReferencia_iga(jsonObjectValorizacion.get("referencia_iga").toString());
			}	
			else{
				clsLugarAprovechamiento.setReferencia_iga("");
			}
			
			if (jsonObjectValorizacion.get("cod_usuario") != null && jsonObjectValorizacion.get("cod_usuario") != "") {
				clsLugarAprovechamiento.setCodigo_usuario(Integer.parseInt(jsonObjectValorizacion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
