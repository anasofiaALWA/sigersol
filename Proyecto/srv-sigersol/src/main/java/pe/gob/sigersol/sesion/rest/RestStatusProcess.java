/**
 * 
 */
package pe.gob.sigersol.sesion.rest;

/**
 * @author ALWA
 *
 */
public class RestStatusProcess {

	private boolean exito = false;
	private String mensajeUsuario = "Ingreso correctamente";
	private String datosAdicionales = "{}";
	private String datosUsuario = "";
	private String perfil;

	public boolean isExito() {
		return exito;
	}

	public void setExito(boolean exito) {
		this.exito = exito;
	}

	public String getMensajeUsuario() {
		return mensajeUsuario;
	}

	public void setMensajeUsuario(String mensajeUsuario) {
		this.mensajeUsuario = mensajeUsuario;
	}

	public String getDatosAdicionales() {
		return datosAdicionales;
	}

	public void setDatosAdicionales(String datosAdicionales) {
		this.datosAdicionales = datosAdicionales;
	}

	public String getDatosUsuario() {
		return datosUsuario;
	}

	public void setDatosUsuario(String datosUsuario) {
		this.datosUsuario = datosUsuario;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String toJSon() {
		return "{\"exito\":" + exito + ",\"datosUsuario\":\"" + datosUsuario + "\",\"perfil\":\"" + perfil
				+ "\", \"mensajeUsuario\":\"" + mensajeUsuario + "\", \"datosAdicionales\":" + datosAdicionales + "}";
	}

}
