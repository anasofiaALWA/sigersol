package pe.gob.sigersol.entities;

public class ClsPersona {

	private Integer persona_id;
	private Integer dni;
	private String nom_apellidos;
	private String sexo;
	private String rango_edad;
	private Integer codigo_usuario;
	
	public Integer getPersona_id() {
		return persona_id;
	}
	public void setPersona_id(Integer persona_id) {
		this.persona_id = persona_id;
	}
	public Integer getDni() {
		return dni;
	}
	public void setDni(Integer dni) {
		this.dni = dni;
	}
	public String getNom_apellidos() {
		return nom_apellidos;
	}
	public void setNom_apellidos(String nom_apellidos) {
		this.nom_apellidos = nom_apellidos;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getRango_edad() {
		return rango_edad;
	}
	public void setRango_edad(String rango_edad) {
		this.rango_edad = rango_edad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
