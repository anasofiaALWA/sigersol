package pe.gob.sigersol.entities;

public class ClsTipoEventos {
	
	private Integer tipo_eventos_id;
	private String nombre_eventos;
	
	public Integer getTipo_eventos_id() {
		return tipo_eventos_id;
	}
	public void setTipo_eventos_id(Integer tipo_eventos_id) {
		this.tipo_eventos_id = tipo_eventos_id;
	}
	public String getNombre_eventos() {
		return nombre_eventos;
	}
	public void setNombre_eventos(String nombre_eventos) {
		this.nombre_eventos = nombre_eventos;
	}
}
