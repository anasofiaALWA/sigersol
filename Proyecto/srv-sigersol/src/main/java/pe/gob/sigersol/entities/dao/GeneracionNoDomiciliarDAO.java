package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsGeneracionNoDomiciliar;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoGeneracion;

public class GeneracionNoDomiciliarDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(GeneracionNoDomiciliarDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_no_domiciliar = "{call SIGERSOLBL.SGR_SP_LIST_SGR_NO_DOMICILIAR(?)}";
	private final String sgr_sp_ins_no_domiciliar = "{call SIGERSOLBL.SGR_SP_INS_NO_DOMICILIAR(?,?,?,?,?)}";
	private final String sgr_sp_upd_no_domiciliar = "{call SIGERSOLBL.SGR_SP_UPD_NO_DOMICILIAR(?,?,?,?,?)}";
	
	private final String sgr_sp_list_gnd = "{call SIGERSOLBL.SGR_SP_LIST_GND(?,?)}";  
	
	public ClsResultado obtener() {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsGeneracionNoDomiciliar> lista = new ArrayList<ClsGeneracionNoDomiciliar>();
		con = null;
		String sql = this.sgr_sp_list_sgr_no_domiciliar;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);

			while (rs.next()) {
				ClsGeneracionNoDomiciliar item = new ClsGeneracionNoDomiciliar();
				try {
					item.setGnd_id(rs.getInt("GENERACION_NO_DOMICILIAR_ID"));
					item.setCantidad(rs.getInt("CANTIDAD_T_X_DIA"));
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado listarGND(ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsGeneracionNoDomiciliar> lista = new ArrayList<ClsGeneracionNoDomiciliar>();
		con = null;
		String sql = this.sgr_sp_list_gnd;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsGeneracionNoDomiciliar.getGeneracion().getGeneracion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsGeneracionNoDomiciliar item = new ClsGeneracionNoDomiciliar();
				ClsTipoGeneracion tipoGeneracion = new ClsTipoGeneracion();
				
				try {
					tipoGeneracion.setTipo_generacion_id(rs.getInt("TIPO_GENERACION_ID"));
					tipoGeneracion.setNombre(rs.getString("NOMBRE_TIPO"));
					tipoGeneracion.setInformacion(rs.getString("INFORMACION"));
					item.setTipo_generacion(tipoGeneracion);
					if(clsGeneracionNoDomiciliar.getGeneracion().getGeneracion_id() != -1)
						item.setCantidad(rs.getInt("CANTIDAD_T_X_DIA"));
					else
						item.setCantidad(null);
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_no_domiciliar;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsGeneracionNoDomiciliar.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsGeneracionNoDomiciliar.getTipo_generacion().getTipo_generacion_id());
			cs.setInt(3, clsGeneracionNoDomiciliar.getCantidad());
			cs.setInt(4, clsGeneracionNoDomiciliar.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);
			
			log.info("GeneracionNoDomiciliarDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_no_domiciliar;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1,clsGeneracionNoDomiciliar.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsGeneracionNoDomiciliar.getTipo_generacion().getTipo_generacion_id());
			cs.setInt(3, clsGeneracionNoDomiciliar.getCantidad());
			cs.setInt(4, clsGeneracionNoDomiciliar.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);

			log.info("GeneracionNoDomiciliarDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
