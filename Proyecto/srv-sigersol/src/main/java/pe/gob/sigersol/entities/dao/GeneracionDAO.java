package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResultado;

public class GeneracionDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(GeneracionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_generacion = "{call SIGERSOLBL.SGR_SP_LIST_SGR_GENERACION(?,?)}";
	private final String sgr_sp_ins_generacion = "{call SIGERSOLBL.SGR_SP_INS_GENERACION(?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_generacion = "{call SIGERSOLBL.SGR_SP_UPD_GENERACION(?,?,?,?,?,?,?,?,?,?)}";
	
	private final String sgr_sp_ins_generacion2 = "{call SIGERSOLBL.SGR_SP_INS_GENERACION2(?,?,?,?)}";
	
	public ClsResultado obtener(ClsGeneracion clsGeneracion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsGeneracion item = new ClsGeneracion();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_sgr_generacion;
			
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsGeneracion.getCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setGeneracion_id(rs.getInt("GENERACION_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(cicloGestionResiduos);
					item.setFlag_generacion(rs.getInt("FLAG_GENERACION"));
					item.setDensidad(rs.getInt("DENSIDAD_T_X_M3"));
					item.setGpc_diario(rs.getDouble("GPC_DIARIO_KG_X_HAB"));
					//item.setDescripcion(rs.getString("DESCRIPCION_OTRO"));
					//item.setPorcentaje(rs.getDouble("PORCENTAJE"));
					//item.setTotal_no_domiciliar(rs.getInt("TOTAL_NO_DOMICILIAR"));
					//item.setTotal_composicion(rs.getInt("TOTAL_COMPOSICION"));
					//item.setTotal_residuos_especiales(rs.getInt("TOTAL_RES_ESPECIALES"));
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsGeneracion clsGeneracion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_generacion;
		String sql2 = this.sgr_sp_ins_generacion2;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			if(clsGeneracion.getFlag_generacion() ==1){
				cs = con.prepareCall(sql);
				cs.setInt(1, clsGeneracion.getCicloGestionResiduos().getCiclo_grs_id());		
				cs.setInt(2, clsGeneracion.getFlag_generacion());
				cs.setInt(3, clsGeneracion.getDensidad());
				cs.setDouble(4, clsGeneracion.getGpc_diario());
				cs.setDouble(5, clsGeneracion.getGeneracion_total_rsd());
				cs.setDouble(6, clsGeneracion.getGeneracion_total_rsnd());
				cs.setDouble(7, clsGeneracion.getGeneracion_total_rcd_om());
				cs.setInt(8, clsGeneracion.getCodigo_usuario());
				cs.registerOutParameter(9, OracleTypes.INTEGER);
				cs.executeUpdate();
				resultado = cs.getInt(9);
			} else{
				cs = con.prepareCall(sql2);
				cs.setInt(1, clsGeneracion.getCicloGestionResiduos().getCiclo_grs_id());		
				cs.setInt(2, clsGeneracion.getFlag_generacion());
				cs.setInt(3, clsGeneracion.getCodigo_usuario());
				cs.registerOutParameter(4, OracleTypes.INTEGER);
				cs.executeUpdate();
				resultado = cs.getInt(4);
			}
			
			log.info("GeneracionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsGeneracion clsGeneracion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_generacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsGeneracion.getGeneracion_id());
			cs.setInt(2, clsGeneracion.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(3, clsGeneracion.getFlag_generacion());
			cs.setInt(4, clsGeneracion.getDensidad());
			cs.setDouble(5, clsGeneracion.getGpc_diario());
			cs.setDouble(6, clsGeneracion.getGeneracion_total_rsd());
			cs.setDouble(7, clsGeneracion.getGeneracion_total_rsnd());
			cs.setDouble(8, clsGeneracion.getGeneracion_total_rcd_om());
			cs.setInt(9, clsGeneracion.getCodigo_usuario());
			cs.registerOutParameter(10, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(10);

			log.info("GeneracionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
