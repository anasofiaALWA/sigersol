package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAccionesEducacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AccionesEducacionDAO;

public class AccionesEducacionService {

	private static Logger log = Logger.getLogger(AccionesEducacionService.class);
	AccionesEducacionDAO accionesEducacionDAO = new AccionesEducacionDAO();
	
	public ClsResultado listarAccionesEducacion(ClsAccionesEducacion clsAccionesEducacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionesEducacionDAO.listarAccionesEducacion(clsAccionesEducacion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAccionesEducacion clsAccionesEducacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionesEducacionDAO.insertar(clsAccionesEducacion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsAccionesEducacion clsAccionesEducacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionesEducacionDAO.actualizar(clsAccionesEducacion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
