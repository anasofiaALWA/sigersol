package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;
import pe.gob.sigersol.generales.service.SitioDisposicionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/sitioDisposicion")
@RequestScoped
public class SitioDisposicionRestService {

	private static Logger log = Logger.getLogger(SitioDisposicionRestService.class.getName());
	private ClsSitioDisposicion clsSitioDisposicion;
	private SitioDisposicionService sitioDisposicionService;
	private List<ClsSitioDisposicion> listaSitioDisposicion;

	@POST
	@Path("/listarCoordenada")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCoordenada(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			sitioDisposicionService = new SitioDisposicionService();
			
			jsonEntradaListaCoordenada(paramJson);
			
			log.info("**Obtener Coordenadas");
			
			clsResultado = sitioDisposicionService.listarCoordenada(clsSitioDisposicion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"coordenada\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/listarCoordenada2")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCoordenada2(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			sitioDisposicionService = new SitioDisposicionService();
			
			jsonEntradaListaCoordenada2(paramJson);
			
			log.info("**Obtener Coordenadas");
			
			clsResultado = sitioDisposicionService.listarCoordenada2(clsSitioDisposicion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"coordenada2\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			sitioDisposicionService = new SitioDisposicionService();
			
			jsonEntradaListaOferta(paramJson);
			
			log.info("**Obtener Generacion No Domiciliar");
			
			clsResultado = sitioDisposicionService.obtener(clsSitioDisposicion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstRelleno\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/obtenerAdm")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtenerAdm(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			sitioDisposicionService = new SitioDisposicionService();
			
			jsonEntradaListaOferta(paramJson);
			
			log.info("**Obtener Lista Relleno Sanitario");
			
			clsResultado = sitioDisposicionService.obtenerAdm(clsSitioDisposicion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstRellenoAdm\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listar(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			sitioDisposicionService = new SitioDisposicionService();
			
			jsonEntradaListaDegrada(paramJson);
			
			log.info("**Obtener Generacion No Domiciliar");
			
			clsResultado = sitioDisposicionService.listar(clsSitioDisposicion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstDegradada\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/listarAdm")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarAdm(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			sitioDisposicionService = new SitioDisposicionService();
			
			jsonEntradaListaDegrada(paramJson);
			
			log.info("**Obtener Generacion No Domiciliar");
			
			clsResultado = sitioDisposicionService.listar(clsSitioDisposicion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstDegradadaAdm\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	private void jsonEntradaListaCoordenada(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectOferta;
		JSONParser jsonParser = new JSONParser();
		
		clsSitioDisposicion = new ClsSitioDisposicion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectOferta = (JSONObject) jsonObjectPrincipal.get("Coordenadas");
			
			if (jsonObjectOferta.get("sitio_disposicion_id") != null && jsonObjectOferta.get("sitio_disposicion_id") != "" &&
					jsonObjectOferta.get("sitio_disposicion_id") != " ") {
				clsSitioDisposicion.setSitio_disposicion_id(Integer.parseInt(jsonObjectOferta.get("sitio_disposicion_id").toString()));
			} else {
				clsSitioDisposicion.setSitio_disposicion_id(-1);
			}
				
						
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
	
	private void jsonEntradaListaCoordenada2(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectOferta;
		JSONParser jsonParser = new JSONParser();
		
		clsSitioDisposicion = new ClsSitioDisposicion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectOferta = (JSONObject) jsonObjectPrincipal.get("Coordenadas2");
			
			if (jsonObjectOferta.get("sitio_disposicion_id") != null && jsonObjectOferta.get("sitio_disposicion_id") != "" &&
					jsonObjectOferta.get("sitio_disposicion_id") != " ") {
				clsSitioDisposicion.setSitio_disposicion_id(Integer.parseInt(jsonObjectOferta.get("sitio_disposicion_id").toString()));
			} else {
				clsSitioDisposicion.setSitio_disposicion_id(-1);
			}
				
						
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaOferta(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectOferta;
		JSONParser jsonParser = new JSONParser();
		
		clsSitioDisposicion = new ClsSitioDisposicion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectOferta = (JSONObject) jsonObjectPrincipal.get("Relleno");
			
			if (jsonObjectOferta.get("ubigeo_id") != null && jsonObjectOferta.get("ubigeo_id") != "" &&
					jsonObjectOferta.get("ubigeo_id") != " ") {
				clsSitioDisposicion.setUbigeo_id(jsonObjectOferta.get("ubigeo_id").toString());
			}
						
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
	
	private void jsonEntradaListaDegrada(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectOferta;
		JSONParser jsonParser = new JSONParser();
		
		clsSitioDisposicion = new ClsSitioDisposicion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectOferta = (JSONObject) jsonObjectPrincipal.get("Degradada");
			
			if (jsonObjectOferta.get("ubigeo_id") != null && jsonObjectOferta.get("ubigeo_id") != "" &&
					jsonObjectOferta.get("ubigeo_id") != " ") {
				clsSitioDisposicion.setUbigeo_id(jsonObjectOferta.get("ubigeo_id").toString());
			}
			
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
