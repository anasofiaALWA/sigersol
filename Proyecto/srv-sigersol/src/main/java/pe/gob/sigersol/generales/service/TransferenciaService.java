package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTransferencia;
import pe.gob.sigersol.entities.dao.TransferenciaDAO;

public class TransferenciaService {

	private static Logger log = Logger.getLogger(TransferenciaService.class);
	TransferenciaDAO transferenciaDAO = new TransferenciaDAO();
	
	public ClsResultado obtener(ClsTransferencia clsTransferencia){
		ClsResultado clsResultado = null;
		try {
			clsResultado = transferenciaDAO.obtener(clsTransferencia);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTransferencia clsTransferencia) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = transferenciaDAO.insertar(clsTransferencia);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsTransferencia clsTransferencia) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = transferenciaDAO.actualizar(clsTransferencia);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
