package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCombustibleVehiculo;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CombustibleVehiculoDAO;


public class CombustibleVehiculoService {

	private static Logger log = Logger.getLogger(CombustibleVehiculoService.class);
	CombustibleVehiculoDAO combustibleVehiculoDAO = new CombustibleVehiculoDAO();
	
	public ClsResultado listarVehiculoRecoleccion(ClsCombustibleVehiculo clsCombustibleVehiculo){
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculoDAO.listarCombustibleVehiculo(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCombustibleVehiculo clsCombustibleVehiculo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculoDAO.insertar(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCombustibleVehiculo clsCombustibleVehiculo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculoDAO.actualizar(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
