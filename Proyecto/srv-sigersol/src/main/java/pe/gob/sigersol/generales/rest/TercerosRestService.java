package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTerceros;
import pe.gob.sigersol.entities.ClsTransferencia;
import pe.gob.sigersol.generales.service.TercerosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/terceros")
@RequestScoped
public class TercerosRestService {
	
	private static Logger log = Logger.getLogger(PropiaRestService.class.getName());
	private ClsTerceros clsTerceros;
	private ClsMunicipalidad clsMunicipalidad;
	private TercerosService tercerosService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			tercerosService = new TercerosService();
			
			jsonEntradaTransferencia(paramJson);
			
			log.info("**obtener terceros**");
			
			clsResultado = tercerosService.obtener(clsTerceros);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"terceros\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		tercerosService = new TercerosService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaTransferencia(paramJson);
			clsResultado = tercerosService.insertar(clsTerceros);	
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		tercerosService = new TercerosService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaTransferencia(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = tercerosService.actualizar(clsTerceros);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Transferencia  
	 * 
	 * */
	private void jsonEntradaTransferencia(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> TransferenciaRestService :jsonEntradaTransferencia" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectTerceros;
		JSONParser jsonParser = new JSONParser();
		
		clsTerceros = new ClsTerceros();
		clsMunicipalidad = new ClsMunicipalidad();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectTerceros = (JSONObject) jsonObjectPrincipal.get("Terceros");
			
			if (jsonObjectTerceros.get("terceros_id") != null && jsonObjectTerceros.get("terceros_id") != "") {
				clsTerceros.setTerceros_id(Integer.parseInt(jsonObjectTerceros.get("terceros_id").toString()));
			}
			
			/*if (jsonObjectTerceros.get("transferencia_id") != null && jsonObjectTerceros.get("transferencia_id") != "") {
				clsTransferencia.setTransferencia_id(Integer.parseInt(jsonObjectTerceros.get("transferencia_id").toString()));
			}
			else
				clsTransferencia.setTransferencia_id(-1);
			
			clsTerceros.setTransferencia(clsTransferencia);*/
			
			if (jsonObjectTerceros.get("municipalidad_id") != null && jsonObjectTerceros.get("municipalidad_id") != "") {
				clsMunicipalidad.setMunicipalidad_id(Integer.parseInt(jsonObjectTerceros.get("municipalidad_id").toString()));
			}
			else
				clsMunicipalidad.setMunicipalidad_id(-1);
			
			clsTerceros.setMunicipalidad(clsMunicipalidad);
			
			if (jsonObjectTerceros.get("num_registro") != null && jsonObjectTerceros.get("num_registro") != "") {
				clsTerceros.setNum_registro(jsonObjectTerceros.get("num_registro").toString());
			}
			else{
				clsTerceros.setNum_registro("");
			}
			
			if (jsonObjectTerceros.get("fecha_inicio_vigencia") != null && jsonObjectTerceros.get("fecha_inicio_vigencia") != "") {
				clsTerceros.setFecha_inicio_vigencia(jsonObjectTerceros.get("fecha_inicio_vigencia").toString());
			}
			else{
				clsTerceros.setFecha_inicio_vigencia("");
			}
			
			if (jsonObjectTerceros.get("fecha_fin_vigencia") != null && jsonObjectTerceros.get("fecha_fin_vigencia") != "") {
				clsTerceros.setFecha_fin_vigencia(jsonObjectTerceros.get("fecha_fin_vigencia").toString());
			}
			else{
				clsTerceros.setFecha_fin_vigencia("");
			}
			
			if (jsonObjectTerceros.get("razon_social") != null && jsonObjectTerceros.get("razon_social") != "") {
				clsTerceros.setRazon_social(jsonObjectTerceros.get("razon_social").toString());
			}
			else{
				clsTerceros.setRazon_social("");
			}
			
			if (jsonObjectTerceros.get("direccion") != null && jsonObjectTerceros.get("direccion") != "") {
				clsTerceros.setDireccion(jsonObjectTerceros.get("direccion").toString());
			}
			else{
				clsTerceros.setDireccion("");
			}
			
			if (jsonObjectTerceros.get("nombre_archivo") != null && jsonObjectTerceros.get("nombre_archivo") != "") {
				clsTerceros.setNombre_archivo(jsonObjectTerceros.get("nombre_archivo").toString());
			}
			else{
				clsTerceros.setNombre_archivo("");
			}
			
			if (jsonObjectTerceros.get("nombre_archivo_ref") != null && jsonObjectTerceros.get("nombre_archivo_ref") != "") {
				clsTerceros.setNombre_archivo_ref(jsonObjectTerceros.get("nombre_archivo_ref").toString());
			}
			else{
				clsTerceros.setNombre_archivo_ref("");
			}
			
			if (jsonObjectTerceros.get("uid_alfresco") != null && jsonObjectTerceros.get("uid_alfresco") != "") {
				clsTerceros.setUid_alfresco(jsonObjectTerceros.get("uid_alfresco").toString());
			}
			else{
				clsTerceros.setUid_alfresco("");
			}
			
			if (jsonObjectTerceros.get("cod_usuario") != null && jsonObjectTerceros.get("cod_usuario") != "") {
				clsTerceros.setCodigo_usuario(Integer.parseInt(jsonObjectTerceros.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
