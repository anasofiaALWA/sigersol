package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsGeneracionNoDomiciliar;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoGeneracion;
import pe.gob.sigersol.generales.service.GeneracionNoDomiciliarService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase GeneracionNoDomiciliarRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de generacion permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/generacionNoDomiciliar")
@RequestScoped
public class GeneracionNoDomiciliarRestService {
	
	private static Logger log = Logger.getLogger(SolicitudRestService.class.getName());
	private ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar;
	private ClsTipoGeneracion clsTipoGeneracion;
	private ClsGeneracion clsGeneracion;
	private GeneracionNoDomiciliarService generacionNoDomiciliarService;
	private List<ClsGeneracionNoDomiciliar> listaGeneracionND;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(@Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener Generacion No Domiciliar**");
			
			clsResultado = generacionNoDomiciliarService.obtener();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"noDomiciliar\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarGND")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarGND(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			generacionNoDomiciliarService = new GeneracionNoDomiciliarService();
			
			jsonEntradaListaNoDomiciliar(paramJson);
			
			log.info("**Obtener Generacion No Domiciliar");
			
			clsResultado = generacionNoDomiciliarService.listarGND(clsGeneracionNoDomiciliar);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstGND\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		generacionNoDomiciliarService = new GeneracionNoDomiciliarService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			jsonEntradaNoDomiciliar(paramJson);
			
			for (ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar : listaGeneracionND) {
				clsResultado = generacionNoDomiciliarService.insertar(clsGeneracionNoDomiciliar);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		generacionNoDomiciliarService = new GeneracionNoDomiciliarService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaNoDomiciliar(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar : listaGeneracionND) {
			clsResultado = generacionNoDomiciliarService.actualizar(clsGeneracionNoDomiciliar);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de GeneracionNoDomiciliar
	 * 
	 * */
	private void jsonEntradaNoDomiciliar(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService :	 jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectTipoGeneracion;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaGeneracionND = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsGeneracionNoDomiciliar = new ClsGeneracionNoDomiciliar();
			clsTipoGeneracion = new ClsTipoGeneracion();
			clsGeneracion = new ClsGeneracion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
			}
			
			clsGeneracionNoDomiciliar.setGeneracion(clsGeneracion);
			
			if (innerObject.get("tipo_generacion") != null && innerObject.get("tipo_generacion").toString() != "") {
				jsonObjectTipoGeneracion  = (JSONObject) innerObject.get("tipo_generacion");
				clsTipoGeneracion.setTipo_generacion_id(Integer.parseInt(jsonObjectTipoGeneracion.get("tipo_generacion_id").toString()));
			}
			clsGeneracionNoDomiciliar.setTipo_generacion(clsTipoGeneracion);
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsGeneracionNoDomiciliar.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else {
				clsGeneracionNoDomiciliar.setCantidad(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsGeneracionNoDomiciliar.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaGeneracionND.add(clsGeneracionNoDomiciliar);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaNoDomiciliar(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectGeneracionNoDomiciliar;
		JSONParser jsonParser = new JSONParser();
		
		clsGeneracionNoDomiciliar = new ClsGeneracionNoDomiciliar();
		clsTipoGeneracion = new ClsTipoGeneracion();
		clsGeneracion = new ClsGeneracion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectGeneracionNoDomiciliar = (JSONObject) jsonObjectPrincipal.get("GeneracionNoDomiciliar");
			
			if (jsonObjectGeneracionNoDomiciliar.get("generacion_id") != null && jsonObjectGeneracionNoDomiciliar.get("generacion_id") != "" &&
					jsonObjectGeneracionNoDomiciliar.get("generacion_id") != " ") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(jsonObjectGeneracionNoDomiciliar.get("generacion_id").toString()));
			}
			else
				clsGeneracion.setGeneracion_id(-1);
			
			clsGeneracionNoDomiciliar.setGeneracion(clsGeneracion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
	
}
