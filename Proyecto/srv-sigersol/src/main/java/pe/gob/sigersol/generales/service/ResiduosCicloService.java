package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResiduosCiclo;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ResiduosCicloDAO;

public class ResiduosCicloService {

	private static Logger log = Logger.getLogger(ResiduosCicloService.class);
	ResiduosCicloDAO residuosCicloDAO = new ResiduosCicloDAO();
	
	public ClsResultado obtener(ClsResiduosCiclo clsResiduosCiclo){
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosCicloDAO.obtener(clsResiduosCiclo);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsResiduosCiclo clsResiduosCiclo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosCicloDAO.insertar(clsResiduosCiclo);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsResiduosCiclo clsResiduosCiclo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosCicloDAO.actualizar(clsResiduosCiclo);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
