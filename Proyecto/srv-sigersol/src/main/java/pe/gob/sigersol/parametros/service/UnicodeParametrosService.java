package pe.gob.sigersol.parametros.service;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.jfree.util.Log;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUnicodeParametro;
import pe.gob.sigersol.entities.dao.UnicodeParametroDAO;

public class UnicodeParametrosService {
	private static Logger log = Logger.getLogger(UnicodeParametrosService.class);
	UnicodeParametroDAO unicodeparametroDAO = new UnicodeParametroDAO();

	/**
	 * @Método : obtener_lista_parametros
	 * @Descripción : obtiene una lista de registros de la tabla
	 *              unicode_parametros.
	 * @Autor - Fecha Creación :MCEA - 24/02/2016
	 * @Autor - Fecha Modificacion :
	 * @Comentario de la modificación:
	 */
	public List<ClsUnicodeParametro> obtener_lista_parametros(String nombre_tabla) {
		List<ClsUnicodeParametro> lista = new ArrayList<ClsUnicodeParametro>();
		try {
			lista = unicodeparametroDAO.obtener_lista_parametros(nombre_tabla);
		} catch (Exception e) {
			log.info("message obtener_lista_parametros >> service >> " + e.getMessage());
		}
		return lista;

	}

	/**
	 * @Método : obtener_parametro
	 * @Descripción : obtiene registros de la tabla unicode_parametros.
	 * @Autor - Fecha Creación :MCEA - 16/03/2016
	 * @Autor - Fecha Modificacion :
	 * @Comentario de la modificación:
	 */
	public String obtener_parametros(String nombre_tabla, String codigo_parametro) {
		String valorParametro = "";

		try {
			UnicodeParametroDAO unicodeParamDAO = new UnicodeParametroDAO();
			List<ClsUnicodeParametro> lista = new ArrayList<ClsUnicodeParametro>();

			lista = unicodeParamDAO.obtener_lista_parametros(nombre_tabla);
			for (ClsUnicodeParametro item : lista) {
				if (item.getCodigo_parametro().compareTo(codigo_parametro) == 0) {
					valorParametro = item.getValor_parametro();
					break;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return valorParametro;
	}

	public ClsResultado validar_periodo(String periodo_id) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = unicodeparametroDAO.validar_periodo(periodo_id);
		} catch (Exception e) {
			log.info("message validar_periodo >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	public List<ClsUnicodeParametro> obtenerProcedenciaByPerfil(String perfil) {

		List<ClsUnicodeParametro> lista = null;

		try {

			lista = new ArrayList<ClsUnicodeParametro>();

			lista = unicodeparametroDAO.obtenerProcedenciaByPerfil(perfil);

		} catch (Exception e) {
			Log.info("message obtenerProcedenciaByPerfil" + e.getMessage());
		}

		return lista;

	}
	
}
