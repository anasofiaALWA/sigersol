package pe.gob.sigersol.entities;

public class ClsResiduosMunicipales {

	private Integer residuos_municipales_id;
	private String nombre;
	
	public Integer getResiduos_municipales_id() {
		return residuos_municipales_id;
	}
	public void setResiduos_municipales_id(Integer residuos_municipales_id) {
		this.residuos_municipales_id = residuos_municipales_id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
