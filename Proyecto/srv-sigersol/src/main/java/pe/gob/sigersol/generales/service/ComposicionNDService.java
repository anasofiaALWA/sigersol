package pe.gob.sigersol.generales.service;

import java.util.List;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsComposicionND;
import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ComposicionNDDAO;

public class ComposicionNDService {

	private static Logger log = Logger.getLogger(ComposicionNDService.class);
	ComposicionNDDAO composicionNDDAO = new ComposicionNDDAO();
	
	public ClsResultado obtener(ClsComposicionND clsComposicionND){
		ClsResultado clsResultado = null;
		ClsResultado clsResultado2 = null;
		ClsResultado clsResultado3 = null;
		ClsResultado clsResultado4 = null;
		ClsResultado clsResultadoFinal = null;
		
		List<ClsComposicionND> listaPadre = null;
		List<ClsComposicionND> listaHijo = null;
		List<ClsComposicionND> listaNieto = null;
		List<ClsComposicionND> listaBisnieto = null;
		
		ClsGeneracion generacion = null;
		
		try {
			
			clsResultadoFinal = new ClsResultado();
			Integer gid = clsComposicionND.getGeneracion().getGeneracion_id();
			
			clsResultado = composicionNDDAO.obtener(clsComposicionND);
			
			listaPadre = (List<ClsComposicionND>) clsResultado.getObjeto();
			
			for(ClsComposicionND item: listaPadre){
				if(item.getTiene_hijos() == 1){
					
					generacion = new ClsGeneracion();
					generacion.setGeneracion_id(gid);
					item.setGeneracion(generacion);
										
					clsResultado2 = composicionNDDAO.obtener(item);
					
					listaHijo = (List<ClsComposicionND>) clsResultado2.getObjeto();
					
					item.setHijos(listaHijo);
					
					for(ClsComposicionND item2 : listaHijo){
							
						if(item2.getTiene_hijos() == 1){
						
							generacion = new ClsGeneracion();
							generacion.setGeneracion_id(gid);
							item2.setGeneracion(generacion);
							
							clsResultado3 = composicionNDDAO.obtener(item2);
								
							listaNieto = (List<ClsComposicionND>) clsResultado3.getObjeto();
							
							item2.setHijos(listaNieto);
							
							for(ClsComposicionND item3 : listaNieto){
								
								if(item3.getTiene_hijos() == 1){
								
									generacion = new ClsGeneracion();
									generacion.setGeneracion_id(gid);
									item3.setGeneracion(generacion);
									
									clsResultado4 = composicionNDDAO.obtener(item3);
										
									listaBisnieto = (List<ClsComposicionND>) clsResultado4.getObjeto();
									
									item3.setHijos(listaBisnieto);
								}
							}
						}
					}
					
				}
			}
			
			clsResultadoFinal.setObjeto(listaPadre);
			clsResultadoFinal.setExito(true);
			clsResultadoFinal.setMensaje("Obtención exitosa");
			
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		
		return clsResultadoFinal;
	}
	
	public ClsResultado insertar(ClsComposicionND clsComposicionND) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = composicionNDDAO.insertar(clsComposicionND);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsComposicionND clsComposicionND) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = composicionNDDAO.actualizar(clsComposicionND);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
