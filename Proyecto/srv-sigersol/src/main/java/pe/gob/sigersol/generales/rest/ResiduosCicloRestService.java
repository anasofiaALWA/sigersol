package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResiduosCiclo;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.ResiduosCicloService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/residuosCicloX")
@RequestScoped
public class ResiduosCicloRestService {
	
	private static Logger log = Logger.getLogger(ResiduosCicloRestService.class.getName());
	private ClsResiduosCiclo clsResiduosCiclo;
	private ClsValorizacion clsValorizacion;
	private ResiduosCicloService residuosCicloService;
	private List<ClsResiduosCiclo> listaResiduosCiclo;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			residuosCicloService = new ResiduosCicloService();
			
			jsonEntradaListaEquipo(paramJson);
			
			log.info("**Obtener Equipo de Barrido");
			
			clsResultado = residuosCicloService.obtener(clsResiduosCiclo);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstResiduosCiclo\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		residuosCicloService = new ResiduosCicloService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaEquipo(paramJson);

			for (ClsResiduosCiclo clsResiduosCiclo : listaResiduosCiclo) {
				clsResultado = residuosCicloService.insertar(clsResiduosCiclo);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		residuosCicloService = new ResiduosCicloService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaEquipo(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsResiduosCiclo clsResiduosCiclo : listaResiduosCiclo) {
			clsResultado = residuosCicloService.actualizar(clsResiduosCiclo);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Oferta
	 * 
	 * */
	private void jsonEntradaEquipo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> EquipoBarridoRestService : jsonEntradaEquipo" + jsonEntrada);

		JSONObject jsonObjectTipoOferta;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaResiduosCiclo = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsResiduosCiclo = new ClsResiduosCiclo();
			clsValorizacion = new ClsValorizacion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("valorizacion_id") != null && innerObject.get("valorizacion_id").toString() != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(innerObject.get("valorizacion_id").toString()));
			}
			
			clsResiduosCiclo.setValorizacion(clsValorizacion);
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsResiduosCiclo.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else {
				clsResiduosCiclo.setCantidad(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsResiduosCiclo.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaResiduosCiclo.add(clsResiduosCiclo);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
		
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaEquipo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> EquipoBarridoRestService :	 jsonEntradaListaEquipo" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectEquipo;
		JSONParser jsonParser = new JSONParser();
		
		clsResiduosCiclo = new ClsResiduosCiclo();
		clsValorizacion = new ClsValorizacion();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectEquipo = (JSONObject) jsonObjectPrincipal.get("ResiduosCiclo");
			
			if (jsonObjectEquipo.get("valorizacion_id") != null && jsonObjectEquipo.get("valorizacion_id") != "" &&
					jsonObjectEquipo.get("valorizacion_id") != " ") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectEquipo.get("valorizacion_id").toString()));
			}
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsResiduosCiclo.setValorizacion(clsValorizacion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
