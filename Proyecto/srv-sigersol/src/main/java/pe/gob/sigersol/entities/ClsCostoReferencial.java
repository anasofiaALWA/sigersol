package pe.gob.sigersol.entities;

public class ClsCostoReferencial {

	private Integer costo_referencial_id;
	private ClsValorizacion valorizacion;
	private ClsTipoResiduoSolido tipo_residuo;
	private Integer costo; 
	private Integer codigo_usuario;
	
	public Integer getCosto_referencial_id() {
		return costo_referencial_id;
	}
	public void setCosto_referencial_id(Integer costo_referencial_id) {
		this.costo_referencial_id = costo_referencial_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public ClsTipoResiduoSolido getTipo_residuo() {
		return tipo_residuo;
	}
	public void setTipo_residuo(ClsTipoResiduoSolido tipo_residuo) {
		this.tipo_residuo = tipo_residuo;
	}
	public Integer getCosto() {
		return costo;
	}
	public void setCosto(Integer costo) {
		this.costo = costo;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
