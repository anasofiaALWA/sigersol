package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAccionesEducacion;
import pe.gob.sigersol.entities.ClsPromotores;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoEventos;
import pe.gob.sigersol.entities.ClsTipoPromotor;

public class PromotoresDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(PromotoresDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_promotores = "{call SIGERSOLBL.SGR_SP_INS_PROMOTORES(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_promotores = "{call SIGERSOLBL.SGR_SP_UPD_PROMOTORES(?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_promotores = "{call SIGERSOLBL.SGR_SP_LIST_PROMOTORES(?,?)}";  
	
	public ClsResultado listarPromotores(ClsPromotores clsPromotores){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsPromotores> lista = new ArrayList<ClsPromotores>();
		con = null;
		String sql = this.sgr_sp_list_promotores;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPromotores.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsPromotores item = new ClsPromotores();
				ClsTipoPromotor tipoPromotor = new ClsTipoPromotor();
				
				try {
					tipoPromotor.setTipo_promotor_id(rs.getInt("TIPO_PROMOTOR_ID"));
					tipoPromotor.setNombre_eventos(rs.getString("NOMBRE_EVENTOS"));
					item.setTipo_promotor(tipoPromotor);
					if(clsPromotores.getEducacion_ambiental().getEducacion_ambiental_id() == -1){
						item.setPromotor_hombres(null);
						item.setPromotor_mujeres(null);
					} else{
						item.setPromotor_hombres(rs.getInt("PROMOTOR_HOMBRES"));
						item.setPromotor_mujeres(rs.getInt("PROMOTOR_MUJERES"));
					}
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsPromotores clsPromotores) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_promotores;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPromotores.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.setInt(2, clsPromotores.getTipo_promotor().getTipo_promotor_id());
			cs.setInt(3, clsPromotores.getPromotor_hombres());
			cs.setInt(4, clsPromotores.getPromotor_mujeres());
			cs.setInt(5, clsPromotores.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("PromotoresDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsPromotores clsPromotores) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_promotores;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPromotores.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.setInt(2, clsPromotores.getTipo_promotor().getTipo_promotor_id());
			cs.setInt(3, clsPromotores.getPromotor_hombres());
			cs.setInt(4, clsPromotores.getPromotor_mujeres());
			cs.setInt(5, clsPromotores.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("PromotoresDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
