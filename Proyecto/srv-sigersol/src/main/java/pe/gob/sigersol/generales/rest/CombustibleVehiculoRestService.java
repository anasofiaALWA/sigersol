package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCombustibleVehiculo;
import pe.gob.sigersol.entities.ClsRecDisposicionFinal;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsRecValorizacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoCombustible;
import pe.gob.sigersol.generales.service.CombustibleVehiculoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase CombustibleVehiculoRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de recoleccion permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/combustibleX")
@RequestScoped
public class CombustibleVehiculoRestService {

	private static Logger log = Logger.getLogger(CombustibleVehiculoRestService.class.getName());
	private Integer accion = null;
	private ClsCombustibleVehiculo clsCombustibleVehiculo;
	private ClsRecDisposicionFinal clsRecDisposicionFinal;
	private ClsTipoCombustible clsTipoCombustible;
	private CombustibleVehiculoService combustibleVehiculoService;
	private List<ClsCombustibleVehiculo> listaCombustibleVehiculo;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCombustible")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCombustibleVehiculo(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			combustibleVehiculoService = new CombustibleVehiculoService();
			
			jsonEntradaListaCombustible(paramJson);	
			log.info("**Obtener combustible Vehiculos");	
			clsResultado = combustibleVehiculoService.listarVehiculoRecoleccion(clsCombustibleVehiculo);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCombustible\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		combustibleVehiculoService = new CombustibleVehiculoService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			jsonEntradaCombustible(paramJson);

			for (ClsCombustibleVehiculo clsCombustibleVehiculo : listaCombustibleVehiculo) {
				clsResultado = combustibleVehiculoService.insertar(clsCombustibleVehiculo);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		combustibleVehiculoService = new CombustibleVehiculoService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCombustible(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCombustibleVehiculo clsCombustibleVehiculo : listaCombustibleVehiculo) {
			clsResultado = combustibleVehiculoService.actualizar(clsCombustibleVehiculo);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de CombustibleVehiculos
	 * 
	 * */
	private void jsonEntradaCombustible(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CombustibleVehiculoRestService : jsonEntradaCombustible" + jsonEntrada);

		JSONObject jsonObjectTipoCombustible;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCombustibleVehiculo = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCombustibleVehiculo = new ClsCombustibleVehiculo();
			clsTipoCombustible = new ClsTipoCombustible();
			clsRecDisposicionFinal = new ClsRecDisposicionFinal();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("rec_disposicion_final_id") != null && innerObject.get("rec_disposicion_final_id").toString() != "") {
				clsRecDisposicionFinal.setRec_disposicion_final_id(Integer.parseInt(innerObject.get("rec_disposicion_final_id").toString()));
			}
			else {
				clsRecDisposicionFinal.setRec_disposicion_final_id(0);
			}
			
			clsCombustibleVehiculo.setRecDisposicionFinal(clsRecDisposicionFinal);
			
			
			if (innerObject.get("tipo_combustible") != null && innerObject.get("tipo_combustible").toString() != "") {
				jsonObjectTipoCombustible  = (JSONObject) innerObject.get("tipo_combustible");
				clsTipoCombustible.setTipo_combustible_id(Integer.parseInt(jsonObjectTipoCombustible.get("tipo_combustible_id").toString()));
			}
			clsCombustibleVehiculo.setTipo_combustible(clsTipoCombustible);
			
			if (innerObject.get("consumo") != null && innerObject.get("consumo").toString() != "") {
				clsCombustibleVehiculo.setConsumo(Integer.parseInt(innerObject.get("consumo").toString()));
			} else{
				clsCombustibleVehiculo.setConsumo(0);
			}
			
			if (innerObject.get("costo") != null && innerObject.get("costo").toString() != "") {
				clsCombustibleVehiculo.setCosto(Integer.parseInt(innerObject.get("costo").toString()));
			} else{
				clsCombustibleVehiculo.setCosto(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCombustibleVehiculo.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCombustibleVehiculo.add(clsCombustibleVehiculo);	
		}
	
		log.info("----------------------------------------------------------------------------------");

	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCombustible(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CombustibleVehiculoRestService :	 jsonEntradaListaCombustible" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectCombustible;
		JSONParser jsonParser = new JSONParser();
		
		clsCombustibleVehiculo = new ClsCombustibleVehiculo();
		clsTipoCombustible = new ClsTipoCombustible();
		clsRecDisposicionFinal = new ClsRecDisposicionFinal();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectCombustible = (JSONObject) jsonObjectPrincipal.get("Combustible");
			
			if (jsonObjectCombustible.get("rec_disposicion_final_id") != null && jsonObjectCombustible.get("rec_disposicion_final_id") != "" &&
					jsonObjectCombustible.get("rec_disposicion_final_id") != " ") {
				clsRecDisposicionFinal.setRec_disposicion_final_id(Integer.parseInt(jsonObjectCombustible.get("rec_disposicion_final_id").toString()));
			}
			else
				clsRecDisposicionFinal.setRec_disposicion_final_id(-1);
			
			clsCombustibleVehiculo.setRecDisposicionFinal(clsRecDisposicionFinal);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
