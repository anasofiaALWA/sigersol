package pe.gob.sigersol.core.bpm;

import java.net.MalformedURLException;
import java.net.URL;

import org.jboss.logging.Logger;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.audit.AuditService;
import org.kie.api.task.TaskService;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;

public class ConexionBPM {

	private static final Logger log = Logger.getLogger(ConexionBPM.class);
	RuntimeEngine engine;
	private KieSession ksession;
	private TaskService taskService;
	protected String deploymentUrlStr;
	protected String deploymentId;
	protected String processDefinitionId;
	protected String emailDeSalida;
	private String usuario;
	private String password;
	private AuditService auditService;
	private boolean connected = false;

	public ConexionBPM(String _deploymentUrlStr, String _processDefinitionId, String _deploymentId, String _usuario,
			String _password) {
		this.usuario = _usuario;
		this.password = _password;
		this.deploymentUrlStr = _deploymentUrlStr;
		this.deploymentId = _deploymentId;
		this.processDefinitionId = _processDefinitionId;

		try {
			crearConexionBPM(usuario, password);
		} catch (Exception e) {
			log.error("Error!! ConexionBPM  e: " + e + " e.message: " + e.getMessage());
		}
	}

	public void crearConexionBPM(String usuario, String password) {
		try {
			log.info("crearConexionBPM>> usuario: " + this.usuario);
			log.info("crearConexionBPM>> password: " + this.password);
			log.info("crearConexionBPM>> deploymentUrlStr: " + deploymentUrlStr);
			log.info("crearConexionBPM>> deploymentId: " + deploymentId);
			log.info("crearConexionBPM>> processDefinitionId: " + processDefinitionId);

			this.engine = RemoteRuntimeEngineFactory.newRestBuilder().addUrl(new URL(deploymentUrlStr))
					.addUserName(this.usuario).addPassword(this.password).addTimeout(50).addDeploymentId(deploymentId)
					.build();
			this.ksession = this.engine.getKieSession();
			this.taskService = this.engine.getTaskService();
			this.auditService = this.engine.getAuditService();
			connected = true;
		} catch (MalformedURLException e) {
			connected = false;
			log.error("Error!! crearConexionBPM  e: " + e + " e.message: " + e.getMessage());
		}
	}

	public void disconnect() {
		connected = false;
		this.ksession = null;
		this.auditService = null;
		this.taskService = null;
	}

	public boolean isConnected() {
		return connected;
	}
}
