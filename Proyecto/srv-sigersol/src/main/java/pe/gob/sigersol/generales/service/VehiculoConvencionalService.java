package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoConvencional;
import pe.gob.sigersol.entities.dao.VehiculoConvencionalDAO;

public class VehiculoConvencionalService {

	private static Logger log = Logger.getLogger(VehiculoConvencionalService.class);
	VehiculoConvencionalDAO vehiculoConvencionalDAO = new VehiculoConvencionalDAO();
	
	public ClsResultado listarVehiculoConvencional(ClsVehiculoConvencional clsVehiculoConvencional){
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencionalDAO.listarVehiculoConvencional(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoConvencional clsVehiculoConvencional) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencionalDAO.insertar(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsVehiculoConvencional clsVehiculoConvencional) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencionalDAO.actualizar(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
