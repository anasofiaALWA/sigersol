package pe.gob.sigersol.generales.service;

import java.util.List;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCCGenCombustible;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CCGenCombustibleDAO;

public class CCGenCombustibleService {
	private static Logger log = Logger.getLogger(CCGenCombustibleService.class.getName());
	CCGenCombustibleDAO dao = new CCGenCombustibleDAO();
	
	public ClsResultado listar(Integer ccGenActividadId){
		ClsResultado clsResultado =null;
		try {
			clsResultado = dao.listarGenCombustible(ccGenActividadId);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(Integer ccGenActividadId,List<ClsCCGenCombustible> listCombustible){
		ClsResultado clsResultado =null;
		try {
			for (ClsCCGenCombustible clsCCGenCombustible : listCombustible) {
				clsResultado = dao.insertar(ccGenActividadId,clsCCGenCombustible);
			}
			
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		
		return clsResultado;
	}
	public ClsResultado actualizar(List<ClsCCGenCombustible> listCombustible){
		ClsResultado clsResultado =null;
		try {
			for (ClsCCGenCombustible clsCCGenCombustible : listCombustible) {
				clsResultado = dao.actualizar(clsCCGenCombustible);
			}
			
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		
		return clsResultado;
	}

}
