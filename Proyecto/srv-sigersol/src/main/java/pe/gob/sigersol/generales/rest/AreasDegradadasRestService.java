package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAreasDegradadas;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;
import pe.gob.sigersol.generales.service.AreasDegradadasService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase AreasDegradadasRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de DisposicionFinal, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/degradadas")
@RequestScoped
public class AreasDegradadasRestService {

	private static Logger log = Logger.getLogger(AreasDegradadasRestService.class.getName());
	
	private ClsDisposicionFinalAdm clsDisposicionFinalAdm;
	private ClsSitioDisposicion clsSitioDisposicion;
	private AreasDegradadasService areasDegradadasService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			areasDegradadasService = new AreasDegradadasService();
			
			jsonEntradaAreasDegradadas(paramJson);
			
			log.info("**obtener almacenamiento");
			
			clsResultado = areasDegradadasService.obtener(clsDisposicionFinalAdm);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"degradadas\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		areasDegradadasService = new AreasDegradadasService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaAreasDegradadas(paramJson);
			clsResultado = areasDegradadasService.insertar(clsDisposicionFinalAdm);		
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		areasDegradadasService = new AreasDegradadasService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaAreasDegradadas(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = areasDegradadasService.actualizar(clsDisposicionFinalAdm);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de AreasDegradadas
	 * 
	 * */
	private void jsonEntradaAreasDegradadas(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AreasDegradadasRestService :jsonEntradaAreasDegradadas" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectAreasDegradadas;
		JSONParser jsonParser = new JSONParser();
		
		clsDisposicionFinalAdm = new ClsDisposicionFinalAdm();
		clsSitioDisposicion = new ClsSitioDisposicion();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectAreasDegradadas = (JSONObject) jsonObjectPrincipal.get("Degradadas");
			
			if (jsonObjectAreasDegradadas.get("disposicion_final_id") != null && jsonObjectAreasDegradadas.get("disposicion_final_id") != "") {
				clsDisposicionFinalAdm.setDisposicion_final_id(Integer.parseInt(jsonObjectAreasDegradadas.get("disposicion_final_id").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("selec_degradada") != null && jsonObjectAreasDegradadas.get("selec_degradada") != "") {
				clsSitioDisposicion.setSitio_disposicion_id(Integer.parseInt(jsonObjectAreasDegradadas.get("selec_degradada").toString()));
			} 
			else
				clsSitioDisposicion.setSitio_disposicion_id(-1);
			
			clsDisposicionFinalAdm.setSitio_disposicion_id(clsSitioDisposicion);
			
			if (jsonObjectAreasDegradadas.get("anio_inicio") != null && jsonObjectAreasDegradadas.get("anio_inicio") != "") {
				clsDisposicionFinalAdm.setAnio_inicio(Integer.parseInt(jsonObjectAreasDegradadas.get("anio_inicio").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("cant_personal") != null && jsonObjectAreasDegradadas.get("cant_personal") != "") {
				clsDisposicionFinalAdm.setCant_personal(Integer.parseInt(jsonObjectAreasDegradadas.get("cant_personal").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("situacion_terreno_id") != null && jsonObjectAreasDegradadas.get("situacion_terreno_id") != "") {
				clsDisposicionFinalAdm.setSituacion_terreno_id((jsonObjectAreasDegradadas.get("situacion_terreno_id").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("admin_servicio_id") != null && jsonObjectAreasDegradadas.get("admin_servicio_id") != "") {
				clsDisposicionFinalAdm.setAdmin_servicio_id(jsonObjectAreasDegradadas.get("admin_servicio_id").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("temperatura_zona") != null && jsonObjectAreasDegradadas.get("temperatura_zona") != "") {
				clsDisposicionFinalAdm.setTemperatura_zona(Integer.parseInt(jsonObjectAreasDegradadas.get("temperatura_zona").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("direccion") != null && jsonObjectAreasDegradadas.get("direccion") != "") {
				clsDisposicionFinalAdm.setDireccion(jsonObjectAreasDegradadas.get("direccion").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("ref_direccion") != null && jsonObjectAreasDegradadas.get("ref_direccion") != "") {
				clsDisposicionFinalAdm.setRef_direccion(jsonObjectAreasDegradadas.get("ref_direccion").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("area_total_terreno") != null && jsonObjectAreasDegradadas.get("area_total_terreno") != "") {
				clsDisposicionFinalAdm.setArea_total_terreno(Integer.parseInt(jsonObjectAreasDegradadas.get("area_total_terreno").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("area_utilizada_terreno") != null && jsonObjectAreasDegradadas.get("area_utilizada_terreno") != "") {
				clsDisposicionFinalAdm.setArea_utilizada_terreno(Integer.parseInt(jsonObjectAreasDegradadas.get("area_utilizada_terreno").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("altura_nf") != null && jsonObjectAreasDegradadas.get("altura_nf") != "") {
				clsDisposicionFinalAdm.setAltura_nf(Integer.parseInt(jsonObjectAreasDegradadas.get("altura_nf").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("altura_rs_ss") != null && jsonObjectAreasDegradadas.get("altura_rs_ss") != "") {
				clsDisposicionFinalAdm.setAltura_rs_ss(jsonObjectAreasDegradadas.get("altura_rs_ss").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("tipo_manejo_eq_id") != null && jsonObjectAreasDegradadas.get("tipo_manejo_eq_id") != "") {
				clsDisposicionFinalAdm.setTipo_manejo_eq_id(jsonObjectAreasDegradadas.get("tipo_manejo_eq_id").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("tipo_manejo_cob_id") != null && jsonObjectAreasDegradadas.get("tipo_manejo_cob_id") != "") {
				clsDisposicionFinalAdm.setTipo_manejo_cob_id(jsonObjectAreasDegradadas.get("tipo_manejo_cob_id").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("tipo_tecnologia_id") != null && jsonObjectAreasDegradadas.get("tipo_tecnologia_id") != "") {
				clsDisposicionFinalAdm.setRef_direccion(jsonObjectAreasDegradadas.get("tipo_tecnologia_id").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("maneja_lixiviados") != null && jsonObjectAreasDegradadas.get("maneja_lixiviados") != "") {
				clsDisposicionFinalAdm.setManeja_lixiviados(Integer.parseInt(jsonObjectAreasDegradadas.get("maneja_lixiviados").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("tipo_manejo_lixiviados_id") != null && jsonObjectAreasDegradadas.get("tipo_manejo_lixiviados_id") != "") {
				clsDisposicionFinalAdm.setTipo_manejo_lixiviados_id(jsonObjectAreasDegradadas.get("tipo_manejo_lixiviados_id").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("maneja_gases") != null && jsonObjectAreasDegradadas.get("maneja_gases") != "") {
				clsDisposicionFinalAdm.setManeja_gases(Integer.parseInt(jsonObjectAreasDegradadas.get("maneja_gases").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("num_chimenea_inst") != null && jsonObjectAreasDegradadas.get("num_chimenea_inst") != "") {
				clsDisposicionFinalAdm.setNum_chimenea_inst(Integer.parseInt(jsonObjectAreasDegradadas.get("num_chimenea_inst").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("num_chimenea_quem_op") != null && jsonObjectAreasDegradadas.get("num_chimenea_quem_op") != "") {
				clsDisposicionFinalAdm.setNum_chimenea_quem_op(Integer.parseInt(jsonObjectAreasDegradadas.get("num_chimenea_quem_op").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("tipo_manejo_gases") != null && jsonObjectAreasDegradadas.get("tipo_manejo_gases") != "") {
				clsDisposicionFinalAdm.setTipo_manejo_gases(jsonObjectAreasDegradadas.get("tipo_manejo_gases").toString());
			}
			
			if (jsonObjectAreasDegradadas.get("genera_ee") != null && jsonObjectAreasDegradadas.get("genera_ee") != "") {
				clsDisposicionFinalAdm.setGenera_ee(Integer.parseInt(jsonObjectAreasDegradadas.get("genera_ee").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("cap_instalada_ee_mw") != null && jsonObjectAreasDegradadas.get("cap_instalada_ee_mw") != "") {
				clsDisposicionFinalAdm.setCap_instalada_ee_mw(Integer.parseInt(jsonObjectAreasDegradadas.get("cap_instalada_ee_mw").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("generacion_ee_anual_mw") != null && jsonObjectAreasDegradadas.get("generacion_ee_anual_mw") != "") {
				clsDisposicionFinalAdm.setGeneracion_ee_anual_mw(Integer.parseInt(jsonObjectAreasDegradadas.get("generacion_ee_anual_mw").toString()));
			}
			
			if (jsonObjectAreasDegradadas.get("cod_usuario") != null && jsonObjectAreasDegradadas.get("cod_usuario") != "") {
				clsDisposicionFinalAdm.setCodigo_usuario(Integer.parseInt(jsonObjectAreasDegradadas.get("cod_usuario").toString()));
			}
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
