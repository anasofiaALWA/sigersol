package pe.gob.sigersol.entities;

public class ClsVehiculoRecoleccion {
	
	private Integer vehiculo_recoleccion_id;
	private ClsRecDisposicionFinal recDisposicionFinal;
	private ClsRecValorizacion recValorizacion;
	private ClsRecRcdOm recRcdOm;
	private ClsTipoVehiculo tipo_vehiculo;
	private Integer antig_promedio;
	private Double capacidad;
	private Double recoleccion_anual;
	private Integer numero_unidades;
	private Integer codigo_usuario;
	
	public Integer getVehiculo_recoleccion_id() {
		return vehiculo_recoleccion_id;
	}
	public void setVehiculo_recoleccion_id(Integer vehiculo_recoleccion_id) {
		this.vehiculo_recoleccion_id = vehiculo_recoleccion_id;
	}
	public ClsRecDisposicionFinal getRecDisposicionFinal() {
		return recDisposicionFinal;
	}
	public void setRecDisposicionFinal(ClsRecDisposicionFinal recDisposicionFinal) {
		this.recDisposicionFinal = recDisposicionFinal;
	}
	public ClsRecValorizacion getRecValorizacion() {
		return recValorizacion;
	}
	public void setRecValorizacion(ClsRecValorizacion recValorizacion) {
		this.recValorizacion = recValorizacion;
	}
	public ClsRecRcdOm getRecRcdOm() {
		return recRcdOm;
	}
	public void setRecRcdOm(ClsRecRcdOm recRcdOm) {
		this.recRcdOm = recRcdOm;
	}
	public ClsTipoVehiculo getTipo_vehiculo() {
		return tipo_vehiculo;
	}
	public void setTipo_vehiculo(ClsTipoVehiculo tipo_vehiculo) {
		this.tipo_vehiculo = tipo_vehiculo;
	}
	public Integer getAntig_promedio() {
		return antig_promedio;
	}
	public void setAntig_promedio(Integer antig_promedio) {
		this.antig_promedio = antig_promedio;
	}
	public Double getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(Double capacidad) {
		this.capacidad = capacidad;
	}
	public Double getRecoleccion_anual() {
		return recoleccion_anual;
	}
	public void setRecoleccion_anual(Double recoleccion_anual) {
		this.recoleccion_anual = recoleccion_anual;
	}
	public Integer getNumero_unidades() {
		return numero_unidades;
	}
	public void setNumero_unidades(Integer numero_unidades) {
		this.numero_unidades = numero_unidades;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
