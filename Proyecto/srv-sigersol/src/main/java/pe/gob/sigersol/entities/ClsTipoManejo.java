package pe.gob.sigersol.entities;

public class ClsTipoManejo {

	private Integer tipo_manejo_id;
	private ClsDisposicionFinalAdm disposicion_final_adm;
	private ClsCobertura cobertura;
	private Integer espesor_cobertura;
	private Integer codigo_usuario;
	
	public Integer getTipo_manejo_id() {
		return tipo_manejo_id;
	}
	public void setTipo_manejo_id(Integer tipo_manejo_id) {
		this.tipo_manejo_id = tipo_manejo_id;
	}
	public ClsDisposicionFinalAdm getDisposicion_final_adm() {
		return disposicion_final_adm;
	}
	public void setDisposicion_final_adm(ClsDisposicionFinalAdm disposicion_final_adm) {
		this.disposicion_final_adm = disposicion_final_adm;
	}
	public ClsCobertura getCobertura() {
		return cobertura;
	}
	public void setCobertura(ClsCobertura cobertura) {
		this.cobertura = cobertura;
	}
	public Integer getEspesor_cobertura() {
		return espesor_cobertura;
	}
	public void setEspesor_cobertura(Integer espesor_cobertura) {
		this.espesor_cobertura = espesor_cobertura;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
