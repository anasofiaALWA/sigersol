package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsGeneracionNoDomiciliar;
import pe.gob.sigersol.entities.ClsResSolidos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoGeneracion;
import pe.gob.sigersol.generales.service.GeneracionNoDomiciliarService;
import pe.gob.sigersol.generales.service.ResSolidosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase ResSolidosRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de generación permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/resSolidos")
@RequestScoped
public class ResSolidosRestService {

	private static Logger log = Logger.getLogger(ResSolidosRestService.class.getName());
	private Integer accion = null;
	
	private ClsResSolidos clsResSolidos;
	private ClsGeneracion clsGeneracion;
	private ResSolidosService resSolidosService;
	private List<ClsResSolidos> listaResSolidos;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			resSolidosService = new ResSolidosService();
			
			clsResSolidos = new ClsResSolidos();
			
			jsonEntradaListaResSolidos(paramJson);
			
			log.info("**Obtener Residuos Solidos ");
			
			clsResultado = resSolidosService.obtener(clsResSolidos);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstResSolidos\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
		
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		resSolidosService = new ResSolidosService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaResSolidos(paramJson);

			for (ClsResSolidos clsResSolidos : listaResSolidos) {
				clsResultado = resSolidosService.insertar(clsResSolidos);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		resSolidosService = new ResSolidosService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaResSolidos(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsResSolidos clsResSolidos : listaResSolidos) {
			clsResultado = resSolidosService.actualizar(clsResSolidos);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de ResSolidos
	 * 
	 * */
	private void jsonEntradaResSolidos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ResSolidosRestService : jsonEntradaResSolidos" + jsonEntrada);

		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaResSolidos = new ArrayList();
		
		// parsear el objeto Json en una lista
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			JSONObject innerObject = (JSONObject) i.next();
			
			String hijos = innerObject.get("hijos").toString();
			
			JSONParser jsonParser2 = new JSONParser();
			JSONArray jsonObject2 = (JSONArray) jsonParser2.parse(hijos);
			
			Iterator it = jsonObject2.iterator();
			
			
			while(it.hasNext()){
				
				clsResSolidos = new ClsResSolidos();
				clsGeneracion = new ClsGeneracion();
				
				JSONObject innerObject2 = (JSONObject) it.next();
				
				if(innerObject2.get("hijos") == null){
					
					if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
						clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
					}
					
					clsResSolidos.setGeneracion(clsGeneracion);
					
					if (innerObject2.get("tipors_id") != null && innerObject2.get("tipors_id").toString() != "") {
						clsResSolidos.setTipors_id(Integer.parseInt(innerObject2.get("tipors_id").toString()));
					} else {
						clsResSolidos.setTipors_id(0);
					}
					
					if (innerObject2.get("porcentaje") != null && innerObject2.get("porcentaje").toString() != "") {
						clsResSolidos.setPorcentaje(Double.valueOf((innerObject2.get("porcentaje").toString())));
					} else {
						clsResSolidos.setPorcentaje(0.0);
					}
					
					if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
						clsResSolidos.setCod_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
					}
					
					listaResSolidos.add(clsResSolidos);	
					
				} else {
					
					String nietos = innerObject2.get("hijos").toString();
					
					JSONParser jsonParser3 = new JSONParser();
					JSONArray jsonObject3 = (JSONArray) jsonParser3.parse(nietos);
					
					Iterator itr = jsonObject3.iterator();
					
					while(itr.hasNext()){
						
						clsResSolidos = new ClsResSolidos();
						clsGeneracion = new ClsGeneracion();
						
						JSONObject innerObject3 = (JSONObject) itr.next();
						
						if(innerObject3.get("hijos") == null){
						
							if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
								clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
							}
							
							clsResSolidos.setGeneracion(clsGeneracion);
							
							if (innerObject3.get("tipors_id") != null && innerObject3.get("tipors_id").toString() != "") {
								clsResSolidos.setTipors_id(Integer.parseInt(innerObject3.get("tipors_id").toString()));
							} else {
								clsResSolidos.setTipors_id(0);
							}
							
							if (innerObject3.get("porcentaje") != null && innerObject3.get("porcentaje").toString() != "") {
								clsResSolidos.setPorcentaje(Double.valueOf(innerObject3.get("porcentaje").toString()));
							} else {
								clsResSolidos.setPorcentaje(0.0);
							}
							
							if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
								clsResSolidos.setCod_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
							}
							
							listaResSolidos.add(clsResSolidos);	
						} else  {
						
							String bisnietos = innerObject3.get("hijos").toString();
							
							JSONParser jsonParser4 = new JSONParser();
							JSONArray jsonObject4 = (JSONArray) jsonParser4.parse(bisnietos);
							
							Iterator iter = jsonObject4.iterator();
							
							while(iter.hasNext()){
								
								clsResSolidos = new ClsResSolidos();
								clsGeneracion = new ClsGeneracion();
								
								JSONObject innerObject4 = (JSONObject) iter.next();
								
								if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
									clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
								}
								
								clsResSolidos.setGeneracion(clsGeneracion);
								
								if (innerObject4.get("tipors_id") != null && innerObject4.get("tipors_id").toString() != "") {
									clsResSolidos.setTipors_id(Integer.parseInt(innerObject4.get("tipors_id").toString()));
								} else {
									clsResSolidos.setTipors_id(0);
								}
								
								if (innerObject4.get("porcentaje") != null && innerObject4.get("porcentaje").toString() != "") {
									clsResSolidos.setPorcentaje(Double.valueOf(innerObject4.get("porcentaje").toString()));
								} else {
									clsResSolidos.setPorcentaje(0.0);
								}
								
								if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
									clsResSolidos.setCod_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
								}
								
								listaResSolidos.add(clsResSolidos);
							}
						}
					}
				}
					
			}
			
			log.info("----------------------------------------------------------------------------------");
		}
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaResSolidos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ResSolidosRestService : jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResSolidos;
		JSONParser jsonParser = new JSONParser();
		
		clsResSolidos = new ClsResSolidos();
		clsGeneracion = new ClsGeneracion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResSolidos = (JSONObject) jsonObjectPrincipal.get("ResSolidos");
			
			if (jsonObjectResSolidos.get("generacion_id") != null && jsonObjectResSolidos.get("generacion_id") != "" &&
					jsonObjectResSolidos.get("generacion_id") != " ") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(jsonObjectResSolidos.get("generacion_id").toString()));
			}
			else
				clsGeneracion.setGeneracion_id(-1);
			
			clsResSolidos.setGeneracion(clsGeneracion);
			
			clsResSolidos.setTipors_id(0);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
