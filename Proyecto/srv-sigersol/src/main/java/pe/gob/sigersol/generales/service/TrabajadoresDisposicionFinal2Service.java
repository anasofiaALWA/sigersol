package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTrabajadores;
import pe.gob.sigersol.entities.dao.TrabajadoresDisposicionFinal2DAO;

public class TrabajadoresDisposicionFinal2Service {

	private static Logger log = Logger.getLogger(TrabajadoresDisposicionFinal2Service.class);
	TrabajadoresDisposicionFinal2DAO trabajadoresDisposicionFinal2DAO = new TrabajadoresDisposicionFinal2DAO();

	public ClsResultado listarTrabajadores(ClsTrabajadores clsTrabajadores){
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresDisposicionFinal2DAO.listarTrabajadores(clsTrabajadores);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTrabajadores clsTrabajadores) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresDisposicionFinal2DAO.insertar(clsTrabajadores);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
