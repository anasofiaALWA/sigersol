package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCombustibleVehiculo;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CombustibleVehiculo3DAO;

public class CombustibleVehiculo3Service {

	private static Logger log = Logger.getLogger(CombustibleVehiculoService.class);
	CombustibleVehiculo3DAO combustibleVehiculo3DAO = new CombustibleVehiculo3DAO();
	
	public ClsResultado listarCombustibleVehiculo(ClsCombustibleVehiculo clsCombustibleVehiculo){
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculo3DAO.listarCombustibleVehiculo(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCombustibleVehiculo clsCombustibleVehiculo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculo3DAO.insertar(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCombustibleVehiculo clsCombustibleVehiculo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculo3DAO.actualizar(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
