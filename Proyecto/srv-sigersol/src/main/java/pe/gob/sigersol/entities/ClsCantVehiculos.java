package pe.gob.sigersol.entities;

public class ClsCantVehiculos {

	private Integer cant_vehiculos_id;
	private ClsDisposicionFinalAdm disposicion_final_adm;
	private ClsTipoVehiculo tipoVehiculo;
	private Integer tiempo_parcial;
	private Integer tiempo_completo;
	private Integer codigo_usuario;
	
	public Integer getCant_vehiculos_id() {
		return cant_vehiculos_id;
	}
	public void setCant_vehiculos_id(Integer cant_vehiculos_id) {
		this.cant_vehiculos_id = cant_vehiculos_id;
	}
	public ClsDisposicionFinalAdm getDisposicion_final_adm() {
		return disposicion_final_adm;
	}
	public void setDisposicion_final_adm(ClsDisposicionFinalAdm disposicion_final_adm) {
		this.disposicion_final_adm = disposicion_final_adm;
	}
	public ClsTipoVehiculo getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(ClsTipoVehiculo tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public Integer getTiempo_parcial() {
		return tiempo_parcial;
	}
	public void setTiempo_parcial(Integer tiempo_parcial) {
		this.tiempo_parcial = tiempo_parcial;
	}
	public Integer getTiempo_completo() {
		return tiempo_completo;
	}
	public void setTiempo_completo(Integer tiempo_completo) {
		this.tiempo_completo = tiempo_completo;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
