package pe.gob.sigersol.entities;

public class ClsSalida {

	private String codigo;
	private String mensaje;
	private Object data;

	public ClsSalida() {
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ClsSalida [codigo=" + codigo + ", mensaje=" + mensaje + ", data=" + data + "]";
	}

	public ClsSalida(String codigo, String mensaje, Object data) {
		super();
		this.codigo = codigo;
		this.mensaje = mensaje;
		this.data = data;
	}

}
