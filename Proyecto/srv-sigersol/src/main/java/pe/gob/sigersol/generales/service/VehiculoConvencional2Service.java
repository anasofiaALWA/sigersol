package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoConvencional;
import pe.gob.sigersol.entities.dao.VehiculoConvencional2DAO;

public class VehiculoConvencional2Service {

	private static Logger log = Logger.getLogger(VehiculoConvencional2Service.class);
	VehiculoConvencional2DAO vehiculoConvencional2DAO = new VehiculoConvencional2DAO();
	
	public ClsResultado listarVehiculoConvencional(ClsVehiculoConvencional clsVehiculoConvencional){
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional2DAO.listarVehiculoConvencional(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoConvencional clsVehiculoConvencional) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional2DAO.insertar(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsVehiculoConvencional clsVehiculoConvencional) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional2DAO.actualizar(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
