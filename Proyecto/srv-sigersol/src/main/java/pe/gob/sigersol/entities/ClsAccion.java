package pe.gob.sigersol.entities;

public class ClsAccion {

	private Integer accion_id;
	private ClsTipoActividad tipo_actividad;
	private ClsEducacionAmbiental educacion_ambiental;
	private Integer tipo;
	private Integer participantes_hombres;
	private Integer participantes_mujeres;
	private Integer total_mujer_hombre;
	private Integer codigo_usuario;
	
	public Integer getAccion_id() {
		return accion_id;
	}
	public void setAccion_id(Integer accion_id) {
		this.accion_id = accion_id;
	}
	public ClsTipoActividad getTipo_actividad() {
		return tipo_actividad;
	}
	public void setTipo_actividad(ClsTipoActividad tipo_actividad) {
		this.tipo_actividad = tipo_actividad;
	}
	public ClsEducacionAmbiental getEducacion_ambiental() {
		return educacion_ambiental;
	}
	public void setEducacion_ambiental(ClsEducacionAmbiental educacion_ambiental) {
		this.educacion_ambiental = educacion_ambiental;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public Integer getParticipantes_hombres() {
		return participantes_hombres;
	}
	public void setParticipantes_hombres(Integer participantes_hombres) {
		this.participantes_hombres = participantes_hombres;
	}
	public Integer getParticipantes_mujeres() {
		return participantes_mujeres;
	}
	public void setParticipantes_mujeres(Integer participantes_mujeres) {
		this.participantes_mujeres = participantes_mujeres;
	}
	public Integer getTotal_mujer_hombre() {
		return total_mujer_hombre;
	}
	public void setTotal_mujer_hombre(Integer total_mujer_hombre) {
		this.total_mujer_hombre = total_mujer_hombre;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}	
}
