package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCeldas;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsInfoCeldas;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.InfoCeldasService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase InfoCeldasRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de disposicion final permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/infoCeldas")
@RequestScoped
public class InfoCeldasRestService {

	private static Logger log = Logger.getLogger(TipoManejoRestService.class.getName());
	private ClsInfoCeldas clsInfoCeldas;
	private ClsCeldas clsCeldas;
	private ClsDisposicionFinalAdm clsDisposicionFinalAdm;
	private InfoCeldasService infoCeldasService;
	private List<ClsInfoCeldas> listaInfoCeldas;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCeldas")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCeldas(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			infoCeldasService = new InfoCeldasService();
			
			jsonEntradaListaInfoCeldas(paramJson);
			
			log.info("**Obtener Info Celdas**");
			
			clsResultado = infoCeldasService.listarCeldas(clsInfoCeldas);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCeldas\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		infoCeldasService = new InfoCeldasService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			jsonEntradaInfoCeldas(paramJson);

			for (ClsInfoCeldas clsInfoCeldas : listaInfoCeldas) {
				clsResultado = infoCeldasService.insertar(clsInfoCeldas);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		infoCeldasService = new InfoCeldasService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaInfoCeldas(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsInfoCeldas clsInfoCeldas : listaInfoCeldas) {
			clsResultado = infoCeldasService.actualizar(clsInfoCeldas);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de InfoCeldas
	 * 
	 * */
	private void jsonEntradaInfoCeldas(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> InfoCeldasRestService : jsonEntradaInfoCeldas" + jsonEntrada);

		JSONObject jsonObjectCeldas;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaInfoCeldas = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsInfoCeldas = new ClsInfoCeldas();
			clsCeldas = new ClsCeldas();
			clsDisposicionFinalAdm = new ClsDisposicionFinalAdm();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("disposicion_final_id") != null && innerObject.get("disposicion_final_id").toString() != "") {
				clsDisposicionFinalAdm.setDisposicion_final_id(Integer.parseInt(innerObject.get("disposicion_final_id").toString()));
			}
			
			clsInfoCeldas.setDisposicion_final_adm(clsDisposicionFinalAdm);
			
			if (innerObject.get("celdas") != null && innerObject.get("celdas").toString() != "") {
				jsonObjectCeldas  = (JSONObject) innerObject.get("celdas");
				clsCeldas.setCeldas_id(Integer.parseInt(jsonObjectCeldas.get("celdas_id").toString()));
			}
			clsInfoCeldas.setCeldas(clsCeldas);
			
			if (innerObject.get("celdas_culminadas") != null && innerObject.get("celdas_culminadas").toString() != "") {
				clsInfoCeldas.setCeldas_culminadas(Integer.parseInt(innerObject.get("celdas_culminadas").toString()));
			} else {
				clsInfoCeldas.setCeldas_culminadas(0);
			}
			
			if (innerObject.get("celdas_activas") != null && innerObject.get("celdas_activas").toString() != "") {
				clsInfoCeldas.setCeldas_activas(Integer.parseInt(innerObject.get("celdas_activas").toString()));
			} else {
				clsInfoCeldas.setCeldas_activas(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsInfoCeldas.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaInfoCeldas.add(clsInfoCeldas);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaInfoCeldas(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> InfoCeldasRestService : jsonEntradaListaInfoCeldas" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectInfoCeldas;
		JSONParser jsonParser = new JSONParser();
		
		clsInfoCeldas = new ClsInfoCeldas();
		clsCeldas = new ClsCeldas();
		clsDisposicionFinalAdm = new ClsDisposicionFinalAdm();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectInfoCeldas = (JSONObject) jsonObjectPrincipal.get("InfoCeldas");
			
			if (jsonObjectInfoCeldas.get("disposicion_final_id") != null && jsonObjectInfoCeldas.get("disposicion_final_id") != "" &&
					jsonObjectInfoCeldas.get("disposicion_final_id") != " ") {
				clsDisposicionFinalAdm.setDisposicion_final_id(Integer.parseInt(jsonObjectInfoCeldas.get("disposicion_final_id").toString()));
			}
			else
				clsDisposicionFinalAdm.setDisposicion_final_id(-1);
			
			clsInfoCeldas.setDisposicion_final_adm(clsDisposicionFinalAdm);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
