package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsOtrosRcdOm;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduo;
import pe.gob.sigersol.generales.service.OtrosRcdOmService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/otrosRcdOm")
@RequestScoped
public class OtrosRcdOmRestService {
	
	private static Logger log = Logger.getLogger(OtrosRcdOmRestService.class.getName());
	private ClsOtrosRcdOm clsOtrosRcdOm;
	private ClsRecRcdOm clsRecRcdOm;
	private ClsTipoResiduo clsTipoResiduo;
	private OtrosRcdOmService otrosRcdOmService;
	private List<ClsOtrosRcdOm> listaOtrosRcdOm;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarOtrosRcdOm")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarOtrosRcdOm(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			otrosRcdOmService = new OtrosRcdOmService();
			
			jsonEntradaListaCantidadResiduos(paramJson);
			
			log.info("**Obtener Recoleccion Vehiculos");
			
			clsResultado = otrosRcdOmService.listarOtrosRcdOm(clsOtrosRcdOm);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstOtrosRcdOm\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		otrosRcdOmService = new OtrosRcdOmService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaCantidadResiduos(paramJson);
	
			for (ClsOtrosRcdOm clsOtrosRcdOm : listaOtrosRcdOm) {
				clsResultado = otrosRcdOmService.insertar(clsOtrosRcdOm);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		otrosRcdOmService = new OtrosRcdOmService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCantidadResiduos(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsOtrosRcdOm clsOtrosRcdOm : listaOtrosRcdOm) {
			clsResultado = otrosRcdOmService.actualizar(clsOtrosRcdOm);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaCantidadResiduos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantidadResiduosRestService : jsonEntradaCantidadResiduos" + jsonEntrada);

		JSONObject jsonObjectTipoResiduos;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaOtrosRcdOm = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsOtrosRcdOm = new ClsOtrosRcdOm();
			clsRecRcdOm = new ClsRecRcdOm();
			clsTipoResiduo = new ClsTipoResiduo();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("rec_rcd_om_id") != null && innerObject.get("rec_rcd_om_id").toString() != "") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(innerObject.get("rec_rcd_om_id").toString()));
			} 
			else {
				clsRecRcdOm.setRec_rcd_om_id(0);
			}
			
			clsOtrosRcdOm.setRecRcdOm(clsRecRcdOm);
			
			if (innerObject.get("tipoResiduos") != null && innerObject.get("tipoResiduos").toString() != "") {
				jsonObjectTipoResiduos  = (JSONObject) innerObject.get("tipoResiduos");
				clsTipoResiduo.setTipo_residuos_id(Integer.parseInt(jsonObjectTipoResiduos.get("tipo_residuos_id").toString()));
			}
			clsOtrosRcdOm.setTipoResiduos(clsTipoResiduo);
			
			if (innerObject.get("usuarioAtendido") != null && innerObject.get("usuarioAtendido").toString() != "") {
				clsOtrosRcdOm.setUsuarioAtendido(Integer.parseInt(innerObject.get("usuarioAtendido").toString()));
			} else{
				clsOtrosRcdOm.setUsuarioAtendido(0);
			}
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsOtrosRcdOm.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else{
				clsOtrosRcdOm.setCantidad(0);
			}
		
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsOtrosRcdOm.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaOtrosRcdOm.add(clsOtrosRcdOm);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCantidadResiduos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantidadResiduosRestService : jsonEntradaListaCantidadResiduos" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectCantidadResiduos;
		JSONParser jsonParser = new JSONParser();
		
		clsOtrosRcdOm = new ClsOtrosRcdOm();
		clsRecRcdOm = new ClsRecRcdOm();
		clsTipoResiduo = new ClsTipoResiduo();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectCantidadResiduos = (JSONObject) jsonObjectPrincipal.get("OtrosRcdOm");
			
			if (jsonObjectCantidadResiduos.get("rec_rcd_om_id") != null && jsonObjectCantidadResiduos.get("rec_rcd_om_id") != "" &&
					jsonObjectCantidadResiduos.get("rec_rcd_om_id") != " ") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(jsonObjectCantidadResiduos.get("rec_rcd_om_id").toString()));
			}
			else
				clsRecRcdOm.setRec_rcd_om_id(-1);
			
			clsOtrosRcdOm.setRecRcdOm(clsRecRcdOm);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
