package pe.gob.sigersol.util;

import java.util.Properties;

import org.jboss.logging.Logger;

public class ApplicationProperties {

	private static final Logger log = Logger.getLogger(ApplicationProperties.class.getName());
	private static final Properties properties;
	private static final String APPLICATION_PROPERTIES = "SrvSigersolProperties.properties";

	public enum ApplicationPropertiesEnum {
		DATASOURCE_NEGOCIO("datasource.negocio"), 
		NOMBRE_APLICACION("nombreAplicacion"),
		DIR_TEMP("dirTemp"),
		SERVER_LDAP("host"),
		PORT_LDAP("port"),
		TIPO_AUTH("tipoAuth"),
		BASE_DN("baseDN"),
		BASE_DN_PRINCIPAL("baseDNPrincipal"),
		CREDENCIAL_PRINCIPAL("credencialPrincipal"),
		ATRIBUTE_FOR_USER("attributeForUser"),
		SISTEMA_ID("sistemaId"),
		APP_SERVER("appServer"),
		LINK_APP("linkApp")
		;

		private String key;

		ApplicationPropertiesEnum(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}

		@Override
		public String toString() {
			return key;
		}
	}

	static {
		properties = new Properties();
		try {
			properties.putAll(PropertyFileUtils.load(APPLICATION_PROPERTIES));
		} catch (Exception e) {
			log.error("ERROR! No se encuentra el archivo '" + APPLICATION_PROPERTIES + "' en el classpath. " + e);
		}
	}

	private ApplicationProperties() {
	}

	public static String getProperty(ApplicationPropertiesEnum strProperty) {
		return properties.getProperty(strProperty.getKey());
	}

	public static int getIntProperty(ApplicationPropertiesEnum strProperty) {
		return Integer.valueOf(properties.getProperty(strProperty.getKey())).intValue();
	}

	public static long getLongProperty(ApplicationPropertiesEnum strProperty) {
		return Long.valueOf(properties.getProperty(strProperty.getKey())).longValue();
	}

	public static String getPropertyByKey(String key) {
		return properties.getProperty(key);
	}

	public static String dirDeployed() {
		String path = ApplicationProperties.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		path = path.replaceAll("/WEB-INF/classes", "");
		// System.out.println("IbsmoProperties dirDeployed path>> " + path);
		return path;
	}

	public static String dirUpload() {
		String strDirUpload = "/src/upload/";
		log.info("strDirUpload: " + strDirUpload);
		return strDirUpload;
	}
}
