package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsPlantaValorizacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.PlantaValorizacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/plantaValorizacion")
@RequestScoped
public class PlantaValorizacionRestService {

	private static Logger log = Logger.getLogger(PlantaValorizacionRestService.class.getName());
	private ClsPlantaValorizacion clsPlantaValorizacion;
	private ClsValorizacion clsValorizacion;
	private PlantaValorizacionService plantaValorizacionService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		plantaValorizacionService = new PlantaValorizacionService();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener Planta de Valorizacion**");		
			jsonEntradaValorizacion(paramJson);	
			clsResultado = plantaValorizacionService.obtener(clsPlantaValorizacion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"planta\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		plantaValorizacionService = new PlantaValorizacionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaValorizacion(paramJson);
			clsResultado = plantaValorizacionService.insertar(clsPlantaValorizacion);			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		plantaValorizacionService = new PlantaValorizacionService();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaValorizacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = plantaValorizacionService.actualizar(clsPlantaValorizacion);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Valorización  
	 * 
	 * */
	private void jsonEntradaValorizacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ValorizacionRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectValorizacion;
		JSONParser jsonParser = new JSONParser();
		
		clsPlantaValorizacion = new ClsPlantaValorizacion();
		clsValorizacion = new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectValorizacion = (JSONObject) jsonObjectPrincipal.get("Planta");
			
			if (jsonObjectValorizacion.get("planta_valorizacion_id") != null && jsonObjectValorizacion.get("planta_valorizacion_id") != "") {
				clsPlantaValorizacion.setPlanta_valorizacion_id(Integer.parseInt(jsonObjectValorizacion.get("planta_valorizacion_id").toString()));
			}
			
			if (jsonObjectValorizacion.get("valorizacion_id") != null && jsonObjectValorizacion.get("valorizacion_id") != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectValorizacion.get("valorizacion_id").toString()));
			} 
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsPlantaValorizacion.setValorizacion(clsValorizacion);
			
			if (jsonObjectValorizacion.get("adm_propia") != null && jsonObjectValorizacion.get("adm_propia") != "") {
				clsPlantaValorizacion.setAdm_propia(Integer.parseInt(jsonObjectValorizacion.get("adm_propia").toString()));
			}
			
			if (jsonObjectValorizacion.get("tercerizado") != null && jsonObjectValorizacion.get("tercerizado") != "") {
				clsPlantaValorizacion.setTercerizado(Integer.parseInt(jsonObjectValorizacion.get("tercerizado").toString()));
			}
			
			if (jsonObjectValorizacion.get("mixto") != null && jsonObjectValorizacion.get("mixto") != "") {
				clsPlantaValorizacion.setMixto(Integer.parseInt(jsonObjectValorizacion.get("mixto").toString()));
			}

			if (jsonObjectValorizacion.get("zona_coordenada") != null && jsonObjectValorizacion.get("zona_coordenada") != "") {
				clsPlantaValorizacion.setZona_coordenada(jsonObjectValorizacion.get("zona_coordenada").toString());
			}
			else {
				clsPlantaValorizacion.setZona_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("norte_coordenada") != null && jsonObjectValorizacion.get("norte_coordenada") != "") {
				clsPlantaValorizacion.setNorte_coordenada(jsonObjectValorizacion.get("norte_coordenada").toString());
			}
			else{
				clsPlantaValorizacion.setNorte_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("sur_coordenada") != null && jsonObjectValorizacion.get("sur_coordenada") != "") {
				clsPlantaValorizacion.setSur_coordenada(jsonObjectValorizacion.get("sur_coordenada").toString());
			}
			else{
				clsPlantaValorizacion.setSur_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("division_politica") != null && jsonObjectValorizacion.get("division_politica") != "") {
				clsPlantaValorizacion.setDivision_politica(jsonObjectValorizacion.get("division_politica").toString());
			}
			else{
				clsPlantaValorizacion.setDivision_politica("");
			}
			if (jsonObjectValorizacion.get("direccion_iga") != null && jsonObjectValorizacion.get("direccion_iga") != "") {
				clsPlantaValorizacion.setDireccion_iga(jsonObjectValorizacion.get("direccion_iga").toString());
			}
			else{
				clsPlantaValorizacion.setDireccion_iga("");
			}
			
			if (jsonObjectValorizacion.get("referencia_iga") != null && jsonObjectValorizacion.get("referencia_iga") != "") {
				clsPlantaValorizacion.setReferencia_iga(jsonObjectValorizacion.get("referencia_iga").toString());
			}	
			else{
				clsPlantaValorizacion.setReferencia_iga("");
			}
			
			if (jsonObjectValorizacion.get("cod_usuario") != null && jsonObjectValorizacion.get("cod_usuario") != "") {
				clsPlantaValorizacion.setCodigo_usuario(Integer.parseInt(jsonObjectValorizacion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
