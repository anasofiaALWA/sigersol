package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTransferencia;

public class TransferenciaDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(TransferenciaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_transferencia = "{call SIGERSOLBL.SGR_SP_LIST_SGR_TRANSFERENCIA(?,?)}";
	private final String sgr_sp_ins_transferencia = "{call SIGERSOLBL.SGR_SP_INS_TRANSFERENCIA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_transferencia = "{call SIGERSOLBL.SGR_SP_UPD_TRANSFERENCIA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsTransferencia clsTransferencia) {
		ClsResultado clsResultado = new ClsResultado();
		ClsTransferencia item = new ClsTransferencia();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_sgr_transferencia;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsTransferencia.getClsCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setTransferencia_id(rs.getInt("TRANSFERENCIA_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setClsCicloGestionResiduos(cicloGestionResiduos);
					item.setFlag_transferencia(rs.getInt("FLAG_TRANSFERENCIA"));
					item.setAdm_serv_id(rs.getString("ADM_SERV_ID"));
					item.setZona_coordenada(rs.getString("ZONA_COORDENADA"));
					item.setNorte_coordenada(rs.getString("NORTE_COORDENADA"));
					item.setEste_coordenada(rs.getString("ESTE_COORDENADA"));
					item.setDepartamento(rs.getString("DEPARTAMENTO"));
					item.setProvincia(rs.getString("PROVINCIA"));
					item.setDistrito(rs.getString("DISTRITO"));
					item.setDireccion_iga(rs.getString("DIRECCION_IGA"));
					item.setReferencia_iga(rs.getString("REFERENCIA_IGA"));
					item.setResiduos_transferidos(rs.getInt("RESIDUOS_TRANSFERIDOS"));
					item.setCosto_anual(rs.getInt("COSTO_ANUAL"));
					item.setFrecuencia_diaria(rs.getInt("FRECUENCIA_DIARIA"));
					item.setDistancia_promedio(rs.getInt("DISTANCIA_PROMEDIO"));
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsTransferencia clsTransferencia) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_transferencia;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsTransferencia.getClsCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsTransferencia.getFlag_transferencia());
			cs.setString(3, clsTransferencia.getAdm_serv_id());
			cs.setString(4, clsTransferencia.getZona_coordenada());
			cs.setString(5, clsTransferencia.getNorte_coordenada());
			cs.setString(6, clsTransferencia.getEste_coordenada());
			cs.setString(7, clsTransferencia.getDepartamento());
			cs.setString(8, clsTransferencia.getProvincia());
			cs.setString(9, clsTransferencia.getDistrito());
			cs.setString(10, clsTransferencia.getDireccion_iga());
			cs.setString(11, clsTransferencia.getReferencia_iga());
			cs.setInt(12, clsTransferencia.getResiduos_transferidos());
			cs.setInt(13, clsTransferencia.getCosto_anual());
			cs.setInt(14, clsTransferencia.getFrecuencia_diaria());
			cs.setInt(15, clsTransferencia.getDistancia_promedio());
			cs.setInt(16, clsTransferencia.getCodigo_usuario());
			cs.registerOutParameter(17, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(17);
			
			log.info("TransferenciaDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsTransferencia clsTransferencia) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_transferencia;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsTransferencia.getTransferencia_id());
			cs.setInt(2, clsTransferencia.getClsCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(3, clsTransferencia.getFlag_transferencia());
			cs.setString(4, clsTransferencia.getAdm_serv_id());
			cs.setString(5, clsTransferencia.getZona_coordenada());
			cs.setString(6, clsTransferencia.getNorte_coordenada());
			cs.setString(7, clsTransferencia.getEste_coordenada());
			cs.setString(8, clsTransferencia.getDepartamento());
			cs.setString(9, clsTransferencia.getProvincia());
			cs.setString(10, clsTransferencia.getDistrito());
			cs.setString(11, clsTransferencia.getDireccion_iga());
			cs.setString(12, clsTransferencia.getReferencia_iga());
			cs.setInt(13, clsTransferencia.getResiduos_transferidos());
			cs.setInt(14, clsTransferencia.getCosto_anual());
			cs.setInt(15, clsTransferencia.getFrecuencia_diaria());
			cs.setInt(16, clsTransferencia.getDistancia_promedio());
			cs.setInt(17, clsTransferencia.getCodigo_usuario());
			cs.registerOutParameter(18, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(18);

			log.info("TransferenciaDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
