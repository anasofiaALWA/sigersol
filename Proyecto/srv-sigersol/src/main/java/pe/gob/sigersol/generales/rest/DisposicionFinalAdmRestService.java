package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;
import pe.gob.sigersol.generales.service.DisposicionFinalAdmService;
import pe.gob.sigersol.generales.service.DisposicionFinalService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/disposicionAdm")
@RequestScoped
public class DisposicionFinalAdmRestService {

	private static Logger log = Logger.getLogger(DisposicionFinalRestService.class.getName());
	private ClsDisposicionFinalAdm clsDisposicionFinalAdm;
	private ClsSitioDisposicion clsSitioDisposicion;
	private DisposicionFinalAdmService disposicionFinalAdmService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		disposicionFinalAdmService = new DisposicionFinalAdmService();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener disposicion**");
			
			jsonEntradaDisposicion(paramJson);
			
			clsResultado = disposicionFinalAdmService.obtener(clsDisposicionFinalAdm);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"disposicion_adm\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		disposicionFinalAdmService = new DisposicionFinalAdmService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaDisposicion(paramJson);
			clsResultado = disposicionFinalAdmService.insertar(clsDisposicionFinalAdm);	
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		disposicionFinalAdmService = new DisposicionFinalAdmService();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaDisposicion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = disposicionFinalAdmService.actualizar(clsDisposicionFinalAdm);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de DisposicionFinal  
	 * 
	 * */
	private void jsonEntradaDisposicion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> DisposicionFinalRestService :	 jsonEntradaDisposicion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectGeneracion;
		JSONParser jsonParser = new JSONParser();
		
		clsDisposicionFinalAdm = new ClsDisposicionFinalAdm();
		clsSitioDisposicion = new ClsSitioDisposicion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectGeneracion = (JSONObject) jsonObjectPrincipal.get("DisposicionAdm");
			
			if (jsonObjectGeneracion.get("disposicion_final_id") != null && jsonObjectGeneracion.get("disposicion_final_id") != "") {
				clsDisposicionFinalAdm.setDisposicion_final_id(Integer.parseInt(jsonObjectGeneracion.get("disposicion_final_id").toString()));
			}
			
			if (jsonObjectGeneracion.get("selec_relleno") != null && jsonObjectGeneracion.get("selec_relleno") != "") {
				clsSitioDisposicion.setSitio_disposicion_id(Integer.parseInt(jsonObjectGeneracion.get("selec_relleno").toString()));
			} 
			else
				clsSitioDisposicion.setSitio_disposicion_id(-1);
			
			clsDisposicionFinalAdm.setSitio_disposicion_id(clsSitioDisposicion);
			
			if (jsonObjectGeneracion.get("anio_inicio") != null && jsonObjectGeneracion.get("anio_inicio") != "") {
				clsDisposicionFinalAdm.setAnio_inicio(Integer.parseInt(jsonObjectGeneracion.get("anio_inicio").toString()));
			}
			
			if (jsonObjectGeneracion.get("cant_personal") != null && jsonObjectGeneracion.get("cant_personal") != "") {
				clsDisposicionFinalAdm.setCant_personal(Integer.parseInt(jsonObjectGeneracion.get("cant_personal").toString()));
			}
			
			if (jsonObjectGeneracion.get("situacion_terreno_id") != null && jsonObjectGeneracion.get("situacion_terreno_id") != "") {
				clsDisposicionFinalAdm.setSituacion_terreno_id((jsonObjectGeneracion.get("situacion_terreno_id").toString()));
			}
			
			if (jsonObjectGeneracion.get("admin_servicio_id") != null && jsonObjectGeneracion.get("admin_servicio_id") != "") {
				clsDisposicionFinalAdm.setAdmin_servicio_id(jsonObjectGeneracion.get("admin_servicio_id").toString());
			}
			
			if (jsonObjectGeneracion.get("temperatura_zona") != null && jsonObjectGeneracion.get("temperatura_zona") != "") {
				clsDisposicionFinalAdm.setTemperatura_zona(Integer.parseInt(jsonObjectGeneracion.get("temperatura_zona").toString()));
			}
			
			if (jsonObjectGeneracion.get("direccion") != null && jsonObjectGeneracion.get("direccion") != "") {
				clsDisposicionFinalAdm.setDireccion(jsonObjectGeneracion.get("direccion").toString());
			}
			
			if (jsonObjectGeneracion.get("ref_direccion") != null && jsonObjectGeneracion.get("ref_direccion") != "") {
				clsDisposicionFinalAdm.setRef_direccion(jsonObjectGeneracion.get("ref_direccion").toString());
			}
			
			if (jsonObjectGeneracion.get("area_total_terreno") != null && jsonObjectGeneracion.get("area_total_terreno") != "") {
				clsDisposicionFinalAdm.setArea_total_terreno(Integer.parseInt(jsonObjectGeneracion.get("area_total_terreno").toString()));
			}
			
			if (jsonObjectGeneracion.get("area_utilizada_terreno") != null && jsonObjectGeneracion.get("area_utilizada_terreno") != "") {
				clsDisposicionFinalAdm.setArea_utilizada_terreno(Integer.parseInt(jsonObjectGeneracion.get("area_utilizada_terreno").toString()));
			}
			
			if (jsonObjectGeneracion.get("altura_nf") != null && jsonObjectGeneracion.get("altura_nf") != "") {
				clsDisposicionFinalAdm.setAltura_nf(Integer.parseInt(jsonObjectGeneracion.get("altura_nf").toString()));
			}
			
			if (jsonObjectGeneracion.get("altura_rs_ss") != null && jsonObjectGeneracion.get("altura_rs_ss") != "") {
				clsDisposicionFinalAdm.setAltura_rs_ss(jsonObjectGeneracion.get("altura_rs_ss").toString());
			}
			
			if (jsonObjectGeneracion.get("tipo_manejo_eq_id") != null && jsonObjectGeneracion.get("tipo_manejo_eq_id") != "") {
				clsDisposicionFinalAdm.setTipo_manejo_eq_id(jsonObjectGeneracion.get("tipo_manejo_eq_id").toString());
			}
			
			if (jsonObjectGeneracion.get("tipo_manejo_cob_id") != null && jsonObjectGeneracion.get("tipo_manejo_cob_id") != "") {
				clsDisposicionFinalAdm.setTipo_manejo_cob_id(jsonObjectGeneracion.get("tipo_manejo_cob_id").toString());
			}
			
			if (jsonObjectGeneracion.get("tipo_tecnologia_id") != null && jsonObjectGeneracion.get("tipo_tecnologia_id") != "") {
				clsDisposicionFinalAdm.setTipo_tecnologia_id(jsonObjectGeneracion.get("tipo_tecnologia_id").toString());
			}
			
			if (jsonObjectGeneracion.get("maneja_lixiviados") != null && jsonObjectGeneracion.get("maneja_lixiviados") != "") {
				clsDisposicionFinalAdm.setManeja_lixiviados(Integer.parseInt(jsonObjectGeneracion.get("maneja_lixiviados").toString()));
			}
			
			if (jsonObjectGeneracion.get("tipo_manejo_lixiviados_id") != null && jsonObjectGeneracion.get("tipo_manejo_lixiviados_id") != "") {
				clsDisposicionFinalAdm.setTipo_manejo_lixiviados_id(jsonObjectGeneracion.get("tipo_manejo_lixiviados_id").toString());
			}
			
			if (jsonObjectGeneracion.get("maneja_gases") != null && jsonObjectGeneracion.get("maneja_gases") != "") {
				clsDisposicionFinalAdm.setManeja_gases(Integer.parseInt(jsonObjectGeneracion.get("maneja_gases").toString()));
			}
			
			if (jsonObjectGeneracion.get("num_chimenea_inst") != null && jsonObjectGeneracion.get("num_chimenea_inst") != "") {
				clsDisposicionFinalAdm.setNum_chimenea_inst(Integer.parseInt(jsonObjectGeneracion.get("num_chimenea_inst").toString()));
			}
			
			if (jsonObjectGeneracion.get("num_chimenea_quem_op") != null && jsonObjectGeneracion.get("num_chimenea_quem_op") != "") {
				clsDisposicionFinalAdm.setNum_chimenea_quem_op(Integer.parseInt(jsonObjectGeneracion.get("num_chimenea_quem_op").toString()));
			}
			
			if (jsonObjectGeneracion.get("tipo_manejo_gases") != null && jsonObjectGeneracion.get("tipo_manejo_gases") != "") {
				clsDisposicionFinalAdm.setTipo_manejo_gases(jsonObjectGeneracion.get("tipo_manejo_gases").toString());
			}
			
			if (jsonObjectGeneracion.get("genera_ee") != null && jsonObjectGeneracion.get("genera_ee") != "") {
				clsDisposicionFinalAdm.setGenera_ee(Integer.parseInt(jsonObjectGeneracion.get("genera_ee").toString()));
			}
			
			if (jsonObjectGeneracion.get("cap_instalada_ee_mw") != null && jsonObjectGeneracion.get("cap_instalada_ee_mw") != "") {
				clsDisposicionFinalAdm.setCap_instalada_ee_mw(Integer.parseInt(jsonObjectGeneracion.get("cap_instalada_ee_mw").toString()));
			}
			
			if (jsonObjectGeneracion.get("generacion_ee_anual_mw") != null && jsonObjectGeneracion.get("generacion_ee_anual_mw") != "") {
				clsDisposicionFinalAdm.setGeneracion_ee_anual_mw(Integer.parseInt(jsonObjectGeneracion.get("generacion_ee_anual_mw").toString()));
			}
			
			if (jsonObjectGeneracion.get("cod_usuario") != null && jsonObjectGeneracion.get("cod_usuario") != "") {
				clsDisposicionFinalAdm.setCodigo_usuario(Integer.parseInt(jsonObjectGeneracion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
