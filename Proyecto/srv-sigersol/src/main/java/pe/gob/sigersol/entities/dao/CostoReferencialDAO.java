package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCostoReferencial;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduoSolido;

public class CostoReferencialDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(CostoReferencialDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_costo_referencial = "{call SIGERSOLBL.SGR_SP_INS_COSTO_REFERENCIAL(?,?,?,?,?)}";
	private final String sgr_sp_upd_costo_referencial = "{call SIGERSOLBL.SGR_SP_UPD_COSTO_REFERENCIAL(?,?,?,?,?)}";
	
	private final String sgr_sp_list_costo_referencial = "{call SIGERSOLBL.SGR_SP_LIST_COSTO_REFERENCIAL(?,?)}";  
	
	public ClsResultado listarCostoReferencial(ClsCostoReferencial clsCostoReferencial){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCostoReferencial> lista = new ArrayList<ClsCostoReferencial>();
		con = null;
		String sql = this.sgr_sp_list_costo_referencial;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCostoReferencial.getValorizacion().getValorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsCostoReferencial item = new ClsCostoReferencial();
				ClsTipoResiduoSolido tipoResiduoSolido = new ClsTipoResiduoSolido();
				try {
					tipoResiduoSolido.setTipo_residuo_id(rs.getInt("TIPO_RESIDUO_ID"));
					tipoResiduoSolido.setNombre(rs.getString("NOMBRE_TIPO_RESIDUO"));
					tipoResiduoSolido.setInformacion(rs.getString("INFORMACION"));
					item.setTipo_residuo(tipoResiduoSolido);
					if(clsCostoReferencial.getValorizacion().getValorizacion_id() == -1){
						item.setCosto(null);
					} else{
						item.setCosto(rs.getInt("COSTO"));
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCostoReferencial clsCostoReferencial) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_costo_referencial;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCostoReferencial.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsCostoReferencial.getTipo_residuo().getTipo_residuo_id());
			cs.setInt(3, clsCostoReferencial.getCosto());
			cs.setInt(4, clsCostoReferencial.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);
			
			log.info("CostoReferencialDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsCostoReferencial clsCostoReferencial) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_costo_referencial;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCostoReferencial.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsCostoReferencial.getTipo_residuo().getTipo_residuo_id());
			cs.setInt(3, clsCostoReferencial.getCosto());
			cs.setInt(4, clsCostoReferencial.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);

			log.info("CostoReferencialDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
