package pe.gob.sigersol.core.dao;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

class ServiceLocator implements Serializable {
	private InitialContext ic = null;
	private Map cache = null;
	private static ServiceLocator instance;

	private ServiceLocator() throws NamingException {
		try {
			this.ic = new InitialContext();
			this.cache = Collections.synchronizedMap(new HashMap());
		} catch (NamingException ne) {
			throw ne;
		}
	}

	private ServiceLocator(InitialContext iCtx) {
		this.ic = iCtx;
	}

	public static ServiceLocator getInstance() throws NamingException {
		if (instance == null) {
			instance = new ServiceLocator();
		}
		return instance;
	}

	public static InitialContext getInitialContext() throws NamingException {
		return new InitialContext();
	}

	public DataSource getDataSource(String dataSourceName) throws NamingException {
		DataSource dataSource = null;
		try {
			if (this.cache.containsKey(dataSourceName)) {
				dataSource = (DataSource) this.cache.get(dataSourceName);
			} else {
				dataSource = (DataSource) this.ic.lookup(dataSourceName);
				this.cache.put(dataSourceName, dataSource);
			}
		} catch (NamingException nex) {
			throw new NamingException();
		} catch (Exception ex) {
			throw new NamingException();
		}
		return dataSource;
	}
}