package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUsuarios_;
import pe.gob.sigersol.generales.service.UsuariosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/usuariosX")
@RequestScoped
public class UsuariosRestService {
	
	private static Logger log = Logger.getLogger(UsuariosRestService.class.getName());
	private Integer accion = null;
	private ClsUsuarios_ clsUsuarios;
	private UsuariosService usuariosService;

	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(@Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener solicitud");
			
			clsResultado = usuariosService.obtener();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"usuario\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		usuariosService = new UsuariosService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaSolicitud(paramJson);

			// if (accion == 1) {
			// clsResultado = academicoService.insertar(clsAcademico);
			// } else {
			// clsResultado = academicoService.actualizar(clsAcademico);
			// }
			//
			
			clsResultado = usuariosService.insertar(clsUsuarios);
			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(@FormParam("paramJson") String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaSolicitud(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = usuariosService.actualizar(clsUsuarios);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"usuario\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	
	private void jsonEntradaSolicitud(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectSolicitud;
		JSONParser jsonParser = new JSONParser();
		
		clsUsuarios = new ClsUsuarios_();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectSolicitud = (JSONObject) jsonObjectPrincipal.get("Usuarios");

			if (jsonObjectSolicitud.get("usuario_login") != null && jsonObjectSolicitud.get("usuario_login") != "") {
				clsUsuarios.setUsuario_login(jsonObjectSolicitud.get("usuario_login").toString());
			}
			if (jsonObjectSolicitud.get("clave_login") != null && jsonObjectSolicitud.get("clave_login") != "") {
				clsUsuarios.setUsuario_login(jsonObjectSolicitud.get("clave_login").toString());
			}
			if (jsonObjectSolicitud.get("llave_encrypt") != null && jsonObjectSolicitud.get("llave_encrypt") != "") {
				clsUsuarios.setUsuario_login(jsonObjectSolicitud.get("llave_encrypt").toString());
			}
			if (jsonObjectSolicitud.get("email") != null && jsonObjectSolicitud.get("email") != "") {
				clsUsuarios.setUsuario_login(jsonObjectSolicitud.get("email").toString());
			}
			if (jsonObjectSolicitud.get("cod_usuario") != null && jsonObjectSolicitud.get("cod_usuario") != "") {
				clsUsuarios.setCodigo_usuario(Integer.parseInt(jsonObjectSolicitud.get("cod_usuario").toString()));
			}
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
