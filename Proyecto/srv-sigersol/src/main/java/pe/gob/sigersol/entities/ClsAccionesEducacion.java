package pe.gob.sigersol.entities;

public class ClsAccionesEducacion {

	private Integer acciones_educacion_id;
	private ClsEducacionAmbiental educacion_ambiental;
	private ClsTipoEventos tipo_eventos;
	private Integer capacitad_hombres;
	private Integer capacitad_mujeres;
	private Integer codigo_usuario;
	
	public Integer getAcciones_educacion_id() {
		return acciones_educacion_id;
	}
	public void setAcciones_educacion_id(Integer acciones_educacion_id) {
		this.acciones_educacion_id = acciones_educacion_id;
	}
	public ClsEducacionAmbiental getEducacion_ambiental() {
		return educacion_ambiental;
	}
	public void setEducacion_ambiental(ClsEducacionAmbiental educacion_ambiental) {
		this.educacion_ambiental = educacion_ambiental;
	}
	public ClsTipoEventos getTipo_eventos() {
		return tipo_eventos;
	}
	public void setTipo_eventos(ClsTipoEventos tipo_eventos) {
		this.tipo_eventos = tipo_eventos;
	}
	public Integer getCapacitad_hombres() {
		return capacitad_hombres;
	}
	public void setCapacitad_hombres(Integer capacitad_hombres) {
		this.capacitad_hombres = capacitad_hombres;
	}
	public Integer getCapacitad_mujeres() {
		return capacitad_mujeres;
	}
	public void setCapacitad_mujeres(Integer capacitad_mujeres) {
		this.capacitad_mujeres = capacitad_mujeres;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
