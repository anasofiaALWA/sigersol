package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.entities.dao.ValorizacionDAO;

public class ValorizacionService {
	
	private static Logger log = Logger.getLogger(ValorizacionService.class);
	ValorizacionDAO valorizacionDAO = new ValorizacionDAO();
	
	public ClsResultado obtener(ClsValorizacion clsValorizacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacionDAO.obtener(clsValorizacion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsValorizacion clsValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacionDAO.insertar(clsValorizacion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsValorizacion clsValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacionDAO.actualizar(clsValorizacion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
