package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsRecDisposicionFinal;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsRecValorizacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoVehiculo;
import pe.gob.sigersol.entities.ClsVehiculoRecoleccion;
import pe.gob.sigersol.generales.service.VehiculoRecoleccion3Service;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase VehiculoRecoleccion3RestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de recoleccion permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/recoleccionVehicular3")
@RequestScoped
public class VehiculoRecoleccion3RestService {

	private static Logger log = Logger.getLogger(VehiculoRecoleccion3RestService.class.getName());
	private ClsVehiculoRecoleccion clsVehiculoRecoleccion;
	private ClsRecRcdOm clsRecRcdOm;
	private ClsTipoVehiculo clsTipoVehiculo;
	private VehiculoRecoleccion3Service vehiculoRecoleccion3Service;
	private List<ClsVehiculoRecoleccion> listaVehiculoRecoleccion;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarVehiculo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarVehiculoRecoleccion(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			vehiculoRecoleccion3Service = new VehiculoRecoleccion3Service();
			
			jsonEntradaListaVehiculos(paramJson);
			
			log.info("**Obtener Recoleccion Vehiculos");
			
			clsResultado = vehiculoRecoleccion3Service.listarVehiculoRecoleccion(clsVehiculoRecoleccion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstVehiculo3\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		vehiculoRecoleccion3Service = new VehiculoRecoleccion3Service();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaVehiculos(paramJson);

			for (ClsVehiculoRecoleccion clsVehiculoRecoleccion : listaVehiculoRecoleccion) {
				clsResultado = vehiculoRecoleccion3Service.insertar(clsVehiculoRecoleccion);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		vehiculoRecoleccion3Service = new VehiculoRecoleccion3Service();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaVehiculos(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsVehiculoRecoleccion clsVehiculoRecoleccion : listaVehiculoRecoleccion) {
			clsResultado = vehiculoRecoleccion3Service.actualizar(clsVehiculoRecoleccion);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaVehiculos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> VehiculoRecoleccion2RestService :	 jsonEntradaVehiculos" + jsonEntrada);

		JSONObject jsonObjectTipoVehiculo;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaVehiculoRecoleccion = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsVehiculoRecoleccion = new ClsVehiculoRecoleccion();
			clsTipoVehiculo = new ClsTipoVehiculo();
			clsRecRcdOm = new ClsRecRcdOm();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("rec_rcd_om_id") != null && innerObject.get("rec_rcd_om_id").toString() != "") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(innerObject.get("rec_rcd_om_id").toString()));
			}
			else {
				clsRecRcdOm.setRec_rcd_om_id(0);
			}
			
			clsVehiculoRecoleccion.setRecRcdOm(clsRecRcdOm);
			
			if (innerObject.get("tipo_vehiculo") != null && innerObject.get("tipo_vehiculo").toString() != "") {
				jsonObjectTipoVehiculo  = (JSONObject) innerObject.get("tipo_vehiculo");
				clsTipoVehiculo.setTipo_vehiculo_id(Integer.parseInt(jsonObjectTipoVehiculo.get("tipo_vehiculo_id").toString()));
			}
			clsVehiculoRecoleccion.setTipo_vehiculo(clsTipoVehiculo);
			
			if (innerObject.get("capacidad") != null && innerObject.get("capacidad").toString() != "") {
				clsVehiculoRecoleccion.setCapacidad(Double.valueOf(innerObject.get("capacidad").toString()));
			} else{
				clsVehiculoRecoleccion.setCapacidad(0.0);
			}
			
			if (innerObject.get("antig_promedio") != null && innerObject.get("antig_promedio").toString() != "") {
				clsVehiculoRecoleccion.setAntig_promedio(Integer.parseInt(innerObject.get("antig_promedio").toString()));
			} else{
				clsVehiculoRecoleccion.setAntig_promedio(0);
			}
			
			if (innerObject.get("recoleccion_anual") != null && innerObject.get("recoleccion_anual").toString() != "") {
				clsVehiculoRecoleccion.setRecoleccion_anual(Double.valueOf(innerObject.get("recoleccion_anual").toString()));
			} else{
				clsVehiculoRecoleccion.setRecoleccion_anual(0.0);
			}
			
			if (innerObject.get("numero_unidades") != null && innerObject.get("numero_unidades").toString() != "") {
				clsVehiculoRecoleccion.setNumero_unidades(Integer.parseInt(innerObject.get("numero_unidades").toString()));
			} else{
				clsVehiculoRecoleccion.setNumero_unidades(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsVehiculoRecoleccion.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaVehiculoRecoleccion.add(clsVehiculoRecoleccion);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaVehiculos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> VehiculoRecoleccion2RestService :	jsonEntradaVehiculos" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectVehiculos;
		JSONParser jsonParser = new JSONParser();
		
		clsVehiculoRecoleccion = new ClsVehiculoRecoleccion();
		clsTipoVehiculo = new ClsTipoVehiculo();
		clsRecRcdOm = new ClsRecRcdOm(); 
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectVehiculos = (JSONObject) jsonObjectPrincipal.get("RecoleccionVehiculos3");
			
			
			if (jsonObjectVehiculos.get("rec_rcd_om_id") != null && jsonObjectVehiculos.get("rec_rcd_om_id") != "" &&
					jsonObjectVehiculos.get("rec_rcd_om_id") != " ") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(jsonObjectVehiculos.get("rec_rcd_om_id").toString()));
			}
			else
				clsRecRcdOm.setRec_rcd_om_id(-1);
			
			clsVehiculoRecoleccion.setRecRcdOm(clsRecRcdOm);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
