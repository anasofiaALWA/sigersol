package pe.gob.sigersol.entities;

public class ClsTipoInversion {

	private Integer tipo_inversion_id;
	private String tipo_inversion;
	private String proceso;
	
	
	public Integer getTipo_inversion_id() {
		return tipo_inversion_id;
	}
	public void setTipo_inversion_id(Integer tipo_inversion_id) {
		this.tipo_inversion_id = tipo_inversion_id;
	}
	public String getTipo_inversion() {
		return tipo_inversion;
	}
	public void setTipo_inversion(String tipo_inversion) {
		this.tipo_inversion = tipo_inversion;
	}
	public String getProceso() {
		return proceso;
	}
	public void setProceso(String proceso) {
		this.proceso = proceso;
	}
}
