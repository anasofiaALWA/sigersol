package pe.gob.sigersol.entities;

public class ClsUniformeEquipo {

	private Integer uniforme_equipo_id;
	private String tipo_uniforme;
	
	public Integer getUniforme_equipo_id() {
		return uniforme_equipo_id;
	}
	public void setUniforme_equipo_id(Integer uniforme_equipo_id) {
		this.uniforme_equipo_id = uniforme_equipo_id;
	}
	public String getTipo_uniforme() {
		return tipo_uniforme;
	}
	public void setTipo_uniforme(String tipo_uniforme) {
		this.tipo_uniforme = tipo_uniforme;
	}
	
}
