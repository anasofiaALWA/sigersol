package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsInfoCeldas;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.InfoCeldasDAO;

public class InfoCeldasService {

	private static Logger log = Logger.getLogger(InfoCeldasService.class);
	InfoCeldasDAO infoCeldasDAO = new InfoCeldasDAO();
	
	public ClsResultado listarCeldas(ClsInfoCeldas clsInfoCeldas){
		ClsResultado clsResultado = null;
		try {
			clsResultado = infoCeldasDAO.listarCeldas(clsInfoCeldas);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsInfoCeldas clsInfoCeldas) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = infoCeldasDAO.insertar(clsInfoCeldas);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsInfoCeldas clsInfoCeldas) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = infoCeldasDAO.actualizar(clsInfoCeldas);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
