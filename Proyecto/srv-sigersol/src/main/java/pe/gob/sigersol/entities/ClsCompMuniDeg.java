package pe.gob.sigersol.entities;

public class ClsCompMuniDeg {

	private Integer comp_muni_deg_id;
	private ClsAreasDegradadas areasDegradadas;
	private ClsResiduosMunicipales residuosMunicipales;
	private Integer porcentaje;
	private Integer codigo_usuario;
	
	public Integer getComp_muni_deg_id() {
		return comp_muni_deg_id;
	}
	public void setComp_muni_deg_id(Integer comp_muni_deg_id) {
		this.comp_muni_deg_id = comp_muni_deg_id;
	}
	public ClsAreasDegradadas getAreasDegradadas() {
		return areasDegradadas;
	}
	public void setAreasDegradadas(ClsAreasDegradadas areasDegradadas) {
		this.areasDegradadas = areasDegradadas;
	}
	public ClsResiduosMunicipales getResiduosMunicipales() {
		return residuosMunicipales;
	}
	public void setResiduosMunicipales(ClsResiduosMunicipales residuosMunicipales) {
		this.residuosMunicipales = residuosMunicipales;
	}
	public Integer getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Integer porcentaje) {
		this.porcentaje = porcentaje;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
