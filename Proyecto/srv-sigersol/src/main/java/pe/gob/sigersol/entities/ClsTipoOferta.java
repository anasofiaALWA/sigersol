package pe.gob.sigersol.entities;

public class ClsTipoOferta {

	private Integer tipo_oferta_id;
	private String tipo_barrido;
	private String informacion;
	
	public Integer getTipo_oferta_id() {
		return tipo_oferta_id;
	}
	public void setTipo_oferta_id(Integer tipo_oferta_id) {
		this.tipo_oferta_id = tipo_oferta_id;
	}
	public String getTipo_barrido() {
		return tipo_barrido;
	}
	public void setTipo_barrido(String tipo_barrido) {
		this.tipo_barrido = tipo_barrido;
	}
	public String getInformacion() {
		return informacion;
	}
	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}		
}
