package pe.gob.sigersol.core.ldap;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.jboss.logging.Logger;

import pe.gob.sigersol.util.ApplicationProperties;

public class LdapAuth {

	private static final Logger log = Logger.getLogger(LdapAuth.class);

	private boolean autenticado = false;
	protected static String server = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.SERVER_LDAP);// "ldap://172.16.19.93";
	protected static String port = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.PORT_LDAP);// "389";
	protected static String tipoAuth = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.TIPO_AUTH);// "simple";
	protected static String dn = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.BASE_DN);// "OU=usuarios,OU=cuentas,DC=testdom,DC=pruebas,DC=com";
	protected static String dnp = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.BASE_DN_PRINCIPAL);// "CN=Administrador,CN=Users,DC=testdom,DC=pruebas,DC=com";
	protected static String passp = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.CREDENCIAL_PRINCIPAL);// "servir2018$$$";
	protected static String attributeForUser = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.ATRIBUTE_FOR_USER);// "sAMAccountName";

	/**
	 * Constructor de la conexion con el Motor de LDAP
	 */

	public LdapAuth() {

	}

	public boolean validarAcceso(String usuario, String clave) {
		
		String strNombreUsuario = buscarNombreUsuario(usuario);
		// log.info("->" + str1 + "=" + usuario + "," + dn);
		String returnedAtts[] = { "distinguishedName", "cn", "name", "uid", "sn", "givenname", "memberOf",
				"samaccountname", "userPrincipalName", "displayName", "sn", "givenName" };

		String searchFilter = "(&(objectClass=user)(" + attributeForUser + "=" + usuario + "))";
		
		SearchControls searchCtls = new SearchControls();
		searchCtls.setReturningAttributes(returnedAtts);
		// Specify the search scope

		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String searchBase = dn;
		Hashtable environment = new Hashtable();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		// Using starndard Port, check your instalation

		environment.put(Context.PROVIDER_URL, server + ":" + port);
		environment.put(Context.SECURITY_AUTHENTICATION, tipoAuth);
		environment.put(Context.SECURITY_PRINCIPAL, strNombreUsuario.trim());

		environment.put(Context.SECURITY_CREDENTIALS, clave);
		LdapContext ctxGC = null;
		try {
			ctxGC = new InitialLdapContext(environment, null);
			// Search for objects in the GC using the filter

			NamingEnumeration answer = ctxGC.search(searchBase, searchFilter, searchCtls);
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					this.autenticado = true;
				}
			}

		} catch (NamingException e) {
			log.error("al autenticar Just reporting error");
			e.printStackTrace();
			autenticado = false;
		}
		return this.autenticado;
	}

	public String buscarNombreUsuario(String username) {
		String nombreUsuario = "";
		String returnedAtts[] = { "distinguishedName", "cn", "name", "uid", "sn", "givenname", "memberOf",
				"samaccountname", "userPrincipalName", "displayName", "sn", "givenName", "distinguishedName" };
		// definiciones de busqueda
		String searchFilter = "(&(objectClass=user)(" + attributeForUser + "=" + username + "))";
		// Create the search controls

		SearchControls searchCtls = new SearchControls();
		searchCtls.setReturningAttributes(returnedAtts);
		// Specify the search scope

		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String searchBase = dn;
		Hashtable environment = new Hashtable();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		environment.put(Context.PROVIDER_URL, server + ":" + port);
		environment.put(Context.SECURITY_AUTHENTICATION, tipoAuth);
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		environment.put(Context.SECURITY_PRINCIPAL, dnp);	
		environment.put(Context.SECURITY_CREDENTIALS, passp);

		LdapContext ctxGC = null;
		try {
			ctxGC = new InitialLdapContext(environment, null);
			// Search for objects in the GC using the filter

			NamingEnumeration answer = ctxGC.search(searchBase, searchFilter, searchCtls);
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						log.info("valor" + obtenerValor(obtenerAttributo(attrs, "distinguishedName")));
						nombreUsuario = obtenerValor(obtenerAttributo(attrs, "distinguishedName"));
					} catch (Exception e) {

						nombreUsuario = "";
					}
				}
			}

		} catch (NamingException e) {
			log.error("al buscar nombre, Just reporting error");
			e.printStackTrace();
		}
		return nombreUsuario;
	}

	
	private String obtenerAttributo(Attributes attrs, String valor) {
		String str = "";
		try {
			str = attrs.get(valor).toString();
		} catch (Exception e) {
			str = "";
		}
		return str;
	}

	private String obtenerValor(String str) {
		String strValor = "";
		try {
			strValor = str.split(":")[1];
		} catch (Exception e) {
			strValor = "";
		}
		return strValor;
	}

}