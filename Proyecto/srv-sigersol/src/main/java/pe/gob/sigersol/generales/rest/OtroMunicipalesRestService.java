package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsOtroMunicipales;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.OtroMunicipalesService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/otroMunicipalesX")
@RequestScoped
public class OtroMunicipalesRestService {

	private static Logger log = Logger.getLogger(OtroMunicipalesRestService.class.getName());
	private ClsOtroMunicipales clsOtroMunicipales;
	private ClsRecRcdOm clsRecRcdOm;
	private OtroMunicipalesService otroMunicipalesService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			otroMunicipalesService = new OtroMunicipalesService();
			
			jsonEntradaOtroMunicipales(paramJson);
			
			log.info("**obtener Otro Municipales**");
			
			clsResultado = otroMunicipalesService.obtener(clsOtroMunicipales);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"otroMunicipales\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		otroMunicipalesService = new OtroMunicipalesService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaOtroMunicipales(paramJson);
			clsResultado = otroMunicipalesService.insertar(clsOtroMunicipales);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		otroMunicipalesService = new OtroMunicipalesService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaOtroMunicipales(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = otroMunicipalesService.actualizar(clsOtroMunicipales);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de RecRcdOm
	 * 
	 * */
	private void jsonEntradaOtroMunicipales(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> OtroMunicipalesRestService :jsonEntradaOtroMunicipales" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectOtroMunicipales;
		JSONParser jsonParser = new JSONParser();
		
		clsOtroMunicipales = new ClsOtroMunicipales();
		clsRecRcdOm = new ClsRecRcdOm();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectOtroMunicipales = (JSONObject) jsonObjectPrincipal.get("OtroMunicipales");
			
			if (jsonObjectOtroMunicipales.get("otro_municipales_id") != null && jsonObjectOtroMunicipales.get("otro_municipales_id") != "") {
				clsOtroMunicipales.setOtro_municipales_id(Integer.parseInt(jsonObjectOtroMunicipales.get("otro_municipales_id").toString()));
			} else {
				clsOtroMunicipales.setOtro_municipales_id(-1);
			}
			
			if (jsonObjectOtroMunicipales.get("rec_rcd_om_id") != null && jsonObjectOtroMunicipales.get("rec_rcd_om_id") != "") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(jsonObjectOtroMunicipales.get("rec_rcd_om_id").toString()));
			} else {
				clsRecRcdOm.setRec_rcd_om_id(-1);
			}
			
			clsOtroMunicipales.setRecRcdOm(clsRecRcdOm);
			
			if (jsonObjectOtroMunicipales.get("flag_municipal") != null && jsonObjectOtroMunicipales.get("flag_municipal") != "") {
				clsOtroMunicipales.setFlag_municipal(Integer.parseInt(jsonObjectOtroMunicipales.get("flag_municipal").toString()));
			}
			
			if (jsonObjectOtroMunicipales.get("adm_serv_id") != null && jsonObjectOtroMunicipales.get("adm_serv_id") != "") {
				clsOtroMunicipales.setAdm_serv_id(Integer.parseInt(jsonObjectOtroMunicipales.get("adm_serv_id").toString()));
			}
			
			if (jsonObjectOtroMunicipales.get("costo") != null && jsonObjectOtroMunicipales.get("costo") != "") {
				clsOtroMunicipales.setCosto(Integer.parseInt(jsonObjectOtroMunicipales.get("costo").toString()));
			}
			
			if (jsonObjectOtroMunicipales.get("descrip_residuos_otros") != null && jsonObjectOtroMunicipales.get("descrip_residuos_otros") != "") {
				clsOtroMunicipales.setDescrip_residuos_otros(jsonObjectOtroMunicipales.get("descrip_residuos_otros").toString());
			}
			else {
				clsOtroMunicipales.setDescrip_residuos_otros("");
			}
			
			if (jsonObjectOtroMunicipales.get("usuario_atendido_otros") != null && jsonObjectOtroMunicipales.get("usuario_atendido_otros") != "") {
				clsOtroMunicipales.setUsuario_atendido_otros(Integer.parseInt(jsonObjectOtroMunicipales.get("usuario_atendido_otros").toString()));
			}
			
			if (jsonObjectOtroMunicipales.get("cantidad_otros") != null && jsonObjectOtroMunicipales.get("cantidad_otros") != "") {
				clsOtroMunicipales.setCantidad_otros(Integer.parseInt(jsonObjectOtroMunicipales.get("cantidad_otros").toString()));
			}
			
			if (jsonObjectOtroMunicipales.get("cod_usuario") != null && jsonObjectOtroMunicipales.get("cod_usuario") != "") {
				clsOtroMunicipales.setCodigo_usuario(Integer.parseInt(jsonObjectOtroMunicipales.get("cod_usuario").toString()));
			}
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
