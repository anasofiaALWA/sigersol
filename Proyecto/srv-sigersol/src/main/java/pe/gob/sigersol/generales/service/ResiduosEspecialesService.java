package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResiduosEspeciales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ResiduosEspecialesDAO;

public class ResiduosEspecialesService {

	private static Logger log = Logger.getLogger(ResiduosEspecialesService.class);
	ResiduosEspecialesDAO residuosEspecialesDAO = new ResiduosEspecialesDAO();
	
	public ClsResultado obtener(){
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosEspecialesDAO.obtener();
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado listarResiduosEspeciales(ClsResiduosEspeciales clsResiduosEspeciales){
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosEspecialesDAO.listarResiduosEspeciales(clsResiduosEspeciales);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsResiduosEspeciales clsResiduosEspeciales) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosEspecialesDAO.insertar(clsResiduosEspeciales);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsResiduosEspeciales clsResiduosEspeciales) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosEspecialesDAO.actualizar(clsResiduosEspeciales);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
