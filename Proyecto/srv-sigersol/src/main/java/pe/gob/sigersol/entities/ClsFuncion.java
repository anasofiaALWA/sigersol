package pe.gob.sigersol.entities;

import com.google.gson.Gson;

public class ClsFuncion {
	private Integer funcion_id;
	private String funcion_nombre;
	private Integer codigo_estado;
	private Integer codigo_padre;
	private Integer nivel;
	private Integer orden;

	public Integer getCodigo_padre() {
		return codigo_padre;
	}

	public void setCodigo_padre(Integer codigo_padre) {
		this.codigo_padre = codigo_padre;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Integer getFuncion_id() {
		return funcion_id;
	}

	public void setFuncion_id(Integer funcion_id) {
		this.funcion_id = funcion_id;
	}

	public String getFuncion_nombre() {
		return funcion_nombre;
	}

	public void setFuncion_nombre(String funcion_nombre) {
		this.funcion_nombre = funcion_nombre;
	}

	public Integer getCodigo_estado() {
		return codigo_estado;
	}

	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
