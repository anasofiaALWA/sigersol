package pe.gob.sigersol.entities;

public class ClsCeldas {

	private Integer celdas_id;
	private String parametro;
	
	public Integer getCeldas_id() {
		return celdas_id;
	}
	public void setCeldas_id(Integer celdas_id) {
		this.celdas_id = celdas_id;
	}
	public String getParametro() {
		return parametro;
	}
	public void setParametro(String parametro) {
		this.parametro = parametro;
	}
	
}
