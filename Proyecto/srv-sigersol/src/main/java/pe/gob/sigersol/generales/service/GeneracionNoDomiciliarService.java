package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsGeneracionNoDomiciliar;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.GeneracionNoDomiciliarDAO;


public class GeneracionNoDomiciliarService {
	
	private static Logger log = Logger.getLogger(GeneracionNoDomiciliarService.class);
	GeneracionNoDomiciliarDAO generacionNoDomiciliarDAO = new GeneracionNoDomiciliarDAO();
	
	public ClsResultado obtener(){
		ClsResultado clsResultado = null;
		try {
			clsResultado = generacionNoDomiciliarDAO.obtener();
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado listarGND(ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar){
		ClsResultado clsResultado = null;
		try {
			clsResultado = generacionNoDomiciliarDAO.listarGND(clsGeneracionNoDomiciliar);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = generacionNoDomiciliarDAO.insertar(clsGeneracionNoDomiciliar);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsGeneracionNoDomiciliar clsGeneracionNoDomiciliar) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = generacionNoDomiciliarDAO.actualizar(clsGeneracionNoDomiciliar);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
