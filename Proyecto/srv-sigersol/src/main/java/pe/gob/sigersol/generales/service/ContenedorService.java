package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsContenedor;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ContenedorDAO;


public class ContenedorService {
	
	private static Logger log = Logger.getLogger(ContenedorService.class);
	ContenedorDAO contenedorDAO = new ContenedorDAO();
	
	public ClsResultado listarContenedor(ClsContenedor clsContenedor){
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = contenedorDAO.listarContenedor(clsContenedor);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsContenedor clsContenedor) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = contenedorDAO.insertar(clsContenedor);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsContenedor clsContenedor) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = contenedorDAO.actualizar(clsContenedor);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
