package pe.gob.sigersol.entities;

import java.sql.Timestamp;

import com.google.gson.Gson;

public class ClsFuncionPerfil {
	private Integer funcion_perfil_id;
	private ClsFuncion funcion;
	private ClsPerfil perfil;
	private Integer codigo_estado;
	private Integer insertar;
	private Integer consultar;
	private Integer actualizar;
	private Integer eliminar;
	private Integer exportar;
	private String usuario_creacion;
	private Timestamp fecha_creacion;
	private String usuario_modificacion;
	private Timestamp fecha_modificacion;

	public Integer getFuncion_perfil_id() {
		return funcion_perfil_id;
	}

	public void setFuncion_perfil_id(Integer funcion_perfil_id) {
		this.funcion_perfil_id = funcion_perfil_id;
	}

	public ClsPerfil getPerfil() {
		return perfil;
	}

	public void setPerfil(ClsPerfil perfil) {
		this.perfil = perfil;
	}

	public Integer getCodigo_estado() {
		return codigo_estado;
	}

	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}

	public Integer getInsertar() {
		return insertar;
	}

	public void setInsertar(Integer insertar) {
		this.insertar = insertar;
	}

	public Integer getConsultar() {
		return consultar;
	}

	public void setConsultar(Integer consultar) {
		this.consultar = consultar;
	}

	public Integer getActualizar() {
		return actualizar;
	}

	public void setActualizar(Integer actualizar) {
		this.actualizar = actualizar;
	}

	public Integer getEliminar() {
		return eliminar;
	}

	public void setEliminar(Integer eliminar) {
		this.eliminar = eliminar;
	}

	public Integer getExportar() {
		return exportar;
	}

	public void setExportar(Integer exportar) {
		this.exportar = exportar;
	}

	public ClsFuncion getFuncion() {
		return funcion;
	}

	public void setFuncion(ClsFuncion funcion) {
		this.funcion = funcion;
	}

	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}

	public Timestamp getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Timestamp fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}

	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}

	public Timestamp getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Timestamp fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
