package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCentroAcopio;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.CentroAcopioService;
import pe.gob.sigersol.generales.service.ValorizacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/centroAcopioX")
@RequestScoped
public class CentroAcopioRestService {

	private static Logger log = Logger.getLogger(CentroAcopioRestService.class.getName());
	private ClsCentroAcopio clsCentroAcopio;
	private ClsValorizacion clsValorizacion;
	private CentroAcopioService centroAcopioService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		centroAcopioService = new CentroAcopioService();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener Centro de Acopio**");		
			jsonEntradaValorizacion(paramJson);	
			clsResultado = centroAcopioService.obtener(clsCentroAcopio);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"acopio\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		centroAcopioService = new CentroAcopioService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaValorizacion(paramJson);
			clsResultado = centroAcopioService.insertar(clsCentroAcopio);			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		centroAcopioService = new CentroAcopioService();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaValorizacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = centroAcopioService.actualizar(clsCentroAcopio);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Valorización  
	 * 
	 * */
	private void jsonEntradaValorizacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ValorizacionRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectValorizacion;
		JSONParser jsonParser = new JSONParser();
		
		clsCentroAcopio = new ClsCentroAcopio();
		clsValorizacion = new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectValorizacion = (JSONObject) jsonObjectPrincipal.get("Acopio");
			
			if (jsonObjectValorizacion.get("centro_acopio_id") != null && jsonObjectValorizacion.get("centro_acopio_id") != "") {
				clsCentroAcopio.setCentro_acopio_id(Integer.parseInt(jsonObjectValorizacion.get("centro_acopio_id").toString()));
			}
			
			if (jsonObjectValorizacion.get("valorizacion_id") != null && jsonObjectValorizacion.get("valorizacion_id") != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectValorizacion.get("valorizacion_id").toString()));
			} 
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsCentroAcopio.setValorizacion(clsValorizacion);
			
			if (jsonObjectValorizacion.get("adm_propia") != null && jsonObjectValorizacion.get("adm_propia") != "") {
				clsCentroAcopio.setAdm_propia(Integer.parseInt(jsonObjectValorizacion.get("adm_propia").toString()));
			}
			
			if (jsonObjectValorizacion.get("tercerizado_eor") != null && jsonObjectValorizacion.get("tercerizado_eor") != "") {
				clsCentroAcopio.setTercerizado_eor(Integer.parseInt(jsonObjectValorizacion.get("tercerizado_eor").toString()));
			}
			
			if (jsonObjectValorizacion.get("tercerizado_asoc_recic") != null && jsonObjectValorizacion.get("tercerizado_asoc_recic") != "") {
				clsCentroAcopio.setTercerizado_asoc_recic(Integer.parseInt(jsonObjectValorizacion.get("tercerizado_asoc_recic").toString()));
			}
			
			if (jsonObjectValorizacion.get("mixto") != null && jsonObjectValorizacion.get("mixto") != "") {
				clsCentroAcopio.setMixto(Integer.parseInt(jsonObjectValorizacion.get("mixto").toString()));
			}

			if (jsonObjectValorizacion.get("zona_coordenada") != null && jsonObjectValorizacion.get("zona_coordenada") != "") {
				clsCentroAcopio.setZona_coordenada(jsonObjectValorizacion.get("zona_coordenada").toString());
			}
			else {
				clsCentroAcopio.setZona_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("norte_coordenada") != null && jsonObjectValorizacion.get("norte_coordenada") != "") {
				clsCentroAcopio.setNorte_coordenada(jsonObjectValorizacion.get("norte_coordenada").toString());
			}
			else{
				clsCentroAcopio.setNorte_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("sur_coordenada") != null && jsonObjectValorizacion.get("sur_coordenada") != "") {
				clsCentroAcopio.setSur_coordenada(jsonObjectValorizacion.get("sur_coordenada").toString());
			}
			else{
				clsCentroAcopio.setSur_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("division_politica") != null && jsonObjectValorizacion.get("division_politica") != "") {
				clsCentroAcopio.setDivision_politica(jsonObjectValorizacion.get("division_politica").toString());
			}
			else{
				clsCentroAcopio.setDivision_politica("");
			}
			if (jsonObjectValorizacion.get("direccion_iga") != null && jsonObjectValorizacion.get("direccion_iga") != "") {
				clsCentroAcopio.setDireccion_iga(jsonObjectValorizacion.get("direccion_iga").toString());
			}
			else{
				clsCentroAcopio.setDireccion_iga("");
			}
			
			if (jsonObjectValorizacion.get("referencia_iga") != null && jsonObjectValorizacion.get("referencia_iga") != "") {
				clsCentroAcopio.setReferencia_iga(jsonObjectValorizacion.get("referencia_iga").toString());
			}	
			else{
				clsCentroAcopio.setReferencia_iga("");
			}
			if (jsonObjectValorizacion.get("numero_recicladores") != null && jsonObjectValorizacion.get("numero_recicladores") != "") {
				clsCentroAcopio.setNumero_recicladores(Integer.parseInt(jsonObjectValorizacion.get("numero_recicladores").toString()));
			}
			
			if (jsonObjectValorizacion.get("recic_hombres_25") != null && jsonObjectValorizacion.get("recic_hombres_25") != "") {
				clsCentroAcopio.setRecic_hombres_25(Integer.parseInt(jsonObjectValorizacion.get("recic_hombres_25").toString()));
			}
			
			if (jsonObjectValorizacion.get("recic_hombres_25_65") != null && jsonObjectValorizacion.get("recic_hombres_25_65") != "") {
				clsCentroAcopio.setRecic_hombres_25_65(Integer.parseInt(jsonObjectValorizacion.get("recic_hombres_25_65").toString()));
			}
		
			if (jsonObjectValorizacion.get("recic_hombres_65") != null && jsonObjectValorizacion.get("recic_hombres_65") != "") {
				clsCentroAcopio.setRecic_hombres_65(Integer.parseInt(jsonObjectValorizacion.get("recic_hombres_65").toString()));
			}
			
			if (jsonObjectValorizacion.get("recic_mujeres_25") != null && jsonObjectValorizacion.get("recic_mujeres_25") != "") {
				clsCentroAcopio.setRecic_mujeres_25(Integer.parseInt(jsonObjectValorizacion.get("recic_mujeres_25").toString()));
			}
			
			if (jsonObjectValorizacion.get("recic_mujeres_25_65") != null && jsonObjectValorizacion.get("recic_mujeres_25_65") != "") {
				clsCentroAcopio.setRecic_mujeres_25_65(Integer.parseInt(jsonObjectValorizacion.get("recic_mujeres_25_65").toString()));
			}
		
			if (jsonObjectValorizacion.get("recic_mujeres_65") != null && jsonObjectValorizacion.get("recic_mujeres_65") != "") {
				clsCentroAcopio.setRecic_mujeres_65(Integer.parseInt(jsonObjectValorizacion.get("recic_mujeres_65").toString()));
			}
			
			if (jsonObjectValorizacion.get("cod_usuario") != null && jsonObjectValorizacion.get("cod_usuario") != "") {
				clsCentroAcopio.setCodigo_usuario(Integer.parseInt(jsonObjectValorizacion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
