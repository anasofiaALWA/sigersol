package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsMuniDispDeg;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.MuniDispDegDAO;

public class MuniDispDegService {

	private static Logger log = Logger.getLogger(MuniDispDegService.class);
	MuniDispDegDAO muniDispDegDAO = new MuniDispDegDAO();
	
	public ClsResultado listarMuniDisp(ClsMuniDispDeg clsMuniDispDeg){
		ClsResultado clsResultado = null;
		try {
			clsResultado = muniDispDegDAO.listarMuniDisp(clsMuniDispDeg);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsMuniDispDeg clsMuniDispDeg) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = muniDispDegDAO.insertar(clsMuniDispDeg);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsMuniDispDeg clsMuniDispDeg) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = muniDispDegDAO.actualizar(clsMuniDispDeg);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
