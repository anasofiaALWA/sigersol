package pe.gob.sigersol.util;

import org.jboss.logging.Logger;

/**
 * @author lflorian
 */

public class FuncionesBase {
	private static final Logger log = Logger.getLogger(FuncionesBase.class);
	
	public static String uploadDir = System.getProperty("jboss.server.base.dir");
	
	public static String dirDeployed() {
		return "/deployments/" + FuncionesBase.darNombreApp() + ".war";
	}

	public static String darNombreApp() {
		return ApplicationProperties.getProperty(ApplicationProperties.ApplicationPropertiesEnum.NOMBRE_APLICACION);
	}



}