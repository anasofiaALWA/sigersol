package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsLugarAprovechamiento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;

public class LugarAprovechamientoDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(LugarAprovechamientoDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_aprovechamiento = "{call SIGERSOLBL.SGR_SP_LIST_APROVECHAMIENTO(?,?)}";
	private final String sgr_sp_ins_aprovechamiento = "{call SIGERSOLBL.SGR_SP_INS_APROVECHAMIENTO(?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_aprovechamiento = "{call SIGERSOLBL.SGR_SP_UPD_APROVECHAMIENTO(?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_aprovechamiento3 = "{call SIGERSOLBL.SGR_SP_UPD_APROVECHAMIENTO3(?,?,?,?)}";
	
	public ClsResultado obtener(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		ClsResultado clsResultado = new ClsResultado();
		ClsLugarAprovechamiento item = new ClsLugarAprovechamiento();
		ClsValorizacion clsValorizacion = new ClsValorizacion();
		con = null;
		String sql = this.sgr_sp_list_aprovechamiento;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsLugarAprovechamiento.getValorizacion().getValorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setLugar_aprovechamiento_id(rs.getInt("LUGAR_APROVECHAMIENTO_ID"));
					clsValorizacion.setValorizacion_id(rs.getInt("VALORIZACION_ID"));
					item.setValorizacion(clsValorizacion);
					item.setAdm_propia(rs.getInt("ADM_PROPIA"));
					item.setTercerizado_eor(rs.getInt("TERCERIZADO_EOR"));
					item.setTercerizado_asoc_recic(rs.getInt("TERCERIZADO_ASOC_RECIC"));
					item.setMixto(rs.getInt("MIXTO"));
					item.setNumero_recicladores(rs.getInt("NUMERO_RECICLADORES"));
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_aprovechamiento;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsLugarAprovechamiento.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsLugarAprovechamiento.getAdm_propia());
			cs.setInt(3, clsLugarAprovechamiento.getTercerizado_eor());
			cs.setInt(4, clsLugarAprovechamiento.getTercerizado_asoc_recic());
			cs.setInt(5, clsLugarAprovechamiento.getMixto());
			cs.setInt(6, clsLugarAprovechamiento.getCodigo_usuario());
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(7);
			
			log.info("LugarAprovechamientoDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_aprovechamiento;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsLugarAprovechamiento.getLugar_aprovechamiento_id());
			cs.setInt(2, clsLugarAprovechamiento.getValorizacion().getValorizacion_id());
			cs.setInt(3, clsLugarAprovechamiento.getAdm_propia());
			cs.setInt(4, clsLugarAprovechamiento.getTercerizado_eor());
			cs.setInt(5, clsLugarAprovechamiento.getTercerizado_asoc_recic());
			cs.setInt(6, clsLugarAprovechamiento.getMixto());
			cs.setInt(7, clsLugarAprovechamiento.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);

			log.info("LugarAprovechamientoDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	public ClsResultado actualizacion(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_aprovechamiento3;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsLugarAprovechamiento.getLugar_aprovechamiento_id());
			cs.setInt(2, clsLugarAprovechamiento.getNumero_recicladores());
			cs.setInt(3, clsLugarAprovechamiento.getCodigo_usuario());
			cs.registerOutParameter(4, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(4);

			log.info("LugarAprovechamientoDAO >> actualizacion() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
