package pe.gob.sigersol.entities;

public class ClsOferta {

	private Integer oferta_id;
	private ClsBarrido barrido;
	private ClsTipoOferta tipo_oferta;
	private Double total_vias;
    private Integer total_asfaltada;
	private Integer total_no_asfaltada;
	private Integer total_plazas;
	private Integer total_playas;
	private Integer total_otros;
	private Integer codigo_usuario;
	
	public Integer getOferta_id() {
		return oferta_id;
	}
	public void setOferta_id(Integer oferta_id) {
		this.oferta_id = oferta_id;
	}
	public ClsBarrido getBarrido() {
		return barrido;
	}
	public void setBarrido(ClsBarrido barrido) {
		this.barrido = barrido;
	}
	public ClsTipoOferta getTipo_oferta() {
		return tipo_oferta;
	}
	public void setTipo_oferta(ClsTipoOferta tipo_oferta) {
		this.tipo_oferta = tipo_oferta;
	}
	public Double getTotal_vias() {
		return total_vias;
	}
	public void setTotal_vias(Double total_vias) {
		this.total_vias = total_vias;
	}
	public Integer getTotal_asfaltada() {
		return total_asfaltada;
	}
	public void setTotal_asfaltada(Integer total_asfaltada) {
		this.total_asfaltada = total_asfaltada;
	}
	public Integer getTotal_no_asfaltada() {
		return total_no_asfaltada;
	}
	public void setTotal_no_asfaltada(Integer total_no_asfaltada) {
		this.total_no_asfaltada = total_no_asfaltada;
	}
	public Integer getTotal_plazas() {
		return total_plazas;
	}
	public void setTotal_plazas(Integer total_plazas) {
		this.total_plazas = total_plazas;
	}
	public Integer getTotal_playas() {
		return total_playas;
	}
	public void setTotal_playas(Integer total_playa) {
		this.total_playas = total_playa;
	}
	public Integer getTotal_otros() {
		return total_otros;
	}
	public void setTotal_otros(Integer total_otros) {
		this.total_otros = total_otros;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
