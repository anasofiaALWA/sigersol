package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsFuncion;
import pe.gob.sigersol.entities.ClsFuncionPerfil;
import pe.gob.sigersol.entities.ClsPerfil;
import pe.gob.sigersol.entities.ClsResultado;

public class CicloGestionResiduosDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(CicloGestionResiduosDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_list_sgr_ciclo_gestion = "{call SIGERSOLBL.SGR_SP_LIST_SGR_CICLO_GESTION(?)}";
	private final String sgr_sp_existe_ciclo_grs = "{call SIGERSOLBL.SGR_SP_EXISTE_CICLO_GRS(?,?)}";
	private final String sgr_sp_ins_ciclo_gestion = "{call SIGERSOLBL.SGR_SP_INS_CICLO_GESTION(?,?,?)}";
	private final String sgr_sp_upd_ciclo_gestion = "{call SIGERSOLBL.SGR_SP_UPD_CICLO_GESTION(?,?,?,?,?,?,?,?,?,?,?,?)}";

	private final String sgr_sp_cc_sel_ult_anio_registro = "{call SIGERSOLBL.SGR_SP_CC_SEL_ULT_ANIO_MUNI(?,?,?)}";

	public ClsResultado obtener() {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCicloGestionResiduos> lista = new ArrayList<ClsCicloGestionResiduos>();
		con = null;
		String sql = this.sgr_sp_list_sgr_ciclo_gestion;
		try {

			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);

			while (rs.next()) {
				ClsCicloGestionResiduos item = new ClsCicloGestionResiduos();
				try {
					item.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setMunicipalidad_id(rs.getInt("MUNICIPALIDAD_ID"));
					item.setAnio(rs.getInt("ANIO"));
					item.setGeneracion(rs.getInt("GENERACION"));
					item.setAlmacenamiento(rs.getInt("ALMACENAMIENTO"));
					item.setBarrido(rs.getInt("BARRIDO"));
					item.setRecoleccion(rs.getInt("RECOLECCION"));
					item.setTransferencia(rs.getInt("TRANSFERENCIA"));
					item.setValorizacion(rs.getInt("VALORIZACION"));
					item.setDisposicion_final(rs.getInt("DISPOSICION_FINAL"));
					item.setFlag_recoleccion(rs.getInt("FLAG_RECOLECCION"));
					item.setReporte_firmado(rs.getInt("REPORTE_FIRMADO"));

				} catch (Exception e) {

				}

				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado validar(ClsCicloGestionResiduos clsCicloGestionResiduos) {
		ClsResultado clsResultado = new ClsResultado();
		ClsCicloGestionResiduos item = new ClsCicloGestionResiduos();

		con = null;
		String sql = this.sgr_sp_existe_ciclo_grs;
		Integer ciclo_id = null;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCicloGestionResiduos.getMunicipalidad_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setGeneracion(rs.getInt("GENERACION"));
					item.setAlmacenamiento(rs.getInt("ALMACENAMIENTO"));
					item.setBarrido(rs.getInt("BARRIDO"));
					item.setRecoleccion(rs.getInt("RECOLECCION"));
					item.setTransferencia(rs.getInt("TRANSFERENCIA"));
					item.setValorizacion(rs.getInt("VALORIZACION"));
					item.setDisposicion_final(rs.getInt("DISPOSICION_FINAL"));
					item.setFlag_recoleccion(rs.getInt("FLAG_RECOLECCION"));
					item.setReporte_firmado(rs.getInt("REPORTE_FIRMADO"));
				} catch (Exception e) {

				}
			}

			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 2 obtener: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 3 obtener: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error SQLException");
		} catch (Exception ex) {
			log.error("ERROR! 4 obtener: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error generico");
		} finally {
			try {
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 5 obtener: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje("Error al cerrar");
			}
		}

		return clsResultado;
	}

	public ClsResultado insertar(ClsCicloGestionResiduos clsCicloGestionResiduos) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_ciclo_gestion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCicloGestionResiduos.getMunicipalidad_id());			
			cs.setInt(2, clsCicloGestionResiduos.getCodigo_usuario());
			cs.registerOutParameter(3, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(3);

			log.info("CicloGestionResiduosDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsCicloGestionResiduos clsCicloGestionResiduos) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_ciclo_gestion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCicloGestionResiduos.getCiclo_grs_id());
			cs.setInt(2, clsCicloGestionResiduos.getGeneracion());
			cs.setInt(3, clsCicloGestionResiduos.getAlmacenamiento());
			cs.setInt(4, clsCicloGestionResiduos.getBarrido());
			cs.setInt(5, clsCicloGestionResiduos.getRecoleccion());
			cs.setInt(6, clsCicloGestionResiduos.getTransferencia());
			cs.setInt(7, clsCicloGestionResiduos.getValorizacion());
			cs.setInt(8, clsCicloGestionResiduos.getDisposicion_final());
			cs.setInt(9, clsCicloGestionResiduos.getReporte_firmado());
			cs.setInt(10, clsCicloGestionResiduos.getFlag_recoleccion());
			cs.setInt(11, clsCicloGestionResiduos.getCodigo_usuario());
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(12);

			log.info("CicloGestionResiduosDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado getUltimoAnioMuni(String codDistrito) {
		ClsResultado clsResultado = new ClsResultado();
		ClsCicloGestionResiduos cicloGestion = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_cc_sel_ult_anio_registro;
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			
			
			cs.setString(1, codDistrito);
			
			cs.registerOutParameter(2, OracleTypes.INTEGER);
			cs.registerOutParameter(3, OracleTypes.INTEGER);
			
			cs.execute();
			
			try {
				Integer municipalidadId = cs.getInt(2);
				cicloGestion.setMunicipalidad_id(municipalidadId);
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				Integer anio = cs.getInt(3);
				cicloGestion.setAnio(anio);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			clsResultado.setExito(true);
			clsResultado.setObjeto(cicloGestion);
			clsResultado.setMensaje("Obtención exitosa");
					
		} catch (SQLException ex) {
			log.error("ERROR! 1 getUltimoAnioMuni: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 getUltimoAnioMuni: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 getUltimoAnioMuni: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 getUltimoAnioMuni: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}

	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
