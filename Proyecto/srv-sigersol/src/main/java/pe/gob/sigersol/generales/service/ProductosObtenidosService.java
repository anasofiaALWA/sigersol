package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsProductosObtenidos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ProductosObtenidosDAO;

public class ProductosObtenidosService {

	private static Logger log = Logger.getLogger(ProductosObtenidosService.class);
	ProductosObtenidosDAO productosObtenidosDAO = new ProductosObtenidosDAO();
	
	public ClsResultado listarProductosObtenidos(ClsProductosObtenidos clsProductosObtenidos){
		ClsResultado clsResultado = null;
		try {
			clsResultado = productosObtenidosDAO.listarProductosObtenidos(clsProductosObtenidos);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsProductosObtenidos clsProductosObtenidos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = productosObtenidosDAO.insertar(clsProductosObtenidos);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsProductosObtenidos clsProductosObtenidos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = productosObtenidosDAO.actualizar(clsProductosObtenidos);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
