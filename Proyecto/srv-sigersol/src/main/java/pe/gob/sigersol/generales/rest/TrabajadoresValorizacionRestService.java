package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsPersona;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTrabajadores;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.PersonaService;
import pe.gob.sigersol.generales.service.TrabajadoresService;
import pe.gob.sigersol.generales.service.TrabajadoresValorizacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/trabajadoresValorizacion")
@RequestScoped
public class TrabajadoresValorizacionRestService {

	private static Logger log = Logger.getLogger(TrabajadoresValorizacionRestService.class.getName());
	private ClsTrabajadores clsTrabajadores;
	private TrabajadoresValorizacionService trabajadoresValorizacionService;
	private PersonaService personaService;
	private ClsPersona clsPersona;
	private ClsPersona clsPersonaAux;
	private ClsValorizacion clsValorizacion;
	private List<ClsTrabajadores> listaTrabajadores;
	private List<ClsPersona> listaPersonas;
	
	@POST
	@Path("/listarTrabajadores")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarTrabajadores(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			trabajadoresValorizacionService = new TrabajadoresValorizacionService();
			
			jsonEntradaListaTrabajadores(paramJson);
			
			log.info("**Obtener Info Celdas**");
			
			clsResultado = trabajadoresValorizacionService.listarTrabajadores(clsTrabajadores);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstTrabajadores\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		trabajadoresValorizacionService = new TrabajadoresValorizacionService();
		personaService = new PersonaService();
		clsPersonaAux = new ClsPersona();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaNoDomiciliar(paramJson);
			
			for (ClsTrabajadores clsTrabajadores : listaTrabajadores) {
				
				idUsuario = personaService.insertar(clsTrabajadores.getPersona());
				clsPersonaAux.setPersona_id(idUsuario);
				clsTrabajadores.setPersona(clsPersonaAux);
				clsResultado = trabajadoresValorizacionService.insertar(clsTrabajadores);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		Integer idUsuario = 0;
		trabajadoresValorizacionService = new TrabajadoresValorizacionService();
		personaService = new PersonaService();
		clsPersonaAux = new ClsPersona();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaNoDomiciliar(paramJson);
			
			for (ClsTrabajadores clsTrabajadores : listaTrabajadores) {
				
				idUsuario = personaService.insertar(clsTrabajadores.getPersona());
				clsPersonaAux.setPersona_id(idUsuario);
				clsTrabajadores.setPersona(clsPersonaAux);
				clsResultado = trabajadoresValorizacionService.insertar(clsTrabajadores);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Contenedor
	 * 
	 * */
	private void jsonEntradaNoDomiciliar(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService : jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectTipoContenedor;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaTrabajadores = new ArrayList();
		
		Integer contador = 1;
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsTrabajadores = new ClsTrabajadores();
			clsPersona = new ClsPersona();
			clsValorizacion = new ClsValorizacion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("dni") != null && innerObject.get("dni").toString() != "") {
				clsPersona.setDni(Integer.parseInt(innerObject.get("dni").toString()));
			}

			if (innerObject.get("nom_apellido") != null && innerObject.get("nom_apellido").toString() != "") {
				clsPersona.setNom_apellidos(innerObject.get("nom_apellido").toString());
			}
			
			if (innerObject.get("sexo") != null && innerObject.get("sexo").toString() != "") {
				clsPersona.setSexo(innerObject.get("sexo").toString());
			}
			
			if (innerObject.get("rango") != null && innerObject.get("rango").toString() != "") {
				clsPersona.setRango_edad(innerObject.get("rango").toString());
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsPersona.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			clsTrabajadores.setPersona(clsPersona);
			
			if (innerObject.get("valorizacion_id") != null && innerObject.get("valorizacion_id").toString() != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(innerObject.get("valorizacion_id").toString()));
			}
			
			clsTrabajadores.setValorizacion(clsValorizacion);
			
			if (innerObject.get("cargo") != null && innerObject.get("cargo").toString() != "") {
				clsTrabajadores.setCargo(innerObject.get("cargo").toString());
			}
			
			if (innerObject.get("laboral") != null && innerObject.get("laboral").toString() != "") {
				clsTrabajadores.setCondicion_laboral(innerObject.get("laboral").toString());
			}
			
			if (innerObject.get("horario") != null && innerObject.get("horario").toString() != "") {
				clsTrabajadores.setHorario_trabajo(innerObject.get("horario").toString());
			}
			
			if (innerObject.get("dias_trabajados") != null && innerObject.get("dias_trabajados").toString() != "") {
				clsTrabajadores.setDias_trabajados(Integer.parseInt(innerObject.get("dias_trabajados").toString()));
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsTrabajadores.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaTrabajadores.add(clsTrabajadores);	
		}
			
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaTrabajadores(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> InfoCeldasRestService : jsonEntradaListaInfoCeldas" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectInfoCeldas;
		JSONParser jsonParser = new JSONParser();
		
		clsTrabajadores = new ClsTrabajadores();
		clsValorizacion = new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectInfoCeldas = (JSONObject) jsonObjectPrincipal.get("Trabajadores");
			
			if (jsonObjectInfoCeldas.get("valorizacion_id") != null && jsonObjectInfoCeldas.get("valorizacion_id") != "" &&
					jsonObjectInfoCeldas.get("valorizacion_id") != " ") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectInfoCeldas.get("valorizacion_id").toString()));
			}
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsTrabajadores.setValorizacion(clsValorizacion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
