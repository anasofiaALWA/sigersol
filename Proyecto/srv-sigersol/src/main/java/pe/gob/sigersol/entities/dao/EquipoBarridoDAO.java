package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsEquipoBarrido;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUniformeEquipo;

public class EquipoBarridoDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(OfertaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_equipo_barrido = "{call SIGERSOLBL.SGR_SP_INS_EQUIPO_BARRIDO(?,?,?,?,?)}";
	private final String sgr_sp_upd_equipo_barrido = "{call SIGERSOLBL.SGR_SP_UPD_EQUIPO_BARRIDO(?,?,?,?,?)}";
	
	private final String sgr_sp_list_equipo_barrido = "{call SIGERSOLBL.SGR_SP_LIST_EQUIPO_BARRIDO(?,?)}";  
	
	
	public ClsResultado listarEquipo(ClsEquipoBarrido clsEquipoBarrido){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsEquipoBarrido> lista = new ArrayList<ClsEquipoBarrido>();
		con = null;
		String sql = this.sgr_sp_list_equipo_barrido;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsEquipoBarrido.getBarrido().getBarrido_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsEquipoBarrido item = new ClsEquipoBarrido();
				ClsUniformeEquipo uniformeEquipo = new ClsUniformeEquipo();
				
				try {
					uniformeEquipo.setUniforme_equipo_id(rs.getInt("UNIFORME_EQUIPO_ID"));
					uniformeEquipo.setTipo_uniforme(rs.getString("TIPO_UNIFORME"));
					item.setUniforme(uniformeEquipo);
					if(clsEquipoBarrido.getBarrido().getBarrido_id() != -1)
					{
						item.setCantidad(rs.getInt("CANTIDAD"));
					}
					else {
						item.setCantidad(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsEquipoBarrido clsEquipoBarrido) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_equipo_barrido;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsEquipoBarrido.getBarrido().getBarrido_id());
			cs.setInt(2, clsEquipoBarrido.getUniforme().getUniforme_equipo_id());
			cs.setDouble(3, clsEquipoBarrido.getCantidad());
			cs.setInt(4, clsEquipoBarrido.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);
			
			log.info("EquipoBarridoDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsEquipoBarrido clsEquipoBarrido) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_equipo_barrido;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsEquipoBarrido.getBarrido().getBarrido_id());
			cs.setInt(2, clsEquipoBarrido.getUniforme().getUniforme_equipo_id());
			cs.setDouble(3, clsEquipoBarrido.getCantidad());
			cs.setInt(4, clsEquipoBarrido.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);

			log.info("EquipoBarridoDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
