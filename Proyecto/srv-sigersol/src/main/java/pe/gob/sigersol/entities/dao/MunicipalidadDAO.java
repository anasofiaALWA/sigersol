package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;

public class MunicipalidadDAO extends GenericoDAO{
	
	private static Logger log = Logger.getLogger(MunicipalidadDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_municipalidad = "{call SIGERSOLBL.SGR_SP_LIST_SOLICITUD(?)}";
	private final String sgr_sp_ins_municipalidad = "{call SIGERSOLBL.SGR_SP_INS_SOLICITUD (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_municipalidad = "{call SIGERSOLBL.SGR_SP_UPD_SOLICITUD (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

	public ClsResultado obtener() {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsMunicipalidad> lista = new ArrayList<ClsMunicipalidad>();
		con = null;
		String sql = this.sgr_sp_list_municipalidad;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);

			while (rs.next()) {
				ClsMunicipalidad item = new ClsMunicipalidad();
				try {
					item.setMunicipalidad_id(rs.getInt("MUNICIPALIDAD_ID"));
					item.setCodigo_ubigeo_departamento(rs.getString("COD_UBIGEO_DEP"));
					item.setCodigo_ubigeo_provincia(rs.getString("COD_UBIGEO_PROV"));
					item.setCodigo_ubigeo_distrito(rs.getString("COD_UBIGEO_DIST"));
					item.setRuc(rs.getString("RUC"));
					item.setNombre_alcalde(rs.getString("NOMBRE_ALCALDE"));
					item.setNombre_gerencia(rs.getString("NOMBRE_GERENCIA"));
					item.setResponsable_limpieza(rs.getString("RESP_LIMPIEZA_PUBLICA"));
					item.setDireccion(rs.getString("DIRECCION"));
					item.setTelefono(rs.getString("TELEFONO"));
					item.setFax(rs.getString("FAX"));
					item.setClasificacion_municipalidad(rs.getInt("CLASIF_MUNICIPALIDAD"));
					item.setTipo_municipalidad(rs.getInt("TIPO_MUNICIPALIDAD"));
					item.setPoblacion_urbana(rs.getInt("POBLACION_URB"));
					item.setPoblacion_urbana_fuente(rs.getString("POBLACION_URB_FUENTE"));
					item.setPoblacion_rural(rs.getInt("POBLACION_RURAL"));
					item.setPoblacion_rural_fuente(rs.getString("POBLACION_RURAL_FUENTE"));
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsMunicipalidad clsMunicipalidad) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_municipalidad;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setString(1, clsMunicipalidad.getCodigo_ubigeo_departamento());
			cs.setString(2, clsMunicipalidad.getCodigo_ubigeo_provincia());
			cs.setString(3, clsMunicipalidad.getCodigo_ubigeo_distrito());
			cs.setString(4, clsMunicipalidad.getRuc());
			cs.setString(5, clsMunicipalidad.getNombre_alcalde());
			cs.setString(6, clsMunicipalidad.getNombre_gerencia());
			cs.setString(7, clsMunicipalidad.getResponsable_limpieza());
			cs.setString(8, clsMunicipalidad.getDireccion());
			cs.setString(9, clsMunicipalidad.getTelefono());
			cs.setString(10, clsMunicipalidad.getFax());
			cs.setInt(11, clsMunicipalidad.getClasificacion_municipalidad());
			cs.setInt(12, clsMunicipalidad.getTipo_municipalidad());
			cs.setInt(13, clsMunicipalidad.getPoblacion_urbana());
			cs.setString(14, clsMunicipalidad.getPoblacion_urbana_fuente());
			cs.setInt(15, clsMunicipalidad.getPoblacion_rural());
			cs.setString(16, clsMunicipalidad.getPoblacion_rural_fuente());
			cs.setInt(17, clsMunicipalidad.getCodigo_usuario());
			cs.registerOutParameter(18, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(18);
			
			log.info("MunicipalidadDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsMunicipalidad clsMunicipalidad) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_municipalidad;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsMunicipalidad.getMunicipalidad_id());
			cs.setString(2, clsMunicipalidad.getCodigo_ubigeo_departamento());
			cs.setString(3, clsMunicipalidad.getCodigo_ubigeo_provincia());
			cs.setString(4, clsMunicipalidad.getCodigo_ubigeo_distrito());
			cs.setString(5, clsMunicipalidad.getRuc());
			cs.setString(6, clsMunicipalidad.getNombre_alcalde());
			cs.setString(7, clsMunicipalidad.getNombre_gerencia());
			cs.setString(8, clsMunicipalidad.getResponsable_limpieza());
			cs.setString(9, clsMunicipalidad.getDireccion());
			cs.setString(10, clsMunicipalidad.getTelefono());
			cs.setString(11, clsMunicipalidad.getFax());
			cs.setInt(12, clsMunicipalidad.getClasificacion_municipalidad());
			cs.setInt(13, clsMunicipalidad.getTipo_municipalidad());
			cs.setInt(14, clsMunicipalidad.getPoblacion_urbana());
			cs.setString(15, clsMunicipalidad.getPoblacion_urbana_fuente());
			cs.setInt(16, clsMunicipalidad.getPoblacion_rural());
			cs.setString(17, clsMunicipalidad.getPoblacion_rural_fuente());
			cs.setInt(18, clsMunicipalidad.getCodigo_usuario());
			cs.registerOutParameter(19, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(19);

			log.info("MunicipalidadDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
