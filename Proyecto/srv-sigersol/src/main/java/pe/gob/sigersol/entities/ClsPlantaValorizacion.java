package pe.gob.sigersol.entities;

public class ClsPlantaValorizacion {

	private Integer planta_valorizacion_id;
	private ClsValorizacion valorizacion;
	private Integer adm_propia;
	private Integer tercerizado;
	private Integer mixto;
	private String zona_coordenada;
	private String norte_coordenada;
	private String sur_coordenada;
	private String division_politica;
	private String direccion_iga;
	private String referencia_iga;
	private Integer codigo_usuario;

	public Integer getPlanta_valorizacion_id() {
		return planta_valorizacion_id;
	}
	public void setPlanta_valorizacion_id(Integer planta_valorizacion_id) {
		this.planta_valorizacion_id = planta_valorizacion_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public Integer getAdm_propia() {
		return adm_propia;
	}
	public void setAdm_propia(Integer adm_propia) {
		this.adm_propia = adm_propia;
	}
	public Integer getTercerizado() {
		return tercerizado;
	}
	public void setTercerizado(Integer tercerizado) {
		this.tercerizado = tercerizado;
	}
	public Integer getMixto() {
		return mixto;
	}
	public void setMixto(Integer mixto) {
		this.mixto = mixto;
	}
	public String getZona_coordenada() {
		return zona_coordenada;
	}
	public void setZona_coordenada(String zona_coordenada) {
		this.zona_coordenada = zona_coordenada;
	}
	public String getNorte_coordenada() {
		return norte_coordenada;
	}
	public void setNorte_coordenada(String norte_coordenada) {
		this.norte_coordenada = norte_coordenada;
	}
	public String getSur_coordenada() {
		return sur_coordenada;
	}
	public void setSur_coordenada(String sur_coordenada) {
		this.sur_coordenada = sur_coordenada;
	}
	public String getDivision_politica() {
		return division_politica;
	}
	public void setDivision_politica(String division_politica) {
		this.division_politica = division_politica;
	}
	public String getDireccion_iga() {
		return direccion_iga;
	}
	public void setDireccion_iga(String direccion_iga) {
		this.direccion_iga = direccion_iga;
	}
	public String getReferencia_iga() {
		return referencia_iga;
	}
	public void setReferencia_iga(String referencia_iga) {
		this.referencia_iga = referencia_iga;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
