package pe.gob.sigersol.entities;

public class ClsTipoEquipo {

	private Integer tipo_equipo_id;
	private String nombre_tipo;
	private String nombre_subtipo;
	
	public Integer getTipo_equipo_id() {
		return tipo_equipo_id;
	}
	public void setTipo_equipo_id(Integer tipo_equipo_id) {
		this.tipo_equipo_id = tipo_equipo_id;
	}
	public String getNombre_tipo() {
		return nombre_tipo;
	}
	public void setNombre_tipo(String nombre_tipo) {
		this.nombre_tipo = nombre_tipo;
	}
	public String getNombre_subtipo() {
		return nombre_subtipo;
	}
	public void setNombre_subtipo(String nombre_subtipo) {
		this.nombre_subtipo = nombre_subtipo;
	}
}
