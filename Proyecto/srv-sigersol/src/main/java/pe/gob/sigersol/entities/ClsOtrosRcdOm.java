package pe.gob.sigersol.entities;

public class ClsOtrosRcdOm {

	private Integer otrosRcdOm_id;
	private ClsRecRcdOm recRcdOm;
	private ClsTipoResiduo tipoResiduos;
	private Integer usuarioAtendido;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getOtrosRcdOm_id() {
		return otrosRcdOm_id;
	}
	public void setOtrosRcdOm_id(Integer otrosRcdOm_id) {
		this.otrosRcdOm_id = otrosRcdOm_id;
	}
	public ClsRecRcdOm getRecRcdOm() {
		return recRcdOm;
	}
	public void setRecRcdOm(ClsRecRcdOm recRcdOm) {
		this.recRcdOm = recRcdOm;
	}
	public ClsTipoResiduo getTipoResiduos() {
		return tipoResiduos;
	}
	public void setTipoResiduos(ClsTipoResiduo tipoResiduos) {
		this.tipoResiduos = tipoResiduos;
	}
	public Integer getUsuarioAtendido() {
		return usuarioAtendido;
	}
	public void setUsuarioAtendido(Integer usuarioAtendido) {
		this.usuarioAtendido = usuarioAtendido;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
