package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAlmacenamiento;
import pe.gob.sigersol.entities.ClsCantPapeleras;
import pe.gob.sigersol.entities.ClsEstado;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CantPapelerasService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/papeleras")
@RequestScoped
public class CantPapelerasRestService {

	private static Logger log = Logger.getLogger(CantVehiculosRestService.class.getName());
	private Integer accion = null;
	private ClsCantPapeleras clsCantPapeleras;
	private ClsEstado clsEstado;
	private ClsAlmacenamiento clsAlmacenamiento;
	private CantPapelerasService cantPapelerasService;
	private List<ClsCantPapeleras> listaCantPapeleras;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarPapeleras")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCantPapeleras(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			cantPapelerasService = new CantPapelerasService();
			
			jsonEntradaListaCantPapeleras(paramJson);
			
			log.info("**Obtener Cantidad Papeleras**");
			
			clsResultado = cantPapelerasService.listarCantPapeleras(clsCantPapeleras);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstPapeleras\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		cantPapelerasService = new CantPapelerasService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaCantPapeleras(paramJson);

			for (ClsCantPapeleras clsCantVehiculos : listaCantPapeleras) {
				clsResultado = cantPapelerasService.insertar(clsCantVehiculos);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		cantPapelerasService = new CantPapelerasService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCantPapeleras(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCantPapeleras clsCantVehiculos : listaCantPapeleras) {
			clsResultado = cantPapelerasService.actualizar(clsCantVehiculos);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de CantVehiculos
	 * 
	 * */
	private void jsonEntradaCantPapeleras(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantVehiculosRestService :	 jsonEntradaCantVehiculos" + jsonEntrada);

		JSONObject jsonObjectCobertura;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCantPapeleras = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCantPapeleras = new ClsCantPapeleras();
			clsEstado = new ClsEstado();
			clsAlmacenamiento = new ClsAlmacenamiento();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("almacenamiento_id") != null && innerObject.get("almacenamiento_id").toString() != "") {
				clsAlmacenamiento.setAlmacenamiento_id(Integer.parseInt(innerObject.get("almacenamiento_id").toString()));
			}
			
			clsCantPapeleras.setAlmacenamiento(clsAlmacenamiento);
			
			if (innerObject.get("estado") != null && innerObject.get("estado").toString() != "") {
				jsonObjectCobertura  = (JSONObject) innerObject.get("estado");
				clsEstado.setEstado_id(Integer.parseInt(jsonObjectCobertura.get("estado_id").toString()));
			}
			clsCantPapeleras.setEstado(clsEstado);
			
			if (innerObject.get("mercados") != null && innerObject.get("mercados").toString() != "") {
				clsCantPapeleras.setMercados(Integer.parseInt(innerObject.get("mercados").toString()));
			} else {
				clsCantPapeleras.setMercados(0);
			}
			
			if (innerObject.get("parques") != null && innerObject.get("parques").toString() != "") {
				clsCantPapeleras.setParques(Integer.parseInt(innerObject.get("parques").toString()));
			} else {
				clsCantPapeleras.setParques(0);
			}
			
			if (innerObject.get("vias") != null && innerObject.get("vias").toString() != "") {
				clsCantPapeleras.setVias(Integer.parseInt(innerObject.get("vias").toString()));
			} else {
				clsCantPapeleras.setVias(0);
			}
			
			if (innerObject.get("otros") != null && innerObject.get("otros").toString() != "") {
				clsCantPapeleras.setOtros(Integer.parseInt(innerObject.get("otros").toString()));
			} else {
				clsCantPapeleras.setOtros(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCantPapeleras.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCantPapeleras.add(clsCantPapeleras);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCantPapeleras(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantVehiculosRestService :	 jsonEntradaListaCantVehiculos" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectTipoManejo;
		JSONParser jsonParser = new JSONParser();
		
		clsCantPapeleras = new ClsCantPapeleras();
		clsEstado = new ClsEstado();
		clsAlmacenamiento = new ClsAlmacenamiento();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectTipoManejo = (JSONObject) jsonObjectPrincipal.get("Papeleras");
			
			if (jsonObjectTipoManejo.get("almacenamiento_id") != null && jsonObjectTipoManejo.get("almacenamiento_id") != "" &&
					jsonObjectTipoManejo.get("almacenamiento_id") != " ") {
				clsAlmacenamiento.setAlmacenamiento_id(Integer.parseInt(jsonObjectTipoManejo.get("almacenamiento_id").toString()));
			}
			else
				clsAlmacenamiento.setAlmacenamiento_id(-1);
			
			clsCantPapeleras.setAlmacenamiento(clsAlmacenamiento);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
