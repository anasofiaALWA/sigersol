package pe.gob.sigersol.entities;

public class ClsRecValorizacion {

	private Integer rec_valorizacion_id;
	private ClsCicloGestionResiduos ciclo_grs_id;
	private String adm_serv_id;
	private Integer costo_anual;
	private Integer fr1;
	private Integer fr2;
	private Integer flag_recoleccion;
	private String frecuencia;
	private Integer cantidad_residuos_organicos;
	private Integer cantidad_residuos_inorganicos;
	private Integer domiciliarios;
	private Integer no_domiciliarios;
	private String otros_descripcion;
    private Integer otros_antig_promedio;
    private Integer otros_capacidad;
    private Integer otros_recoleccion_anual;
    private Integer otros_numero_unidades;
	private Integer codigo_usuario;
	
	public Integer getRec_valorizacion_id() {
		return rec_valorizacion_id;
	}
	public void setRec_valorizacion_id(Integer rec_valorizacion_id) {
		this.rec_valorizacion_id = rec_valorizacion_id;
	}
	public ClsCicloGestionResiduos getCiclo_grs_id() {
		return ciclo_grs_id;
	}
	public void setCiclo_grs_id(ClsCicloGestionResiduos ciclo_grs_id) {
		this.ciclo_grs_id = ciclo_grs_id;
	}
	public String getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(String adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getFlag_recoleccion() {
		return flag_recoleccion;
	}
	public void setFlag_recoleccion(Integer flag_recoleccion) {
		this.flag_recoleccion = flag_recoleccion;
	}
	public Integer getFr1() {
		return fr1;
	}
	public void setFr1(Integer fr1) {
		this.fr1 = fr1;
	}
	public Integer getFr2() {
		return fr2;
	}
	public void setFr2(Integer fr2) {
		this.fr2 = fr2;
	}
	public String getFrecuencia() {
		return frecuencia;
	}
	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}
	public Integer getCantidad_residuos_organicos() {
		return cantidad_residuos_organicos;
	}
	public void setCantidad_residuos_organicos(Integer cantidad_residuos_organicos) {
		this.cantidad_residuos_organicos = cantidad_residuos_organicos;
	}
	public Integer getCantidad_residuos_inorganicos() {
		return cantidad_residuos_inorganicos;
	}
	public void setCantidad_residuos_inorganicos(Integer cantidad_residuos_inorganicos) {
		this.cantidad_residuos_inorganicos = cantidad_residuos_inorganicos;
	}
	public Integer getDomiciliarios() {
		return domiciliarios;
	}
	public void setDomiciliarios(Integer domiciliarios) {
		this.domiciliarios = domiciliarios;
	}
	public Integer getNo_domiciliarios() {
		return no_domiciliarios;
	}
	public void setNo_domiciliarios(Integer no_domiciliarios) {
		this.no_domiciliarios = no_domiciliarios;
	}
	public String getOtros_descripcion() {
		return otros_descripcion;
	}
	public void setOtros_descripcion(String otros_descripcion) {
		this.otros_descripcion = otros_descripcion;
	}
	public Integer getOtros_antig_promedio() {
		return otros_antig_promedio;
	}
	public void setOtros_antig_promedio(Integer otros_antig_promedio) {
		this.otros_antig_promedio = otros_antig_promedio;
	}
	public Integer getOtros_capacidad() {
		return otros_capacidad;
	}
	public void setOtros_capacidad(Integer otros_capacidad) {
		this.otros_capacidad = otros_capacidad;
	}
	public Integer getOtros_recoleccion_anual() {
		return otros_recoleccion_anual;
	}
	public void setOtros_recoleccion_anual(Integer otros_recoleccion_anual) {
		this.otros_recoleccion_anual = otros_recoleccion_anual;
	}
	public Integer getOtros_numero_unidades() {
		return otros_numero_unidades;
	}
	public void setOtros_numero_unidades(Integer otros_numero_unidades) {
		this.otros_numero_unidades = otros_numero_unidades;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
