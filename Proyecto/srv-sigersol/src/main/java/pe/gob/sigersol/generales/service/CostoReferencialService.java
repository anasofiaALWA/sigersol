package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCostoReferencial;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CostoReferencialDAO;

public class CostoReferencialService {
	
	private static Logger log = Logger.getLogger(CostoReferencialService.class);
	CostoReferencialDAO costoReferencialDAO = new CostoReferencialDAO();
	
	public ClsResultado listarCostoReferencial(ClsCostoReferencial clsCostoReferencial){
		ClsResultado clsResultado = null;
		try {
			clsResultado = costoReferencialDAO.listarCostoReferencial(clsCostoReferencial);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCostoReferencial clsCostoReferencial) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = costoReferencialDAO.insertar(clsCostoReferencial);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCostoReferencial clsCostoReferencial) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = costoReferencialDAO.actualizar(clsCostoReferencial);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
