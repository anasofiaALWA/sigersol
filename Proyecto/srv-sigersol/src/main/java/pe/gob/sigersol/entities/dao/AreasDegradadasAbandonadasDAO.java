package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAreasDegradadasAbandonadas;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;

public class AreasDegradadasAbandonadasDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(AreasDegradadasAbandonadasDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_area_abandonadas = "{call SIGERSOLBL.SGR_SP_LIST_AREA_ABANDONADAS(?,?)}";
	private final String sgr_sp_ins_area_abandonadas = "{call SIGERSOLBL.SGR_SP_INS_AREA_ABANDONADAS(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_area_abandonadas = "{call SIGERSOLBL.SGR_SP_UPD_AREA_ABANDONADAS(?,?,?,?,?,?,?)}";

	public ClsResultado obtener(ClsAreasDegradadasAbandonadas clsAreasDegradadasAbandonadas) {
		
		ClsResultado clsResultado = new ClsResultado();
		ClsAreasDegradadasAbandonadas item = new ClsAreasDegradadasAbandonadas();
		ClsDisposicionFinal disposicionFinal = new ClsDisposicionFinal();
		con = null;
		String sql = this.sgr_sp_list_area_abandonadas;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAreasDegradadasAbandonadas.getDisposicion_final().getDisposicion_final_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setAreas_degradadas_abandonadas_id(rs.getInt("AREA_ABANDONADAS_ID"));
					disposicionFinal.setDisposicion_final_id(rs.getInt("DISPOSICION_FINAL_ID"));
					item.setDisposicion_final(disposicionFinal);
					item.setFlag_areas_abandonadas(rs.getInt("FLAG_AREAS"));
					item.setNumero_areas(rs.getInt("NUMERO_AREAS"));
					item.setAltura_napa(rs.getInt("ALTURA_NAPA"));

				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsAreasDegradadasAbandonadas clsAreasDegradadasAbandonadas) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_area_abandonadas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}
		
		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAreasDegradadasAbandonadas.getDisposicion_final().getDisposicion_final_id());
			cs.setInt(2, clsAreasDegradadasAbandonadas.getFlag_areas_abandonadas());
			cs.setInt(3, clsAreasDegradadasAbandonadas.getNumero_areas());
			cs.setInt(4, clsAreasDegradadasAbandonadas.getAltura_napa());
			cs.setInt(5, clsAreasDegradadasAbandonadas.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("AreasDegradadasAbandonadasDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsAreasDegradadasAbandonadas clsAreasDegradadasAbandonadas) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_area_abandonadas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAreasDegradadasAbandonadas.getAreas_degradadas_abandonadas_id());
			cs.setInt(2, clsAreasDegradadasAbandonadas.getDisposicion_final().getDisposicion_final_id());
			cs.setInt(3, clsAreasDegradadasAbandonadas.getFlag_areas_abandonadas());
			cs.setInt(4, clsAreasDegradadasAbandonadas.getNumero_areas());
			cs.setInt(5, clsAreasDegradadasAbandonadas.getAltura_napa());
			cs.setInt(6, clsAreasDegradadasAbandonadas.getCodigo_usuario());
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(7);

			log.info("AreasDegradadasAbandonadasDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}	
}
