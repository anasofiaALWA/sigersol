package pe.gob.sigersol.entities;

public class ClsVehiculoConvencional {

	private Integer vehiculo_convencional_id;
	private ClsRecDisposicionFinal recDisposicionFinal;
	private ClsRecValorizacion recValorizacion;
	private ClsRecRcdOm recRcdOm;
	private ClsTransferencia transferencia;
	private String placa;
	private Integer recorrido_anual;
	private String tipo_vehiculo;
	private Integer anio_fabricacion;
	private Double capacidad;
	private Integer tipo_combustible;
	private Double cantidad_combustible;
	private Integer costo_anual;
	private Double rendimiento_anual;
	private Double capacidad_viaje;
	private Integer efectividad;
	private Double promedio_viajes;
	private Double promedio_turno;
	private Integer dias_trabajados;
	private Double recoleccion_media;
	private Double recoleccion_anual;
	private Integer codigo_usuario;
	
	public Integer getVehiculo_convencional_id() {
		return vehiculo_convencional_id;
	}
	public void setVehiculo_convencional_id(Integer vehiculo_convencional_id) {
		this.vehiculo_convencional_id = vehiculo_convencional_id;
	}
	public ClsRecDisposicionFinal getRecDisposicionFinal() {
		return recDisposicionFinal;
	}
	public void setRecDisposicionFinal(ClsRecDisposicionFinal recDisposicionFinal) {
		this.recDisposicionFinal = recDisposicionFinal;
	}
	public ClsRecValorizacion getRecValorizacion() {
		return recValorizacion;
	}
	public void setRecValorizacion(ClsRecValorizacion recValorizacion) {
		this.recValorizacion = recValorizacion;
	}
	public ClsRecRcdOm getRecRcdOm() {
		return recRcdOm;
	}
	public void setRecRcdOm(ClsRecRcdOm recRcdOm) {
		this.recRcdOm = recRcdOm;
	}
	public ClsTransferencia getTransferencia() {
		return transferencia;
	}
	public void setTransferencia(ClsTransferencia transferencia) {
		this.transferencia = transferencia;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public Integer getRecorrido_anual() {
		return recorrido_anual;
	}
	public void setRecorrido_anual(Integer recorrido_anual) {
		this.recorrido_anual = recorrido_anual;
	}
	public String getTipo_vehiculo() {
		return tipo_vehiculo;
	}
	public void setTipo_vehiculo(String tipo_vehiculo) {
		this.tipo_vehiculo = tipo_vehiculo;
	}
	public Integer getAnio_fabricacion() {
		return anio_fabricacion;
	}
	public void setAnio_fabricacion(Integer anio_fabricacion) {
		this.anio_fabricacion = anio_fabricacion;
	}
	public Double getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(Double capacidad) {
		this.capacidad = capacidad;
	}
	public Integer getTipo_combustible() {
		return tipo_combustible;
	}
	public void setTipo_combustible(Integer tipo_combustible) {
		this.tipo_combustible = tipo_combustible;
	}
	public Double getCantidad_combustible() {
		return cantidad_combustible;
	}
	public void setCantidad_combustible(Double cantidad_combustible) {
		this.cantidad_combustible = cantidad_combustible;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Double getRendimiento_anual() {
		return rendimiento_anual;
	}
	public void setRendimiento_anual(Double rendimiento_anual) {
		this.rendimiento_anual = rendimiento_anual;
	}
	public Double getCapacidad_viaje() {
		return capacidad_viaje;
	}
	public void setCapacidad_viaje(Double capacidad_viaje) {
		this.capacidad_viaje = capacidad_viaje;
	}
	public Integer getEfectividad() {
		return efectividad;
	}
	public void setEfectividad(Integer efectividad) {
		this.efectividad = efectividad;
	}
	public Double getPromedio_viajes() {
		return promedio_viajes;
	}
	public void setPromedio_viajes(Double promedio_viajes) {
		this.promedio_viajes = promedio_viajes;
	}
	public Double getPromedio_turno() {
		return promedio_turno;
	}
	public void setPromedio_turno(Double promedio_turno) {
		this.promedio_turno = promedio_turno;
	}
	public Integer getDias_trabajados() {
		return dias_trabajados;
	}
	public void setDias_trabajados(Integer dias_trabajados) {
		this.dias_trabajados = dias_trabajados;
	}
	public Double getRecoleccion_media() {
		return recoleccion_media;
	}
	public void setRecoleccion_media(Double recoleccion_media) {
		this.recoleccion_media = recoleccion_media;
	}
	public Double getRecoleccion_anual() {
		return recoleccion_anual;
	}
	public void setRecoleccion_anual(Double recoleccion_anual) {
		this.recoleccion_anual = recoleccion_anual;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
