package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsOtroMunicipales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.OtroMunicipalesDAO;

public class OtroMunicipalesService {

	private static Logger log = Logger.getLogger(OtroMunicipalesService.class);
	OtroMunicipalesDAO otroMunicipalesDAO = new OtroMunicipalesDAO();

	public ClsResultado obtener(ClsOtroMunicipales clsOtroMunicipales){
		ClsResultado clsResultado = null;
		try {
			clsResultado = otroMunicipalesDAO.obtener(clsOtroMunicipales);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsOtroMunicipales clsOtroMunicipales) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = otroMunicipalesDAO.insertar(clsOtroMunicipales);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsOtroMunicipales clsOtroMunicipales) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = otroMunicipalesDAO.actualizar(clsOtroMunicipales);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
