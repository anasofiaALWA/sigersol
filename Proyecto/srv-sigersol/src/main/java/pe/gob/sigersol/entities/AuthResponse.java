package pe.gob.sigersol.entities;

import pe.gob.sigersol.util.StringUtility;

public class AuthResponse {
	private String token;
	private ClsUsuario usuario;
	private ClsSesion sesion;

	public AuthResponse(ClsUsuario usuario) {
		this.usuario = usuario;
		this.token = usuario.getUsuario_id() + "@" + StringUtility.generateRamdonString();
	}

	public AuthResponse(ClsSesion sesion) {
		this.sesion = sesion;
		this.token = sesion.getSesion_id() + "@" + StringUtility.generateRamdonString();
	}

	public AuthResponse() {
		// TODO Auto-generated constructor stub
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public ClsUsuario getUsuario() {
		return usuario;
	}

	public void setUsuario(ClsUsuario usuario) {
		this.usuario = usuario;
	}

	public ClsSesion getSesion() {
		return sesion;
	}

	public void setSesion(ClsSesion sesion) {
		this.sesion = sesion;
	}

}
