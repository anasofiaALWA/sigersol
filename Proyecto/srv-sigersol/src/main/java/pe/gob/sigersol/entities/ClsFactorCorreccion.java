package pe.gob.sigersol.entities;

public class ClsFactorCorreccion {
	private Integer factorCorreccionId;
	private String condicionId;
	private Double valor_phi;
	private String descripcion;
	
	public Integer getFactorCorreccionId() {
		return factorCorreccionId;
	}
	public void setFactorCorreccionId(Integer factorCorreccionId) {
		this.factorCorreccionId = factorCorreccionId;
	}
	public String getCondicionId() {
		return condicionId;
	}
	public void setCondicionId(String condicionId) {
		this.condicionId = condicionId;
	}
	public Double getValor_phi() {
		return valor_phi;
	}
	public void setValor_phi(Double valor_phi) {
		this.valor_phi = valor_phi;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
