package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsProgramaSegregacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;

public class ProgramaSegregacionDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(ProgramaSegregacionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_segregacion = "{call SIGERSOLBL.SGR_SP_LIST_SGR_SEGREGACION(?,?)}";
	private final String sgr_sp_ins_segregacion = "{call SIGERSOLBL.SGR_SP_INS_SEGREGACION(?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_segregacion = "{call SIGERSOLBL.SGR_SP_UPD_SEGREGACION(?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsProgramaSegregacion clsProgramaSegregacion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsProgramaSegregacion item = new ClsProgramaSegregacion();
		ClsValorizacion valorizacion = new ClsValorizacion();
		con = null;
		String sql = this.sgr_sp_list_sgr_segregacion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsProgramaSegregacion.getValorizacion().getValorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setPrograma_segregacion_id(rs.getInt("PROGRAMA_SEGREGACION_ID"));
					valorizacion.setValorizacion_id(rs.getInt("VALORIZACION_ID"));
					item.setValorizacion(valorizacion);
					item.setCaf_adm_propia(rs.getInt("CAF_ADM_PROPIA"));
					item.setCaf_tercerizado(rs.getInt("CAF_TERCERIZADO"));
					item.setCaf_mixto(rs.getInt("CAF_MIXTO"));
					item.setRecicladores_hombre_25(rs.getInt("RECICLADORES_HOMBRE_25"));
					item.setRecicladores_mujer_25(rs.getInt("RECICLADORES_MUJER_25"));
					item.setRecicladores_hombre_25_65(rs.getInt("RECICLADORES_HOMBRE_25_65"));
					item.setRecicladores_mujer_25_65(rs.getInt("RECICLADORES_MUJER_25_65"));
					item.setRecicladores_hombre_65(rs.getInt("RECICLADORES_HOMBRE_65"));
					item.setRecicladores_mujer_65(rs.getInt("RECICLADORES_MUJER_65"));
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsProgramaSegregacion clsProgramaSegregacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_segregacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsProgramaSegregacion.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsProgramaSegregacion.getCaf_adm_propia());
			cs.setInt(3, clsProgramaSegregacion.getCaf_tercerizado());
			cs.setInt(4, clsProgramaSegregacion.getCaf_mixto());
			cs.setInt(5, clsProgramaSegregacion.getRecicladores_hombre_25());
			cs.setInt(6, clsProgramaSegregacion.getRecicladores_mujer_25());
			cs.setInt(7, clsProgramaSegregacion.getRecicladores_hombre_25_65());
			cs.setInt(8, clsProgramaSegregacion.getRecicladores_mujer_25_65());
			cs.setInt(9, clsProgramaSegregacion.getRecicladores_hombre_65());
			cs.setInt(10, clsProgramaSegregacion.getRecicladores_mujer_65());
			cs.setInt(11, clsProgramaSegregacion.getCodigo_usuario());
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(12);
			
			log.info("ProgramaSegregacionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsProgramaSegregacion clsProgramaSegregacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_segregacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsProgramaSegregacion.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsProgramaSegregacion.getCaf_adm_propia());
			cs.setInt(3, clsProgramaSegregacion.getCaf_tercerizado());
			cs.setInt(4, clsProgramaSegregacion.getCaf_mixto());
			cs.setInt(5, clsProgramaSegregacion.getRecicladores_hombre_25());
			cs.setInt(6, clsProgramaSegregacion.getRecicladores_mujer_25());
			cs.setInt(7, clsProgramaSegregacion.getRecicladores_hombre_25_65());
			cs.setInt(8, clsProgramaSegregacion.getRecicladores_mujer_25_65());
			cs.setInt(9, clsProgramaSegregacion.getRecicladores_hombre_65());
			cs.setInt(10, clsProgramaSegregacion.getRecicladores_mujer_65());
			cs.setInt(11, clsProgramaSegregacion.getCodigo_usuario());
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(12);

			log.info("ProgramaSegregacionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
