package pe.gob.sigersol.entities;

public class ClsTipoContenedor {

	private Integer tipo_contenedor_id;
	private String tipo_contenedor;
	private String estado_contenedor;
	
	public Integer getTipo_contenedor_id() {
		return tipo_contenedor_id;
	}
	public void setTipo_contenedor_id(Integer tipo_contenedor_id) {
		this.tipo_contenedor_id = tipo_contenedor_id;
	}
	public String getTipo_contenedor() {
		return tipo_contenedor;
	}
	public void setTipo_contenedor(String tipo_contenedor) {
		this.tipo_contenedor = tipo_contenedor;
	}
	public String getEstado_contenedor() {
		return estado_contenedor;
	}
	public void setEstado_contenedor(String estado_contenedor) {
		this.estado_contenedor = estado_contenedor;
	}
}
