package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsGeneracionNoDomiciliar;
import pe.gob.sigersol.entities.ClsResSolidos;
import pe.gob.sigersol.entities.ClsResultado;

public class ResSolidosDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(ResSolidosDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_rs = "{call SIGERSOLBL.SGR_SP_LIST_RS(?,?,?)}";
	private final String sgr_sp_ins_composicion = "{call SIGERSOLBL.SGR_SP_INS_COMPOSICION(?,?,?,?,?)}";
	private final String sgr_sp_upd_composicion = "{call SIGERSOLBL.SGR_SP_UPD_COMPOSICION(?,?,?,?,?)}";
	
	
	public ClsResultado obtener(ClsResSolidos clsResSolidos) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsResSolidos> lista = new ArrayList<ClsResSolidos>();
		
		con = null;
		String sql = this.sgr_sp_list_rs;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResSolidos.getTipors_id());
			cs.setInt(2, clsResSolidos.getGeneracion().getGeneracion_id());
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);

			while (rs.next()) {
				ClsResSolidos item = new ClsResSolidos();
				ClsGeneracion generacion = new ClsGeneracion();
				try {
					//item.setResiduos_solidos_id(rs.getInt("RESIDUOS_SOLIDOS_ID"));
					item.setTipors_id(rs.getInt("TIPORS_ID"));
					item.setNombre(rs.getString("TIPORS_NOMBRE"));
					item.setInformacion(rs.getString("INFORMACION"));
					item.setPadre_id(rs.getInt("PADRE_ID"));
					item.setNivel(rs.getInt("NIVEL"));
					item.setTiene_hijos(rs.getInt("TIENE_HIJOS"));
					if(clsResSolidos.getGeneracion().getGeneracion_id() != -1){
						item.setPorcentaje(rs.getDouble("PORCENTAJE"));
						generacion.setGeneracion_id(rs.getInt("GENERACION_ID"));
						item.setGeneracion(generacion);
					}
					else{
						item.setPorcentaje(null);
						generacion.setGeneracion_id(-1);
						item.setGeneracion(generacion);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
				
			}
			
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsResSolidos clsResSolidos) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_composicion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResSolidos.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsResSolidos.getTipors_id());
			cs.setDouble(3, clsResSolidos.getPorcentaje());
			cs.setInt(4, clsResSolidos.getCod_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);
			
			log.info("ResSolidosDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsResSolidos clsResSolidos) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_composicion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResSolidos.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsResSolidos.getTipors_id());
			cs.setDouble(3, clsResSolidos.getPorcentaje());
			cs.setInt(4, clsResSolidos.getCod_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);

			log.info("ResSolidosDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
