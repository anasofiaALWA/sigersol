package pe.gob.sigersol.entities;

public class ClsInversion {

	private Integer inversion_id;
	private ClsAdmFinanzas adm_finanzas;
	private ClsTipoInversion tipo_inversion;
	private Integer monto_inv;
	private Integer fuente_financ;
	private Integer codigo_usuario;
	
	
	public Integer getInversion_id() {
		return inversion_id;
	}
	public void setInversion_id(Integer inversion_id) {
		this.inversion_id = inversion_id;
	}
	public ClsAdmFinanzas getAdm_finanzas() {
		return adm_finanzas;
	}
	public void setAdm_finanzas(ClsAdmFinanzas adm_finanzas) {
		this.adm_finanzas = adm_finanzas;
	}
	public ClsTipoInversion getTipo_inversion() {
		return tipo_inversion;
	}
	public void setTipo_inversion(ClsTipoInversion tipo_inversion) {
		this.tipo_inversion = tipo_inversion;
	}
	public Integer getMonto_inv() {
		return monto_inv;
	}
	public void setMonto_inv(Integer monto_inv) {
		this.monto_inv = monto_inv;
	}
	public Integer getFuente_financ() {
		return fuente_financ;
	}
	public void setFuente_financ(Integer fuente_financ) {
		this.fuente_financ = fuente_financ;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
