package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAlmacenamiento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AlmacenamientoDAO;

public class AlmacenamientoService {

	private static Logger log = Logger.getLogger(AlmacenamientoService.class);
	AlmacenamientoDAO almacenamientoDAO = new AlmacenamientoDAO();
	
	public ClsResultado obtener(ClsAlmacenamiento clsAlmacenamiento){
		ClsResultado clsResultado = null;
		try {
			clsResultado = almacenamientoDAO.obtener(clsAlmacenamiento);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAlmacenamiento clsAlmacenamiento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = almacenamientoDAO.insertar(clsAlmacenamiento);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsAlmacenamiento clsAlmacenamiento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = almacenamientoDAO.actualizar(clsAlmacenamiento);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
