package pe.gob.sigersol.generales.service;

import java.util.List;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResSolidosEspeciales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ResSolidosEspecialesDAO;

public class ResSolidosEspecialesService {

	private static Logger log = Logger.getLogger(ComposicionNDService.class);
	ResSolidosEspecialesDAO resSolidosEspecialesDAO = new ResSolidosEspecialesDAO();
	
	public ClsResultado obtener(ClsResSolidosEspeciales clsResSolidosEspeciales){
		ClsResultado clsResultado = null;
		ClsResultado clsResultado2 = null;
		ClsResultado clsResultado3 = null;
		ClsResultado clsResultado4 = null;
		ClsResultado clsResultadoFinal = null;
		
		List<ClsResSolidosEspeciales> listaPadre = null;
		List<ClsResSolidosEspeciales> listaHijo = null;
		List<ClsResSolidosEspeciales> listaNieto = null;
		List<ClsResSolidosEspeciales> listaBisnieto = null;
		
		ClsGeneracion generacion = null;
		
		try {
			
			clsResultadoFinal = new ClsResultado();
			Integer gid = clsResSolidosEspeciales.getGeneracion().getGeneracion_id();
			
			clsResultado = resSolidosEspecialesDAO.obtener(clsResSolidosEspeciales);
			
			listaPadre = (List<ClsResSolidosEspeciales>) clsResultado.getObjeto();
			
			for(ClsResSolidosEspeciales item: listaPadre){
				if(item.getTiene_hijos() == 1){
					
					generacion = new ClsGeneracion();
					generacion.setGeneracion_id(gid);
					item.setGeneracion(generacion);
										
					clsResultado2 = resSolidosEspecialesDAO.obtener(item);
					
					listaHijo = (List<ClsResSolidosEspeciales>) clsResultado2.getObjeto();
					
					item.setHijos(listaHijo);
					
					for(ClsResSolidosEspeciales item2 : listaHijo){
							
						if(item2.getTiene_hijos() == 1){
						
							generacion = new ClsGeneracion();
							generacion.setGeneracion_id(gid);
							item2.setGeneracion(generacion);
							
							clsResultado3 = resSolidosEspecialesDAO.obtener(item2);
								
							listaNieto = (List<ClsResSolidosEspeciales>) clsResultado3.getObjeto();
							
							item2.setHijos(listaNieto);
							
							for(ClsResSolidosEspeciales item3 : listaNieto){
								
								if(item3.getTiene_hijos() == 1){
								
									generacion = new ClsGeneracion();
									generacion.setGeneracion_id(gid);
									item3.setGeneracion(generacion);
									
									clsResultado4 = resSolidosEspecialesDAO.obtener(item3);
										
									listaBisnieto = (List<ClsResSolidosEspeciales>) clsResultado4.getObjeto();
									
									item3.setHijos(listaBisnieto);
								}
							}
						}
					}
					
				}
			}
			
			clsResultadoFinal.setObjeto(listaPadre);
			clsResultadoFinal.setExito(true);
			clsResultadoFinal.setMensaje("Obtención exitosa");
			
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		
		return clsResultadoFinal;
	}
	
	public ClsResultado insertar(ClsResSolidosEspeciales clsResSolidosEspeciales) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = resSolidosEspecialesDAO.insertar(clsResSolidosEspeciales);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsResSolidosEspeciales clsResSolidosEspeciales) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = resSolidosEspecialesDAO.actualizar(clsResSolidosEspeciales);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
