package pe.gob.sigersol.generales.service;

import java.util.ArrayList;
import java.util.List;

import org.jfree.util.Log;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUnicodeParametro;
import pe.gob.sigersol.entities.dao.UnicodeParametroDAO;

public class UnicodeParametroService {
	UnicodeParametroDAO unicodeParametroDAO = new UnicodeParametroDAO();

	public List<ClsUnicodeParametro> obtener_lista_parametros(String nombre_tabla) {

		List<ClsUnicodeParametro> lista = null;

		try {

			lista = new ArrayList<ClsUnicodeParametro>();

			lista = unicodeParametroDAO.obtener_lista_parametros(nombre_tabla);

		} catch (Exception e) {
			Log.info("message obtener_tabla" + e.getMessage());
		}

		return lista;

	}

	public List<ClsUnicodeParametro> obtener_lista_tablas() {

		List<ClsUnicodeParametro> lista = null;

		try {

			lista = new ArrayList<ClsUnicodeParametro>();

			lista = unicodeParametroDAO.obtener_lista_tablas();

		} catch (Exception e) {
			Log.info("message obtener_tabla" + e.getMessage());
		}

		return lista;

	}

	public ClsResultado insertar_tabla(ClsUnicodeParametro clsUnicodeParametro) {

		ClsResultado clsResultado = null;

		try {

			clsResultado = unicodeParametroDAO.insertar_tabla(clsUnicodeParametro);

		} catch (Exception e) {
			Log.info("message insertar_tabla" + e.getMessage());
		}

		return clsResultado;
	}

	public ClsResultado actualizar_tabla(String nombre_tabla_anterior, String nombre_tabla_nueva) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = unicodeParametroDAO.actualizar_tabla(nombre_tabla_anterior, nombre_tabla_nueva);
		} catch (Exception e) {
			Log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}

	public ClsResultado actualizar_parametro(ClsUnicodeParametro clsUnicodeParametro) {

		ClsResultado clsResultado = null;

		try {

			clsResultado = unicodeParametroDAO.actualizar_parametro(clsUnicodeParametro);

		} catch (Exception e) {
			Log.info("message insertar_tabla" + e.getMessage());
		}

		return clsResultado;
	}

	/**
	 * @Método : obtener_parametro
	 * @Descripción : obtiene registros de la tabla unicode_parametros.
	 * @Autor - Fecha Creación :MCEA - 16/03/2016
	 * @Autor - Fecha Modificacion :
	 * @Comentario de la modificación:
	 */
	public String obtener_parametros(String nombre_tabla, String codigo_parametro) {
		String valorParametro = "";

		try {
			UnicodeParametroDAO unicodeParamDAO = new UnicodeParametroDAO();
			List<ClsUnicodeParametro> lista = new ArrayList<ClsUnicodeParametro>();

			lista = unicodeParamDAO.obtener_lista_parametros(nombre_tabla);
			for (ClsUnicodeParametro item : lista) {
				if (item.getCodigo_parametro().compareTo(codigo_parametro) == 0) {
					valorParametro = item.getValor_parametro();
					break;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return valorParametro;
	}

	
}
