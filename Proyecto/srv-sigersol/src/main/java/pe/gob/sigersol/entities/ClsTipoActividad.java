package pe.gob.sigersol.entities;

public class ClsTipoActividad {

	private Integer tipo_actividad_id;
	private String nombre_tipo;
	private Integer flag_actividad;
	
	public Integer getTipo_actividad_id() {
		return tipo_actividad_id;
	}
	public void setTipo_actividad_id(Integer tipo_actividad_id) {
		this.tipo_actividad_id = tipo_actividad_id;
	}
	public String getNombre_tipo() {
		return nombre_tipo;
	}
	public void setNombre_tipo(String nombre_tipo) {
		this.nombre_tipo = nombre_tipo;
	}
	public Integer getFlag_actividad() {
		return flag_actividad;
	}
	public void setFlag_actividad(Integer flag_actividad) {
		this.flag_actividad = flag_actividad;
	}
}
