package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResiduosEspeciales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduosEspeciales;
import pe.gob.sigersol.generales.service.ResiduosEspecialesService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/residuosEspecialesX")
@RequestScoped
public class ResiduosEspecialesRestService {

	private static Logger log = Logger.getLogger(SolicitudRestService.class.getName());
	private ClsResiduosEspeciales clsResiduosEspeciales;
	private ClsTipoResiduosEspeciales clsTipoResiduosEspeciales;
	private ClsGeneracion clsGeneracion;
	private ResiduosEspecialesService residuosEspecialesService;
	private List<ClsResiduosEspeciales> listaResiduosEspeciales;

	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(@Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener Residuos Especiales**");
			
			clsResultado = residuosEspecialesService.obtener();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"residuosEspeciales\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/listarResiduosEspeciales")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarResiduosEspeciales(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			residuosEspecialesService = new ResiduosEspecialesService();
			
			jsonEntradaListaResEspeciales(paramJson);
			
			log.info("**Obtener Generacion No Domiciliar");
			
			clsResultado = residuosEspecialesService.listarResiduosEspeciales(clsResiduosEspeciales);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstResEspeciales\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		residuosEspecialesService = new ResiduosEspecialesService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaResEspeciales(paramJson);

			// if (accion == 1) {
			// clsResultado = academicoService.insertar(clsAcademico);
			// } else {
			// clsResultado = academicoService.actualizar(clsAcademico);
			// }
			//
			
			for (ClsResiduosEspeciales clsResiduosEspeciales : listaResiduosEspeciales) {
				clsResultado = residuosEspecialesService.insertar(clsResiduosEspeciales);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		residuosEspecialesService = new ResiduosEspecialesService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaResEspeciales(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsResiduosEspeciales clsResiduosEspeciales : listaResiduosEspeciales) {
			clsResultado = residuosEspecialesService.actualizar(clsResiduosEspeciales);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	
	private void jsonEntradaResEspeciales(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ResiduosEspecialesRestService : jsonEntradaResEspeciales" + jsonEntrada);

		JSONObject jsonObjectTipoEspeciales;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaResiduosEspeciales = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsResiduosEspeciales = new ClsResiduosEspeciales();
			clsTipoResiduosEspeciales = new ClsTipoResiduosEspeciales();
			clsGeneracion = new ClsGeneracion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
			}
			
			clsResiduosEspeciales.setGeneracion(clsGeneracion);
			
			if (innerObject.get("tipo_especiales") != null && innerObject.get("tipo_especiales").toString() != "") {
				jsonObjectTipoEspeciales  = (JSONObject) innerObject.get("tipo_especiales");
				clsTipoResiduosEspeciales.setTipo_residuos_especiales_id(Integer.parseInt(jsonObjectTipoEspeciales.get("tipo_residuos_especiales_id").toString()));
			}
			clsResiduosEspeciales.setTipo_especiales(clsTipoResiduosEspeciales);
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsResiduosEspeciales.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else {
				clsResiduosEspeciales.setCantidad(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsResiduosEspeciales.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaResiduosEspeciales.add(clsResiduosEspeciales);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	private void jsonEntradaListaResEspeciales(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ResiduosEspecialesRestService : jsonEntradaListaResEspeciales" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResiduosEspeciales;
		JSONParser jsonParser = new JSONParser();
		
		clsResiduosEspeciales = new ClsResiduosEspeciales();
		clsTipoResiduosEspeciales = new ClsTipoResiduosEspeciales();
		clsGeneracion = new ClsGeneracion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResiduosEspeciales = (JSONObject) jsonObjectPrincipal.get("ResiduosEspeciales");
			
			if (jsonObjectResiduosEspeciales.get("generacion_id") != null && jsonObjectResiduosEspeciales.get("generacion_id") != "" &&
					jsonObjectResiduosEspeciales.get("generacion_id") != " ") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(jsonObjectResiduosEspeciales.get("generacion_id").toString()));
			}
			else
				clsGeneracion.setGeneracion_id(-1);
			
			clsResiduosEspeciales.setGeneracion(clsGeneracion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
