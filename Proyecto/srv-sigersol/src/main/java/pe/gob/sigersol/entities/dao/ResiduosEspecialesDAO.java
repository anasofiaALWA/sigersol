package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResiduosEspeciales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduosEspeciales;

public class ResiduosEspecialesDAO extends GenericoDAO{

	private static Logger log = Logger.getLogger(ResiduosEspecialesDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_res_especiales = "{call SIGERSOLBL.SGR_SP_LIST_SGR_RES_ESPECIALES(?)}";
	private final String sgr_sp_ins_res_especiales = "{call SIGERSOLBL.SGR_SP_INS_RES_ESPECIALES(?,?,?,?,?)}";
	private final String sgr_sp_upd_res_especiales = "{call SIGERSOLBL.SGR_SP_UPD_RES_ESPECIALES(?,?,?,?,?)}";
	
	private final String sgr_sp_list_res_especiales = "{call SIGERSOLBL.SGR_SP_LIST_RES_ESPECIALES(?,?)}";  
	
	public ClsResultado obtener() {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsResiduosEspeciales> lista = new ArrayList<ClsResiduosEspeciales>();
		con = null;
		String sql = this.sgr_sp_list_sgr_res_especiales;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);

			while (rs.next()) {
				ClsResiduosEspeciales item = new ClsResiduosEspeciales();
				try {
					item.setResiduos_especiales_id(rs.getInt("RESIDUOS_ESPECIALES_ID"));
					item.setCantidad(rs.getInt("CANTIDAD"));
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado listarResiduosEspeciales(ClsResiduosEspeciales clsResiduosEspeciales){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsResiduosEspeciales> lista = new ArrayList<ClsResiduosEspeciales>();
		con = null;
		String sql = this.sgr_sp_list_res_especiales;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosEspeciales.getGeneracion().getGeneracion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsResiduosEspeciales item = new ClsResiduosEspeciales();
				ClsTipoResiduosEspeciales tipoResiduosEspeciales = new ClsTipoResiduosEspeciales();
				
				try {
					tipoResiduosEspeciales.setTipo_residuos_especiales_id(rs.getInt("TIPO_ESPECIALES_ID"));
					tipoResiduosEspeciales.setNombre_tipo(rs.getString("NOMBRE_ESPECIALES"));
					item.setTipo_especiales(tipoResiduosEspeciales);
					if(clsResiduosEspeciales.getGeneracion().getGeneracion_id() != -1)
						item.setCantidad(rs.getInt("CANTIDAD"));
					else
						item.setCantidad(null);
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsResiduosEspeciales clsResiduosEspeciales) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_res_especiales;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosEspeciales.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsResiduosEspeciales.getTipo_especiales().getTipo_residuos_especiales_id());
			cs.setInt(3, clsResiduosEspeciales.getCantidad());
			cs.setInt(4, clsResiduosEspeciales.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);
			
			log.info("ResiduosEspecialesDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsResiduosEspeciales clsResiduosEspeciales) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_res_especiales;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosEspeciales.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsResiduosEspeciales.getTipo_especiales().getTipo_residuos_especiales_id());
			cs.setInt(3, clsResiduosEspeciales.getCantidad());
			cs.setInt(4, clsResiduosEspeciales.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);

			log.info("ResiduosEspecialesDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
