package pe.gob.sigersol.entities;

public class ClsLugarAprovechamiento {

	private Integer lugar_aprovechamiento_id;
	private ClsValorizacion valorizacion;
	private Integer adm_propia;
	private Integer tercerizado_eor;
	private Integer tercerizado_asoc_recic;
	private Integer tercerizado;
	private Integer mixto;
	private Integer flag_centro_acopio;
	private Integer flag_planta;
	private String zona_coordenada;
	private String norte_coordenada;
	private String sur_coordenada;
	private String div_politica_dep;
	private String div_politica_prov;
	private String div_politica_dist;
	private String direccion_iga;
	private String referencia_iga;
	private Integer numero_recicladores;
	private Integer codigo_usuario;
	
	public Integer getLugar_aprovechamiento_id() {
		return lugar_aprovechamiento_id;
	}
	public void setLugar_aprovechamiento_id(Integer lugar_aprovechamiento_id) {
		this.lugar_aprovechamiento_id = lugar_aprovechamiento_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public Integer getAdm_propia() {
		return adm_propia;
	}
	public void setAdm_propia(Integer adm_propia) {
		this.adm_propia = adm_propia;
	}
	public Integer getTercerizado_eor() {
		return tercerizado_eor;
	}
	public void setTercerizado_eor(Integer tercerizado_eor) {
		this.tercerizado_eor = tercerizado_eor;
	}
	public Integer getTercerizado_asoc_recic() {
		return tercerizado_asoc_recic;
	}
	public void setTercerizado_asoc_recic(Integer tercerizado_asoc_recic) {
		this.tercerizado_asoc_recic = tercerizado_asoc_recic;
	}
	public Integer getTercerizado() {
		return tercerizado;
	}
	public void setTercerizado(Integer tercerizado) {
		this.tercerizado = tercerizado;
	}
	public Integer getMixto() {
		return mixto;
	}
	public void setMixto(Integer mixto) {
		this.mixto = mixto;
	}
	public Integer getFlag_centro_acopio() {
		return flag_centro_acopio;
	}
	public void setFlag_centro_acopio(Integer flag_centro_acopio) {
		this.flag_centro_acopio = flag_centro_acopio;
	}
	public Integer getFlag_planta() {
		return flag_planta;
	}
	public void setFlag_planta(Integer flag_planta) {
		this.flag_planta = flag_planta;
	}
	public String getZona_coordenada() {
		return zona_coordenada;
	}
	public void setZona_coordenada(String zona_coordenada) {
		this.zona_coordenada = zona_coordenada;
	}
	public String getNorte_coordenada() {
		return norte_coordenada;
	}
	public void setNorte_coordenada(String norte_coordenada) {
		this.norte_coordenada = norte_coordenada;
	}
	public String getSur_coordenada() {
		return sur_coordenada;
	}
	public void setSur_coordenada(String sur_coordenada) {
		this.sur_coordenada = sur_coordenada;
	}
	public String getDiv_politica_dep() {
		return div_politica_dep;
	}
	public void setDiv_politica_dep(String div_politica_dep) {
		this.div_politica_dep = div_politica_dep;
	}
	public String getDiv_politica_prov() {
		return div_politica_prov;
	}
	public void setDiv_politica_prov(String div_politica_prov) {
		this.div_politica_prov = div_politica_prov;
	}
	public String getDiv_politica_dist() {
		return div_politica_dist;
	}
	public void setDiv_politica_dist(String div_politica_dist) {
		this.div_politica_dist = div_politica_dist;
	}
	public String getDireccion_iga() {
		return direccion_iga;
	}
	public void setDireccion_iga(String direccion_iga) {
		this.direccion_iga = direccion_iga;
	}
	public String getReferencia_iga() {
		return referencia_iga;
	}
	public void setReferencia_iga(String referencia_iga) {
		this.referencia_iga = referencia_iga;
	}
	public Integer getNumero_recicladores() {
		return numero_recicladores;
	}
	public void setNumero_recicladores(Integer numero_recicladores) {
		this.numero_recicladores = numero_recicladores;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
