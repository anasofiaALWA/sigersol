package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAdministracionMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AdministracionMunicipalidadDAO;

public class AdministracionMunicipalidadService {
	private static Logger log = Logger.getLogger(AdministracionMunicipalidadService.class);
	AdministracionMunicipalidadDAO administracionMunicipalidadDAO = new AdministracionMunicipalidadDAO();
	
	public ClsResultado obtener(){
		ClsResultado clsResultado = null;
		try {
			clsResultado = administracionMunicipalidadDAO.obtener();
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAdministracionMunicipalidad clsAdministracionMunicipalidad) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = administracionMunicipalidadDAO.insertar(clsAdministracionMunicipalidad);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsAdministracionMunicipalidad clsAdministracionMunicipalidad) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = administracionMunicipalidadDAO.actualizar(clsAdministracionMunicipalidad);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
