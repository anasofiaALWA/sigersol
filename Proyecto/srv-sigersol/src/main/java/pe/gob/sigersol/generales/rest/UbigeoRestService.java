package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCCGeneracion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUbigeo;
import pe.gob.sigersol.generales.service.UbigeoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/ubigeo")
@RequestScoped
public class UbigeoRestService {
	private static Logger log = Logger.getLogger(UbigeoRestService.class.getName());
	private UbigeoService ubigeoService;

	@POST
	@Path("/listPorUbigeo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarPorUbigeo(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ubigeoService = new UbigeoService();
		ClsUbigeo  clsUbigeo = jsonEntrada(paramJson);
		
		clsResultado = ubigeoService.listarPorUbigeo(clsUbigeo.getUbigeoId());
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();

	}
	
	
	@POST
	@Path("/listDepartamento")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarDepartamento(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response){
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ubigeoService = new UbigeoService();
		clsResultado = ubigeoService.listaDepartamento();
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();

	}
	
	@POST
	@Path("/listProvincia")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarProvincia(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ubigeoService = new UbigeoService();
		
		ClsUbigeo  clsUbigeo = jsonEntrada(paramJson);
		clsResultado = ubigeoService.listaProvincia(clsUbigeo.getDepartamento());
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();

	}
	
	@POST
	@Path("/listDistrito")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarDistrito(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ubigeoService = new UbigeoService();
		
		ClsUbigeo  clsUbigeo = jsonEntrada(paramJson);
		clsResultado = ubigeoService.listaDistrito(clsUbigeo.getDepartamento(), clsUbigeo.getProvincia());
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();

	}
	
	private ClsUbigeo jsonEntrada(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> UbigeoRestService :	jsonEntrada" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();
		ClsUbigeo ubigeo = new ClsUbigeo();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Ubigeo");
			if(jsonObject.get("CodDep") != null && jsonObject.get("CodDep") != ""){
				ubigeo.setDepartamento(jsonObject.get("CodDep").toString());				
			}
			if(jsonObject.get("CodProv") != null && jsonObject.get("CodProv") != ""){
				ubigeo.setProvincia(jsonObject.get("CodProv").toString());				
			}
			if(jsonObject.get("ubigeo_id") != null && jsonObject.get("ubigeo_id") != ""){
				ubigeo.setUbigeoId(jsonObject.get("ubigeo_id").toString());				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ubigeo;
		
	}

}
