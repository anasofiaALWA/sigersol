package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUbigeo;

public class UbigeoDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(UbigeoDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_sel_ubigeo = "{call SIGERSOLBL.SGR_SEL_UBIGEO(?,?)}";
	private final String sgr_sp_sel_departamento = "{call SIGERSOLBL.SGR_SEL_DEPARTAMENTO(?)}";
	private final String sgr_sp_sel_provincia = "{call SIGERSOLBL.SGR_SEL_PROVINCIA(?,?)}";
	private final String sgr_sp_sel_distrito = "{call SIGERSOLBL.SGR_SEL_DISTRITO(?,?,?)}";

	public ClsResultado listarPorUbigeo(String ubigeo_id) {
		ClsResultado clsResultado = new ClsResultado();
		ClsUbigeo ubigeo = null;
		con = null;
		String sql = this.sgr_sp_sel_ubigeo;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, ubigeo_id);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ubigeo = new ClsUbigeo();
				try {
					ubigeo.setDepartamento(rs.getString("DEPARTAMENTO"));
					ubigeo.setProvincia(rs.getString("PROVINCIA"));
					ubigeo.setDistrito(rs.getString("DISTRITO"));
				} catch (Exception e) {

				}
			}

			clsResultado.setExito(true);
			clsResultado.setObjeto(ubigeo);
			clsResultado.setMensaje("Obtención exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 listarPorUbigeo: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listarPorUbigeo: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listarPorUbigeo: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listarPorUbigeo: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	public ClsResultado listaDepartamento() {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsUbigeo> lista = new ArrayList<ClsUbigeo>();
		ClsUbigeo ubigeo = null;
		con = null;
		String sql = this.sgr_sp_sel_departamento;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);

			while (rs.next()) {
				ubigeo = new ClsUbigeo();
				try {
					ubigeo.setDepartamento(rs.getString("DEPARTAMENTO"));
				} catch (Exception e) {

				}
				lista.add(ubigeo);
			}

			clsResultado.setExito(true);
			clsResultado.setObjeto(lista);
			clsResultado.setMensaje("Obtención exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 listaDepartamento: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listaDepartamento: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listaDepartamento: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listaDepartamento: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado listaProvincia(String departamento) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsUbigeo> lista = new ArrayList<ClsUbigeo>();
		ClsUbigeo ubigeo = null;
		con = null;
		String sql = this.sgr_sp_sel_provincia;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, departamento);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ubigeo = new ClsUbigeo();
				try {
					ubigeo.setProvincia(rs.getString("PROVINCIA"));
				} catch (Exception e) {

				}
				lista.add(ubigeo);
			}

			clsResultado.setExito(true);
			clsResultado.setObjeto(lista);
			clsResultado.setMensaje("Obtención exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 listaProvincia: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listaProvincia: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listaProvincia: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listaProvincia: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado listaDistrito(String departamento, String provincia) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsUbigeo> lista = new ArrayList<ClsUbigeo>();
		ClsUbigeo ubigeo = null;
		con = null;
		String sql = this.sgr_sp_sel_distrito;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, departamento);
			cs.setString(2, provincia);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);

			while (rs.next()) {
				ubigeo = new ClsUbigeo();
				try {
					ubigeo.setUbigeoId(rs.getString("UBIGEO_ID"));
				} catch (Exception e) {

				}
				try {
					ubigeo.setDistrito(rs.getString("DISTRITO"));
				} catch (Exception e) {

				}
				try {
					ubigeo.setClasif_municipal(rs.getString("CLASIFICACION_MUNICIPAL"));
				} catch (Exception e) {

				}
				try {
					ubigeo.setTemperatura(rs.getDouble("CLASIFICACION_MUNICIPAL"));
				} catch (Exception e) {

				}
				lista.add(ubigeo);
			}

			clsResultado.setExito(true);
			clsResultado.setObjeto(lista);
			clsResultado.setMensaje("Obtención exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 listaDistrito: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listaDistrito: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listaDistrito: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listaDistrito: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
