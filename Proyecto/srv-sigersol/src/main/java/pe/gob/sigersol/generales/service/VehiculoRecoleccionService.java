package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoRecoleccion;
import pe.gob.sigersol.entities.dao.VehiculoRecoleccionDAO;

public class VehiculoRecoleccionService {
	
	private static Logger log = Logger.getLogger(VehiculoRecoleccionService.class);
	VehiculoRecoleccionDAO vehiculoRecoleccionDAO = new VehiculoRecoleccionDAO();
	
	public ClsResultado listarVehiculoRecoleccion(ClsVehiculoRecoleccion clsVehiculoRecoleccion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccionDAO.listarVehiculoRecoleccion(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoRecoleccion clsVehiculoRecoleccion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccionDAO.insertar(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsVehiculoRecoleccion clsVehiculoRecoleccion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccionDAO.actualizar(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
