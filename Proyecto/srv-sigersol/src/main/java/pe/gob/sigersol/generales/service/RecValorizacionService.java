package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsRecValorizacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.RecValorizacionDAO;

public class RecValorizacionService {

	private static Logger log = Logger.getLogger(RecValorizacionService.class);
	RecValorizacionDAO recValorizacionDAO = new RecValorizacionDAO();

	public ClsResultado obtener(ClsRecValorizacion clsRecValorizacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = recValorizacionDAO.obtener(clsRecValorizacion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsRecValorizacion clsRecValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = recValorizacionDAO.insertar(clsRecValorizacion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsRecValorizacion clsRecValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = recValorizacionDAO.actualizar(clsRecValorizacion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
