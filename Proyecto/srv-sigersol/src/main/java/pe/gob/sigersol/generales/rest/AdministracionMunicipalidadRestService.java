package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAdministracionMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.AdministracionMunicipalidadService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/administracionX")
@RequestScoped
public class AdministracionMunicipalidadRestService {
	
	private static Logger log = Logger.getLogger(SolicitudRestService.class.getName());
	private ClsAdministracionMunicipalidad clsAdministracionMunicipalidad;
	private AdministracionMunicipalidadService administracionMunicipalidadService;

	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(@Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener administracion**");
			
			clsResultado = administracionMunicipalidadService.obtener();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"administracion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		administracionMunicipalidadService = new AdministracionMunicipalidadService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaSolicitud(paramJson);
			clsResultado = administracionMunicipalidadService.insertar(clsAdministracionMunicipalidad);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(@FormParam("paramJson") String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaSolicitud(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = administracionMunicipalidadService.actualizar(clsAdministracionMunicipalidad);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	
	private void jsonEntradaSolicitud(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectSolicitud;
		JSONParser jsonParser = new JSONParser();
		
		clsAdministracionMunicipalidad = new ClsAdministracionMunicipalidad();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectSolicitud = (JSONObject) jsonObjectPrincipal.get("Administracion");

			if (jsonObjectSolicitud.get("municipalidad_id") != null && jsonObjectSolicitud.get("municipalidad_id") != "") {
				clsAdministracionMunicipalidad.setMunicipalidad_id(Integer.parseInt(jsonObjectSolicitud.get("municipalidad_id").toString()));
			}
			if (jsonObjectSolicitud.get("numeroOrdenanza") != null && jsonObjectSolicitud.get("numeroOrdenanza") != "") {
				clsAdministracionMunicipalidad.setNumero_ordenanza(jsonObjectSolicitud.get("numeroOrdenanza").toString());
			}
			if (jsonObjectSolicitud.get("periodoAnio") != null && jsonObjectSolicitud.get("periodoAnio") != "") {
				clsAdministracionMunicipalidad.setPeriodo_anio(Integer.parseInt(jsonObjectSolicitud.get("periodoAnio").toString()));
			}
			if (jsonObjectSolicitud.get("responsable") != null && jsonObjectSolicitud.get("responsable") != "") {
				clsAdministracionMunicipalidad.setResponsable(jsonObjectSolicitud.get("responsable").toString());
			}
			if (jsonObjectSolicitud.get("areaGerencia") != null && jsonObjectSolicitud.get("areaGerencia") != "") {
				clsAdministracionMunicipalidad.setArea_gerencia(jsonObjectSolicitud.get("areaGerencia").toString());
			}
			if (jsonObjectSolicitud.get("archivoOrdenanza") != null && jsonObjectSolicitud.get("archivoOrdenanza") != "") {
				clsAdministracionMunicipalidad.setArchivo_ordenanza(jsonObjectSolicitud.get("archivoOrdenanza").toString());
			}
			if (jsonObjectSolicitud.get("archivo_ordenanza_referencia") != null && jsonObjectSolicitud.get("archivo_ordenanza_referencia") != "") {
				clsAdministracionMunicipalidad.setArchivo_ordenanza_referencia(jsonObjectSolicitud.get("archivo_ordenanza_referencia").toString());
			}
			
			if (jsonObjectSolicitud.get("cod_usuario") != null && jsonObjectSolicitud.get("cod_usuario") != "") {
				clsAdministracionMunicipalidad.setCodigo_usuario(Integer.parseInt(jsonObjectSolicitud.get("cod_usuario").toString()));
			}
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
