package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;

public class DisposicionFinalAdmDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(DisposicionFinalAdmDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_admin_sdf = "{call SIGERSOLBL.SGR_SP_LIST_ADMIN_SDF(?,?)}";
	private final String sgr_sp_ins_admin_sdf = "{call SIGERSOLBL.SGR_SP_INS_ADMIN_SDF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_admin_sdf = "{call SIGERSOLBL.SGR_SP_UPD_ADMIN_SDF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		ClsResultado clsResultado = new ClsResultado();
		ClsDisposicionFinalAdm item = new ClsDisposicionFinalAdm();
		ClsSitioDisposicion sitioDisposicion = new ClsSitioDisposicion();
		con = null;
		String sql = this.sgr_sp_list_admin_sdf;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinalAdm.getSitio_disposicion_id().getSitio_disposicion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setDisposicion_final_id(rs.getInt("ADMIN_SDF_ID"));
					sitioDisposicion.setSitio_disposicion_id(rs.getInt("SITIO_DISPOSICION_ID"));
					item.setSitio_disposicion_id(sitioDisposicion);
					item.setAnio_inicio(rs.getInt("ANIO_INICIO"));
					item.setCant_personal(rs.getInt("CANT_PERSONAL"));
					item.setSituacion_terreno_id(rs.getString("SITUACION_TERRENO_ID"));
					item.setTemperatura_zona(rs.getInt("TEMPERATURA_ZONA"));
					item.setDireccion(rs.getString("DIRECCION"));
					item.setRef_direccion(rs.getString("REF_DIRECCION"));
					item.setArea_total_terreno(rs.getInt("AREA_TOTAL_TERRENO"));
					item.setArea_utilizada_terreno(rs.getInt("AREA_UTILIZADA_TERRENO"));
					item.setAltura_nf(rs.getInt("ALTURA_NF"));
					item.setTipo_manejo_eq_id(rs.getString("TIPO_MANEJO_EQ_ID"));
					item.setTipo_tecnologia_id(rs.getString("TIPO_TECNOLOGIA_ID"));
					item.setManeja_lixiviados(rs.getInt("MANEJA_LIXIVIADOS"));
					item.setTipo_manejo_lixiviados_id(rs.getString("TIPO_MANEJO_LIXIVIADOS_ID"));
					item.setManeja_gases(rs.getInt("MANEJA_GASES"));
					item.setNum_chimenea_inst(rs.getInt("NUM_CHIMENEA_INST"));
					item.setNum_chimenea_quem_op(rs.getInt("NUM_CHIMENEA_QUEM_OP"));
					item.setTipo_manejo_gases(rs.getString("TIPO_MANEJO_GASES"));
					item.setGenera_ee(rs.getInt("GENERA_EE"));
					item.setCap_instalada_ee_mw(rs.getInt("CAP_INSTALADA_EE_MW"));
					item.setGeneracion_ee_anual_mw(rs.getInt("GENERACION_EE_ANUAL_MWH"));

				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_admin_sdf;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinalAdm.getSitio_disposicion_id().getSitio_disposicion_id());
			cs.setInt(2, clsDisposicionFinalAdm.getAnio_inicio());
			cs.setInt(3, clsDisposicionFinalAdm.getCant_personal());
			cs.setString(4, clsDisposicionFinalAdm.getSituacion_terreno_id());
			cs.setInt(5, clsDisposicionFinalAdm.getTemperatura_zona());
			cs.setString(6, clsDisposicionFinalAdm.getDireccion());
			cs.setString(7, clsDisposicionFinalAdm.getRef_direccion());
			cs.setInt(8, clsDisposicionFinalAdm.getArea_total_terreno());
			cs.setInt(9, clsDisposicionFinalAdm.getArea_utilizada_terreno());
			cs.setInt(10, clsDisposicionFinalAdm.getAltura_nf());		
			cs.setString(11, clsDisposicionFinalAdm.getTipo_manejo_eq_id());			
			cs.setString(12, clsDisposicionFinalAdm.getTipo_tecnologia_id());
			cs.setInt(13, clsDisposicionFinalAdm.getManeja_lixiviados());
			cs.setString(14, clsDisposicionFinalAdm.getTipo_manejo_lixiviados_id());
			cs.setInt(15, clsDisposicionFinalAdm.getManeja_gases());
			cs.setInt(16, clsDisposicionFinalAdm.getNum_chimenea_inst());
			cs.setInt(17, clsDisposicionFinalAdm.getNum_chimenea_quem_op());
			cs.setString(18, clsDisposicionFinalAdm.getTipo_manejo_gases());
			cs.setInt(19, clsDisposicionFinalAdm.getGenera_ee());
			cs.setInt(20, clsDisposicionFinalAdm.getCap_instalada_ee_mw());
			cs.setInt(21, clsDisposicionFinalAdm.getGeneracion_ee_anual_mw());
			cs.setInt(22, clsDisposicionFinalAdm.getCodigo_usuario());
			cs.registerOutParameter(23, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(23);
			
			log.info("DisposicionFinalAdmDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_admin_sdf;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinalAdm.getDisposicion_final_id());
			cs.setInt(2, clsDisposicionFinalAdm.getSitio_disposicion_id().getSitio_disposicion_id());
			cs.setInt(3, clsDisposicionFinalAdm.getAnio_inicio());
			cs.setInt(4, clsDisposicionFinalAdm.getCant_personal());
			cs.setString(5, clsDisposicionFinalAdm.getSituacion_terreno_id());
			cs.setInt(6, clsDisposicionFinalAdm.getTemperatura_zona());
			cs.setString(7, clsDisposicionFinalAdm.getDireccion());
			cs.setString(8, clsDisposicionFinalAdm.getRef_direccion());
			cs.setInt(9, clsDisposicionFinalAdm.getArea_total_terreno());
			cs.setInt(10, clsDisposicionFinalAdm.getArea_utilizada_terreno());
			cs.setInt(11, clsDisposicionFinalAdm.getAltura_nf());
			cs.setString(12, clsDisposicionFinalAdm.getTipo_manejo_eq_id());
			cs.setString(13, clsDisposicionFinalAdm.getTipo_tecnologia_id());
			cs.setInt(14, clsDisposicionFinalAdm.getManeja_lixiviados());
			cs.setString(15, clsDisposicionFinalAdm.getTipo_manejo_lixiviados_id());
			cs.setInt(16, clsDisposicionFinalAdm.getManeja_gases());
			cs.setInt(17, clsDisposicionFinalAdm.getNum_chimenea_inst());
			cs.setInt(18, clsDisposicionFinalAdm.getNum_chimenea_quem_op());
			cs.setString(19, clsDisposicionFinalAdm.getTipo_manejo_gases());
			cs.setInt(20, clsDisposicionFinalAdm.getGenera_ee());
			cs.setInt(21, clsDisposicionFinalAdm.getCap_instalada_ee_mw());
			cs.setInt(22, clsDisposicionFinalAdm.getGeneracion_ee_anual_mw());
			cs.setInt(23, clsDisposicionFinalAdm.getCodigo_usuario());
			cs.registerOutParameter(24, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(24);

			log.info("DisposicionFinalAdmDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
