package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.RecRcdOmDAO;

public class RecRcdOmService {

	private static Logger log = Logger.getLogger(RecRcdOmService.class);
	RecRcdOmDAO recRcdOmDAO = new RecRcdOmDAO();

	public ClsResultado obtener(ClsRecRcdOm clsRecRcdOm){
		ClsResultado clsResultado = null;
		try {
			clsResultado = recRcdOmDAO.obtener(clsRecRcdOm);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsRecRcdOm clsRecRcdOm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = recRcdOmDAO.insertar(clsRecRcdOm);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsRecRcdOm clsRecRcdOm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = recRcdOmDAO.actualizar(clsRecRcdOm);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
