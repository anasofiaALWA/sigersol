package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CCTipoResiduoDAO;

public class CCTipoResiduoService {
	private static Logger log = Logger.getLogger(VariableService.class);
	CCTipoResiduoDAO dao = new CCTipoResiduoDAO();

	public ClsResultado listarDOC() {
		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.listarDOC();
		} catch (Exception e) {
			log.info("message listarDOC >> service >> " + e.getMessage());
		}

		return clsResultado;

	}
	
	public ClsResultado listarTasaDesc(String ubigeoId,String condicionId) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.listarTasaDesc(ubigeoId, condicionId);
		} catch (Exception e) {
			log.info("message listarTasaDesc >> service >> " + e.getMessage());
		}

		return clsResultado;

	}

}
