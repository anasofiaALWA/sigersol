package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.RcdOmDAO;

public class RcdOmService {
	
	private static Logger log = Logger.getLogger(RcdOmService.class);
	RcdOmDAO rcdOmDAO = new RcdOmDAO();
	
	public ClsResultado obtener(ClsRcdOm clsRcdOm){
		ClsResultado clsResultado = null;
		try {
			clsResultado = rcdOmDAO.obtener(clsRcdOm);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsRcdOm clsRcdOm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = rcdOmDAO.insertar(clsRcdOm);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsRcdOm clsRcdOm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = rcdOmDAO.actualizar(clsRcdOm);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
