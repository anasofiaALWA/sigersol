package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.MunicipalidadDAO;

public class MunicipalidadService {
	private static Logger log = Logger.getLogger(MunicipalidadService.class);
	MunicipalidadDAO municipalidadDAO = new MunicipalidadDAO();
	
	public ClsResultado obtener(){
		ClsResultado clsResultado = null;
		try {
			clsResultado = municipalidadDAO.obtener();
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsMunicipalidad clsMunicipalidad) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = municipalidadDAO.insertar(clsMunicipalidad);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsMunicipalidad clsMunicipalidad) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = municipalidadDAO.actualizar(clsMunicipalidad);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
