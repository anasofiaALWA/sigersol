package pe.gob.sigersol.entities;

public class ClsTipoResiduo {

	private Integer tipo_residuos_id;
	private String nombre_residuos;
	
	public Integer getTipo_residuos_id() {
		return tipo_residuos_id;
	}
	public void setTipo_residuos_id(Integer tipo_residuos_id) {
		this.tipo_residuos_id = tipo_residuos_id;
	}
	public String getNombre_residuos() {
		return nombre_residuos;
	}
	public void setNombre_residuos(String nombre_residuos) {
		this.nombre_residuos = nombre_residuos;
	}
}
