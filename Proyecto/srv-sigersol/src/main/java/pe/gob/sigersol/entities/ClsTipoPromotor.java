package pe.gob.sigersol.entities;

public class ClsTipoPromotor {

	private Integer tipo_promotor_id;
	private String nombre_eventos;
	
	public Integer getTipo_promotor_id() {
		return tipo_promotor_id;
	}
	public void setTipo_promotor_id(Integer tipo_promotor_id) {
		this.tipo_promotor_id = tipo_promotor_id;
	}
	public String getNombre_eventos() {
		return nombre_eventos;
	}
	public void setNombre_eventos(String nombre_eventos) {
		this.nombre_eventos = nombre_eventos;
	}	
}
