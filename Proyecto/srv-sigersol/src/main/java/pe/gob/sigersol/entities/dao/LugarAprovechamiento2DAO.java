package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsLugarAprovechamiento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;

public class LugarAprovechamiento2DAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(PlantaValorizacionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_aprovechamiento2 = "{call SIGERSOLBL.SGR_SP_LIST_APROVECHAMIENTO2(?,?)}";
	private final String sgr_sp_ins_aprovechamiento2 = "{call SIGERSOLBL.SGR_SP_INS_APROVECHAMIENTO2(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_aprovechamiento2 = "{call SIGERSOLBL.SGR_SP_UPD_APROVECHAMIENTO2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		ClsResultado clsResultado = new ClsResultado();
		ClsLugarAprovechamiento item = new ClsLugarAprovechamiento();
		ClsValorizacion clsValorizacion = new ClsValorizacion();
		con = null;
		String sql = this.sgr_sp_list_aprovechamiento2;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsLugarAprovechamiento.getValorizacion().getValorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setLugar_aprovechamiento_id(rs.getInt("LUGAR_APROVECHAMIENTO_ID"));
					clsValorizacion.setValorizacion_id(rs.getInt("VALORIZACION_ID"));
					item.setValorizacion(clsValorizacion);
					item.setAdm_propia(rs.getInt("ADM_PROPIA"));
					item.setTercerizado(rs.getInt("TERCERIZADO"));
					item.setMixto(rs.getInt("MIXTO"));
					item.setZona_coordenada(rs.getString("ZONA_COORDENADA"));
					item.setNorte_coordenada(rs.getString("NORTE_COORDENADA"));
					item.setSur_coordenada(rs.getString("SUR_COORDENADA"));					
					item.setDiv_politica_dep(rs.getString("DIV_POLITICA_DEP"));
					item.setDiv_politica_prov(rs.getString("DIV_POLITICA_PROV"));
					item.setDiv_politica_dist(rs.getString("DIV_POLITICA_DIST"));					
					item.setDireccion_iga(rs.getString("DIRECCION_IGA"));
					item.setReferencia_iga(rs.getString("REFERENCIA_IGA"));
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_aprovechamiento2;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsLugarAprovechamiento.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsLugarAprovechamiento.getAdm_propia());
			cs.setInt(3, clsLugarAprovechamiento.getTercerizado());
			cs.setInt(4, clsLugarAprovechamiento.getMixto());
			cs.setString(5, clsLugarAprovechamiento.getZona_coordenada());
			cs.setString(6, clsLugarAprovechamiento.getNorte_coordenada());
			cs.setString(7, clsLugarAprovechamiento.getSur_coordenada());
			cs.setString(8, clsLugarAprovechamiento.getDiv_politica_dep());
			cs.setString(9, clsLugarAprovechamiento.getDiv_politica_prov());
			cs.setString(10, clsLugarAprovechamiento.getDiv_politica_dist());
			cs.setString(11, clsLugarAprovechamiento.getDireccion_iga());
			cs.setString(12, clsLugarAprovechamiento.getReferencia_iga());
			cs.setInt(13, clsLugarAprovechamiento.getCodigo_usuario());
			cs.registerOutParameter(14, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(14);
			
			log.info("LugarAprovechamiento2DAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_aprovechamiento2;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsLugarAprovechamiento.getLugar_aprovechamiento_id());
			cs.setInt(2, clsLugarAprovechamiento.getValorizacion().getValorizacion_id());
			cs.setInt(3, clsLugarAprovechamiento.getAdm_propia());
			cs.setInt(4, clsLugarAprovechamiento.getTercerizado());
			cs.setInt(5, clsLugarAprovechamiento.getMixto());
			cs.setString(6, clsLugarAprovechamiento.getZona_coordenada());
			cs.setString(7, clsLugarAprovechamiento.getNorte_coordenada());
			cs.setString(8, clsLugarAprovechamiento.getSur_coordenada());
			cs.setString(9, clsLugarAprovechamiento.getDiv_politica_dep());
			cs.setString(10, clsLugarAprovechamiento.getDiv_politica_prov());
			cs.setString(11, clsLugarAprovechamiento.getDiv_politica_dist());
			cs.setString(12, clsLugarAprovechamiento.getDireccion_iga());
			cs.setString(13, clsLugarAprovechamiento.getReferencia_iga());
			cs.setInt(14, clsLugarAprovechamiento.getCodigo_usuario());
			cs.registerOutParameter(15, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(15);

			log.info("LugarAprovechamiento2DAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
