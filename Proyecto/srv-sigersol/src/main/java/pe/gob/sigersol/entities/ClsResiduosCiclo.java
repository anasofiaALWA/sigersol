package pe.gob.sigersol.entities;

public class ClsResiduosCiclo {

	private Integer residuos_ciclo_id;
	private ClsValorizacion valorizacion;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getResiduos_ciclo_id() {
		return residuos_ciclo_id;
	}
	public void setResiduos_ciclo_id(Integer residuos_ciclo_id) {
		this.residuos_ciclo_id = residuos_ciclo_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}	
}
