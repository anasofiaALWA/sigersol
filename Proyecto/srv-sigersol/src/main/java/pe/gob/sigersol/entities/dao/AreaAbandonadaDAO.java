package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAreaAbandonada;
import pe.gob.sigersol.entities.ClsCantVehiculos;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoVehiculo;

public class AreaAbandonadaDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(AreaAbandonadaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_area_abandonadas = "{call SIGERSOLBL.SGR_SP_INS_AREA_ABANDONADAS(?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_area_abandonadas = "{call SIGERSOLBL.SGR_SP_UPD_AREA_ABANDONADAS(?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_list_area_abandonadas = "{call SIGERSOLBL.SGR_SP_LIST_AREA_ABANDONADAS(?,?)}";  
	
	public ClsResultado listarAreaAbandonada(ClsAreaAbandonada clsAreaAbandonada){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsAreaAbandonada> lista = new ArrayList<ClsAreaAbandonada>();
		con = null;
		String sql = this.sgr_sp_list_area_abandonadas;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAreaAbandonada.getMunicipalidad().getMunicipalidad_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsAreaAbandonada item = new ClsAreaAbandonada();
				ClsMunicipalidad clsMunicipalidad = new ClsMunicipalidad();
				
				try {
					item.setArea_abandonadas_id(rs.getInt("AREA_ABANDONADAS_ID"));
					clsMunicipalidad.setMunicipalidad_id(rs.getInt("MUNICIPALIDAD_ID"));
					item.setMunicipalidad(clsMunicipalidad);
					item.setAnio_inicio(rs.getInt("ANIO_INICIO"));
					item.setAnio_cierre(rs.getInt("ANIO_CIERRE"));
					item.setArea_total(rs.getInt("AREA_TOTAL"));
					item.setResponsable(rs.getString("RESPONSABLE"));
					item.setAltura_nf(rs.getInt("ALTURA_NF"));
					item.setCantidad(rs.getInt("CANTIDAD"));
					item.setMetodo_usado(rs.getString("METODO_USADO"));
										
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAreaAbandonada clsAreaAbandonada) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_area_abandonadas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAreaAbandonada.getMunicipalidad().getMunicipalidad_id());
			cs.setInt(2, clsAreaAbandonada.getAnio_inicio());
			cs.setInt(3, clsAreaAbandonada.getAnio_cierre());
			cs.setInt(4, clsAreaAbandonada.getArea_total());
			cs.setString(5, clsAreaAbandonada.getResponsable());
			cs.setInt(6, clsAreaAbandonada.getAltura_nf());
			cs.setInt(7, clsAreaAbandonada.getCantidad());
			cs.setString(8, clsAreaAbandonada.getMetodo_usado());
			cs.setInt(9, clsAreaAbandonada.getCodigo_usuario());
			cs.registerOutParameter(10, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(10);
			
			log.info("AreaAbandonadaDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsAreaAbandonada clsAreaAbandonada) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_area_abandonadas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAreaAbandonada.getArea_abandonadas_id());
			cs.setInt(2, clsAreaAbandonada.getMunicipalidad().getMunicipalidad_id());
			cs.setInt(3, clsAreaAbandonada.getAnio_inicio());
			cs.setInt(4, clsAreaAbandonada.getAnio_cierre());
			cs.setInt(5, clsAreaAbandonada.getArea_total());
			cs.setString(6, clsAreaAbandonada.getResponsable());
			cs.setInt(7, clsAreaAbandonada.getAltura_nf());
			cs.setInt(8, clsAreaAbandonada.getCantidad());
			cs.setString(9, clsAreaAbandonada.getMetodo_usado());
			cs.setInt(10, clsAreaAbandonada.getCodigo_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);

			log.info("AreaAbandonadaDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
