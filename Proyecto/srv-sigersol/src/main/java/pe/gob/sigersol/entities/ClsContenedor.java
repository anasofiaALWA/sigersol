package pe.gob.sigersol.entities;

public class ClsContenedor {
	
	private Integer contenedor_id;
	private ClsAlmacenamiento almacenamiento;
	private ClsTipoContenedor tipo_contenedor;
	private String tipo;
	private String estado;
	private Double cantidad;
	private Integer capacidad;
	private Integer mercados;
	private Integer parques;
	private Integer vias;
	private Integer otros;
	private Integer codigo_usuario;
	
	public Integer getContenedor_id() {
		return contenedor_id;
	}
	public void setContenedor_id(Integer contenedor_id) {
		this.contenedor_id = contenedor_id;
	}
	public ClsAlmacenamiento getAlmacenamiento() {
		return almacenamiento;
	}
	public void setAlmacenamiento(ClsAlmacenamiento almacenamiento) {
		this.almacenamiento = almacenamiento;
	}
	public ClsTipoContenedor getTipo_contenedor() {
		return tipo_contenedor;
	}
	public void setTipo_contenedor(ClsTipoContenedor tipo_contenedor) {
		this.tipo_contenedor = tipo_contenedor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}
	public Integer getMercados() {
		return mercados;
	}
	public void setMercados(Integer mercados) {
		this.mercados = mercados;
	}
	public Integer getParques() {
		return parques;
	}
	public void setParques(Integer parques) {
		this.parques = parques;
	}
	public Integer getVias() {
		return vias;
	}
	public void setVias(Integer vias) {
		this.vias = vias;
	}
	public Integer getOtros() {
		return otros;
	}
	public void setOtros(Integer otros) {
		this.otros = otros;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
