package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import com.lowagie.text.pdf.AcroFields.Item;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsOferta;
import pe.gob.sigersol.entities.ClsPersona;
import pe.gob.sigersol.entities.ClsResultado;

public class PersonaDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(PersonaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_existe_persona = "{call SIGERSOLBL.SGR_SP_EXISTE_PERSONA(?,?)}";
	private final String sgr_sp_ins_persona = "{call SIGERSOLBL.SGR_SP_INS_PERSONA(?,?,?,?,?,?)}";
	
	public Integer existe(ClsPersona clsPersona) {
		Integer clsResultado = null;
		ClsBarrido barrido = new ClsBarrido();
		int cont = 0;
		
		con = null;
		String sql = this.sgr_sp_existe_persona;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPersona.getDni());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsPersona item = new ClsPersona();
				try {
					item.setPersona_id(rs.getInt("PERSONA_ID"));
					cont++;
					clsResultado = item.getPersona_id();
				} catch (Exception e) {

				}
	
			}
			
			if (cont == 0){
				return cont;
			}
			
			
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
		
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
			}
		}
		return clsResultado;
	}
	
	public Integer insertar(ClsPersona clsPersona) {
		Integer resultado = null;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_persona;
		
		Integer cont_contenedor = 1;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPersona.getDni());
			cs.setString(2, clsPersona.getNom_apellidos());
			cs.setString(3, clsPersona.getSexo());
			cs.setString(4, clsPersona.getRango_edad());
			cs.setInt(5, clsPersona.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			cont_contenedor++;
			
			log.info("PersonaDAO >> insertar() >> resultado :" + resultado);

			//clsResultado.setObjeto(resultado);
			//clsResultado.setExito(true);
			//clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return resultado;
	}

	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
