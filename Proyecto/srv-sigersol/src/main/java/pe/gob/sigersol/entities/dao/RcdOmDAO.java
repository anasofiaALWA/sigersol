package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoConstruccion;

public class RcdOmDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(RcdOmDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_list_rcd_om = "{call SIGERSOLBL.SGR_SP_LIST_RCD_OM(?,?)}";
	private final String sgr_sp_ins_rcd_om = "{call SIGERSOLBL.SGR_SP_INS_RCD_OM(?,?,?,?,?)}";
	private final String sgr_sp_upd_rcd_om = "{call SIGERSOLBL.SGR_SP_UPD_RCD_OM(?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsRcdOm clsRcdOm) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsRcdOm> lista = new ArrayList<ClsRcdOm>();
		
		con = null;
		String sql = this.sgr_sp_list_rcd_om;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRcdOm.getGeneracion().getGeneracion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsRcdOm item = new ClsRcdOm();
				ClsGeneracion generacion = new ClsGeneracion();
				ClsTipoConstruccion tipoConstruccion = new ClsTipoConstruccion();
				try {
					tipoConstruccion.setTipo_construccion_id(rs.getInt("TIPO_CONSTRUCCION_ID"));
					tipoConstruccion.setNombre_tipo(rs.getString("NOMBRE_TIPO"));
					tipoConstruccion.setFlag_cantidad(rs.getInt("FLAG_CANTIDAD"));
					item.setTipo_construccion(tipoConstruccion);
					if(clsRcdOm.getGeneracion().getGeneracion_id() != -1){
						item.setCantidad(rs.getInt("CANTIDAD"));
					}
					else{
						item.setCantidad(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
				
			}
			
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsRcdOm clsRcdOm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_rcd_om;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRcdOm.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsRcdOm.getTipo_construccion().getTipo_construccion_id());
			cs.setInt(3, clsRcdOm.getCantidad());
			cs.setInt(4, clsRcdOm.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);
			
			log.info("RcdOmDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsRcdOm clsRcdOm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_rcd_om;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRcdOm.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsRcdOm.getTipo_construccion().getTipo_construccion_id());
			cs.setInt(3, clsRcdOm.getCantidad());
			cs.setInt(4, clsRcdOm.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);

			log.info("RcdOmDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
