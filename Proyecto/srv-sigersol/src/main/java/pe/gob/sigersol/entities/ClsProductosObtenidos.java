package pe.gob.sigersol.entities;

public class ClsProductosObtenidos {

	private Integer productos_obtenidos_id;
	private ClsValorizacion valorizacion;
	private ClsTipoProductos tipoProductos;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getProductos_obtenidos_id() {
		return productos_obtenidos_id;
	}
	public void setProductos_obtenidos_id(Integer productos_obtenidos_id) {
		this.productos_obtenidos_id = productos_obtenidos_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public ClsTipoProductos getTipoProductos() {
		return tipoProductos;
	}
	public void setTipoProductos(ClsTipoProductos tipoProductos) {
		this.tipoProductos = tipoProductos;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
