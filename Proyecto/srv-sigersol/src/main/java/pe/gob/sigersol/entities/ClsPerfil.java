package pe.gob.sigersol.entities;

import java.sql.Timestamp;

import com.google.gson.Gson;

/**
 * @author
 */
public class ClsPerfil {
	
	private Integer perfil_id;
	private Integer codigo_estado;
	
	private String perfil_nombre;
	private String perfil_nombre_grupo_ad;
	
	private String descripcion;
	private String usuario_creacion;
	private Timestamp fecha_creacion;
	private String usuario_modificacion;
	private Timestamp fecha_modificacion;


	public String getPerfil_nombre() {
		return perfil_nombre;
	}

	public void setPerfil_nombre(String perfil_nombre) {
		this.perfil_nombre = perfil_nombre;
	}

	public String getPerfil_nombre_grupo_ad() {
		return perfil_nombre_grupo_ad;
	}

	public void setPerfil_nombre_grupo_ad(String perfil_nombre_grupo_ad) {
		this.perfil_nombre_grupo_ad = perfil_nombre_grupo_ad;
	}

	public Integer getPerfil_id() {
		return perfil_id;
	}

	public void setPerfil_id(Integer perfil_id) {
		this.perfil_id = perfil_id;
	}

	public Integer getCodigo_estado() {
		return codigo_estado;
	}

	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUsuario_creacion() {
		return usuario_creacion;
	}

	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}

	public Timestamp getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Timestamp fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}

	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}

	public Timestamp getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Timestamp fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}
	
	
}