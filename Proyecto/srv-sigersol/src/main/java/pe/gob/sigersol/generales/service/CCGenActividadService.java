package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCCGenActividad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CCGenActividadDAO;

public class CCGenActividadService {

	private static Logger log = Logger.getLogger(CCGenActividadService.class.getName());
	CCGenActividadDAO dao = new CCGenActividadDAO();
	CCGenCombustibleService ccGenCombustibleService = new CCGenCombustibleService();

	public ClsResultado insertar(ClsCCGenActividad ccGenActividad) {

		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.insertar(ccGenActividad);
			if (clsResultado.isExito()) {
				Integer genActividadId = (Integer) clsResultado.getObjeto();
				clsResultado = ccGenCombustibleService.insertar(genActividadId, ccGenActividad.getListCombustible());
				if (clsResultado.isExito()) {
					clsResultado.setObjeto(genActividadId);
				}
			}
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	public ClsResultado actualizar(ClsCCGenActividad ccGenActividad) {

		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.actualizar(ccGenActividad);
			if (clsResultado.isExito()) {
				Integer genActividadId = (Integer) clsResultado.getObjeto();
				clsResultado = ccGenCombustibleService.actualizar(ccGenActividad.getListCombustible());
				if (clsResultado.isExito()) {
					clsResultado.setObjeto(genActividadId);
				}
			}
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado getGenActividad(Integer ccGeneracionId) {

		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.getGenActividad(ccGeneracionId);			
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
