package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAreaAbandonada;
import pe.gob.sigersol.entities.ClsCantVehiculos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AreaAbandonadaDAO;
import pe.gob.sigersol.entities.dao.CantVehiculosDAO;

public class AreaAbandonadaService {

	private static Logger log = Logger.getLogger(AreaAbandonadaService.class);
	AreaAbandonadaDAO areaAbandonadaDAO = new AreaAbandonadaDAO();
	
	public ClsResultado listarAreaAbandonada(ClsAreaAbandonada clsAreaAbandonada){
		ClsResultado clsResultado = null;
		try {
			clsResultado = areaAbandonadaDAO.listarAreaAbandonada(clsAreaAbandonada);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAreaAbandonada clsAreaAbandonada) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = areaAbandonadaDAO.insertar(clsAreaAbandonada);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsAreaAbandonada clsAreaAbandonada) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = areaAbandonadaDAO.actualizar(clsAreaAbandonada);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
