package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.DisposicionFinal2DAO;

public class DisposicionFinal2Service {

	private static Logger log = Logger.getLogger(DisposicionFinal2Service.class);
	DisposicionFinal2DAO disposicionFinal2DAO = new DisposicionFinal2DAO();
	
	public ClsResultado obtener(ClsDisposicionFinal clsDisposicionFinal){
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinal2DAO.obtener(clsDisposicionFinal);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsDisposicionFinal clsDisposicionFinal) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinal2DAO.insertar(clsDisposicionFinal);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsDisposicionFinal clsDisposicionFinal) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinal2DAO.actualizar(clsDisposicionFinal);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
