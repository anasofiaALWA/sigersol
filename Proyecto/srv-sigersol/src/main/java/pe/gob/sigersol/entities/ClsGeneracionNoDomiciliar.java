package pe.gob.sigersol.entities;

public class ClsGeneracionNoDomiciliar {
	private Integer gnd_id;
	private ClsGeneracion generacion;
	private ClsTipoGeneracion tipo_generacion;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getGnd_id() {
		return gnd_id;
	}
	public void setGnd_id(Integer gnd_id) {
		this.gnd_id = gnd_id;
	}
	public ClsGeneracion getGeneracion() {
		return generacion;
	}
	public void setGeneracion(ClsGeneracion generacion) {
		this.generacion = generacion;
	}
	public ClsTipoGeneracion getTipo_generacion() {
		return tipo_generacion;
	}
	public void setTipo_generacion(ClsTipoGeneracion tipo_generacion) {
		this.tipo_generacion = tipo_generacion;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}