package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsPlantaValorizacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;

public class PlantaValorizacionDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(PlantaValorizacionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_centro_acopio2 = "{call SIGERSOLBL.SGR_SP_LIST_SGR_CENTRO_ACOPIO2(?,?)}";
	private final String sgr_sp_ins_centro_acopio2 = "{call SIGERSOLBL.SGR_SP_INS_CENTRO_ACOPIO2(?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_centro_acopio2 = "{call SIGERSOLBL.SGR_SP_UPD_CENTRO_ACOPIO2(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsPlantaValorizacion clsPlantaValorizacion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsPlantaValorizacion item = new ClsPlantaValorizacion();
		ClsValorizacion clsValorizacion = new ClsValorizacion();
		con = null;
		String sql = this.sgr_sp_list_sgr_centro_acopio2;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPlantaValorizacion.getValorizacion().getValorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setPlanta_valorizacion_id(rs.getInt("CENTRO_ACOPIO_ID"));
					clsValorizacion.setValorizacion_id(rs.getInt("VALORIZACION_ID"));
					item.setValorizacion(clsValorizacion);
					item.setAdm_propia(rs.getInt("ADM_PROPIA"));
					item.setTercerizado(rs.getInt("TERCERIZADO"));
					item.setMixto(rs.getInt("MIXTO"));
					item.setZona_coordenada(rs.getString("ZONA_COORDENADA"));
					item.setNorte_coordenada(rs.getString("NORTE_COORDENADA"));
					item.setSur_coordenada(rs.getString("SUR_COORDENADA"));
					item.setDivision_politica(rs.getString("DIVISION_POLITICA"));
					item.setDireccion_iga(rs.getString("DIRECCION_IGA"));
					item.setReferencia_iga(rs.getString("REFERENCIA_IGA"));
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsPlantaValorizacion clsPlantaValorizacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_centro_acopio2;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPlantaValorizacion.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsPlantaValorizacion.getAdm_propia());
			cs.setInt(3, clsPlantaValorizacion.getTercerizado());
			cs.setInt(4, clsPlantaValorizacion.getMixto());
			cs.setString(5, clsPlantaValorizacion.getZona_coordenada());
			cs.setString(6, clsPlantaValorizacion.getNorte_coordenada());
			cs.setString(7, clsPlantaValorizacion.getSur_coordenada());
			cs.setString(8, clsPlantaValorizacion.getDivision_politica());
			cs.setString(9, clsPlantaValorizacion.getDireccion_iga());
			cs.setString(10, clsPlantaValorizacion.getReferencia_iga());
			cs.setInt(11, clsPlantaValorizacion.getCodigo_usuario());
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(12);
			
			log.info("PlantaValorizacionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsPlantaValorizacion clsPlantaValorizacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_centro_acopio2;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPlantaValorizacion.getPlanta_valorizacion_id());
			cs.setInt(2, clsPlantaValorizacion.getValorizacion().getValorizacion_id());
			cs.setInt(3, clsPlantaValorizacion.getAdm_propia());
			cs.setInt(4, clsPlantaValorizacion.getTercerizado());
			cs.setInt(5, clsPlantaValorizacion.getMixto());
			cs.setString(6, clsPlantaValorizacion.getZona_coordenada());
			cs.setString(7, clsPlantaValorizacion.getNorte_coordenada());
			cs.setString(8, clsPlantaValorizacion.getSur_coordenada());
			cs.setString(9, clsPlantaValorizacion.getDivision_politica());
			cs.setString(10, clsPlantaValorizacion.getDireccion_iga());
			cs.setString(11, clsPlantaValorizacion.getReferencia_iga());
			cs.setInt(12, clsPlantaValorizacion.getCodigo_usuario());
			cs.registerOutParameter(13, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(13);

			log.info("PlantaValorizacionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
