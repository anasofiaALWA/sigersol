package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsPerfil;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.util.DateUtility;

public class PerfilDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(PerfilDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String ep_mg_list_perfil = "{call COPEREBL.EP_MG_LIST_PERFIL(?)}";
	private final String ep_mg_list_perfil_x_nombre = "{call COPEREBL.EP_MG_LIST_PERFIL_X_NOMBRE(?,?)}";
	private final String ep_mg_sp_ins_perfil = "{call COPEREBL.EP_MG_SP_INS_PERFIL(?,?,?,?,?)}";
	private final String ep_mg_sp_upd_perfil = "{call COPEREBL.EP_MG_SP_UPD_PERFIL(?,?,?,?,?,?)}";
	
	public ClsResultado obtener_lista_perfil() {
		ClsResultado clsResultado = new ClsResultado();
		ClsPerfil item = null;
		List<ClsPerfil> lista = new ArrayList<ClsPerfil>();
		con = null;
		String sql = this.ep_mg_list_perfil;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				item = new ClsPerfil();
				try {
					item.setPerfil_id(rs.getInt("PERFIL_ID"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setPerfil_nombre(rs.getString("PERFIL_NOMBRE"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setCodigo_estado(rs.getInt("CODIGO_ESTADO"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setDescripcion(rs.getString("DESCRIPCION"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				lista.add(item);
				
				clsResultado.setObjeto(lista);
				clsResultado.setExito(true);
				clsResultado.setMensaje("Obtención exitosa");
			}
		} catch (SQLException ex) {
			log.error(
					"ERROR! 1 obtener_lista_perfil: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_lista_perfil: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje(ex.getMessage());
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_lista_perfil: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje(ex.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_lista_perfil: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje(exception.getMessage());
			}
		}
		return clsResultado;
	}

	public ClsResultado obtener_lista_perfil(String nombre) {
		ClsResultado clsResultado = new ClsResultado();
		ClsPerfil item = null;
		List<ClsPerfil> lista = new ArrayList<ClsPerfil>();
		con = null;
		String sql = this.ep_mg_list_perfil_x_nombre;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, nombre);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				item = new ClsPerfil();
				try {
					item.setPerfil_id(rs.getInt("PERFIL_ID"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setPerfil_nombre(rs.getString("PERFIL_NOMBRE"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setCodigo_estado(rs.getInt("CODIGO_ESTADO"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setDescripcion(rs.getString("DESCRIPCION"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				lista.add(item);
				
				clsResultado.setObjeto(lista);
				clsResultado.setExito(true);
				clsResultado.setMensaje("Obtención exitosa");
			}
		} catch (SQLException ex) {
			log.error(
					"ERROR! 1 obtener_lista_perfil: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_lista_perfil: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje(ex.getMessage());
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_lista_perfil: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje(ex.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_lista_perfil: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje(exception.getMessage());
			}
		}
		return clsResultado;
	}
	
	public ClsResultado insertarPerfil(ClsPerfil clsPerfil) {
		ClsResultado clsResultado = new ClsResultado();
		log.info("SP: " + this.ep_mg_sp_ins_perfil);
		
		String sql = this.ep_mg_sp_ins_perfil;
		Integer resultado;

		try {
			con = getConnection();
		} catch (Exception e) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error de conexion");
			return null;
		}

		try {
			cs = con.prepareCall(sql);

			cs.setString(1, clsPerfil.getPerfil_nombre());
			cs.setInt(2, clsPerfil.getCodigo_estado());
			cs.setString(3, clsPerfil.getDescripcion());
			cs.setString(4, clsPerfil.getUsuario_creacion());
			cs.registerOutParameter(5, OracleTypes.INTEGER);

			cs.executeUpdate();
			resultado = cs.getInt(5);					

			log.info("PerfilDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");
	

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error SQLException");
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error Generico");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje("Error al cerrar conexion");
			}
		}

		return clsResultado;
	}
	
	public ClsResultado updatePerfil(ClsPerfil clsPerfil) {
		ClsResultado clsResultado = new ClsResultado();
		log.info("SP: " + this.ep_mg_sp_upd_perfil + " >> Funcion_id: "
				+ clsPerfil.getPerfil_id());
		log.info("Fecha inicio: " + DateUtility.getCurrentTimestamp());

		String sql = this.ep_mg_sp_upd_perfil;
		Integer resultado;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error de conexion");
			return null;
		}

		try {
			cs = con.prepareCall(sql);

			cs.setString(1, clsPerfil.getPerfil_nombre());
			cs.setInt(2, clsPerfil.getCodigo_estado());
			cs.setString(3, clsPerfil.getDescripcion());
			cs.setString(4, clsPerfil.getUsuario_modificacion());
			cs.setInt(5, clsPerfil.getPerfil_id());
			cs.registerOutParameter(6, OracleTypes.INTEGER);

			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("PerfilDAO >> actualizar() >> resultado :" + 1);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

			log.info("Fecha fin: " + DateUtility.getCurrentTimestamp());

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error SQLException");
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error Generico");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje("Error al cerrar conexion");
			}
		}

		return clsResultado;
	}
}
