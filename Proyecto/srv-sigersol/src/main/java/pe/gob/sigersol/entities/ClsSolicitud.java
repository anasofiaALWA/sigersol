package pe.gob.sigersol.entities;


public class ClsSolicitud {
	private Integer solicitud_id;
	private String codigo_ubigeo_departamento;
	private String codigo_ubigeo_provincia;
	private String codigo_ubigeo_distrito;
	private String ruc_municipalidad;
	private String nombre_alcalde;
	private String direccion;
	private String telefono;
	private String fax;
	private String email;
	private Integer codigo_estado;
	private Integer codigo_usuario;
	
	
	public Integer getSolicitud_id() {
		return solicitud_id;
	}
	public void setSolicitud_id(Integer solicitud_id) {
		this.solicitud_id = solicitud_id;
	}
	public String getCodigo_ubigeo_departamento() {
		return codigo_ubigeo_departamento;
	}
	public void setCodigo_ubigeo_departamento(String codigo_ubigeo_departamento) {
		this.codigo_ubigeo_departamento = codigo_ubigeo_departamento;
	}
	public String getCodigo_ubigeo_provincia() {
		return codigo_ubigeo_provincia;
	}
	public void setCodigo_ubigeo_provincia(String codigo_ubigeo_provincia) {
		this.codigo_ubigeo_provincia = codigo_ubigeo_provincia;
	}
	public String getCodigo_ubigeo_distrito() {
		return codigo_ubigeo_distrito;
	}
	public void setCodigo_ubigeo_distrito(String codigo_ubigeo_distrito) {
		this.codigo_ubigeo_distrito = codigo_ubigeo_distrito;
	}
	public String getRuc_municipalidad() {
		return ruc_municipalidad;
	}
	public void setRuc_municipalidad(String ruc_municipalidad) {
		this.ruc_municipalidad = ruc_municipalidad;
	}
	public String getNombre_alcalde() {
		return nombre_alcalde;
	}
	public void setNombre_alcalde(String nombre_alcalde) {
		this.nombre_alcalde = nombre_alcalde;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getCodigo_estado() {
		return codigo_estado;
	}
	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
	
}
