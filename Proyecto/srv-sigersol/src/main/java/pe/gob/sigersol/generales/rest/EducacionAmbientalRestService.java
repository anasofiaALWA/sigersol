package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsEducacionAmbiental;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.EducacionAmbientalService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase EducacionAmbientalRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de Educacion Ambiental, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/ambientalX")
@RequestScoped
public class EducacionAmbientalRestService {
	private static Logger log = Logger.getLogger(SolicitudRestService.class.getName());
	private ClsEducacionAmbiental clsEducacionAmbiental;
	private EducacionAmbientalService educacionAmbientalService;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	
	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			educacionAmbientalService = new EducacionAmbientalService();
			
			jsonEntradaAmbiental(paramJson);
			
			log.info("**obtener ambiental**");
			
			clsResultado = educacionAmbientalService.obtener(clsEducacionAmbiental);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"ambiental\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		educacionAmbientalService = new EducacionAmbientalService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaAmbiental(paramJson);
			clsResultado = educacionAmbientalService.insertar(clsEducacionAmbiental);	
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		educacionAmbientalService = new EducacionAmbientalService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaAmbiental(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = educacionAmbientalService.actualizar(clsEducacionAmbiental);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de EducacionAmbiental  
	 * 
	 * */
	private void jsonEntradaAmbiental(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> EducacionAmbientalRestService : jsonEntradaAmbiental" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectAmbiental;
		JSONParser jsonParser = new JSONParser();
		
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		clsEducacionAmbiental = new ClsEducacionAmbiental();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectAmbiental = (JSONObject) jsonObjectPrincipal.get("Ambiental");

			if (jsonObjectAmbiental.get("educacion_ambiental_id") != null && jsonObjectAmbiental.get("educacion_ambiental_id") != "") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(jsonObjectAmbiental.get("educacion_ambiental_id").toString()));
			}
			else{
				clsEducacionAmbiental.setEducacion_ambiental_id(-1);
			}
			
			if (jsonObjectAmbiental.get("ciclo_grs_id") != null && jsonObjectAmbiental.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectAmbiental.get("ciclo_grs_id").toString()));
			}
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsEducacionAmbiental.setCicloGestionResiduos(clsCicloGestionResiduos);
			
			if (jsonObjectAmbiental.get("flag_ambiental") != null && jsonObjectAmbiental.get("flag_ambiental") != "") {
				clsEducacionAmbiental.setFlag_ambiental(Integer.parseInt(jsonObjectAmbiental.get("flag_ambiental").toString()));
			}
			
			if (jsonObjectAmbiental.get("flag_minam") != null && jsonObjectAmbiental.get("flag_minam") != "") {
				clsEducacionAmbiental.setFlag_minam(Integer.parseInt(jsonObjectAmbiental.get("flag_minam").toString()));
			}
			
			if (jsonObjectAmbiental.get("flag_actividades") != null && jsonObjectAmbiental.get("flag_actividades") != "") {
				clsEducacionAmbiental.setFlag_actividades(Integer.parseInt(jsonObjectAmbiental.get("flag_actividades").toString()));
			}
			
			if (jsonObjectAmbiental.get("documento_aprobacion") != null && jsonObjectAmbiental.get("documento_aprobacion") != "") {
				clsEducacionAmbiental.setDocumento_aprobacion(jsonObjectAmbiental.get("documento_aprobacion").toString());
			}
			
			if (jsonObjectAmbiental.get("eventos_otros") != null && jsonObjectAmbiental.get("eventos_otros") != "") {
				clsEducacionAmbiental.setEventos_otros(jsonObjectAmbiental.get("eventos_otros").toString());
			}
			if (jsonObjectAmbiental.get("capacit_hombres_otros") != null && jsonObjectAmbiental.get("capacit_hombres_otros") != "") {
				clsEducacionAmbiental.setCapacit_hombres_otros(Integer.parseInt(jsonObjectAmbiental.get("capacit_hombres_otros").toString()));
			}
			
			if (jsonObjectAmbiental.get("capacit_mujeres_otros") != null && jsonObjectAmbiental.get("capacit_mujeres_otros") != "") {
				clsEducacionAmbiental.setCapacit_mujeres_otros(Integer.parseInt(jsonObjectAmbiental.get("capacit_mujeres_otros").toString()));
			}
			
			if (jsonObjectAmbiental.get("cod_usuario") != null && jsonObjectAmbiental.get("cod_usuario") != "") {
				clsEducacionAmbiental.setCodigo_usuario(Integer.parseInt(jsonObjectAmbiental.get("cod_usuario").toString()));
			}
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
