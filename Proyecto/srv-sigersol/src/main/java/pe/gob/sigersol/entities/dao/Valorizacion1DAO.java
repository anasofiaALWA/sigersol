package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;

public class Valorizacion1DAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(ValorizacionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_valorizacion = "{call SIGERSOLBL.SGR_SP_LIST_SGR_VALORIZACION1(?,?)}";
	private final String sgr_sp_ins_valorizacion = "{call SIGERSOLBL.SGR_SP_INS_VALORIZACION1(?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_valorizacion = "{call SIGERSOLBL.SGR_SP_UPD_VALORIZACION(?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsValorizacion clsValorizacion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsValorizacion item = new ClsValorizacion();
		ClsCicloGestionResiduos clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_sgr_valorizacion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsValorizacion.getCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setValorizacion_id(rs.getInt("VALORIZACION_ID"));
					clsCicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(clsCicloGestionResiduos);
					item.setFlag_acopio(rs.getInt("FLAG_ACOPIO"));
					item.setNumero_acopio(rs.getInt("NUMERO_ACOPIO"));
					item.setFlag_acopio2(rs.getInt("FLAG_ACOPIO2"));
					item.setFlag_valorizacion(rs.getInt("FLAG_VALORIZACION"));
					item.setNumero_valorizacion(rs.getInt("NUMERO_VALORIZACION"));
					item.setFlag_valorizacion2(rs.getInt("FLAG_VALORIZACION2"));
				
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsValorizacion clsValorizacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_valorizacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsValorizacion.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsValorizacion.getFlag_acopio());
			cs.setInt(3, clsValorizacion.getNumero_acopio());
			cs.setInt(4, clsValorizacion.getFlag_acopio2());
			cs.setInt(5, clsValorizacion.getFlag_valorizacion());
			cs.setInt(6, clsValorizacion.getNumero_valorizacion());
			cs.setInt(7, clsValorizacion.getFlag_valorizacion2());
			cs.setInt(8, clsValorizacion.getCodigo_usuario());
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(9);
			
			log.info("ValorizacionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsValorizacion clsValorizacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_valorizacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsValorizacion.getValorizacion_id());
			cs.setInt(2, clsValorizacion.getFlag_acopio());
			cs.setInt(3, clsValorizacion.getNumero_acopio());
			cs.setInt(4, clsValorizacion.getFlag_acopio2());
			cs.setInt(5, clsValorizacion.getFlag_valorizacion());
			cs.setInt(6, clsValorizacion.getNumero_valorizacion());
			cs.setInt(7, clsValorizacion.getFlag_valorizacion2());
			cs.setInt(8, clsValorizacion.getCodigo_usuario());
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(9);

			log.info("ValorizacionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
