package pe.gob.sigersol.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtility {
	public static Timestamp getCurrentTimestamp() {
		java.util.Date date = new java.util.Date();
		return new Timestamp(date.getTime());
	}

	public static String getCurrentTimestamp_format(String format) {
		String timeStamp = new SimpleDateFormat(format).format(new Date());
		return timeStamp;
	}
	
	public static String getDate_format(java.sql.Date date, String format) {
		String timeStamp = new SimpleDateFormat(format).format(date);
		return timeStamp;
	}
}
