package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsOfertaBarrido;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoOferta;
import pe.gob.sigersol.generales.service.OfertaBarridoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase MuniDispDfRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de disposicion final permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/ofertaBarrido")
@RequestScoped
public class OfertaBarridoRestService {

	private static Logger log = Logger.getLogger(OfertaBarridoRestService.class.getName());
	
	private ClsOfertaBarrido clsOfertaBarrido;
	private ClsBarrido clsBarrido;
	private ClsTipoOferta clsTipoOferta;
	
	private OfertaBarridoService ofertaBarridoService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			ofertaBarridoService = new OfertaBarridoService();
			
			jsonEntradaOfertaBarrido(paramJson);
			
			log.info("**obtener Oferta Barrido");
			
			clsResultado = ofertaBarridoService.obtener(clsOfertaBarrido);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"ofertaBarrido\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		ofertaBarridoService = new OfertaBarridoService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			jsonEntradaOfertaBarrido(paramJson);
			clsResultado = ofertaBarridoService.insertar(clsOfertaBarrido);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ofertaBarridoService = new OfertaBarridoService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaOfertaBarrido(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = ofertaBarridoService.actualizar(clsOfertaBarrido);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de OfertaBarrido
	 * 
	 * */
	private void jsonEntradaOfertaBarrido(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ProgramaSegregacionRestService :jsonEntradaProgramaSegregacion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectOfertaBarrido;
		JSONParser jsonParser = new JSONParser();
		
		clsOfertaBarrido = new ClsOfertaBarrido();
		clsBarrido = new ClsBarrido();
		clsTipoOferta = new ClsTipoOferta();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectOfertaBarrido = (JSONObject) jsonObjectPrincipal.get("OfertaBarrido");
			
			if (jsonObjectOfertaBarrido.get("oferta_id") != null && jsonObjectOfertaBarrido.get("oferta_id") != "") {
				clsOfertaBarrido.setOferta_id(Integer.parseInt(jsonObjectOfertaBarrido.get("oferta_id").toString()));
			} else {
				clsOfertaBarrido.setOferta_id(-1);
			}
			
			if (jsonObjectOfertaBarrido.get("barrido_id") != null && jsonObjectOfertaBarrido.get("barrido_id") != "") {
				clsBarrido.setBarrido_id(Integer.parseInt(jsonObjectOfertaBarrido.get("barrido_id").toString()));
			} else {
				clsBarrido.setBarrido_id(-1);
			}
			
			clsOfertaBarrido.setBarrido(clsBarrido);
			
			clsTipoOferta.setTipo_oferta_id(1);
			clsOfertaBarrido.setTipoOferta(clsTipoOferta);
			
			
			if (jsonObjectOfertaBarrido.get("total_asfaltada") != null && jsonObjectOfertaBarrido.get("total_asfaltada") != "") {
				clsOfertaBarrido.setTotal_asfaltada(Integer.parseInt(jsonObjectOfertaBarrido.get("total_asfaltada").toString()));
			} 
			else
				clsOfertaBarrido.setTotal_asfaltada(0);
			
			if (jsonObjectOfertaBarrido.get("total_no_asfaltada") != null && jsonObjectOfertaBarrido.get("total_no_asfaltada") != "") {
				clsOfertaBarrido.setTotal_no_asfaltada(Integer.parseInt(jsonObjectOfertaBarrido.get("total_no_asfaltada").toString()));
			}
			else
				clsOfertaBarrido.setTotal_no_asfaltada(0);
			
			if (jsonObjectOfertaBarrido.get("total_plazas") != null && jsonObjectOfertaBarrido.get("total_plazas") != "") {
				clsOfertaBarrido.setTotal_plazas(Integer.parseInt(jsonObjectOfertaBarrido.get("total_plazas").toString()));
			}
			else
				clsOfertaBarrido.setTotal_plazas(0);
			
			if (jsonObjectOfertaBarrido.get("total_playas") != null && jsonObjectOfertaBarrido.get("total_playas") != "") {
				clsOfertaBarrido.setTotal_playas(Integer.parseInt(jsonObjectOfertaBarrido.get("total_playas").toString()));
			}
			else
				clsOfertaBarrido.setTotal_playas(0);
			
			if (jsonObjectOfertaBarrido.get("total_otros") != null && jsonObjectOfertaBarrido.get("total_otros") != "") {
				clsOfertaBarrido.setTotal_otros(Integer.parseInt(jsonObjectOfertaBarrido.get("total_otros").toString()));
			}
			else
				clsOfertaBarrido.setTotal_otros(0);
			
			if (jsonObjectOfertaBarrido.get("cod_usuario") != null && jsonObjectOfertaBarrido.get("cod_usuario") != "") {
				clsOfertaBarrido.setCodigo_usuario(Integer.parseInt(jsonObjectOfertaBarrido.get("cod_usuario").toString()));
			}
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
