package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsLugarAprovechamiento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.LugarAprovechamiento2DAO;

public class LugarAprovechamiento2Service {

	private static Logger log = Logger.getLogger(LugarAprovechamiento2Service.class);
	LugarAprovechamiento2DAO lugarAprovechamiento2DAO = new LugarAprovechamiento2DAO();
	
	public ClsResultado obtener(ClsLugarAprovechamiento clsLugarAprovechamiento){
		ClsResultado clsResultado = null;
		try {
			clsResultado = lugarAprovechamiento2DAO.obtener(clsLugarAprovechamiento);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = lugarAprovechamiento2DAO.insertar(clsLugarAprovechamiento);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsLugarAprovechamiento clsLugarAprovechamiento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = lugarAprovechamiento2DAO.actualizar(clsLugarAprovechamiento);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
