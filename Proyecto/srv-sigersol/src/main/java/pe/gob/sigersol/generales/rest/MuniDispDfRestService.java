package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsMes;
import pe.gob.sigersol.entities.ClsMuniDispDf;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.MuniDispDfService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase MuniDispDfRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de disposicion final permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/MuniDispDf")
@RequestScoped
public class MuniDispDfRestService {

	private static Logger log = Logger.getLogger(MuniDispDfRestService.class.getName());
	private ClsMuniDispDf clsMuniDispDf;
	private ClsMes clsMes;
	private ClsDisposicionFinal clsDisposicionFinal;
	private MuniDispDfService muniDispDfService;
	private List<ClsMuniDispDf> listaMuniDispDf;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarMuniDisp")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarMuniDisp(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			muniDispDfService = new MuniDispDfService();
			
			jsonEntradaListaMuniDisp(paramJson);
			
			log.info("**Obtener Composicion Municipales**");
			
			clsResultado = muniDispDfService.listarMuniDisp(clsMuniDispDf);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstMuniDisp\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		muniDispDfService = new MuniDispDfService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			jsonEntradaMuniDisp(paramJson);

			for (ClsMuniDispDf clsMuniDispDf : listaMuniDispDf) {
				clsResultado = muniDispDfService.insertar(clsMuniDispDf);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		muniDispDfService = new MuniDispDfService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaMuniDisp(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsMuniDispDf clsMuniDispDf : listaMuniDispDf) {
			clsResultado = muniDispDfService.actualizar(clsMuniDispDf);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de MuniDispDf
	 * 
	 * */
	private void jsonEntradaMuniDisp(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> MuniDispDfRestService : jsonEntradaMuniDisp" + jsonEntrada);

		JSONObject jsonObjectResMunicipales;
		JSONObject jsonObjectRelleno;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaMuniDispDf = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsMuniDispDf = new ClsMuniDispDf();
			clsMes = new ClsMes();
			clsDisposicionFinal = new ClsDisposicionFinal();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("disposicion_final_id") != null && innerObject.get("disposicion_final_id").toString() != "") {
				clsDisposicionFinal.setDisposicion_final_id(Integer.parseInt(innerObject.get("disposicion_final_id").toString()));
			}
			
			clsMuniDispDf.setDisposicionFinal(clsDisposicionFinal);
			
			if (innerObject.get("mes") != null && innerObject.get("mes").toString() != "") {
				jsonObjectResMunicipales  = (JSONObject) innerObject.get("mes");
				clsMes.setMes_id(Integer.parseInt(jsonObjectResMunicipales.get("mes_id").toString()));
			}
			clsMuniDispDf.setMes(clsMes);
			
			/*if (innerObject.get("selec_relleno") != null && innerObject.get("selec_relleno").toString() != "") {
				jsonObjectRelleno  = (JSONObject) innerObject.get("selec_relleno");
				clsMuniDispDf.setSelec_relleno(Integer.parseInt(jsonObjectRelleno.get("sitio_disposicion_id").toString()));
			}*/
			
			if (innerObject.get("selec_relleno") != null && innerObject.get("selec_relleno").toString() != "") {
				clsMuniDispDf.setSelec_relleno(Integer.parseInt(innerObject.get("selec_relleno").toString()));
			}
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsMuniDispDf.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else {
				clsMuniDispDf.setCantidad(0);
			}
			
			if (innerObject.get("metodo_usado") != null && innerObject.get("metodo_usado").toString() != "") {
				clsMuniDispDf.setMetodo_usado(Integer.parseInt(innerObject.get("metodo_usado").toString()));
			} else {
				clsMuniDispDf.setMetodo_usado(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsMuniDispDf.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaMuniDispDf.add(clsMuniDispDf);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaMuniDisp(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> MuniDispDfRestService : jsonEntradaListaMuniDisp" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectCompMuni;
		JSONParser jsonParser = new JSONParser();
		
		clsMuniDispDf = new ClsMuniDispDf();
		clsMes = new ClsMes();
		clsDisposicionFinal = new ClsDisposicionFinal();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectCompMuni = (JSONObject) jsonObjectPrincipal.get("MuniDispDf");
			
			if (jsonObjectCompMuni.get("disposicion_final_id") != null && jsonObjectCompMuni.get("disposicion_final_id") != "" &&
					jsonObjectCompMuni.get("disposicion_final_id") != " ") {
				clsDisposicionFinal.setDisposicion_final_id(Integer.parseInt(jsonObjectCompMuni.get("disposicion_final_id").toString()));
			}
			else
				clsDisposicionFinal.setDisposicion_final_id(-1);

			clsMuniDispDf.setDisposicionFinal(clsDisposicionFinal);
			
			if (jsonObjectCompMuni.get("selec_relleno") != null && jsonObjectCompMuni.get("selec_relleno") != "" &&
					jsonObjectCompMuni.get("selec_relleno") != " ") {
				clsMuniDispDf.setSelec_relleno(Integer.parseInt(jsonObjectCompMuni.get("selec_relleno").toString()));
			}
			else
				clsMuniDispDf.setSelec_relleno(-1);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
