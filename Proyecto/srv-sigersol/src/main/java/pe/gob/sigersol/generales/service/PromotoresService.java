package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsPromotores;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.PromotoresDAO;

public class PromotoresService {

	private static Logger log = Logger.getLogger(PromotoresService.class);
	PromotoresDAO promotoresDAO = new PromotoresDAO();
	
	public ClsResultado listarPromotores(ClsPromotores clsPromotores){
		ClsResultado clsResultado = null;
		try {
			clsResultado = promotoresDAO.listarPromotores(clsPromotores);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsPromotores clsPromotores) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = promotoresDAO.insertar(clsPromotores);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsPromotores clsPromotores) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = promotoresDAO.actualizar(clsPromotores);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
