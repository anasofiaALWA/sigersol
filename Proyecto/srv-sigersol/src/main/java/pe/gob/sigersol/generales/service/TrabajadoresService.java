package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsInfoCeldas;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTrabajadores;
import pe.gob.sigersol.entities.dao.TrabajadoresDAO;

public class TrabajadoresService {

	private static Logger log = Logger.getLogger(TrabajadoresService.class);
	TrabajadoresDAO trabajadoresDAO = new TrabajadoresDAO();

	public ClsResultado listarTrabajadores(ClsTrabajadores clsTrabajadores){
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresDAO.listarTrabajadores(clsTrabajadores);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTrabajadores clsTrabajadores) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresDAO.insertar(clsTrabajadores);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
