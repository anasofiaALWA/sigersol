package pe.gob.sigersol.entities;

public class ClsCantPapeleras {

	private Integer cant_papeleras_id;
	private ClsAlmacenamiento almacenamiento;
	private ClsEstado estado;
	private Integer mercados;
	private Integer parques;
	private Integer vias;
	private Integer otros;
	private Integer codigo_usuario;
	
	public Integer getCant_papeleras_id() {
		return cant_papeleras_id;
	}
	public void setCant_papeleras_id(Integer cant_papeleras_id) {
		this.cant_papeleras_id = cant_papeleras_id;
	}
	public ClsAlmacenamiento getAlmacenamiento() {
		return almacenamiento;
	}
	public void setAlmacenamiento(ClsAlmacenamiento almacenamiento) {
		this.almacenamiento = almacenamiento;
	}
	public ClsEstado getEstado() {
		return estado;
	}
	public void setEstado(ClsEstado estado) {
		this.estado = estado;
	}
	public Integer getMercados() {
		return mercados;
	}
	public void setMercados(Integer mercados) {
		this.mercados = mercados;
	}
	public Integer getParques() {
		return parques;
	}
	public void setParques(Integer parques) {
		this.parques = parques;
	}
	public Integer getVias() {
		return vias;
	}
	public void setVias(Integer vias) {
		this.vias = vias;
	}
	public Integer getOtros() {
		return otros;
	}
	public void setOtros(Integer otros) {
		this.otros = otros;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
