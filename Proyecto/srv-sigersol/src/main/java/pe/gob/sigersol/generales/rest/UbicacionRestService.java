package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsLugarAprovechamiento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUbicacion;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.CostoReferencialService;
import pe.gob.sigersol.generales.service.UbicacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/ubicacion")
@RequestScoped
public class UbicacionRestService {

	private static Logger log = Logger.getLogger(UbicacionRestService.class.getName());
	private ClsUbicacion clsUbicacion ;
	private ClsLugarAprovechamiento clsLugarAprovechamiento ;
	private UbicacionService ubicacionService;
	private List<ClsUbicacion> listaUbicacion;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarUbicacion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarUbicacion(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			ubicacionService = new UbicacionService();
			
			jsonEntradaListaCostoReferencial(paramJson);
			
			log.info("**Obtener Residuos Aprovechados**");
			
			clsResultado = ubicacionService.listarUbicacion(clsUbicacion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"Ubicacion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCoordenada")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCoordenada(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			ubicacionService = new UbicacionService();
			
			jsonEntradaListaCostoReferencial(paramJson);
			
			log.info("**Obtener Residuos Aprovechados**");
			
			clsResultado = ubicacionService.listarCoordenada(clsUbicacion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCoordenadas\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		ubicacionService = new UbicacionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaCostoReferencial(paramJson);

			clsResultado = ubicacionService.insertar(clsUbicacion);
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ubicacionService = new UbicacionService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCostoReferencial(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsUbicacion clsUbicacion : listaUbicacion) {
			clsResultado = ubicacionService.actualizar(clsUbicacion);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de ResiduosAprovechados
	 * 
	 * */
	private void jsonEntradaCostoReferencial(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CostoReferencialRestService :	jsonEntradaCostoReferencial" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectValorizacion;
		JSONParser jsonParser = new JSONParser();
		
		clsLugarAprovechamiento = new ClsLugarAprovechamiento();
		clsUbicacion = new ClsUbicacion();
		
		try {
			
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectValorizacion = (JSONObject) jsonObjectPrincipal.get("Ubicacion");
			
			if (jsonObjectValorizacion.get("lugar_aprovechamiento_id") != null && jsonObjectValorizacion.get("lugar_aprovechamiento_id").toString() != "") {
				clsLugarAprovechamiento.setLugar_aprovechamiento_id(Integer.parseInt(jsonObjectValorizacion.get("lugar_aprovechamiento_id").toString()));
			}
			
			clsUbicacion.setLugar_aprovechamiento(clsLugarAprovechamiento);
			
			if (jsonObjectValorizacion.get("zona_coordenada") != null && jsonObjectValorizacion.get("zona_coordenada").toString() != "") {
				clsUbicacion.setZona_coordenada(jsonObjectValorizacion.get("zona_coordenada").toString());
			}
			else {
				clsUbicacion.setZona_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("zona_coordenada") != null && jsonObjectValorizacion.get("zona_coordenada").toString() != "") {
				clsUbicacion.setZona_coordenada(jsonObjectValorizacion.get("zona_coordenada").toString());
			}
			else {
				clsUbicacion.setZona_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("norte_coordenada") != null && jsonObjectValorizacion.get("norte_coordenada").toString() != "") {
				clsUbicacion.setNorte_coordenada(jsonObjectValorizacion.get("norte_coordenada").toString());
			}
			else {
				clsUbicacion.setNorte_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("sur_coordenada") != null && jsonObjectValorizacion.get("sur_coordenada").toString() != "") {
				clsUbicacion.setSur_coordenada(jsonObjectValorizacion.get("sur_coordenada").toString());
			}
			else {
				clsUbicacion.setSur_coordenada("");
			}
			
			if (jsonObjectValorizacion.get("div_politica_dep") != null && jsonObjectValorizacion.get("div_politica_dep").toString() != "") {
				clsUbicacion.setDiv_politica_dep(jsonObjectValorizacion.get("div_politica_dep").toString());
			}
			else {
				clsUbicacion.setDiv_politica_dep("");
			}
			
			if (jsonObjectValorizacion.get("div_politica_prov") != null && jsonObjectValorizacion.get("div_politica_prov").toString() != "") {
				clsUbicacion.setDiv_politica_prov(jsonObjectValorizacion.get("div_politica_prov").toString());
			}
			else {
				clsUbicacion.setDiv_politica_prov("");
			}
			
			if (jsonObjectValorizacion.get("div_politica_dist") != null && jsonObjectValorizacion.get("div_politica_dist").toString() != "") {
				clsUbicacion.setDiv_politica_dist(jsonObjectValorizacion.get("div_politica_dist").toString());
			}
			else {
				clsUbicacion.setDiv_politica_dist("");
			}
			
			if (jsonObjectValorizacion.get("direccion") != null && jsonObjectValorizacion.get("direccion").toString() != "") {
				clsUbicacion.setDireccion(jsonObjectValorizacion.get("direccion").toString());
			}
			else {
				clsUbicacion.setDireccion("");
			}
			
			if (jsonObjectValorizacion.get("referencia") != null && jsonObjectValorizacion.get("referencia").toString() != "") {
				clsUbicacion.setReferencia(jsonObjectValorizacion.get("referencia").toString());
			}
			else {
				clsUbicacion.setReferencia("");
			}

			if (jsonObjectValorizacion.get("cod_usuario") != null && jsonObjectValorizacion.get("cod_usuario").toString() != "") {
				clsUbicacion.setCodigo_usuario(Integer.parseInt(jsonObjectValorizacion.get("cod_usuario").toString()));
			}
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
		log.info("----------------------------------------------------------------------------------");
		
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCostoReferencial(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CostoReferencialRestService :	jsonEntradaListaCostoReferencial" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResiduosAprovechados;
		JSONParser jsonParser = new JSONParser();
		
		clsUbicacion = new ClsUbicacion();
		clsLugarAprovechamiento= new ClsLugarAprovechamiento();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResiduosAprovechados = (JSONObject) jsonObjectPrincipal.get("Ubicacion");
			
			if (jsonObjectResiduosAprovechados.get("lugar_aprovechamiento_id") != null && jsonObjectResiduosAprovechados.get("lugar_aprovechamiento_id") != "" &&
					jsonObjectResiduosAprovechados.get("lugar_aprovechamiento_id") != " ") {
				clsLugarAprovechamiento.setLugar_aprovechamiento_id(Integer.parseInt(jsonObjectResiduosAprovechados.get("lugar_aprovechamiento_id").toString()));
			}
			else
				clsLugarAprovechamiento.setLugar_aprovechamiento_id(-1);
			
			clsUbicacion.setLugar_aprovechamiento(clsLugarAprovechamiento);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
