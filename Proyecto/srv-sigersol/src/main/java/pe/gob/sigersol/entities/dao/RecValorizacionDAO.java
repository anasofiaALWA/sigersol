package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsRecValorizacion;
import pe.gob.sigersol.entities.ClsResultado;

public class RecValorizacionDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(RecValorizacionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_rec_valor = "{call SIGERSOLBL.SGR_SP_LIST_SGR_REC_VALOR(?,?)}";
	private final String sgr_sp_ins_rec_valorizacion= "{call SIGERSOLBL.SGR_SP_INS_REC_VALORIZACION(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_rec_valorizacion = "{call SIGERSOLBL.SGR_SP_UPD_REC_VALORIZACION(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsRecValorizacion clsRecValorizacion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsRecValorizacion item = new ClsRecValorizacion();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		
		String[] s; 
		
		con = null;
		String sql = this.sgr_sp_list_sgr_rec_valor;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecValorizacion.getCiclo_grs_id().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setRec_valorizacion_id(rs.getInt("REC_VALORIZACION_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCiclo_grs_id(cicloGestionResiduos);
					item.setAdm_serv_id(rs.getString("ADM_SERV_ID"));
					item.setCosto_anual(rs.getInt("COSTO_ANUAL"));
					item.setFlag_recoleccion(rs.getInt("FLAG_RECOLECCION"));
					item.setFrecuencia(rs.getString("FRECUENCIA"));
					
					s = item.getFrecuencia().split("/");
					item.setFr1(Integer.parseInt(s[0]));
					item.setFr2(Integer.parseInt(s[1]));
					
					item.setCantidad_residuos_organicos(rs.getInt("CANT_RESIDUOS_ORGANICOS"));
					item.setCantidad_residuos_inorganicos(rs.getInt("CANT_RESIDUOS_INORGANICOS"));
					item.setDomiciliarios(rs.getInt("DOMICILIARIOS"));
					item.setNo_domiciliarios(rs.getInt("NO_DOMICILIARIOS"));
					item.setOtros_descripcion(rs.getString("OTROS_DESCRIPCION"));
					item.setOtros_antig_promedio(rs.getInt("OTROS_ANTIG_PROMEDIO"));
					item.setOtros_capacidad(rs.getInt("OTROS_CAPACIDAD"));
			        item.setOtros_recoleccion_anual(rs.getInt("OTROS_RECOLECCION_ANUAL"));
			        item.setOtros_numero_unidades(rs.getInt("OTROS_NUMERO_UNIDADES"));
					item.setCodigo_usuario(rs.getInt("COD_USUARIO_CREACION"));
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsRecValorizacion clsRecValorizacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_rec_valorizacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecValorizacion.getCiclo_grs_id().getCiclo_grs_id());
			cs.setString(2, clsRecValorizacion.getAdm_serv_id());
			cs.setInt(3, clsRecValorizacion.getCosto_anual());
			cs.setInt(4, clsRecValorizacion.getFlag_recoleccion());
			cs.setString(5, clsRecValorizacion.getFrecuencia());
			cs.setInt(6, clsRecValorizacion.getCantidad_residuos_organicos());
			cs.setInt(7, clsRecValorizacion.getCantidad_residuos_inorganicos());
			cs.setInt(8, clsRecValorizacion.getDomiciliarios());
			cs.setInt(9, clsRecValorizacion.getNo_domiciliarios());
			cs.setString(10, clsRecValorizacion.getOtros_descripcion());
			cs.setInt(11, clsRecValorizacion.getOtros_antig_promedio());
			cs.setInt(12, clsRecValorizacion.getOtros_capacidad());
			cs.setInt(13, clsRecValorizacion.getOtros_recoleccion_anual());
			cs.setInt(14, clsRecValorizacion.getOtros_numero_unidades());
			cs.setInt(15, clsRecValorizacion.getCodigo_usuario());
			cs.registerOutParameter(16, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(16);
			
			log.info("RecValorizacionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsRecValorizacion clsRecValorizacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_rec_valorizacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecValorizacion.getRec_valorizacion_id());
			cs.setInt(2, clsRecValorizacion.getCiclo_grs_id().getCiclo_grs_id());
			cs.setString(3, clsRecValorizacion.getAdm_serv_id());
			cs.setInt(4, clsRecValorizacion.getCosto_anual());
			cs.setInt(5, clsRecValorizacion.getFlag_recoleccion());
			cs.setString(6, clsRecValorizacion.getFrecuencia());
			cs.setInt(7, clsRecValorizacion.getCantidad_residuos_organicos());
			cs.setInt(8, clsRecValorizacion.getCantidad_residuos_inorganicos());
			cs.setInt(9, clsRecValorizacion.getDomiciliarios());
			cs.setInt(10, clsRecValorizacion.getNo_domiciliarios());
			cs.setString(11, clsRecValorizacion.getOtros_descripcion());
			cs.setInt(12, clsRecValorizacion.getOtros_antig_promedio());
			cs.setInt(13, clsRecValorizacion.getOtros_capacidad());
			cs.setInt(14, clsRecValorizacion.getOtros_recoleccion_anual());
			cs.setInt(15, clsRecValorizacion.getOtros_numero_unidades());
			cs.setInt(16, clsRecValorizacion.getCodigo_usuario());
			cs.registerOutParameter(17, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(17);

			log.info("RecValorizacionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
