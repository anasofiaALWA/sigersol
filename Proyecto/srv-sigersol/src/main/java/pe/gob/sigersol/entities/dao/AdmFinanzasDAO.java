package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAdmFinanzas;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;

public class AdmFinanzasDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(AdmFinanzasDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_adm_finanzas = "{call SIGERSOLBL.SGR_SP_LIST_ADM_FINANZAS(?,?)}";
	private final String sgr_sp_ins_adm_finanzas = "{call SIGERSOLBL.SGR_SP_INS_ADM_FINANZAS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_adm_finanzas = "{call SIGERSOLBL.SGR_SP_UPD_ADM_FINANZAS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsAdmFinanzas clsAdmFinanzas) {
		ClsResultado clsResultado = new ClsResultado();
		ClsAdmFinanzas item = new ClsAdmFinanzas();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_adm_finanzas;
		try {			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAdmFinanzas.getCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setAdm_finanzas_id(rs.getInt("ADM_FINANZAS_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(cicloGestionResiduos);
					item.setRecaudacion_arbitrios(rs.getInt("RECAUDACION_ARBITRIOS"));
					item.setCant_predios(rs.getInt("CANT_PREDIOS"));
					item.setPredios_afectos(rs.getInt("PREDIOS_AFECTOS"));
					item.setFlag_ambiental(rs.getInt("FLAG_AMBIENTAL"));
					item.setNombre_ambiental(rs.getString("NOMBRE_AMBIENTAL"));
					item.setFlag_limpieza(rs.getInt("FLAG_LIMPIEZA"));
					item.setFlag_rentas(rs.getInt("FLAG_RENTAS"));
					item.setFlag_poi(rs.getInt("FLAG_POI"));
					item.setCarga_poi(rs.getInt("CARGA_POI"));
				    item.setFlag_pp(rs.getInt("FLAG_PP"));
				    item.setCodigo_pp(rs.getString("CODIGO_PP"));
				    item.setMonto_programado(rs.getInt("MONTO_PROGRAMADO"));
				    item.setCodigo_programado(rs.getString("CODIGO_PROGRAMADO"));
				    item.setMonto_ejecutado(rs.getInt("MONTO_EJECUTADO"));
				    item.setCodigo_ejecutado(rs.getString("CODIGO_EJECUTADO"));
				    item.setFlag_pmi(rs.getInt("FLAG_PMI"));
				    item.setProgramado_inv(rs.getInt("PROGRAMADO_INV"));
				    item.setEjecutado_inv(rs.getInt("EJECUTADO_INV"));
				    item.setFlag_proyecto(rs.getInt("FLAG_PROYECTO"));
				    item.setCodigo_proyecto(rs.getString("CODIGO_PROYECTO"));
				    item.setFuente_financiamiento(rs.getInt("FUENTE_FINANCIAMIENTO"));
				    item.setInv_no_proyectos(rs.getInt("INV_NO_PROYECTOS"));
				    item.setFlag_manejo(rs.getInt("FLAG_MANEJO"));
				    item.setFlag_pigars(rs.getInt("FLAG_PIGARS"));
				    item.setFlag_instrumento(rs.getInt("FLAG_INSTRUMENTO"));
				    item.setFlag_licencia(rs.getInt("FLAG_LICENCIA"));
				    item.setFlag_exp_tecnico(rs.getInt("FLAG_EXP_TECNICO"));
				    item.setFlag_ordenanza(rs.getInt("FLAG_ORDENANZA"));
				    item.setFlag_tributario(rs.getInt("FLAG_TRIBUTARIO"));
				    
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsAdmFinanzas clsAdmFinanzas) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_adm_finanzas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAdmFinanzas.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsAdmFinanzas.getRecaudacion_arbitrios());
			cs.setInt(3, clsAdmFinanzas.getCant_predios());
			cs.setInt(4, clsAdmFinanzas.getPredios_afectos());
			cs.setInt(5, clsAdmFinanzas.getFlag_ambiental());
			cs.setString(6, clsAdmFinanzas.getNombre_ambiental());
			cs.setInt(7, clsAdmFinanzas.getFlag_limpieza());
			cs.setInt(8, clsAdmFinanzas.getFlag_rentas());
			cs.setInt(9, clsAdmFinanzas.getFlag_poi());
			cs.setInt(10, clsAdmFinanzas.getCarga_poi());
			cs.setInt(11, clsAdmFinanzas.getFlag_pp());
			cs.setString(12, clsAdmFinanzas.getCodigo_pp());
			cs.setInt(13, clsAdmFinanzas.getMonto_programado());
			cs.setString(14, clsAdmFinanzas.getCodigo_programado());
			cs.setInt(15, clsAdmFinanzas.getMonto_ejecutado());
			cs.setString(16, clsAdmFinanzas.getCodigo_ejecutado());
			cs.setInt(17, clsAdmFinanzas.getFlag_pmi());
			cs.setInt(18, clsAdmFinanzas.getProgramado_inv());
			cs.setInt(19, clsAdmFinanzas.getEjecutado_inv());
			cs.setInt(20, clsAdmFinanzas.getFlag_proyecto());
			cs.setString(21, clsAdmFinanzas.getCodigo_proyecto());
			cs.setInt(22, clsAdmFinanzas.getFuente_financiamiento());
			cs.setInt(23, clsAdmFinanzas.getInv_no_proyectos());
			cs.setInt(24, clsAdmFinanzas.getFlag_manejo());
			cs.setInt(25, clsAdmFinanzas.getFlag_pigars());
			cs.setInt(26, clsAdmFinanzas.getFlag_instrumento());
			cs.setInt(27, clsAdmFinanzas.getFlag_licencia());
			cs.setInt(28, clsAdmFinanzas.getFlag_exp_tecnico());
			cs.setInt(29, clsAdmFinanzas.getFlag_ordenanza());
			cs.setInt(30, clsAdmFinanzas.getFlag_tributario());
			cs.setInt(31, clsAdmFinanzas.getCodigo_usuario());
			cs.registerOutParameter(32, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(32);
			
			log.info("AdmFinanzasDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsAdmFinanzas clsAdmFinanzas) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_adm_finanzas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAdmFinanzas.getAdm_finanzas_id());
			cs.setInt(2, clsAdmFinanzas.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(3, clsAdmFinanzas.getRecaudacion_arbitrios());
			cs.setInt(4, clsAdmFinanzas.getCant_predios());
			cs.setInt(5, clsAdmFinanzas.getPredios_afectos());
			cs.setInt(6, clsAdmFinanzas.getFlag_ambiental());
			cs.setString(7, clsAdmFinanzas.getNombre_ambiental());
			cs.setInt(8, clsAdmFinanzas.getFlag_limpieza());
			cs.setInt(9, clsAdmFinanzas.getFlag_rentas());
			cs.setInt(10, clsAdmFinanzas.getFlag_poi());
			cs.setInt(11, clsAdmFinanzas.getCarga_poi());
			cs.setInt(12, clsAdmFinanzas.getFlag_pp());
			cs.setString(13, clsAdmFinanzas.getCodigo_pp());
			cs.setInt(14, clsAdmFinanzas.getMonto_programado());
			cs.setString(15, clsAdmFinanzas.getCodigo_programado());
			cs.setInt(16, clsAdmFinanzas.getMonto_ejecutado());
			cs.setString(17, clsAdmFinanzas.getCodigo_ejecutado());
			cs.setInt(18, clsAdmFinanzas.getFlag_pmi());
			cs.setInt(19, clsAdmFinanzas.getProgramado_inv());
			cs.setInt(20, clsAdmFinanzas.getEjecutado_inv());
			cs.setInt(21, clsAdmFinanzas.getFlag_proyecto());
			cs.setString(22, clsAdmFinanzas.getCodigo_proyecto());
			cs.setInt(23, clsAdmFinanzas.getFuente_financiamiento());
			cs.setInt(24, clsAdmFinanzas.getInv_no_proyectos());
			cs.setInt(25, clsAdmFinanzas.getFlag_manejo());
			cs.setInt(26, clsAdmFinanzas.getFlag_pigars());
			cs.setInt(27, clsAdmFinanzas.getFlag_instrumento());
			cs.setInt(28, clsAdmFinanzas.getFlag_licencia());
			cs.setInt(29, clsAdmFinanzas.getFlag_exp_tecnico());
			cs.setInt(30, clsAdmFinanzas.getFlag_ordenanza());
			cs.setInt(31, clsAdmFinanzas.getFlag_tributario());
			cs.setInt(32, clsAdmFinanzas.getCodigo_usuario());
			cs.registerOutParameter(33, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(33);

			log.info("AdmFinanzasDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
