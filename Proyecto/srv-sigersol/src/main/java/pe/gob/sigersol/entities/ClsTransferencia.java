package pe.gob.sigersol.entities;

public class ClsTransferencia {

	private Integer transferencia_id;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private Integer flag_transferencia;
	private String adm_serv_id;
	private String zona_coordenada;
	private String norte_coordenada;
	private String este_coordenada;
	private String division_politica;
	private String departamento;
	private String provincia;
	private String distrito;
	private String direccion_iga;
	private String referencia_iga;
	private Integer residuos_transferidos;
	private Integer costo_anual;
	private Integer frecuencia_diaria;
	private Integer distancia_promedio;	
	private Integer codigo_usuario;
	
	public Integer getTransferencia_id() {
		return transferencia_id;
	}
	public void setTransferencia_id(Integer transferencia_id) {
		this.transferencia_id = transferencia_id;
	}
	public ClsCicloGestionResiduos getClsCicloGestionResiduos() {
		return clsCicloGestionResiduos;
	}
	public void setClsCicloGestionResiduos(ClsCicloGestionResiduos clsCicloGestionResiduos) {
		this.clsCicloGestionResiduos = clsCicloGestionResiduos;
	}
	public Integer getFlag_transferencia() {
		return flag_transferencia;
	}
	public void setFlag_transferencia(Integer flag_transferencia) {
		this.flag_transferencia = flag_transferencia;
	}
	public String getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(String adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public String getZona_coordenada() {
		return zona_coordenada;
	}
	public void setZona_coordenada(String zona_coordenada) {
		this.zona_coordenada = zona_coordenada;
	}
	public String getNorte_coordenada() {
		return norte_coordenada;
	}
	public void setNorte_coordenada(String norte_coordenada) {
		this.norte_coordenada = norte_coordenada;
	}
	public String getEste_coordenada() {
		return este_coordenada;
	}
	public void setEste_coordenada(String este_coordenada) {
		this.este_coordenada = este_coordenada;
	}
	public void setDivision_politica(String division_politica) {
		this.division_politica = division_politica;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getDireccion_iga() {
		return direccion_iga;
	}
	public void setDireccion_iga(String direccion_iga) {
		this.direccion_iga = direccion_iga;
	}
	public String getReferencia_iga() {
		return referencia_iga;
	}
	public void setReferencia_iga(String referencia_iga) {
		this.referencia_iga = referencia_iga;
	}
	public Integer getResiduos_transferidos() {
		return residuos_transferidos;
	}
	public void setResiduos_transferidos(Integer residuos_transferidos) {
		this.residuos_transferidos = residuos_transferidos;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getFrecuencia_diaria() {
		return frecuencia_diaria;
	}
	public void setFrecuencia_diaria(Integer frecuencia_diaria) {
		this.frecuencia_diaria = frecuencia_diaria;
	}
	public Integer getDistancia_promedio() {
		return distancia_promedio;
	}
	public void setDistancia_promedio(Integer distancia_promedio) {
		this.distancia_promedio = distancia_promedio;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
