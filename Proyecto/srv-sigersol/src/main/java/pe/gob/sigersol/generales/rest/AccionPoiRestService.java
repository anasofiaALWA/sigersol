package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAccionPoi;
import pe.gob.sigersol.entities.ClsAccionTipo;
import pe.gob.sigersol.entities.ClsEducacionAmbiental;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.AccionPoiService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/accionPoiX")
@RequestScoped
public class AccionPoiRestService {
	
	private static Logger log = Logger.getLogger(AccionPoiRestService.class.getName());
	private ClsAccionPoi clsAccionPoi;
	private ClsEducacionAmbiental clsEducacionAmbiental;
	private ClsAccionTipo clsAccionTipo;
	private AccionPoiService accionPoiService;
	private List<ClsAccionPoi> listaAccionPoi;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarAccionPoi")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarAccionPoi(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			accionPoiService = new AccionPoiService();
			
			jsonEntradaListaAccionPoi(paramJson);
			
			log.info("**Obtener Accion Poi**");
			
			clsResultado = accionPoiService.listarAccionPoi(clsAccionPoi);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstAccionPoi\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		accionPoiService = new AccionPoiService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaAccionPoi(paramJson);

			for (ClsAccionPoi clsAccionPoi : listaAccionPoi) {
				clsResultado = accionPoiService.insertar(clsAccionPoi);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		accionPoiService = new AccionPoiService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaAccionPoi(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsAccionPoi clsAccionPoi : listaAccionPoi) {
			clsResultado = accionPoiService.actualizar(clsAccionPoi);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaAccionPoi(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AccionPoiRestService :	 jsonEntradaAccionPoi" + jsonEntrada);

		JSONObject jsonObjectAccionTipo;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaAccionPoi = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsAccionPoi = new ClsAccionPoi();
			clsAccionTipo = new ClsAccionTipo();
			clsEducacionAmbiental = new ClsEducacionAmbiental();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("educacion_ambiental_id") != null && innerObject.get("educacion_ambiental_id").toString() != "") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(innerObject.get("educacion_ambiental_id").toString()));
			}
			else {
				clsEducacionAmbiental.setEducacion_ambiental_id(-1);
			}
			
			clsAccionPoi.setEducacion_ambiental(clsEducacionAmbiental);
			
			if (innerObject.get("accion_tipo") != null && innerObject.get("accion_tipo").toString() != "") {
				jsonObjectAccionTipo  = (JSONObject) innerObject.get("accion_tipo");
				clsAccionTipo.setAccion_tipo_id(Integer.parseInt(jsonObjectAccionTipo.get("accion_tipo_id").toString()));
			}
			clsAccionPoi.setAccion_tipo(clsAccionTipo);
			
			if (innerObject.get("tipo_accion") != null && innerObject.get("tipo_accion").toString() != "") {
				clsAccionPoi.setTipo_accion(innerObject.get("tipo_accion").toString());
			} else{
				clsAccionPoi.setTipo_accion("");
			}
			
			if (innerObject.get("cantidad_accion") != null && innerObject.get("cantidad_accion").toString() != "") {
				clsAccionPoi.setCantidad_accion(Integer.parseInt(innerObject.get("cantidad_accion").toString()));
			} else{
				clsAccionPoi.setCantidad_accion(0);
			}
			
			if (innerObject.get("presupuesto_asignado") != null && innerObject.get("presupuesto_asignado").toString() != "") {
				clsAccionPoi.setPresupuesto_asignado(Integer.parseInt(innerObject.get("presupuesto_asignado").toString()));
			} else{
				clsAccionPoi.setPresupuesto_asignado(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsAccionPoi.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaAccionPoi.add(clsAccionPoi);	
		}
	
		log.info("----------------------------------------------------------------------------------");
		
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaAccionPoi(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AccionPoiRestService : jsonEntradaListaAccionPoi" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectAccionPoi;
		JSONParser jsonParser = new JSONParser();
		
		clsAccionPoi = new ClsAccionPoi();
		clsAccionTipo = new ClsAccionTipo();
		clsEducacionAmbiental = new ClsEducacionAmbiental();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectAccionPoi = (JSONObject) jsonObjectPrincipal.get("AccionPoi");
				
			if (jsonObjectAccionPoi.get("educacion_ambiental_id") != null && jsonObjectAccionPoi.get("educacion_ambiental_id") != "" &&
					jsonObjectAccionPoi.get("educacion_ambiental_id") != " ") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(jsonObjectAccionPoi.get("educacion_ambiental_id").toString()));
			}
			else
				clsEducacionAmbiental.setEducacion_ambiental_id(-1);
			
			clsAccionPoi.setEducacion_ambiental(clsEducacionAmbiental);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
