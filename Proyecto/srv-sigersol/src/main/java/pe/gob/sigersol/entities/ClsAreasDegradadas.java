package pe.gob.sigersol.entities;

public class ClsAreasDegradadas {
	
	private Integer areas_degradadas_id;
	private ClsDisposicionFinal disposicion_final;
	private Integer numero_areas;
	private Integer coordenadas;
	private Integer ubicacion;
	private String zona_coordenada;
	private String norte_coordenada;
	private String sur_coordenada;
	private String division_politica;
	private String direccion;
	private String referencia;
	private Integer adm_serv_id;
	private Integer cantidad_colaboradores;
	private Integer terreno;
	private Integer anio_inicio;
	private Integer costo;
	private Integer area_total;
	private String responsable;
	private Integer tipo_manejo;
	private Integer altura_promedio;
	private Integer altura_napa;
	private Integer area_utilizada;
	private Integer area_disponible;
	private Integer codigo_usuario;
	
	public Integer getAreas_degradadas_id() {
		return areas_degradadas_id;
	}
	public void setAreas_degradadas_id(Integer areas_degradadas_id) {
		this.areas_degradadas_id = areas_degradadas_id;
	}
	public ClsDisposicionFinal getDisposicion_final() {
		return disposicion_final;
	}
	public void setDisposicion_final(ClsDisposicionFinal disposicion_final) {
		this.disposicion_final = disposicion_final;
	}
	public Integer getNumero_areas() {
		return numero_areas;
	}
	public void setNumero_areas(Integer numero_areas) {
		this.numero_areas = numero_areas;
	}
	public Integer getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(Integer coordenadas) {
		this.coordenadas = coordenadas;
	}
	public Integer getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(Integer ubicacion) {
		this.ubicacion = ubicacion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public Integer getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(Integer adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCantidad_colaboradores() {
		return cantidad_colaboradores;
	}
	public void setCantidad_colaboradores(Integer cantidad_colaboradores) {
		this.cantidad_colaboradores = cantidad_colaboradores;
	}
	public Integer getAnio_inicio() {
		return anio_inicio;
	}
	public void setAnio_inicio(Integer anio_inicio) {
		this.anio_inicio = anio_inicio;
	}
	public Integer getCosto() {
		return costo;
	}
	public void setCosto(Integer costo) {
		this.costo = costo;
	}
	public Integer getArea_total() {
		return area_total;
	}
	public void setArea_total(Integer area_total) {
		this.area_total = area_total;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public Integer getTipo_manejo() {
		return tipo_manejo;
	}
	public void setTipo_manejo(Integer tipo_manejo) {
		this.tipo_manejo = tipo_manejo;
	}
	public Integer getAltura_promedio() {
		return altura_promedio;
	}
	public void setAltura_promedio(Integer altura_promedio) {
		this.altura_promedio = altura_promedio;
	}
	public Integer getAltura_napa() {
		return altura_napa;
	}
	public void setAltura_napa(Integer altura_napa) {
		this.altura_napa = altura_napa;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public String getZona_coordenada() {
		return zona_coordenada;
	}
	public void setZona_coordenada(String zona_coordenada) {
		this.zona_coordenada = zona_coordenada;
	}
	public String getNorte_coordenada() {
		return norte_coordenada;
	}
	public void setNorte_coordenada(String norte_coordenada) {
		this.norte_coordenada = norte_coordenada;
	}
	public String getSur_coordenada() {
		return sur_coordenada;
	}
	public void setSur_coordenada(String sur_coordenada) {
		this.sur_coordenada = sur_coordenada;
	}
	public String getDivision_politica() {
		return division_politica;
	}
	public void setDivision_politica(String division_politica) {
		this.division_politica = division_politica;
	}
	public Integer getTerreno() {
		return terreno;
	}
	public void setTerreno(Integer terreno) {
		this.terreno = terreno;
	}
	public Integer getArea_utilizada() {
		return area_utilizada;
	}
	public void setArea_utilizada(Integer area_utilizada) {
		this.area_utilizada = area_utilizada;
	}
	public Integer getArea_disponible() {
		return area_disponible;
	}
	public void setArea_disponible(Integer area_disponible) {
		this.area_disponible = area_disponible;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
