package pe.gob.sigersol.entities;

public class ClsAreaAbandonada {

	private Integer area_abandonadas_id;
	private ClsMunicipalidad municipalidad;
	private Integer anio_inicio;
	private Integer anio_cierre;
	private Integer area_total;
	private String responsable;
	private Integer altura_nf;
	private Integer cantidad;
	private String metodo_usado;
	private Integer codigo_usuario;
	
	public Integer getArea_abandonadas_id() {
		return area_abandonadas_id;
	}
	public void setArea_abandonadas_id(Integer area_abandonadas_id) {
		this.area_abandonadas_id = area_abandonadas_id;
	}
	public ClsMunicipalidad getMunicipalidad() {
		return municipalidad;
	}
	public void setMunicipalidad(ClsMunicipalidad municipalidad) {
		this.municipalidad = municipalidad;
	}
	public Integer getAnio_inicio() {
		return anio_inicio;
	}
	public void setAnio_inicio(Integer anio_inicio) {
		this.anio_inicio = anio_inicio;
	}
	public Integer getAnio_cierre() {
		return anio_cierre;
	}
	public void setAnio_cierre(Integer anio_cierre) {
		this.anio_cierre = anio_cierre;
	}
	public Integer getArea_total() {
		return area_total;
	}
	public void setArea_total(Integer area_total) {
		this.area_total = area_total;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public Integer getAltura_nf() {
		return altura_nf;
	}
	public void setAltura_nf(Integer altura_nf) {
		this.altura_nf = altura_nf;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public String getMetodo_usado() {
		return metodo_usado;
	}
	public void setMetodo_usado(String metodo_usado) {
		this.metodo_usado = metodo_usado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
