package pe.gob.sigersol.entities;

public class ClsResiduosAprovechados {

	private Integer residuos_aprovechados_id;
	private ClsValorizacion valorizacion;
	private ClsTipoResiduoSolido tipo_residuo_solido;
	private Integer enero;
	private Integer febrero;
	private Integer marzo;
	private Integer abril;
	private Integer mayo;
	private Integer junio;
	private Integer julio;
	private Integer agosto;
	private Integer setiembre;
	private Integer octubre;
	private Integer noviembre;
	private Integer diciembre;
	private Integer total;
	private Integer codigo_usuario;
	
	public Integer getResiduos_aprovechados_id() {
		return residuos_aprovechados_id;
	}
	public void setResiduos_aprovechados_id(Integer residuos_aprovechados_id) {
		this.residuos_aprovechados_id = residuos_aprovechados_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public ClsTipoResiduoSolido getTipo_residuo_solido() {
		return tipo_residuo_solido;
	}
	public void setTipo_residuo_solido(ClsTipoResiduoSolido tipo_residuo_solido) {
		this.tipo_residuo_solido = tipo_residuo_solido;
	}
	public Integer getEnero() {
		return enero;
	}
	public void setEnero(Integer enero) {
		this.enero = enero;
	}
	public Integer getFebrero() {
		return febrero;
	}
	public void setFebrero(Integer febrero) {
		this.febrero = febrero;
	}
	public Integer getMarzo() {
		return marzo;
	}
	public void setMarzo(Integer marzo) {
		this.marzo = marzo;
	}
	public Integer getAbril() {
		return abril;
	}
	public void setAbril(Integer abril) {
		this.abril = abril;
	}
	public Integer getMayo() {
		return mayo;
	}
	public void setMayo(Integer mayo) {
		this.mayo = mayo;
	}
	public Integer getJunio() {
		return junio;
	}
	public void setJunio(Integer junio) {
		this.junio = junio;
	}
	public Integer getJulio() {
		return julio;
	}
	public void setJulio(Integer julio) {
		this.julio = julio;
	}
	public Integer getAgosto() {
		return agosto;
	}
	public void setAgosto(Integer agosto) {
		this.agosto = agosto;
	}
	public Integer getSetiembre() {
		return setiembre;
	}
	public void setSetiembre(Integer setiembre) {
		this.setiembre = setiembre;
	}
	public Integer getOctubre() {
		return octubre;
	}
	public void setOctubre(Integer octubre) {
		this.octubre = octubre;
	}
	public Integer getNoviembre() {
		return noviembre;
	}
	public void setNoviembre(Integer noviembre) {
		this.noviembre = noviembre;
	}
	public Integer getDiciembre() {
		return diciembre;
	}
	public void setDiciembre(Integer diciembre) {
		this.diciembre = diciembre;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}	
}
