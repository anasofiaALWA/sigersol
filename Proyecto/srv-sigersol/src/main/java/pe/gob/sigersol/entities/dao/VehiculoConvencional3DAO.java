package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoConvencional;

public class VehiculoConvencional3DAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(VehiculoConvencional3DAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_vehiculo = "{call SIGERSOLBL.SGR_SP_INS_VEHICULO3(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_vehiculo = "{call SIGERSOLBL.SGR_SP_UPD_VEHICULO3(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_vehiculo = "{call SIGERSOLBL.SGR_SP_LIST_VEHICULO3(?,?)}";  
	
	public ClsResultado listarVehiculoConvencional(ClsVehiculoConvencional clsVehiculoConvencional){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsVehiculoConvencional> lista = new ArrayList<ClsVehiculoConvencional>();
		con = null;
		String sql = this.sgr_sp_list_vehiculo;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsVehiculoConvencional.getRecRcdOm().getRec_rcd_om_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsVehiculoConvencional item = new ClsVehiculoConvencional();
				
				try {
					item.setVehiculo_convencional_id(rs.getInt("VEHICULO_ID"));
					item.setPlaca(rs.getString("PLACA"));
					item.setRecorrido_anual(rs.getInt("RECORRIDO_ANUAL"));
					item.setTipo_vehiculo(rs.getString("TIPO_VEHICULO"));
					item.setAnio_fabricacion(rs.getInt("ANIO_FABRICACION"));
					item.setCapacidad(rs.getDouble("CAPACIDAD"));
					item.setTipo_combustible(rs.getInt("TIPO_COMBUSTIBLE"));
					item.setCantidad_combustible(rs.getDouble("CANTIDAD_COMBUSTIBLE"));
					item.setCosto_anual(rs.getInt("COSTO_ANUAL"));
					item.setRendimiento_anual(rs.getDouble("RENDIMIENTO_ANUAL"));
					item.setCapacidad_viaje(rs.getDouble("CAPACIDAD_VIAJE"));
					item.setEfectividad(rs.getInt("EFECTIVIDAD"));
					item.setPromedio_viajes(rs.getDouble("PROMEDIO_VIAJES"));
					item.setPromedio_turno(rs.getDouble("PROMEDIO_TURNO"));
					item.setDias_trabajados(rs.getInt("DIAS_TRABAJADOS"));
					item.setRecoleccion_media(rs.getDouble("RECOLECCION_MEDIA"));
					item.setRecoleccion_anual(rs.getDouble("RECOLECCION_ANUAL"));
					}
				 catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoConvencional clsVehiculoConvencional) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_vehiculo;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsVehiculoConvencional.getRecRcdOm().getRec_rcd_om_id());
			cs.setString(2, clsVehiculoConvencional.getPlaca());
			cs.setInt(3, clsVehiculoConvencional.getRecorrido_anual());
			cs.setString(4, clsVehiculoConvencional.getTipo_vehiculo());
			cs.setInt(5, clsVehiculoConvencional.getAnio_fabricacion());
			cs.setDouble(6, clsVehiculoConvencional.getCapacidad());
			cs.setInt(7, clsVehiculoConvencional.getTipo_combustible());
			cs.setDouble(8, clsVehiculoConvencional.getCantidad_combustible());
			cs.setInt(9, clsVehiculoConvencional.getCosto_anual());
			cs.setDouble(10, clsVehiculoConvencional.getRendimiento_anual());
			cs.setDouble(11, clsVehiculoConvencional.getCapacidad_viaje());
			cs.setInt(12, clsVehiculoConvencional.getEfectividad());
			cs.setDouble(13, clsVehiculoConvencional.getPromedio_viajes());
			cs.setDouble(14, clsVehiculoConvencional.getPromedio_turno());
			cs.setInt(15, clsVehiculoConvencional.getDias_trabajados());
			cs.setDouble(16, clsVehiculoConvencional.getRecoleccion_media());
			cs.setDouble(17, clsVehiculoConvencional.getRecoleccion_anual());			
			cs.setInt(18, clsVehiculoConvencional.getCodigo_usuario());
			cs.registerOutParameter(19, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(19);
			
			log.info("VehiculoConvencional3DAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsVehiculoConvencional clsVehiculoConvencional) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_vehiculo;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsVehiculoConvencional.getVehiculo_convencional_id());
			cs.setInt(2, clsVehiculoConvencional.getRecRcdOm().getRec_rcd_om_id());
			cs.setString(3, clsVehiculoConvencional.getPlaca());
			cs.setInt(4, clsVehiculoConvencional.getRecorrido_anual());
			cs.setString(5, clsVehiculoConvencional.getTipo_vehiculo());
			cs.setInt(6, clsVehiculoConvencional.getAnio_fabricacion());
			cs.setDouble(7, clsVehiculoConvencional.getCapacidad());
			cs.setInt(8, clsVehiculoConvencional.getTipo_combustible());
			cs.setDouble(9, clsVehiculoConvencional.getCantidad_combustible());
			cs.setInt(10, clsVehiculoConvencional.getCosto_anual());
			cs.setDouble(11, clsVehiculoConvencional.getRendimiento_anual());
			cs.setDouble(12, clsVehiculoConvencional.getCapacidad_viaje());
			cs.setInt(13, clsVehiculoConvencional.getEfectividad());
			cs.setDouble(14, clsVehiculoConvencional.getPromedio_viajes());
			cs.setDouble(15, clsVehiculoConvencional.getPromedio_turno());
			cs.setInt(16, clsVehiculoConvencional.getDias_trabajados());
			cs.setDouble(17, clsVehiculoConvencional.getRecoleccion_media());
			cs.setDouble(18, clsVehiculoConvencional.getRecoleccion_anual());			
			cs.setInt(19, clsVehiculoConvencional.getCodigo_usuario());
			cs.registerOutParameter(20, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(20);

			log.info("VehiculoConvencional3DAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
