package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCombustibleVehiculo;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoCombustible;

public class CombustibleVehiculo2DAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(CombustibleVehiculo2DAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_comb_vehic_valor = "{call SIGERSOLBL.SGR_SP_INS_COMB_VEHIC_VALOR(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_comb_vehic_valor = "{call SIGERSOLBL.SGR_SP_UPD_COMB_VEHIC_VALOR(?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_comb_vehic_valor = "{call SIGERSOLBL.SGR_SP_LIST_COMB_VEHIC_VALOR(?,?)}";  
	
	public ClsResultado listarCombustibleVehiculo(ClsCombustibleVehiculo clsCombustibleVehiculo){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCombustibleVehiculo> lista = new ArrayList<ClsCombustibleVehiculo>();
		con = null;
		String sql = this.sgr_sp_list_comb_vehic_valor;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCombustibleVehiculo.getRecValorizacion().getRec_valorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsCombustibleVehiculo item = new ClsCombustibleVehiculo();
				ClsTipoCombustible tipoCombustible = new ClsTipoCombustible();
				
				try {
					tipoCombustible.setTipo_combustible_id(rs.getInt("TIPO_COMBUSTIBLE_ID"));
					tipoCombustible.setNombre_tipo(rs.getString("NOMBRE_TIPO"));
					tipoCombustible.setMedida(rs.getString("MEDIDA"));
					item.setTipo_combustible(tipoCombustible);
					if(clsCombustibleVehiculo.getRecValorizacion().getRec_valorizacion_id() == -1){
						item.setConsumo(null);
						item.setCosto(null);
					} else{
						item.setConsumo(rs.getInt("CONSUMO"));
						item.setCosto(rs.getInt("COSTO"));
					}
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCombustibleVehiculo clsCombustibleVehiculo) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_comb_vehic_valor;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCombustibleVehiculo.getRecValorizacion().getRec_valorizacion_id());
			cs.setInt(2, clsCombustibleVehiculo.getTipo_combustible().getTipo_combustible_id());
			cs.setInt(3, clsCombustibleVehiculo.getConsumo());
			cs.setInt(4, clsCombustibleVehiculo.getCosto());
			cs.setInt(5, clsCombustibleVehiculo.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("CombustibleVehiculoDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsCombustibleVehiculo clsCombustibleVehiculo) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_comb_vehic_valor;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCombustibleVehiculo.getRecValorizacion().getRec_valorizacion_id());
			cs.setInt(2, clsCombustibleVehiculo.getTipo_combustible().getTipo_combustible_id());
			cs.setInt(3, clsCombustibleVehiculo.getConsumo());
			cs.setInt(4, clsCombustibleVehiculo.getCosto());
			cs.setInt(5, clsCombustibleVehiculo.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("CombustibleVehiculoDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
