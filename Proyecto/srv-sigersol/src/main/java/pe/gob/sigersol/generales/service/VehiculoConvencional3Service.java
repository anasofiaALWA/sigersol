package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoConvencional;
import pe.gob.sigersol.entities.dao.VehiculoConvencional3DAO;

public class VehiculoConvencional3Service {

	private static Logger log = Logger.getLogger(VehiculoConvencional2Service.class);
	VehiculoConvencional3DAO vehiculoConvencional3DAO = new VehiculoConvencional3DAO();
	
	public ClsResultado listarVehiculoConvencional(ClsVehiculoConvencional clsVehiculoConvencional){
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional3DAO.listarVehiculoConvencional(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoConvencional clsVehiculoConvencional) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional3DAO.insertar(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsVehiculoConvencional clsVehiculoConvencional) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoConvencional3DAO.actualizar(clsVehiculoConvencional);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
