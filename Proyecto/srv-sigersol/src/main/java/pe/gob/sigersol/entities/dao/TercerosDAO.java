package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTerceros;
import pe.gob.sigersol.entities.ClsTransferencia;

public class TercerosDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(TercerosDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_terceros = "{call SIGERSOLBL.SGR_SP_LIST_SGR_TERCEROS(?,?)}";
	private final String sgr_sp_ins_terceros = "{call SIGERSOLBL.SGR_SP_INS_TERCEROS(?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_terceros = "{call SIGERSOLBL.SGR_SP_UPD_TERCEROS(?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsTerceros clsTerceros) {
		ClsResultado clsResultado = new ClsResultado();
		ClsTerceros item = new ClsTerceros();
		ClsMunicipalidad municipalidad = new ClsMunicipalidad();
		con = null;
		String sql = this.sgr_sp_list_sgr_terceros;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsTerceros.getMunicipalidad().getMunicipalidad_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setTerceros_id(rs.getInt("TERCEROS_ID"));
					//municipalidad.setMunicipalidad_id(rs.getInt("MUNICIPALIDAD_ID"));
					//item.setMunicipalidad(municipalidad);
					item.setNum_registro(rs.getString("NUM_REGISTRO"));
					item.setFecha_inicio_vigencia(rs.getString("NUM_REGISTRO"));
					item.setFecha_fin_vigencia(rs.getString("NUM_REGISTRO"));
					item.setNombre_archivo(rs.getString("NOMBRE_ARCHIVO"));
					item.setNombre_archivo_ref(rs.getString("NOMBRE_ARCHIVO_REF"));
					item.setUid_alfresco(rs.getString("UID_ALFRESCO"));
								
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsTerceros clsTerceros) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_terceros;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsTerceros.getMunicipalidad().getMunicipalidad_id());
			cs.setString(2, clsTerceros.getNum_registro());
			cs.setString(3, clsTerceros.getFecha_inicio_vigencia());
			cs.setString(4, clsTerceros.getFecha_fin_vigencia());
			cs.setString(5, clsTerceros.getRazon_social());
			cs.setString(6, clsTerceros.getDireccion());
			cs.setString(7, clsTerceros.getNombre_archivo());
			cs.setString(8, clsTerceros.getNombre_archivo_ref());
			cs.setString(9, clsTerceros.getUid_alfresco());
			cs.setInt(10, clsTerceros.getCodigo_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);
			
			log.info("TransferenciaDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsTerceros clsTerceros) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_terceros;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsTerceros.getTerceros_id());
			cs.setInt(2, clsTerceros.getMunicipalidad().getMunicipalidad_id());
			cs.setString(3, clsTerceros.getNum_registro());
			cs.setString(4, clsTerceros.getFecha_inicio_vigencia());
			cs.setString(5, clsTerceros.getFecha_fin_vigencia());
			cs.setString(6, clsTerceros.getRazon_social());
			cs.setString(7, clsTerceros.getDireccion());
			cs.setString(8, clsTerceros.getNombre_archivo());
			cs.setString(9, clsTerceros.getNombre_archivo_ref());
			cs.setString(10, clsTerceros.getUid_alfresco());
			cs.setInt(11, clsTerceros.getCodigo_usuario());
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(12);

			log.info("TransferenciaDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}	
}
