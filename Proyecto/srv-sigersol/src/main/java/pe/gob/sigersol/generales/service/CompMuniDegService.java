package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCompMuniDeg;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CompMuniDegDAO;

public class CompMuniDegService {

	private static Logger log = Logger.getLogger(CompMuniDegService.class);
	CompMuniDegDAO compMuniDegDAO = new CompMuniDegDAO();
	
	public ClsResultado listarCompMuni(ClsCompMuniDeg clsCompMuniDeg){
		ClsResultado clsResultado = null;
		try {
			clsResultado = compMuniDegDAO.listarCompMuni(clsCompMuniDeg);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCompMuniDeg clsCompMuniDeg) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = compMuniDegDAO.insertar(clsCompMuniDeg);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCompMuniDeg clsCompMuniDeg) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = compMuniDegDAO.actualizar(clsCompMuniDeg);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
