package pe.gob.sigersol.entities;

public class ClsInfoCeldas {

	private Integer info_celdas_id;
	private ClsDisposicionFinalAdm disposicion_final_adm;
	private ClsCeldas celdas;
	private Integer celdas_culminadas;
	private Integer celdas_activas;
	private Integer codigo_usuario;
	
	public Integer getInfo_celdas_id() {
		return info_celdas_id;
	}
	public void setInfo_celdas_id(Integer info_celdas_id) {
		this.info_celdas_id = info_celdas_id;
	}
	public ClsDisposicionFinalAdm getDisposicion_final_adm() {
		return disposicion_final_adm;
	}
	public void setDisposicion_final_adm(ClsDisposicionFinalAdm disposicion_final_adm) {
		this.disposicion_final_adm = disposicion_final_adm;
	}
	public ClsCeldas getCeldas() {
		return celdas;
	}
	public void setCeldas(ClsCeldas celdas) {
		this.celdas = celdas;
	}
	public Integer getCeldas_culminadas() {
		return celdas_culminadas;
	}
	public void setCeldas_culminadas(Integer celdas_culminadas) {
		this.celdas_culminadas = celdas_culminadas;
	}
	public Integer getCeldas_activas() {
		return celdas_activas;
	}
	public void setCeldas_activas(Integer celdas_activas) {
		this.celdas_activas = celdas_activas;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
