package pe.gob.sigersol.entities;

import com.google.gson.Gson;

/**
 * 
 * @author Ingrid Mendoza Clase destinada para seteo de campos comunes de filtro
 *         en tablas
 *
 */
public class ClsParametro {
	private String criterio;
	private String operador;
	private String argumento;
	private String argumento2;
	private Integer codigo_estado;
	private Integer grupo_admin_id;
	private Integer arma_id;
	private String gruposAdminId;
	private String login;
	private String nombre;

	private String nucleo_id;
	private String gu_id;

	// Unidad
	private String unidad_origen_id;
	private String unidad_destino_id;
	private String numero_admin;
	private String grado_ref;
	private String est_civil;
	private String cip;
	
	private String unidad_id;
	private String situacion_id;
	
	private String estados;

	
	public String getUnidad_id() {
		return unidad_id;
	}

	public void setUnidad_id(String unidad_id) {
		this.unidad_id = unidad_id;
	}

	public String getSituacion_id() {
		return situacion_id;
	}

	public void setSituacion_id(String situacion_id) {
		this.situacion_id = situacion_id;
	}

	public String getCip() {
		return cip;
	}

	public void setCip(String cip) {
		this.cip = cip;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getGruposAdminId() {
		return gruposAdminId;
	}

	public void setGruposAdminId(String gruposAdminId) {
		this.gruposAdminId = gruposAdminId;
	}

	public Integer getGrupo_admin_id() {
		return grupo_admin_id;
	}

	public void setGrupo_admin_id(Integer grupo_admin_id) {
		this.grupo_admin_id = grupo_admin_id;
	}

	public Integer getArma_id() {
		return arma_id;
	}

	public void setArma_id(Integer arma_id) {
		this.arma_id = arma_id;
	}

	public String getCriterio() {
		return criterio;
	}

	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public String getArgumento() {
		return argumento;
	}

	public void setArgumento(String argumento) {
		this.argumento = argumento;
	}

	public Integer getCodigo_estado() {
		return codigo_estado;
	}

	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}

	public String getArgumento2() {
		return argumento2;
	}

	public void setArgumento2(String argumento2) {
		this.argumento2 = argumento2;
	}

	public String getUnidad_origen_id() {
		return unidad_origen_id;
	}

	public void setUnidad_origen_id(String unidad_origen_id) {
		this.unidad_origen_id = unidad_origen_id;
	}

	public String getUnidad_destino_id() {
		return unidad_destino_id;
	}

	public void setUnidad_destino_id(String unidad_destino_id) {
		this.unidad_destino_id = unidad_destino_id;
	}

	public String getNumero_admin() {
		return numero_admin;
	}

	public void setNumero_admin(String numero_admin) {
		this.numero_admin = numero_admin;
	}

	public String getGrado_ref() {
		return grado_ref;
	}

	public void setGrado_ref(String grado_ref) {
		this.grado_ref = grado_ref;
	}

	public String getEst_civil() {
		return est_civil;
	}

	public void setEst_civil(String est_civil) {
		this.est_civil = est_civil;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNucleo_id() {
		return nucleo_id;
	}

	public void setNucleo_id(String nucleo_id) {
		this.nucleo_id = nucleo_id;
	}

	public String getGu_id() {
		return gu_id;
	}

	public void setGu_id(String gu_id) {
		this.gu_id = gu_id;
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	public String getEstados() {
		return estados;
	}

	public void setEstados(String estados) {
		this.estados = estados;
	}
}
