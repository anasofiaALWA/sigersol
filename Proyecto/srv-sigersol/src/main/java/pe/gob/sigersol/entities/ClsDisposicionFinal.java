package pe.gob.sigersol.entities;

public class ClsDisposicionFinal {

	private Integer disposicion_final_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private Integer flag_df;
	private Integer selec_relleno;
	private Integer relleno;
	private String resolucion;
	private String autoridad;
	private String fecha_emision;
	private String inicio_permiso;
	private String culminacion_permiso;
	private Integer flag_df2;
	private Integer selec_degradada;
	private Integer degradada;
	private Integer costo;
	private Integer cantidad_colaboradores;
	private Integer costo_degradadas;
	private Integer codigo_usuario;
	private ClsSitioDisposicion sitioDisposicion;
	
	public Integer getDisposicion_final_id() {
		return disposicion_final_id;
	}
	public void setDisposicion_final_id(Integer disposicion_final_id) {
		this.disposicion_final_id = disposicion_final_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public Integer getFlag_df() {
		return flag_df;
	}
	public void setFlag_df(Integer flag_df) {
		this.flag_df = flag_df;
	}
	public Integer getSelec_relleno() {
		return selec_relleno;
	}
	public void setSelec_relleno(Integer selec_relleno) {
		this.selec_relleno = selec_relleno;
	}
	public Integer getRelleno() {
		return relleno;
	}
	public void setRelleno(Integer relleno) {
		this.relleno = relleno;
	}
	public String getResolucion() {
		return resolucion;
	}
	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}
	public String getAutoridad() {
		return autoridad;
	}
	public void setAutoridad(String autoridad) {
		this.autoridad = autoridad;
	}
	public String getFecha_emision() {
		return fecha_emision;
	}
	public void setFecha_emision(String fecha_emision) {
		this.fecha_emision = fecha_emision;
	}
	public String getInicio_permiso() {
		return inicio_permiso;
	}
	public void setInicio_permiso(String inicio_permiso) {
		this.inicio_permiso = inicio_permiso;
	}
	public String getCulminacion_permiso() {
		return culminacion_permiso;
	}
	public void setCulminacion_permiso(String culminacion_permiso) {
		this.culminacion_permiso = culminacion_permiso;
	}
	public Integer getFlag_df2() {
		return flag_df2;
	}
	public void setFlag_df2(Integer flag_df2) {
		this.flag_df2 = flag_df2;
	}
	public Integer getSelec_degradada() {
		return selec_degradada;
	}
	public void setSelec_degradada(Integer selec_degradada) {
		this.selec_degradada = selec_degradada;
	}
	public Integer getDegradada() {
		return degradada;
	}
	public void setDegradada(Integer degradada) {
		this.degradada = degradada;
	}
	public Integer getCosto() {
		return costo;
	}
	public void setCosto(Integer costo) {
		this.costo = costo;
	}
	public Integer getCantidad_colaboradores() {
		return cantidad_colaboradores;
	}
	public void setCantidad_colaboradores(Integer cantidad_colaboradores) {
		this.cantidad_colaboradores = cantidad_colaboradores;
	}
	public Integer getCosto_degradadas() {
		return costo_degradadas;
	}
	public void setCosto_degradadas(Integer costo_degradadas) {
		this.costo_degradadas = costo_degradadas;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	public ClsSitioDisposicion getSitioDisposicion() {
		return sitioDisposicion;
	}
	public void setSitioDisposicion(ClsSitioDisposicion sitioDisposicion) {
		this.sitioDisposicion = sitioDisposicion;
	}
	
	
}
