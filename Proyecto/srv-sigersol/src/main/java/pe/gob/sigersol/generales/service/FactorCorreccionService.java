package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.FactorCorreccionDAO;


public class FactorCorreccionService {
	private static Logger log = Logger.getLogger(VariableService.class);
	FactorCorreccionDAO dao = new FactorCorreccionDAO();
	
	public ClsResultado listar(Integer disposicionFinal_id) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.listarFactor(disposicionFinal_id);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}

		return clsResultado;

	}

}
