package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCCGenCombustible;
import pe.gob.sigersol.entities.ClsResultado;

public class CCGenCombustibleDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(CCGenCombustibleDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_cc_sel_combustible_act = "{call SIGERSOLBL.SGR_SP_CC_SEL_COMBUSTIBLE_ACT(?,?)}";
	private final String sgr_sp_ins_cc_cumbustible_act = "{ call SIGERSOLBL.SGR_SP_INS_CC_GEN_COMBUSTIBLE (?,?,?,?,?)}";
	private final String sgr_sp_upd_cc_cumbustible_act = "{ call SIGERSOLBL.SGR_SP_UPD_CC_GEN_COMBUSTIBLE (?,?,?,?)}";

	public ClsResultado listarGenCombustible(Integer ccGenActividadId) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCCGenCombustible> lista = new ArrayList<ClsCCGenCombustible>();
		con = null;
		String sql = this.sgr_sp_cc_sel_combustible_act;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);

			cs.setInt(1, ccGenActividadId);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsCCGenCombustible item = new ClsCCGenCombustible();
				try {
					item.setCcGenCombustibleId(rs.getInt("CC_COMBUSTIBLE_ACT_ID"));
				} catch (Exception e) {
					item.setCcGenCombustibleId(-1);
				}
				try {
					item.setCc_tipoCombustibleId(rs.getInt("CC_TIPO_COMBUSTIBLE_ID"));
				} catch (Exception e) {

				}
				try {
					item.setCc_tipoCombustible_desc(rs.getString("DESCRIPCION"));
				} catch (Exception e) {

				}
				try {
					item.setCantidad(rs.getDouble("CANTIDAD"));
				} catch (Exception e) {
					item.setCantidad(0.0);
				}

				lista.add(item);
			}
			clsResultado.setExito(true);
			clsResultado.setObjeto(lista);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 listarGenCombustible: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listarGenCombustible: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listarGenCombustible: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listarGenCombustible: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;

	}

	public ClsResultado insertar(Integer ccGenActividadId, ClsCCGenCombustible ccGenCombustible) {
		ClsResultado clsResultado = new ClsResultado();
		Integer ccCombustActId;
		con = null;
		String sql = this.sgr_sp_ins_cc_cumbustible_act;

		try {
			con = getConnection();
			con.setAutoCommit(true);
			cs = con.prepareCall(sql);

			cs.setInt(1, ccGenActividadId);
			cs.setInt(2, ccGenCombustible.getCc_tipoCombustibleId());
			cs.setDouble(3, ccGenCombustible.getCantidad());
			cs.setInt(4, ccGenCombustible.getSesionId());
			cs.registerOutParameter(5, OracleTypes.INTEGER);

			cs.executeUpdate();
			ccCombustActId = cs.getInt(5);
			log.info("CCGenCombustibleDAO >> insertar() >> resultado :" + ccCombustActId);
			clsResultado.setObjeto(ccCombustActId);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCCGenCombustible ccGenCombustible) {
		ClsResultado clsResultado = new ClsResultado();
		Integer ccCombustActId;
		con = null;
		String sql = this.sgr_sp_upd_cc_cumbustible_act;

		try {
			con = getConnection();
			con.setAutoCommit(true);
			cs = con.prepareCall(sql);

		
			cs.setInt(1, ccGenCombustible.getCcGenCombustibleId());
			cs.setDouble(2, ccGenCombustible.getCantidad());			
			cs.setInt(3, ccGenCombustible.getSesionId());
			cs.registerOutParameter(4, OracleTypes.INTEGER);

			cs.executeUpdate();
			ccCombustActId = cs.getInt(4);
			log.info("CCGenCombustibleDAO >> actualizar() >> resultado :" + ccCombustActId);
			clsResultado.setObjeto(ccCombustActId);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
