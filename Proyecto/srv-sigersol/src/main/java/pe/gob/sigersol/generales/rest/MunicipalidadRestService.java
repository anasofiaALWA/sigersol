package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.MunicipalidadService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/municipalidad")
@RequestScoped
public class MunicipalidadRestService {
	
	private static Logger log = Logger.getLogger(MunicipalidadRestService.class.getName());
	private ClsMunicipalidad clsMunicipalidad;
	private MunicipalidadService municipalidadService;

	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(@Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**Obtener Municipalidad**");
			
			clsResultado = municipalidadService.obtener();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"municipalidad\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		municipalidadService = new MunicipalidadService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaMunicipalidad(paramJson);

			// if (accion == 1) {
			// clsResultado = academicoService.insertar(clsAcademico);
			// } else {
			// clsResultado = academicoService.actualizar(clsAcademico);
			// }
			//
			
			clsResultado = municipalidadService.insertar(clsMunicipalidad);
			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(@FormParam("paramJson") String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaMunicipalidad(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = municipalidadService.actualizar(clsMunicipalidad);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	
	private void jsonEntradaMunicipalidad(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectMunicipalidad;
		JSONParser jsonParser = new JSONParser();
		
		clsMunicipalidad = new ClsMunicipalidad();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectMunicipalidad = (JSONObject) jsonObjectPrincipal.get("Municipalidad");

			if (jsonObjectMunicipalidad.get("cod_departamento") != null && jsonObjectMunicipalidad.get("cod_departamento") != "") {
				clsMunicipalidad.setCodigo_ubigeo_departamento(jsonObjectMunicipalidad.get("cod_departamento").toString());
			}
			if (jsonObjectMunicipalidad.get("cod_provincia") != null && jsonObjectMunicipalidad.get("cod_provincia") != "") {
				clsMunicipalidad.setCodigo_ubigeo_provincia(jsonObjectMunicipalidad.get("cod_provincia").toString());
			}
			if (jsonObjectMunicipalidad.get("cod_distrito") != null && jsonObjectMunicipalidad.get("cod_distrito") != "") {
				clsMunicipalidad.setCodigo_ubigeo_distrito(jsonObjectMunicipalidad.get("cod_distrito").toString());
			}
			if (jsonObjectMunicipalidad.get("ruc") != null && jsonObjectMunicipalidad.get("ruc") != "") {
				clsMunicipalidad.setRuc(jsonObjectMunicipalidad.get("ruc").toString());
			}
			if (jsonObjectMunicipalidad.get("nombreAlcalde") != null && jsonObjectMunicipalidad.get("nombreAlcalde") != "") {
				clsMunicipalidad.setNombre_alcalde(jsonObjectMunicipalidad.get("nombreAlcalde").toString());
			}
			if (jsonObjectMunicipalidad.get("respLimpieza") != null && jsonObjectMunicipalidad.get("respLimpieza") != "") {
				clsMunicipalidad.setResponsable_limpieza(jsonObjectMunicipalidad.get("respLimpieza").toString());
			}
			if (jsonObjectMunicipalidad.get("ruc") != null && jsonObjectMunicipalidad.get("ruc") != "") {
				clsMunicipalidad.setRuc(jsonObjectMunicipalidad.get("ruc").toString());
			}			
			if (jsonObjectMunicipalidad.get("direccion") != null && jsonObjectMunicipalidad.get("direccion") != "") {
				clsMunicipalidad.setDireccion(jsonObjectMunicipalidad.get("direccion").toString());
			}
			if (jsonObjectMunicipalidad.get("telefono") != null && jsonObjectMunicipalidad.get("telefono") != "") {
				clsMunicipalidad.setTelefono(jsonObjectMunicipalidad.get("telefono").toString());
			}
			if (jsonObjectMunicipalidad.get("fax") != null && jsonObjectMunicipalidad.get("fax") != "") {
				clsMunicipalidad.setFax(jsonObjectMunicipalidad.get("fax").toString());
			}
			if (jsonObjectMunicipalidad.get("nombreAlcalde") != null && jsonObjectMunicipalidad.get("nombreAlcalde") != "") {
				clsMunicipalidad.setNombre_alcalde(jsonObjectMunicipalidad.get("nombreAlcalde").toString());
			}
			if (jsonObjectMunicipalidad.get("clasifMunicipalidad") != null && jsonObjectMunicipalidad.get("clasifMunicipalidad") != "") {
				clsMunicipalidad.setClasificacion_municipalidad(Integer.parseInt(jsonObjectMunicipalidad.get("clasifMunicipalidad").toString()));
			}
			if (jsonObjectMunicipalidad.get("tipoMunicipalidad") != null && jsonObjectMunicipalidad.get("tipoMunicipalidad") != "") {
				clsMunicipalidad.setTipo_municipalidad(Integer.parseInt(jsonObjectMunicipalidad.get("tipoMunicipalidad").toString()));
			}			
			if (jsonObjectMunicipalidad.get("poblacion_urb") != null && jsonObjectMunicipalidad.get("poblacion_urb") != "") {
				clsMunicipalidad.setPoblacion_urbana(Integer.parseInt(jsonObjectMunicipalidad.get("poblacion_urb").toString()));
			}
			if (jsonObjectMunicipalidad.get("poblacion_urb_fuente") != null && jsonObjectMunicipalidad.get("poblacion_urb_fuente") != "") {
				clsMunicipalidad.setPoblacion_urbana_fuente(jsonObjectMunicipalidad.get("poblacion_urb_fuente").toString());
			}
			if (jsonObjectMunicipalidad.get("poblacion_rural") != null && jsonObjectMunicipalidad.get("poblacion_rural") != "") {
				clsMunicipalidad.setPoblacion_urbana(Integer.parseInt(jsonObjectMunicipalidad.get("poblacion_rural").toString()));
			}
			if (jsonObjectMunicipalidad.get("poblacion_rural_fuente") != null && jsonObjectMunicipalidad.get("poblacion_rural_fuente") != "") {
				clsMunicipalidad.setPoblacion_urbana_fuente(jsonObjectMunicipalidad.get("poblacion_rural_fuente").toString());
			}
			if (jsonObjectMunicipalidad.get("cod_usuario") != null && jsonObjectMunicipalidad.get("cod_usuario") != "") {
				clsMunicipalidad.setCodigo_usuario(Integer.parseInt(jsonObjectMunicipalidad.get("cod_usuario").toString()));
			}
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
