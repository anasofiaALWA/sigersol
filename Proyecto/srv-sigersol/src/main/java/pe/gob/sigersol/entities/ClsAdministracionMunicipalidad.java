package pe.gob.sigersol.entities;

public class ClsAdministracionMunicipalidad {
	
	private Integer administracion_municipalidad_id;
	private Integer municipalidad_id;
	private String numero_ordenanza;
	private Integer periodo_anio;
	private String responsable;
	private String area_gerencia;
	private String archivo_ordenanza;
	private String archivo_ordenanza_referencia;
	private Integer codigo_estado;
	private Integer codigo_usuario;
	
	public Integer getAdministracion_municipalidad_id() {
		return administracion_municipalidad_id;
	}
	public void setAdministracion_municipalidad_id(Integer administracion_municipalidad_id) {
		this.administracion_municipalidad_id = administracion_municipalidad_id;
	}
	public Integer getMunicipalidad_id() {
		return municipalidad_id;
	}
	public void setMunicipalidad_id(Integer municipalidad_id) {
		this.municipalidad_id = municipalidad_id;
	}
	public String getNumero_ordenanza() {
		return numero_ordenanza;
	}
	public void setNumero_ordenanza(String numero_ordenanza) {
		this.numero_ordenanza = numero_ordenanza;
	}
	public Integer getPeriodo_anio() {
		return periodo_anio;
	}
	public void setPeriodo_anio(Integer periodo_anio) {
		this.periodo_anio = periodo_anio;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getArea_gerencia() {
		return area_gerencia;
	}
	public void setArea_gerencia(String area_gerencia) {
		this.area_gerencia = area_gerencia;
	}
	public String getArchivo_ordenanza() {
		return archivo_ordenanza;
	}
	public void setArchivo_ordenanza(String archivo_ordenanza) {
		this.archivo_ordenanza = archivo_ordenanza;
	}
	public String getArchivo_ordenanza_referencia() {
		return archivo_ordenanza_referencia;
	}
	public void setArchivo_ordenanza_referencia(String archivo_ordenanza_referencia) {
		this.archivo_ordenanza_referencia = archivo_ordenanza_referencia;
	}
	public Integer getCodigo_estado() {
		return codigo_estado;
	}
	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
