package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsRecValorizacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.RecValorizacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase RecValorizacionRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de recolección permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/recoleccionVal")
@RequestScoped
public class RecValorizacionRestService {
	
	private static Logger log = Logger.getLogger(RecValorizacionRestService.class.getName());
	
	private ClsRecValorizacion clsRecValorizacion;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private RecValorizacionService recValorizacionService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			recValorizacionService = new RecValorizacionService();
			
			jsonEntradaRecValorizacion(paramJson);
			
			log.info("**obtener RecValorizacion");
			
			clsResultado = recValorizacionService.obtener(clsRecValorizacion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"recoleccion_val\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		recValorizacionService = new RecValorizacionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaRecValorizacion(paramJson);
			clsResultado = recValorizacionService.insertar(clsRecValorizacion);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		recValorizacionService = new RecValorizacionService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaRecValorizacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = recValorizacionService.actualizar(clsRecValorizacion);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de RecValorizacion
	 * 
	 * */
	private void jsonEntradaRecValorizacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> RecValorizacionRestService :jsonEntradaRecValorizacion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectRecValorizacion;
		JSONParser jsonParser = new JSONParser();
		
		clsRecValorizacion = new ClsRecValorizacion();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectRecValorizacion = (JSONObject) jsonObjectPrincipal.get("RecoleccionVal");
			
			if (jsonObjectRecValorizacion.get("rec_valorizacion_id") != null && jsonObjectRecValorizacion.get("rec_valorizacion_id") != "") {
				clsRecValorizacion.setRec_valorizacion_id(Integer.parseInt(jsonObjectRecValorizacion.get("rec_valorizacion_id").toString()));
			} else {
				clsRecValorizacion.setRec_valorizacion_id(-1);
			}
			
			if (jsonObjectRecValorizacion.get("ciclo_grs_id") != null && jsonObjectRecValorizacion.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectRecValorizacion.get("ciclo_grs_id").toString()));
			}
			
			clsRecValorizacion.setCiclo_grs_id(clsCicloGestionResiduos);
			
			if (jsonObjectRecValorizacion.get("adm_serv_id") != null && jsonObjectRecValorizacion.get("adm_serv_id") != "") {
				clsRecValorizacion.setAdm_serv_id(jsonObjectRecValorizacion.get("adm_serv_id").toString());
			}
			
			if (jsonObjectRecValorizacion.get("costo_anual") != null && jsonObjectRecValorizacion.get("costo_anual") != "") {
				clsRecValorizacion.setCosto_anual(Integer.parseInt(jsonObjectRecValorizacion.get("costo_anual").toString()));
			}
			else
			{
				clsRecValorizacion.setCosto_anual(0);
			}
			
			if (jsonObjectRecValorizacion.get("flag_recoleccion") != null && jsonObjectRecValorizacion.get("flag_recoleccion") != "") {
				clsRecValorizacion.setFlag_recoleccion(Integer.parseInt(jsonObjectRecValorizacion.get("flag_recoleccion").toString()));
			}
			
			if (jsonObjectRecValorizacion.get("fr1") != null && jsonObjectRecValorizacion.get("fr1") != "") {
				clsRecValorizacion.setFr1(Integer.parseInt(jsonObjectRecValorizacion.get("fr1").toString()));
			}
			
			if (jsonObjectRecValorizacion.get("fr2") != null && jsonObjectRecValorizacion.get("fr2") != "") {
				clsRecValorizacion.setFr2(Integer.parseInt(jsonObjectRecValorizacion.get("fr2").toString()));
				
				clsRecValorizacion.setFrecuencia(clsRecValorizacion.getFr1().toString().concat("/").concat(clsRecValorizacion.getFr2().toString()));
			}
			
			if (jsonObjectRecValorizacion.get("cantidad_residuos_organicos") != null && jsonObjectRecValorizacion.get("cantidad_residuos_organicos") != "") {
				clsRecValorizacion.setCantidad_residuos_organicos(Integer.parseInt(jsonObjectRecValorizacion.get("cantidad_residuos_organicos").toString()));
			}
			else{
				clsRecValorizacion.setCantidad_residuos_organicos(0);
			}
			
			if (jsonObjectRecValorizacion.get("cantidad_residuos_inorganicos") != null && jsonObjectRecValorizacion.get("cantidad_residuos_inorganicos") != "") {
				clsRecValorizacion.setCantidad_residuos_inorganicos(Integer.parseInt(jsonObjectRecValorizacion.get("cantidad_residuos_inorganicos").toString()));
			}
			else{
				clsRecValorizacion.setCantidad_residuos_inorganicos(0);
			}
			
			if (jsonObjectRecValorizacion.get("domiciliarios") != null && jsonObjectRecValorizacion.get("domiciliarios") != "") {
				clsRecValorizacion.setDomiciliarios(Integer.parseInt(jsonObjectRecValorizacion.get("domiciliarios").toString()));
			} 
			else{
				clsRecValorizacion.setDomiciliarios(0);
			}
			
			if (jsonObjectRecValorizacion.get("no_domiciliarios") != null && jsonObjectRecValorizacion.get("no_domiciliarios") != "") {
				clsRecValorizacion.setNo_domiciliarios(Integer.parseInt(jsonObjectRecValorizacion.get("no_domiciliarios").toString()));
			} 
			else{
				clsRecValorizacion.setNo_domiciliarios(0);
			}
			
			if (jsonObjectRecValorizacion.get("otros_descripcion") != null && jsonObjectRecValorizacion.get("otros_descripcion") != "") {
				clsRecValorizacion.setOtros_descripcion(jsonObjectRecValorizacion.get("otros_descripcion").toString());
			} 
			else{
				clsRecValorizacion.setOtros_descripcion("");
			}
			
			if (jsonObjectRecValorizacion.get("otros_antig_promedio") != null && jsonObjectRecValorizacion.get("otros_antig_promedio") != "") {
				clsRecValorizacion.setOtros_antig_promedio(Integer.parseInt(jsonObjectRecValorizacion.get("otros_antig_promedio").toString()));
			} 
			else{
				clsRecValorizacion.setOtros_antig_promedio(0);
			}
			
			if (jsonObjectRecValorizacion.get("otros_capacidad") != null && jsonObjectRecValorizacion.get("otros_capacidad") != "") {
				clsRecValorizacion.setOtros_capacidad(Integer.parseInt(jsonObjectRecValorizacion.get("otros_capacidad").toString()));
			} 
			else{
				clsRecValorizacion.setOtros_capacidad(0);
			}
			
			if (jsonObjectRecValorizacion.get("otros_recoleccion_anual") != null && jsonObjectRecValorizacion.get("otros_recoleccion_anual") != "") {
				clsRecValorizacion.setOtros_recoleccion_anual(Integer.parseInt(jsonObjectRecValorizacion.get("otros_recoleccion_anual").toString()));
			} 
			else{
				clsRecValorizacion.setOtros_recoleccion_anual(0);
			}
			
			if (jsonObjectRecValorizacion.get("otros_numero_unidades") != null && jsonObjectRecValorizacion.get("otros_numero_unidades") != "") {
				clsRecValorizacion.setOtros_numero_unidades(Integer.parseInt(jsonObjectRecValorizacion.get("otros_numero_unidades").toString()));
			} 
			else{
				clsRecValorizacion.setOtros_numero_unidades(0);
			}
		   
			if (jsonObjectRecValorizacion.get("cod_usuario") != null && jsonObjectRecValorizacion.get("cod_usuario") != "") {
				clsRecValorizacion.setCodigo_usuario(Integer.parseInt(jsonObjectRecValorizacion.get("cod_usuario").toString()));
			}
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
