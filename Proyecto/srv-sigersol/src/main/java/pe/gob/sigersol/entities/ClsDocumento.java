package pe.gob.sigersol.entities;

public class ClsDocumento {

	private Integer documento_id;
	private String uid_documento;
	private String nombre_archivo;
	private String numero_documento;
	private String tipo_documento;
	private String razon_social;
	private String nombre_infraestructura;
	private String nombre_autoridad;
	private String fecha_emision;
	private String fecha_caducidad;
	private String inicio_permiso;
	private String culminacion_permiso;
	private String numero_licencia;
	private String numero_expediente;
    private Integer codigo_usuario;
	
    public Integer getDocumento_id() {
		return documento_id;
	}
	public void setDocumento_id(Integer documento_id) {
		this.documento_id = documento_id;
	}
	public String getUid_documento() {
		return uid_documento;
	}
	public void setUid_documento(String uid_documento) {
		this.uid_documento = uid_documento;
	}
	public String getNombre_archivo() {
		return nombre_archivo;
	}
	public void setNombre_archivo(String nombre_archivo) {
		this.nombre_archivo = nombre_archivo;
	}
	public String getNumero_documento() {
		return numero_documento;
	}
	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}
	public String getTipo_documento() {
		return tipo_documento;
	}
	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}
	public String getRazon_social() {
		return razon_social;
	}
	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}
	public String getNombre_infraestructura() {
		return nombre_infraestructura;
	}
	public void setNombre_infraestructura(String nombre_infraestructura) {
		this.nombre_infraestructura = nombre_infraestructura;
	}
	public String getNombre_autoridad() {
		return nombre_autoridad;
	}
	public void setNombre_autoridad(String nombre_autoridad) {
		this.nombre_autoridad = nombre_autoridad;
	}
	public String getFecha_emision() {
		return fecha_emision;
	}
	public void setFecha_emision(String fecha_emision) {
		this.fecha_emision = fecha_emision;
	}
	public String getFecha_caducidad() {
		return fecha_caducidad;
	}
	public void setFecha_caducidad(String fecha_caducidad) {
		this.fecha_caducidad = fecha_caducidad;
	}
	public String getInicio_permiso() {
		return inicio_permiso;
	}
	public void setInicio_permiso(String inicio_permiso) {
		this.inicio_permiso = inicio_permiso;
	}
	public String getCulminacion_permiso() {
		return culminacion_permiso;
	}
	public void setCulminacion_permiso(String culminacion_permiso) {
		this.culminacion_permiso = culminacion_permiso;
	}
	public String getNumero_licencia() {
		return numero_licencia;
	}
	public void setNumero_licencia(String numero_licencia) {
		this.numero_licencia = numero_licencia;
	}
	public String getNumero_expediente() {
		return numero_expediente;
	}
	public void setNumero_expediente(String numero_expediente) {
		this.numero_expediente = numero_expediente;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
    
}
