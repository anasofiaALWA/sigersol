package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsEquipoBarrido;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUniformeEquipo;
import pe.gob.sigersol.generales.service.EquipoBarridoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/equipoX")
@RequestScoped
public class EquipoBarridoRestService {
	
	private static Logger log = Logger.getLogger(OfertaRestService.class.getName());
	private ClsEquipoBarrido clsEquipoBarrido;
	private ClsUniformeEquipo clsUniformeEquipo;
	private ClsBarrido clsBarrido;
	private EquipoBarridoService equipoBarridoService;
	private List<ClsEquipoBarrido> listaEquipoBarrido;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarEquipo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarEquipo(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			equipoBarridoService = new EquipoBarridoService();
			
			jsonEntradaListaEquipo(paramJson);
			
			log.info("**Obtener Equipo de Barrido");
			
			clsResultado = equipoBarridoService.listarEquipo(clsEquipoBarrido);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstEquipo\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		equipoBarridoService = new EquipoBarridoService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaEquipo(paramJson);

			for (ClsEquipoBarrido clsEquipoBarrido : listaEquipoBarrido) {
				clsResultado = equipoBarridoService.insertar(clsEquipoBarrido);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		equipoBarridoService = new EquipoBarridoService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaEquipo(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsEquipoBarrido clsEquipoBarrido : listaEquipoBarrido) {
			clsResultado = equipoBarridoService.actualizar(clsEquipoBarrido);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Oferta
	 * 
	 * */
	private void jsonEntradaEquipo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> EquipoBarridoRestService : jsonEntradaEquipo" + jsonEntrada);

		JSONObject jsonObjectTipoOferta;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaEquipoBarrido = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsEquipoBarrido = new ClsEquipoBarrido();
			clsUniformeEquipo = new ClsUniformeEquipo ();
			clsBarrido = new ClsBarrido();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("barrido_id") != null && innerObject.get("barrido_id").toString() != "") {
				clsBarrido.setBarrido_id(Integer.parseInt(innerObject.get("barrido_id").toString()));
			}
			
			clsEquipoBarrido.setBarrido(clsBarrido);
			
			if (innerObject.get("uniforme") != null && innerObject.get("uniforme").toString() != "") {
				jsonObjectTipoOferta  = (JSONObject) innerObject.get("uniforme");
				clsUniformeEquipo.setUniforme_equipo_id(Integer.parseInt(jsonObjectTipoOferta.get("uniforme_equipo_id").toString()));
			}
			clsEquipoBarrido.setUniforme(clsUniformeEquipo);
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsEquipoBarrido.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else {
				clsEquipoBarrido.setCantidad(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsEquipoBarrido.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaEquipoBarrido.add(clsEquipoBarrido);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
		
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaEquipo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> EquipoBarridoRestService :	 jsonEntradaListaEquipo" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectEquipo;
		JSONParser jsonParser = new JSONParser();
		
		clsEquipoBarrido = new ClsEquipoBarrido();
		clsUniformeEquipo = new ClsUniformeEquipo ();
		clsBarrido = new ClsBarrido();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectEquipo = (JSONObject) jsonObjectPrincipal.get("Equipo");
			
			if (jsonObjectEquipo.get("barrido_id") != null && jsonObjectEquipo.get("barrido_id") != "" &&
					jsonObjectEquipo.get("barrido_id") != " ") {
				clsBarrido.setBarrido_id(Integer.parseInt(jsonObjectEquipo.get("barrido_id").toString()));
			}
			else
				clsBarrido.setBarrido_id(-1);
			
			clsEquipoBarrido.setBarrido(clsBarrido);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
