package pe.gob.sigersol.entities;

public class ClsCobertura {

	private Integer cobertura_id;
	private String tipo_cobertura;
	
	public Integer getCobertura_id() {
		return cobertura_id;
	}
	public void setCobertura_id(Integer cobertura_id) {
		this.cobertura_id = cobertura_id;
	}
	public String getTipo_cobertura() {
		return tipo_cobertura;
	}
	public void setTipo_cobertura(String tipo_cobertura) {
		this.tipo_cobertura = tipo_cobertura;
	}
	
}
