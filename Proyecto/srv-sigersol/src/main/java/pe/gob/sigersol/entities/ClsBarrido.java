package pe.gob.sigersol.entities;

public class ClsBarrido {
	
	private Integer barrido_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private Integer adm_serv_id;
	private Integer costo_anual;
	private Integer via_asfaltada;
	private Integer via_no_asfaltada;
	private Integer plazas;
	private Integer playas;
	private Integer demanda_barrido;
	private String otros_barridos;
	private Integer otros_demanda;
	private Integer fr1;
	private Integer fr2;
	private String frecuencia;
	private Integer oferta;
	private Integer hombre_25;
	private Integer mujer_25;
	private Integer hombre_25_65;
	private Integer mujer_25_65;  
	private Integer hombre_65;
	private Integer mujer_65;
	private Integer cr_hombre_25;
	private Integer cr_hombre_25_65;
	private Integer cr_hombre_65;
	private Integer cpr_hombre_25;
	private Integer cpr_hombre_25_65;
	private Integer cpr_hombre_65;
	private Integer cr_mujer_25;
	private Integer cr_mujer_25_65;
	private Integer cr_mujer_65;
	private Integer cpr_mujer_25;
	private Integer cpr_mujer_25_65;
	private Integer cpr_mujer_65;
	private Integer cr_25;
	private Integer cr_25_65;
	private Integer cr_65;
	private Integer cpr_25;
	private Integer cpr_25_65;
	private Integer cpr_65;
	private Double total_recolectado;
	private String descripcion_otros;
	private Integer codigo_usuario;
	
	public Integer getBarrido_id() {
		return barrido_id;
	}
	public void setBarrido_id(Integer barrido_id) {
		this.barrido_id = barrido_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public Integer getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(Integer adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer coste_anual) {
		this.costo_anual = coste_anual;
	}
	public Integer getVia_asfaltada() {
		return via_asfaltada;
	}
	public void setVia_asfaltada(Integer via_asfaltada) {
		this.via_asfaltada = via_asfaltada;
	}
	public Integer getVia_no_asfaltada() {
		return via_no_asfaltada;
	}
	public void setVia_no_asfaltada(Integer via_no_asfaltada) {
		this.via_no_asfaltada = via_no_asfaltada;
	}
	public Integer getPlazas() {
		return plazas;
	}
	public void setPlazas(Integer plazas) {
		this.plazas = plazas;
	}
	public Integer getPlayas() {
		return playas;
	}
	public void setPlayas(Integer playas) {
		this.playas = playas;
	}
	public Integer getDemanda_barrido() {
		return demanda_barrido;
	}
	public void setDemanda_barrido(Integer demanda_barrido) {
		this.demanda_barrido = demanda_barrido;
	}
	public String getOtros_barridos() {
		return otros_barridos;
	}
	public void setOtros_barridos(String otros_barridos) {
		this.otros_barridos = otros_barridos;
	}
	public Integer getOtros_demanda() {
		return otros_demanda;
	}
	public void setOtros_demanda(Integer otros_demanda) {
		this.otros_demanda = otros_demanda;
	}
	public Integer getFr1() {
		return fr1;
	}
	public void setFr1(Integer fr1) {
		this.fr1 = fr1;
	}
	public Integer getFr2() {
		return fr2;
	}
	public void setFr2(Integer fr2) {
		this.fr2 = fr2;
	}
	public String getFrecuencia() {
		return frecuencia;
	}
	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}
	public Integer getOferta() {
		return oferta;
	}
	public void setOferta(Integer oferta) {
		this.oferta = oferta;
	}
	public Integer getHombre_25() {
		return hombre_25;
	}
	public void setHombre_25(Integer hombre_25) {
		this.hombre_25 = hombre_25;
	}
	public Integer getMujer_25() {
		return mujer_25;
	}
	public void setMujer_25(Integer mujer_25) {
		this.mujer_25 = mujer_25;
	}
	public Integer getHombre_25_65() {
		return hombre_25_65;
	}
	public void setHombre_25_65(Integer hombre_25_65) {
		this.hombre_25_65 = hombre_25_65;
	}
	public Integer getMujer_25_65() {
		return mujer_25_65;
	}
	public void setMujer_25_65(Integer mujer_25_65) {
		this.mujer_25_65 = mujer_25_65;
	}
	public Integer getHombre_65() {
		return hombre_65;
	}
	public void setHombre_65(Integer hombre_65) {
		this.hombre_65 = hombre_65;
	}
	public Integer getMujer_65() {
		return mujer_65;
	}
	public void setMujer_65(Integer mujer_65) {
		this.mujer_65 = mujer_65;
	}
	public Integer getCr_hombre_25() {
		return cr_hombre_25;
	}
	public void setCr_hombre_25(Integer cr_hombre_25) {
		this.cr_hombre_25 = cr_hombre_25;
	}
	public Integer getCr_hombre_25_65() {
		return cr_hombre_25_65;
	}
	public void setCr_hombre_25_65(Integer cr_hombre_25_65) {
		this.cr_hombre_25_65 = cr_hombre_25_65;
	}
	public Integer getCr_hombre_65() {
		return cr_hombre_65;
	}
	public void setCr_hombre_65(Integer cr_hombre_65) {
		this.cr_hombre_65 = cr_hombre_65;
	}
	public Integer getCpr_hombre_25() {
		return cpr_hombre_25;
	}
	public void setCpr_hombre_25(Integer cpr_hombre_25) {
		this.cpr_hombre_25 = cpr_hombre_25;
	}
	public Integer getCpr_hombre_25_65() {
		return cpr_hombre_25_65;
	}
	public void setCpr_hombre_25_65(Integer cpr_hombre_25_65) {
		this.cpr_hombre_25_65 = cpr_hombre_25_65;
	}
	public Integer getCpr_hombre_65() {
		return cpr_hombre_65;
	}
	public void setCpr_hombre_65(Integer cpr_hombre_65) {
		this.cpr_hombre_65 = cpr_hombre_65;
	}
	public Integer getCr_mujer_25() {
		return cr_mujer_25;
	}
	public void setCr_mujer_25(Integer cr_mujer_25) {
		this.cr_mujer_25 = cr_mujer_25;
	}
	public Integer getCr_mujer_25_65() {
		return cr_mujer_25_65;
	}
	public void setCr_mujer_25_65(Integer cr_mujer_25_65) {
		this.cr_mujer_25_65 = cr_mujer_25_65;
	}
	public Integer getCr_mujer_65() {
		return cr_mujer_65;
	}
	public void setCr_mujer_65(Integer cr_mujer_65) {
		this.cr_mujer_65 = cr_mujer_65;
	}
	public Integer getCpr_mujer_25() {
		return cpr_mujer_25;
	}
	public void setCpr_mujer_25(Integer cpr_mujer_25) {
		this.cpr_mujer_25 = cpr_mujer_25;
	}
	public Integer getCpr_mujer_25_65() {
		return cpr_mujer_25_65;
	}
	public void setCpr_mujer_25_65(Integer cpr_mujer_25_65) {
		this.cpr_mujer_25_65 = cpr_mujer_25_65;
	}
	public Integer getCpr_mujer_65() {
		return cpr_mujer_65;
	}
	public void setCpr_mujer_65(Integer cpr_mujer_65) {
		this.cpr_mujer_65 = cpr_mujer_65;
	}
	public String getDescripcion_otros() {
		return descripcion_otros;
	}
	public void setDescripcion_otros(String descripcion_otros) {
		this.descripcion_otros = descripcion_otros;
	}
	public Integer getCr_25() {
		return cr_25;
	}
	public void setCr_25(Integer cr_25) {
		this.cr_25 = cr_25;
	}
	public Integer getCr_25_65() {
		return cr_25_65;
	}
	public void setCr_25_65(Integer cr_25_65) {
		this.cr_25_65 = cr_25_65;
	}
	public Integer getCr_65() {
		return cr_65;
	}
	public void setCr_65(Integer cr_65) {
		this.cr_65 = cr_65;
	}
	public Integer getCpr_25() {
		return cpr_25;
	}
	public void setCpr_25(Integer cpr_25) {
		this.cpr_25 = cpr_25;
	}
	public Integer getCpr_25_65() {
		return cpr_25_65;
	}
	public void setCpr_25_65(Integer cpr_25_65) {
		this.cpr_25_65 = cpr_25_65;
	}
	public Integer getCpr_65() {
		return cpr_65;
	}
	public void setCpr_65(Integer cpr_65) {
		this.cpr_65 = cpr_65;
	}
	public Double getTotal_recolectado() {
		return total_recolectado;
	}
	public void setTotal_recolectado(Double total_recolectado) {
		this.total_recolectado = total_recolectado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
