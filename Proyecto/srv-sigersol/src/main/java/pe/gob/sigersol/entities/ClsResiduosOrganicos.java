package pe.gob.sigersol.entities;

public class ClsResiduosOrganicos {

	private Integer residuos_organicos_id;
	private ClsValorizacion valorizacion;
	private ClsTipoProductos tipoProductos;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getResiduos_organicos_id() {
		return residuos_organicos_id;
	}
	public void setResiduos_organicos_id(Integer residuos_organicos_id) {
		this.residuos_organicos_id = residuos_organicos_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public ClsTipoProductos getTipoProductos() {
		return tipoProductos;
	}
	public void setTipoProductos(ClsTipoProductos tipoProductos) {
		this.tipoProductos = tipoProductos;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
