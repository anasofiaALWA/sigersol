package pe.gob.sigersol.parametros.rest;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;

import com.google.gson.Gson;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUnicodeParametro;
import pe.gob.sigersol.parametros.service.UnicodeParametrosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/parametrosX")
@RequestScoped
public class UnicodeParametroRestService {
	private static final Logger log = Logger.getLogger(UnicodeParametroRestService.class);
	UnicodeParametrosService unicodeparametros_service = new UnicodeParametrosService();
	Gson gson = new Gson();

	@POST
	@Path("/obtener_unicodeparametros")
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener_unicodeparametros(@FormParam("nombre_tabla") String nombre_tabla,
			@FormParam("accion") int accion, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		String json_unicodeparametros = "";
		try {
			if (accion == 1) { // get tipoarchivo

			} else if (accion == 2) { // get list tipoarchivo
				//Borrar//
				//json_unicodeparametros="[{\"unicode_parametro_id\":\"\",\"nombre_tabla\":\"\",\"codigo_parametro\":\"10\",\"valor_parametro\":\"Valor 10 Valor\"},{\"unicode_parametro_id\":\"\",\"nombre_tabla\":\"\",\"codigo_parametro\":\"11\",\"valor_parametro\":\"Valor 11 Valor\"}]";
				
								
				json_unicodeparametros = gson.toJson(unicodeparametros_service.obtener_lista_parametros(nombre_tabla));
			}
		} catch (Exception e) {
			log.info("obtener_unicodeparametros >> ERROR! 1 >> " + e.getMessage());
		}
		return json_unicodeparametros;
	}
	
	@POST
	@Path("/validar_periodo")
	@Produces(MediaType.APPLICATION_JSON)
	public String validar_periodo(@FormParam("periodo_id") String periodo_id, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		clsResultado = unicodeparametros_service.validar_periodo(periodo_id);
		log.info("validar_periodo >> rest >> clsResultado.getObjeto() >> " + StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"Parametro\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/obtener_parametro")
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener_parametro(@FormParam("nombre_tabla") String nombre_tabla,@FormParam("codigo_parametro") String codigo_parametro,
			 @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		String json_unicodeparametros = "";
		try {			
				json_unicodeparametros = gson.toJson(unicodeparametros_service.obtener_parametros(nombre_tabla, codigo_parametro));
			
		} catch (Exception e) {
			log.info("obtener_parametro >> ERROR! 1 >> " + e.getMessage());
		}
		return json_unicodeparametros;
	}
	
	@POST
	@Path("/obtenerProcedenciaByPerfil")
	@Produces(MediaType.APPLICATION_JSON)
	public String obtenerProcedenciaByPerfil(@FormParam("perfil") String perfil,
			@Context HttpServletRequest _request,@Context HttpServletResponse _reponse){
		
		String json="";
		List<ClsUnicodeParametro> lstUnicodeParametro = new ArrayList<ClsUnicodeParametro>();
		
		log.info("** perfil : " + perfil);
		
		lstUnicodeParametro = unicodeparametros_service.obtenerProcedenciaByPerfil(perfil);		
		
		json = StringUtility.convertToJSON(lstUnicodeParametro);
		
		//log.info("Json : " + lRespuesta.toJSon());
		
		return json;
		
	}
}
