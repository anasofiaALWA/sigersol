package pe.gob.sigersol.entities;

public class ClsRecDisposicionFinal {

	private Integer rec_disposicion_final_id;
	private ClsCicloGestionResiduos ciclo_grs_id;
	private String adm_serv_id;
	private Integer costo_anual;
	private Integer fr1;
	private Integer fr2;
	private String frecuencia;
	private Integer cantidad_residuos;
	private String otros_descripcion;
	private Integer otros_antig_promedio;
	private Integer otros_capacidad;
	private Integer otros_recoleccion_anual;
	private Integer otros_numero_unidades;
	private Double total_rec_almacenamiento;
	private Double total_rec_barrido;
	private Integer codigo_usuario;
	
	public Integer getRec_disposicion_final_id() {
		return rec_disposicion_final_id;
	}
	public void setRec_disposicion_final_id(Integer rec_disposicion_final_id) {
		this.rec_disposicion_final_id = rec_disposicion_final_id;
	}
	public ClsCicloGestionResiduos getCiclo_grs_id() {
		return ciclo_grs_id;
	}
	public void setCiclo_grs_id(ClsCicloGestionResiduos ciclo_grs_id) {
		this.ciclo_grs_id = ciclo_grs_id;
	}
	public String getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(String adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getFr1() {
		return fr1;
	}
	public void setFr1(Integer fr1) {
		this.fr1 = fr1;
	}
	public Integer getFr2() {
		return fr2;
	}
	public void setFr2(Integer fr2) {
		this.fr2 = fr2;
	}
	public String getFrecuencia() {
		return frecuencia;
	}
	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}
	public Integer getCantidad_residuos() {
		return cantidad_residuos;
	}
	public void setCantidad_residuos(Integer cantidad_residuos) {
		this.cantidad_residuos = cantidad_residuos;
	}
	public String getOtros_descripcion() {
		return otros_descripcion;
	}
	public void setOtros_descripcion(String otros_descripcion) {
		this.otros_descripcion = otros_descripcion;
	}
	public Integer getOtros_antig_promedio() {
		return otros_antig_promedio;
	}
	public void setOtros_antig_promedio(Integer otros_antig_promedio) {
		this.otros_antig_promedio = otros_antig_promedio;
	}
	public Integer getOtros_capacidad() {
		return otros_capacidad;
	}
	public void setOtros_capacidad(Integer otros_capacidad) {
		this.otros_capacidad = otros_capacidad;
	}
	public Integer getOtros_recoleccion_anual() {
		return otros_recoleccion_anual;
	}
	public void setOtros_recoleccion_anual(Integer otros_recoleccion_anual) {
		this.otros_recoleccion_anual = otros_recoleccion_anual;
	}
	public Integer getOtros_numero_unidades() {
		return otros_numero_unidades;
	}
	public void setOtros_numero_unidades(Integer otros_numero_unidades) {
		this.otros_numero_unidades = otros_numero_unidades;
	}
	public Double getTotal_rec_almacenamiento() {
		return total_rec_almacenamiento;
	}
	public void setTotal_rec_almacenamiento(Double total_rec_almacenamiento) {
		this.total_rec_almacenamiento = total_rec_almacenamiento;
	}
	public Double getTotal_rec_barrido() {
		return total_rec_barrido;
	}
	public void setTotal_rec_barrido(Double total_rec_barrido) {
		this.total_rec_barrido = total_rec_barrido;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
