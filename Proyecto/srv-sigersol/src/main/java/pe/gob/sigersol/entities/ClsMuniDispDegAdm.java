package pe.gob.sigersol.entities;

public class ClsMuniDispDegAdm {

	private Integer muni_disp_deg_id;
	private ClsAreasDegradadas areasDegradadas;
	private ClsMes mes;
	private Integer cantidad;
	private Integer metodo_usado;
	private Integer codigo_usuario;
	
	public Integer getMuni_disp_deg_id() {
		return muni_disp_deg_id;
	}
	public void setMuni_disp_deg_id(Integer muni_disp_deg_id) {
		this.muni_disp_deg_id = muni_disp_deg_id;
	}
	public ClsAreasDegradadas getAreasDegradadas() {
		return areasDegradadas;
	}
	public void setAreasDegradadas(ClsAreasDegradadas areasDegradadas) {
		this.areasDegradadas = areasDegradadas;
	}
	public ClsMes getMes() {
		return mes;
	}
	public void setMes(ClsMes mes) {
		this.mes = mes;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getMetodo_usado() {
		return metodo_usado;
	}
	public void setMetodo_usado(Integer metodo_usado) {
		this.metodo_usado = metodo_usado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
