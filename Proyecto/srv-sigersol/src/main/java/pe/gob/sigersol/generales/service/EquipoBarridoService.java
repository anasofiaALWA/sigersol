package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsEquipoBarrido;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.EquipoBarridoDAO;

public class EquipoBarridoService {

	private static Logger log = Logger.getLogger(EquipoBarridoService.class);
	EquipoBarridoDAO equipoBarridoDAO = new EquipoBarridoDAO();
	
	public ClsResultado listarEquipo(ClsEquipoBarrido clsEquipoBarrido){
		ClsResultado clsResultado = null;
		try {
			clsResultado = equipoBarridoDAO.listarEquipo(clsEquipoBarrido);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsEquipoBarrido clsEquipoBarrido) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = equipoBarridoDAO.insertar(clsEquipoBarrido);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsEquipoBarrido clsEquipoBarrido) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = equipoBarridoDAO.actualizar(clsEquipoBarrido);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
