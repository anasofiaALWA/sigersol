package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCostoReferencial;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduoSolido;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.CostoReferencialService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/costoReferencial")
@RequestScoped
public class CostoReferencialRestService {

	private static Logger log = Logger.getLogger(CostoReferencialRestService.class.getName());
	private ClsCostoReferencial clsCostoReferencial;
	private ClsValorizacion clsValorizacion;
	private ClsTipoResiduoSolido clsTipoResiduoSolido;
	private CostoReferencialService costoReferencialService;
	private List<ClsCostoReferencial> listaCostoReferencial;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCostoReferencial")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCostoReferencial(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			costoReferencialService = new CostoReferencialService();
			
			jsonEntradaListaCostoReferencial(paramJson);
			
			log.info("**Obtener Residuos Aprovechados**");
			
			clsResultado = costoReferencialService.listarCostoReferencial(clsCostoReferencial);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCostoReferencial\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		costoReferencialService = new CostoReferencialService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaCostoReferencial(paramJson);

			for (ClsCostoReferencial clsCostoReferencial : listaCostoReferencial) {
				clsResultado = costoReferencialService.insertar(clsCostoReferencial);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		costoReferencialService = new CostoReferencialService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCostoReferencial(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCostoReferencial clsCostoReferencial : listaCostoReferencial) {
			clsResultado = costoReferencialService.actualizar(clsCostoReferencial);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de ResiduosAprovechados
	 * 
	 * */
	private void jsonEntradaCostoReferencial(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CostoReferencialRestService :	jsonEntradaCostoReferencial" + jsonEntrada);

		JSONObject jsonObjectTipoResiduo;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCostoReferencial = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCostoReferencial = new ClsCostoReferencial();
			clsTipoResiduoSolido = new ClsTipoResiduoSolido();
			clsValorizacion= new ClsValorizacion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("valorizacion_id") != null && innerObject.get("valorizacion_id").toString() != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(innerObject.get("valorizacion_id").toString()));
			}
			
			clsCostoReferencial.setValorizacion(clsValorizacion);
			
			if (innerObject.get("tipo_residuo") != null && innerObject.get("tipo_residuo").toString() != "") {
				jsonObjectTipoResiduo  = (JSONObject) innerObject.get("tipo_residuo");
				clsTipoResiduoSolido.setTipo_residuo_id(Integer.parseInt(jsonObjectTipoResiduo.get("tipo_residuo_id").toString()));
			}
			clsCostoReferencial.setTipo_residuo(clsTipoResiduoSolido);
			
			if (innerObject.get("costo") != null && innerObject.get("costo").toString() != "") {
				clsCostoReferencial.setCosto(Integer.parseInt(innerObject.get("costo").toString()));
			}
			else {
				clsCostoReferencial.setCosto(0);
			}

			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCostoReferencial.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCostoReferencial.add(clsCostoReferencial);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCostoReferencial(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CostoReferencialRestService :	jsonEntradaListaCostoReferencial" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResiduosAprovechados;
		JSONParser jsonParser = new JSONParser();
		
		clsCostoReferencial = new ClsCostoReferencial();
		clsTipoResiduoSolido = new ClsTipoResiduoSolido();
		clsValorizacion= new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResiduosAprovechados = (JSONObject) jsonObjectPrincipal.get("CostoReferencial");
			
			if (jsonObjectResiduosAprovechados.get("valorizacion_id") != null && jsonObjectResiduosAprovechados.get("valorizacion_id") != "" &&
					jsonObjectResiduosAprovechados.get("valorizacion_id") != " ") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectResiduosAprovechados.get("valorizacion_id").toString()));
			}
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsCostoReferencial.setValorizacion(clsValorizacion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
