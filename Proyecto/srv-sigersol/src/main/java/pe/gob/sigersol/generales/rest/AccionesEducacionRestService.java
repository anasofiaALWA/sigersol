package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAccionesEducacion;
import pe.gob.sigersol.entities.ClsEducacionAmbiental;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoEventos;
import pe.gob.sigersol.generales.service.AccionesEducacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/accionesEducacionX")
@RequestScoped
public class AccionesEducacionRestService {

	private static Logger log = Logger.getLogger(AccionesEducacionRestService.class.getName());
	private ClsAccionesEducacion clsAccionesEducacion;
	private ClsEducacionAmbiental clsEducacionAmbiental;
	private ClsTipoEventos clsTipoEventos;
	private AccionesEducacionService accionesEducacionService;
	private List<ClsAccionesEducacion> listaAccionesEducacion;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarAccionesEducacion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarAccionesEducacion(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			accionesEducacionService = new AccionesEducacionService();
			
			jsonEntradaListaAccionesEducacion(paramJson);
			
			log.info("**Obtener Acciones Educacion");
			
			clsResultado = accionesEducacionService.listarAccionesEducacion(clsAccionesEducacion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstAcciones\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		accionesEducacionService = new AccionesEducacionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaAccionesEducacion(paramJson);
	
			for (ClsAccionesEducacion clsAccionesEducacion : listaAccionesEducacion) {
				clsResultado = accionesEducacionService.insertar(clsAccionesEducacion);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		accionesEducacionService = new AccionesEducacionService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaAccionesEducacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsAccionesEducacion clsAccionesEducacion : listaAccionesEducacion) {
			clsResultado = accionesEducacionService.actualizar(clsAccionesEducacion);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaAccionesEducacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AccionesEducacionRestService : jsonEntradaVehiculos" + jsonEntrada);

		JSONObject jsonObjectTipoEventos;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaAccionesEducacion = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsAccionesEducacion = new ClsAccionesEducacion();
			clsTipoEventos = new ClsTipoEventos();
			clsEducacionAmbiental = new ClsEducacionAmbiental();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("educacion_ambiental_id") != null && innerObject.get("educacion_ambiental_id").toString() != "") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(innerObject.get("educacion_ambiental_id").toString()));
			} 
			else {
				clsEducacionAmbiental.setEducacion_ambiental_id(-1);
			}
			
			clsAccionesEducacion.setEducacion_ambiental(clsEducacionAmbiental);
			
			if (innerObject.get("tipo_eventos") != null && innerObject.get("tipo_eventos").toString() != "") {
				jsonObjectTipoEventos  = (JSONObject) innerObject.get("tipo_eventos");
				clsTipoEventos.setTipo_eventos_id(Integer.parseInt(jsonObjectTipoEventos.get("tipo_eventos_id").toString()));
			}
			clsAccionesEducacion.setTipo_eventos(clsTipoEventos);
			
			if (innerObject.get("capacitad_hombres") != null && innerObject.get("capacitad_hombres").toString() != "") {
				clsAccionesEducacion.setCapacitad_hombres(Integer.parseInt(innerObject.get("capacitad_hombres").toString()));
			} else{
				clsAccionesEducacion.setCapacitad_hombres(0);
			}
			
			if (innerObject.get("capacitad_mujeres") != null && innerObject.get("capacitad_mujeres").toString() != "") {
				clsAccionesEducacion.setCapacitad_mujeres(Integer.parseInt(innerObject.get("capacitad_mujeres").toString()));
			} else{
				clsAccionesEducacion.setCapacitad_mujeres(0);
			}
		
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsAccionesEducacion.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaAccionesEducacion.add(clsAccionesEducacion);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaAccionesEducacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AccionesEducacionRestService : jsonEntradaListaAccionesEducacion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectEducacion;
		JSONParser jsonParser = new JSONParser();
		
		clsAccionesEducacion = new ClsAccionesEducacion();
		clsTipoEventos = new ClsTipoEventos();
		clsEducacionAmbiental = new ClsEducacionAmbiental();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectEducacion = (JSONObject) jsonObjectPrincipal.get("AccionesEducacion");
			
			if (jsonObjectEducacion.get("educacion_ambiental_id") != null && jsonObjectEducacion.get("educacion_ambiental_id") != "" &&
					jsonObjectEducacion.get("educacion_ambiental_id") != " ") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(jsonObjectEducacion.get("educacion_ambiental_id").toString()));
			}
			else
				clsEducacionAmbiental.setEducacion_ambiental_id(-1);
			
			clsAccionesEducacion.setEducacion_ambiental(clsEducacionAmbiental);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
