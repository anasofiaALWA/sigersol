package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.UbigeoDAO;

public class UbigeoService {
	private static Logger log = Logger.getLogger(UbigeoService.class.getName());
	UbigeoDAO dao = new UbigeoDAO();
	
	public ClsResultado listarPorUbigeo(String ubigeo_id){
		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.listarPorUbigeo(ubigeo_id);
		} catch (Exception e) {
			log.info("message listarPorUbigeo >> service >> " + e.getMessage());
		}
		
		return clsResultado;
	}
	public ClsResultado listaDepartamento(){
		ClsResultado clsResultado = null;
		try {
			clsResultado= dao.listaDepartamento();
		} catch (Exception e) {
			log.info("message listaDepartamento >> service >> " + e.getMessage());
		}
		
		return clsResultado;
	}
	public ClsResultado listaProvincia(String departamento){
		ClsResultado clsResultado = null;
		try {
			clsResultado= dao.listaProvincia(departamento);
		} catch (Exception e) {
			log.info("message listaProvincia >> service >> " + e.getMessage());
		}
		
		return clsResultado;
	}
	public ClsResultado listaDistrito(String departamento,String provincia){
		ClsResultado clsResultado = null;
		try {
			clsResultado= dao.listaDistrito(departamento, provincia);
		} catch (Exception e) {
			log.info("message listaDistrito >> service >> " + e.getMessage());
		}
		
		return clsResultado;
	}

}
