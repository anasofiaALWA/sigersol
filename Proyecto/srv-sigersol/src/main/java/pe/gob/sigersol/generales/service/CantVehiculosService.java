package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCantVehiculos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CantVehiculosDAO;

public class CantVehiculosService {

	private static Logger log = Logger.getLogger(CantVehiculosService.class);
	CantVehiculosDAO cantVehiculosDAO = new CantVehiculosDAO();
	
	public ClsResultado listarCantVehiculos(ClsCantVehiculos clsCantVehiculo){
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantVehiculosDAO.listarCantVehiculos(clsCantVehiculo);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCantVehiculos clsCantVehiculo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantVehiculosDAO.insertar(clsCantVehiculo);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCantVehiculos clsCantVehiculo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantVehiculosDAO.actualizar(clsCantVehiculo);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
