package pe.gob.sigersol.entities;

public class ClsRecoleccionSelectivaRCD {

	private Integer recoleccion_id;
	private ClsCicloGestionResiduos ciclo_grs_id;
	private String adm_serv_id;
	private Integer costo_anual;
	private Integer cantidad_residuos;
	private Integer usuarios_atendidos;
	private String descrip_otro_vehiculo;
	private Integer cantidad_otro;
	private Integer antig_promedio_otro;
	private Integer codigo_usuario;
	
	public Integer getRecoleccion_id() {
		return recoleccion_id;
	}
	public void setRecoleccion_id(Integer recoleccion_id) {
		this.recoleccion_id = recoleccion_id;
	}
	public ClsCicloGestionResiduos getCiclo_grs_id() {
		return ciclo_grs_id;
	}
	public void setCiclo_grs_id(ClsCicloGestionResiduos ciclo_grs_id) {
		this.ciclo_grs_id = ciclo_grs_id;
	}
	public String getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(String adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getCantidad_residuos() {
		return cantidad_residuos;
	}
	public void setCantidad_residuos(Integer cantidad_residuos) {
		this.cantidad_residuos = cantidad_residuos;
	}
	public Integer getUsuarios_atendidos() {
		return usuarios_atendidos;
	}
	public void setUsuarios_atendidos(Integer usuarios_atendidos) {
		this.usuarios_atendidos = usuarios_atendidos;
	}
	public String getDescrip_otro_vehiculo() {
		return descrip_otro_vehiculo;
	}
	public void setDescrip_otro_vehiculo(String descrip_otro_vehiculo) {
		this.descrip_otro_vehiculo = descrip_otro_vehiculo;
	}
	public Integer getCantidad_otro() {
		return cantidad_otro;
	}
	public void setCantidad_otro(Integer cantidad_otro) {
		this.cantidad_otro = cantidad_otro;
	}
	public Integer getAntig_promedio_otro() {
		return antig_promedio_otro;
	}
	public void setAntig_promedio_otro(Integer antig_promedio_otro) {
		this.antig_promedio_otro = antig_promedio_otro;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
