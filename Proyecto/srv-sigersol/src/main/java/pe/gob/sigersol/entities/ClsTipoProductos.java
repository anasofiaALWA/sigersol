package pe.gob.sigersol.entities;

public class ClsTipoProductos {

	private Integer tipo_productos_id;
	private String nombre;
	private String informacion;
	
	public Integer getTipo_productos_id() {
		return tipo_productos_id;
	}
	public void setTipo_productos_id(Integer tipo_productos_id) {
		this.tipo_productos_id = tipo_productos_id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getInformacion() {
		return informacion;
	}
	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}
	
}
