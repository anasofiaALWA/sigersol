package pe.gob.sigersol.entities;

public class ClsProteccion {

	private Integer proteccion_id;
	private ClsBarrido barrido;
	private ClsTipoEquipo tipo_equipo;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getProteccion_id() {
		return proteccion_id;
	}
	public void setProteccion_id(Integer proteccion_id) {
		this.proteccion_id = proteccion_id;
	}
	public ClsBarrido getBarrido() {
		return barrido;
	}
	public void setBarrido(ClsBarrido barrido) {
		this.barrido = barrido;
	}
	public ClsTipoEquipo getTipo_equipo() {
		return tipo_equipo;
	}
	public void setTipo_equipo(ClsTipoEquipo tipo_equipo) {
		this.tipo_equipo = tipo_equipo;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
