package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsPropia;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTransferencia;

public class PropiaDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(PropiaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_propia = "{call SIGERSOLBL.SGR_SP_LIST_SGR_PROPIA(?,?)}";
	private final String sgr_sp_ins_propia = "{call SIGERSOLBL.SGR_SP_INS_PROPIA(?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_propia = "{call SIGERSOLBL.SGR_SP_UPD_PROPIA(?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsPropia clsPropia) {
		ClsResultado clsResultado = new ClsResultado();
		ClsPropia item = new ClsPropia();
		ClsTransferencia transferencia = new ClsTransferencia();
		con = null;
		String sql = this.sgr_sp_list_sgr_propia;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPropia.getTransferencia().getTransferencia_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setPropia_id(rs.getInt("PROPIA_ID"));
					transferencia.setTransferencia_id(rs.getInt("TRANSFERENCIA_ID"));
					item.setTransferencia(transferencia);
					item.setNum_iga(rs.getString("NUM_IGA"));
					item.setInfraest(rs.getString("INFRAEST"));
					item.setAutoridad_resolucion(rs.getString("AUTORIDAD_RESOLUCION"));
					item.setFecha_emision(rs.getString("FECHA_EMISION"));
					item.setLicencia(rs.getString("LICENCIA"));
					item.setInicio_permiso(rs.getString("INICIO_PERMISO"));
					item.setCulminacion_permiso(rs.getString("CULMINACION_PERMISO"));
							
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsPropia clsPropia) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_propia;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPropia.getTransferencia().getTransferencia_id());
			cs.setString(2, clsPropia.getNum_iga());
			cs.setString(3, clsPropia.getInfraest());
			cs.setString(4, clsPropia.getAutoridad_resolucion());
			cs.setString(5, clsPropia.getFecha_emision());
			cs.setString(6, clsPropia.getLicencia());
			cs.setString(7, clsPropia.getInicio_permiso());
			cs.setString(8, clsPropia.getCulminacion_permiso());
			cs.setInt(9, clsPropia.getCodigo_usuario());
			cs.registerOutParameter(10, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(10);
			
			log.info("TransferenciaDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsPropia clsPropia) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_propia;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsPropia.getPropia_id());
			cs.setInt(2, clsPropia.getTransferencia().getTransferencia_id());
			cs.setString(3, clsPropia.getNum_iga());
			cs.setString(4, clsPropia.getInfraest());
			cs.setString(5, clsPropia.getAutoridad_resolucion());
			cs.setString(6, clsPropia.getFecha_emision());
			cs.setString(7, clsPropia.getLicencia());
			cs.setString(8, clsPropia.getInicio_permiso());
			cs.setString(9, clsPropia.getCulminacion_permiso());
			cs.setInt(10, clsPropia.getCodigo_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);

			log.info("TransferenciaDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}	
}
