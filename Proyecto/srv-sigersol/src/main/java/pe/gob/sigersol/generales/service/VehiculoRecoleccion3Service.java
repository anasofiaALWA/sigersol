package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoRecoleccion;
import pe.gob.sigersol.entities.dao.VehiculoRecoleccion3DAO;

public class VehiculoRecoleccion3Service {

	private static Logger log = Logger.getLogger(VehiculoRecoleccion3Service.class);
	VehiculoRecoleccion3DAO vehiculoRecoleccion3DAO = new VehiculoRecoleccion3DAO();
	
	public ClsResultado listarVehiculoRecoleccion(ClsVehiculoRecoleccion clsVehiculoRecoleccion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccion3DAO.listarVehiculoRecoleccion(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoRecoleccion clsVehiculoRecoleccion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccion3DAO.insertar(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsVehiculoRecoleccion clsVehiculoRecoleccion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = vehiculoRecoleccion3DAO.actualizar(clsVehiculoRecoleccion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
