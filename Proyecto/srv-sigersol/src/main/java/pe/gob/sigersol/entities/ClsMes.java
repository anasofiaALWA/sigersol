package pe.gob.sigersol.entities;

public class ClsMes {

	private Integer mes_id;
	private String nombre_mes;
	
	public Integer getMes_id() {
		return mes_id;
	}
	public void setMes_id(Integer mes_id) {
		this.mes_id = mes_id;
	}
	public String getNombre_mes() {
		return nombre_mes;
	}
	public void setNombre_mes(String nombre_mes) {
		this.nombre_mes = nombre_mes;
	}
	
	
}
