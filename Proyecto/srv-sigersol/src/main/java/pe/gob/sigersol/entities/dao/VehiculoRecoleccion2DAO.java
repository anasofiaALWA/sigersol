package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoVehiculo;
import pe.gob.sigersol.entities.ClsVehiculoRecoleccion;

public class VehiculoRecoleccion2DAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(VehiculoRecoleccion2DAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_vehic_reco_valor = "{call SIGERSOLBL.SGR_SP_INS_VEHICULO_NC2(?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_vehic_reco_valor = "{call SIGERSOLBL.SGR_SP_UPD_VEHICULO_NC2(?,?,?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_vehic_reco_valor = "{call SIGERSOLBL.SGR_SP_LIST_VEHICULO_NC2(?,?)}";  
	
	public ClsResultado listarVehiculoRecoleccion(ClsVehiculoRecoleccion clsVehiculoRecoleccion){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsVehiculoRecoleccion> lista = new ArrayList<ClsVehiculoRecoleccion>();
		con = null;
		String sql = this.sgr_sp_list_vehic_reco_valor;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsVehiculoRecoleccion.getRecValorizacion().getRec_valorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsVehiculoRecoleccion item = new ClsVehiculoRecoleccion();
				ClsTipoVehiculo tipoVehiculo = new ClsTipoVehiculo();
				
				try {
					tipoVehiculo.setTipo_vehiculo_id(rs.getInt("TIPO_VEHICULO_ID"));
					tipoVehiculo.setNombre_tipo(rs.getString("NOMBRE_TIPO"));
					item.setTipo_vehiculo(tipoVehiculo);
					if(clsVehiculoRecoleccion.getRecValorizacion().getRec_valorizacion_id() == -1){
						item.setAntig_promedio(null);
						item.setCapacidad(null);
						item.setRecoleccion_anual(null);
						item.setNumero_unidades(null);
					} else{
						item.setAntig_promedio(rs.getInt("ANTIG_PROMEDIO"));
						item.setCapacidad(rs.getDouble("CAPACIDAD"));
						item.setRecoleccion_anual(rs.getDouble("RECOLECCION_ANUAL"));
						item.setNumero_unidades(rs.getInt("NUMERO_UNIDADES"));
					}
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsVehiculoRecoleccion clsVehiculoRecoleccion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_vehic_reco_valor;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsVehiculoRecoleccion.getRecValorizacion().getRec_valorizacion_id());
			cs.setInt(2, clsVehiculoRecoleccion.getTipo_vehiculo().getTipo_vehiculo_id());
			cs.setInt(3, clsVehiculoRecoleccion.getAntig_promedio());
			cs.setDouble(4, clsVehiculoRecoleccion.getCapacidad());
			cs.setDouble(5, clsVehiculoRecoleccion.getRecoleccion_anual());
			cs.setInt(6, clsVehiculoRecoleccion.getNumero_unidades());
			cs.setInt(7, clsVehiculoRecoleccion.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);
			
			log.info("VehiculoRecoleccion2DAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsVehiculoRecoleccion clsVehiculoRecoleccion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_vehic_reco_valor;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsVehiculoRecoleccion.getRecValorizacion().getRec_valorizacion_id());
			cs.setInt(2, clsVehiculoRecoleccion.getTipo_vehiculo().getTipo_vehiculo_id());
			cs.setInt(3, clsVehiculoRecoleccion.getAntig_promedio());
			cs.setDouble(4, clsVehiculoRecoleccion.getCapacidad());
			cs.setDouble(5, clsVehiculoRecoleccion.getRecoleccion_anual());
			cs.setInt(6, clsVehiculoRecoleccion.getNumero_unidades());
			cs.setInt(7, clsVehiculoRecoleccion.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);

			log.info("VehiculoRecoleccion2DAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
