package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.JsonObject;

import pe.gob.sigersol.entities.ClsCCGenCombustible;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CCGenCombustibleService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/ccGenCombustible")
@RequestScoped
public class CCGenCombustibleRestService {
	private static Logger log = Logger.getLogger(CCGenCombustibleRestService.class.getName());
	private CCGenCombustibleService ccGenCombustibleService;

	@POST
	@Path("/listar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listar(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response)
			throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		ccGenCombustibleService = new CCGenCombustibleService();
		Integer ccGenActividadId = jsonEntradaList(paramJson);
		clsResultado = ccGenCombustibleService.listar(ccGenActividadId);
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();

	}

	private Integer jsonEntradaList(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CCGenCombustibleRestService :	jsonEntradaList" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		Integer ccGenActividadId = -1;

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Filtro");

			if (jsonObject.get("ccGenActividadId") != null && jsonObject.get("ccGenActividadId") != "") {
				ccGenActividadId = Integer.parseInt(jsonObject.get("ccGenActividadId").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return ccGenActividadId;
	}

	/*private List<ClsCCGenCombustible> jsonEntradaInsertar(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CCGenCombustibleRestService :	jsonEntradaInsertar" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObjectArray;

		List<ClsCCGenCombustible> lista = new ArrayList<ClsCCGenCombustible>();

		try {
			jsonObjectArray = (JSONArray) jsonParser.parse(jsonEntrada);
			Iterator i = jsonObjectArray.iterator();
			while (i.hasNext()) {
				JSONObject jsonObject = (JSONObject) i.next();
				ClsCCGenCombustible item = new ClsCCGenCombustible();
				if (jsonObject.get("ccGenActividadId") != null && jsonObject.get("ccGenActividadId") != "") {
					item.setCcGenActividadId(Integer.parseInt(jsonObject.get("ccGenActividadId").toString()));
				}
				if (jsonObject.get("ccGenActividadId") != null && jsonObject.get("ccGenActividadId") != "") {
					item.set(Integer.parseInt(jsonObject.get("ccGenActividadId").toString()));
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return lista;
	}*/
}
