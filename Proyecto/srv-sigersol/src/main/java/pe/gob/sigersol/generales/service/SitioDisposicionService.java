package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;
import pe.gob.sigersol.entities.dao.SitioDisposicionDAO;

public class SitioDisposicionService {

	private static Logger log = Logger.getLogger(SitioDisposicionService.class);
	SitioDisposicionDAO sitioDisposicionDAO = new SitioDisposicionDAO();
	
	public ClsResultado listarCoordenada(ClsSitioDisposicion clsSitioDisposicion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = sitioDisposicionDAO.listarCoordenada(clsSitioDisposicion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado listarCoordenada2(ClsSitioDisposicion clsSitioDisposicion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = sitioDisposicionDAO.listarCoordenada2(clsSitioDisposicion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado obtener(ClsSitioDisposicion clsSitioDisposicion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = sitioDisposicionDAO.obtener(clsSitioDisposicion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado obtenerAdm(ClsSitioDisposicion clsSitioDisposicion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = sitioDisposicionDAO.obtenerAdm(clsSitioDisposicion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado listar(ClsSitioDisposicion clsSitioDisposicion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = sitioDisposicionDAO.listar(clsSitioDisposicion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado listarAdm(ClsSitioDisposicion clsSitioDisposicion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = sitioDisposicionDAO.listarAdm(clsSitioDisposicion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
