package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.entities.dao.Valorizacion1DAO;
import pe.gob.sigersol.entities.dao.ValorizacionDAO;

public class Valorizacion1Service {

	private static Logger log = Logger.getLogger(Valorizacion1Service.class);
	Valorizacion1DAO valorizacion1DAO = new Valorizacion1DAO();
	
	public ClsResultado obtener(ClsValorizacion clsValorizacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacion1DAO.obtener(clsValorizacion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsValorizacion clsValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacion1DAO.insertar(clsValorizacion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsValorizacion clsValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacion1DAO.actualizar(clsValorizacion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
