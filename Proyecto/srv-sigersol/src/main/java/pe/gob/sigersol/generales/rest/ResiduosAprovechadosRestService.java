package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResiduosAprovechados;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduoSolido;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.ResiduosAprovechadosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase ResiduosAprovechadosRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de valorización permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/residuosAprovechados")
@RequestScoped
public class ResiduosAprovechadosRestService {

	private static Logger log = Logger.getLogger(ResiduosAprovechadosRestService.class.getName());
	private ClsResiduosAprovechados clsResiduosAprovechados;
	private ClsValorizacion clsValorizacion;
	private ClsTipoResiduoSolido clsTipoResiduoSolido;
	
	private ResiduosAprovechadosService residuosAprovechadosService;
	private List<ClsResiduosAprovechados> listaResiduosAprovechados;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarResiduosAprovechados")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarResiduosAprovechados(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			residuosAprovechadosService = new ResiduosAprovechadosService();
			
			jsonEntradaListaResiduosAprovechados(paramJson);
			
			log.info("**Obtener Residuos Aprovechados**");
			
			clsResultado = residuosAprovechadosService.listarResiduosAprovechados(clsResiduosAprovechados);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstResiduosAprovechados\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		residuosAprovechadosService = new ResiduosAprovechadosService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaResiduosAprovechados(paramJson);

			for (ClsResiduosAprovechados clsResiduosAprovechados : listaResiduosAprovechados) {
				clsResultado = residuosAprovechadosService.insertar(clsResiduosAprovechados);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		residuosAprovechadosService = new ResiduosAprovechadosService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaResiduosAprovechados(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsResiduosAprovechados clsResiduosAprovechados : listaResiduosAprovechados) {
			clsResultado = residuosAprovechadosService.actualizar(clsResiduosAprovechados);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de ResiduosAprovechados
	 * 
	 * */
	private void jsonEntradaResiduosAprovechados(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ResiduosAprovechadosRestService :	jsonEntradaResiduosAprovechados" + jsonEntrada);

		JSONObject jsonObjectTipoResiduo;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaResiduosAprovechados = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsResiduosAprovechados = new ClsResiduosAprovechados();
			clsTipoResiduoSolido = new ClsTipoResiduoSolido();
			clsValorizacion= new ClsValorizacion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("valorizacion_id") != null && innerObject.get("valorizacion_id").toString() != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(innerObject.get("valorizacion_id").toString()));
			}
			
			clsResiduosAprovechados.setValorizacion(clsValorizacion);
			
			if (innerObject.get("tipo_residuo_solido") != null && innerObject.get("tipo_residuo_solido").toString() != "") {
				jsonObjectTipoResiduo  = (JSONObject) innerObject.get("tipo_residuo_solido");
				clsTipoResiduoSolido.setTipo_residuo_id(Integer.parseInt(jsonObjectTipoResiduo.get("tipo_residuo_id").toString()));
			}
			clsResiduosAprovechados.setTipo_residuo_solido(clsTipoResiduoSolido);
			
			if (innerObject.get("enero") != null && innerObject.get("enero").toString() != "") {
				clsResiduosAprovechados.setEnero(Integer.parseInt(innerObject.get("enero").toString()));
			}
			else {
				clsResiduosAprovechados.setEnero(0);
			}
			
			if (innerObject.get("febrero") != null && innerObject.get("febrero").toString() != "") {
				clsResiduosAprovechados.setFebrero(Integer.parseInt(innerObject.get("febrero").toString()));
			}
			else {
				clsResiduosAprovechados.setFebrero(0);
			}
			
			if (innerObject.get("marzo") != null && innerObject.get("marzo").toString() != "") {
				clsResiduosAprovechados.setMarzo(Integer.parseInt(innerObject.get("marzo").toString()));
			}
			else {
				clsResiduosAprovechados.setMarzo(0);
			}
			
			if (innerObject.get("abril") != null && innerObject.get("abril").toString() != "") {
				clsResiduosAprovechados.setAbril(Integer.parseInt(innerObject.get("abril").toString()));
			}
			else {
				clsResiduosAprovechados.setAbril(0);
			}
			
			if (innerObject.get("mayo") != null && innerObject.get("mayo").toString() != "") {
				clsResiduosAprovechados.setMayo(Integer.parseInt(innerObject.get("mayo").toString()));
			}
			else {
				clsResiduosAprovechados.setMayo(0);
			}
			
			if (innerObject.get("junio") != null && innerObject.get("junio").toString() != "") {
				clsResiduosAprovechados.setJunio(Integer.parseInt(innerObject.get("junio").toString()));
			}
			else {
				clsResiduosAprovechados.setJunio(0);
			}
			
			if (innerObject.get("julio") != null && innerObject.get("julio").toString() != "") {
				clsResiduosAprovechados.setJulio(Integer.parseInt(innerObject.get("julio").toString()));
			}
			else {
				clsResiduosAprovechados.setJulio(0);
			}
			
			if (innerObject.get("agosto") != null && innerObject.get("agosto").toString() != "") {
				clsResiduosAprovechados.setAgosto(Integer.parseInt(innerObject.get("agosto").toString()));
			}
			else {
				clsResiduosAprovechados.setAgosto(0);
			}
			
			if (innerObject.get("setiembre") != null && innerObject.get("setiembre").toString() != "") {
				clsResiduosAprovechados.setSetiembre(Integer.parseInt(innerObject.get("setiembre").toString()));
			}
			else {
				clsResiduosAprovechados.setSetiembre(0);
			}
			
			if (innerObject.get("octubre") != null && innerObject.get("octubre").toString() != "") {
				clsResiduosAprovechados.setOctubre(Integer.parseInt(innerObject.get("octubre").toString()));
			}
			else {
				clsResiduosAprovechados.setOctubre(0);
			}
			
			if (innerObject.get("noviembre") != null && innerObject.get("noviembre").toString() != "") {
				clsResiduosAprovechados.setNoviembre(Integer.parseInt(innerObject.get("noviembre").toString()));
			}
			else {
				clsResiduosAprovechados.setNoviembre(0);
			}
			
			if (innerObject.get("diciembre") != null && innerObject.get("diciembre").toString() != "") {
				clsResiduosAprovechados.setDiciembre(Integer.parseInt(innerObject.get("diciembre").toString()));
			}
			else {
				clsResiduosAprovechados.setDiciembre(0);
			}			
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsResiduosAprovechados.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaResiduosAprovechados.add(clsResiduosAprovechados);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaResiduosAprovechados(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ResiduosAprovechadosRestService :	jsonEntradaListaResiduosAprovechados" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResiduosAprovechados;
		JSONParser jsonParser = new JSONParser();
		
		clsResiduosAprovechados = new ClsResiduosAprovechados();
		clsTipoResiduoSolido = new ClsTipoResiduoSolido();
		clsValorizacion= new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResiduosAprovechados = (JSONObject) jsonObjectPrincipal.get("ResiduosAprovechados");
			
			if (jsonObjectResiduosAprovechados.get("valorizacion_id") != null && jsonObjectResiduosAprovechados.get("valorizacion_id") != "" &&
					jsonObjectResiduosAprovechados.get("valorizacion_id") != " ") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectResiduosAprovechados.get("valorizacion_id").toString()));
			}
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsResiduosAprovechados.setValorizacion(clsValorizacion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
