package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsPersona;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.PersonaService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/persona")
@RequestScoped
public class PersonaRestService {

	private static Logger log = Logger.getLogger(PersonaRestService.class.getName());
	private ClsPersona clsPersona;
	private PersonaService personaService;
	private List<ClsPersona> listaPersona;

	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		personaService = new PersonaService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaNoDomiciliar(paramJson);
			
			for (ClsPersona clsPersona : listaPersona) {
			//	clsResultado = personaService.insertar(clsPersona);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Contenedor
	 * 
	 * */
	private void jsonEntradaNoDomiciliar(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService : jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectTipoContenedor;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaPersona = new ArrayList();
		
		Integer contador = 1;
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsPersona = new ClsPersona();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("dni") != null && innerObject.get("dni").toString() != "") {
				clsPersona.setDni(Integer.parseInt(innerObject.get("dni").toString()));
			}

			if (innerObject.get("nom_apellido") != null && innerObject.get("nom_apellido").toString() != "") {
				clsPersona.setNom_apellidos(innerObject.get("nom_apellido").toString());
			}
			
			if (innerObject.get("sexo") != null && innerObject.get("sexo").toString() != "") {
				clsPersona.setSexo(innerObject.get("sexo").toString());
			}
			
			if (innerObject.get("rango") != null && innerObject.get("rango").toString() != "") {
				clsPersona.setRango_edad(innerObject.get("rango").toString());
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsPersona.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaPersona.add(clsPersona);	
		}
			
		log.info("----------------------------------------------------------------------------------");
	}
}
