package pe.gob.sigersol.entities;

public class ClsFiltro{
	private Integer int_filtro1;
	private Integer int_filtro2;
	private Integer int_filtro3;
	
	private String str_filtro1;
	private String str_filtro2;
	
	private Float fl_filtro1;
	
	public Integer getInt_filtro1() {
		return int_filtro1;
	}
	public void setInt_filtro1(Integer int_filtro1) {
		this.int_filtro1 = int_filtro1;
	}
	public Integer getInt_filtro2() {
		return int_filtro2;
	}
	public void setInt_filtro2(Integer int_filtro2) {
		this.int_filtro2 = int_filtro2;
	}	
	public Integer getInt_filtro3() {
		return int_filtro3;
	}
	public void setInt_filtro3(Integer int_filtro3) {
		this.int_filtro3 = int_filtro3;
	}
	public String getStr_filtro1() {
		return str_filtro1;
	}
	public void setStr_filtro1(String str_filtro1) {
		this.str_filtro1 = str_filtro1;
	}
	public String getStr_filtro2() {
		return str_filtro2;
	}
	public void setStr_filtro2(String str_filtro2) {
		this.str_filtro2 = str_filtro2;
	}
	public Float getFl_filtro1() {
		return fl_filtro1;
	}
	public void setFl_filtro1(Float fl_filtro1) {
		this.fl_filtro1 = fl_filtro1;
	}
		
	
	
	
	
}