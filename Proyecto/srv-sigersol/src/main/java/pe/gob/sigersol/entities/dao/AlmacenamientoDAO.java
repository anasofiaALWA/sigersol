package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAlmacenamiento;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;

public class AlmacenamientoDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(AlmacenamientoDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_almacenamiento = "{call SIGERSOLBL.SGR_SP_LIST_SGR_ALMACENAMIENTO(?,?)}";
	private final String sgr_sp_ins_almacenamiento = "{call SIGERSOLBL.SGR_SP_INS_ALMACENAMIENTO(?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_almacenamiento = "{call SIGERSOLBL.SGR_SP_UPD_ALMACENAMIENTO(?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsAlmacenamiento clsAlmacenamiento) {
		ClsResultado clsResultado = new ClsResultado();
		ClsAlmacenamiento item = new ClsAlmacenamiento();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_sgr_almacenamiento;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAlmacenamiento.getCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setAlmacenamiento_id(rs.getInt("ALMACENAMIENTO_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(cicloGestionResiduos);
					item.setCosto_anual(rs.getInt("COSTO_ANUAL"));
					item.setCapacidad_papeleras(rs.getInt("CAPACIDAD"));
					item.setMetal(rs.getInt("METAL"));
					item.setFibra_vidrio(rs.getInt("FIBRA_VIDRIO"));
					item.setPlastico(rs.getInt("PLASTICO"));
					item.setMixto(rs.getInt("MIXTO"));
					item.setDescripcion_mixto(rs.getString("DESCRIPCION_MIXTO"));
					item.setTotal_recolectado(rs.getDouble("TOTAL_RECOLECTADO"));
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsAlmacenamiento clsAlmacenamiento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_almacenamiento;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAlmacenamiento.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsAlmacenamiento.getCosto_anual());
			cs.setInt(3, clsAlmacenamiento.getCapacidad_papeleras());
			cs.setInt(4, clsAlmacenamiento.getMetal());
			cs.setInt(5, clsAlmacenamiento.getFibra_vidrio());
			cs.setInt(6, clsAlmacenamiento.getPlastico());
			cs.setInt(7, clsAlmacenamiento.getMixto());
			cs.setString(8, clsAlmacenamiento.getDescripcion_mixto());
			cs.setDouble(9, clsAlmacenamiento.getTotal_recolectado());
			cs.setInt(10, clsAlmacenamiento.getCodigo_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);
			
			log.info("AlmacenamientoDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsAlmacenamiento clsAlmacenamiento) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_almacenamiento;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAlmacenamiento.getAlmacenamiento_id());
			cs.setInt(2, clsAlmacenamiento.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(3, clsAlmacenamiento.getCosto_anual());
			cs.setInt(4, clsAlmacenamiento.getCapacidad_papeleras());
			cs.setInt(5, clsAlmacenamiento.getMetal());
			cs.setInt(6, clsAlmacenamiento.getFibra_vidrio());
			cs.setInt(7, clsAlmacenamiento.getPlastico());
			cs.setInt(8, clsAlmacenamiento.getMixto());
			cs.setString(9, clsAlmacenamiento.getDescripcion_mixto());
			cs.setDouble(10, clsAlmacenamiento.getTotal_recolectado());
			cs.setInt(11, clsAlmacenamiento.getCodigo_usuario());
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(12);

			log.info("AlmacenamientoDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
