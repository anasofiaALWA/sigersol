package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAdmFinanzas;
import pe.gob.sigersol.entities.ClsInversion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoInversion;
import pe.gob.sigersol.generales.service.InversionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/inversionX")
@RequestScoped
public class InversionRestService {

	private static Logger log = Logger.getLogger(ContenedorRestService.class.getName());
	private ClsInversion clsInversion;
	private ClsTipoInversion clsTipoInversion;
	private ClsAdmFinanzas clsAdmFinanzas;
	private InversionService inversionService;
	private List<ClsInversion> listaInversion;

	@POST
	@Path("/listarInversion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarInversion(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			inversionService = new InversionService();
			
			jsonEntradaListaInversion(paramJson);
			
			log.info("**Obtener Inversion");
			
			clsResultado = inversionService.listarInversion(clsInversion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstInversion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		inversionService = new InversionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaInversion(paramJson);
			
			for (ClsInversion clsInversion : listaInversion) {
				clsResultado = inversionService.insertar(clsInversion);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		inversionService = new InversionService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaInversion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsInversion clsInversion : listaInversion) {
			clsResultado = inversionService.actualizar(clsInversion);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	
	private void jsonEntradaInversion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> InversionRestService : jsonEntradaInversion" + jsonEntrada);

		JSONObject jsonObjectTipoInversion;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaInversion = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsInversion = new ClsInversion();
			clsTipoInversion = new ClsTipoInversion();
			clsAdmFinanzas = new ClsAdmFinanzas();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("adm_finanzas_id") != null && innerObject.get("adm_finanzas_id").toString() != "") {
				clsAdmFinanzas.setAdm_finanzas_id(Integer.parseInt(innerObject.get("adm_finanzas_id").toString()));
			}
			
			clsInversion.setAdm_finanzas(clsAdmFinanzas);
			
			if (innerObject.get("tipo_inversion") != null && innerObject.get("tipo_inversion").toString() != "") {
				jsonObjectTipoInversion  = (JSONObject) innerObject.get("tipo_inversion");
				clsTipoInversion.setTipo_inversion_id(Integer.parseInt(jsonObjectTipoInversion.get("tipo_inversion_id").toString()));
			}
			clsInversion.setTipo_inversion(clsTipoInversion);
			
			if (innerObject.get("monto_inv") != null && innerObject.get("monto_inv").toString() != "") {
				clsInversion.setMonto_inv(Integer.parseInt(innerObject.get("monto_inv").toString()));
			} else {
				clsInversion.setMonto_inv(0);
			}
			
			if (innerObject.get("fuente_financ") != null && innerObject.get("fuente_financ").toString() != "") {
				clsInversion.setFuente_financ(Integer.parseInt(innerObject.get("fuente_financ").toString()));
			} else {
				clsInversion.setFuente_financ(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsInversion.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			
			listaInversion.add(clsInversion);	
		}
			
		log.info("----------------------------------------------------------------------------------");
	}
	
	
	private void jsonEntradaListaInversion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> InversionRestService : jsonEntradaListaInversion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectInversion;
		JSONParser jsonParser = new JSONParser();
		
		clsInversion = new ClsInversion();
		clsTipoInversion = new ClsTipoInversion();
		clsAdmFinanzas = new ClsAdmFinanzas();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectInversion = (JSONObject) jsonObjectPrincipal.get("Inversion");
			
			if (jsonObjectInversion.get("adm_finanzas_id") != null && jsonObjectInversion.get("adm_finanzas_id") != "" &&
					jsonObjectInversion.get("adm_finanzas_id") != " ") {
				clsAdmFinanzas.setAdm_finanzas_id(Integer.parseInt(jsonObjectInversion.get("adm_finanzas_id").toString()));
			}
			else
				clsAdmFinanzas.setAdm_finanzas_id(-1);
			
			clsInversion.setAdm_finanzas(clsAdmFinanzas);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
