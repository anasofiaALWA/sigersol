package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsProgramaSegregacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ProgramaSegregacionDAO;

public class ProgramaSegregacionService {
	
	private static Logger log = Logger.getLogger(ProgramaSegregacionService.class);
	ProgramaSegregacionDAO programaSegregacionDAO = new ProgramaSegregacionDAO();

	public ClsResultado obtener(ClsProgramaSegregacion clsProgramaSegregacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = programaSegregacionDAO.obtener(clsProgramaSegregacion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsProgramaSegregacion clsProgramaSegregacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = programaSegregacionDAO.insertar(clsProgramaSegregacion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsProgramaSegregacion clsProgramaSegregacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = programaSegregacionDAO.actualizar(clsProgramaSegregacion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
