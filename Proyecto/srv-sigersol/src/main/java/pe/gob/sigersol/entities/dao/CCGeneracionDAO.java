package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCCGeneracion;
import pe.gob.sigersol.entities.ClsResultado;

public class CCGeneracionDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(CCGeneracionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_ins_cc_generacion = "{call SIGERSOLBL.SGR_SP_INS_CC_GENERACION(?,?,?,?,?)}";
	private final String sgr_sp_cc_calcular_tool4 = "{call SIGERSOLBL.SGR_SP_CC_CALCULAR_TOOL4(?,?,?,?,?,?)}";

	public ClsResultado insertar(ClsCCGeneracion ccGeneracion) {
		ClsResultado clsResultado = new ClsResultado();
		Integer ccGeneracionId = -1;
		con = null;
		String sql = this.sgr_sp_ins_cc_generacion;
		try {
			con = getConnection();
			con.setAutoCommit(true);
			cs = con.prepareCall(sql);
			cs.setInt(1, ccGeneracion.getDisposicionFinalId());
			cs.setInt(2, ccGeneracion.getAnioInicio_sdf());
			cs.setDouble(3, ccGeneracion.getGeneracionTotal_sdf());
			cs.setInt(4, ccGeneracion.getSesionId());
			cs.registerOutParameter(5, OracleTypes.INTEGER);

			cs.executeUpdate();
			ccGeneracionId = cs.getInt(5);

			log.info("CCGeneracionDAO >> insertar() >> resultado :" + ccGeneracionId);
			clsResultado.setObjeto(ccGeneracionId);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado calcularTool4(String ubigeoId, Integer municipalidadId, Integer ccGeneracionId,
			String condicionId,Integer anioCGRS) {
		ClsResultado clsResultado = new ClsResultado();
		Double ccTotalTool4 = 0.0;
		con = null;
		String sql = this.sgr_sp_cc_calcular_tool4;
		try {
			con = getConnection();
			con.setAutoCommit(true);
			cs = con.prepareCall(sql);
			cs.setString(1, ubigeoId);
			cs.setInt(2, municipalidadId);
			cs.setInt(3, ccGeneracionId);
			cs.setString(4, condicionId);
			cs.setInt(5, anioCGRS);
			cs.registerOutParameter(6, OracleTypes.INTEGER);

			cs.execute();
			ccTotalTool4 = cs.getDouble(6);

			log.info("CCGeneracionDAO >> calcularTool4() >> resultado :" + ccTotalTool4);
			clsResultado.setObjeto(ccTotalTool4);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 calcularTool4: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 calcularTool4: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 calcularTool4: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 calcularTool4: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
