package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsLugarAprovechamiento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.LugarAprovechamientoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/lugarAprovechamiento")
@RequestScoped
public class LugarAprovechamientoRestService {

	private static Logger log = Logger.getLogger(LugarAprovechamientoRestService.class.getName());
	private ClsLugarAprovechamiento clsLugarAprovechamiento;
	private ClsValorizacion clsValorizacion;
	private LugarAprovechamientoService lugarAprovechamientoService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		lugarAprovechamientoService = new LugarAprovechamientoService();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener Planta de Valorizacion**");		
			jsonEntradaValorizacion(paramJson);	
			clsResultado = lugarAprovechamientoService.obtener(clsLugarAprovechamiento);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"acopio\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		lugarAprovechamientoService = new LugarAprovechamientoService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaValorizacion(paramJson);
			clsResultado = lugarAprovechamientoService.insertar(clsLugarAprovechamiento);			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		lugarAprovechamientoService = new LugarAprovechamientoService();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaValorizacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = lugarAprovechamientoService.actualizar(clsLugarAprovechamiento);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizacion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizacion(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		lugarAprovechamientoService = new LugarAprovechamientoService();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaValorizacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = lugarAprovechamientoService.actualizacion(clsLugarAprovechamiento);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Valorización  
	 * 
	 * */
	private void jsonEntradaValorizacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ValorizacionRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectValorizacion;
		JSONParser jsonParser = new JSONParser();
		
		clsLugarAprovechamiento = new ClsLugarAprovechamiento();
		clsValorizacion = new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectValorizacion = (JSONObject) jsonObjectPrincipal.get("LugarAprovechamiento");
			
			if (jsonObjectValorizacion.get("lugar_aprovechamiento_id") != null && jsonObjectValorizacion.get("lugar_aprovechamiento_id") != "") {
				clsLugarAprovechamiento.setLugar_aprovechamiento_id(Integer.parseInt(jsonObjectValorizacion.get("lugar_aprovechamiento_id").toString()));
			}
			
			if (jsonObjectValorizacion.get("valorizacion_id") != null && jsonObjectValorizacion.get("valorizacion_id") != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectValorizacion.get("valorizacion_id").toString()));
			} 
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsLugarAprovechamiento.setValorizacion(clsValorizacion);
			
			if (jsonObjectValorizacion.get("adm_propia") != null && jsonObjectValorizacion.get("adm_propia") != "") {
				clsLugarAprovechamiento.setAdm_propia(Integer.parseInt(jsonObjectValorizacion.get("adm_propia").toString()));
			}
			
			if (jsonObjectValorizacion.get("tercerizado_eor") != null && jsonObjectValorizacion.get("tercerizado_eor") != "") {
				clsLugarAprovechamiento.setTercerizado_eor(Integer.parseInt(jsonObjectValorizacion.get("tercerizado_eor").toString()));
			}
			
			if (jsonObjectValorizacion.get("tercerizado_asoc_recic") != null && jsonObjectValorizacion.get("tercerizado_asoc_recic") != "") {
				clsLugarAprovechamiento.setTercerizado_asoc_recic(Integer.parseInt(jsonObjectValorizacion.get("tercerizado_asoc_recic").toString()));
			}
			
			if (jsonObjectValorizacion.get("mixto") != null && jsonObjectValorizacion.get("mixto") != "") {
				clsLugarAprovechamiento.setMixto(Integer.parseInt(jsonObjectValorizacion.get("mixto").toString()));
			}
			
			if (jsonObjectValorizacion.get("numero_recicladores") != null && jsonObjectValorizacion.get("numero_recicladores") != "") {
				clsLugarAprovechamiento.setNumero_recicladores(Integer.parseInt(jsonObjectValorizacion.get("numero_recicladores").toString()));
			}
			
			if (jsonObjectValorizacion.get("cod_usuario") != null && jsonObjectValorizacion.get("cod_usuario") != "") {
				clsLugarAprovechamiento.setCodigo_usuario(Integer.parseInt(jsonObjectValorizacion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
