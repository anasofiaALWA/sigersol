package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsResultado;

public class RecRcdOmDAO extends GenericoDAO{

	private static Logger log = Logger.getLogger(RecRcdOmDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_rec_rcd_om = "{call SIGERSOLBL.SGR_SP_LIST_SGR_REC_RCD_OM(?,?,?)}";
	private final String sgr_sp_ins_rec_rcd_om = "{call SIGERSOLBL.SGR_SP_INS_REC_RCD_OM(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_rec_rcd_om = "{call SIGERSOLBL.SGR_SP_UPD_REC_RCD_OM(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsRecRcdOm clsRecRcdOm) {
		ClsResultado clsResultado = new ClsResultado();
		ClsRecRcdOm item = new ClsRecRcdOm();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_sgr_rec_rcd_om;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecRcdOm.getCiclo_grs_id().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.DOUBLE);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);

			try {
				Double total_rcd_om = cs.getDouble(2);
				item.setRcd_om(total_rcd_om);
			} catch (Exception e) {
				
			}
			
			
			while (rs.next()) {
				try {
					item.setRec_rcd_om_id(rs.getInt("REC_RCD_OM_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCiclo_grs_id(cicloGestionResiduos);
					item.setFlag_rcd(rs.getInt("FLAG_RCD"));
					item.setAdm_serv_id(rs.getString("ADM_SERV_ID"));
					item.setCosto_anual(rs.getInt("COSTO_ANUAL"));
					item.setCantidad_residuos(rs.getInt("CANTIDAD_RESIDUOS"));
					item.setUsuarios_atendidos(rs.getInt("USUARIOS_ATENDIDOS"));
					item.setOtros_descripcion(rs.getString("OTROS_DESCRIPCION"));
					item.setOtros_antig_promedio(rs.getInt("OTROS_ANTIG_PROMEDIO"));
					item.setOtros_capacidad(rs.getInt("OTROS_CAPACIDAD"));
			        item.setOtros_recoleccion_anual(rs.getInt("OTROS_RECOLECCION_ANUAL"));
			        item.setOtros_numero_unidades(rs.getInt("OTROS_NUMERO_UNIDADES"));
			        item.setFlag_municipal(rs.getInt("FLAG_MUNICIPAL"));
			        item.setOtro_munic_adm_serv_id(rs.getString("OTRO_MUNIC_ADM_SERV_ID")); 
			        item.setOtro_munic_costo(rs.getInt("OTRO_MUNIC_COSTO"));
			        item.setDescrip_residuos_otros(rs.getString("DESCRIP_RESIDUOS_OTROS")); 
			        item.setUsuario_atendido_otros(rs.getInt("USUARIO_ATENDIDO_OTROS"));
			        item.setCantidad_otros(rs.getInt("CANTIDAD_OTROS"));
			        item.setUsuarios_rcd_om(rs.getInt("USUARIOS_RCD_OM"));
					item.setCodigo_usuario(rs.getInt("COD_USUARIO_CREACION"));
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsRecRcdOm clsRecRcdOm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_rec_rcd_om;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecRcdOm.getCiclo_grs_id().getCiclo_grs_id());
			cs.setInt(2, clsRecRcdOm.getFlag_rcd());
			cs.setString(3, clsRecRcdOm.getAdm_serv_id());
			cs.setInt(4, clsRecRcdOm.getCosto_anual());
			cs.setInt(5, clsRecRcdOm.getCantidad_residuos());
			cs.setInt(6, clsRecRcdOm.getUsuarios_atendidos());
			cs.setString(7, clsRecRcdOm.getOtros_descripcion());
			cs.setInt(8, clsRecRcdOm.getOtros_antig_promedio());
			cs.setInt(9, clsRecRcdOm.getOtros_capacidad());
			cs.setInt(10, clsRecRcdOm.getOtros_recoleccion_anual());
			cs.setInt(11, clsRecRcdOm.getOtros_numero_unidades());
			cs.setInt(12, clsRecRcdOm.getFlag_municipal());
			cs.setString(13, clsRecRcdOm.getOtro_munic_adm_serv_id());
			cs.setInt(14, clsRecRcdOm.getOtro_munic_costo());
			cs.setString(15, clsRecRcdOm.getDescrip_residuos_otros());
			cs.setInt(16, clsRecRcdOm.getUsuario_atendido_otros());
			cs.setInt(17, clsRecRcdOm.getCantidad_otros());
			cs.setInt(18, clsRecRcdOm.getUsuarios_rcd_om());
			cs.setInt(19, clsRecRcdOm.getCodigo_usuario());
			cs.registerOutParameter(20, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(20);
			
			log.info("RecRcdOmDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsRecRcdOm clsRecRcdOm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_rec_rcd_om;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecRcdOm.getRec_rcd_om_id());
			cs.setInt(2, clsRecRcdOm.getCiclo_grs_id().getCiclo_grs_id());
			cs.setInt(3, clsRecRcdOm.getFlag_rcd());
			cs.setString(4, clsRecRcdOm.getAdm_serv_id());
			cs.setInt(5, clsRecRcdOm.getCosto_anual());
			cs.setInt(6, clsRecRcdOm.getCantidad_residuos());
			cs.setInt(7, clsRecRcdOm.getUsuarios_atendidos());
			cs.setString(8, clsRecRcdOm.getOtros_descripcion());
			cs.setInt(9, clsRecRcdOm.getOtros_antig_promedio());
			cs.setInt(10, clsRecRcdOm.getOtros_capacidad());
			cs.setInt(11, clsRecRcdOm.getOtros_recoleccion_anual());
			cs.setInt(12, clsRecRcdOm.getOtros_numero_unidades());
			cs.setInt(13, clsRecRcdOm.getFlag_municipal());
			cs.setString(14, clsRecRcdOm.getOtro_munic_adm_serv_id());
			cs.setInt(15, clsRecRcdOm.getOtro_munic_costo());
			cs.setString(16, clsRecRcdOm.getDescrip_residuos_otros());
			cs.setInt(17, clsRecRcdOm.getUsuario_atendido_otros());
			cs.setInt(18, clsRecRcdOm.getCantidad_otros());
			cs.setInt(19, clsRecRcdOm.getUsuarios_rcd_om());
			cs.setInt(20, clsRecRcdOm.getCodigo_usuario());
			cs.registerOutParameter(21, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(21);

			log.info("RecRcdOmDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
