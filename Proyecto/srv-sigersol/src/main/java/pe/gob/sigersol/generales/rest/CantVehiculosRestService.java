package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCantVehiculos;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoVehiculo;
import pe.gob.sigersol.generales.service.CantVehiculosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase CantVehiculosRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de DisposicionFinal permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/cantVehiculos")
@RequestScoped
public class CantVehiculosRestService {

	private static Logger log = Logger.getLogger(CantVehiculosRestService.class.getName());
	private Integer accion = null;
	private ClsCantVehiculos clsCantVehiculos;
	private ClsTipoVehiculo clsTipoVehiculo;
	private ClsDisposicionFinalAdm clsDisposicionFinalAdm;
	private CantVehiculosService cantVehiculosService;
	private List<ClsCantVehiculos> listaCantVehiculos;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCantidadVehiculos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCantVehiculos(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			cantVehiculosService = new CantVehiculosService();
			
			jsonEntradaListaCantVehiculos(paramJson);
			
			log.info("**Obtener Cant Vehiculos**");
			
			clsResultado = cantVehiculosService.listarCantVehiculos(clsCantVehiculos);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCantVehiculos\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		cantVehiculosService = new CantVehiculosService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaCantVehiculos(paramJson);

			for (ClsCantVehiculos clsCantVehiculos : listaCantVehiculos) {
				clsResultado = cantVehiculosService.insertar(clsCantVehiculos);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		cantVehiculosService = new CantVehiculosService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCantVehiculos(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCantVehiculos clsCantVehiculos : listaCantVehiculos) {
			clsResultado = cantVehiculosService.actualizar(clsCantVehiculos);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de CantVehiculos
	 * 
	 * */
	private void jsonEntradaCantVehiculos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantVehiculosRestService :	 jsonEntradaCantVehiculos" + jsonEntrada);

		JSONObject jsonObjectCobertura;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCantVehiculos = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCantVehiculos = new ClsCantVehiculos();
			clsTipoVehiculo = new ClsTipoVehiculo();
			clsDisposicionFinalAdm = new ClsDisposicionFinalAdm();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("disposicion_final_id") != null && innerObject.get("disposicion_final_id").toString() != "") {
				clsDisposicionFinalAdm.setDisposicion_final_id(Integer.parseInt(innerObject.get("disposicion_final_id").toString()));
			}
			
			clsCantVehiculos.setDisposicion_final_adm(clsDisposicionFinalAdm);
			
			if (innerObject.get("tipoVehiculo") != null && innerObject.get("tipoVehiculo").toString() != "") {
				jsonObjectCobertura  = (JSONObject) innerObject.get("tipoVehiculo");
				clsTipoVehiculo.setTipo_vehiculo_id(Integer.parseInt(jsonObjectCobertura.get("tipo_vehiculo_id").toString()));
			}
			clsCantVehiculos.setTipoVehiculo(clsTipoVehiculo);
			
			if (innerObject.get("tiempo_parcial") != null && innerObject.get("tiempo_parcial").toString() != "") {
				clsCantVehiculos.setTiempo_parcial(Integer.parseInt(innerObject.get("tiempo_parcial").toString()));
			} else {
				clsCantVehiculos.setTiempo_parcial(0);
			}
			
			if (innerObject.get("tiempo_completo") != null && innerObject.get("tiempo_completo").toString() != "") {
				clsCantVehiculos.setTiempo_completo(Integer.parseInt(innerObject.get("tiempo_completo").toString()));
			} else {
				clsCantVehiculos.setTiempo_completo(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCantVehiculos.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCantVehiculos.add(clsCantVehiculos);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCantVehiculos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CantVehiculosRestService :	 jsonEntradaListaCantVehiculos" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectTipoManejo;
		JSONParser jsonParser = new JSONParser();
		
		clsCantVehiculos = new ClsCantVehiculos();
		clsTipoVehiculo = new ClsTipoVehiculo();
		clsDisposicionFinalAdm = new ClsDisposicionFinalAdm();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectTipoManejo = (JSONObject) jsonObjectPrincipal.get("CantVehiculos");
			
			if (jsonObjectTipoManejo.get("disposicion_final_id") != null && jsonObjectTipoManejo.get("disposicion_final_id") != "" &&
					jsonObjectTipoManejo.get("disposicion_final_id") != " ") {
				clsDisposicionFinalAdm.setDisposicion_final_id(Integer.parseInt(jsonObjectTipoManejo.get("disposicion_final_id").toString()));
			}
			else
				clsDisposicionFinalAdm.setDisposicion_final_id(-1);
			
			clsCantVehiculos.setDisposicion_final_adm(clsDisposicionFinalAdm);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
