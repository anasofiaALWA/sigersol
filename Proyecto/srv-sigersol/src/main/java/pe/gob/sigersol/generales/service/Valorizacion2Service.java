package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.entities.dao.Valorizacion2DAO;
import pe.gob.sigersol.entities.dao.ValorizacionDAO;

public class Valorizacion2Service {

	private static Logger log = Logger.getLogger(Valorizacion2Service.class);
	Valorizacion2DAO valorizacion2DAO = new Valorizacion2DAO();
	
	public ClsResultado obtener(ClsValorizacion clsValorizacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacion2DAO.obtener(clsValorizacion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsValorizacion clsValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacion2DAO.insertar(clsValorizacion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsValorizacion clsValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = valorizacion2DAO.actualizar(clsValorizacion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
