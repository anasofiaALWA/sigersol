package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CicloGestionResiduosDAO;


public class CicloGestionResiduosService {
	private static Logger log = Logger.getLogger(CicloGestionResiduosService.class);
	CicloGestionResiduosDAO cicloGestionResiduosDAO = new CicloGestionResiduosDAO();
	
	public ClsResultado obtener(){
		ClsResultado clsResultado = null;
		try {
			clsResultado = cicloGestionResiduosDAO.obtener();
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado validar(ClsCicloGestionResiduos clsCicloGestionResiduos){
		ClsResultado clsResultado = null;
		try {
			clsResultado = cicloGestionResiduosDAO.validar(clsCicloGestionResiduos);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCicloGestionResiduos clsCicloGestionResiduos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = cicloGestionResiduosDAO.insertar(clsCicloGestionResiduos);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCicloGestionResiduos clsCicloGestionResiduos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = cicloGestionResiduosDAO.actualizar(clsCicloGestionResiduos);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado getUltimoAnioCGR(String codDistrito){
		ClsResultado clsResultado=null;
		try {
			clsResultado= cicloGestionResiduosDAO.getUltimoAnioMuni(codDistrito);
		} catch (Exception e) {
			log.info("message getUltimoAnioCGR >> service >> " + e.getMessage());
		}
		
		return clsResultado;
	}
}
