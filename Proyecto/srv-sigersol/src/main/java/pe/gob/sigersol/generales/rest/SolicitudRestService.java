package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSolicitud;
import pe.gob.sigersol.generales.service.SolicitudService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/solicitudX")
@RequestScoped
public class SolicitudRestService {
	private static Logger log = Logger.getLogger(SolicitudRestService.class.getName());
	private Integer accion = null;
	private ClsSolicitud clsSolicitud;
	private SolicitudService solicitudService;

	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(@Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			solicitudService = new SolicitudService();
			
			log.info("**obtener solicitud**");
			
			clsResultado = solicitudService.obtener();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstSolicitud\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		solicitudService = new SolicitudService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaSolicitud(paramJson);

			// if (accion == 1) {
			// clsResultado = academicoService.insertar(clsAcademico);
			// } else {
			// clsResultado = academicoService.actualizar(clsAcademico);
			// }
			//
			
			clsResultado = solicitudService.insertar(clsSolicitud);
			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(@FormParam("paramJson") String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaSolicitud(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = solicitudService.actualizar(clsSolicitud);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"usuario\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	
	private void jsonEntradaSolicitud(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectSolicitud;
		JSONParser jsonParser = new JSONParser();
		
		clsSolicitud = new ClsSolicitud();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectSolicitud = (JSONObject) jsonObjectPrincipal.get("Solicitud");

			if (jsonObjectSolicitud.get("cod_departamento") != null && jsonObjectSolicitud.get("cod_departamento") != "") {
				clsSolicitud.setCodigo_ubigeo_departamento(jsonObjectSolicitud.get("cod_departamento").toString());
			}
			if (jsonObjectSolicitud.get("cod_provincia") != null && jsonObjectSolicitud.get("cod_provincia") != "") {
				clsSolicitud.setCodigo_ubigeo_provincia(jsonObjectSolicitud.get("cod_provincia").toString());
			}
			if (jsonObjectSolicitud.get("cod_distrito") != null && jsonObjectSolicitud.get("cod_distrito") != "") {
				clsSolicitud.setCodigo_ubigeo_distrito(jsonObjectSolicitud.get("cod_distrito").toString());
			}
			if (jsonObjectSolicitud.get("ruc") != null && jsonObjectSolicitud.get("ruc") != "") {
				clsSolicitud.setRuc_municipalidad(jsonObjectSolicitud.get("ruc").toString());
			}
			if (jsonObjectSolicitud.get("nombreAlcalde") != null && jsonObjectSolicitud.get("nombreAlcalde") != "") {
				clsSolicitud.setNombre_alcalde(jsonObjectSolicitud.get("nombreAlcalde").toString());
			}
			if (jsonObjectSolicitud.get("direccion") != null && jsonObjectSolicitud.get("direccion") != "") {
				clsSolicitud.setDireccion(jsonObjectSolicitud.get("direccion").toString());
			}
			if (jsonObjectSolicitud.get("telefono") != null && jsonObjectSolicitud.get("telefono") != "") {
				clsSolicitud.setTelefono(jsonObjectSolicitud.get("telefono").toString());
			}
			if (jsonObjectSolicitud.get("fax") != null && jsonObjectSolicitud.get("fax") != "") {
				clsSolicitud.setFax(jsonObjectSolicitud.get("fax").toString());
			}
			if (jsonObjectSolicitud.get("email") != null && jsonObjectSolicitud.get("email") != "") {
				clsSolicitud.setEmail(jsonObjectSolicitud.get("email").toString());
			}
			if (jsonObjectSolicitud.get("cod_usuario") != null && jsonObjectSolicitud.get("cod_usuario") != "") {
				clsSolicitud.setCodigo_usuario(Integer.parseInt(jsonObjectSolicitud.get("cod_usuario").toString()));
			}
			// if (jsonObjectAcademico.get("flagAccion") != null &&
			// jsonObjectAcademico.get("flagAccion") != "") {
			// accion =
			// Integer.parseInt(jsonObjectAcademico.get("flagAccion").toString());
			// }

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
