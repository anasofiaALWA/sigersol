package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsRecDisposicionFinal;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVehiculoConvencional;
import pe.gob.sigersol.generales.service.VehiculoConvencional3Service;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/vehiculoConvencional3")
@RequestScoped
public class VehiculoConvencional3RestService {

	private static Logger log = Logger.getLogger(VehiculoRecoleccionRestService.class.getName());
	private ClsVehiculoConvencional clsVehiculoConvencional;
	private ClsRecRcdOm clsRecRcdOm;
	private VehiculoConvencional3Service vehiculoConvencional3Service;
	private List<ClsVehiculoConvencional> listaVehiculoConvencional;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarVehiculoConvencional")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarVehiculoConvencional(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			vehiculoConvencional3Service = new VehiculoConvencional3Service();
			
			jsonEntradaListaVehiculos(paramJson);
			
			log.info("**Obtener Vehiculos**");
			
			clsResultado = vehiculoConvencional3Service.listarVehiculoConvencional(clsVehiculoConvencional);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstVehiculoConvencional3\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		vehiculoConvencional3Service = new VehiculoConvencional3Service();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaVehiculos(paramJson);
	
			for (ClsVehiculoConvencional clsVehiculoConvencional : listaVehiculoConvencional) {
				clsResultado = vehiculoConvencional3Service.insertar(clsVehiculoConvencional);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		vehiculoConvencional3Service = new VehiculoConvencional3Service();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaVehiculos2(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsVehiculoConvencional clsVehiculoConvencional : listaVehiculoConvencional) {
			clsResultado = vehiculoConvencional3Service.insertar(clsVehiculoConvencional);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaVehiculos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService : jsonEntradaVehiculos" + jsonEntrada);

		JSONObject jsonObjectTipoVehiculo;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaVehiculoConvencional = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsVehiculoConvencional = new ClsVehiculoConvencional();
			clsRecRcdOm = new ClsRecRcdOm();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("rec_rcd_om_id") != null && innerObject.get("rec_rcd_om_id").toString() != "") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(innerObject.get("rec_rcd_om_id").toString()));
			} 
			else {
				clsRecRcdOm.setRec_rcd_om_id(0);
			}
			
			clsVehiculoConvencional.setRecRcdOm(clsRecRcdOm);
					
			if (innerObject.get("placa") != null && innerObject.get("placa").toString() != "") {
				clsVehiculoConvencional.setPlaca(innerObject.get("placa").toString());
			} else{
				clsVehiculoConvencional.setPlaca("");
			}
		
			if (innerObject.get("recorrido_anual") != null && innerObject.get("recorrido_anual").toString() != "") {
				clsVehiculoConvencional.setRecorrido_anual(Integer.parseInt(innerObject.get("recorrido_anual").toString()));
			} else{
				clsVehiculoConvencional.setRecorrido_anual(0);
			}
			
			if (innerObject.get("tipo_vehiculo") != null && innerObject.get("tipo_vehiculo").toString() != "") {
				clsVehiculoConvencional.setTipo_vehiculo(innerObject.get("tipo_vehiculo").toString());
			} else{
				clsVehiculoConvencional.setTipo_vehiculo("");
			}
			
			if (innerObject.get("anio_fabricacion") != null && innerObject.get("anio_fabricacion").toString() != "") {
				clsVehiculoConvencional.setAnio_fabricacion(Integer.parseInt(innerObject.get("anio_fabricacion").toString()));
			} else{
				clsVehiculoConvencional.setAnio_fabricacion(0);
			}
			
			if (innerObject.get("capacidad") != null && innerObject.get("capacidad").toString() != "") {
				clsVehiculoConvencional.setCapacidad(Double.valueOf(innerObject.get("capacidad").toString()));
			} else{
				clsVehiculoConvencional.setCapacidad(0.0);
			}
			
			if (innerObject.get("tipo_combustible") != null && innerObject.get("tipo_combustible").toString() != "") {
				clsVehiculoConvencional.setTipo_combustible(Integer.parseInt(innerObject.get("tipo_combustible").toString()));
			} else{
				clsVehiculoConvencional.setTipo_combustible(0);
			}
			
			if (innerObject.get("cantidad_combustible") != null && innerObject.get("cantidad_combustible").toString() != "") {
				clsVehiculoConvencional.setCantidad_combustible(Double.valueOf(innerObject.get("cantidad_combustible").toString()));
			} else{
				clsVehiculoConvencional.setCantidad_combustible(0.0);
			}
			
			if (innerObject.get("costo_anual") != null && innerObject.get("costo_anual").toString() != "") {
				clsVehiculoConvencional.setCosto_anual(Integer.parseInt(innerObject.get("costo_anual").toString()));
			} else{
				clsVehiculoConvencional.setCosto_anual(0);
			}
			
			if (innerObject.get("rendimiento_anual") != null && innerObject.get("rendimiento_anual").toString() != "") {
				clsVehiculoConvencional.setRendimiento_anual(Double.valueOf(innerObject.get("rendimiento_anual").toString()));
			} else{
				clsVehiculoConvencional.setRendimiento_anual(0.0);
			}
			
			if (innerObject.get("capacidad_viaje") != null && innerObject.get("capacidad_viaje").toString() != "") {
				clsVehiculoConvencional.setCapacidad_viaje(Double.valueOf(innerObject.get("capacidad_viaje").toString()));
			} else{
				clsVehiculoConvencional.setCapacidad_viaje(0.0);
			}
			
			if (innerObject.get("efectividad") != null && innerObject.get("efectividad").toString() != "") {
				clsVehiculoConvencional.setEfectividad(Integer.parseInt(innerObject.get("efectividad").toString()));
			} else{
				clsVehiculoConvencional.setEfectividad(0);
			}
			
			if (innerObject.get("promedio_viajes") != null && innerObject.get("promedio_viajes").toString() != "") {
				clsVehiculoConvencional.setPromedio_viajes(Double.valueOf(innerObject.get("promedio_viajes").toString()));
			} else{
				clsVehiculoConvencional.setPromedio_viajes(0.0);
			}
			
			if (innerObject.get("promedio_turno") != null && innerObject.get("promedio_turno").toString() != "") {
				clsVehiculoConvencional.setPromedio_turno(Double.valueOf(innerObject.get("promedio_turno").toString()));
			} else{
				clsVehiculoConvencional.setPromedio_turno(0.0);
			}
			
			if (innerObject.get("dias_trabajados") != null && innerObject.get("dias_trabajados").toString() != "") {
				clsVehiculoConvencional.setDias_trabajados(Integer.parseInt(innerObject.get("dias_trabajados").toString()));
			} else{
				clsVehiculoConvencional.setDias_trabajados(0);
			}
			
			if (innerObject.get("recoleccion_media") != null && innerObject.get("recoleccion_media").toString() != "") {
				clsVehiculoConvencional.setRecoleccion_media(Double.valueOf(innerObject.get("recoleccion_media").toString()));
			} else{
				clsVehiculoConvencional.setRecoleccion_media(0.0);
			}
			
			if (innerObject.get("recoleccion_anual") != null && innerObject.get("recoleccion_anual").toString() != "") {
				clsVehiculoConvencional.setRecoleccion_anual(Double.valueOf(innerObject.get("recoleccion_anual").toString()));
			} else{
				clsVehiculoConvencional.setRecoleccion_anual(0.0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsVehiculoConvencional.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaVehiculoConvencional.add(clsVehiculoConvencional);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaVehiculos2(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService : jsonEntradaVehiculos" + jsonEntrada);

		JSONObject jsonObjectTipoVehiculo;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaVehiculoConvencional = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsVehiculoConvencional = new ClsVehiculoConvencional();
			clsRecRcdOm = new ClsRecRcdOm();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("rec_rcd_om_id") != null && innerObject.get("rec_rcd_om_id").toString() != "") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(innerObject.get("rec_rcd_om_id").toString()));
			} 
			else {
				clsRecRcdOm.setRec_rcd_om_id(0);
			}
			
			clsVehiculoConvencional.setRecRcdOm(clsRecRcdOm);
					
			if (innerObject.get("placa3") != null && innerObject.get("placa3").toString() != "") {
				clsVehiculoConvencional.setPlaca(innerObject.get("placa3").toString());
			} else{
				clsVehiculoConvencional.setPlaca("");
			}
		
			if (innerObject.get("recorrido_anual3") != null && innerObject.get("recorrido_anual3").toString() != "") {
				clsVehiculoConvencional.setRecorrido_anual(Integer.parseInt(innerObject.get("recorrido_anual3").toString()));
			} else{
				clsVehiculoConvencional.setRecorrido_anual(0);
			}
			
			if (innerObject.get("tpv") != null && innerObject.get("tpv").toString() != "") {
				clsVehiculoConvencional.setTipo_vehiculo(innerObject.get("tpv").toString());
			} else{
				clsVehiculoConvencional.setTipo_vehiculo("");
			}
			
			if (innerObject.get("anio_fabricacion3") != null && innerObject.get("anio_fabricacion3").toString() != "") {
				clsVehiculoConvencional.setAnio_fabricacion(Integer.parseInt(innerObject.get("anio_fabricacion3").toString()));
			} else{
				clsVehiculoConvencional.setAnio_fabricacion(0);
			}
			
			if (innerObject.get("capacidad3") != null && innerObject.get("capacidad3").toString() != "") {
				clsVehiculoConvencional.setCapacidad(Double.valueOf(innerObject.get("capacidad3").toString()));
			} else{
				clsVehiculoConvencional.setCapacidad(0.0);
			}
			
			if (innerObject.get("tipo_combustible3") != null && innerObject.get("tipo_combustible3").toString() != "") {
				clsVehiculoConvencional.setTipo_combustible(Integer.parseInt(innerObject.get("tipo_combustible3").toString()));
			} else{
				clsVehiculoConvencional.setTipo_combustible(0);
			}
			
			if (innerObject.get("cantidad_combustible3") != null && innerObject.get("cantidad_combustible3").toString() != "") {
				clsVehiculoConvencional.setCantidad_combustible(Double.valueOf(innerObject.get("cantidad_combustible3").toString()));
			} else{
				clsVehiculoConvencional.setCantidad_combustible(0.0);
			}
			
			if (innerObject.get("costo_anual3") != null && innerObject.get("costo_anual3").toString() != "") {
				clsVehiculoConvencional.setCosto_anual(Integer.parseInt(innerObject.get("costo_anual3").toString()));
			} else{
				clsVehiculoConvencional.setCosto_anual(0);
			}
			
			if (innerObject.get("rendimiento_anual3") != null && innerObject.get("rendimiento_anual3").toString() != "") {
				clsVehiculoConvencional.setRendimiento_anual(Double.valueOf(innerObject.get("rendimiento_anual3").toString()));
			} else{
				clsVehiculoConvencional.setRendimiento_anual(0.0);
			}
			
			if (innerObject.get("capacidad_viaje3") != null && innerObject.get("capacidad_viaje3").toString() != "") {
				clsVehiculoConvencional.setCapacidad_viaje(Double.valueOf(innerObject.get("capacidad_viaje3").toString()));
			} else{
				clsVehiculoConvencional.setCapacidad_viaje(0.0);
			}
			
			if (innerObject.get("efectividad3") != null && innerObject.get("efectividad3").toString() != "") {
				clsVehiculoConvencional.setEfectividad(Integer.parseInt(innerObject.get("efectividad3").toString()));
			} else{
				clsVehiculoConvencional.setEfectividad(0);
			}
			
			if (innerObject.get("promedio_viajes3") != null && innerObject.get("promedio_viajes3").toString() != "") {
				clsVehiculoConvencional.setPromedio_viajes(Double.valueOf(innerObject.get("promedio_viajes3").toString()));
			} else{
				clsVehiculoConvencional.setPromedio_viajes(0.0);
			}
			
			if (innerObject.get("promedio_turno3") != null && innerObject.get("promedio_turno3").toString() != "") {
				clsVehiculoConvencional.setPromedio_turno(Double.valueOf(innerObject.get("promedio_turno3").toString()));
			} else{
				clsVehiculoConvencional.setPromedio_turno(0.0);
			}
			
			if (innerObject.get("dias_trabajados3") != null && innerObject.get("dias_trabajados3").toString() != "") {
				clsVehiculoConvencional.setDias_trabajados(Integer.parseInt(innerObject.get("dias_trabajados3").toString()));
			} else{
				clsVehiculoConvencional.setDias_trabajados(0);
			}
			
			if (innerObject.get("recoleccion_media3") != null && innerObject.get("recoleccion_media3").toString() != "") {
				clsVehiculoConvencional.setRecoleccion_media(Double.valueOf(innerObject.get("recoleccion_media3").toString()));
			} else{
				clsVehiculoConvencional.setRecoleccion_media(0.0);
			}
			
			if (innerObject.get("recoleccion_anual3") != null && innerObject.get("recoleccion_anual3").toString() != "") {
				clsVehiculoConvencional.setRecoleccion_anual(Double.valueOf(innerObject.get("recoleccion_anual3").toString()));
			} else{
				clsVehiculoConvencional.setRecoleccion_anual(0.0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsVehiculoConvencional.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaVehiculoConvencional.add(clsVehiculoConvencional);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaVehiculos(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> VehiculoRecoleccionRestService : jsonEntradaVehiculoRecoleccion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectVehiculos;
		JSONParser jsonParser = new JSONParser();
		
		clsVehiculoConvencional = new ClsVehiculoConvencional();
		clsRecRcdOm = new ClsRecRcdOm();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectVehiculos = (JSONObject) jsonObjectPrincipal.get("VehiculoConvencional3");
			
			if (jsonObjectVehiculos.get("rec_rcd_om_id") != null && jsonObjectVehiculos.get("rec_rcd_om_id") != "" &&
					jsonObjectVehiculos.get("rec_rcd_om_id") != " ") {
				clsRecRcdOm.setRec_rcd_om_id(Integer.parseInt(jsonObjectVehiculos.get("rec_rcd_om_id").toString()));
			}
			else
				clsRecRcdOm.setRec_rcd_om_id(-1);
			
			clsVehiculoConvencional.setRecRcdOm(clsRecRcdOm);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
