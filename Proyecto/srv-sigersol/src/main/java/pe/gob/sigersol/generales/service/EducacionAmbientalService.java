package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsEducacionAmbiental;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.EducacionAmbientalDAO;


public class EducacionAmbientalService {
	private static Logger log = Logger.getLogger(EducacionAmbientalService.class);
	EducacionAmbientalDAO educacionAmbientalDAO = new EducacionAmbientalDAO();
	
	
	public ClsResultado obtener(ClsEducacionAmbiental clsEducacionAmbiental){
		ClsResultado clsResultado = null;
		try {
			clsResultado = educacionAmbientalDAO.obtener(clsEducacionAmbiental);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsEducacionAmbiental clsEducacionAmbiental) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = educacionAmbientalDAO.insertar(clsEducacionAmbiental);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsEducacionAmbiental clsEducacionAmbiental) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = educacionAmbientalDAO.actualizar(clsEducacionAmbiental);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
