package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;
import pe.gob.sigersol.generales.service.DisposicionFinal2Service;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/disposicion2")
@RequestScoped
public class DisposicionFinal2RestService {

	private static Logger log = Logger.getLogger(DisposicionFinalRestService.class.getName());
	private ClsDisposicionFinal clsDisposicionFinal;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private ClsSitioDisposicion clsSitioDisposicion;
	private DisposicionFinal2Service disposicionFinal2Service;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		disposicionFinal2Service = new DisposicionFinal2Service();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener disposicion**");
			
			jsonEntradaDisposicion(paramJson);
			
			clsResultado = disposicionFinal2Service.obtener(clsDisposicionFinal);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"disposicion2\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		disposicionFinal2Service = new DisposicionFinal2Service();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaDisposicion(paramJson);
			clsResultado = disposicionFinal2Service.insertar(clsDisposicionFinal);	
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		disposicionFinal2Service = new DisposicionFinal2Service();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaDisposicion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = disposicionFinal2Service.actualizar(clsDisposicionFinal);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de DisposicionFinal  
	 * 
	 * */
	private void jsonEntradaDisposicion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> DisposicionFinalRestService :	 jsonEntradaDisposicion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectGeneracion;
		JSONParser jsonParser = new JSONParser();
		
		clsDisposicionFinal = new ClsDisposicionFinal();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		clsSitioDisposicion = new ClsSitioDisposicion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectGeneracion = (JSONObject) jsonObjectPrincipal.get("Disposicion2");
			
			if (jsonObjectGeneracion.get("disposicion_final_id") != null && jsonObjectGeneracion.get("disposicion_final_id") != "") {
				clsDisposicionFinal.setDisposicion_final_id(Integer.parseInt(jsonObjectGeneracion.get("disposicion_final_id").toString()));
			}
			
			if (jsonObjectGeneracion.get("ciclo_grs_id") != null && jsonObjectGeneracion.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectGeneracion.get("ciclo_grs_id").toString()));
			} 
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsDisposicionFinal.setCicloGestionResiduos(clsCicloGestionResiduos);
			
			if (jsonObjectGeneracion.get("selec_degradada") != null && jsonObjectGeneracion.get("selec_degradada") != "") {
				clsSitioDisposicion
						.setSitio_disposicion_id(Integer.parseInt(jsonObjectGeneracion.get("selec_degradada").toString()));
			} else
				clsSitioDisposicion.setSitio_disposicion_id(-1);

			clsDisposicionFinal.setSitioDisposicion(clsSitioDisposicion);
			
			if (jsonObjectGeneracion.get("flag_df") != null && jsonObjectGeneracion.get("flag_df") != "") {
				if(jsonObjectGeneracion.get("flag_df").toString() == "true")
					clsDisposicionFinal.setFlag_df(1);
				else
					clsDisposicionFinal.setFlag_df(0);
			}
			else {
				clsDisposicionFinal.setFlag_df(0);
			}
			
			if (jsonObjectGeneracion.get("selec_relleno") != null && jsonObjectGeneracion.get("selec_relleno") != "") {
				clsDisposicionFinal.setSelec_relleno(Integer.parseInt(jsonObjectGeneracion.get("selec_relleno").toString()));
			} else {
				clsDisposicionFinal.setSelec_relleno(-1);
			}
			
			if (jsonObjectGeneracion.get("relleno") != null && jsonObjectGeneracion.get("relleno") != "") {
				clsDisposicionFinal.setRelleno(Integer.parseInt(jsonObjectGeneracion.get("relleno").toString()));
			}
			
			if (jsonObjectGeneracion.get("resolucion") != null && jsonObjectGeneracion.get("resolucion") != "") {
				clsDisposicionFinal.setResolucion(jsonObjectGeneracion.get("resolucion").toString());
			}
			
			if (jsonObjectGeneracion.get("autoridad") != null && jsonObjectGeneracion.get("autoridad") != "") {
				clsDisposicionFinal.setAutoridad(jsonObjectGeneracion.get("autoridad").toString());
			}
			
			if (jsonObjectGeneracion.get("fecha_emision") != null && jsonObjectGeneracion.get("fecha_emision") != "") {
				clsDisposicionFinal.setFecha_emision(jsonObjectGeneracion.get("fecha_emision").toString());
			}
			
			if (jsonObjectGeneracion.get("inicio_permiso") != null && jsonObjectGeneracion.get("inicio_permiso") != "") {
				clsDisposicionFinal.setInicio_permiso(jsonObjectGeneracion.get("inicio_permiso").toString());
			}
			
			if (jsonObjectGeneracion.get("culminacion_permiso") != null && jsonObjectGeneracion.get("culminacion_permiso") != "") {
				clsDisposicionFinal.setCulminacion_permiso(jsonObjectGeneracion.get("culminacion_permiso").toString());
			}
			
			if (jsonObjectGeneracion.get("flag_df2") != null && jsonObjectGeneracion.get("flag_df2") != "") {
				if(jsonObjectGeneracion.get("flag_df2").toString() == "true")
					clsDisposicionFinal.setFlag_df2(1);
				else
					clsDisposicionFinal.setFlag_df2(0);
			}
			else {
				clsDisposicionFinal.setFlag_df2(0);
			}
			
			if (jsonObjectGeneracion.get("selec_degradada") != null && jsonObjectGeneracion.get("selec_degradada") != "") {
				clsDisposicionFinal.setSelec_degradada(Integer.parseInt(jsonObjectGeneracion.get("selec_degradada").toString()));
			} else {
				clsDisposicionFinal.setSelec_degradada(-1);
			}
			
			if (jsonObjectGeneracion.get("degradada") != null && jsonObjectGeneracion.get("degradada") != "") {
				clsDisposicionFinal.setDegradada(Integer.parseInt(jsonObjectGeneracion.get("degradada").toString()));
			}
			
			if (jsonObjectGeneracion.get("costo") != null && jsonObjectGeneracion.get("costo") != "") {
				clsDisposicionFinal.setCosto(Integer.parseInt(jsonObjectGeneracion.get("costo").toString()));
			}
			
			if (jsonObjectGeneracion.get("cantidad_colaboradores") != null && jsonObjectGeneracion.get("cantidad_colaboradores") != "") {
				clsDisposicionFinal.setCantidad_colaboradores(Integer.parseInt(jsonObjectGeneracion.get("cantidad_colaboradores").toString()));
			}
			
			if (jsonObjectGeneracion.get("costo_degradadas") != null && jsonObjectGeneracion.get("costo_degradadas") != "") {
				clsDisposicionFinal.setCosto_degradadas(Integer.parseInt(jsonObjectGeneracion.get("costo_degradadas").toString()));
			}
			
			if (jsonObjectGeneracion.get("cod_usuario") != null && jsonObjectGeneracion.get("cod_usuario") != "") {
				clsDisposicionFinal.setCodigo_usuario(Integer.parseInt(jsonObjectGeneracion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
