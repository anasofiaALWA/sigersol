package pe.gob.sigersol.entities;

public class ClsCicloGestionResiduos {
	
	private Integer ciclo_grs_id;
	private Integer municipalidad_id;
	private Integer anio;
	private Integer generacion;
	private Integer almacenamiento; 
	private Integer barrido;
	private Integer recoleccion;
	private Integer transferencia;
	private Integer valorizacion;
	private Integer disposicion_final;
	private Integer flag_recoleccion;
	private Integer reporte_firmado;
	private Integer codigo_usuario;
	
	public Integer getCiclo_grs_id() {
		return ciclo_grs_id;
	}
	public void setCiclo_grs_id(Integer ciclo_grs_id) {
		this.ciclo_grs_id = ciclo_grs_id;
	}
	public Integer getMunicipalidad_id() {
		return municipalidad_id;
	}
	public void setMunicipalidad_id(Integer municipalidad_id) {
		this.municipalidad_id = municipalidad_id;
	}
	public Integer getAnio() {
		return anio;
	}
	public void setAnio(Integer anio) {
		this.anio = anio;
	}
	public Integer getGeneracion() {
		return generacion;
	}
	public void setGeneracion(Integer generacion) {
		this.generacion = generacion;
	}
	public Integer getAlmacenamiento() {
		return almacenamiento;
	}
	public void setAlmacenamiento(Integer almacenamiento) {
		this.almacenamiento = almacenamiento;
	}
	public Integer getBarrido() {
		return barrido;
	}
	public void setBarrido(Integer barrido) {
		this.barrido = barrido;
	}
	public Integer getRecoleccion() {
		return recoleccion;
	}
	public void setRecoleccion(Integer recoleccion) {
		this.recoleccion = recoleccion;
	}
	public Integer getTransferencia() {
		return transferencia;
	}
	public void setTransferencia(Integer transferencia) {
		this.transferencia = transferencia;
	}
	public Integer getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(Integer valorizacion) {
		this.valorizacion = valorizacion;
	}
	public Integer getDisposicion_final() {
		return disposicion_final;
	}
	public void setDisposicion_final(Integer disposicion_final) {
		this.disposicion_final = disposicion_final;
	}
	public Integer getFlag_recoleccion() {
		return flag_recoleccion;
	}
	public void setFlag_recoleccion(Integer flag_recoleccion) {
		this.flag_recoleccion = flag_recoleccion;
	}
	public Integer getReporte_firmado() {
		return reporte_firmado;
	}
	public void setReporte_firmado(Integer reporte_firmado) {
		this.reporte_firmado = reporte_firmado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
