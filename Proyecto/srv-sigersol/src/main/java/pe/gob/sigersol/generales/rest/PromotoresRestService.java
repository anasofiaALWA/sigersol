package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsEducacionAmbiental;
import pe.gob.sigersol.entities.ClsPromotores;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoPromotor;
import pe.gob.sigersol.generales.service.PromotoresService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/promotoresX")
@RequestScoped
public class PromotoresRestService {

	private static Logger log = Logger.getLogger(PromotoresRestService.class.getName());
	private ClsPromotores clsPromotores;
	private ClsEducacionAmbiental clsEducacionAmbiental;
	private ClsTipoPromotor clsTipoPromotor;
	private PromotoresService promotoresService;
	private List<ClsPromotores> listaPromotores;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarPromotores")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarPromotores(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			promotoresService = new PromotoresService();
			
			jsonEntradaListaPromotores(paramJson);
			
			log.info("**Obtener Promotores**");
			
			clsResultado = promotoresService.listarPromotores(clsPromotores);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstPromotores\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		promotoresService = new PromotoresService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaPromotores(paramJson);
	
			for (ClsPromotores clsPromotores : listaPromotores) {
				clsResultado = promotoresService.insertar(clsPromotores);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		promotoresService = new PromotoresService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaPromotores(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsPromotores clsPromotores : listaPromotores) {
			clsResultado = promotoresService.insertar(clsPromotores);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaPromotores(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AccionesEducacionRestService : jsonEntradaVehiculos" + jsonEntrada);

		JSONObject jsonObjectTipoEventos;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaPromotores = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsPromotores = new ClsPromotores();
			clsTipoPromotor = new ClsTipoPromotor();
			clsEducacionAmbiental = new ClsEducacionAmbiental();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("educacion_ambiental_id") != null && innerObject.get("educacion_ambiental_id").toString() != "") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(innerObject.get("educacion_ambiental_id").toString()));
			} 
			else {
				clsEducacionAmbiental.setEducacion_ambiental_id(-1);
			}
			
			clsPromotores.setEducacion_ambiental(clsEducacionAmbiental);
			
			if (innerObject.get("tipo_promotor") != null && innerObject.get("tipo_promotor").toString() != "") {
				jsonObjectTipoEventos  = (JSONObject) innerObject.get("tipo_promotor");
				clsTipoPromotor.setTipo_promotor_id(Integer.parseInt(jsonObjectTipoEventos.get("tipo_promotor_id").toString()));
			}
			clsPromotores.setTipo_promotor(clsTipoPromotor);
			
			if (innerObject.get("promotor_hombres") != null && innerObject.get("promotor_hombres").toString() != "") {
				clsPromotores.setPromotor_hombres(Integer.parseInt(innerObject.get("promotor_hombres").toString()));
			} else{
				clsPromotores.setPromotor_hombres(0);
			}
			
			if (innerObject.get("promotor_mujeres") != null && innerObject.get("promotor_mujeres").toString() != "") {
				clsPromotores.setPromotor_mujeres(Integer.parseInt(innerObject.get("promotor_mujeres").toString()));
			} else{
				clsPromotores.setPromotor_mujeres(0);
			}
		
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsPromotores.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaPromotores.add(clsPromotores);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaPromotores(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AccionesEducacionRestService : jsonEntradaListaAccionesEducacion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectEducacion;
		JSONParser jsonParser = new JSONParser();
		
		clsPromotores = new ClsPromotores();
		clsTipoPromotor = new ClsTipoPromotor();
		clsEducacionAmbiental = new ClsEducacionAmbiental();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectEducacion = (JSONObject) jsonObjectPrincipal.get("Promotores");
			
			if (jsonObjectEducacion.get("educacion_ambiental_id") != null && jsonObjectEducacion.get("educacion_ambiental_id") != "" &&
					jsonObjectEducacion.get("educacion_ambiental_id") != " ") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(jsonObjectEducacion.get("educacion_ambiental_id").toString()));
			}
			else
				clsEducacionAmbiental.setEducacion_ambiental_id(-1);
			
			clsPromotores.setEducacion_ambiental(clsEducacionAmbiental);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
