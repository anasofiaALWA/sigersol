package pe.gob.sigersol.entities;

public class ClsTipoCombustible {

	private Integer tipo_combustible_id;
	private String nombre_tipo;
	private String medida;
	
	public Integer getTipo_combustible_id() {
		return tipo_combustible_id;
	}
	public void setTipo_combustible_id(Integer tipo_combustible_id) {
		this.tipo_combustible_id = tipo_combustible_id;
	}
	public String getNombre_tipo() {
		return nombre_tipo;
	}
	public void setNombre_tipo(String nombre_tipo) {
		this.nombre_tipo = nombre_tipo;
	}
	public String getMedida() {
		return medida;
	}
	public void setMedida(String medida) {
		this.medida = medida;
	}
	
}
