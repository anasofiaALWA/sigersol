package pe.gob.sigersol.entities;

public class ClsResiduosEspeciales {

	private Integer residuos_especiales_id;
	private ClsGeneracion generacion;
	private ClsTipoResiduosEspeciales tipo_especiales;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getResiduos_especiales_id() {
		return residuos_especiales_id;
	}
	public void setResiduos_especiales_id(Integer residuos_especiales_id) {
		this.residuos_especiales_id = residuos_especiales_id;
	}
	public ClsGeneracion getGeneracion() {
		return generacion;
	}
	public void setGeneracion(ClsGeneracion generacion) {
		this.generacion = generacion;
	}
	public ClsTipoResiduosEspeciales getTipo_especiales() {
		return tipo_especiales;
	}
	public void setTipo_especiales(ClsTipoResiduosEspeciales tipo_especiales) {
		this.tipo_especiales = tipo_especiales;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
