package pe.gob.sigersol.entities;

public class ClsCentroAcopio {

	private Integer centro_acopio_id;
	private ClsValorizacion valorizacion;
	private Integer adm_propia;
	private Integer tercerizado_eor;
	private Integer tercerizado_asoc_recic;
	private Integer mixto;
	private String zona_coordenada;
	private String norte_coordenada;
	private String sur_coordenada;
	private String division_politica;
	private String direccion_iga;
	private String referencia_iga;
	private Integer numero_recicladores;
	private Integer recic_hombres_25;
	private Integer recic_hombres_25_65;
	private Integer recic_hombres_65;
	private Integer recic_mujeres_25;
	private Integer recic_mujeres_25_65;
	private Integer recic_mujeres_65;
	private Integer codigo_usuario;
	
	public Integer getCentro_acopio_id() {
		return centro_acopio_id;
	}
	public void setCentro_acopio_id(Integer centro_acopio_id) {
		this.centro_acopio_id = centro_acopio_id;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public Integer getAdm_propia() {
		return adm_propia;
	}
	public void setAdm_propia(Integer adm_propia) {
		this.adm_propia = adm_propia;
	}
	public Integer getTercerizado_eor() {
		return tercerizado_eor;
	}
	public void setTercerizado_eor(Integer tercerizado_eor) {
		this.tercerizado_eor = tercerizado_eor;
	}
	public Integer getTercerizado_asoc_recic() {
		return tercerizado_asoc_recic;
	}
	public void setTercerizado_asoc_recic(Integer tercerizado_asoc_recic) {
		this.tercerizado_asoc_recic = tercerizado_asoc_recic;
	}
	public Integer getMixto() {
		return mixto;
	}
	public void setMixto(Integer mixto) {
		this.mixto = mixto;
	}
	public String getZona_coordenada() {
		return zona_coordenada;
	}
	public void setZona_coordenada(String zona_coordenada) {
		this.zona_coordenada = zona_coordenada;
	}
	public String getNorte_coordenada() {
		return norte_coordenada;
	}
	public void setNorte_coordenada(String norte_coordemada) {
		this.norte_coordenada = norte_coordemada;
	}
	public String getSur_coordenada() {
		return sur_coordenada;
	}
	public void setSur_coordenada(String sur_coordenada) {
		this.sur_coordenada = sur_coordenada;
	}
	public String getDivision_politica() {
		return division_politica;
	}
	public void setDivision_politica(String division_politica) {
		this.division_politica = division_politica;
	}
	public String getDireccion_iga() {
		return direccion_iga;
	}
	public void setDireccion_iga(String direccion_iga) {
		this.direccion_iga = direccion_iga;
	}
	public String getReferencia_iga() {
		return referencia_iga;
	}
	public void setReferencia_iga(String referencia_iga) {
		this.referencia_iga = referencia_iga;
	}
	public Integer getNumero_recicladores() {
		return numero_recicladores;
	}
	public void setNumero_recicladores(Integer numero_recicladores) {
		this.numero_recicladores = numero_recicladores;
	}
	public Integer getRecic_hombres_25() {
		return recic_hombres_25;
	}
	public void setRecic_hombres_25(Integer recic_hombres_25) {
		this.recic_hombres_25 = recic_hombres_25;
	}
	public Integer getRecic_hombres_25_65() {
		return recic_hombres_25_65;
	}
	public void setRecic_hombres_25_65(Integer recic_hombres_25_65) {
		this.recic_hombres_25_65 = recic_hombres_25_65;
	}
	public Integer getRecic_hombres_65() {
		return recic_hombres_65;
	}
	public void setRecic_hombres_65(Integer recic_hombres_65) {
		this.recic_hombres_65 = recic_hombres_65;
	}
	public Integer getRecic_mujeres_25() {
		return recic_mujeres_25;
	}
	public void setRecic_mujeres_25(Integer recic_mujeres_25) {
		this.recic_mujeres_25 = recic_mujeres_25;
	}
	public Integer getRecic_mujeres_25_65() {
		return recic_mujeres_25_65;
	}
	public void setRecic_mujeres_25_65(Integer recic_mujeres_25_65) {
		this.recic_mujeres_25_65 = recic_mujeres_25_65;
	}
	public Integer getRecic_mujeres_65() {
		return recic_mujeres_65;
	}
	public void setRecic_mujeres_65(Integer recic_mujeres_65) {
		this.recic_mujeres_65 = recic_mujeres_65;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
