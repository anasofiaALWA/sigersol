package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;

public class SitioDisposicionDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(OfertaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sel_coordenada = "{call SIGERSOLBL.SGR_SEL_COORDENADA(?,?)}";
	private final String sgr_sel_coordenada2 = "{call SIGERSOLBL.SGR_SEL_COORDENADA2(?,?)}";
	private final String sgr_sp_list_relleno_sanitario = "{call SIGERSOLBL.SGR_SP_LIST_RELLENO_SANITARIO(?,?)}";
	private final String sgr_sp_list_relleno_adm = "{call SIGERSOLBL.SGR_SP_LIST_RELLENO_ADM(?,?)}";
	private final String sgr_sp_list_area_degradada = "{call SIGERSOLBL.SGR_SP_LIST_AREA_DEGRADADA(?,?)}";
	private final String sgr_sp_list_area_degradada_adm = "{call SIGERSOLBL.SGR_SP_LIST_AREA_DEGRADADA_ADM(?,?)}";

	public ClsResultado listarCoordenada(ClsSitioDisposicion clsSitioDisposicion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsSitioDisposicion item = new ClsSitioDisposicion();
		con = null;
		String sql = this.sgr_sel_coordenada;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsSitioDisposicion.getSitio_disposicion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				
				try {
					item.setZona(rs.getInt("ZONA"));
					item.setCoordenada_norte(rs.getString("COORDENADA_NORTE"));
					item.setCoordenada_este(rs.getString("COORDENADA_ESTE"));
					
				} catch (Exception e) {

				}
				
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	public ClsResultado listarCoordenada2(ClsSitioDisposicion clsSitioDisposicion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsSitioDisposicion item = new ClsSitioDisposicion();
		con = null;
		String sql = this.sgr_sel_coordenada2;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsSitioDisposicion.getSitio_disposicion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				
				try {
					item.setZona(rs.getInt("ZONA"));
					item.setCoordenada_norte(rs.getString("COORDENADA_NORTE"));
					item.setCoordenada_este(rs.getString("COORDENADA_ESTE"));
					
				} catch (Exception e) {

				}
				
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	
	public ClsResultado obtener(ClsSitioDisposicion clsSitioDisposicion) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsSitioDisposicion> lista = new ArrayList<ClsSitioDisposicion>();
		con = null;
		String sql = this.sgr_sp_list_relleno_sanitario;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, clsSitioDisposicion.getUbigeo_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsSitioDisposicion item = new ClsSitioDisposicion();
				try {
					item.setSitio_disposicion_id(rs.getInt("SITIO_DISPOSICION_ID"));
					item.setNombre_sitio(rs.getString("NOMBRE_SITIO"));
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	public ClsResultado obtenerAdm(ClsSitioDisposicion clsSitioDisposicion) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsSitioDisposicion> lista = new ArrayList<ClsSitioDisposicion>();
		con = null;
		String sql = this.sgr_sp_list_relleno_adm;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, clsSitioDisposicion.getUbigeo_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsSitioDisposicion item = new ClsSitioDisposicion();
				try {
					item.setSitio_disposicion_id(rs.getInt("SITIO_DISPOSICION_ID"));
					item.setNombre_sitio(rs.getString("NOMBRE_SITIO"));
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	public ClsResultado listar(ClsSitioDisposicion clsSitioDisposicion) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsSitioDisposicion> lista = new ArrayList<ClsSitioDisposicion>();
		con = null;
		String sql = this.sgr_sp_list_area_degradada;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, clsSitioDisposicion.getUbigeo_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsSitioDisposicion item = new ClsSitioDisposicion();
				try {
					item.setSitio_disposicion_id(rs.getInt("SITIO_DISPOSICION_ID"));
					item.setNombre_sitio(rs.getString("NOMBRE_SITIO"));
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	public ClsResultado listarAdm(ClsSitioDisposicion clsSitioDisposicion) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsSitioDisposicion> lista = new ArrayList<ClsSitioDisposicion>();
		con = null;
		String sql = this.sgr_sp_list_area_degradada_adm;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, clsSitioDisposicion.getUbigeo_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsSitioDisposicion item = new ClsSitioDisposicion();
				try {
					item.setSitio_disposicion_id(rs.getInt("SITIO_DISPOSICION_ID"));
					item.setNombre_sitio(rs.getString("NOMBRE_SITIO"));
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
	
}
