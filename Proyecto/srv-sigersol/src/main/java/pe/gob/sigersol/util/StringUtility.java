package pe.gob.sigersol.util;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.jboss.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class StringUtility {
	private static final Logger log = Logger.getLogger(StringUtility.class);

	private static final char[] CONSTS_HEX = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
			'e', 'f' };

	/**
	 * Función que crea el MD5 a partir de un string
	 * 
	 * @param stringAEncriptar
	 * @return
	 */
	public static String toMD5(String stringAEncriptar) {
		// System.out.println("******************* StringUtility: " +
		// "toMD5");
		try {
			MessageDigest msgd = MessageDigest.getInstance("MD5");
			byte[] bytes = msgd.digest(stringAEncriptar.getBytes());
			StringBuilder strbCadenaMD5 = new StringBuilder(2 * bytes.length);
			for (int i = 0; i < bytes.length; i++) {
				int bajo = (int) (bytes[i] & 0x0f);
				int alto = (int) ((bytes[i] & 0xf0) >> 4);
				strbCadenaMD5.append(CONSTS_HEX[alto]);
				strbCadenaMD5.append(CONSTS_HEX[bajo]);
			}
			return strbCadenaMD5.toString();
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

	public static String generateRamdonString() {
		// System.out.println("******************* StringUtility: " +
		// "generateRamdonString");
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}

	public static ObjectNode parseJsonString(String src) {

		// System.out.println("******************* StringUtility: " +
		// "parseJsonString");
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode event = null;
		try {
			event = mapper.readValue(src, ObjectNode.class);
		} catch (JsonParseException e) {
			log.error("ERROR! Parse exception: ", e.getCause());
		} catch (JsonMappingException e) {
			log.error("ERROR! Json mapping exception: ", e.getCause());
		} catch (IOException e) {
			log.error("ERROR! IO exception: ", e.getCause());
		}
		return event;
	}

	public static String dateFormat(Date date) {
		// System.out.println("******************* StringUtility: "+
		// "dateFormat");
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		return dateFormat.format(date);
	}

	public static boolean isEmpty(String obj) {
		// System.out.println("******************* StringUtility: " +
		// "isEmpty");
		if ((obj == null) || (obj.trim().length() == 0)) {
			return true;
		} else {
			return false;
		}
	}

	public static String trimEmptyLines(String obj) {
		// System.out.println("******************* StringUtility: "+
		// "trimEmptyLines");
		StringBuilder result = new StringBuilder("");
		if ((obj != null)) {
			if (obj.contains("\n")) {
				String[] lines = obj.split("\n");
				boolean first = true;
				for (String line : lines) {
					log.debug("trimEmptyLines line:" + line);
					if (!first) {
						line += "\n";
					} else {
						first = false;
					}

					if (StringUtils.isNotBlank(line)) {
						result.append(line.trim());
					}

				}
				return result.toString();
			} else {
				return obj;
			}
		} else {
			return result.toString();
		}
	}

	public List<Integer> separateCommaList(String lista) {

		// System.out.println("******************* StringUtility: " +
		// "separateCommaList");
		List<Integer> nuevaLista = new ArrayList<Integer>();

		lista = lista.replaceAll("\\s+", "");
		StringTokenizer tokens = new StringTokenizer(lista, ",");
		while (tokens.hasMoreTokens()) {
			nuevaLista.add(Integer.parseInt(tokens.nextToken()));
		}
		return nuevaLista;
	}

	public static String toSeparatedCommaList(List<Integer> lista) {
		// System.out.println("******************* StringUtility: " +
		// "toSeparatedCommaList");
		StringBuilder neoLista = new StringBuilder();
		for (Integer numero : lista) {
			neoLista.append(numero.toString()).append(",");
		}

		return neoLista.substring(0, neoLista.length() - 1);
	}

	public static String decimalToString(float numero) {
		// Formato de Número
		DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
		simbolo.setDecimalSeparator('.');
		simbolo.setGroupingSeparator(',');
		DecimalFormat df = new DecimalFormat("###,##0.00", simbolo);

		String cadena = "0.00";
		try {
			cadena = String.valueOf(df.format(numero));
		} catch (Exception e) {
			log.error("ERROR! al convertir numero " + e);
		}

		return cadena;
	}

	public static String intToString(float numero) {
		DecimalFormat decimalFormat = new DecimalFormat("0");
		String cadena = "0";
		try {
			cadena = String.valueOf(decimalFormat.format(numero));
		} catch (Exception e) {
			log.error("ERROR! al convertir numero " + e);
		}

		return cadena;
	}
	/*
	 * public static Map<String, String> arrayToMap(String[] l1, String[] l2) {
	 * Map<String, String> lp = new HashMap<String, String>(); try { for (int i
	 * = 0; i < l1.length; i++) { lp.put(l2[i], l1[i]); } } catch (Exception e)
	 * { log.error("ERROR! al convertir String[] a Map<String, String>"); }
	 * 
	 * return lp;
	 * 
	 * }
	 */

	public static Timestamp convertStringDateToTimestamp(String date) {
		Timestamp timestamp = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date parsedDate = dateFormat.parse(date);
			timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return timestamp;
	}

	public static String convertToJSON(Object object) {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create();
		String representacionJSON = gson.toJson(object);
		return representacionJSON;
	}
}
