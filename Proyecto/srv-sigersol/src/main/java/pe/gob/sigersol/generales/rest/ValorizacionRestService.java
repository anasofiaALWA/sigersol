package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.ValorizacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase ValorizacionRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de Valorizacion, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/valorizacionX")
@RequestScoped
public class ValorizacionRestService {
	
	private static Logger log = Logger.getLogger(ValorizacionRestService.class.getName());
	private ClsValorizacion clsValorizacion;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private ValorizacionService valorizacionService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		valorizacionService = new ValorizacionService();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener generacion**");		
			jsonEntradaValorizacion(paramJson);	
			clsResultado = valorizacionService.obtener(clsValorizacion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"valorizacion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		valorizacionService = new ValorizacionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaValorizacion(paramJson);
			clsResultado = valorizacionService.insertar(clsValorizacion);			
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		valorizacionService = new ValorizacionService();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaValorizacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = valorizacionService.actualizar(clsValorizacion);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Valorización  
	 * 
	 * */
	private void jsonEntradaValorizacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ValorizacionRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectValorizacion;
		JSONParser jsonParser = new JSONParser();
		
		clsValorizacion = new ClsValorizacion();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectValorizacion = (JSONObject) jsonObjectPrincipal.get("Valorizacion");
			
			if (jsonObjectValorizacion.get("valorizacion_id") != null && jsonObjectValorizacion.get("valorizacion_id") != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectValorizacion.get("valorizacion_id").toString()));
			}
			
			if (jsonObjectValorizacion.get("ciclo_grs_id") != null && jsonObjectValorizacion.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectValorizacion.get("ciclo_grs_id").toString()));
			} 
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsValorizacion.setCicloGestionResiduos(clsCicloGestionResiduos);
			
			if (jsonObjectValorizacion.get("flag_acopio") != null && jsonObjectValorizacion.get("flag_acopio") != "") {
				clsValorizacion.setFlag_acopio(Integer.parseInt(jsonObjectValorizacion.get("flag_acopio").toString()));
			}
			
			if (jsonObjectValorizacion.get("flag_acopio2") != null && jsonObjectValorizacion.get("flag_acopio2") != "") {
				clsValorizacion.setFlag_acopio2(Integer.parseInt(jsonObjectValorizacion.get("flag_acopio2").toString()));
			}
			
			if (jsonObjectValorizacion.get("flag_valorizacion") != null && jsonObjectValorizacion.get("flag_valorizacion") != "") {
				clsValorizacion.setFlag_valorizacion(Integer.parseInt(jsonObjectValorizacion.get("flag_valorizacion").toString()));
			}
			
			if (jsonObjectValorizacion.get("flag_valorizacion2") != null && jsonObjectValorizacion.get("flag_valorizacion2") != "") {
				clsValorizacion.setFlag_valorizacion2(Integer.parseInt(jsonObjectValorizacion.get("flag_valorizacion2").toString()));
			}
			
			if (jsonObjectValorizacion.get("numero_acopio") != null && jsonObjectValorizacion.get("numero_acopio") != "") {
				clsValorizacion.setNumero_acopio(Integer.parseInt(jsonObjectValorizacion.get("numero_acopio").toString()));
			}
			
			if (jsonObjectValorizacion.get("numero_valorizacion") != null && jsonObjectValorizacion.get("numero_valorizacion") != "") {
				clsValorizacion.setNumero_valorizacion(Integer.parseInt(jsonObjectValorizacion.get("numero_valorizacion").toString()));
			}

			if (jsonObjectValorizacion.get("residuo_inorganico_otro") != null && jsonObjectValorizacion.get("residuo_inorganico_otro") != "") {
				clsValorizacion.setResiduo_inorganico_otro(jsonObjectValorizacion.get("residuo_inorganico_otro").toString());
			}
			else {
				clsValorizacion.setResiduo_inorganico_otro("");
			}
			
			if (jsonObjectValorizacion.get("enero_otro") != null && jsonObjectValorizacion.get("enero_otro") != "") {
				clsValorizacion.setEnero_otro(Integer.parseInt(jsonObjectValorizacion.get("enero_otro").toString()));
			}
			else{
				clsValorizacion.setEnero_otro(0);
			}
			
			if (jsonObjectValorizacion.get("febrero_otro") != null && jsonObjectValorizacion.get("febrero_otro") != "") {
				clsValorizacion.setFebrero_otro(Integer.parseInt(jsonObjectValorizacion.get("febrero_otro").toString()));
			}
			else{
				clsValorizacion.setFebrero_otro(0);
			}
			
			if (jsonObjectValorizacion.get("marzo_otro") != null && jsonObjectValorizacion.get("marzo_otro") != "") {
				clsValorizacion.setMarzo_otro(Integer.parseInt(jsonObjectValorizacion.get("marzo_otro").toString()));
			}
			else{
				clsValorizacion.setMarzo_otro(0);
			}
			
			if (jsonObjectValorizacion.get("abril_otro") != null && jsonObjectValorizacion.get("abril_otro") != "") {
				clsValorizacion.setAbril_otro(Integer.parseInt(jsonObjectValorizacion.get("abril_otro").toString()));
			}
			else{
				clsValorizacion.setAbril_otro(0);
			}
			
			if (jsonObjectValorizacion.get("mayo_otro") != null && jsonObjectValorizacion.get("mayo_otro") != "") {
				clsValorizacion.setMayo_otro(Integer.parseInt(jsonObjectValorizacion.get("mayo_otro").toString()));
			}
			else{
				clsValorizacion.setMayo_otro(0);
			}
			
			if (jsonObjectValorizacion.get("junio_otro") != null && jsonObjectValorizacion.get("junio_otro") != "") {
				clsValorizacion.setJunio_otro(Integer.parseInt(jsonObjectValorizacion.get("junio_otro").toString()));
			}
			else{
				clsValorizacion.setJunio_otro(0);
			}
			
			if (jsonObjectValorizacion.get("julio_otro") != null && jsonObjectValorizacion.get("julio_otro") != "") {
				clsValorizacion.setJulio_otro(Integer.parseInt(jsonObjectValorizacion.get("julio_otro").toString()));
			}	
			else{
				clsValorizacion.setJulio_otro(0);
			}
			
			if (jsonObjectValorizacion.get("agosto_otro") != null && jsonObjectValorizacion.get("agosto_otro") != "") {
				clsValorizacion.setAgosto_otro(Integer.parseInt(jsonObjectValorizacion.get("agosto_otro").toString()));
			}
			else{
				clsValorizacion.setAgosto_otro(0);
			}
			
			if (jsonObjectValorizacion.get("setiembre_otro") != null && jsonObjectValorizacion.get("setiembre_otro") != "") {
				clsValorizacion.setSetiembre_otro(Integer.parseInt(jsonObjectValorizacion.get("setiembre_otro").toString()));
			}
			else{
				clsValorizacion.setSetiembre_otro(0);
			}
			
			if (jsonObjectValorizacion.get("octubre_otro") != null && jsonObjectValorizacion.get("octubre_otro") != "") {
				clsValorizacion.setOctubre_otro(Integer.parseInt(jsonObjectValorizacion.get("octubre_otro").toString()));
			}
			else{
				clsValorizacion.setOctubre_otro(0);
			}
			
			if (jsonObjectValorizacion.get("noviembre_otro") != null && jsonObjectValorizacion.get("noviembre_otro") != "") {
				clsValorizacion.setNoviembre_otro(Integer.parseInt(jsonObjectValorizacion.get("noviembre_otro").toString()));
			}
			else{
				clsValorizacion.setNoviembre_otro(0);
			}
			
			if (jsonObjectValorizacion.get("diciembre_otro") != null && jsonObjectValorizacion.get("diciembre_otro") != "") {
				clsValorizacion.setDiciembre_otro(Integer.parseInt(jsonObjectValorizacion.get("diciembre_otro").toString()));
			}
			else{
				clsValorizacion.setDiciembre_otro(0);
			}
			
			if (jsonObjectValorizacion.get("compostaje") != null && jsonObjectValorizacion.get("compostaje") != "") {
				if(jsonObjectValorizacion.get("compostaje").toString() == "true")
					clsValorizacion.setCompostaje(1);
				else
					clsValorizacion.setCompostaje(0);
			}
			else {
				clsValorizacion.setCompostaje(0);
			}
			
			if (jsonObjectValorizacion.get("lombricultura") != null && jsonObjectValorizacion.get("lombricultura") != "") {
				if(jsonObjectValorizacion.get("lombricultura").toString() == "true")
					clsValorizacion.setLombricultura(1);
				else
					clsValorizacion.setLombricultura(0);
			}
			else {
				clsValorizacion.setLombricultura(0);
			}
			
			if (jsonObjectValorizacion.get("biodigestion") != null && jsonObjectValorizacion.get("biodigestion") != "") {
				if(jsonObjectValorizacion.get("biodigestion").toString() == "true")
					clsValorizacion.setBiodigestion(1);
				else
					clsValorizacion.setBiodigestion(0);
			}
			else {
				clsValorizacion.setBiodigestion(0);
			}
			
			if (jsonObjectValorizacion.get("descrip_otros") != null && jsonObjectValorizacion.get("descrip_otros") != "") {
				clsValorizacion.setDescrip_otros(jsonObjectValorizacion.get("descrip_otros").toString());
			}
			else {
				clsValorizacion.setDescrip_otros("");
			}
			
			if (jsonObjectValorizacion.get("otros_actividades") != null && jsonObjectValorizacion.get("otros_actividades") != "") {
				if(jsonObjectValorizacion.get("otros_actividades").toString() == "true")
					clsValorizacion.setOtros_actividades(1);
				else
					clsValorizacion.setOtros_actividades(0);
			}
			else {
				clsValorizacion.setOtros_actividades(0);
			}
			
			
			if (jsonObjectValorizacion.get("descrip_productos") != null && jsonObjectValorizacion.get("descrip_productos") != "") {
				clsValorizacion.setDescrip_productos(jsonObjectValorizacion.get("descrip_productos").toString());
			}
			else {
				clsValorizacion.setDescrip_productos("");
			}
			
			if (jsonObjectValorizacion.get("cantidad_productos") != null && jsonObjectValorizacion.get("cantidad_productos") != "") {
				clsValorizacion.setCantidad_productos(Integer.parseInt(jsonObjectValorizacion.get("cantidad_productos").toString()));
			}
			else {
				clsValorizacion.setCantidad_productos(0);
			}
			
			if (jsonObjectValorizacion.get("numero_ciclos") != null && jsonObjectValorizacion.get("numero_ciclos") != "") {
				clsValorizacion.setNumero_ciclos(Integer.parseInt(jsonObjectValorizacion.get("numero_ciclos").toString()));
			}
			else {
				clsValorizacion.setNumero_ciclos(0);
			}
			
			if (jsonObjectValorizacion.get("cantidad_residuos") != null && jsonObjectValorizacion.get("cantidad_residuos") != "") {
				clsValorizacion.setCantidad_residuos(Integer.parseInt(jsonObjectValorizacion.get("cantidad_residuos").toString()));
			}
			else {
				clsValorizacion.setCantidad_residuos(0);
			}
			
			if (jsonObjectValorizacion.get("cod_usuario") != null && jsonObjectValorizacion.get("cod_usuario") != "") {
				clsValorizacion.setCodigo_usuario(Integer.parseInt(jsonObjectValorizacion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
