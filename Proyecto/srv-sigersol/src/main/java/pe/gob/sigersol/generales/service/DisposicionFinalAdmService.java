package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.DisposicionFinalAdmDAO;
import pe.gob.sigersol.entities.dao.DisposicionFinalDAO;

public class DisposicionFinalAdmService {

	private static Logger log = Logger.getLogger(DisposicionFinalAdmService.class);
	DisposicionFinalAdmDAO disposicionFinalAdmDAO = new DisposicionFinalAdmDAO();
	
	public ClsResultado obtener(ClsDisposicionFinalAdm clsDisposicionFinalAdm){
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinalAdmDAO.obtener(clsDisposicionFinalAdm);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinalAdmDAO.insertar(clsDisposicionFinalAdm);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinalAdmDAO.actualizar(clsDisposicionFinalAdm);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
