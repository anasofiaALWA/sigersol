package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUnicodeParametro;

/**
 * @author
 */

public class UnicodeParametroDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(UnicodeParametroDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String ep_am_usp_sel_list_unicodeparametros = "{call COPEREBL.EP_AM_SP_SEL_LIST_PARAMETROS(?,?)}";
	private final String ep_am_sp_valida_periodo = "{call COPEREBL.EP_AM_SP_VALIDA_PERIODO(?,?)}";
	private final String ep_mg_sp_upd_est_periodo_vigente = "{call COPEREBL.EP_MG_UPD_EST_PERIODOVIGENTE(?,?)}";
	private final String ep_am_sp_sel_list_tablas = "{call COPEREBL.EP_MG_LIST_TABLA_PARAMETROS(?)}";
	private final String ep_am_sp_ins_tabla = "{call COPEREBL.EP_MG_SP_INS_PARAMETRO_GRAL(?,?,?,?,?,?)}";
	private final String ep_am_sp_upd_tabla = "{call COPEREBL.EP_MG_SP_UPD_TABLA_PARAMETRO(?,?,?)}";
	private final String ep_am_sp_upd_parametro = "{call COPEREBL.EP_MG_SP_UPD_PARAMETRO_GRAL(?,?,?,?,?)}";
	private final String ep_al_sp_proc_by_perfil = "{call COPEREBL.EP_AL_GET_PROCEDENCIA_PERFIL (?,?)}";
	
	public List<ClsUnicodeParametro> obtener_lista_parametros(String nombre_tabla) {
		ClsUnicodeParametro item = null;
		List<ClsUnicodeParametro> lista = new ArrayList<ClsUnicodeParametro>();
		con = null;
		String sql = this.ep_am_usp_sel_list_unicodeparametros;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, nombre_tabla);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
//			cs.registerOutParameter(2, Types.OTHER);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				item = new ClsUnicodeParametro();
				try {
					item.setUnicode_parametro_id(rs.getInt("PARAMETRO_ID"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setNombre_tabla(rs.getString("NOMBRE_TABLA"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setCodigo_parametro(rs.getString("CODIGO_PARAMETRO"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setValor_parametro(rs.getString("VALOR_PARAMETRO"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setCodigo_estado(rs.getInt("CODIGO_ESTADO"));
				} catch (Exception e) {
					// TODO: handle exception
				}				
				lista.add(item);
			}
		} catch (SQLException ex) {
			log.error(
					"ERROR! 1 obtener_lista_parametros: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_lista_parametros: " + ex.getMessage() + ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_lista_parametros: " + ex.getMessage() + ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_lista_parametros: " + exception.getMessage() + exception);
			}
		}
		return lista;
	}

	public ClsResultado validar_periodo(String periodo_id) {
		ClsResultado clsResultado = new ClsResultado();
		con = null;
		String sql = this.ep_am_sp_valida_periodo;
		Integer resultado = -1;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, periodo_id);
			cs.registerOutParameter(2, OracleTypes.INTEGER);
			cs.execute();
			resultado = cs.getInt(2);
			
			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error SQLException");
			log.error(
					"ERROR! 1 obtener_lista_parametros: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_lista_parametros: " + ex.getMessage() + ex);
		} catch (Exception ex) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error Exception");
			log.error("ERROR! 3 obtener_lista_parametros: " + ex.getMessage() + ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_lista_parametros: " + exception.getMessage() + exception);
			}
		}
		return clsResultado;		
	}
	
	public Integer updateEstadoPeriodoVigente(Integer codigo_estado) {
		Integer resultado = null;
		String sql = null;
		int id = 0;

		try {
			con = getConnection();			
			sql = this.ep_mg_sp_upd_est_periodo_vigente;
			cs = con.prepareCall(sql);

			cs.setInt(1, codigo_estado);			
			cs.registerOutParameter(2, OracleTypes.INTEGER);

			cs.executeUpdate();

			id = cs.getInt(2);
			
			resultado = id;

		} catch (SQLException ex) {
			log.error("ERROR!" + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR!" + ex.getMessage() + ex);

		} catch (Exception ex) {
			log.error("ERROR!" + ex.getMessage() + ex);

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR!" + exception.getMessage() + exception);
			}
		}
		log.info("Id Parametro:" + resultado);
		return resultado;
	}
	
	public List<ClsUnicodeParametro> obtener_lista_tablas() {
		ClsUnicodeParametro item = null;
		List<ClsUnicodeParametro> lista = new ArrayList<ClsUnicodeParametro>();
		con = null;
		String sql = this.ep_am_sp_sel_list_tablas;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				item = new ClsUnicodeParametro();
				try {
					item.setNombre_tabla(rs.getString("NOMBRE_TABLA"));
					item.setEditable(Integer.parseInt(rs.getString("EDITABLE")));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				lista.add(item);
				
			}
		} catch (SQLException ex) {
			log.error(
					"ERROR! 1 obtener_lista_parametros: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_lista_parametros: " + ex.getMessage() + ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_lista_parametros: " + ex.getMessage() + ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_lista_parametros: " + exception.getMessage() + exception);
			}
		}
		return lista;
	}
	
	public ClsResultado insertar_tabla(ClsUnicodeParametro clsUnicodeParametro){
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.ep_am_sp_ins_tabla;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}
		
		try {
			cs = con.prepareCall(sql);
			cs.setString(1, clsUnicodeParametro.getNombre_tabla());
			cs.setString(2, clsUnicodeParametro.getCodigo_parametro());
			cs.setString(3, clsUnicodeParametro.getValor_parametro());
			cs.setInt(4, clsUnicodeParametro.getCodigo_estado());
			cs.setInt(5, clsUnicodeParametro.getEditable());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			
			log.info("UnicodeDAO >> insertar() " + resultado);
			
			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");
			
		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado actualizar_tabla(String nombre_tabla_anterior,String nombre_tabla_nueva) {
		Integer resultado = null;
		ClsResultado clsResultado = new ClsResultado();
		String sql = null;

		try {
			con = getConnection();			
			sql = this.ep_am_sp_upd_tabla;
			cs = con.prepareCall(sql);

			cs.setString(1, nombre_tabla_anterior);	
			cs.setString(2, nombre_tabla_nueva);	
			cs.registerOutParameter(3, OracleTypes.INTEGER);

			cs.executeUpdate();

			resultado = cs.getInt(3);
			
			log.info("TablaDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR!" + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR!" + ex.getMessage() + ex);

		} catch (Exception ex) {
			log.error("ERROR!" + ex.getMessage() + ex);

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR!" + exception.getMessage() + exception);
			}
		}
		log.info("Id Parametro:" + resultado);
		return clsResultado;
	}

	public ClsResultado actualizar_parametro(ClsUnicodeParametro clsUnicodeParametro){
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.ep_am_sp_upd_parametro;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}
		
		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsUnicodeParametro.getUnicode_parametro_id());
			cs.setString(2, clsUnicodeParametro.getCodigo_parametro());
			cs.setString(3, clsUnicodeParametro.getValor_parametro());
			cs.setInt(4, clsUnicodeParametro.getCodigo_estado());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);
			
			
			log.info("UnicodeDAO >> actualizar() " + resultado);
			
			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");
			
		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public List<ClsUnicodeParametro> obtenerProcedenciaByPerfil(String perfil) {
		ClsUnicodeParametro item = null;
		List<ClsUnicodeParametro> lista = new ArrayList<ClsUnicodeParametro>();
		con = null;
		String sql = this.ep_al_sp_proc_by_perfil;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, perfil);
			cs.registerOutParameter(2, OracleTypes.CURSOR);

			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				item = new ClsUnicodeParametro();
				try {
					item.setCodigo_parametro(rs.getString("CODIGO_PARAMETRO"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setValor_parametro(rs.getString("VALOR_PARAMETRO"));
				} catch (Exception e) {
					// TODO: handle exception
				}
							
				lista.add(item);
			}
		} catch (SQLException ex) {
			log.error(
					"ERROR! 1 obtenerProcedenciaByPerfil: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtenerProcedenciaByPerfil: " + ex.getMessage() + ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtenerProcedenciaByPerfil: " + ex.getMessage() + ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 obtenerProcedenciaByPerfil: " + exception.getMessage() + exception);
			}
		}
		return lista;
	}
	
	
	private void cerrarConexiones() throws SQLException{
		if(rs != null){
			rs.close();
		}
		if(cs != null){
			cs.close();
		}
		if(con != null){
			con.close();
		}
	}
	
	private void setMensajeError(ClsResultado clsResultado, Exception ex){
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
	
	
	
}
