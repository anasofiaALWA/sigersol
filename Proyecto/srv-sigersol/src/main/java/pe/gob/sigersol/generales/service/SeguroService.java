package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSeguro;
import pe.gob.sigersol.entities.dao.SeguroDAO;

public class SeguroService {
	private static Logger log = Logger.getLogger(SeguroService.class);
	SeguroDAO seguroDAO = new SeguroDAO();
	
	public ClsResultado insertar(ClsSeguro clsSeguro) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = seguroDAO.insertar(clsSeguro);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado obtener_seguro(String persona_militar_id) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = seguroDAO.obtener_seguro(persona_militar_id);			
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualiza_estado(Integer seguro_id, Integer codigo_estado) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = seguroDAO.actualiza_estado(seguro_id,codigo_estado);
		} catch (Exception e) {
			log.info("message actualiza_estado >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
