package pe.gob.sigersol.entities;

public class ClsTipoGeneracion {
	
	private Integer tipo_generacion_id;
	private String nombre;
	private String informacion;
	
	public Integer getTipo_generacion_id() {
		return tipo_generacion_id;
	}
	public void setTipo_generacion_id(Integer tipo_generacion_id) {
		this.tipo_generacion_id = tipo_generacion_id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getInformacion() {
		return informacion;
	}
	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}
}