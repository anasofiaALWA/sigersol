package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsVariable;

public class VariableDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(VariableDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_cc_sel_variable = "{call SIGERSOLBL.SGR_CC_SP_SEL_VARIABLE(?,?)}";

	public ClsResultado listarVariable(Integer sitioDF_id) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsVariable> lista = new ArrayList<ClsVariable>();
		con = null;
		String sql = this.sgr_sp_cc_sel_variable;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);

			cs.setInt(1, sitioDF_id);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsVariable item = new ClsVariable();
				try {
					item.setNombreVariable(rs.getString("NOMBRE_VARIABLE"));
				} catch (Exception e) {

				}
				try {
					item.setValor(rs.getDouble("VALOR"));
				} catch (Exception e) {

				}

				lista.add(item);
			}
			clsResultado.setExito(true);
			clsResultado.setObjeto(lista);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 listarVariable: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listarVariable: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listarVariable: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listarVariable: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;

	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}

}
