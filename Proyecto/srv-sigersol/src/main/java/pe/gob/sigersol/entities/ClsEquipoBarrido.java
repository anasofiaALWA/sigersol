package pe.gob.sigersol.entities;

public class ClsEquipoBarrido {

	private Integer equipo_barrido_id;
	private ClsBarrido barrido;
	private ClsUniformeEquipo uniforme;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getEquipo_barrido_id() {
		return equipo_barrido_id;
	}
	public void setEquipo_barrido_id(Integer equipo_barrido_id) {
		this.equipo_barrido_id = equipo_barrido_id;
	}
	public ClsBarrido getBarrido() {
		return barrido;
	}
	public void setBarrido(ClsBarrido barrido) {
		this.barrido = barrido;
	}
	public ClsUniformeEquipo getUniforme() {
		return uniforme;
	}
	public void setUniforme(ClsUniformeEquipo uniforme) {
		this.uniforme = uniforme;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
