package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCantidadResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CantidadResiduosDAO;;

public class CantidadResiduosService {

	private static Logger log = Logger.getLogger(CantidadResiduosService.class);
	CantidadResiduosDAO cantidadResiduosDAO = new CantidadResiduosDAO();
	
	public ClsResultado listarCantidadResiduos(ClsCantidadResiduos clsCantidadResiduos){
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantidadResiduosDAO.listarCantidadResiduos(clsCantidadResiduos);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCantidadResiduos clsCantidadResiduos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantidadResiduosDAO.insertar(clsCantidadResiduos);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCantidadResiduos clsCantidadResiduos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = cantidadResiduosDAO.actualizar(clsCantidadResiduos);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
