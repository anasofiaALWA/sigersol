package pe.gob.sigersol.entities;

public class ClsTrabajadores {

	private Integer trabajadores_id;
	private ClsPersona persona;
	private ClsBarrido barrido;
	private ClsRecRcdOm recRcdOm;
	private ClsTransferencia transferencia;
	private ClsValorizacion valorizacion;
	private ClsDisposicionFinalAdm disposicion_final;
	private String cargo;
	private String condicion_laboral;
	private String horario_trabajo;
	private Integer dias_trabajados;
	private Integer codigo_usuario;
	
	public Integer getTrabajadores_id() {
		return trabajadores_id;
	}
	public void setTrabajadores_id(Integer trabajadores_id) {
		this.trabajadores_id = trabajadores_id;
	}
	public ClsPersona getPersona() {
		return persona;
	}
	public void setPersona(ClsPersona persona) {
		this.persona = persona;
	}
	public ClsBarrido getBarrido() {
		return barrido;
	}
	public void setBarrido(ClsBarrido barrido) {
		this.barrido = barrido;
	}
	public ClsRecRcdOm getRecRcdOm() {
		return recRcdOm;
	}
	public void setRecRcdOm(ClsRecRcdOm recRcdOm) {
		this.recRcdOm = recRcdOm;
	}
	public ClsTransferencia getTransferencia() {
		return transferencia;
	}
	public void setTransferencia(ClsTransferencia transferencia) {
		this.transferencia = transferencia;
	}
	public ClsValorizacion getValorizacion() {
		return valorizacion;
	}
	public void setValorizacion(ClsValorizacion valorizacion) {
		this.valorizacion = valorizacion;
	}
	public ClsDisposicionFinalAdm getDisposicion_final() {
		return disposicion_final;
	}
	public void setDisposicion_final(ClsDisposicionFinalAdm disposicion_final) {
		this.disposicion_final = disposicion_final;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getCondicion_laboral() {
		return condicion_laboral;
	}
	public void setCondicion_laboral(String condicion_laboral) {
		this.condicion_laboral = condicion_laboral;
	}
	public String getHorario_trabajo() {
		return horario_trabajo;
	}
	public void setHorario_trabajo(String horario_trabajo) {
		this.horario_trabajo = horario_trabajo;
	}
	public Integer getDias_trabajados() {
		return dias_trabajados;
	}
	public void setDias_trabajados(Integer dias_trabajados) {
		this.dias_trabajados = dias_trabajados;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
