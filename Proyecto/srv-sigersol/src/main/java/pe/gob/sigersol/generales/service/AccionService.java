package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAccion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AccionDAO;

public class AccionService {

	private static Logger log = Logger.getLogger(AccionService.class);
	AccionDAO accionDAO = new AccionDAO();
	
	public ClsResultado listarAccion(ClsAccion clsAccion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionDAO.listarAccion(clsAccion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAccion clsAccion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionDAO.insertar(clsAccion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsAccion clsAccion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionDAO.actualizar(clsAccion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
