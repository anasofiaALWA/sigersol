package pe.gob.sigersol.reportes.rest;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.parser.ParseException;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.util.ApplicationProperties;
import pe.gob.sigersol.util.FuncionesBase;


@Path("/reporte")
@RequestScoped
public class ReportesRESTService {
	private static Logger log = Logger.getLogger(ReportesRESTService.class.getName());
	
	protected static String directorio = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.DIR_TEMP);
	
	@POST
	@Path("/resumenGral")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String reporteResumenGral(String paramJson, @Context HttpServletRequest request,
			@Context HttpServletResponse response) throws ParseException {

		//jsonEntradaFiltro(paramJson);
		Connection connection = null;
		String respuesta = "";
		
		byte[] reporte = null;

		try {
			String rutaFile = "";

			// OBTENER LA CONEXION A BASE DE DATOS QUE USARA EL REPORTE
			GenericoDAO genericodao = new GenericoDAO();
			connection = genericodao.getConnection();

			try {
				// AQUI VA LA RUTA DEL REPORTE
				rutaFile = request.getSession().getServletContext().getRealPath("/rpt/rptSGRS_ResumenGral.jrxml");
				log.info("rutaFile:" + rutaFile);

				JasperReport jasperReport = JasperCompileManager.compileReport(rutaFile);
				// AQUI VAN LOS PARAMETROS QUE NECESITA EL JASPER
				log.info("Controller resumenGral DESPUES DE COMPILAR EL JASPERREPORT");
				
				String P_URL = FuncionesBase.uploadDir.replace("\\", "/") + "/deployments/srv-sigersol.war"
						+ "/rpt/";
				
				Map parametros = new HashMap();				
				parametros.put("P_RUTA_IMG", P_URL);
				parametros.put(JRParameter.REPORT_LOCALE, new Locale("es", "PE"));

				log.info("Controller resumenGral DESPUES DE PARAMETROS");
				// CORRER EL REPORTE
				reporte = JasperRunManager.runReportToPdf(jasperReport, parametros);
				log.info("reporte.length: " + reporte.length);
				log.info("Controller resumenGral DESPUES DE runReportToPdf");

			} catch (Exception e) {
				log.error("ERROR! generar pdf reporte JASPER e:", e);
			}

			try {
				
				String filename = "resumenGral_" + System.currentTimeMillis() + ".pdf";
				String path = directorio;
				String nombre_Archivo = path + filename;
				log.info("nombre_Archivo: " + nombre_Archivo);
				respuesta = filename;
				log.info("respuesta: " + respuesta);
				OutputStream out = new FileOutputStream(nombre_Archivo);
				out.write(reporte);
				out.close();				

			} catch (Exception e) {
				log.error("ERROR! generar pdf reporte GENERACION ARCHIVO e:", e);
			}

		} catch (Exception e) {
			log.error("ERROR! reporte resumenGral ", e);
		} finally {
			/*if (connection != null) { try { connection.close(); } catch
			 * (SQLException e) {
			 * log.error("ERROR! resumenGral EN cerrar conexion"); } }
			 */
		}
		return respuesta;
	}

}