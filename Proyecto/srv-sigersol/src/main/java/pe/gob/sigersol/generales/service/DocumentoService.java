package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsDocumento;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.DocumentoDAO;

public class DocumentoService {

	private static Logger log = Logger.getLogger(DocumentoService.class);
	DocumentoDAO documentoDAO = new DocumentoDAO();
	
	public ClsResultado obtener(ClsDocumento clsDocumento){
		ClsResultado clsResultado = null;
		try {
			clsResultado = documentoDAO.obtener(clsDocumento);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsDocumento clsDocumento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = documentoDAO.insertar(clsDocumento);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsDocumento clsDocumento) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = documentoDAO.actualizar(clsDocumento);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
