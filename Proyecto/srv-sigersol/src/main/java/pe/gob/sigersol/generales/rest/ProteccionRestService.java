package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsProteccion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoEquipo;
import pe.gob.sigersol.generales.service.ProteccionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/proteccionX")
@RequestScoped
public class ProteccionRestService {

	private static Logger log = Logger.getLogger(ProteccionRestService.class.getName());
	private ClsProteccion clsProteccion;
	private ClsTipoEquipo clsTipoEquipo;
	private ClsBarrido clsBarrido;
	private ProteccionService proteccionService;
	private List<ClsProteccion> listaProteccion;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarProteccion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarProteccion(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			proteccionService = new ProteccionService();
			
			jsonEntradaListaProteccion(paramJson);
			
			log.info("**Obtener Proteccion**");
			
			clsResultado = proteccionService.listarProteccion(clsProteccion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstProteccion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		proteccionService = new ProteccionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaProteccion(paramJson);
			
			for (ClsProteccion clsProteccion : listaProteccion) {
				clsResultado = proteccionService.insertar(clsProteccion);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		proteccionService = new ProteccionService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaProteccion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsProteccion clsProteccion : listaProteccion) {
			clsResultado = proteccionService.actualizar(clsProteccion);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Contenedor
	 * 
	 * */
	private void jsonEntradaProteccion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ProteccionRestService : jsonEntradaProteccion" + jsonEntrada);

		JSONObject jsonObjectTipoContenedor;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaProteccion = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsProteccion = new ClsProteccion();
			clsTipoEquipo = new ClsTipoEquipo();
			clsBarrido = new ClsBarrido();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("barrido_id") != null && innerObject.get("barrido_id").toString() != "") {
				clsBarrido.setBarrido_id(Integer.parseInt(innerObject.get("barrido_id").toString()));
			}
			else {
				clsBarrido.setBarrido_id(-1);
			}
			
			clsProteccion.setBarrido(clsBarrido);
			
			if (innerObject.get("tipo_equipo") != null && innerObject.get("tipo_equipo").toString() != "") {
				jsonObjectTipoContenedor  = (JSONObject) innerObject.get("tipo_equipo");
				clsTipoEquipo.setTipo_equipo_id(Integer.parseInt(jsonObjectTipoContenedor.get("tipo_equipo_id").toString()));
			}
			clsProteccion.setTipo_equipo(clsTipoEquipo);
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsProteccion.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			} else {
				clsProteccion.setCantidad(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsProteccion.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaProteccion.add(clsProteccion);	
		}
			
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaProteccion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ProteccionRestService : jsonEntradaListaProteccion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectContenedor;
		JSONParser jsonParser = new JSONParser();
		
		clsProteccion = new ClsProteccion();
		clsTipoEquipo = new ClsTipoEquipo();
		clsBarrido = new ClsBarrido();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectContenedor = (JSONObject) jsonObjectPrincipal.get("Proteccion");
			
			if (jsonObjectContenedor.get("barrido_id") != null && jsonObjectContenedor.get("barrido_id") != "" &&
					jsonObjectContenedor.get("barrido_id") != " ") {
				clsBarrido.setBarrido_id(Integer.parseInt(jsonObjectContenedor.get("barrido_id").toString()));
			}
			else
				clsBarrido.setBarrido_id(-1);
			
			clsProteccion.setBarrido(clsBarrido);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
