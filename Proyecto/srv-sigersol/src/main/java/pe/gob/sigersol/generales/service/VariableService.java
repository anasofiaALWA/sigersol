package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.VariableDAO;

public class VariableService {
	private static Logger log = Logger.getLogger(VariableService.class);
	VariableDAO dao = new VariableDAO();
	
	public ClsResultado listar(Integer sitioDF_id) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.listarVariable(sitioDF_id);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}

		return clsResultado;

	}
}
