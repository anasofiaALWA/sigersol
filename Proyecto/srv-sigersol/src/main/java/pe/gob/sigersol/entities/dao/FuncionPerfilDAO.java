package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsFuncion;
import pe.gob.sigersol.entities.ClsFuncionPerfil;
import pe.gob.sigersol.entities.ClsPerfil;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.util.DateUtility;

public class FuncionPerfilDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(FuncionPerfilDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	private final String ep_mg_list_tip_funcion_string = "{call COPEREBL.EP_MG_LIST_TIP_FUNCION_STRING(?,?)}";
	private final String ep_mg_list_funcion_x_perfil = "{call COPEREBL.EP_MG_LIST_FUNCION_X_PERFIL(?,?,?)}";
	private final String ep_mg_sp_ins_funcion_x_perfil = "{call COPEREBL.EP_MG_SP_INS_FUNCION_X_PERFIL(?,?,?,?,?,?,?,?,?,?)}";
	private final String ep_mg_sp_upd_funcion_x_perfil = "{call COPEREBL.EP_MG_SP_UPD_FUNCION_X_PERFIL(?,?,?,?,?,?,?,?,?)}";

	public ClsResultado actualizarEstadoFuncionXPerfil(ClsFuncionPerfil clsFuncionPerfil) {
		ClsResultado clsResultado = new ClsResultado();
		log.info("SP: " + this.ep_mg_sp_upd_funcion_x_perfil + " >> Funcion_perfil_id: "
				+ clsFuncionPerfil.getFuncion_perfil_id() + ", Codigo_estado: " + clsFuncionPerfil.getCodigo_estado());
		log.info("Fecha inicio: " + DateUtility.getCurrentTimestamp());

		String sql = this.ep_mg_sp_upd_funcion_x_perfil;
		Integer resultado;
		try {
			con = getConnection();
		} catch (Exception e) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error de conexion");
			return null;
		}

		try {
			cs = con.prepareCall(sql);

			if (clsFuncionPerfil.getFuncion_perfil_id() == null) {
				cs.setInt(1, -1);
			} else {
				cs.setInt(1, clsFuncionPerfil.getFuncion_perfil_id());
			}

			if (clsFuncionPerfil.getCodigo_estado() == null) {
				cs.setInt(2, -1);
			} else {
				cs.setInt(2, clsFuncionPerfil.getCodigo_estado());
			}

			if (clsFuncionPerfil.getInsertar() == null) {
				cs.setInt(3, -1);
			} else {
				cs.setInt(3, clsFuncionPerfil.getInsertar());
			}

			if (clsFuncionPerfil.getConsultar() == null) {
				cs.setInt(4, -1);
			} else {
				cs.setInt(4, clsFuncionPerfil.getConsultar());
			}

			if (clsFuncionPerfil.getActualizar() == null) {
				cs.setInt(5, -1);
			} else {
				cs.setInt(5, clsFuncionPerfil.getActualizar());
			}

			if (clsFuncionPerfil.getEliminar() == null) {
				cs.setInt(6, -1);
			} else {
				cs.setInt(6, clsFuncionPerfil.getEliminar());
			}

			if (clsFuncionPerfil.getExportar() == null) {
				cs.setInt(7, -1);
			} else {
				cs.setInt(7, clsFuncionPerfil.getExportar());
			}
			
			cs.setString(8, clsFuncionPerfil.getUsuario_modificacion());

			cs.registerOutParameter(9, OracleTypes.INTEGER);

			cs.executeUpdate();
			resultado = cs.getInt(9);

			log.info("FuncionPerfilDAO >> actualizar() >> resultado :" );

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

			log.info("Fecha fin: " + DateUtility.getCurrentTimestamp());

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error SQLException");
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error Generico");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje("Error al cerrar conexion");
			}
		}

		return clsResultado;
	}

	public ClsResultado insertarFuncionXPerfil(ClsFuncionPerfil clsFuncionPerfil) {
		ClsResultado clsResultado = new ClsResultado();
		log.info("SP: " + this.ep_mg_sp_ins_funcion_x_perfil + " >> Funcion_id: "
				+ clsFuncionPerfil.getFuncion().getFuncion_id() + ", Perfil: "
				+ clsFuncionPerfil.getPerfil().getPerfil_nombre());
		
		String sql = this.ep_mg_sp_ins_funcion_x_perfil;
		Integer resultado;
		try {
			con = getConnection();
		} catch (Exception e) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error de conexion");
			return null;
		}

		try {
			cs = con.prepareCall(sql);

			if (clsFuncionPerfil.getFuncion() == null || clsFuncionPerfil.getFuncion().getFuncion_id() == null) {
				cs.setInt(1, -1);
			} else {
				cs.setInt(1, clsFuncionPerfil.getFuncion().getFuncion_id());
			}

			if (clsFuncionPerfil.getPerfil().getPerfil_nombre() == null
					|| clsFuncionPerfil.getPerfil().getPerfil_nombre() == ""
					|| clsFuncionPerfil.getPerfil().getPerfil_nombre() == "-1") {
				cs.setString(2, "%");
			} else {
				cs.setString(2, clsFuncionPerfil.getPerfil().getPerfil_nombre());
			}

			if (clsFuncionPerfil.getCodigo_estado() == null) {
				cs.setInt(3, -1);
			} else {
				cs.setInt(3, clsFuncionPerfil.getCodigo_estado());
			}

			if (clsFuncionPerfil.getInsertar() == null) {
				cs.setInt(4, -1);
			} else {
				cs.setInt(4, clsFuncionPerfil.getInsertar());
			}

			if (clsFuncionPerfil.getConsultar() == null) {
				cs.setInt(5, -1);
			} else {
				cs.setInt(5, clsFuncionPerfil.getConsultar());
			}

			if (clsFuncionPerfil.getActualizar() == null) {
				cs.setInt(6, -1);
			} else {
				cs.setInt(6, clsFuncionPerfil.getActualizar());
			}

			if (clsFuncionPerfil.getEliminar() == null) {
				cs.setInt(7, -1);
			} else {
				cs.setInt(7, clsFuncionPerfil.getEliminar());
			}

			if (clsFuncionPerfil.getExportar() == null) {
				cs.setInt(8, -1);
			} else {
				cs.setInt(8, clsFuncionPerfil.getExportar());
			}
			
			cs.setString(9, clsFuncionPerfil.getUsuario_creacion());
			cs.registerOutParameter(10, OracleTypes.INTEGER);

			cs.executeUpdate();
			resultado = cs.getInt(10);
			log.info("FuncionPerfilDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

			

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error SQLException");
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error Generico");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje("Error al cerrar conexion");
			}
		}

		return clsResultado;
	}

	public ClsResultado obtener_por_perfil_string(String perfil) {
		ClsResultado clsResultado = new ClsResultado();
		String item = null;
		con = null;
		String sql = this.ep_mg_list_tip_funcion_string;

		try {
			con = getConnection();
		} catch (Exception e) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error de conexion");
			return null;
		}

		try {
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, perfil);
			cs.registerOutParameter(2, Types.VARCHAR);
			cs.execute();
			item = cs.getString(2);

			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 2 obtener: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 3 obtener: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error SQLException");
		} catch (Exception ex) {
			log.error("ERROR! 4 obtener: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error generico");
		} finally {
			try {
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 5 obtener: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje("Error al cerrar");
			}
		}

		return clsResultado;
	}

	public ClsResultado obtener_por_perfil(String perfil, String funcion) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsFuncionPerfil> lista = new ArrayList<ClsFuncionPerfil>();
		ClsFuncionPerfil clsFuncionPerfil;
		ClsFuncion clsFuncion;
		ClsPerfil clsPerfil;
		con = null;
		String sql = this.ep_mg_list_funcion_x_perfil;

		try {
			con = getConnection();
		} catch (Exception e) {
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error de conexion");
			return null;
		}

		try {
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, perfil);
			if (funcion == null || funcion.trim().equals("")) {
				log.info("parametro funcion vacio");
				cs.setString(2, "%");
			} else {
				cs.setString(2, "%" + funcion + "%");
			}
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);
			while (rs.next()) {
				clsFuncionPerfil = new ClsFuncionPerfil();
				clsFuncionPerfil.setFuncion_perfil_id(rs.getInt("FUNCION_PERFIL_ID"));

				clsFuncion = new ClsFuncion();
				clsFuncion.setFuncion_id(rs.getInt("FUNCION_ID"));
				clsFuncion.setFuncion_nombre(rs.getString("FUNCION_NOMBRE"));

				clsFuncionPerfil.setFuncion(clsFuncion);

				clsPerfil = new ClsPerfil();
				clsPerfil.setPerfil_id(rs.getInt("PERFIL_ID"));
				clsPerfil.setPerfil_nombre(rs.getString("PERFIL_PERFIL_NOMBRE"));

				clsFuncionPerfil.setPerfil(clsPerfil);

				clsFuncionPerfil.setCodigo_estado(rs.getInt("CODIGO_ESTADO"));
				clsFuncionPerfil.setInsertar(rs.getInt("INSERTAR"));
				clsFuncionPerfil.setConsultar(rs.getInt("CONSULTAR"));
				clsFuncionPerfil.setActualizar(rs.getInt("ACTUALIZAR"));
				clsFuncionPerfil.setEliminar(rs.getInt("ELIMINAR"));
				clsFuncionPerfil.setExportar(rs.getInt("EXPORTAR"));
				lista.add(clsFuncionPerfil);
			}

			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 2 obtener: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 3 obtener: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error SQLException");
		} catch (Exception ex) {
			log.error("ERROR! 4 obtener: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje("Error generico");
		} finally {
			try {
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 5 obtener: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje("Error al cerrar");
			}
		}

		return clsResultado;
	}
}
