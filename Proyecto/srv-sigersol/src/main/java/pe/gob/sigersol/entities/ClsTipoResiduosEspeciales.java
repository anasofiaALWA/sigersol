package pe.gob.sigersol.entities;

public class ClsTipoResiduosEspeciales {

	private Integer tipo_residuos_especiales_id;
	private String nombre_tipo;
	
	public Integer getTipo_residuos_especiales_id() {
		return tipo_residuos_especiales_id;
	}
	public void setTipo_residuos_especiales_id(Integer tipo_residuos_especiales_id) {
		this.tipo_residuos_especiales_id = tipo_residuos_especiales_id;
	}
	public String getNombre_tipo() {
		return nombre_tipo;
	}
	public void setNombre_tipo(String nombre_tipo) {
		this.nombre_tipo = nombre_tipo;
	}
	
}
