package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsOtrosRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduo;

public class OtrosRcdOmDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(OtrosRcdOmDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_otros_rcd_om = "{call SIGERSOLBL.SGR_SP_INS_OTROS_RCD_OM(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_otros_rcd_om = "{call SIGERSOLBL.SGR_SP_UPD_OTROS_RCD_OM(?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_otros_rcd_om = "{call SIGERSOLBL.SGR_SP_LIST_OTROS_RCD_OM(?,?)}";  
	
	public ClsResultado listarOtrosRcdOm(ClsOtrosRcdOm clsOtrosRcdOm){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsOtrosRcdOm> lista = new ArrayList<ClsOtrosRcdOm>();
		con = null;
		String sql = this.sgr_sp_list_otros_rcd_om;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOtrosRcdOm.getRecRcdOm().getRec_rcd_om_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsOtrosRcdOm item = new ClsOtrosRcdOm();
				ClsTipoResiduo tipoResiduo = new ClsTipoResiduo();
				
				try {
					tipoResiduo.setTipo_residuos_id(rs.getInt("TIPO_RESIDUOS_ID"));
					tipoResiduo.setNombre_residuos(rs.getString("NOMBRE_RESIDUOS"));
					item.setTipoResiduos(tipoResiduo);
					if(clsOtrosRcdOm.getRecRcdOm().getRec_rcd_om_id() == -1){
						item.setUsuarioAtendido(null);
						item.setCantidad(null);
					} else{
						item.setUsuarioAtendido(rs.getInt("USUARIO_ATENDIDO"));
						item.setCantidad(rs.getInt("CANTIDAD"));
					}
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsOtrosRcdOm clsOtrosRcdOm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_otros_rcd_om;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOtrosRcdOm.getRecRcdOm().getRec_rcd_om_id());
			cs.setInt(2, clsOtrosRcdOm.getTipoResiduos().getTipo_residuos_id());
			cs.setInt(3, clsOtrosRcdOm.getUsuarioAtendido());
			cs.setInt(4, clsOtrosRcdOm.getCantidad());
			cs.setInt(5, clsOtrosRcdOm.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("OtrosRcdOmDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsOtrosRcdOm clsOtrosRcdOm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_otros_rcd_om;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOtrosRcdOm.getRecRcdOm().getRec_rcd_om_id());
			cs.setInt(2, clsOtrosRcdOm.getTipoResiduos().getTipo_residuos_id());
			cs.setInt(3, clsOtrosRcdOm.getUsuarioAtendido());
			cs.setInt(4, clsOtrosRcdOm.getCantidad());
			cs.setInt(5, clsOtrosRcdOm.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("OtrosRcdOmDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
