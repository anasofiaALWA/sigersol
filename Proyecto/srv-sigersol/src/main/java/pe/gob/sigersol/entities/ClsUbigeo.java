package pe.gob.sigersol.entities;

public class ClsUbigeo {
	private String ubigeoId;
	private String departamento;
	private String provincia;
	private String distrito;
	private String clasif_municipal;
	private Double temperatura;
	
	public String getUbigeoId() {
		return ubigeoId;
	}
	public void setUbigeoId(String ubigeoId) {
		this.ubigeoId = ubigeoId;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getClasif_municipal() {
		return clasif_municipal;
	}
	public void setClasif_municipal(String clasif_municipal) {
		this.clasif_municipal = clasif_municipal;
	}
	public Double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}
	
	
	

}
