package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAdministracionMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;


public class AdministracionMunicipalidadDAO extends GenericoDAO{
	private static Logger log = Logger.getLogger(AdministracionMunicipalidadDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_administracion = "{call SIGERSOLBL.SGR_SP_LIST_ADMINISTRACION(?)}";
	private final String sgr_sp_ins_administracion = "{call SIGERSOLBL.SGR_SP_INS_ADMINISTRACION(?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_administracion = "{call SIGERSOLBL.SGR_SP_UPD_ADMINISTRACION(?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener() {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsAdministracionMunicipalidad> lista = new ArrayList<ClsAdministracionMunicipalidad>();
		con = null;
		String sql = this.sgr_sp_list_administracion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);

			while (rs.next()) {
				ClsAdministracionMunicipalidad item = new ClsAdministracionMunicipalidad();
				try {
					item.setAdministracion_municipalidad_id(rs.getInt("ADMINISTRACION_MUNI_ID"));
					item.setMunicipalidad_id(rs.getInt("MUNICIPALIDAD_ID"));
					item.setNumero_ordenanza(rs.getString("NUMERO_ORDENANZA"));
					item.setPeriodo_anio(rs.getInt("PERIODO_ANIO"));
					item.setResponsable(rs.getString("RESPONSABLE"));
					item.setArea_gerencia(rs.getString("AREA_GERENCIA"));
					item.setArchivo_ordenanza(rs.getString("ARCHIVO_ORDENANZA"));
					item.setArchivo_ordenanza_referencia(rs.getString("ARCHIVO_ORDENANZA_REF"));
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsAdministracionMunicipalidad clsAdministracionMunicipalidad) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_administracion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAdministracionMunicipalidad.getMunicipalidad_id());
			cs.setString(2, clsAdministracionMunicipalidad.getNumero_ordenanza());
			cs.setInt(3, clsAdministracionMunicipalidad.getPeriodo_anio());
			cs.setString(4, clsAdministracionMunicipalidad.getResponsable());
			cs.setString(5, clsAdministracionMunicipalidad.getArea_gerencia());
			cs.setString(6, clsAdministracionMunicipalidad.getArchivo_ordenanza());
			cs.setString(7, clsAdministracionMunicipalidad.getArchivo_ordenanza_referencia());
			cs.setInt(8, clsAdministracionMunicipalidad.getCodigo_usuario());
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(9);
			
			log.info("AdministracionMunicipalidadDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsAdministracionMunicipalidad clsAdministracionMunicipalidad) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_administracion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAdministracionMunicipalidad.getAdministracion_municipalidad_id());
			cs.setInt(2, clsAdministracionMunicipalidad.getMunicipalidad_id());
			cs.setString(3, clsAdministracionMunicipalidad.getNumero_ordenanza());
			cs.setInt(4, clsAdministracionMunicipalidad.getPeriodo_anio());
			cs.setString(5, clsAdministracionMunicipalidad.getResponsable());
			cs.setString(6, clsAdministracionMunicipalidad.getArea_gerencia());
			cs.setString(7, clsAdministracionMunicipalidad.getArchivo_ordenanza());
			cs.setString(8, clsAdministracionMunicipalidad.getArchivo_ordenanza_referencia());
			cs.setInt(9, clsAdministracionMunicipalidad.getCodigo_usuario());
			cs.registerOutParameter(10, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(10);

			log.info("AdministracionMunicipalidadDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
