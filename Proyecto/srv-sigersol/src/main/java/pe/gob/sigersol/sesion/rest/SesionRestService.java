package pe.gob.sigersol.sesion.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.Produces;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pe.gob.sigersol.core.ldap.LdapAuth;
import pe.gob.sigersol.entities.AuthResponse;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSesion;
import pe.gob.sigersol.entities.ClsUsuario;
import pe.gob.sigersol.sesion.service.SessionService;
import pe.gob.sigersol.util.StringUtility;
import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@Path("/sesion")
@RequestScoped
public class SesionRestService {

	SessionService service = new SessionService();
	private static Logger log = Logger.getLogger(SesionRestService.class.getName());

	@POST
	@Path("/revisarSesion")
	@Produces(MediaType.APPLICATION_JSON)
	public String revisarSesion(@Context HttpServletRequest request) {
		String respuesta = "";
		log.info("token: " + request.getSession().getAttribute("token"));
		ClsSesion sesion = (ClsSesion) request.getSession().getAttribute("sesion");
		if (sesion != null) {
			respuesta = StringUtility.convertToJSON(sesion);
		} else {
			respuesta = StringUtility.convertToJSON(null);
		}
		return respuesta;
	}

	@POST
	@Path("/validarTemp")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String validarTemp(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado resultado = new ClsResultado();
		String strNombreUsuario = "";
		String strContrasenha = "";
		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		log.info("host: " + _request.getRemoteHost());
		log.info("addr:" + _request.getRemoteAddr());
		log.info("user:" + _request.getRemoteUser());
		log.info("User-Agent" + _request.getHeader("User-Agent"));

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(paramJson);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Login");

			if (jsonObject.get("usuario") != null && jsonObject.get("usuario") != "") {
				strNombreUsuario = jsonObject.get("usuario").toString();
			}
			if (jsonObject.get("contrasenia") != null && jsonObject.get("contrasenia") != "") {
				strContrasenha = jsonObject.get("contrasenia").toString();
			}

			resultado = service.validarUsuarioTemp(strNombreUsuario, strContrasenha,_request);

		} catch (Exception e) {
			// TODO: handle exception
		}

		lRespuesta.setExito(resultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(resultado.getObjeto()));
		lRespuesta.setMensajeUsuario(resultado.getMensaje());
		log.info("Resultado >> " + lRespuesta.toJSon());
		return lRespuesta.toJSon();

	}

	@POST
	@Path("validar")
	public Response validar(@FormParam("strNombreUsuario") String strNombreUsuario,
			@FormParam("strContrasenha") String strContrasenha, @Context HttpServletRequest request,
			@Context HttpServletResponse response) {

		ClsResultado resultado;
		String respuesta = "";

		// ClsUsuarioSesion clsUsuarioO = null;
		boolean autenticado = false;

		try {
			LdapAuth ldap = new LdapAuth();
			// verificar usuario en LDAP (usuario interno)
			boolean autenticadoLdap = ldap.validarAcceso(strNombreUsuario, strContrasenha);

			if (!autenticadoLdap) {
				// verificar usuario en BD (usuario externo)
				ClsResultado resultUser = service.validarUsuario(strNombreUsuario, strContrasenha);
				if (resultUser.isExito()) {
					autenticado = true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		log.info("autenticado::" + autenticado);

		if (autenticado) {
			// obtenemos los datos del usuario.
			// ClsResultado obtencionDatosUsuario =
			// sesionServicio.obtenerUsuario(strNombreUsuario);

			// creamos la sesion.
			// ClsResultado creacionSesion = crearSesion(request, response,
			// strNom);
			// if (creacionSesion.isExito()) {

			// } else {

			// }
		}
		return null;

	}

//	private ClsResultado crearSesion(HttpServletRequest request, HttpServletResponse response) {
//		SessionService sessionService = new SessionService();
//		HttpSession session = null;
//		ClsResultado result = new ClsResultado();
//			// redirigiendo el transito.
//		try {
//			session = sessionService.createSession(request);
//			log.info("redirigiendo el transito nivel 1");
//
//		} catch (Exception e) {
//			log.info("error al crear sesion.");
//			// TODO Auto-generated catch block e.printStackTrace();
//			result.setExito(false);
//		}
//
//		return result;
//	}

	@GET
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public String logout(@Context HttpServletRequest request) {
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				String token = (String) session.getAttribute("token");
				session.removeAttribute("token");
				session.removeAttribute("usuario");
				session.invalidate();
				log.info("logout>> Invalidado token: " + token);
			}
			return null;

		} catch (Exception e) {
			log.error("ERROR!: logout: " + e.getMessage() + e);
		}
		return null;
	}

	
}
