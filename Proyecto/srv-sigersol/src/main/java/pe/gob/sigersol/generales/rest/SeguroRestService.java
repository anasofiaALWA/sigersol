package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSeguro;
import pe.gob.sigersol.generales.service.SeguroService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/seguroX")
@RequestScoped
public class SeguroRestService {
	private static Logger log = Logger.getLogger(SeguroRestService.class.getName());
	ClsSeguro clsSeguro;
	SeguroService seguroService = new SeguroService();
	
	@POST
	@Path("/insertar")
	@Produces(MediaType.APPLICATION_JSON)
	public String insertar(@FormParam("paramJson") String paramJson,
			@Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idSeguro = 0;
		try {
			
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			log.info("paramJson: " + paramJson);
			
			jsonEntradaSeguro(paramJson);
			clsResultado = seguroService.insertar(clsSeguro);
			idSeguro = (Integer) clsResultado.getObjeto();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}

		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/obtener_seguro")
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener_seguro(@FormParam("persona_militar_id") String persona_militar_id,
			@Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idSeguro = 0;
		
		try {
			
			log.info("persona_militar_id :" + persona_militar_id);
			
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			clsResultado = seguroService.obtener_seguro(persona_militar_id);
			
			//idSeguro = (Integer)(clsResultado.getObjeto());
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"seguro\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		} catch (Exception e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		log.info("json : "+ lRespuesta.toJSon());
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/eliminar")
	@Produces(MediaType.APPLICATION_JSON)
	public String actualiza_estado(@FormParam("seguro_id") Integer seguro_id,@FormParam("estado") Integer codigo_estado,
			@Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idSeguro = 0;
		try {
			
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			clsResultado = seguroService.actualiza_estado(seguro_id, codigo_estado);
			idSeguro = (Integer) clsResultado.getObjeto();

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		} catch (Exception e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}

		return lRespuesta.toJSon();
	}
	
	private void jsonEntradaSeguro(String jsonEntrada) throws ParseException {
		log.info("----------------------------------------------------------------------------------");
		log.info("** InicioScriptJson INGRESO>> SeguroRestService : jsonEntradaSeguro>>");
		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();
		clsSeguro = new ClsSeguro();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("SeguroIngr");

			if (jsonObject.get("persona_militar_id") != null && Integer.parseInt(jsonObject.get("persona_militar_id").toString()) != -1) {
				clsSeguro.setPersona_militar_id(Integer.parseInt(jsonObject.get("persona_militar_id").toString()));
			}
			if (jsonObject.get("tipo_seguro_id") != null && Integer.parseInt(jsonObject.get("tipo_seguro_id").toString()) != -1) {
				clsSeguro.setSeguro_tipo_id(Integer.parseInt(jsonObject.get("tipo_seguro_id").toString()));
			}
			if (jsonObject.get("seguro_particular") != null && jsonObject.get("seguro_particular").toString() != "") {
				clsSeguro.setSeguro_particular(jsonObject.get("seguro_particular").toString());
			}
			if (jsonObject.get("usuario_creacion") != null && jsonObject.get("usuario_creacion").toString() != "") {
				clsSeguro.setUsuario_creacion(jsonObject.get("usuario_creacion").toString());
			}

			//log.info("clsCurso: " + clsCurso.toJson());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//log.info("** FinScriptJson SALIDA>> jsonEntradaCurso : " + clsSeguro.toJson());
		log.info("----------------------------------------------------------------------------------");
	}
}
