package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCentroAcopio;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CentroAcopioDAO;

public class CentroAcopioService {

	private static Logger log = Logger.getLogger(CentroAcopioService.class);
	CentroAcopioDAO centroAcopioDAO = new CentroAcopioDAO();
	
	public ClsResultado obtener(ClsCentroAcopio clsCentroAcopio){
		ClsResultado clsResultado = null;
		try {
			clsResultado = centroAcopioDAO.obtener(clsCentroAcopio);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCentroAcopio clsCentroAcopio) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = centroAcopioDAO.insertar(clsCentroAcopio);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCentroAcopio clsCentroAcopio) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = centroAcopioDAO.actualizar(clsCentroAcopio);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
