package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;

public class DisposicionFinalDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(DisposicionFinalDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_list_disposicion = "{call SIGERSOLBL.SGR_SP_LIST_DISPOSICION(?,?,?)}";
	private final String sgr_sp_ins_disposicion = "{call SIGERSOLBL.SGR_SP_INS_DISPOSICION(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_disposicion = "{call SIGERSOLBL.SGR_SP_UPD_DISPOSICION(?,?,?,?,?,?,?)}";
	private final String sgr_cc_sp_list_sitio_disposicion = "{ call SIGERSOLBL.SGR_SP_CC_SEL_DISPOSICION (?,?,?)}";

	public ClsResultado obtener(ClsDisposicionFinal clsDisposicionFinal) {
		ClsResultado clsResultado = new ClsResultado();
		ClsDisposicionFinal item = new ClsDisposicionFinal();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		ClsSitioDisposicion sitioDisposicion = new ClsSitioDisposicion();
		con = null;
		String sql = this.sgr_sp_list_disposicion;
		try {

			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinal.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsDisposicionFinal.getSelec_relleno());
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);

			while (rs.next()) {
				try {
					item.setDisposicion_final_id(rs.getInt("DISPOSICION_FINAL_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(cicloGestionResiduos);
					sitioDisposicion.setSitio_disposicion_id(rs.getInt("SITIO_DISPOSICION_ID"));
					item.setSitioDisposicion(sitioDisposicion);
					item.setRelleno(rs.getInt("FLAG_ADMINISTRACION"));
					item.setCosto(rs.getInt("COSTO"));

				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsDisposicionFinal clsDisposicionFinal) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_disposicion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinal.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsDisposicionFinal.getSitioDisposicion().getSitio_disposicion_id());
			cs.setInt(3, clsDisposicionFinal.getRelleno());
			cs.setInt(4, clsDisposicionFinal.getCosto());
			cs.setInt(5, clsDisposicionFinal.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("DisposicionFinalDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsDisposicionFinal clsDisposicionFinal) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_disposicion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinal.getDisposicion_final_id());
			cs.setInt(2, clsDisposicionFinal.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(3, clsDisposicionFinal.getSitioDisposicion().getSitio_disposicion_id());
			cs.setInt(4, clsDisposicionFinal.getRelleno());
			cs.setInt(5, clsDisposicionFinal.getCosto());
			cs.setInt(6, clsDisposicionFinal.getCodigo_usuario());
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(7);

			log.info("DisposicionFinalDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado listSDF(ClsCicloGestionResiduos cicloGRS) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsDisposicionFinal> lista = new ArrayList<ClsDisposicionFinal>();
		
		con = null;
		String sql = this.sgr_cc_sp_list_sitio_disposicion;
		try {

			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, cicloGRS.getMunicipalidad_id());
			cs.setInt(2, cicloGRS.getAnio());
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);

			while (rs.next()) {
				ClsSitioDisposicion sitioDF = new ClsSitioDisposicion();
				ClsDisposicionFinal item = new ClsDisposicionFinal();
				try {
					item.setDisposicion_final_id(rs.getInt("DISPOSICION_FINAL_ID"));
					sitioDF.setSitio_disposicion_id(rs.getInt("SITIO_DISPOSICION_ID"));
					sitioDF.setNombre_sitio(rs.getString("NOMBRE_SITIO"));
					item.setSitioDisposicion(sitioDF);

				} catch (Exception e) {
					log.error("ERROR! 5 listSDF: " + e.getMessage() + e);
				}

				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 listSDF: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listSDF: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listSDF: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 listSDF: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
