package pe.gob.sigersol.entities;

import com.google.gson.Gson;

public class ClsError {
	private String ErrorCode;
	private String Message;
	private String Date;

	public ClsError() {
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public ClsError(String errorCode, String message, String date) {
		super();
		ErrorCode = errorCode;
		Message = message;
		Date = date;
	}

	@Override
	public String toString() {
		return "Error [ErrorCode=" + ErrorCode + ", Message=" + Message + ", Date=" + Date + "]";
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
