package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsProductosObtenidos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoProductos;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.ProductosObtenidosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/productosObtenidos")
@RequestScoped
public class ProductosObtenidosRestService {

	private static Logger log = Logger.getLogger(ProductosObtenidosRestService.class.getName());
	private ClsProductosObtenidos clsProductosObtenidos;
	private ClsValorizacion clsValorizacion;
	private ClsTipoProductos clsTipoProductos;
	
	private ProductosObtenidosService productosObtenidosService;
	private List<ClsProductosObtenidos> listaProductosObtenidos;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarProductosObtenidos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarProductosObtenidos(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			productosObtenidosService = new ProductosObtenidosService();
			
			jsonEntradaListaResiduosAprovechados(paramJson);
			
			log.info("**Obtener Productos Obtenidos**");
			
			clsResultado = productosObtenidosService.listarProductosObtenidos(clsProductosObtenidos);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstProductosObtenidos\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		productosObtenidosService = new ProductosObtenidosService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaResiduosAprovechados(paramJson);

			for (ClsProductosObtenidos clsProductosObtenidos : listaProductosObtenidos) {
				clsResultado = productosObtenidosService.insertar(clsProductosObtenidos);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		productosObtenidosService = new ProductosObtenidosService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaResiduosAprovechados(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsProductosObtenidos clsProductosObtenidos : listaProductosObtenidos) {
			clsResultado = productosObtenidosService.actualizar(clsProductosObtenidos);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de ResiduosAprovechados
	 * 
	 * */
	private void jsonEntradaResiduosAprovechados(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService :	jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectTipoResiduo;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaProductosObtenidos = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsProductosObtenidos = new ClsProductosObtenidos();
			clsTipoProductos = new ClsTipoProductos();
			clsValorizacion= new ClsValorizacion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("valorizacion_id") != null && innerObject.get("valorizacion_id").toString() != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(innerObject.get("valorizacion_id").toString()));
			}
			
			clsProductosObtenidos.setValorizacion(clsValorizacion);
			
			if (innerObject.get("tipoProductos") != null && innerObject.get("tipoProductos").toString() != "") {
				jsonObjectTipoResiduo  = (JSONObject) innerObject.get("tipoProductos");
				clsTipoProductos.setTipo_productos_id(Integer.parseInt(jsonObjectTipoResiduo.get("tipo_productos_id").toString()));
			}
			clsProductosObtenidos.setTipoProductos(clsTipoProductos);
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsProductosObtenidos.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			}
			else {
				clsProductosObtenidos.setCantidad(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsProductosObtenidos.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaProductosObtenidos.add(clsProductosObtenidos);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaResiduosAprovechados(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ResiduosAprovechadosRestService :	 jsonEntradaListaResiduosAprovechados" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResiduosAprovechados;
		JSONParser jsonParser = new JSONParser();
		
		clsProductosObtenidos = new ClsProductosObtenidos();
		clsTipoProductos = new ClsTipoProductos();
		clsValorizacion= new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResiduosAprovechados = (JSONObject) jsonObjectPrincipal.get("ProductosObtenidos");
			
			if (jsonObjectResiduosAprovechados.get("valorizacion_id") != null && jsonObjectResiduosAprovechados.get("valorizacion_id") != "" &&
					jsonObjectResiduosAprovechados.get("valorizacion_id") != " ") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectResiduosAprovechados.get("valorizacion_id").toString()));
			}
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsProductosObtenidos.setValorizacion(clsValorizacion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
