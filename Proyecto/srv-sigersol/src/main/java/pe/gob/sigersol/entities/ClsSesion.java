package pe.gob.sigersol.entities;

public class ClsSesion {
	private Integer sesion_id;
	private String mac_adress;
	private String version_modulo;
	private Integer usuario_id;
	private Integer sistema_id;
	private String ip_adress;
	private String app_server;
	private String usuario_red;
	private String sistema_operativo;
	private String link_app;
	
	public Integer getSesion_id() {
		return sesion_id;
	}
	public void setSesion_id(Integer sesion_id) {
		this.sesion_id = sesion_id;
	}
	public String getMac_adress() {
		return mac_adress;
	}
	public void setMac_adress(String mac_adress) {
		this.mac_adress = mac_adress;
	}
	public String getVersion_modulo() {
		return version_modulo;
	}
	public void setVersion_modulo(String version_modulo) {
		this.version_modulo = version_modulo;
	}
	public Integer getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}
	public Integer getSistema_id() {
		return sistema_id;
	}
	public void setSistema_id(Integer sistema_id) {
		this.sistema_id = sistema_id;
	}
	public String getIp_adress() {
		return ip_adress;
	}
	public void setIp_adress(String ip_adress) {
		this.ip_adress = ip_adress;
	}
	public String getApp_server() {
		return app_server;
	}
	public void setApp_server(String app_server) {
		this.app_server = app_server;
	}
	public String getUsuario_red() {
		return usuario_red;
	}
	public void setUsuario_red(String usuario_red) {
		this.usuario_red = usuario_red;
	}
	public String getSistema_operativo() {
		return sistema_operativo;
	}
	public void setSistema_operativo(String sistema_operativo) {
		this.sistema_operativo = sistema_operativo;
	}
	public String getLink_app() {
		return link_app;
	}
	public void setLink_app(String link_app) {
		this.link_app = link_app;
	}
	
	
	
}
