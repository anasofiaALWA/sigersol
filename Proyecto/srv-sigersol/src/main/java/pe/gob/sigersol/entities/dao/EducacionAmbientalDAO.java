package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsDatosGenerales;
import pe.gob.sigersol.entities.ClsEducacionAmbiental;
import pe.gob.sigersol.entities.ClsResultado;

public class EducacionAmbientalDAO extends GenericoDAO{
	private static Logger log = Logger.getLogger(EducacionAmbientalDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_educ_ambiental = "{call SIGERSOLBL.SGR_SP_LIST_EDUC_AMBIENTAL(?,?)}";
	private final String sgr_sp_ins_educacion_ambiental = "{call SIGERSOLBL.SGR_SP_INS_EDUCACION_AMBIENTAL(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_educacion_ambiental = "{call SIGERSOLBL.SGR_SP_UPD_EDUCACION_AMBIENTAL(?,?,?,?,?,?,?)}";
	
	
	public ClsResultado obtener(ClsEducacionAmbiental clsEducacionAmbiental) {
		ClsResultado clsResultado = new ClsResultado();
		ClsEducacionAmbiental item = new ClsEducacionAmbiental();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_educ_ambiental;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsEducacionAmbiental.getCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				
				try {
					item.setEducacion_ambiental_id(rs.getInt("EDUCACION_AMBIENTAL_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(cicloGestionResiduos);
					item.setFlag_ambiental(rs.getInt("FLAG_AMBIENTAL"));
					item.setFlag_minam(rs.getInt("FLAG_MINAM"));
					item.setFlag_actividades(rs.getInt("FLAG_ACTIVIDADES"));
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsEducacionAmbiental clsEducacionAmbiental) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_educacion_ambiental;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsEducacionAmbiental.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsEducacionAmbiental.getFlag_ambiental());
			cs.setInt(3, clsEducacionAmbiental.getFlag_minam());
			cs.setInt(4, clsEducacionAmbiental.getFlag_actividades());		
			cs.setInt(5, clsEducacionAmbiental.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("EducacionAmbientalDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsEducacionAmbiental clsEducacionAmbiental) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_educacion_ambiental;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsEducacionAmbiental.getEducacion_ambiental_id());
			cs.setInt(2, clsEducacionAmbiental.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(3, clsEducacionAmbiental.getFlag_ambiental());
			cs.setInt(4, clsEducacionAmbiental.getFlag_minam());
			cs.setInt(5, clsEducacionAmbiental.getFlag_actividades());	
			cs.setInt(6, clsEducacionAmbiental.getCodigo_usuario());
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(7);

			log.info("EducacionAmbientalDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
