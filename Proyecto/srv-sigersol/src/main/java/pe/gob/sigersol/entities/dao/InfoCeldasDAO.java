package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCeldas;
import pe.gob.sigersol.entities.ClsInfoCeldas;
import pe.gob.sigersol.entities.ClsResultado;

public class InfoCeldasDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(InfoCeldasDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_info_celdas = "{call SIGERSOLBL.SGR_SP_INS_INFO_CELDAS(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_info_celdas = "{call SIGERSOLBL.SGR_SP_UPD_INFO_CELDAS(?,?,?,?,?,?)}";
	private final String sgr_sp_list_info_celdas = "{call SIGERSOLBL.SGR_SP_LIST_INFO_CELDAS(?,?)}";  
	
	public ClsResultado listarCeldas(ClsInfoCeldas clsInfoCeldas){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsInfoCeldas> lista = new ArrayList<ClsInfoCeldas>();
		con = null;
		String sql = this.sgr_sp_list_info_celdas;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsInfoCeldas.getDisposicion_final_adm().getDisposicion_final_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsInfoCeldas item = new ClsInfoCeldas();
				ClsCeldas celdas = new ClsCeldas();
				
				try {
					celdas.setCeldas_id(rs.getInt("CELDAS_ID"));
					celdas.setParametro(rs.getString("PARAMETRO"));
					item.setCeldas(celdas);
					if(clsInfoCeldas.getDisposicion_final_adm().getDisposicion_final_id() != -1){
						item.setCeldas_culminadas(rs.getInt("CELDAS_CULMINADAS"));
						item.setCeldas_activas(rs.getInt("CELDAS_ACTIVAS"));					
					}
					else{
						item.setCeldas_culminadas(null);
						item.setCeldas_activas(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsInfoCeldas clsInfoCeldas) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_info_celdas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsInfoCeldas.getDisposicion_final_adm().getDisposicion_final_id());
			cs.setInt(2, clsInfoCeldas.getCeldas().getCeldas_id());
			cs.setInt(3, clsInfoCeldas.getCeldas_culminadas());
			cs.setInt(4, clsInfoCeldas.getCeldas_activas());
			cs.setInt(5, clsInfoCeldas.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("InfoCeldasDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsInfoCeldas clsInfoCeldas) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_info_celdas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsInfoCeldas.getDisposicion_final_adm().getDisposicion_final_id());
			cs.setInt(2, clsInfoCeldas.getCeldas().getCeldas_id());
			cs.setInt(3, clsInfoCeldas.getCeldas_culminadas());
			cs.setInt(4, clsInfoCeldas.getCeldas_activas());
			cs.setInt(5, clsInfoCeldas.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("InfoCeldasDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
