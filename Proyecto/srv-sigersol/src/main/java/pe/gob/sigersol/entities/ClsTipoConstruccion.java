package pe.gob.sigersol.entities;

public class ClsTipoConstruccion {
	
	private Integer tipo_construccion_id;
	private String nombre_tipo;
	private Integer flag_cantidad;
	
	public Integer getTipo_construccion_id() {
		return tipo_construccion_id;
	}
	public void setTipo_construccion_id(Integer tipo_construccion_id) {
		this.tipo_construccion_id = tipo_construccion_id;
	}
	public String getNombre_tipo() {
		return nombre_tipo;
	}
	public void setNombre_tipo(String nombre_tipo) {
		this.nombre_tipo = nombre_tipo;
	}
	public Integer getFlag_cantidad() {
		return flag_cantidad;
	}
	public void setFlag_cantidad(Integer flag_cantidad) {
		this.flag_cantidad = flag_cantidad;
	}
}
