package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCCGenComposicion;
import pe.gob.sigersol.entities.ClsCCGeneracion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CCGeneracionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/ccGeneracion")
@RequestScoped
public class CCGeneracionRestService {
	private static Logger log = Logger.getLogger(CCGeneracionRestService.class.getName());
	private CCGeneracionService ccGeneracionService;

	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		ccGeneracionService = new CCGeneracionService();
		ClsCCGeneracion ccGeneracion = jsonEntradaInsertar(paramJson);
		clsResultado = ccGeneracionService.insertar(ccGeneracion);
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();

	}

	@POST
	@Path("/calcularTool4")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String calcularTool4(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ccGeneracionService = new CCGeneracionService();
		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		String ubigeoId = null;
		Integer municipalidadId = null;
		Integer ccGeneracionId = null;
		String condicionId = null;
		Integer anioCGRS=null;

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(paramJson);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Filtro");

			if (jsonObject.get("ubigeoId") != null && jsonObject.get("ubigeoId") != "") {
				ubigeoId = jsonObject.get("ubigeoId").toString();
			}
			if (jsonObject.get("municipalidadId") != null && jsonObject.get("municipalidadId") != "") {
				municipalidadId = Integer.valueOf(jsonObject.get("municipalidadId").toString());
			}
			if (jsonObject.get("ccGeneracionId") != null && jsonObject.get("ccGeneracionId") != "") {
				ccGeneracionId = Integer.parseInt(jsonObject.get("ccGeneracionId").toString());
			}
			if (jsonObject.get("condicionId") != null && jsonObject.get("condicionId") != "") {
				condicionId = jsonObject.get("condicionId").toString();
			}
			if (jsonObject.get("anioCGRS") != null && jsonObject.get("anioCGRS") != "") {
				anioCGRS =Integer.parseInt(jsonObject.get("anioCGRS").toString());
			}

			clsResultado = ccGeneracionService.calcularTool4(ubigeoId, municipalidadId, ccGeneracionId, condicionId,anioCGRS);

		} catch (Exception e) {
			e.printStackTrace();
		}

		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();

	}

	private ClsCCGeneracion jsonEntradaInsertar(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CCGeneracionRestService :	jsonEntradaInsertar" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		ClsCCGeneracion ccGeneracion = new ClsCCGeneracion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("ccGeneracion");

			if (jsonObject.get("anioInicio") != null && jsonObject.get("anioInicio") != "") {
				ccGeneracion.setAnioInicio_sdf(Integer.parseInt(jsonObject.get("anioInicio").toString()));
			}
			if (jsonObject.get("totalRRSS") != null && jsonObject.get("totalRRSS") != "") {
				ccGeneracion.setGeneracionTotal_sdf(Double.valueOf(jsonObject.get("totalRRSS").toString()));
			}
			if (jsonObject.get("disposicionFinalId") != null && jsonObject.get("disposicionFinalId") != "") {
				ccGeneracion.setDisposicionFinalId(Integer.parseInt(jsonObject.get("disposicionFinalId").toString()));
			}
			if (jsonObject.get("sesionId") != null && jsonObject.get("sesionId") != "") {
				ccGeneracion.setSesionId(Integer.parseInt(jsonObject.get("sesionId").toString()));
			}

			org.json.simple.JSONArray jsonListComposicion = (org.json.simple.JSONArray) jsonObject
					.get("listComposicion");
			java.util.Iterator i = jsonListComposicion.iterator();
			List<ClsCCGenComposicion> listComposicion = new ArrayList<ClsCCGenComposicion>();
			while (i.hasNext()) {
				org.json.simple.JSONObject objComposicion = (org.json.simple.JSONObject) i.next();
				ClsCCGenComposicion item = new ClsCCGenComposicion();

				if (objComposicion.get("cc_tipoResiduoId") != null && objComposicion.get("cc_tipoResiduoId") != "") {
					item.setCc_tipoResiduoId(Integer.parseInt(objComposicion.get("cc_tipoResiduoId").toString()));
				}
				if (objComposicion.get("porcentaje") != null && objComposicion.get("porcentaje") != "") {
					item.setPorcentaje(Double.valueOf(objComposicion.get("porcentaje").toString()));
				}
				item.setSesionId(ccGeneracion.getSesionId());

				listComposicion.add(item);

			}

			ccGeneracion.setListComposicion(listComposicion);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ccGeneracion;
	}
}
