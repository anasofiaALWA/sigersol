package pe.gob.sigersol.entities;

public class ClsRecoleccionSelectivaValorizacion {

	private Integer recoleccion_id;
	private ClsCicloGestionResiduos ciclo_grs_id;
	private Integer flag_recoleccion_selectiva;
	private String adm_serv_id;
	private Integer costo_anual;
	private Integer frecuencia_recoleccion;
	private Integer fr_cada_dias;
	private Integer cantidad_residuos_organicos;
	private Integer cantidad_residuos_inorganicos;
	private String descrip_otro_vehiculo;
	private Integer cantidad_otro;
	private Integer antig_promedio_otro;
	private Integer codigo_usuario;
	
	public Integer getRecoleccion_id() {
		return recoleccion_id;
	}
	public void setRecoleccion_id(Integer recoleccion_id) {
		this.recoleccion_id = recoleccion_id;
	}
	public ClsCicloGestionResiduos getCiclo_grs_id() {
		return ciclo_grs_id;
	}
	public void setCiclo_grs_id(ClsCicloGestionResiduos ciclo_grs_id) {
		this.ciclo_grs_id = ciclo_grs_id;
	}
	public Integer getFlag_recoleccion_selectiva() {
		return flag_recoleccion_selectiva;
	}
	public void setFlag_recoleccion_selectiva(Integer flag_recoleccion_selectiva) {
		this.flag_recoleccion_selectiva = flag_recoleccion_selectiva;
	}
	public String getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(String adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getFrecuencia_recoleccion() {
		return frecuencia_recoleccion;
	}
	public void setFrecuencia_recoleccion(Integer frecuencia_recoleccion) {
		this.frecuencia_recoleccion = frecuencia_recoleccion;
	}
	public Integer getFr_cada_dias() {
		return fr_cada_dias;
	}
	public void setFr_cada_dias(Integer fr_cada_dias) {
		this.fr_cada_dias = fr_cada_dias;
	}
	public Integer getCantidad_residuos_organicos() {
		return cantidad_residuos_organicos;
	}
	public void setCantidad_residuos_organicos(Integer cantidad_residuos_organicos) {
		this.cantidad_residuos_organicos = cantidad_residuos_organicos;
	}
	public Integer getCantidad_residuos_inorganicos() {
		return cantidad_residuos_inorganicos;
	}
	public void setCantidad_residuos_inorganicos(Integer cantidad_residuos_inorganicos) {
		this.cantidad_residuos_inorganicos = cantidad_residuos_inorganicos;
	}
	public String getDescrip_otro_vehiculo() {
		return descrip_otro_vehiculo;
	}
	public void setDescrip_otro_vehiculo(String descrip_otro_vehiculo) {
		this.descrip_otro_vehiculo = descrip_otro_vehiculo;
	}
	public Integer getCantidad_otro() {
		return cantidad_otro;
	}
	public void setCantidad_otro(Integer cantidad_otro) {
		this.cantidad_otro = cantidad_otro;
	}
	public Integer getAntig_promedio_otro() {
		return antig_promedio_otro;
	}
	public void setAntig_promedio_otro(Integer antig_promedio_otro) {
		this.antig_promedio_otro = antig_promedio_otro;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
