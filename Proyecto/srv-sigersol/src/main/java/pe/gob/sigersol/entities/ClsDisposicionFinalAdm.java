package pe.gob.sigersol.entities;

public class ClsDisposicionFinalAdm {

	private Integer disposicion_final_id;
	private ClsSitioDisposicion sitio_disposicion_id;
	private Integer anio_inicio;
	private Integer cant_personal;
	private String situacion_terreno_id;
	private String admin_servicio_id;
	private Integer temperatura_zona;
	private String direccion;
	private String ref_direccion;
	private Integer area_total_terreno;
	private Integer area_utilizada_terreno;
	private Integer altura_nf;
	private String altura_rs_ss;
	private String tipo_manejo_eq_id;
	private String tipo_manejo_cob_id;
	private String tipo_tecnologia_id;
	private Integer maneja_lixiviados;
	private String tipo_manejo_lixiviados_id;
	private Integer maneja_gases;
	private Integer num_chimenea_inst;
	private Integer num_chimenea_quem_op;
	private String tipo_manejo_gases;
	private Integer genera_ee;
	private Integer cap_instalada_ee_mw;
	private Integer generacion_ee_anual_mw;
	private Integer codigo_usuario;
	
	public Integer getDisposicion_final_id() {
		return disposicion_final_id;
	}
	public void setDisposicion_final_id(Integer disposicion_final_id) {
		this.disposicion_final_id = disposicion_final_id;
	}
	public ClsSitioDisposicion getSitio_disposicion_id() {
		return sitio_disposicion_id;
	}
	public void setSitio_disposicion_id(ClsSitioDisposicion sitio_disposicion_id) {
		this.sitio_disposicion_id = sitio_disposicion_id;
	}
	public Integer getAnio_inicio() {
		return anio_inicio;
	}
	public void setAnio_inicio(Integer anio_inicio) {
		this.anio_inicio = anio_inicio;
	}
	public Integer getCant_personal() {
		return cant_personal;
	}
	public void setCant_personal(Integer cant_personal) {
		this.cant_personal = cant_personal;
	}
	public String getSituacion_terreno_id() {
		return situacion_terreno_id;
	}
	public void setSituacion_terreno_id(String situacion_terreno_id) {
		this.situacion_terreno_id = situacion_terreno_id;
	}
	public String getAdmin_servicio_id() {
		return admin_servicio_id;
	}
	public void setAdmin_servicio_id(String admin_servicio_id) {
		this.admin_servicio_id = admin_servicio_id;
	}
	public Integer getTemperatura_zona() {
		return temperatura_zona;
	}
	public void setTemperatura_zona(Integer temperatura_zona) {
		this.temperatura_zona = temperatura_zona;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getRef_direccion() {
		return ref_direccion;
	}
	public void setRef_direccion(String ref_direccion) {
		this.ref_direccion = ref_direccion;
	}
	public Integer getArea_total_terreno() {
		return area_total_terreno;
	}
	public void setArea_total_terreno(Integer area_total_terreno) {
		this.area_total_terreno = area_total_terreno;
	}
	public Integer getArea_utilizada_terreno() {
		return area_utilizada_terreno;
	}
	public void setArea_utilizada_terreno(Integer area_utilizada_terreno) {
		this.area_utilizada_terreno = area_utilizada_terreno;
	}
	public Integer getAltura_nf() {
		return altura_nf;
	}
	public void setAltura_nf(Integer altura_nf) {
		this.altura_nf = altura_nf;
	}
	public String getAltura_rs_ss() {
		return altura_rs_ss;
	}
	public void setAltura_rs_ss(String altura_rs_ss) {
		this.altura_rs_ss = altura_rs_ss;
	}
	public String getTipo_manejo_eq_id() {
		return tipo_manejo_eq_id;
	}
	public void setTipo_manejo_eq_id(String tipo_manejo_eq_id) {
		this.tipo_manejo_eq_id = tipo_manejo_eq_id;
	}
	public String getTipo_manejo_cob_id() {
		return tipo_manejo_cob_id;
	}
	public void setTipo_manejo_cob_id(String tipo_manejo_cob_id) {
		this.tipo_manejo_cob_id = tipo_manejo_cob_id;
	}
	public String getTipo_tecnologia_id() {
		return tipo_tecnologia_id;
	}
	public void setTipo_tecnologia_id(String tipo_tecnologia_id) {
		this.tipo_tecnologia_id = tipo_tecnologia_id;
	}
	public Integer getManeja_lixiviados() {
		return maneja_lixiviados;
	}
	public void setManeja_lixiviados(Integer maneja_lixiviados) {
		this.maneja_lixiviados = maneja_lixiviados;
	}
	public String getTipo_manejo_lixiviados_id() {
		return tipo_manejo_lixiviados_id;
	}
	public void setTipo_manejo_lixiviados_id(String tipo_manejo_lixiviados_id) {
		this.tipo_manejo_lixiviados_id = tipo_manejo_lixiviados_id;
	}
	public Integer getManeja_gases() {
		return maneja_gases;
	}
	public void setManeja_gases(Integer maneja_gases) {
		this.maneja_gases = maneja_gases;
	}
	public Integer getNum_chimenea_inst() {
		return num_chimenea_inst;
	}
	public void setNum_chimenea_inst(Integer num_chimenea_inst) {
		this.num_chimenea_inst = num_chimenea_inst;
	}
	public Integer getNum_chimenea_quem_op() {
		return num_chimenea_quem_op;
	}
	public void setNum_chimenea_quem_op(Integer num_chimenea_quem_op) {
		this.num_chimenea_quem_op = num_chimenea_quem_op;
	}
	public String getTipo_manejo_gases() {
		return tipo_manejo_gases;
	}
	public void setTipo_manejo_gases(String tipo_manejo_gases) {
		this.tipo_manejo_gases = tipo_manejo_gases;
	}
	public Integer getGenera_ee() {
		return genera_ee;
	}
	public void setGenera_ee(Integer genera_ee) {
		this.genera_ee = genera_ee;
	}
	public Integer getCap_instalada_ee_mw() {
		return cap_instalada_ee_mw;
	}
	public void setCap_instalada_ee_mw(Integer cap_instalada_ee_mw) {
		this.cap_instalada_ee_mw = cap_instalada_ee_mw;
	}
	public Integer getGeneracion_ee_anual_mw() {
		return generacion_ee_anual_mw;
	}
	public void setGeneracion_ee_anual_mw(Integer generacion_ee_anual_mw) {
		this.generacion_ee_anual_mw = generacion_ee_anual_mw;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
