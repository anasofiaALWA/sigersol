package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsProgramaSegregacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.ProgramaSegregacionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase ProgramaSegregacionRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de valorizacion permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/programaSegregacionX")
@RequestScoped
public class ProgramaSegregacionRestService {
	
	private static Logger log = Logger.getLogger(SolicitudRestService.class.getName());
	
	private ClsProgramaSegregacion clsProgramaSegregacion;
	private ClsValorizacion clsValorizacion;
	
	private ProgramaSegregacionService programaSegregacionService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			programaSegregacionService = new ProgramaSegregacionService();
			
			jsonEntradaProgramaSegregacion(paramJson);
			
			log.info("**obtener almacenamiento");
			
			clsResultado = programaSegregacionService.obtener(clsProgramaSegregacion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"segregacion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		programaSegregacionService = new ProgramaSegregacionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaProgramaSegregacion(paramJson);
			clsResultado = programaSegregacionService.insertar(clsProgramaSegregacion);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		programaSegregacionService = new ProgramaSegregacionService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaProgramaSegregacion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = programaSegregacionService.actualizar(clsProgramaSegregacion);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de ProgramaSegregacion
	 * 
	 * */
	private void jsonEntradaProgramaSegregacion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ProgramaSegregacionRestService :jsonEntradaProgramaSegregacion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectSegregacion;
		JSONParser jsonParser = new JSONParser();
		
		clsProgramaSegregacion = new ClsProgramaSegregacion();
		clsValorizacion = new ClsValorizacion();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectSegregacion = (JSONObject) jsonObjectPrincipal.get("Segregacion");
			
			if (jsonObjectSegregacion.get("programa_segregacion_id") != null && jsonObjectSegregacion.get("programa_segregacion_id") != "") {
				clsProgramaSegregacion.setPrograma_segregacion_id(Integer.parseInt(jsonObjectSegregacion.get("programa_segregacion_id").toString()));
			} else {
				clsProgramaSegregacion.setPrograma_segregacion_id(-1);
			}
			
			if (jsonObjectSegregacion.get("valorizacion_id") != null && jsonObjectSegregacion.get("valorizacion_id") != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectSegregacion.get("valorizacion_id").toString()));
			} else {
				clsValorizacion.setValorizacion_id(-1);
			}
			
			clsProgramaSegregacion.setValorizacion(clsValorizacion);
			
			if (jsonObjectSegregacion.get("caf_adm_propia") != null && jsonObjectSegregacion.get("caf_adm_propia") != "") {
				clsProgramaSegregacion.setCaf_adm_propia(Integer.parseInt(jsonObjectSegregacion.get("caf_adm_propia").toString()));
			}
			
			if (jsonObjectSegregacion.get("caf_tercerizado") != null && jsonObjectSegregacion.get("caf_tercerizado") != "") {
				clsProgramaSegregacion.setCaf_tercerizado(Integer.parseInt(jsonObjectSegregacion.get("caf_tercerizado").toString()));
			}
			
			if (jsonObjectSegregacion.get("caf_mixto") != null && jsonObjectSegregacion.get("caf_mixto") != "") {
				clsProgramaSegregacion.setCaf_mixto(Integer.parseInt(jsonObjectSegregacion.get("caf_mixto").toString()));
			}
			
			if (jsonObjectSegregacion.get("recicladores_hombre_25") != null && jsonObjectSegregacion.get("recicladores_hombre_25") != "") {
				clsProgramaSegregacion.setRecicladores_hombre_25(Integer.parseInt(jsonObjectSegregacion.get("recicladores_hombre_25").toString()));
			}
			
			if (jsonObjectSegregacion.get("recicladores_mujer_25") != null && jsonObjectSegregacion.get("recicladores_mujer_25") != "") {
				clsProgramaSegregacion.setRecicladores_mujer_25(Integer.parseInt(jsonObjectSegregacion.get("recicladores_mujer_25").toString()));
			}
			
			if (jsonObjectSegregacion.get("recicladores_hombre_25_65") != null && jsonObjectSegregacion.get("recicladores_hombre_25_65") != "") {
				clsProgramaSegregacion.setRecicladores_hombre_25_65(Integer.parseInt(jsonObjectSegregacion.get("recicladores_hombre_25_65").toString()));
			}
			
			if (jsonObjectSegregacion.get("recicladores_mujer_25_65") != null && jsonObjectSegregacion.get("recicladores_mujer_25_65") != "") {
				clsProgramaSegregacion.setRecicladores_mujer_25_65(Integer.parseInt(jsonObjectSegregacion.get("recicladores_mujer_25_65").toString()));
			}
			
			if (jsonObjectSegregacion.get("recicladores_hombre_65") != null && jsonObjectSegregacion.get("recicladores_hombre_65") != "") {
				clsProgramaSegregacion.setRecicladores_hombre_65(Integer.parseInt(jsonObjectSegregacion.get("recicladores_hombre_65").toString()));
			}
			
			if (jsonObjectSegregacion.get("recicladores_mujer_65") != null && jsonObjectSegregacion.get("recicladores_mujer_65") != "") {
				clsProgramaSegregacion.setRecicladores_mujer_65(Integer.parseInt(jsonObjectSegregacion.get("recicladores_mujer_65").toString()));
			}
				
			if (jsonObjectSegregacion.get("cod_usuario") != null && jsonObjectSegregacion.get("cod_usuario") != "") {
				clsProgramaSegregacion.setCodigo_usuario(Integer.parseInt(jsonObjectSegregacion.get("cod_usuario").toString()));
			}
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
