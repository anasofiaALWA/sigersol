package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;

public class BarridoDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(BarridoDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_barrido = "{call SIGERSOLBL.SGR_SP_LIST_SGR_BARRIDO(?,?)}";
	private final String sgr_sp_ins_barrido = "{call SIGERSOLBL.SGR_SP_INS_BARRIDO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_barrido = "{call SIGERSOLBL.SGR_SP_UPD_BARRIDO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsBarrido clsBarrido) {
		ClsResultado clsResultado = new ClsResultado();
		ClsBarrido item = new ClsBarrido();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		
		String[] s;
		
		con = null;
		String sql = this.sgr_sp_list_sgr_barrido;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsBarrido.getCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setBarrido_id(rs.getInt("BARRIDO_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(cicloGestionResiduos);
					item.setAdm_serv_id(rs.getInt("ADM_SERV_ID"));
					item.setCosto_anual(rs.getInt("COSTO_ANUAL"));
					item.setVia_asfaltada(rs.getInt("VIA_ASFALTADA"));
					item.setVia_no_asfaltada(rs.getInt("VIA_NO_ASFALTADA"));
					item.setPlazas(rs.getInt("PLAZAS"));
					item.setPlayas(rs.getInt("PLAYAS"));
					item.setDemanda_barrido(rs.getInt("DEMANDA_BARRIDO"));
					item.setFrecuencia(rs.getString("FRECUENCIA"));
					item.setOtros_barridos(rs.getString("OTROS_BARRIDO"));
					item.setOtros_demanda(rs.getInt("OTROS_DEMANDA"));
					
					s = item.getFrecuencia().split("/");
					item.setFr1(Integer.parseInt(s[0]));
					item.setFr2(Integer.parseInt(s[1]));
					
					item.setDescripcion_otros(rs.getString("DESCRIPCION_OTROS"));
					item.setCr_25(rs.getInt("CANTIDAD_DISPOSITIVO1"));
					item.setCr_25_65(rs.getInt("CANTIDAD_DISPOSITIVO2"));
					item.setCr_65(rs.getInt("CANTIDAD_DISPOSITIVO3"));
					item.setCpr_25(rs.getInt("CANTIDAD_PROMEDIO1"));
					item.setCpr_25_65(rs.getInt("CANTIDAD_PROMEDIO2"));
					item.setCpr_65(rs.getInt("CANTIDAD_PROMEDIO3"));
					item.setTotal_recolectado(rs.getDouble("TOTAL_RECOLECTADO"));
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsBarrido clsBarrido) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_barrido;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsBarrido.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsBarrido.getAdm_serv_id());
			cs.setInt(3, clsBarrido.getCosto_anual());
			cs.setInt(4, clsBarrido.getVia_asfaltada());
			cs.setInt(5, clsBarrido.getVia_no_asfaltada());
			cs.setInt(6, clsBarrido.getPlazas());
			cs.setInt(7, clsBarrido.getPlayas());
			cs.setInt(8, clsBarrido.getDemanda_barrido());
			cs.setString(9, clsBarrido.getOtros_barridos());
			cs.setInt(10, clsBarrido.getOtros_demanda());
			cs.setString(11, clsBarrido.getFrecuencia());
			cs.setString(12, clsBarrido.getDescripcion_otros());
			cs.setInt(13, clsBarrido.getCr_25());
			cs.setInt(14, clsBarrido.getCr_25_65());
			cs.setInt(15, clsBarrido.getCr_65());
			cs.setInt(16, clsBarrido.getCpr_25());
			cs.setInt(17, clsBarrido.getCpr_25_65());
			cs.setInt(18, clsBarrido.getCpr_65());
			cs.setDouble(19, clsBarrido.getTotal_recolectado());
			cs.setInt(20, clsBarrido.getCodigo_usuario());
			cs.registerOutParameter(21, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(21);
			
			log.info("BarridoDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsBarrido clsBarrido) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_barrido;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsBarrido.getBarrido_id());
			cs.setInt(2, clsBarrido.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(3, clsBarrido.getAdm_serv_id());
			cs.setInt(4, clsBarrido.getCosto_anual());
			cs.setInt(5, clsBarrido.getVia_asfaltada());
			cs.setInt(6, clsBarrido.getVia_no_asfaltada());
			cs.setInt(7, clsBarrido.getPlazas());
			cs.setInt(8, clsBarrido.getPlayas());
			cs.setInt(9, clsBarrido.getDemanda_barrido());
			cs.setString(10, clsBarrido.getOtros_barridos());
			cs.setInt(11, clsBarrido.getOtros_demanda());
			cs.setString(12, clsBarrido.getFrecuencia());
			cs.setString(13, clsBarrido.getDescripcion_otros());
			cs.setInt(14, clsBarrido.getCr_25());
			cs.setInt(15, clsBarrido.getCr_25_65());
			cs.setInt(16, clsBarrido.getCr_65());
			cs.setInt(17, clsBarrido.getCpr_25());
			cs.setInt(18, clsBarrido.getCpr_25_65());
			cs.setInt(19, clsBarrido.getCpr_65());
			cs.setDouble(20, clsBarrido.getTotal_recolectado());
			cs.setInt(21, clsBarrido.getCodigo_usuario());
			cs.registerOutParameter(22, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(22);

			log.info("BarridoDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
	
}
