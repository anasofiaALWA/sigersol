package pe.gob.sigersol.entities;

public class ClsRecoleccion {

	private Integer recoleccion_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private Integer adm_serv_id;
	private Integer costo_anual;
	private Integer flag_recoleccion;
	private Integer cantidad_recolectados;
	private Integer cantidad_aprovechable;
	private Integer cantidad_no_aprovechable;
	private Integer cant_zonas;
	private Integer cant_viviendas;
	private String descrip_otro_vehiculo;
	private Integer cantidad_otro;
	private Integer num_llantas_otro;
	private Integer dias_bajas_vehiculos;
	private Integer costo_llantas_otro;
	private Integer consumo_combustible;
	private Integer costo_combustible;
	private Integer codigo_usuario;
	
	public Integer getRecoleccion_id() {
		return recoleccion_id;
	}
	public void setRecoleccion_id(Integer recoleccion_id) {
		this.recoleccion_id = recoleccion_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public Integer getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(Integer adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getFlag_recoleccion() {
		return flag_recoleccion;
	}
	public void setFlag_recoleccion(Integer flag_recoleccion) {
		this.flag_recoleccion = flag_recoleccion;
	}
	public Integer getCantidad_recolectados() {
		return cantidad_recolectados;
	}
	public void setCantidad_recolectados(Integer cantidad_recolectados) {
		this.cantidad_recolectados = cantidad_recolectados;
	}
	public Integer getCantidad_aprovechable() {
		return cantidad_aprovechable;
	}
	public void setCantidad_aprovechable(Integer cantidad_aprovechable) {
		this.cantidad_aprovechable = cantidad_aprovechable;
	}
	public Integer getCantidad_no_aprovechable() {
		return cantidad_no_aprovechable;
	}
	public void setCantidad_no_aprovechable(Integer cantidad_no_aprovechable) {
		this.cantidad_no_aprovechable = cantidad_no_aprovechable;
	}
	public Integer getCant_zonas() {
		return cant_zonas;
	}
	public void setCant_zonas(Integer cant_zonas) {
		this.cant_zonas = cant_zonas;
	}
	public Integer getCant_viviendas() {
		return cant_viviendas;
	}
	public void setCant_viviendas(Integer cant_viviendas) {
		this.cant_viviendas = cant_viviendas;
	}
	public String getDescrip_otro_vehiculo() {
		return descrip_otro_vehiculo;
	}
	public void setDescrip_otro_vehiculo(String descrip_otro_vehiculo) {
		this.descrip_otro_vehiculo = descrip_otro_vehiculo;
	}
	public Integer getCantidad_otro() {
		return cantidad_otro;
	}
	public void setCantidad_otro(Integer cantidad_otro) {
		this.cantidad_otro = cantidad_otro;
	}
	public Integer getNum_llantas_otro() {
		return num_llantas_otro;
	}
	public void setNum_llantas_otro(Integer num_llantas_otro) {
		this.num_llantas_otro = num_llantas_otro;
	}
	public Integer getDias_bajas_vehiculos() {
		return dias_bajas_vehiculos;
	}
	public void setDias_bajas_vehiculos(Integer dias_bajas_vehiculos) {
		this.dias_bajas_vehiculos = dias_bajas_vehiculos;
	}
	public Integer getCosto_llantas_otro() {
		return costo_llantas_otro;
	}
	public void setCosto_llantas_otro(Integer costo_llantas_otro) {
		this.costo_llantas_otro = costo_llantas_otro;
	}
	public Integer getConsumo_combustible() {
		return consumo_combustible;
	}
	public void setConsumo_combustible(Integer consumo_combustible) {
		this.consumo_combustible = consumo_combustible;
	}
	public Integer getCosto_combustible() {
		return costo_combustible;
	}
	public void setCosto_combustible(Integer costo_combustible) {
		this.costo_combustible = costo_combustible;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
