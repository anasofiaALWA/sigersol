package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCantVehiculos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoVehiculo;

public class CantVehiculosDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(CantVehiculosDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_cant_vehiculos = "{call SIGERSOLBL.SGR_SP_INS_CANT_VEHICULOS(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_cant_vehiculos = "{call SIGERSOLBL.SGR_SP_UPD_CANT_VEHICULOS(?,?,?,?,?,?)}";
	private final String sgr_sp_list_cant_vehiculos = "{call SIGERSOLBL.SGR_SP_LIST_CANT_VEHICULOS(?,?)}";  
	
	public ClsResultado listarCantVehiculos(ClsCantVehiculos clsCantVehiculos){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCantVehiculos> lista = new ArrayList<ClsCantVehiculos>();
		con = null;
		String sql = this.sgr_sp_list_cant_vehiculos;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCantVehiculos.getDisposicion_final_adm().getDisposicion_final_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsCantVehiculos item = new ClsCantVehiculos();
				ClsTipoVehiculo tipoVehiculo = new ClsTipoVehiculo();
				
				try {
					tipoVehiculo.setTipo_vehiculo_id(rs.getInt("TIPO_VEHICULO_ID"));
					tipoVehiculo.setNombre_tipo(rs.getString("NOMBRE_TIPO"));
					item.setTipoVehiculo(tipoVehiculo);
					if(clsCantVehiculos.getDisposicion_final_adm().getDisposicion_final_id() != -1){
						item.setTiempo_parcial(rs.getInt("TIEMPO_PARCIAL"));
						item.setTiempo_completo(rs.getInt("TIEMPO_COMPLETO"));
					}
					else{
						item.setTiempo_parcial(null);
						item.setTiempo_completo(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCantVehiculos clsCantVehiculos) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_cant_vehiculos;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCantVehiculos.getDisposicion_final_adm().getDisposicion_final_id());
			cs.setInt(2, clsCantVehiculos.getTipoVehiculo().getTipo_vehiculo_id());
			cs.setInt(3, clsCantVehiculos.getTiempo_parcial());
			cs.setInt(4, clsCantVehiculos.getTiempo_completo());
			cs.setInt(5, clsCantVehiculos.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("TipoManejoDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsCantVehiculos clsCantVehiculos) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_cant_vehiculos;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCantVehiculos.getDisposicion_final_adm().getDisposicion_final_id());
			cs.setInt(2, clsCantVehiculos.getTipoVehiculo().getTipo_vehiculo_id());
			cs.setInt(3, clsCantVehiculos.getTiempo_parcial());
			cs.setInt(4, clsCantVehiculos.getTiempo_completo());
			cs.setInt(5, clsCantVehiculos.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("TipoManejoDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
