package pe.gob.sigersol.downloadFile.rest;

import java.io.File;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.jboss.logging.Logger;

import pe.gob.sigersol.generales.service.UnicodeParametroService;
import pe.gob.sigersol.util.ApplicationProperties;
import pe.gob.sigersol.util.StringUtility;

@Path("/descarga")
@RequestScoped

public class DownloadFileRestService {
	private static Logger log = Logger.getLogger(DownloadFileRestService.class.getName());
	
	protected static String directorio = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.DIR_TEMP);

	@GET
	@Path("/getFile")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getFileAS(@QueryParam("fileNameRef") String fileNameRef, @QueryParam("fileName") String fileName,
			@Context HttpServletRequest request) {
		try {
						
			
			String rutaFileServer = directorio;

			File fileTemporal = null;
			if (!StringUtility.isEmpty(fileNameRef)) {
				fileTemporal = new File(rutaFileServer + fileNameRef);
			}

			ResponseBuilder response = Response.ok((Object) fileTemporal);
			response.header("charset", "UTF-8");
			response.header("Content-Disposition", "attachment; filename=" + fileName);

			return response.build();
		} catch (Exception e) {
			log.warn("ERROR! getFile no se obtuvo objeto" + e);
			e.printStackTrace();
		}
		return null;
	}

}