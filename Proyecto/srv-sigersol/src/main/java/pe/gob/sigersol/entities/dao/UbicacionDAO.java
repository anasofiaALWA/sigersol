package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduoSolido;
import pe.gob.sigersol.entities.ClsUbicacion;

public class UbicacionDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(UbicacionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_ubicacion = "{call SIGERSOLBL.SGR_SP_INS_UBICACION(?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_ubicacion = "{call SIGERSOLBL.SGR_SP_UPD_UBICACION(?,?,?,?,?,?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_ubicacion = "{call SIGERSOLBL.SGR_SP_LIST_UBICACION(?,?)}"; 
	
	
	public ClsResultado listarUbicacion(ClsUbicacion clsUbicacion){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsUbicacion> lista = new ArrayList<ClsUbicacion>();
		con = null;
		String sql = this.sgr_sp_list_ubicacion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsUbicacion.getLugar_aprovechamiento().getLugar_aprovechamiento_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsUbicacion item = new ClsUbicacion();
				try {
					item.setUbicacion_id(rs.getInt("UBICACION_ID"));
					item.setZona_coordenada(rs.getString("ZONA_COORDENADA"));
					item.setNorte_coordenada(rs.getString("NORTE_COORDENADA"));
					item.setSur_coordenada(rs.getString("SUR_COORDENADA"));
					item.setDiv_politica_dep(rs.getString("DIV_POLITICA_DEP"));
					item.setDiv_politica_prov(rs.getString("DIV_POLITICA_PROV"));
					item.setDiv_politica_dist(rs.getString("DIV_POLITICA_DIST"));
					item.setDireccion(rs.getString("DIRECCION"));
					item.setDireccion(rs.getString("REFERENCIA"));

				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado listarCoordenada(ClsUbicacion clsUbicacion){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsUbicacion> lista = new ArrayList<ClsUbicacion>();
		con = null;
		String sql = this.sgr_sp_list_ubicacion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsUbicacion.getLugar_aprovechamiento().getLugar_aprovechamiento_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsUbicacion item = new ClsUbicacion();
				try {
					item.setUbicacion_id(rs.getInt("UBICACION_ID"));
					item.setZona_coordenada(rs.getString("ZONA_COORDENADA"));
					item.setNorte_coordenada(rs.getString("NORTE_COORDENADA"));
					item.setSur_coordenada(rs.getString("SUR_COORDENADA"));
					item.setDiv_politica_dep(rs.getString("DIV_POLITICA_DEP"));
					item.setDiv_politica_prov(rs.getString("DIV_POLITICA_PROV"));
					item.setDiv_politica_dist(rs.getString("DIV_POLITICA_DIST"));
					item.setDireccion(rs.getString("DIRECCION"));
					item.setReferencia(rs.getString("REFERENCIA"));
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsUbicacion clsUbicacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_ubicacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsUbicacion.getLugar_aprovechamiento().getLugar_aprovechamiento_id());
			cs.setString(2, clsUbicacion.getZona_coordenada());
			cs.setString(3, clsUbicacion.getNorte_coordenada());
			cs.setString(4, clsUbicacion.getSur_coordenada());
			cs.setString(5, clsUbicacion.getDiv_politica_dep());
			cs.setString(6, clsUbicacion.getDiv_politica_prov());
			cs.setString(7, clsUbicacion.getDiv_politica_dist());
			cs.setString(8, clsUbicacion.getDireccion());
			cs.setString(9, clsUbicacion.getReferencia());
			cs.setInt(10, clsUbicacion.getCodigo_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);
			
			log.info("UbicacionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsUbicacion clsUbicacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_ubicacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsUbicacion.getLugar_aprovechamiento().getLugar_aprovechamiento_id());
			cs.setString(2, clsUbicacion.getZona_coordenada());
			cs.setString(3, clsUbicacion.getNorte_coordenada());
			cs.setString(4, clsUbicacion.getSur_coordenada());
			cs.setString(5, clsUbicacion.getDiv_politica_dep());
			cs.setString(6, clsUbicacion.getDiv_politica_prov());
			cs.setString(7, clsUbicacion.getDiv_politica_dist());
			cs.setString(8, clsUbicacion.getDireccion());
			cs.setString(9, clsUbicacion.getReferencia());
			cs.setInt(10, clsUbicacion.getCodigo_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);

			log.info("UbicacionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
