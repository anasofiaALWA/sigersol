package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCombustibleVehiculo;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CombustibleVehiculo2DAO;


public class CombustibleVehiculo2Service {

	private static Logger log = Logger.getLogger(CombustibleVehiculo2Service.class);
	CombustibleVehiculo2DAO combustibleVehiculo2DAO = new CombustibleVehiculo2DAO();
	
	public ClsResultado listarVehiculoRecoleccion(ClsCombustibleVehiculo clsCombustibleVehiculo){
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculo2DAO.listarCombustibleVehiculo(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCombustibleVehiculo clsCombustibleVehiculo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculo2DAO.insertar(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCombustibleVehiculo clsCombustibleVehiculo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = combustibleVehiculo2DAO.actualizar(clsCombustibleVehiculo);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
