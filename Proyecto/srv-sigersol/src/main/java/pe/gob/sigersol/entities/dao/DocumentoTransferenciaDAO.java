package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsDocumentoTransferencia;
import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTerceros;

public class DocumentoTransferenciaDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(DocumentoTransferenciaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_terceros = "{call SIGERSOLBL.SGR_SP_LIST_SGR_DOC_TRANSF(?,?)}";
	private final String sgr_sp_ins_terceros = "{call SIGERSOLBL.SGR_SP_INS_DOCUMENTO_TRANSF(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_terceros = "{call SIGERSOLBL.SGR_SP_UPD_DOCUMENTO_TRANSF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsDocumentoTransferencia clsDocumentoTransferencia) {
		ClsResultado clsResultado = new ClsResultado();
		ClsDocumentoTransferencia item = new ClsDocumentoTransferencia();
		ClsMunicipalidad municipalidad = new ClsMunicipalidad();
		con = null;
		String sql = this.sgr_sp_list_sgr_terceros;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDocumentoTransferencia.getMunicipalidad().getMunicipalidad_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setDocumento_transferencia_id(rs.getInt("DOCUMENTO_TRANSFERENCIA_ID"));
					municipalidad.setMunicipalidad_id(rs.getInt("MUNICIPALIDAD_ID"));
					item.setMunicipalidad(municipalidad);
					item.setNumero_documento(rs.getString("NUMERO_DOCUMENTO"));
					item.setNombre_infraestructura(rs.getString("NOMBRE_INFRAESTRUCTURA"));
					item.setNombre_autoridad(rs.getString("NOMBRE_AUTORIDAD"));
					item.setFecha_emision(rs.getString("FECHA_EMISION"));
					item.setNumero_licencia(rs.getString("NUMERO_LICENCIA"));
					item.setFecha_inicio_licencia(rs.getString("FECHA_INICIO_LICENCIA"));
					item.setFecha_fin_licencia(rs.getString("FECHA_FIN_LICENCIA"));
					item.setTipo_documento(rs.getString("TIPO_DOCUMENTO"));
					item.setNombre_archivo(rs.getString("NOMBRE_ARCHIVO"));
					item.setNombre_archivo_ref(rs.getString("NOMBRE_ARCHIVO_REF"));
					item.setUid_alfresco(rs.getString("UID_AFRESCO"));			
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsDocumentoTransferencia clsDocumentoTransferencia) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_terceros;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDocumentoTransferencia.getMunicipalidad().getMunicipalidad_id());
			cs.setString(2, clsDocumentoTransferencia.getNumero_documento());
			cs.setString(3, clsDocumentoTransferencia.getNombre_infraestructura());
			cs.setString(4, clsDocumentoTransferencia.getNombre_autoridad());
			cs.setString(5, clsDocumentoTransferencia.getFecha_emision());
			cs.setString(6, clsDocumentoTransferencia.getNumero_licencia());
			cs.setString(7, clsDocumentoTransferencia.getFecha_inicio_licencia());
			cs.setString(8, clsDocumentoTransferencia.getFecha_fin_licencia());
			cs.setString(9, clsDocumentoTransferencia.getTipo_documento());
			cs.setString(10, clsDocumentoTransferencia.getNombre_archivo());
			cs.setString(11, clsDocumentoTransferencia.getNombre_archivo_ref());
			cs.setString(12, clsDocumentoTransferencia.getUid_alfresco());
			cs.setInt(13, clsDocumentoTransferencia.getCodigo_usuario());
			cs.registerOutParameter(14, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(14);
			
			log.info("TransferenciaDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsDocumentoTransferencia clsDocumentoTransferencia) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_terceros;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDocumentoTransferencia.getDocumento_transferencia_id());
			cs.setInt(2, clsDocumentoTransferencia.getMunicipalidad().getMunicipalidad_id());
			cs.setString(3, clsDocumentoTransferencia.getNumero_documento());
			cs.setString(4, clsDocumentoTransferencia.getNombre_infraestructura());
			cs.setString(5, clsDocumentoTransferencia.getNombre_autoridad());
			cs.setString(6, clsDocumentoTransferencia.getFecha_emision());
			cs.setString(7, clsDocumentoTransferencia.getNumero_licencia());
			cs.setString(8, clsDocumentoTransferencia.getFecha_inicio_licencia());
			cs.setString(9, clsDocumentoTransferencia.getFecha_fin_licencia());
			cs.setString(10, clsDocumentoTransferencia.getTipo_documento());
			cs.setString(11, clsDocumentoTransferencia.getNombre_archivo());
			cs.setString(12, clsDocumentoTransferencia.getNombre_archivo_ref());
			cs.setString(13, clsDocumentoTransferencia.getUid_alfresco());
			cs.setInt(14, clsDocumentoTransferencia.getCodigo_usuario());
			cs.registerOutParameter(15, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(15);

			log.info("TransferenciaDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
