package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAccion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoActividad;

public class AccionDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(AccionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_accion = "{call SIGERSOLBL.SGR_SP_INS_ACCION(?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_accion = "{call SIGERSOLBL.SGR_SP_UPD_ACCION(?,?,?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_accion = "{call SIGERSOLBL.SGR_SP_LIST_ACCION(?,?)}";  
	
	public ClsResultado listarAccion(ClsAccion clsAccion){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsAccion> lista = new ArrayList<ClsAccion>();
		con = null;
		String sql = this.sgr_sp_list_accion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccion.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsAccion item = new ClsAccion();
				ClsTipoActividad tipoActividad = new ClsTipoActividad();
				
				try {
					tipoActividad.setTipo_actividad_id(rs.getInt("TIPO_ACTIVIDAD_ID"));
					tipoActividad.setNombre_tipo(rs.getString("NOMBRE_TIPO"));
					tipoActividad.setFlag_actividad(rs.getInt("FLAG_ACTIVIDAD"));
					item.setTipo_actividad(tipoActividad);
					if(clsAccion.getEducacion_ambiental().getEducacion_ambiental_id() == -1){
						item.setTipo(null);
						item.setParticipantes_hombres(null);
						item.setParticipantes_mujeres(null);
						item.setTotal_mujer_hombre(null);
					} else{
						item.setTipo(rs.getInt("TIPO"));
						item.setParticipantes_hombres(rs.getInt("PARTICIPANTES_HOMBRES"));
						item.setParticipantes_mujeres(rs.getInt("PARTICIPANTES_MUJERES"));
						item.setTotal_mujer_hombre(rs.getInt("TOTAL_MUJER_HOMBRE"));
					}
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAccion clsAccion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_accion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccion.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.setInt(2, clsAccion.getTipo_actividad().getTipo_actividad_id());
			cs.setInt(3, clsAccion.getTipo());
			cs.setInt(4, clsAccion.getParticipantes_hombres());
			cs.setInt(5, clsAccion.getParticipantes_mujeres());
			cs.setInt(6, clsAccion.getTotal_mujer_hombre());
			cs.setInt(7, clsAccion.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);
			
			log.info("AccionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsAccion clsAccion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_accion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccion.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.setInt(2, clsAccion.getTipo_actividad().getTipo_actividad_id());
			cs.setInt(3, clsAccion.getTipo());
			cs.setInt(4, clsAccion.getParticipantes_hombres());
			cs.setInt(5, clsAccion.getParticipantes_mujeres());
			cs.setInt(6, clsAccion.getTotal_mujer_hombre());
			cs.setInt(7, clsAccion.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);

			log.info("AccionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
	
}
