package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsPersona;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.PersonaDAO;


public class PersonaService {

	private static Logger log = Logger.getLogger(PersonaService.class);
	PersonaDAO personaDAO = new PersonaDAO();

	public Integer insertar(ClsPersona clsPersona) {
		
		Integer clsResultado = null;
		Integer aux = 0;
		try {
			aux = personaDAO.existe(clsPersona);
			if( aux == 0)
				clsResultado = personaDAO.insertar(clsPersona);
			else
				clsResultado = aux;
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
