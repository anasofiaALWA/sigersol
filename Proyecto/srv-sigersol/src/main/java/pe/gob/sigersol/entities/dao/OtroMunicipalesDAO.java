package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsOtroMunicipales;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsResultado;

public class OtroMunicipalesDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(OtroMunicipalesDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_otro_municipales = "{call SIGERSOLBL.SGR_SP_LIST_OTRO_MUNICIPALES(?,?)}";
	private final String sgr_sp_ins_otro_municipales = "{call SIGERSOLBL.SGR_SP_INS_OTRO_MUNICIPALES(?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_otro_municipales = "{call SIGERSOLBL.SGR_SP_UPD_OTRO_MUNICIPALES(?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsOtroMunicipales clsOtroMunicipales) {
		
		ClsResultado clsResultado = new ClsResultado();
		ClsOtroMunicipales item = new ClsOtroMunicipales();
		ClsRecRcdOm recRcdOm = new ClsRecRcdOm();
		con = null;
		String sql = this.sgr_sp_list_otro_municipales;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOtroMunicipales.getRecRcdOm().getRec_rcd_om_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setOtro_municipales_id(rs.getInt("OTRO_MUNICIPALES_ID"));
					recRcdOm.setRec_rcd_om_id(rs.getInt("REC_RCD_OM_ID"));
					item.setRecRcdOm(recRcdOm);
					item.setFlag_municipal(rs.getInt("FLAG_MUNICIPAL"));
					item.setAdm_serv_id(rs.getInt("ADM_SERV_ID"));
					item.setCosto(rs.getInt("COSTO"));
					item.setDescrip_residuos_otros(rs.getString("DESCRIP_RESIDUOS_OTROS"));
					item.setUsuario_atendido_otros(rs.getInt("USUARIO_ATENDIDO_OTROS"));
					item.setCantidad_otros(rs.getInt("CANTIDAD_OTROS"));
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsOtroMunicipales clsOtroMunicipales) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_otro_municipales;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOtroMunicipales.getRecRcdOm().getRec_rcd_om_id());
			cs.setInt(2, clsOtroMunicipales.getFlag_municipal());
			cs.setInt(3, clsOtroMunicipales.getAdm_serv_id());
			cs.setInt(4, clsOtroMunicipales.getCosto());
			cs.setString(5, clsOtroMunicipales.getDescrip_residuos_otros());
			cs.setInt(6, clsOtroMunicipales.getUsuario_atendido_otros());
			cs.setInt(7, clsOtroMunicipales.getCantidad_otros());
			cs.setInt(8, clsOtroMunicipales.getCodigo_usuario());
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(9);
			
			log.info("OtroMunicipalesDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsOtroMunicipales clsOtroMunicipales) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_otro_municipales;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsOtroMunicipales.getOtro_municipales_id());
			cs.setInt(2, clsOtroMunicipales.getRecRcdOm().getRec_rcd_om_id());
			cs.setInt(3, clsOtroMunicipales.getFlag_municipal());
			cs.setInt(4, clsOtroMunicipales.getAdm_serv_id());
			cs.setInt(5, clsOtroMunicipales.getCosto());
			cs.setString(6, clsOtroMunicipales.getDescrip_residuos_otros());
			cs.setInt(7, clsOtroMunicipales.getUsuario_atendido_otros());
			cs.setInt(8, clsOtroMunicipales.getCantidad_otros());
			cs.setInt(9, clsOtroMunicipales.getCodigo_usuario());
			cs.registerOutParameter(10, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(10);

			log.info("OtroMunicipalesDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
