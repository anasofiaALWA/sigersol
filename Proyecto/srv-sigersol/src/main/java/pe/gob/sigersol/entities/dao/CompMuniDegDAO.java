package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCompMuniDeg;
import pe.gob.sigersol.entities.ClsResiduosMunicipales;
import pe.gob.sigersol.entities.ClsResultado;

public class CompMuniDegDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(CompMuniDegDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_comp_muni_deg = "{call SIGERSOLBL.SGR_SP_INS_COMP_MUNI_DEG(?,?,?,?,?)}";
	private final String sgr_sp_upd_comp_muni_deg = "{call SIGERSOLBL.SGR_SP_UPD_COMP_MUNI_DEG(?,?,?,?,?)}";
	private final String sgr_sp_list_comp_muni_deg = "{call SIGERSOLBL.SGR_SP_LIST_COMP_MUNI_DEG(?,?)}";  
	
	public ClsResultado listarCompMuni(ClsCompMuniDeg clsCompMuniDeg){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCompMuniDeg> lista = new ArrayList<ClsCompMuniDeg>();
		con = null;
		String sql = this.sgr_sp_list_comp_muni_deg;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCompMuniDeg.getAreasDegradadas().getAreas_degradadas_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsCompMuniDeg item = new ClsCompMuniDeg();
				ClsResiduosMunicipales residuosMunicipales = new ClsResiduosMunicipales();
				
				try {
					residuosMunicipales.setResiduos_municipales_id(rs.getInt("RESIDUOS_MUNICIPALES_ID"));
					residuosMunicipales.setNombre(rs.getString("NOMBRE"));
					item.setResiduosMunicipales(residuosMunicipales);
					if(clsCompMuniDeg.getAreasDegradadas().getAreas_degradadas_id() != -1)
						item.setPorcentaje(rs.getInt("PORCENTAJE"));
					else
						item.setPorcentaje(null);
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCompMuniDeg clsCompMuniDeg) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_comp_muni_deg;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCompMuniDeg.getAreasDegradadas().getAreas_degradadas_id());
			cs.setInt(2, clsCompMuniDeg.getResiduosMunicipales().getResiduos_municipales_id());
			cs.setInt(3, clsCompMuniDeg.getPorcentaje());
			cs.setInt(4, clsCompMuniDeg.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);
			
			log.info("CompMuniDegDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsCompMuniDeg clsCompMuniDeg) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_comp_muni_deg;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCompMuniDeg.getAreasDegradadas().getAreas_degradadas_id());
			cs.setInt(2, clsCompMuniDeg.getResiduosMunicipales().getResiduos_municipales_id());
			cs.setInt(3, clsCompMuniDeg.getPorcentaje());
			cs.setInt(4, clsCompMuniDeg.getCodigo_usuario());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(5);

			log.info("CompMuniDegDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
