package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsRecDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.RecDisposicionFinalDAO;

public class RecDisposicionFinalService {

	private static Logger log = Logger.getLogger(RecDisposicionFinalService.class);
	RecDisposicionFinalDAO recDisposicionFinalDAO = new RecDisposicionFinalDAO();

	public ClsResultado obtener(ClsRecDisposicionFinal clsRecDisposicionFinal){
		ClsResultado clsResultado = null;
		try {
			clsResultado = recDisposicionFinalDAO.obtener(clsRecDisposicionFinal);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsRecDisposicionFinal clsRecDisposicionFinal) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = recDisposicionFinalDAO.insertar(clsRecDisposicionFinal);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsRecDisposicionFinal clsRecDisposicionFinal) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = recDisposicionFinalDAO.actualizar(clsRecDisposicionFinal);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
