package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsMunicipalidad;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSesion;
import pe.gob.sigersol.entities.ClsUbigeo;
import pe.gob.sigersol.entities.ClsUsuario;

public class SesionDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(SesionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String minam_usp_sel_usuario_activo = "{call BD_MINAM.USP_SEL_SG_USUARIO_ACTIVO(?,?)}";
	private final String segfys_usp_sel_personal_usuario = "{call SEGFYS.USP_SEL_PERSONAL_USUARIO(?,?)}";
	private final String minam_usp_sel_usuario_id_segfys = "{call BD_MINAM.USP_SEL_SG_USUARIO_ID_SEGFYS(?,?)}";
	private final String minam_usp_ins_sesion = "{call BD_MINAM.USP_INSERTAR_SESION(?,?,?,?,?,?,?,?,?,?)}";

	private final String sgr_validar_login_temp = "{call SIGERSOLBL.SGR_LOGIN_TEMP(?,?,?)}";
	private final String sgr_get_usuario_temp = "{call SIGERSOLBL.SGR_SP_SEL_USUARIO_MUNI_TEMP(?,?)}";

	public ClsResultado segfys_obtener_usuarioId(String username) {
		ClsResultado clsResultado = new ClsResultado();
		String seg_userId;

		con = null;
		String sql = this.segfys_usp_sel_personal_usuario;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);

		}

		try {
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, username);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.executeUpdate();
			seg_userId = cs.getString(2);

			clsResultado.setObjeto(seg_userId);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Resultado exitoso");
		} catch (SQLException ex) {
			log.error(
					"ERROR! 1 segfys_obtener_usuarioId: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 segfys_obtener_usuarioId: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 segfys_obtener_usuarioId: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 segfys_obtener_usuarioId: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado minam_obtener_usuarioId_segfys(String segfys_userId) {
		ClsResultado clsResultado = new ClsResultado();
		Integer userId;

		con = null;
		String sql = this.minam_usp_sel_usuario_id_segfys;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return clsResultado;
		}

		try {
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, segfys_userId);
			cs.registerOutParameter(2, OracleTypes.INTEGER);
			cs.executeUpdate();
			userId = cs.getInt(2);

			clsResultado.setObjeto(userId);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Resultado exitoso");
		} catch (SQLException ex) {
			log.error(
					"ERROR! 1 minam_obtener_usuarioId: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 minam_obtener_usuarioId: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 minam_obtener_usuarioId: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 minam_obtener_usuarioId: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado minam_obtener_usuarioId_activo(String username) {
		ClsResultado clsResultado = new ClsResultado();
		Integer userId;
		ClsUsuario clsUsuario = null;

		con = null;
		String sql = this.minam_usp_sel_usuario_activo;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return clsResultado;
		}

		try {
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, username);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				clsUsuario = new ClsUsuario();
				clsUsuario.setUsuario_id(rs.getInt("ID_USUARIO"));
				clsUsuario.setUsuario(rs.getString("USUARIO"));
				clsUsuario.setContrasenha(rs.getString("CONTRACENA"));
				clsUsuario.setNombres(rs.getString("NOMBRES"));
				clsUsuario.setApellidos(rs.getString("APELLIDOS"));
				clsUsuario.setCorreo(rs.getString("CORREO"));
				clsUsuario.setTelefono1(rs.getString("TELEFONO1"));
				clsUsuario.setTelefono2(rs.getString("TELEFONO2"));
				clsUsuario.setEstadoRegistro(rs.getString("ESTADO_REGISTRO"));
				clsUsuario.setCargo(rs.getString("CARGO"));
				clsUsuario.setOficina(rs.getString("OFICINA"));
				clsUsuario.setTipo(rs.getInt("TIPO"));

				clsResultado.setObjeto(clsUsuario);
				clsResultado.setExito(true);
			}

			clsResultado.setMensaje("Resultado exitoso");
		} catch (SQLException ex) {
			log.error("ERROR! 1 minam_obtener_usuarioId_activo: "
					+ new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 minam_obtener_usuarioId_activo: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 minam_obtener_usuarioId_activo: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 minam_obtener_usuarioId_activo: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado minam_registra_sesion(ClsSesion sesion) {
		ClsResultado clsResultado = new ClsResultado();
		Integer sesionId;

		con = null;
		String sql = this.minam_usp_ins_sesion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return clsResultado;
		}

		try {
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, sesion.getMac_adress());
			cs.setString(2, sesion.getVersion_modulo());
			cs.setInt(3, sesion.getUsuario_id());
			cs.setInt(4, sesion.getSistema_id());
			cs.setString(5, sesion.getIp_adress());
			cs.setString(6, sesion.getApp_server());
			cs.setString(7, sesion.getUsuario_red());
			cs.setString(8, sesion.getSistema_operativo());
			cs.setString(9, sesion.getLink_app());
			cs.registerOutParameter(10, OracleTypes.INTEGER);
			cs.executeUpdate();
			sesionId = cs.getInt(10);

			clsResultado.setObjeto(sesionId);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Resultado exitoso");
		} catch (SQLException ex) {
			log.error(
					"ERROR! 1 minam_obtener_usuarioId: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 minam_obtener_usuarioId: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 minam_obtener_usuarioId: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 minam_obtener_usuarioId: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado validarLoginTemp(String username, String clave) {
		ClsResultado clsResultado = new ClsResultado();
		Integer userId;

		con = null;
		String sql = this.sgr_validar_login_temp;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, username);
			cs.setString(2, clave);
			cs.registerOutParameter(3, OracleTypes.INTEGER);
			cs.executeUpdate();
			userId = cs.getInt(3);
			clsResultado.setObjeto(userId);
			if (userId > 0) {
				clsResultado.setExito(true);
			}

			clsResultado.setMensaje("Resultado exitoso");
		} catch (SQLException ex) {
			log.error("ERROR! 1 validarLoginTemp: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 validarLoginTemp: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 validarLoginTemp: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 validarLoginTemp: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado obtener_usuarioTemp(Integer usuarioId) {
		ClsResultado clsResultado = new ClsResultado();
		ClsUsuario clsUsuario = new ClsUsuario();
		ClsMunicipalidad userMuni = null;
		ClsUbigeo ubigeo = null;
		con = null;
		String sql = this.sgr_get_usuario_temp;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return clsResultado;
		}

		try {
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, usuarioId);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				ubigeo = new ClsUbigeo();
				userMuni = new ClsMunicipalidad();
				clsUsuario.setUsuario_id(rs.getInt("USUARIO_ID"));
				userMuni.setMunicipalidad_id(rs.getInt("MUNICIPALIDAD_ID"));
				ubigeo.setUbigeoId(rs.getString("UBIGEO_ID"));
				ubigeo.setDepartamento(rs.getString("DEPARTAMENTO"));
				ubigeo.setProvincia(rs.getString("PROVINCIA"));
				ubigeo.setDistrito(rs.getString("DISTRITO"));
				userMuni.setUbigeo(ubigeo);
				clsUsuario.setMunicipalidad(userMuni);
			}

			clsResultado.setObjeto(clsUsuario);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Resultado exitoso");
		} catch (SQLException ex) {
			log.error("ERROR! 1 minam_obtener_usuarioId_activo: "
					+ new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 minam_obtener_usuarioId_activo: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 minam_obtener_usuarioId_activo: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 minam_obtener_usuarioId_activo: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}

}
