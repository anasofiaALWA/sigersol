package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsInversion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.InversionDAO;

public class InversionService {

	private static Logger log = Logger.getLogger(InversionService.class);
	InversionDAO inversionDAO = new InversionDAO();
	
	
	public ClsResultado listarInversion(ClsInversion clsInversion){
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = inversionDAO.listarInversion(clsInversion);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsInversion clsInversion) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = inversionDAO.insertar(clsInversion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsInversion clsInversion) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = inversionDAO.actualizar(clsInversion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
