package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAdmFinanzas;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.AdmFinanzasService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/finanzasX")
@RequestScoped
public class AdmFinanzasRestService {

	private static Logger log = Logger.getLogger(AdmFinanzasRestService.class.getName());
	private Integer accion = null;
	private ClsAdmFinanzas clsAdmFinanzas;
	private AdmFinanzasService admFinanzasService;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	
	
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			admFinanzasService = new AdmFinanzasService();
			
			jsonEntradaFinanzas(paramJson);
			
			log.info("**obtener Administracion Finanzas**");
			
			clsResultado = admFinanzasService.obtener(clsAdmFinanzas);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"admFinanzas\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		admFinanzasService = new AdmFinanzasService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaFinanzas(paramJson);
			clsResultado = admFinanzasService.insertar(clsAdmFinanzas);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}


	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		admFinanzasService = new AdmFinanzasService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaFinanzas(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = admFinanzasService.actualizar(clsAdmFinanzas);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	private void jsonEntradaFinanzas(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AdmFinanzasRestService : jsonEntradaFinanzas" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectAdmFinanzas;
		JSONParser jsonParser = new JSONParser();
		
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		clsAdmFinanzas = new ClsAdmFinanzas();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectAdmFinanzas = (JSONObject) jsonObjectPrincipal.get("AdmFinanzas");
			
			if (jsonObjectAdmFinanzas.get("adm_finanzas_id") != null && jsonObjectAdmFinanzas.get("adm_finanzas_id") != "") {
				clsAdmFinanzas.setAdm_finanzas_id(Integer.parseInt(jsonObjectAdmFinanzas.get("adm_finanzas_id").toString()));
			}
			else {
				clsAdmFinanzas.setAdm_finanzas_id(-1);
			}
			
			if (jsonObjectAdmFinanzas.get("ciclo_grs_id") != null && jsonObjectAdmFinanzas.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectAdmFinanzas.get("ciclo_grs_id").toString()));
			}
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsAdmFinanzas.setCicloGestionResiduos(clsCicloGestionResiduos);
			
			if (jsonObjectAdmFinanzas.get("recaudacion_arbitrios") != null && jsonObjectAdmFinanzas.get("recaudacion_arbitrios") != "") {
				clsAdmFinanzas.setRecaudacion_arbitrios(Integer.parseInt(jsonObjectAdmFinanzas.get("recaudacion_arbitrios").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("cant_predios") != null && jsonObjectAdmFinanzas.get("cant_predios") != "") {
				clsAdmFinanzas.setCant_predios(Integer.parseInt(jsonObjectAdmFinanzas.get("cant_predios").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("predios_afectos") != null && jsonObjectAdmFinanzas.get("predios_afectos") != "") {
				clsAdmFinanzas.setPredios_afectos(Integer.parseInt(jsonObjectAdmFinanzas.get("predios_afectos").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_ambiental") != null && jsonObjectAdmFinanzas.get("flag_ambiental") != "") {
				clsAdmFinanzas.setFlag_ambiental(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_ambiental").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("nombre_ambiental") != null && jsonObjectAdmFinanzas.get("nombre_ambiental") != "") {
				clsAdmFinanzas.setNombre_ambiental(jsonObjectAdmFinanzas.get("nombre_ambiental").toString());
			}
			
			if (jsonObjectAdmFinanzas.get("flag_limpieza") != null && jsonObjectAdmFinanzas.get("flag_limpieza") != "") {
				clsAdmFinanzas.setFlag_limpieza(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_limpieza").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_rentas") != null && jsonObjectAdmFinanzas.get("flag_rentas") != "") {
				clsAdmFinanzas.setFlag_rentas(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_rentas").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_poi") != null && jsonObjectAdmFinanzas.get("flag_poi") != "") {
				clsAdmFinanzas.setFlag_poi(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_poi").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("carga_poi") != null && jsonObjectAdmFinanzas.get("carga_poi") != "") {
				clsAdmFinanzas.setCarga_poi(Integer.parseInt(jsonObjectAdmFinanzas.get("carga_poi").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_pp") != null && jsonObjectAdmFinanzas.get("flag_pp") != "") {
				clsAdmFinanzas.setFlag_pp(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_pp").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("codigo_pp") != null && jsonObjectAdmFinanzas.get("codigo_pp") != "") {
				clsAdmFinanzas.setCodigo_pp(jsonObjectAdmFinanzas.get("codigo_pp").toString());
			}
			
			if (jsonObjectAdmFinanzas.get("monto_programado") != null && jsonObjectAdmFinanzas.get("monto_programado") != "") {
				clsAdmFinanzas.setMonto_programado(Integer.parseInt(jsonObjectAdmFinanzas.get("monto_programado").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("codigo_programado") != null && jsonObjectAdmFinanzas.get("codigo_programado") != "") {
				clsAdmFinanzas.setCodigo_programado(jsonObjectAdmFinanzas.get("codigo_programado").toString());
			}
			
			if (jsonObjectAdmFinanzas.get("monto_ejecutado") != null && jsonObjectAdmFinanzas.get("monto_ejecutado") != "") {
				clsAdmFinanzas.setMonto_ejecutado(Integer.parseInt(jsonObjectAdmFinanzas.get("monto_ejecutado").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("codigo_ejecutado") != null && jsonObjectAdmFinanzas.get("codigo_ejecutado") != "") {
				clsAdmFinanzas.setCodigo_ejecutado(jsonObjectAdmFinanzas.get("codigo_ejecutado").toString());
			}
			
			if (jsonObjectAdmFinanzas.get("flag_pmi") != null && jsonObjectAdmFinanzas.get("flag_pmi") != "") {
				clsAdmFinanzas.setFlag_pmi(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_pmi").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("programado_inv") != null && jsonObjectAdmFinanzas.get("programado_inv") != "") {
				clsAdmFinanzas.setProgramado_inv(Integer.parseInt(jsonObjectAdmFinanzas.get("programado_inv").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("ejecutado_inv") != null && jsonObjectAdmFinanzas.get("ejecutado_inv") != "") {
				clsAdmFinanzas.setEjecutado_inv(Integer.parseInt(jsonObjectAdmFinanzas.get("ejecutado_inv").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_proyecto") != null && jsonObjectAdmFinanzas.get("flag_proyecto") != "") {
				clsAdmFinanzas.setFlag_proyecto(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_proyecto").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("codigo_proyecto") != null && jsonObjectAdmFinanzas.get("codigo_proyecto") != "") {
				clsAdmFinanzas.setCodigo_proyecto(jsonObjectAdmFinanzas.get("codigo_proyecto").toString());
			}
			
			if (jsonObjectAdmFinanzas.get("fuente_financiamiento") != null && jsonObjectAdmFinanzas.get("fuente_financiamiento") != "") {
				clsAdmFinanzas.setFuente_financiamiento(Integer.parseInt(jsonObjectAdmFinanzas.get("fuente_financiamiento").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("inv_no_proyectos") != null && jsonObjectAdmFinanzas.get("inv_no_proyectos") != "") {
				clsAdmFinanzas.setInv_no_proyectos(Integer.parseInt(jsonObjectAdmFinanzas.get("inv_no_proyectos").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_manejo") != null && jsonObjectAdmFinanzas.get("flag_manejo") != "") {
				clsAdmFinanzas.setFlag_manejo(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_manejo").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_pigars") != null && jsonObjectAdmFinanzas.get("flag_pigars") != "") {
				clsAdmFinanzas.setFlag_pigars(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_pigars").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_instrumento") != null && jsonObjectAdmFinanzas.get("flag_instrumento") != "") {
				clsAdmFinanzas.setFlag_instrumento(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_instrumento").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_licencia") != null && jsonObjectAdmFinanzas.get("flag_licencia") != "") {
				clsAdmFinanzas.setFlag_licencia(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_licencia").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_exp_tecnico") != null && jsonObjectAdmFinanzas.get("flag_exp_tecnico") != "") {
				clsAdmFinanzas.setFlag_exp_tecnico(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_exp_tecnico").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_ordenanza") != null && jsonObjectAdmFinanzas.get("flag_ordenanza") != "") {
				clsAdmFinanzas.setFlag_ordenanza(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_ordenanza").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("flag_tributario") != null && jsonObjectAdmFinanzas.get("flag_tributario") != "") {
				clsAdmFinanzas.setFlag_tributario(Integer.parseInt(jsonObjectAdmFinanzas.get("flag_tributario").toString()));
			}
			
			if (jsonObjectAdmFinanzas.get("cod_usuario") != null && jsonObjectAdmFinanzas.get("cod_usuario") != "") {
				clsAdmFinanzas.setCodigo_usuario(Integer.parseInt(jsonObjectAdmFinanzas.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
