package pe.gob.sigersol.entities;

public class ClsDocumentoTransferencia {

	private Integer documento_transferencia_id;
	private ClsMunicipalidad municipalidad;
	private String numero_documento;
	private String nombre_infraestructura;
	private String nombre_autoridad;
	private String fecha_emision;
	private String numero_licencia;
	private String fecha_inicio_licencia;
	private String fecha_fin_licencia;
	private String tipo_documento;
	private String nombre_archivo;
	private String nombre_archivo_ref;
	private String uid_alfresco;
	private Integer codigo_usuario;
	
	public Integer getDocumento_transferencia_id() {
		return documento_transferencia_id;
	}
	public void setDocumento_transferencia_id(Integer documento_transferencia_id) {
		this.documento_transferencia_id = documento_transferencia_id;
	}
	public ClsMunicipalidad getMunicipalidad() {
		return municipalidad;
	}
	public void setMunicipalidad(ClsMunicipalidad municipalidad) {
		this.municipalidad = municipalidad;
	}
	public String getNumero_documento() {
		return numero_documento;
	}
	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}
	public String getNombre_infraestructura() {
		return nombre_infraestructura;
	}
	public void setNombre_infraestructura(String nombre_infraestructura) {
		this.nombre_infraestructura = nombre_infraestructura;
	}
	public String getNombre_autoridad() {
		return nombre_autoridad;
	}
	public void setNombre_autoridad(String nombre_autoridad) {
		this.nombre_autoridad = nombre_autoridad;
	}
	public String getFecha_emision() {
		return fecha_emision;
	}
	public void setFecha_emision(String fecha_emision) {
		this.fecha_emision = fecha_emision;
	}
	public String getNumero_licencia() {
		return numero_licencia;
	}
	public void setNumero_licencia(String numero_licencia) {
		this.numero_licencia = numero_licencia;
	}
	public String getFecha_inicio_licencia() {
		return fecha_inicio_licencia;
	}
	public void setFecha_inicio_licencia(String fecha_inicio_licencia) {
		this.fecha_inicio_licencia = fecha_inicio_licencia;
	}
	public String getFecha_fin_licencia() {
		return fecha_fin_licencia;
	}
	public void setFecha_fin_licencia(String fecha_fin_licencia) {
		this.fecha_fin_licencia = fecha_fin_licencia;
	}
	public String getTipo_documento() {
		return tipo_documento;
	}
	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}
	public String getNombre_archivo() {
		return nombre_archivo;
	}
	public void setNombre_archivo(String nombre_archivo) {
		this.nombre_archivo = nombre_archivo;
	}
	public String getNombre_archivo_ref() {
		return nombre_archivo_ref;
	}
	public void setNombre_archivo_ref(String nombre_archivo_ref) {
		this.nombre_archivo_ref = nombre_archivo_ref;
	}
	public String getUid_alfresco() {
		return uid_alfresco;
	}
	public void setUid_alfresco(String uid_alfresco) {
		this.uid_alfresco = uid_alfresco;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
