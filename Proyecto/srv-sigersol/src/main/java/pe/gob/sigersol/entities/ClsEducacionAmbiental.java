package pe.gob.sigersol.entities;


public class ClsEducacionAmbiental {	
	
	private Integer educacion_ambiental_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private Integer flag_ambiental;
	private Integer flag_minam;
	private Integer flag_actividades;
	private String documento_aprobacion;
	private String eventos_otros;
	private Integer capacit_hombres_otros;
	private Integer capacit_mujeres_otros;
	private Integer codigo_usuario;	
	
	public Integer getEducacion_ambiental_id() {
		return educacion_ambiental_id;
	}
	public void setEducacion_ambiental_id(Integer educacion_ambiental_id) {
		this.educacion_ambiental_id = educacion_ambiental_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public Integer getFlag_ambiental() {
		return flag_ambiental;
	}
	public void setFlag_ambiental(Integer flag_ambiental) {
		this.flag_ambiental = flag_ambiental;
	}
	public Integer getFlag_minam() {
		return flag_minam;
	}
	public void setFlag_minam(Integer flag_minam) {
		this.flag_minam = flag_minam;
	}
	public Integer getFlag_actividades() {
		return flag_actividades;
	}
	public void setFlag_actividades(Integer flag_actividades) {
		this.flag_actividades = flag_actividades;
	}
	public String getDocumento_aprobacion() {
		return documento_aprobacion;
	}
	public void setDocumento_aprobacion(String documento_aprobacion) {
		this.documento_aprobacion = documento_aprobacion;
	}
	public String getEventos_otros() {
		return eventos_otros;
	}
	public void setEventos_otros(String eventos_otros) {
		this.eventos_otros = eventos_otros;
	}
	public Integer getCapacit_hombres_otros() {
		return capacit_hombres_otros;
	}
	public void setCapacit_hombres_otros(Integer capacit_hombres_otros) {
		this.capacit_hombres_otros = capacit_hombres_otros;
	}
	public Integer getCapacit_mujeres_otros() {
		return capacit_mujeres_otros;
	}
	public void setCapacit_mujeres_otros(Integer capacit_mujeres_otros) {
		this.capacit_mujeres_otros = capacit_mujeres_otros;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
