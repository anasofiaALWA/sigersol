package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSolicitud;


public class SolicitudDAO extends GenericoDAO{
	private static Logger log = Logger.getLogger(SolicitudDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_solicitud = "{call SIGERSOLBL.SGR_SP_LIST_SOLICITUD(?)}";
	private final String sgr_sp_ins_solicitud = "{call SIGERSOLBL.SGR_SP_INS_SOLICITUD (?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_solicitud = "{call SIGERSOLBL.SGR_SP_UPD_SOLICITUD (?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener() {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsSolicitud> lista = new ArrayList<ClsSolicitud>();
		con = null;
		String sql = this.sgr_sp_list_solicitud;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);

			while (rs.next()) {
				ClsSolicitud item = new ClsSolicitud();
				try {
					item.setSolicitud_id(rs.getInt("SOLICITUD_ID"));
					item.setCodigo_ubigeo_departamento(rs.getString("COD_UBIGEO_DEP"));
					item.setCodigo_ubigeo_provincia(rs.getString("COD_UBIGEO_PROV"));
					item.setCodigo_ubigeo_distrito(rs.getString("COD_UBIGEO_DIST"));
					item.setRuc_municipalidad(rs.getString("RUC_MUNICIPALIDAD"));
					item.setNombre_alcalde(rs.getString("NOMBRE_ALCALDE"));
					item.setDireccion(rs.getString("DIRECCION"));
					item.setTelefono(rs.getString("TELEFONO"));
					item.setFax(rs.getString("FAX"));
					item.setEmail(rs.getString("EMAIL"));
		
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsSolicitud clsSolicitud) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_solicitud;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setString(1, clsSolicitud.getCodigo_ubigeo_departamento());
			cs.setString(2, clsSolicitud.getCodigo_ubigeo_provincia());
			cs.setString(3, clsSolicitud.getCodigo_ubigeo_distrito());
			cs.setString(4, clsSolicitud.getRuc_municipalidad());
			cs.setString(5, clsSolicitud.getNombre_alcalde());
			cs.setString(6, clsSolicitud.getDireccion());
			cs.setString(7, clsSolicitud.getTelefono());
			cs.setString(8, clsSolicitud.getFax());
			cs.setString(9, clsSolicitud.getEmail());
			cs.setInt(10, clsSolicitud.getCodigo_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);
			
			log.info("SolicitudDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsSolicitud clsSolicitud) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_solicitud;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsSolicitud.getSolicitud_id());
			cs.setString(2, clsSolicitud.getCodigo_ubigeo_departamento());
			cs.setString(3, clsSolicitud.getCodigo_ubigeo_provincia());
			cs.setString(4, clsSolicitud.getCodigo_ubigeo_distrito());
			cs.setString(5, clsSolicitud.getRuc_municipalidad());
			cs.setString(6, clsSolicitud.getNombre_alcalde());
			cs.setString(7, clsSolicitud.getDireccion());
			cs.setString(8, clsSolicitud.getTelefono());
			cs.setString(9, clsSolicitud.getFax());
			cs.setString(10, clsSolicitud.getEmail());
			cs.setInt(11, clsSolicitud.getCodigo_usuario());
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(12);

			log.info("SolicitudDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
