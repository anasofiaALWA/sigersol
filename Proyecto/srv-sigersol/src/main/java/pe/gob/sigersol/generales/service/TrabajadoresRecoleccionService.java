package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTrabajadores;
import pe.gob.sigersol.entities.dao.TrabajadoresRecoleccionDAO;

public class TrabajadoresRecoleccionService {

	private static Logger log = Logger.getLogger(TrabajadoresRecoleccionService.class);
	TrabajadoresRecoleccionDAO trabajadoresRecoleccionDAO = new TrabajadoresRecoleccionDAO();

	public ClsResultado listarTrabajadores(ClsTrabajadores clsTrabajadores){
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresRecoleccionDAO.listarTrabajadores(clsTrabajadores);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTrabajadores clsTrabajadores) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresRecoleccionDAO.insertar(clsTrabajadores);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
