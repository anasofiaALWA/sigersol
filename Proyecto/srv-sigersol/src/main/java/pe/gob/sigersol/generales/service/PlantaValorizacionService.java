package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsPlantaValorizacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.PlantaValorizacionDAO;

public class PlantaValorizacionService {

	private static Logger log = Logger.getLogger(PlantaValorizacionService.class);
	PlantaValorizacionDAO plantaValorizacionDAO = new PlantaValorizacionDAO();
	
	public ClsResultado obtener(ClsPlantaValorizacion clsPlantaValorizacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = plantaValorizacionDAO.obtener(clsPlantaValorizacion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsPlantaValorizacion clsPlantaValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = plantaValorizacionDAO.insertar(clsPlantaValorizacion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsPlantaValorizacion clsPlantaValorizacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = plantaValorizacionDAO.actualizar(clsPlantaValorizacion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
