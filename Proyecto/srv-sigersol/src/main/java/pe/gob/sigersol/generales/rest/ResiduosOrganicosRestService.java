package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResiduosOrganicos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoProductos;
import pe.gob.sigersol.entities.ClsValorizacion;
import pe.gob.sigersol.generales.service.ResiduosOrganicosService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/residuosOrganicos")
@RequestScoped
public class ResiduosOrganicosRestService {

	private static Logger log = Logger.getLogger(ResiduosOrganicosRestService.class.getName());
	private ClsResiduosOrganicos clsResiduosOrganicos;
	private ClsValorizacion clsValorizacion;
	private ClsTipoProductos clsTipoProductos;
	
	private ResiduosOrganicosService residuosOrganicosService;
	private List<ClsResiduosOrganicos> listaResiduosOrganicos;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarResiduosOrganicos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarResiduosOrganicos(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			residuosOrganicosService = new ResiduosOrganicosService();
			
			jsonEntradaListaResiduosAprovechados(paramJson);
			
			log.info("**Obtener Residuos Organicos**");
			
			clsResultado = residuosOrganicosService.listarResiduosOrganicos(clsResiduosOrganicos);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstResiduosOrganicos\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		residuosOrganicosService = new ResiduosOrganicosService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaResiduosAprovechados(paramJson);

			for (ClsResiduosOrganicos clsResiduosOrganicos : listaResiduosOrganicos) {
				clsResultado = residuosOrganicosService.insertar(clsResiduosOrganicos);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		residuosOrganicosService = new ResiduosOrganicosService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaResiduosAprovechados(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsResiduosOrganicos clsResiduosOrganicos : listaResiduosOrganicos) {
			clsResultado = residuosOrganicosService.actualizar(clsResiduosOrganicos);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de ResiduosAprovechados
	 * 
	 * */
	private void jsonEntradaResiduosAprovechados(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService :	jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectTipoResiduo;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaResiduosOrganicos = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsResiduosOrganicos = new ClsResiduosOrganicos();
			clsTipoProductos = new ClsTipoProductos();
			clsValorizacion= new ClsValorizacion();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("valorizacion_id") != null && innerObject.get("valorizacion_id").toString() != "") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(innerObject.get("valorizacion_id").toString()));
			}
			
			clsResiduosOrganicos.setValorizacion(clsValorizacion);
			
			if (innerObject.get("tipoProductos") != null && innerObject.get("tipoProductos").toString() != "") {
				jsonObjectTipoResiduo  = (JSONObject) innerObject.get("tipoProductos");
				clsTipoProductos.setTipo_productos_id(Integer.parseInt(jsonObjectTipoResiduo.get("tipo_productos_id").toString()));
			}
			clsResiduosOrganicos.setTipoProductos(clsTipoProductos);
			
			if (innerObject.get("cantidad") != null && innerObject.get("cantidad").toString() != "") {
				clsResiduosOrganicos.setCantidad(Integer.parseInt(innerObject.get("cantidad").toString()));
			}
			else {
				clsResiduosOrganicos.setCantidad(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsResiduosOrganicos.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaResiduosOrganicos.add(clsResiduosOrganicos);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaResiduosAprovechados(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ResiduosAprovechadosRestService :	 jsonEntradaListaResiduosAprovechados" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResiduosAprovechados;
		JSONParser jsonParser = new JSONParser();
		
		clsResiduosOrganicos = new ClsResiduosOrganicos();
		clsTipoProductos = new ClsTipoProductos();
		clsValorizacion= new ClsValorizacion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResiduosAprovechados = (JSONObject) jsonObjectPrincipal.get("ResiduosOrganicos");
			
			if (jsonObjectResiduosAprovechados.get("valorizacion_id") != null && jsonObjectResiduosAprovechados.get("valorizacion_id") != "" &&
					jsonObjectResiduosAprovechados.get("valorizacion_id") != " ") {
				clsValorizacion.setValorizacion_id(Integer.parseInt(jsonObjectResiduosAprovechados.get("valorizacion_id").toString()));
			}
			else
				clsValorizacion.setValorizacion_id(-1);
			
			clsResiduosOrganicos.setValorizacion(clsValorizacion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
