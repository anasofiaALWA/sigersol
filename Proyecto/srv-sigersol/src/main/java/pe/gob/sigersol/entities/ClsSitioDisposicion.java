package pe.gob.sigersol.entities;

public class ClsSitioDisposicion {

	private Integer sitio_disposicion_id;
	private Integer municipalidad_id;
	private String nombre_sitio;
	private Integer flag_relleno;
	private String denominacion;
	private String ubigeo_id;
	private String coordenada_este;
	private String coordenada_norte;
	private Integer zona;
	private String descripcion_municipalidad;
	
	public Integer getSitio_disposicion_id() {
		return sitio_disposicion_id;
	}
	public void setSitio_disposicion_id(Integer sitio_disposicion_id) {
		this.sitio_disposicion_id = sitio_disposicion_id;
	}
	public Integer getMunicipalidad_id() {
		return municipalidad_id;
	}
	public void setMunicipalidad_id(Integer municipalidad_id) {
		this.municipalidad_id = municipalidad_id;
	}
	public String getNombre_sitio() {
		return nombre_sitio;
	}
	public void setNombre_sitio(String nombre_sitio) {
		this.nombre_sitio = nombre_sitio;
	}
	public Integer getFlag_relleno() {
		return flag_relleno;
	}
	public void setFlag_relleno(Integer flag_relleno) {
		this.flag_relleno = flag_relleno;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getUbigeo_id() {
		return ubigeo_id;
	}
	public void setUbigeo_id(String ubigeo_id) {
		this.ubigeo_id = ubigeo_id;
	}
	public String getCoordenada_este() {
		return coordenada_este;
	}
	public void setCoordenada_este(String coordenada_este) {
		this.coordenada_este = coordenada_este;
	}
	public String getCoordenada_norte() {
		return coordenada_norte;
	}
	public void setCoordenada_norte(String coordenada_norte) {
		this.coordenada_norte = coordenada_norte;
	}
	public Integer getZona() {
		return zona;
	}
	public void setZona(Integer zona) {
		this.zona = zona;
	}
	public String getDescripcion_municipalidad() {
		return descripcion_municipalidad;
	}
	public void setDescripcion_municipalidad(String descripcion_municipalidad) {
		this.descripcion_municipalidad = descripcion_municipalidad;
	}
}
