package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAccionPoi;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AccionPoiDAO;

public class AccionPoiService {

	private static Logger log = Logger.getLogger(AccionPoiService.class);
	AccionPoiDAO accionPoiDAO = new AccionPoiDAO();
	
	public ClsResultado listarAccionPoi(ClsAccionPoi clsAccionPoi){
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionPoiDAO.listarAccionPoi(clsAccionPoi);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAccionPoi clsAccionPoi) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionPoiDAO.insertar(clsAccionPoi);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsAccionPoi clsAccionPoi) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = accionPoiDAO.actualizar(clsAccionPoi);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
