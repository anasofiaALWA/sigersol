package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTransferencia;
import pe.gob.sigersol.generales.service.TransferenciaService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase TransferenciaRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de Transferencia, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/transferencia")
@RequestScoped
public class TransferenciaRestService {
	
	private static Logger log = Logger.getLogger(TransferenciaRestService.class.getName());
	private ClsTransferencia clsTransferencia;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private TransferenciaService transferenciaService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			transferenciaService = new TransferenciaService();
			
			jsonEntradaTransferencia(paramJson);
			
			log.info("**obtener transferencia**");
			
			clsResultado = transferenciaService.obtener(clsTransferencia);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"transferencia\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		transferenciaService = new TransferenciaService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaTransferencia(paramJson);
			clsResultado = transferenciaService.insertar(clsTransferencia);	
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		transferenciaService = new TransferenciaService();
		
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaTransferencia(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = transferenciaService.actualizar(clsTransferencia);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Transferencia  
	 * 
	 * */
	private void jsonEntradaTransferencia(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> TransferenciaRestService :jsonEntradaTransferencia" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectTransferencia;
		JSONParser jsonParser = new JSONParser();
		
		clsTransferencia = new ClsTransferencia();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectTransferencia = (JSONObject) jsonObjectPrincipal.get("Transferencia");
			
			if (jsonObjectTransferencia.get("transferencia_id") != null && jsonObjectTransferencia.get("transferencia_id") != "") {
				clsTransferencia.setTransferencia_id(Integer.parseInt(jsonObjectTransferencia.get("transferencia_id").toString()));
			}
			
			if (jsonObjectTransferencia.get("ciclo_grs_id") != null && jsonObjectTransferencia.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectTransferencia.get("ciclo_grs_id").toString()));
			}
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsTransferencia.setClsCicloGestionResiduos(clsCicloGestionResiduos);
			
			if (jsonObjectTransferencia.get("flag_transferencia") != null && jsonObjectTransferencia.get("flag_transferencia") != "") {
				clsTransferencia.setFlag_transferencia(Integer.parseInt(jsonObjectTransferencia.get("flag_transferencia").toString()));
			}
			
			if (jsonObjectTransferencia.get("adm_serv_id") != null && jsonObjectTransferencia.get("adm_serv_id") != "") {
				clsTransferencia.setAdm_serv_id(jsonObjectTransferencia.get("adm_serv_id").toString());
			}
			
			if (jsonObjectTransferencia.get("zona_coordenada") != null && jsonObjectTransferencia.get("zona_coordenada") != "") {
				clsTransferencia.setZona_coordenada(jsonObjectTransferencia.get("zona_coordenada").toString());
			}
			
			if (jsonObjectTransferencia.get("norte_coordenada") != null && jsonObjectTransferencia.get("norte_coordenada") != "") {
				clsTransferencia.setNorte_coordenada(jsonObjectTransferencia.get("norte_coordenada").toString());
			}
			
			if (jsonObjectTransferencia.get("este_coordenada") != null && jsonObjectTransferencia.get("este_coordenada") != "") {
				clsTransferencia.setEste_coordenada(jsonObjectTransferencia.get("este_coordenada").toString());
			}
	
			if (jsonObjectTransferencia.get("division_politica") != null && jsonObjectTransferencia.get("division_politica") != "") {
				clsTransferencia.setDivision_politica(jsonObjectTransferencia.get("division_politica").toString());
			}
			
			if (jsonObjectTransferencia.get("departamento") != null && jsonObjectTransferencia.get("departamento") != "") {
				clsTransferencia.setDepartamento(jsonObjectTransferencia.get("departamento").toString());
			}
			
			if (jsonObjectTransferencia.get("provincia") != null && jsonObjectTransferencia.get("provincia") != "") {
				clsTransferencia.setProvincia(jsonObjectTransferencia.get("provincia").toString());
			}
			
			if (jsonObjectTransferencia.get("distrito") != null && jsonObjectTransferencia.get("distrito") != "") {
				clsTransferencia.setDistrito(jsonObjectTransferencia.get("distrito").toString());
			}
			
			if (jsonObjectTransferencia.get("direccion_iga") != null && jsonObjectTransferencia.get("direccion_iga") != "") {
				clsTransferencia.setDireccion_iga(jsonObjectTransferencia.get("direccion_iga").toString());
			}
			
			if (jsonObjectTransferencia.get("referencia_iga") != null && jsonObjectTransferencia.get("referencia_iga") != "") {
				clsTransferencia.setReferencia_iga(jsonObjectTransferencia.get("referencia_iga").toString());
			}
			
			if (jsonObjectTransferencia.get("adm_serv_id") != null && jsonObjectTransferencia.get("adm_serv_id") != "") {
				clsTransferencia.setAdm_serv_id(jsonObjectTransferencia.get("adm_serv_id").toString());
			}
			
			if (jsonObjectTransferencia.get("residuos_transferidos") != null && jsonObjectTransferencia.get("residuos_transferidos") != "") {
				clsTransferencia.setResiduos_transferidos(Integer.parseInt(jsonObjectTransferencia.get("residuos_transferidos").toString()));
			}
			
			if (jsonObjectTransferencia.get("costo_anual") != null && jsonObjectTransferencia.get("costo_anual") != "") {
				clsTransferencia.setCosto_anual(Integer.parseInt(jsonObjectTransferencia.get("costo_anual").toString()));
			}
			
			if (jsonObjectTransferencia.get("frecuencia_diaria") != null && jsonObjectTransferencia.get("frecuencia_diaria") != "") {
				clsTransferencia.setFrecuencia_diaria(Integer.parseInt(jsonObjectTransferencia.get("frecuencia_diaria").toString()));
			}
			
			if (jsonObjectTransferencia.get("distancia_promedio") != null && jsonObjectTransferencia.get("distancia_promedio") != "") {
				clsTransferencia.setDistancia_promedio(Integer.parseInt(jsonObjectTransferencia.get("distancia_promedio").toString()));
			}
			
			if (jsonObjectTransferencia.get("cod_usuario") != null && jsonObjectTransferencia.get("cod_usuario") != "") {
				clsTransferencia.setCodigo_usuario(Integer.parseInt(jsonObjectTransferencia.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
