package pe.gob.sigersol.entities;

public class ClsRecoleccionVehiculo {
	
	private Integer recoleccion_vehiculo_id;
	private ClsRecoleccion recoleccion;
	private ClsTipoVehiculo tipo_vehiculo;
	private Integer cantidad;
	private Integer numero_llantas;
	private Double costo_llanta;
	private Integer codigo_usuario;
	
	public Integer getRecoleccion_vehiculo_id() {
		return recoleccion_vehiculo_id;
	}
	public void setRecoleccion_vehiculo_id(Integer recoleccion_vehiculo_id) {
		this.recoleccion_vehiculo_id = recoleccion_vehiculo_id;
	}
	public ClsRecoleccion getRecoleccion() {
		return recoleccion;
	}
	public void setRecoleccion(ClsRecoleccion recoleccion) {
		this.recoleccion = recoleccion;
	}
	public ClsTipoVehiculo getTipo_vehiculo() {
		return tipo_vehiculo;
	}
	public void setTipo_vehiculo(ClsTipoVehiculo tipo_vehiculo) {
		this.tipo_vehiculo = tipo_vehiculo;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getNumero_llantas() {
		return numero_llantas;
	}
	public void setNumero_llantas(Integer numero_llantas) {
		this.numero_llantas = numero_llantas;
	}
	public Double getCosto_llanta() {
		return costo_llanta;
	}
	public void setCosto_llanta(Double costo_llanta) {
		this.costo_llanta = costo_llanta;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
