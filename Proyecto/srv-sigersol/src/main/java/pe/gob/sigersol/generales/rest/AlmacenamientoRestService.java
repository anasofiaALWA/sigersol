package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAlmacenamiento;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.AlmacenamientoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase AlmacenamientoRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de Almacenamiento, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */


@Path("/almacenamiento")
@RequestScoped
public class AlmacenamientoRestService {
	
	private static Logger log = Logger.getLogger(AlmacenamientoRestService.class.getName());
	private ClsAlmacenamiento clsAlmacenamiento;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private AlmacenamientoService almacenamientoService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			almacenamientoService = new AlmacenamientoService();
			
			jsonEntradaAlmacenamiento(paramJson);
			log.info("**obtener almacenamiento**");
			clsResultado = almacenamientoService.obtener(clsAlmacenamiento);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"almacenamiento\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		almacenamientoService = new AlmacenamientoService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaAlmacenamiento(paramJson);
			clsResultado = almacenamientoService.insertar(clsAlmacenamiento);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		almacenamientoService = new AlmacenamientoService();
		
		log.info("paramJson: " + paramJson);
		
		try {
			jsonEntradaAlmacenamiento(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = almacenamientoService.actualizar(clsAlmacenamiento);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Almacenamiento  
	 * 
	 * */
	private void jsonEntradaAlmacenamiento(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AlmacenamientoRestService :jsonEntradaAlmacenamiento" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectAlmacenamiento;
		JSONParser jsonParser = new JSONParser();
		
		clsAlmacenamiento = new ClsAlmacenamiento();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectAlmacenamiento = (JSONObject) jsonObjectPrincipal.get("Almacenamiento");
			
			if (jsonObjectAlmacenamiento.get("almacenamiento_id") != null && jsonObjectAlmacenamiento.get("almacenamiento_id") != "") {
				clsAlmacenamiento.setAlmacenamiento_id(Integer.parseInt(jsonObjectAlmacenamiento.get("almacenamiento_id").toString()));
			}
			
			if (jsonObjectAlmacenamiento.get("ciclo_grs_id") != null && jsonObjectAlmacenamiento.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectAlmacenamiento.get("ciclo_grs_id").toString()));
			}
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
		
			clsAlmacenamiento.setCicloGestionResiduos(clsCicloGestionResiduos);

			if (jsonObjectAlmacenamiento.get("costo_anual") != null && jsonObjectAlmacenamiento.get("costo_anual") != "") {
				clsAlmacenamiento.setCosto_anual(Integer.parseInt(jsonObjectAlmacenamiento.get("costo_anual").toString()));
			}
			else
				clsAlmacenamiento.setCosto_anual(0);
			
			if (jsonObjectAlmacenamiento.get("capacidad_papeleras") != null && jsonObjectAlmacenamiento.get("capacidad_papeleras") != "") {
				clsAlmacenamiento.setCapacidad_papeleras(Integer.parseInt(jsonObjectAlmacenamiento.get("capacidad_papeleras").toString()));
			}
			else
				clsAlmacenamiento.setCapacidad_papeleras(0);
			
			if (jsonObjectAlmacenamiento.get("metal") != null && jsonObjectAlmacenamiento.get("metal") != "") {
				if(jsonObjectAlmacenamiento.get("metal").toString() == "true")
					clsAlmacenamiento.setMetal(1);
				else
					clsAlmacenamiento.setMetal(0);
			}
			else {
				clsAlmacenamiento.setMetal(0);
			}
			
			if (jsonObjectAlmacenamiento.get("fibra_vidrio") != null && jsonObjectAlmacenamiento.get("fibra_vidrio") != "") {
				if(jsonObjectAlmacenamiento.get("fibra_vidrio").toString() == "true")
					clsAlmacenamiento.setFibra_vidrio(1);
				else
					clsAlmacenamiento.setFibra_vidrio(0);
			} 
			else{
				clsAlmacenamiento.setFibra_vidrio(0);
			}
			
			if (jsonObjectAlmacenamiento.get("plastico") != null && jsonObjectAlmacenamiento.get("plastico") != "") {
				if(jsonObjectAlmacenamiento.get("plastico").toString() == "true")
					clsAlmacenamiento.setPlastico(1);
				else
					clsAlmacenamiento.setPlastico(0);
			}
			else {
				clsAlmacenamiento.setPlastico(0);
			}
			
			if (jsonObjectAlmacenamiento.get("mixto") != null && jsonObjectAlmacenamiento.get("mixto") != "") {
				if(jsonObjectAlmacenamiento.get("mixto").toString() == "true")
					clsAlmacenamiento.setMixto(1);
				else
					clsAlmacenamiento.setMixto(0);
			}
			else {
				clsAlmacenamiento.setMixto(0);
			}
			
			if (jsonObjectAlmacenamiento.get("descripcion_mixto") != null && jsonObjectAlmacenamiento.get("descripcion_mixto") != "") {
				clsAlmacenamiento.setDescripcion_mixto(jsonObjectAlmacenamiento.get("descripcion_mixto").toString());
			}
			else {
				clsAlmacenamiento.setDescripcion_mixto("");
			}
			
			if (jsonObjectAlmacenamiento.get("total_recolectado") != null && jsonObjectAlmacenamiento.get("total_recolectado") != "") {
				clsAlmacenamiento.setTotal_recolectado(Double.valueOf(jsonObjectAlmacenamiento.get("total_recolectado").toString()));
			}
			else
				clsAlmacenamiento.setTotal_recolectado(0.0);
			
			if (jsonObjectAlmacenamiento.get("cod_usuario") != null && jsonObjectAlmacenamiento.get("cod_usuario") != "") {
				clsAlmacenamiento.setCodigo_usuario(Integer.parseInt(jsonObjectAlmacenamiento.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
