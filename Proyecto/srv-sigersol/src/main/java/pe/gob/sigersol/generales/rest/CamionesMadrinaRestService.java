package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCamionesMadrina;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoCombustible;
import pe.gob.sigersol.entities.ClsTransferencia;
import pe.gob.sigersol.generales.service.CamionesMadrinaService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/camionesMadrinaX")
@RequestScoped
public class CamionesMadrinaRestService {

	private static Logger log = Logger.getLogger(CamionesMadrinaRestService.class.getName());
	private ClsCamionesMadrina clsCamionesMadrina;
	private ClsTransferencia clsTransferencia;
	private ClsTipoCombustible clsTipoCombustible;
	private CamionesMadrinaService camionesMadrinaService;
	private List<ClsCamionesMadrina> listaCamionesMadrina;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCamionesMadrina")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCamionesMadrina(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			camionesMadrinaService = new CamionesMadrinaService();
			
			jsonEntradaListaCamionesMadrina(paramJson);
			
			log.info("**Obtener Camiones Madrina");
			
			clsResultado = camionesMadrinaService.listarCamionesMadrina(clsCamionesMadrina);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCamionesMadrina\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		camionesMadrinaService = new CamionesMadrinaService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaCamionesMadrina(paramJson);
	
			for (ClsCamionesMadrina clsCamionesMadrina : listaCamionesMadrina) {
				clsResultado = camionesMadrinaService.insertar(clsCamionesMadrina);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		camionesMadrinaService = new CamionesMadrinaService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCamionesMadrina(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCamionesMadrina clsCamionesMadrina : listaCamionesMadrina) {
			clsResultado = camionesMadrinaService.actualizar(clsCamionesMadrina);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de VehiculoRecoleccion
	 * 
	 * */
	private void jsonEntradaCamionesMadrina(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService : jsonEntradaVehiculos" + jsonEntrada);

		JSONObject jsonObjectTipoCombustible;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCamionesMadrina = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCamionesMadrina = new ClsCamionesMadrina();
			clsTipoCombustible = new ClsTipoCombustible();
			clsTransferencia = new ClsTransferencia();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("transferencia_id") != null && innerObject.get("transferencia_id").toString() != "") {
				clsTransferencia.setTransferencia_id(Integer.parseInt(innerObject.get("transferencia_id").toString()));
			} 
			else {
				clsTransferencia.setTransferencia_id(0);
			}
			
			clsCamionesMadrina.setTransferencia(clsTransferencia);
			
			if (innerObject.get("tipo_combustible") != null && innerObject.get("tipo_combustible").toString() != "") {
				jsonObjectTipoCombustible  = (JSONObject) innerObject.get("tipo_combustible");
				clsTipoCombustible.setTipo_combustible_id(Integer.parseInt(jsonObjectTipoCombustible.get("tipo_combustible_id").toString()));
			}
			clsCamionesMadrina.setTipo_combustible(clsTipoCombustible);
			
			if (innerObject.get("cantidad_camiones") != null && innerObject.get("cantidad_camiones").toString() != "") {
				clsCamionesMadrina.setCantidad_camiones(Integer.parseInt(innerObject.get("cantidad_camiones").toString()));
			} else{
				clsCamionesMadrina.setCantidad_camiones(0);
			}
			
			if (innerObject.get("recorrido_anual") != null && innerObject.get("recorrido_anual").toString() != "") {
				clsCamionesMadrina.setRecorrido_anual(Integer.parseInt(innerObject.get("recorrido_anual").toString()));
			} else{
				clsCamionesMadrina.setRecorrido_anual(0);
			}
		
			if (innerObject.get("cantidad_combustible") != null && innerObject.get("cantidad_combustible").toString() != "") {
				clsCamionesMadrina.setCantidad_combustible(Integer.parseInt(innerObject.get("cantidad_combustible").toString()));
			} else{
				clsCamionesMadrina.setCantidad_combustible(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCamionesMadrina.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCamionesMadrina.add(clsCamionesMadrina);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCamionesMadrina(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> VehiculoRecoleccionRestService : jsonEntradaVehiculoRecoleccion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectCamionesMadrina;
		JSONParser jsonParser = new JSONParser();
		
		clsCamionesMadrina = new ClsCamionesMadrina();
		clsTipoCombustible = new ClsTipoCombustible();
		clsTransferencia = new ClsTransferencia();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectCamionesMadrina = (JSONObject) jsonObjectPrincipal.get("CamionesMadrina");
			
			if (jsonObjectCamionesMadrina.get("transferencia_id") != null && jsonObjectCamionesMadrina.get("transferencia_id") != "" &&
					jsonObjectCamionesMadrina.get("transferencia_id") != " ") {
				clsTransferencia.setTransferencia_id(Integer.parseInt(jsonObjectCamionesMadrina.get("transferencia_id").toString()));
			}
			else
				clsTransferencia.setTransferencia_id(-1);
			
			clsCamionesMadrina.setTransferencia(clsTransferencia);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
