package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCamionesMadrina;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoCombustible;

public class CamionesMadrinaDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(CamionesMadrinaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_camiones_madrinas = "{call SIGERSOLBL.SGR_SP_INS_CAMIONES_MADRINAS(?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_camiones_madrinas= "{call SIGERSOLBL.SGR_SP_UPD_CAMIONES_MADRINAS(?,?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_camiones_madrinas = "{call SIGERSOLBL.SGR_SP_LIST_CAMIONES_MADRINAS(?,?)}";  
	
	public ClsResultado listarCamionesMadrina(ClsCamionesMadrina clsCamionesMadrina){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCamionesMadrina> lista = new ArrayList<ClsCamionesMadrina>();
		con = null;
		String sql = this.sgr_sp_list_camiones_madrinas;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCamionesMadrina.getTransferencia().getTransferencia_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsCamionesMadrina item = new ClsCamionesMadrina();
				ClsTipoCombustible tipoCombustible = new ClsTipoCombustible();
				
				try {
					tipoCombustible.setTipo_combustible_id(rs.getInt("TIPO_COMBUSTIBLE_ID"));
					tipoCombustible.setNombre_tipo(rs.getString("NOMBRE_TIPO"));
					tipoCombustible.setMedida(rs.getString("MEDIDA"));
					item.setTipo_combustible(tipoCombustible);
					if(clsCamionesMadrina.getTransferencia().getTransferencia_id() == -1){
						item.setCantidad_camiones(null);
						item.setRecorrido_anual(null);
						item.setCantidad_combustible(null);
					} else{
						item.setCantidad_camiones(rs.getInt("CANTIDAD_CAMIONES"));
						item.setRecorrido_anual(rs.getInt("RECORRIDO_ANUAL"));
						item.setCantidad_combustible(rs.getInt("CANTIDAD_COMBUSTIBLE"));
					}
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCamionesMadrina clsCamionesMadrina) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_camiones_madrinas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCamionesMadrina.getTransferencia().getTransferencia_id());
			cs.setInt(2, clsCamionesMadrina.getTipo_combustible().getTipo_combustible_id());
			cs.setInt(3, clsCamionesMadrina.getCantidad_camiones());
			cs.setInt(4, clsCamionesMadrina.getRecorrido_anual());
			cs.setInt(5, clsCamionesMadrina.getCantidad_combustible());
			cs.setInt(6, clsCamionesMadrina.getCodigo_usuario());
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(7);
			
			log.info("CamionesMadrinaDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsCamionesMadrina clsCamionesMadrina) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_camiones_madrinas;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCamionesMadrina.getTransferencia().getTransferencia_id());
			cs.setInt(2, clsCamionesMadrina.getTipo_combustible().getTipo_combustible_id());
			cs.setInt(3, clsCamionesMadrina.getCantidad_camiones());
			cs.setInt(4, clsCamionesMadrina.getRecorrido_anual());
			cs.setInt(5, clsCamionesMadrina.getCantidad_combustible());
			cs.setInt(6, clsCamionesMadrina.getCodigo_usuario());
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(7);

			log.info("CamionesMadrinaDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
