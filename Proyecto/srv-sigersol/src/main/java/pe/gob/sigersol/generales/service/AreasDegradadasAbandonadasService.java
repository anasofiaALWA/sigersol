package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAreasDegradadasAbandonadas;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AreasDegradadasAbandonadasDAO;

public class AreasDegradadasAbandonadasService {

	private static Logger log = Logger.getLogger(AreasDegradadasAbandonadasService.class);
	AreasDegradadasAbandonadasDAO areasDegradadasAbandonadasDAO = new AreasDegradadasAbandonadasDAO();
	
	public ClsResultado obtener(ClsAreasDegradadasAbandonadas clsAreasDegradadasAbandonadas){
		ClsResultado clsResultado = null;
		try {
			clsResultado = areasDegradadasAbandonadasDAO.obtener(clsAreasDegradadasAbandonadas);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAreasDegradadasAbandonadas clsAreasDegradadasAbandonadas) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = areasDegradadasAbandonadasDAO.insertar(clsAreasDegradadasAbandonadas);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsAreasDegradadasAbandonadas clsAreasDegradadasAbandonadas) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = areasDegradadasAbandonadasDAO.actualizar(clsAreasDegradadasAbandonadas);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
