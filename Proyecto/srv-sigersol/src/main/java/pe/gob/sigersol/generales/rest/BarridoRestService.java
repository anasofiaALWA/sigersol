package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsBarrido;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.BarridoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase BarridoRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de Barrido, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/barrido")
@RequestScoped
public class BarridoRestService {
	
	private static Logger log = Logger.getLogger(BarridoRestService.class.getName());
	private ClsBarrido clsBarrido;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private BarridoService barridoService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			barridoService = new BarridoService();
			
			jsonEntradaBarrido(paramJson);
			
			log.info("**obtener barrido**");
			
			clsResultado = barridoService.obtener(clsBarrido);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"barrido\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		barridoService = new BarridoService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaBarrido(paramJson);
			clsResultado = barridoService.insertar(clsBarrido);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		barridoService = new BarridoService();
	
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaBarrido(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = barridoService.actualizar(clsBarrido);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Barrido  
	 * 
	 * */
	private void jsonEntradaBarrido(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> BarridoRestService :jsonEntradaBarrido" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectBarrido;
		JSONParser jsonParser = new JSONParser();
		
		clsBarrido = new ClsBarrido();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectBarrido = (JSONObject) jsonObjectPrincipal.get("Barrido");
			
			if (jsonObjectBarrido.get("barrido_id") != null && jsonObjectBarrido.get("barrido_id") != "") {
				clsBarrido.setBarrido_id(Integer.parseInt(jsonObjectBarrido.get("barrido_id").toString()));
			}
			
			if (jsonObjectBarrido.get("ciclo_grs_id") != null && jsonObjectBarrido.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectBarrido.get("ciclo_grs_id").toString()));
			}
			else 
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsBarrido.setCicloGestionResiduos(clsCicloGestionResiduos);
				
			if (jsonObjectBarrido.get("adm_serv_id") != null && jsonObjectBarrido.get("adm_serv_id") != "") {
				clsBarrido.setAdm_serv_id(Integer.parseInt(jsonObjectBarrido.get("adm_serv_id").toString()));
			}
			else
				clsBarrido.setAdm_serv_id(0);
			
			if (jsonObjectBarrido.get("costo_anual") != null && jsonObjectBarrido.get("costo_anual") != "") {
				clsBarrido.setCosto_anual(Integer.parseInt(jsonObjectBarrido.get("costo_anual").toString()));
			}
			else
				clsBarrido.setCosto_anual(0);
			
			if (jsonObjectBarrido.get("via_asfaltada") != null && jsonObjectBarrido.get("via_asfaltada") != "") {
				clsBarrido.setVia_asfaltada(Integer.parseInt(jsonObjectBarrido.get("via_asfaltada").toString()));
			}
			else
				clsBarrido.setVia_asfaltada(0);
			
			if (jsonObjectBarrido.get("via_no_asfaltada") != null && jsonObjectBarrido.get("via_no_asfaltada") != "") {
				clsBarrido.setVia_no_asfaltada(Integer.parseInt(jsonObjectBarrido.get("via_no_asfaltada").toString()));
			}
			else
				clsBarrido.setVia_no_asfaltada(0);
			
			if (jsonObjectBarrido.get("plazas") != null && jsonObjectBarrido.get("plazas") != "") {
				clsBarrido.setPlazas(Integer.parseInt(jsonObjectBarrido.get("plazas").toString()));
			}
			else
				clsBarrido.setPlazas(0);
			
			if (jsonObjectBarrido.get("playas") != null && jsonObjectBarrido.get("playas") != "") {
				clsBarrido.setPlayas(Integer.parseInt(jsonObjectBarrido.get("playas").toString()));
			}
			else
				clsBarrido.setPlayas(0);
			
			if (jsonObjectBarrido.get("demanda_barrido") != null && jsonObjectBarrido.get("demanda_barrido") != "") {
				clsBarrido.setDemanda_barrido(Integer.parseInt(jsonObjectBarrido.get("demanda_barrido").toString()));
			}
			else
				clsBarrido.setDemanda_barrido(0);
			
			if (jsonObjectBarrido.get("otros_barridos") != null && jsonObjectBarrido.get("otros_barridos") != "") {
				clsBarrido.setOtros_barridos(jsonObjectBarrido.get("otros_barridos").toString());
			}
			else
				clsBarrido.setOtros_barridos("");
			
			if (jsonObjectBarrido.get("otros_demanda") != null && jsonObjectBarrido.get("otros_demanda") != "") {
				clsBarrido.setOtros_demanda(Integer.parseInt(jsonObjectBarrido.get("otros_demanda").toString()));
			}
			else
				clsBarrido.setOtros_demanda(0);
			
			if (jsonObjectBarrido.get("fr1") != null && jsonObjectBarrido.get("fr1") != "") {
				clsBarrido.setFr1(Integer.parseInt(jsonObjectBarrido.get("fr1").toString()));
			}
			else
				clsBarrido.setFr1(0);
			
			if (jsonObjectBarrido.get("fr2") != null && jsonObjectBarrido.get("fr2") != "") {
				clsBarrido.setFr2(Integer.parseInt(jsonObjectBarrido.get("fr2").toString()));
				
				clsBarrido.setFrecuencia(clsBarrido.getFr1().toString().concat("/").concat(clsBarrido.getFr2().toString()));
			}
			else{
				clsBarrido.setFr2(0);
				clsBarrido.setFrecuencia("");
			}
			
			if (jsonObjectBarrido.get("oferta") != null && jsonObjectBarrido.get("oferta") != "") {
				clsBarrido.setOferta(Integer.parseInt(jsonObjectBarrido.get("oferta").toString()));
			}
			else
				clsBarrido.setOferta(0);
			
			if (jsonObjectBarrido.get("hombre_25") != null && jsonObjectBarrido.get("hombre_25") != "") {
				clsBarrido.setHombre_25(Integer.parseInt(jsonObjectBarrido.get("hombre_25").toString()));
			}
			else
				clsBarrido.setHombre_25(0);
			
			if (jsonObjectBarrido.get("mujer_25") != null && jsonObjectBarrido.get("mujer_25") != "") {
				clsBarrido.setMujer_25(Integer.parseInt(jsonObjectBarrido.get("mujer_25").toString()));
			}
			else
				clsBarrido.setMujer_25(0);
			
			if (jsonObjectBarrido.get("hombre_25_65") != null && jsonObjectBarrido.get("hombre_25_65") != "") {
				clsBarrido.setHombre_25_65(Integer.parseInt(jsonObjectBarrido.get("hombre_25_65").toString()));
			}
			else
				clsBarrido.setHombre_25_65(0);
			
			if (jsonObjectBarrido.get("mujer_25_65") != null && jsonObjectBarrido.get("mujer_25_65") != "") {
				clsBarrido.setMujer_25_65(Integer.parseInt(jsonObjectBarrido.get("mujer_25_65").toString()));
			}
			else
				clsBarrido.setMujer_25_65(0);
			
			if (jsonObjectBarrido.get("hombre_65") != null && jsonObjectBarrido.get("hombre_65") != "") {
				clsBarrido.setHombre_65(Integer.parseInt(jsonObjectBarrido.get("hombre_65").toString()));
			}
			else
				clsBarrido.setHombre_65(0);
			
			if (jsonObjectBarrido.get("mujer_65") != null && jsonObjectBarrido.get("mujer_65") != "") {
				clsBarrido.setMujer_65(Integer.parseInt(jsonObjectBarrido.get("mujer_65").toString()));
			}
			else
				clsBarrido.setMujer_65(0);
			
			if (jsonObjectBarrido.get("cr_hombre_25") != null && jsonObjectBarrido.get("cr_hombre_25") != "") {
				clsBarrido.setCr_hombre_25(Integer.parseInt(jsonObjectBarrido.get("cr_hombre_25").toString()));
			}
			else
				clsBarrido.setCr_hombre_25(0);
			
			if (jsonObjectBarrido.get("cr_hombre_25_65") != null && jsonObjectBarrido.get("cr_hombre_25_65") != "") {
				clsBarrido.setCr_hombre_25_65(Integer.parseInt(jsonObjectBarrido.get("cr_hombre_25_65").toString()));
			}
			else
				clsBarrido.setCr_hombre_25_65(0);
			
			if (jsonObjectBarrido.get("cr_hombre_65") != null && jsonObjectBarrido.get("cr_hombre_65") != "") {
				clsBarrido.setCr_hombre_65(Integer.parseInt(jsonObjectBarrido.get("cr_hombre_65").toString()));
			}
			else
				clsBarrido.setCr_hombre_65(0);
			
			if (jsonObjectBarrido.get("cpr_hombre_25") != null && jsonObjectBarrido.get("cpr_hombre_25") != "") {
				clsBarrido.setCpr_hombre_25(Integer.parseInt(jsonObjectBarrido.get("cpr_hombre_25").toString()));
			}
			else
				clsBarrido.setCpr_hombre_25(0);
			
			if (jsonObjectBarrido.get("cpr_hombre_25_65") != null && jsonObjectBarrido.get("cpr_hombre_25_65") != "") {
				clsBarrido.setCpr_hombre_25_65(Integer.parseInt(jsonObjectBarrido.get("cpr_hombre_25_65").toString()));
			}
			else
				clsBarrido.setCpr_hombre_25_65(0);
			
			if (jsonObjectBarrido.get("cpr_hombre_65") != null && jsonObjectBarrido.get("cpr_hombre_65") != "") {
				clsBarrido.setCpr_hombre_65(Integer.parseInt(jsonObjectBarrido.get("cpr_hombre_65").toString()));
			}
			else
				clsBarrido.setCpr_hombre_65(0);
			
			if (jsonObjectBarrido.get("cr_mujer_25") != null && jsonObjectBarrido.get("cr_mujer_25") != "") {
				clsBarrido.setCr_mujer_25(Integer.parseInt(jsonObjectBarrido.get("cr_mujer_25").toString()));
			}
			else
				clsBarrido.setCr_mujer_25(0);
			
			if (jsonObjectBarrido.get("cr_mujer_25_65") != null && jsonObjectBarrido.get("cr_mujer_25_65") != "") {
				clsBarrido.setCr_mujer_25_65(Integer.parseInt(jsonObjectBarrido.get("cr_mujer_25_65").toString()));
			}
			else
				clsBarrido.setCr_mujer_25_65(0);
			
			if (jsonObjectBarrido.get("cr_mujer_65") != null && jsonObjectBarrido.get("cr_mujer_65") != "") {
				clsBarrido.setCr_mujer_65(Integer.parseInt(jsonObjectBarrido.get("cr_mujer_65").toString()));
			}
			else
				clsBarrido.setCr_mujer_65(0);
			
			if (jsonObjectBarrido.get("cpr_mujer_25") != null && jsonObjectBarrido.get("cpr_mujer_25") != "") {
				clsBarrido.setCpr_mujer_25(Integer.parseInt(jsonObjectBarrido.get("cpr_mujer_25").toString()));
			}
			else
				clsBarrido.setCpr_mujer_25(0);
			
			if (jsonObjectBarrido.get("cpr_mujer_25_65") != null && jsonObjectBarrido.get("cpr_mujer_25_65") != "") {
				clsBarrido.setCpr_mujer_25_65(Integer.parseInt(jsonObjectBarrido.get("cpr_mujer_25_65").toString()));
			}
			else
				clsBarrido.setCpr_mujer_25_65(0);
			
			if (jsonObjectBarrido.get("cpr_mujer_65") != null && jsonObjectBarrido.get("cpr_mujer_65") != "") {
				clsBarrido.setCpr_mujer_65(Integer.parseInt(jsonObjectBarrido.get("cpr_mujer_65").toString()));
			}
			else
				clsBarrido.setCpr_mujer_65(0);
			
			if (jsonObjectBarrido.get("cr_25") != null && jsonObjectBarrido.get("cr_25") != "") {
				clsBarrido.setCr_25(Integer.parseInt(jsonObjectBarrido.get("cr_25").toString()));
			}
			else
				clsBarrido.setCr_25(0);
			
			if (jsonObjectBarrido.get("cr_25_65") != null && jsonObjectBarrido.get("cr_25_65") != "") {
				clsBarrido.setCr_25_65(Integer.parseInt(jsonObjectBarrido.get("cr_25_65").toString()));
			}
			else
				clsBarrido.setCr_25_65(0);
			
			if (jsonObjectBarrido.get("cr_65") != null && jsonObjectBarrido.get("cr_65") != "") {
				clsBarrido.setCr_65(Integer.parseInt(jsonObjectBarrido.get("cr_65").toString()));
			}
			else
				clsBarrido.setCr_65(0);
			
			if (jsonObjectBarrido.get("cpr_25") != null && jsonObjectBarrido.get("cpr_25") != "") {
				clsBarrido.setCpr_25(Integer.parseInt(jsonObjectBarrido.get("cpr_25").toString()));
			}
			else
				clsBarrido.setCpr_25(0);
			
			if (jsonObjectBarrido.get("cpr_25_65") != null && jsonObjectBarrido.get("cpr_25_65") != "") {
				clsBarrido.setCpr_25_65(Integer.parseInt(jsonObjectBarrido.get("cpr_25_65").toString()));
			}
			else
				clsBarrido.setCpr_25_65(0);
			
			if (jsonObjectBarrido.get("cpr_65") != null && jsonObjectBarrido.get("cpr_65") != "") {
				clsBarrido.setCpr_65(Integer.parseInt(jsonObjectBarrido.get("cpr_65").toString()));
			}
			else
				clsBarrido.setCpr_65(0);
			
			if (jsonObjectBarrido.get("descripcion_otros") != null && jsonObjectBarrido.get("descripcion_otros") != "") {
				clsBarrido.setDescripcion_otros(jsonObjectBarrido.get("descripcion_otros").toString());
			}
			else
				clsBarrido.setDescripcion_otros("");
			
			if (jsonObjectBarrido.get("total_recolectado") != null && jsonObjectBarrido.get("total_recolectado") != "") {
				clsBarrido.setTotal_recolectado(Double.valueOf(jsonObjectBarrido.get("total_recolectado").toString()));
			}
			else
				clsBarrido.setTotal_recolectado(0.0);
			
			if (jsonObjectBarrido.get("cod_usuario") != null && jsonObjectBarrido.get("cod_usuario") != "") {
				clsBarrido.setCodigo_usuario(Integer.parseInt(jsonObjectBarrido.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
