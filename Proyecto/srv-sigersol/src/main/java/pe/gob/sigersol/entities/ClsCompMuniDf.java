package pe.gob.sigersol.entities;

public class ClsCompMuniDf {

	private Integer comp_muni_df_id;
	private ClsDisposicionFinal disposicionFinal;
	private ClsResiduosMunicipales residuosMunicipales;
	private Integer porcentaje;
	private Integer codigo_usuario;
	
	public Integer getComp_muni_df_id() {
		return comp_muni_df_id;
	}
	public void setComp_muni_df_id(Integer comp_muni_df_id) {
		this.comp_muni_df_id = comp_muni_df_id;
	}
	public ClsDisposicionFinal getDisposicionFinal() {
		return disposicionFinal;
	}
	public void setDisposicionFinal(ClsDisposicionFinal disposicionFinal) {
		this.disposicionFinal = disposicionFinal;
	}
	public ClsResiduosMunicipales getResiduosMunicipales() {
		return residuosMunicipales;
	}
	public void setResiduosMunicipales(ClsResiduosMunicipales residuosMunicipales) {
		this.residuosMunicipales = residuosMunicipales;
	}
	public Integer getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Integer porcentaje) {
		this.porcentaje = porcentaje;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
