package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAccionPoi;
import pe.gob.sigersol.entities.ClsAccionTipo;
import pe.gob.sigersol.entities.ClsResultado;

public class AccionPoiDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(AccionPoiDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_accion_poi = "{call SIGERSOLBL.SGR_SP_INS_ACCION_POI(?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_accion_poi = "{call SIGERSOLBL.SGR_SP_UPD_ACCION_POI(?,?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_accion_poi = "{call SIGERSOLBL.SGR_SP_LIST_ACCION_POI(?,?)}";  
	
	public ClsResultado listarAccionPoi(ClsAccionPoi clsAccionPoi){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsAccionPoi> lista = new ArrayList<ClsAccionPoi>();
		con = null;
		String sql = this.sgr_sp_list_accion_poi;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccionPoi.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsAccionPoi item = new ClsAccionPoi();
				ClsAccionTipo accionTipo = new ClsAccionTipo();
				
				try {
					accionTipo.setAccion_tipo_id(rs.getInt("ACCION_TIPO_ID"));
					item.setAccion_tipo(accionTipo);
					if(clsAccionPoi.getEducacion_ambiental().getEducacion_ambiental_id() == -1){
						item.setTipo_accion(null);
						item.setCantidad_accion(null);
						item.setPresupuesto_asignado(null);
					} else{
						item.setTipo_accion(rs.getString("TIPO_ACCION"));
						item.setCantidad_accion(rs.getInt("CANTIDAD_ACCION"));
						item.setPresupuesto_asignado(rs.getInt("PRESUPUESTO_ASIGNADO"));
					}
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAccionPoi clsAccionPoi) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_accion_poi;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccionPoi.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.setInt(2, clsAccionPoi.getAccion_tipo().getAccion_tipo_id());
			cs.setString(3, clsAccionPoi.getTipo_accion());
			cs.setInt(4, clsAccionPoi.getCantidad_accion());
			cs.setInt(5, clsAccionPoi.getPresupuesto_asignado());
			cs.setInt(6, clsAccionPoi.getCodigo_usuario());
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(7);
			
			log.info("AccionPoiDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsAccionPoi clsAccionPoi) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_accion_poi;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsAccionPoi.getEducacion_ambiental().getEducacion_ambiental_id());
			cs.setInt(2, clsAccionPoi.getAccion_tipo().getAccion_tipo_id());
			cs.setString(3, clsAccionPoi.getTipo_accion());
			cs.setInt(4, clsAccionPoi.getCantidad_accion());
			cs.setInt(5, clsAccionPoi.getPresupuesto_asignado());
			cs.setInt(6, clsAccionPoi.getCodigo_usuario());
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(7);

			log.info("AccionPoiDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
