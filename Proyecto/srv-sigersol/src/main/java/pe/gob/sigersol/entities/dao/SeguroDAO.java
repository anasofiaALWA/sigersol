package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSeguro;

public class SeguroDAO extends GenericoDAO{
	private static Logger log = Logger.getLogger(SeguroDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String ep_al_sp_ins_seguro = "{call COPEREBL.EP_AL_SP_INS_SEGURO(?,?,?,?,?)}";
	private final String ep_al_sp_lista_seguro_persona = "{call COPEREBL.EP_AL_SP_LISTA_SEGURO_PERSONA(?,?)}";
	private final String ep_al_sp_upd_seguro = "{call COPEREBL.EP_AL_SP_UPD_SEGURO_PERSONA(?,?,?)}";
	
	public ClsResultado insertar(ClsSeguro clsSeguro) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql=ep_al_sp_ins_seguro;
		
		try {
			con = getConnection();
			cs = con.prepareCall(sql);
			cs.setInt(1, clsSeguro.getPersona_militar_id());
			cs.setInt(2, clsSeguro.getSeguro_tipo_id());
			cs.setString(3, clsSeguro.getSeguro_particular());
			cs.setString(4, clsSeguro.getUsuario_creacion());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.executeQuery();
			resultado = cs.getInt(5);
			
			log.info("SeguroDAO >> insertar() >> resultado :" + resultado);
			
			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");
			
		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		}catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		}finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	public ClsResultado obtener_seguro(String persona_militar_id) {
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsSeguro> lista = new ArrayList<ClsSeguro>();
		con = null;
		String sql = this.ep_al_sp_lista_seguro_persona;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, Integer.parseInt(persona_militar_id));
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				lista.add(getDatos(persona_militar_id));
			}
			
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_seguro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_seguro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_seguro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_seguro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		
		return clsResultado;
	}
	
	public ClsResultado actualiza_estado(Integer seguro_id, Integer codigo_estado) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql=ep_al_sp_upd_seguro;
		
		try {
			con = getConnection();
			cs = con.prepareCall(sql);
			cs.setInt(1, seguro_id);
			cs.setInt(2, codigo_estado);
			
			cs.registerOutParameter(3, OracleTypes.INTEGER);
			cs.executeQuery();
			resultado = cs.getInt(3);
			
			log.info("SeguroDAO >> actualiza_estado() >> resultado :" + resultado);
			
			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");
			
		} catch (SQLException ex) {
			log.error("ERROR! 1 actualiza_estado: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualiza_estado: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		}catch (Exception ex) {
			log.error("ERROR! 3 actualiza_estado: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		}finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualiza_estado: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}
	
	
	private ClsSeguro getDatos(String persona_militar_id) {
		ClsSeguro item = new ClsSeguro();
		try {
			item.setPersona_militar_id(Integer.parseInt(persona_militar_id));
			
		} catch (Exception e) {
		}
		try {
			item.setSeguro_id(rs.getInt("SEGURO_ID"));
			
		} catch (Exception e) {
		}
		try {
			item.setSeguro_tipo_id(rs.getInt("TIPO_SEGURO_ID"));
			
		} catch (Exception e) {
		}
		try {
			item.setSeguro(rs.getString("SEGURO"));
			
		} catch (Exception e) {
		}		
		try {
			item.setSeguro_particular(rs.getString("SEGURO_PARTICULAR"));
			
		} catch (Exception e) {
		}
		return item;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}
	
	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
