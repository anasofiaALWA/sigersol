package pe.gob.sigersol.entities;

public class ClsRecoleccionSelectivaDisposicionFinal {
	
	private Integer recoleccion_id;
	private ClsCicloGestionResiduos ciclo_grs_id;
	private String adm_serv_id;
	private Integer costo_anual;
	private Integer frecuencia_recoleccion;
	private Integer fr_cada_dias;
	private Integer cantidad_residuos;
	private Integer viviendas_atendidas;
	private String descrip_otro_vehiculo;
	private Integer antig_promedio_otro;
	private Double total_rec_almacenamiento;
	private Double total_rec_barrido;
	private Integer codigo_usuario;
	
	public Integer getRecoleccion_id() {
		return recoleccion_id;
	}
	public void setRecoleccion_id(Integer recoleccion_id) {
		this.recoleccion_id = recoleccion_id;
	}
	public ClsCicloGestionResiduos getCiclo_grs_id() {
		return ciclo_grs_id;
	}
	public void setCiclo_grs_id(ClsCicloGestionResiduos ciclo_grs_id) {
		this.ciclo_grs_id = ciclo_grs_id;
	}
	public String getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(String adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getFrecuencia_recoleccion() {
		return frecuencia_recoleccion;
	}
	public void setFrecuencia_recoleccion(Integer frecuencia_recoleccion) {
		this.frecuencia_recoleccion = frecuencia_recoleccion;
	}
	public Integer getFr_cada_dias() {
		return fr_cada_dias;
	}
	public void setFr_cada_dias(Integer fr_cada_dias) {
		this.fr_cada_dias = fr_cada_dias;
	}
	public Integer getCantidad_residuos() {
		return cantidad_residuos;
	}
	public void setCantidad_residuos(Integer cantidad_residuos) {
		this.cantidad_residuos = cantidad_residuos;
	}
	public Integer getViviendas_atendidas() {
		return viviendas_atendidas;
	}
	public void setViviendas_atendidas(Integer viviendas_atendidas) {
		this.viviendas_atendidas = viviendas_atendidas;
	}
	public String getDescrip_otro_vehiculo() {
		return descrip_otro_vehiculo;
	}
	public void setDescrip_otro_vehiculo(String descrip_otro_vehiculo) {
		this.descrip_otro_vehiculo = descrip_otro_vehiculo;
	}
	public Integer getAntig_promedio_otro() {
		return antig_promedio_otro;
	}
	public void setAntig_promedio_otro(Integer antig_promedio_otro) {
		this.antig_promedio_otro = antig_promedio_otro;
	}
	public Double getTotal_rec_almacenamiento() {
		return total_rec_almacenamiento;
	}
	public void setTotal_rec_almacenamiento(Double total_rec_almacenamiento) {
		this.total_rec_almacenamiento = total_rec_almacenamiento;
	}
	public Double getTotal_rec_barrido() {
		return total_rec_barrido;
	}
	public void setTotal_rec_barrido(Double total_rec_barrido) {
		this.total_rec_barrido = total_rec_barrido;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
	
}
