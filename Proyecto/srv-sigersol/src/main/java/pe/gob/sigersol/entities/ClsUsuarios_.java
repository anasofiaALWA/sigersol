package pe.gob.sigersol.entities;

public class ClsUsuarios_ {

	private Integer usuario_id;
	private String usuario_login;
	private String clave_login;
	private String llave_encrypt;
	private String email;
	private Integer codigo_estado;
	private Integer codigo_usuario;
	
	public Integer getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}
	public String getUsuario_login() {
		return usuario_login;
	}
	public void setUsuario_login(String usuario_login) {
		this.usuario_login = usuario_login;
	}
	public String getClave_login() {
		return clave_login;
	}
	public void setClave_login(String clave_login) {
		this.clave_login = clave_login;
	}
	public String getLlave_encrypt() {
		return llave_encrypt;
	}
	public void setLlave_encrypt(String llave_encrypt) {
		this.llave_encrypt = llave_encrypt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getCodigo_estado() {
		return codigo_estado;
	}
	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
