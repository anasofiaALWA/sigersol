package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CCGenCombustibleService;
import pe.gob.sigersol.generales.service.VariableService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/ccVariable")
@RequestScoped
public class VariableRestService {
	private static Logger log = Logger.getLogger(VariableRestService.class.getName());
	private VariableService varService;
	
	@POST
	@Path("/listar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listar(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response)
			throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		varService = new VariableService();
		Integer sitioDF_id = jsonEntradaList(paramJson);
		clsResultado = varService.listar(sitioDF_id);
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();

	}
	
	private Integer jsonEntradaList(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> VariableRestService :	jsonEntradaList" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		Integer sitioDF_id = -1;

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Filtro");

			if (jsonObject.get("sitioDFId") != null && jsonObject.get("sitioDFId") != "") {
				sitioDF_id = Integer.parseInt(jsonObject.get("sitioDFId").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return sitioDF_id;
	}

	
}
