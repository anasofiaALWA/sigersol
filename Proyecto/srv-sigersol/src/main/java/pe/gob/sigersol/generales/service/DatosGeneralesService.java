package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsDatosGenerales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.DatosGeneralesDAO;

public class DatosGeneralesService {

	private static Logger log = Logger.getLogger(DatosGeneralesService.class);
	DatosGeneralesDAO datosGeneralesDAO = new DatosGeneralesDAO();
	
	
	public ClsResultado obtener(ClsDatosGenerales clsDatosGenerales){
		ClsResultado clsResultado = null;
		try {
			clsResultado = datosGeneralesDAO.obtener(clsDatosGenerales);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsDatosGenerales clsDatosGenerales) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = datosGeneralesDAO.insertar(clsDatosGenerales);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsDatosGenerales clsDatosGenerales) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = datosGeneralesDAO.actualizar(clsDatosGenerales);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
