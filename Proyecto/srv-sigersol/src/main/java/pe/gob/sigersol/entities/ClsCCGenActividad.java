package pe.gob.sigersol.entities;

import java.util.List;

public class ClsCCGenActividad {
	private Integer cc_genActividadId;
	private Integer cc_GeneracionId;
	private Integer monitoreo;
	private Double metano_quemado;
	private Double horas_quemador;
	private Double eficiencia_quemador;
	private Double gas_total;
	private Double gas_quemado;
	private Double gas_generacion;
	private Double p_metanoEnGas;
	private Double densidad_metano;
	private Double eficienciaCaptura;
	private List<ClsCCGenCombustible> listCombustible;
	
	
	public Double getEficienciaCaptura() {
		return eficienciaCaptura;
	}
	public void setEficienciaCaptura(Double eficienciaCaptura) {
		this.eficienciaCaptura = eficienciaCaptura;
	}
	private Integer sesionId;
	
	public Integer getCc_genActividadId() {
		return cc_genActividadId;
	}
	public void setCc_genActividadId(Integer cc_genActividadId) {
		this.cc_genActividadId = cc_genActividadId;
	}
	public Integer getCc_GeneracionId() {
		return cc_GeneracionId;
	}
	public void setCc_GeneracionId(Integer cc_GeneracionId) {
		this.cc_GeneracionId = cc_GeneracionId;
	}

	public Integer getMonitoreo() {
		return monitoreo;
	}
	public void setMonitoreo(Integer monitoreo) {
		this.monitoreo = monitoreo;
	}
	public Double getMetano_quemado() {
		return metano_quemado;
	}
	public void setMetano_quemado(Double metano_quemado) {
		this.metano_quemado = metano_quemado;
	}
	public Double getHoras_quemador() {
		return horas_quemador;
	}
	public void setHoras_quemador(Double horas_quemador) {
		this.horas_quemador = horas_quemador;
	}
	public Double getEficiencia_quemador() {
		return eficiencia_quemador;
	}
	public void setEficiencia_quemador(Double eficiencia_quemador) {
		this.eficiencia_quemador = eficiencia_quemador;
	}
	public Double getGas_total() {
		return gas_total;
	}
	public void setGas_total(Double gas_total) {
		this.gas_total = gas_total;
	}
	
	public Double getGas_quemado() {
		return gas_quemado;
	}
	public void setGas_quemado(Double gas_quemado) {
		this.gas_quemado = gas_quemado;
	}
	public Double getGas_generacion() {
		return gas_generacion;
	}
	public void setGas_generacion(Double gas_generacion) {
		this.gas_generacion = gas_generacion;
	}
	public Double getP_metanoEnGas() {
		return p_metanoEnGas;
	}
	public void setP_metanoEnGas(Double p_metanoEnGas) {
		this.p_metanoEnGas = p_metanoEnGas;
	}
	public Double getDensidad_metano() {
		return densidad_metano;
	}
	public void setDensidad_metano(Double densidad_metano) {
		this.densidad_metano = densidad_metano;
	}
	
	public Integer getSesionId() {
		return sesionId;
	}
	public void setSesionId(Integer sesionId) {
		this.sesionId = sesionId;
	}
	public List<ClsCCGenCombustible> getListCombustible() {
		return listCombustible;
	}
	public void setListCombustible(List<ClsCCGenCombustible> listCombustible) {
		this.listCombustible = listCombustible;
	}
	
		
	

}
