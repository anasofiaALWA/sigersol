package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCCGenComposicion;
import pe.gob.sigersol.entities.ClsCCGeneracion;
import pe.gob.sigersol.entities.ClsResultado;

public class CCGenComposicionDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(CCGenComposicionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_cc_sel_datos_sdf = "{call SIGERSOLBL.SGR_SP_CC_SEL_DATOS_SDF(?,?,?,?,?)}";
	private final String sgr_sp_ins_gen_composicion = "{call SIGERSOLBL.SGR_SP_INS_CC_GEN_COMPOSICION(?,?,?,?,?)}";

	public ClsResultado getGenComposicion(Integer disposicion_final_id) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCCGenComposicion> listGenComposicion = new ArrayList<ClsCCGenComposicion>();
		ClsCCGeneracion ccGeneracion = new ClsCCGeneracion();
		con = null;
		String sql = this.sgr_sp_cc_sel_datos_sdf;

		try {

			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);

			cs.setInt(1, disposicion_final_id);
			cs.registerOutParameter(2, OracleTypes.INTEGER);
			cs.registerOutParameter(3, OracleTypes.INTEGER);
			cs.registerOutParameter(4, OracleTypes.INTEGER);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(5);

			try {
				Integer ccGeneracionId = cs.getInt(2);
				ccGeneracion.setCc_generacionId(ccGeneracionId);
			} catch (Exception e) {
				
			}

			try {
				Integer anioInicio_sdf = cs.getInt(3);
				ccGeneracion.setAnioInicio_sdf(anioInicio_sdf);

			} catch (Exception e) {
				
			}
			try {
				Double generacionTotal = cs.getDouble(4);
				ccGeneracion.setGeneracionTotal_sdf(generacionTotal);

			} catch (Exception e) {
				
			}

			while (rs.next()) {
				ClsCCGenComposicion ccGenComp = new ClsCCGenComposicion();
				try {
					ccGenComp.setCc_tipoResiduoId(rs.getInt("CC_TIPO_RESIDUO_ID"));
				} catch (Exception e) {
					
				}
				try {
					ccGenComp.setCc_tipoResiduoDesc(rs.getString("DESCRIPCION"));
				} catch (Exception e) {
					
				}
				try {
					ccGenComp.setPorcentaje(rs.getDouble("PORCENTAJE_COMPOSICION"));
				} catch (Exception e) {
					
				}

				listGenComposicion.add(ccGenComp);
			}

			ccGeneracion.setListComposicion(listGenComposicion);
			
			clsResultado.setExito(true);
			clsResultado.setObjeto(ccGeneracion);
			clsResultado.setMensaje("Obtención exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 listGenComposicion: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listGenComposicion: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listGenComposicion: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listGenComposicion: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	public ClsResultado insertar(Integer ccGeneracionId, ClsCCGenComposicion ccGenComposicion) {
		ClsResultado clsResultado = new ClsResultado();		
		Integer genComposicionId;
		con = null;
		String sql = this.sgr_sp_ins_gen_composicion;

		try {

			con = getConnection();
			con.setAutoCommit(true);
			cs = con.prepareCall(sql);

			cs.setInt(1, ccGeneracionId);
			cs.setInt(2, ccGenComposicion.getCc_tipoResiduoId());
			cs.setDouble(3, ccGenComposicion.getPorcentaje());
			cs.setInt(4, ccGenComposicion.getSesionId());
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			
			cs.executeUpdate();
			genComposicionId = cs.getInt(5);

			
			clsResultado.setExito(true);
			clsResultado.setObjeto(genComposicionId);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}


	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
