package pe.gob.sigersol.generales.service;

import java.util.List;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsGeneracionNoDomiciliar;
import pe.gob.sigersol.entities.ClsResSolidos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ResSolidosDAO;

public class ResSolidosService {

	private static Logger log = Logger.getLogger(ResSolidosService.class);
	ResSolidosDAO resSolidosDAO = new ResSolidosDAO();
	
	public ClsResultado obtener(ClsResSolidos clsResSolidos){
		ClsResultado clsResultado = null;
		ClsResultado clsResultado2 = null;
		ClsResultado clsResultado3 = null;
		ClsResultado clsResultado4 = null;
		ClsResultado clsResultadoFinal = null;
		
		List<ClsResSolidos> listaPadre = null;
		List<ClsResSolidos> listaHijo = null;
		List<ClsResSolidos> listaNieto = null;
		List<ClsResSolidos> listaBisnieto = null;
		
		ClsGeneracion generacion = null;
		
		try {
			
			clsResultadoFinal = new ClsResultado();
			Integer gid = clsResSolidos.getGeneracion().getGeneracion_id();
			
			clsResultado = resSolidosDAO.obtener(clsResSolidos);
			
			listaPadre = (List<ClsResSolidos>) clsResultado.getObjeto();
			
			for(ClsResSolidos item: listaPadre){
				if(item.getTiene_hijos() == 1){
					
					generacion = new ClsGeneracion();
					generacion.setGeneracion_id(gid);
					item.setGeneracion(generacion);
										
					clsResultado2 = resSolidosDAO.obtener(item);
					
					listaHijo = (List<ClsResSolidos>) clsResultado2.getObjeto();
					
					item.setHijos(listaHijo);
					
					for(ClsResSolidos item2 : listaHijo){
							
						if(item2.getTiene_hijos() == 1){
						
							generacion = new ClsGeneracion();
							generacion.setGeneracion_id(gid);
							item2.setGeneracion(generacion);
							
							clsResultado3 = resSolidosDAO.obtener(item2);
								
							listaNieto = (List<ClsResSolidos>) clsResultado3.getObjeto();
							
							item2.setHijos(listaNieto);
							
							for(ClsResSolidos item3 : listaNieto){
								
								if(item3.getTiene_hijos() == 1){
								
									generacion = new ClsGeneracion();
									generacion.setGeneracion_id(gid);
									item3.setGeneracion(generacion);
									
									clsResultado4 = resSolidosDAO.obtener(item3);
										
									listaBisnieto = (List<ClsResSolidos>) clsResultado4.getObjeto();
									
									item3.setHijos(listaBisnieto);
								}
							}
						}
					}
					
				}
			}
			
			clsResultadoFinal.setObjeto(listaPadre);
			clsResultadoFinal.setExito(true);
			clsResultadoFinal.setMensaje("Obtención exitosa");
			
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		
		return clsResultadoFinal;
	}
	
	public ClsResultado insertar(ClsResSolidos clsResSolidos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = resSolidosDAO.insertar(clsResSolidos);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsResSolidos clsResSolidos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = resSolidosDAO.actualizar(clsResSolidos);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
