package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoManejo;
import pe.gob.sigersol.entities.dao.TipoManejoDAO;

public class TipoManejoService {

	private static Logger log = Logger.getLogger(GeneracionNoDomiciliarService.class);
	TipoManejoDAO tipoManejoDAO = new TipoManejoDAO();
	
	public ClsResultado listarManejo(ClsTipoManejo clsTipoManejo){
		ClsResultado clsResultado = null;
		try {
			clsResultado = tipoManejoDAO.listarManejo(clsTipoManejo);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTipoManejo clsTipoManejo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = tipoManejoDAO.insertar(clsTipoManejo);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsTipoManejo clsTipoManejo) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = tipoManejoDAO.actualizar(clsTipoManejo);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
