package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsDatosGenerales;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.DatosGeneralesService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase DatosGeneralesRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de Datos Generales, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/datosGeneralesX")
@RequestScoped
public class DatosGeneralesRestService {
	
	private static Logger log = Logger.getLogger(DatosGeneralesRestService.class.getName());
	private ClsDatosGenerales clsDatosGenerales;
	private DatosGeneralesService datosGeneralesService;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	
	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			datosGeneralesService = new DatosGeneralesService();
			
			jsonEntradaDatosGenerales(paramJson);
			
			log.info("**obtener almacenamiento**");
			
			clsResultado = datosGeneralesService.obtener(clsDatosGenerales);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"datosGenerales\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		datosGeneralesService = new DatosGeneralesService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaDatosGenerales(paramJson);
			clsResultado = datosGeneralesService.insertar(clsDatosGenerales);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		datosGeneralesService = new DatosGeneralesService();
		
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaDatosGenerales(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = datosGeneralesService.actualizar(clsDatosGenerales);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de DatosGenerales  
	 * 
	 * */
	private void jsonEntradaDatosGenerales(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> DatosGeneralesRestService :jsonEntradaDatosGenerales" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectDatosGenerales;
		JSONParser jsonParser = new JSONParser();
		
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		clsDatosGenerales = new ClsDatosGenerales();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectDatosGenerales = (JSONObject) jsonObjectPrincipal.get("DatosGenerales");
			
			if (jsonObjectDatosGenerales.get("datos_generales_id") != null && jsonObjectDatosGenerales.get("datos_generales_id") != "") {
				clsDatosGenerales.setDatos_generales_id(Integer.parseInt(jsonObjectDatosGenerales.get("datos_generales_id").toString()));
			}
			else {
				clsDatosGenerales.setDatos_generales_id(-1);
			}
			
			if (jsonObjectDatosGenerales.get("ciclo_grs_id") != null && jsonObjectDatosGenerales.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectDatosGenerales.get("ciclo_grs_id").toString()));
			}
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsDatosGenerales.setCicloGestionResiduos(clsCicloGestionResiduos);
			
			if (jsonObjectDatosGenerales.get("nombre_alcalde") != null && jsonObjectDatosGenerales.get("nombre_alcalde") != "") {
				clsDatosGenerales.setNombre_alcalde(jsonObjectDatosGenerales.get("nombre_alcalde").toString());
			}
			
			if (jsonObjectDatosGenerales.get("direccion") != null && jsonObjectDatosGenerales.get("direccion") != "") {
				clsDatosGenerales.setDireccion(jsonObjectDatosGenerales.get("direccion").toString());
			}
			
			if (jsonObjectDatosGenerales.get("telefono") != null && jsonObjectDatosGenerales.get("telefono") != "") {
				clsDatosGenerales.setTelefono(jsonObjectDatosGenerales.get("telefono").toString());
			}
			
			if (jsonObjectDatosGenerales.get("fax") != null && jsonObjectDatosGenerales.get("fax") != "") {
				clsDatosGenerales.setFax(jsonObjectDatosGenerales.get("fax").toString());
			}
			
			if (jsonObjectDatosGenerales.get("email") != null && jsonObjectDatosGenerales.get("email") != "") {
				clsDatosGenerales.setEmail(jsonObjectDatosGenerales.get("email").toString());
			}
			
			if (jsonObjectDatosGenerales.get("clasif_municipalidad") != null && jsonObjectDatosGenerales.get("clasif_municipalidad") != "") {
				clsDatosGenerales.setClasif_municipalidad(jsonObjectDatosGenerales.get("clasif_municipalidad").toString());
			}
			
			if (jsonObjectDatosGenerales.get("tipo_municipalidad") != null && jsonObjectDatosGenerales.get("tipo_municipalidad") != "") {
				clsDatosGenerales.setTipo_municipalidad(jsonObjectDatosGenerales.get("tipo_municipalidad").toString());
			}
			
			if (jsonObjectDatosGenerales.get("nombre_slp") != null && jsonObjectDatosGenerales.get("nombre_slp") != "") {
				clsDatosGenerales.setNombre_slp(jsonObjectDatosGenerales.get("nombre_slp").toString());
			}
			
			if (jsonObjectDatosGenerales.get("responsable_slp") != null && jsonObjectDatosGenerales.get("responsable_slp") != "") {
				clsDatosGenerales.setResponsable_slp(jsonObjectDatosGenerales.get("responsable_slp").toString());
			}
			
			if (jsonObjectDatosGenerales.get("telefono_slp") != null && jsonObjectDatosGenerales.get("telefono_slp") != "") {
				clsDatosGenerales.setTelefono_slp(jsonObjectDatosGenerales.get("telefono_slp").toString());
			}
			
			if (jsonObjectDatosGenerales.get("celular_slp") != null && jsonObjectDatosGenerales.get("celular_slp") != "") {
				clsDatosGenerales.setCelular_slp(jsonObjectDatosGenerales.get("celular_slp").toString());
			}
			
			if (jsonObjectDatosGenerales.get("email_slp") != null && jsonObjectDatosGenerales.get("email_slp") != "") {
				clsDatosGenerales.setEmail_slp(jsonObjectDatosGenerales.get("email_slp").toString());
			}
			
			if (jsonObjectDatosGenerales.get("nombre_sigersol") != null && jsonObjectDatosGenerales.get("nombre_sigersol") != "") {
				clsDatosGenerales.setNombre_sigersol(jsonObjectDatosGenerales.get("nombre_sigersol").toString());
			}
			
			if (jsonObjectDatosGenerales.get("telefono_sigersol") != null && jsonObjectDatosGenerales.get("telefono_sigersol") != "") {
				clsDatosGenerales.setTelefono_sigersol(jsonObjectDatosGenerales.get("telefono_sigersol").toString());
			}
			
			if (jsonObjectDatosGenerales.get("celular_sigersol") != null && jsonObjectDatosGenerales.get("celular_sigersol") != "") {
				clsDatosGenerales.setCelular_sigersol(jsonObjectDatosGenerales.get("celular_sigersol").toString());
			}
			
			if (jsonObjectDatosGenerales.get("email_sigersol") != null && jsonObjectDatosGenerales.get("email_sigersol") != "") {
				clsDatosGenerales.setEmail_sigersol(jsonObjectDatosGenerales.get("email_sigersol").toString());
			}
			
			if (jsonObjectDatosGenerales.get("direccion_sigersol") != null && jsonObjectDatosGenerales.get("direccion_sigersol") != "") {
				clsDatosGenerales.setDireccion_sigersol(jsonObjectDatosGenerales.get("direccion_sigersol").toString());
			}
			
			if (jsonObjectDatosGenerales.get("poblacion_urbana") != null && jsonObjectDatosGenerales.get("poblacion_urbana") != "") {
				clsDatosGenerales.setPoblacion_urbana(Integer.parseInt(jsonObjectDatosGenerales.get("poblacion_urbana").toString()));
			}
			
			if (jsonObjectDatosGenerales.get("poblacion_rural") != null && jsonObjectDatosGenerales.get("poblacion_rural") != "") {
				clsDatosGenerales.setPoblacion_rural(Integer.parseInt(jsonObjectDatosGenerales.get("poblacion_rural").toString()));
			}
			
			if (jsonObjectDatosGenerales.get("total_viviendas") != null && jsonObjectDatosGenerales.get("total_viviendas") != "") {
				clsDatosGenerales.setTotal_viviendas(Integer.parseInt(jsonObjectDatosGenerales.get("total_viviendas").toString()));
			}
			
			if (jsonObjectDatosGenerales.get("cod_usuario") != null && jsonObjectDatosGenerales.get("cod_usuario") != "") {
				clsDatosGenerales.setCodigo_usuario(Integer.parseInt(jsonObjectDatosGenerales.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
