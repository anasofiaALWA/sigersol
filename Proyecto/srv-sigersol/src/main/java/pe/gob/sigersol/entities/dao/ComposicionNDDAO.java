package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsComposicionND;
import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResultado;

public class ComposicionNDDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(ComposicionNDDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_rs_no_domiciliar = "{call SIGERSOLBL.SGR_SP_LIST_RS_NO_DOMICILIAR(?,?,?)}";
	private final String sgr_sp_ins_composicion_nd = "{call SIGERSOLBL.SGR_SP_INS_COMPOSICION_ND(?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_composicion_nd = "{call SIGERSOLBL.SGR_SP_UPD_COMPOSICION_ND(?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsComposicionND clsComposicionND) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsComposicionND> lista = new ArrayList<ClsComposicionND>();
		
		con = null;
		String sql = this.sgr_sp_list_rs_no_domiciliar;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsComposicionND.getTipors_id());
			cs.setInt(2, clsComposicionND.getGeneracion().getGeneracion_id());
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);

			while (rs.next()) {
				ClsComposicionND item = new ClsComposicionND();
				ClsGeneracion generacion = new ClsGeneracion();
				try {
					//item.setResiduos_solidos_id(rs.getInt("RESIDUOS_SOLIDOS_ID"));
					item.setTipors_id(rs.getInt("TIPORS_ID"));
					item.setNombre(rs.getString("TIPORS_NOMBRE"));
					item.setInformacion(rs.getString("INFORMACION"));
					item.setPadre_id(rs.getInt("PADRE_ID"));
					item.setNivel(rs.getInt("NIVEL"));
					item.setTiene_hijos(rs.getInt("TIENE_HIJOS"));
					if(clsComposicionND.getGeneracion().getGeneracion_id() != -1){
						item.setPorcentaje1(rs.getDouble("PORCENTAJE1"));
						item.setPorcentaje2(rs.getDouble("PORCENTAJE2"));
						item.setPorcentaje3(rs.getDouble("PORCENTAJE3"));
						item.setPorcentaje4(rs.getDouble("PORCENTAJE4"));
						item.setPorcentaje5(rs.getDouble("PORCENTAJE5"));
						item.setPorcentaje6(rs.getDouble("PORCENTAJE6"));
						item.setPorcentaje7(rs.getDouble("PORCENTAJE7"));
						generacion.setGeneracion_id(rs.getInt("GENERACION_ID"));
						item.setGeneracion(generacion);
					}
					else{
						item.setPorcentaje1(null);
						item.setPorcentaje2(null);
						item.setPorcentaje3(null);
						item.setPorcentaje4(null);
						item.setPorcentaje5(null);
						item.setPorcentaje6(null);
						item.setPorcentaje7(null);
						generacion.setGeneracion_id(-1);
						item.setGeneracion(generacion);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
				
			}
			
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsComposicionND clsComposicionND) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_composicion_nd;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsComposicionND.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsComposicionND.getTipors_id());
			cs.setDouble(3, clsComposicionND.getPorcentaje1());
			cs.setDouble(4, clsComposicionND.getPorcentaje2());
			cs.setDouble(5, clsComposicionND.getPorcentaje3());
			cs.setDouble(6, clsComposicionND.getPorcentaje4());
			cs.setDouble(7, clsComposicionND.getPorcentaje5());
			cs.setDouble(8, clsComposicionND.getPorcentaje6());
			cs.setDouble(9, clsComposicionND.getPorcentaje7());
			cs.setInt(10, clsComposicionND.getCod_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);
			
			log.info("ComposicionNDDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsComposicionND clsComposicionND) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_composicion_nd;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsComposicionND.getGeneracion().getGeneracion_id());
			cs.setInt(2, clsComposicionND.getTipors_id());
			cs.setDouble(3, clsComposicionND.getPorcentaje1());
			cs.setDouble(4, clsComposicionND.getPorcentaje2());
			cs.setDouble(5, clsComposicionND.getPorcentaje3());
			cs.setDouble(6, clsComposicionND.getPorcentaje4());
			cs.setDouble(7, clsComposicionND.getPorcentaje5());
			cs.setDouble(8, clsComposicionND.getPorcentaje6());
			cs.setDouble(9, clsComposicionND.getPorcentaje7());
			cs.setInt(10, clsComposicionND.getCod_usuario());
			cs.registerOutParameter(11, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(11);

			log.info("ComposicionNDDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
