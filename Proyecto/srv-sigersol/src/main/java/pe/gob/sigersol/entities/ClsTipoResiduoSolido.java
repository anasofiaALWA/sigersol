package pe.gob.sigersol.entities;

public class ClsTipoResiduoSolido {
	
	private Integer tipo_residuo_id;
	private String nombre;
	private String informacion;
	
	public Integer getTipo_residuo_id() {
		return tipo_residuo_id;
	}
	public void setTipo_residuo_id(Integer tipo_residuo_id) {
		this.tipo_residuo_id = tipo_residuo_id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getInformacion() {
		return informacion;
	}
	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}
		
}
