package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCombustibleVehiculo;
import pe.gob.sigersol.entities.ClsRecDisposicionFinal;
import pe.gob.sigersol.entities.ClsRecRcdOm;
import pe.gob.sigersol.entities.ClsRecValorizacion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoCombustible;
import pe.gob.sigersol.generales.service.CombustibleVehiculo2Service;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase CombustibleVehiculo2RestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de recoleccion permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/combustible2X")
@RequestScoped
public class CombustibleVehiculo2RestService {

	private static Logger log = Logger.getLogger(CombustibleVehiculo2RestService.class.getName());
	private ClsCombustibleVehiculo clsCombustibleVehiculo;
	private ClsRecValorizacion clsRecValorizacion;
	private ClsTipoCombustible clsTipoCombustible;
	private CombustibleVehiculo2Service combustibleVehiculo2Service;
	private List<ClsCombustibleVehiculo> listaCombustibleVehiculo;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarCombustible")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarCombustibleVehiculo(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			combustibleVehiculo2Service = new CombustibleVehiculo2Service();
			
			jsonEntradaListaCombustible(paramJson);
			
			log.info("**Obtener Recoleccion Vehiculos");
			
			clsResultado = combustibleVehiculo2Service.listarVehiculoRecoleccion(clsCombustibleVehiculo);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstCombustible2\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		combustibleVehiculo2Service = new CombustibleVehiculo2Service();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			jsonEntradaCombustible(paramJson);

			for (ClsCombustibleVehiculo clsCombustibleVehiculo : listaCombustibleVehiculo) {
				clsResultado = combustibleVehiculo2Service.insertar(clsCombustibleVehiculo);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		combustibleVehiculo2Service = new CombustibleVehiculo2Service();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaCombustible(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsCombustibleVehiculo clsCombustibleVehiculo : listaCombustibleVehiculo) {
			clsResultado = combustibleVehiculo2Service.actualizar(clsCombustibleVehiculo);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de CombustibleVehiculos
	 * 
	 * */
	private void jsonEntradaCombustible(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService :	jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectTipoCombustible;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaCombustibleVehiculo = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsCombustibleVehiculo = new ClsCombustibleVehiculo();
			clsTipoCombustible = new ClsTipoCombustible();
			clsRecValorizacion = new ClsRecValorizacion();
			
			JSONObject innerObject = (JSONObject) i.next();

			if (innerObject.get("rec_valorizacion_id") != null && innerObject.get("rec_valorizacion_id").toString() != "") {
				clsRecValorizacion.setRec_valorizacion_id(Integer.parseInt(innerObject.get("rec_valorizacion_id").toString()));
			}
			else {
				clsRecValorizacion.setRec_valorizacion_id(0);
			}
			
			clsCombustibleVehiculo.setRecValorizacion(clsRecValorizacion);
			
			
			if (innerObject.get("tipo_combustible") != null && innerObject.get("tipo_combustible").toString() != "") {
				jsonObjectTipoCombustible  = (JSONObject) innerObject.get("tipo_combustible");
				clsTipoCombustible.setTipo_combustible_id(Integer.parseInt(jsonObjectTipoCombustible.get("tipo_combustible_id").toString()));
			}
			clsCombustibleVehiculo.setTipo_combustible(clsTipoCombustible);
			
			if (innerObject.get("consumo") != null && innerObject.get("consumo").toString() != "") {
				clsCombustibleVehiculo.setConsumo(Integer.parseInt(innerObject.get("consumo").toString()));
			} else{
				clsCombustibleVehiculo.setConsumo(0);
			}
			
			if (innerObject.get("costo") != null && innerObject.get("costo").toString() != "") {
				clsCombustibleVehiculo.setCosto(Integer.parseInt(innerObject.get("costo").toString()));
			} else{
				clsCombustibleVehiculo.setCosto(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsCombustibleVehiculo.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaCombustibleVehiculo.add(clsCombustibleVehiculo);	
		}
	
		log.info("----------------------------------------------------------------------------------");
		
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaCombustible(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectCombustible;
		JSONParser jsonParser = new JSONParser();
		
		clsCombustibleVehiculo = new ClsCombustibleVehiculo();
		clsTipoCombustible = new ClsTipoCombustible();
		clsRecValorizacion = new ClsRecValorizacion();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectCombustible = (JSONObject) jsonObjectPrincipal.get("Combustible2");

			if (jsonObjectCombustible.get("rec_valorizacion_id") != null && jsonObjectCombustible.get("rec_valorizacion_id") != "" &&
					jsonObjectCombustible.get("rec_valorizacion_id") != " ") {
				clsRecValorizacion.setRec_valorizacion_id(Integer.parseInt(jsonObjectCombustible.get("rec_valorizacion_id").toString()));
			}
			else
				clsRecValorizacion.setRec_valorizacion_id(-1);
			
			clsCombustibleVehiculo.setRecValorizacion(clsRecValorizacion);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
