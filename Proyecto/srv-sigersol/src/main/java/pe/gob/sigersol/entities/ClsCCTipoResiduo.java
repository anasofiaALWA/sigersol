package pe.gob.sigersol.entities;

public class ClsCCTipoResiduo {
	
	private Integer tipoResiduoId;
	private String descripcion;
	private Double valorCondHumeda;
	private Double valorCondSeca;
	private String tipoClima;
	private Double tasaDescomposicion;
	
	public Integer getTipoResiduoId() {
		return tipoResiduoId;
	}
	public void setTipoResiduoId(Integer tipoResiduoId) {
		this.tipoResiduoId = tipoResiduoId;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Double getValorCondHumeda() {
		return valorCondHumeda;
	}
	public void setValorCondHumeda(Double valorCondHumeda) {
		this.valorCondHumeda = valorCondHumeda;
	}
	public Double getValorCondSeca() {
		return valorCondSeca;
	}
	public void setValorCondSeca(Double valorCondSeca) {
		this.valorCondSeca = valorCondSeca;
	}
	public String getTipoClima() {
		return tipoClima;
	}
	public void setTipoClima(String tipoClima) {
		this.tipoClima = tipoClima;
	}
	public Double getTasaDescomposicion() {
		return tasaDescomposicion;
	}
	public void setTasaDescomposicion(Double tasaDescomposicion) {
		this.tasaDescomposicion = tasaDescomposicion;
	}	
	

}
