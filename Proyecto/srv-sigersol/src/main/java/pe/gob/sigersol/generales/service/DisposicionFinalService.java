package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.DisposicionFinalDAO;

public class DisposicionFinalService {

	private static Logger log = Logger.getLogger(DisposicionFinalService.class);
	DisposicionFinalDAO disposicionFinalDAO = new DisposicionFinalDAO();
	
	public ClsResultado obtener(ClsDisposicionFinal clsDisposicionFinal){
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinalDAO.obtener(clsDisposicionFinal);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsDisposicionFinal clsDisposicionFinal) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinalDAO.insertar(clsDisposicionFinal);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsDisposicionFinal clsDisposicionFinal) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinalDAO.actualizar(clsDisposicionFinal);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado listarSDF(ClsCicloGestionResiduos clsCicloGRS){
		ClsResultado clsResultado = null;
		try {
			clsResultado = disposicionFinalDAO.listSDF(clsCicloGRS);
		} catch (Exception e) {
			log.info("message listarSDF >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
