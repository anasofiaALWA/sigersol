package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTrabajadores;
import pe.gob.sigersol.entities.dao.TrabajadoresDisposicionFinalDAO;

public class TrabajadoresDisposicionFinalService {

	private static Logger log = Logger.getLogger(TrabajadoresDisposicionFinalService.class);
	TrabajadoresDisposicionFinalDAO trabajadoresDisposicionFinalDAO = new TrabajadoresDisposicionFinalDAO();

	public ClsResultado listarTrabajadores(ClsTrabajadores clsTrabajadores){
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresDisposicionFinalDAO.listarTrabajadores(clsTrabajadores);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTrabajadores clsTrabajadores) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresDisposicionFinalDAO.insertar(clsTrabajadores);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
