package pe.gob.sigersol.core.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.sql.DataSource;

import pe.gob.sigersol.util.ApplicationProperties;

public class GenericoDAO {
	boolean EXITO = true;
	boolean FRACASO = false;
	protected static String DATASOURCENEGOCIO = ApplicationProperties
			.getProperty(ApplicationProperties.ApplicationPropertiesEnum.DATASOURCE_NEGOCIO);
	

	public Connection getConnection() throws NamingException, SQLException {
		ServiceLocator locator = ServiceLocator.getInstance();
		DataSource ds = locator.getDataSource(DATASOURCENEGOCIO);
		return ds.getConnection();
	}
	
	

}