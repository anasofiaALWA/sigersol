package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsComposicionND;
import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResSolidos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.ComposicionNDService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/composicionND")
@RequestScoped
public class ComposicionNDRestService {

	private static Logger log = Logger.getLogger(RecValorizacionRestService.class.getName());
	private Integer accion = null;
	
	private ClsComposicionND clsComposicionND;
	private ClsGeneracion clsGeneracion;
	private ComposicionNDService composicionNDService;
	private List<ClsComposicionND> listaComposicionND;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			composicionNDService = new ComposicionNDService();
			
			clsComposicionND = new ClsComposicionND();
			
			jsonEntradaListaComposicionND(paramJson);
			
			log.info("**Obtener Composicion No Domiciliar ");
			
			clsResultado = composicionNDService.obtener(clsComposicionND);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstComposicionND\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
		
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		composicionNDService = new ComposicionNDService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaComposicionND(paramJson);

			for (ClsComposicionND clsComposicionND : listaComposicionND) {
				clsResultado = composicionNDService.insertar(clsComposicionND);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		composicionNDService = new ComposicionNDService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaComposicionND(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsComposicionND clsComposicionND : listaComposicionND) {
			clsResultado = composicionNDService.actualizar(clsComposicionND);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de ResSolidos
	 * 
	 * */
	private void jsonEntradaComposicionND(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ComposicionNDRestService : jsonEntradaComposicionND" + jsonEntrada);

		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaComposicionND = new ArrayList();
		
		// parsear el objeto Json en una lista
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			JSONObject innerObject = (JSONObject) i.next();
			
			String hijos = innerObject.get("hijos").toString();
			
			JSONParser jsonParser2 = new JSONParser();
			JSONArray jsonObject2 = (JSONArray) jsonParser2.parse(hijos);
			
			Iterator it = jsonObject2.iterator();
			
			
			while(it.hasNext()){
				
				clsComposicionND = new ClsComposicionND();
				clsGeneracion = new ClsGeneracion();
				
				JSONObject innerObject2 = (JSONObject) it.next();
				
				if(innerObject2.get("hijos") == null){
					
					if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
						clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
					}
					
					clsComposicionND.setGeneracion(clsGeneracion);
					
					if (innerObject2.get("tipors_id") != null && innerObject2.get("tipors_id").toString() != "") {
						clsComposicionND.setTipors_id(Integer.parseInt(innerObject2.get("tipors_id").toString()));
					} else {
						clsComposicionND.setTipors_id(0);
					}
					
					if (innerObject2.get("porcentaje1") != null && innerObject2.get("porcentaje1").toString() != "") {
						clsComposicionND.setPorcentaje1(Double.valueOf(innerObject2.get("porcentaje1").toString()));
					} else {
						clsComposicionND.setPorcentaje1(0.0);
					}
					
					if (innerObject2.get("porcentaje2") != null && innerObject2.get("porcentaje2").toString() != "") {
						clsComposicionND.setPorcentaje2(Double.valueOf(innerObject2.get("porcentaje2").toString()));
					} else {
						clsComposicionND.setPorcentaje2(0.0);
					}
					
					if (innerObject2.get("porcentaje3") != null && innerObject2.get("porcentaje3").toString() != "") {
						clsComposicionND.setPorcentaje3(Double.valueOf(innerObject2.get("porcentaje3").toString()));
					} else {
						clsComposicionND.setPorcentaje3(0.0);
					}
					
					if (innerObject2.get("porcentaje4") != null && innerObject2.get("porcentaje4").toString() != "") {
						clsComposicionND.setPorcentaje4(Double.valueOf(innerObject2.get("porcentaje4").toString()));
					} else {
						clsComposicionND.setPorcentaje4(0.0);
					}
					
					if (innerObject2.get("porcentaje5") != null && innerObject2.get("porcentaje5").toString() != "") {
						clsComposicionND.setPorcentaje5(Double.valueOf(innerObject2.get("porcentaje5").toString()));
					} else {
						clsComposicionND.setPorcentaje5(0.0);
					}
					
					if (innerObject2.get("porcentaje6") != null && innerObject2.get("porcentaje6").toString() != "") {
						clsComposicionND.setPorcentaje6(Double.valueOf(innerObject2.get("porcentaje6").toString()));
					} else {
						clsComposicionND.setPorcentaje6(0.0);
					}
					
					if (innerObject2.get("porcentaje7") != null && innerObject2.get("porcentaje7").toString() != "") {
						clsComposicionND.setPorcentaje7(Double.valueOf(innerObject2.get("porcentaje7").toString()));
					} else {
						clsComposicionND.setPorcentaje7(0.0);
					}
					
					if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
						clsComposicionND.setCod_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
					}
					
					listaComposicionND.add(clsComposicionND);	
					
				} else {
					
					String nietos = innerObject2.get("hijos").toString();
					
					JSONParser jsonParser3 = new JSONParser();
					JSONArray jsonObject3 = (JSONArray) jsonParser3.parse(nietos);
					
					Iterator itr = jsonObject3.iterator();
					
					while(itr.hasNext()){
						
						clsComposicionND = new ClsComposicionND();
						clsGeneracion = new ClsGeneracion();
						
						JSONObject innerObject3 = (JSONObject) itr.next();
						
						if(innerObject3.get("hijos") == null){
						
							if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
								clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
							}
							
							clsComposicionND.setGeneracion(clsGeneracion);
							
							if (innerObject3.get("tipors_id") != null && innerObject3.get("tipors_id").toString() != "") {
								clsComposicionND.setTipors_id(Integer.parseInt(innerObject3.get("tipors_id").toString()));
							} else {
								clsComposicionND.setTipors_id(0);
							}
							
							if (innerObject3.get("porcentaje1") != null && innerObject3.get("porcentaje1").toString() != "") {
								clsComposicionND.setPorcentaje1(Double.valueOf(innerObject3.get("porcentaje1").toString()));
							} else {
								clsComposicionND.setPorcentaje1(0.0);
							}
							
							if (innerObject3.get("porcentaje2") != null && innerObject3.get("porcentaje2").toString() != "") {
								clsComposicionND.setPorcentaje2(Double.valueOf(innerObject3.get("porcentaje2").toString()));
							} else {
								clsComposicionND.setPorcentaje2(0.0);
							}
							
							if (innerObject3.get("porcentaje3") != null && innerObject3.get("porcentaje3").toString() != "") {
								clsComposicionND.setPorcentaje3(Double.valueOf(innerObject3.get("porcentaje3").toString()));
							} else {
								clsComposicionND.setPorcentaje3(0.0);
							}
							
							if (innerObject3.get("porcentaje4") != null && innerObject3.get("porcentaje4").toString() != "") {
								clsComposicionND.setPorcentaje4(Double.valueOf(innerObject3.get("porcentaje4").toString()));
							} else {
								clsComposicionND.setPorcentaje4(0.0);
							}
							
							if (innerObject3.get("porcentaje5") != null && innerObject3.get("porcentaje5").toString() != "") {
								clsComposicionND.setPorcentaje5(Double.valueOf(innerObject3.get("porcentaje5").toString()));
							} else {
								clsComposicionND.setPorcentaje5(0.0);
							}
							
							if (innerObject3.get("porcentaje6") != null && innerObject3.get("porcentaje6").toString() != "") {
								clsComposicionND.setPorcentaje6(Double.valueOf(innerObject3.get("porcentaje6").toString()));
							} else {
								clsComposicionND.setPorcentaje6(0.0);
							}
							
							if (innerObject3.get("porcentaje7") != null && innerObject3.get("porcentaje7").toString() != "") {
								clsComposicionND.setPorcentaje7(Double.valueOf(innerObject3.get("porcentaje7").toString()));
							} else {
								clsComposicionND.setPorcentaje7(0.0);
							}
							
							if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
								clsComposicionND.setCod_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
							}
							
							listaComposicionND.add(clsComposicionND);	
						} else {
							
							String bisnietos = innerObject3.get("hijos").toString();
							
							JSONParser jsonParser4 = new JSONParser();
							JSONArray jsonObject4 = (JSONArray) jsonParser4.parse(bisnietos);
							
							Iterator iter = jsonObject4.iterator();
							
							while(iter.hasNext()){
								
								clsComposicionND = new ClsComposicionND();
								clsGeneracion = new ClsGeneracion();
								
								JSONObject innerObject4 = (JSONObject) iter.next();
								
								if (innerObject.get("generacion_id") != null && innerObject.get("generacion_id").toString() != "") {
									clsGeneracion.setGeneracion_id(Integer.parseInt(innerObject.get("generacion_id").toString()));
								}
								
								clsComposicionND.setGeneracion(clsGeneracion);
								
								if (innerObject4.get("tipors_id") != null && innerObject4.get("tipors_id").toString() != "") {
									clsComposicionND.setTipors_id(Integer.parseInt(innerObject4.get("tipors_id").toString()));
								} else {
									clsComposicionND.setTipors_id(0);
								}
								
								if (innerObject4.get("porcentaje1") != null && innerObject4.get("porcentaje1").toString() != "") {
									clsComposicionND.setPorcentaje1(Double.valueOf(innerObject4.get("porcentaje1").toString()));
								} else {
									clsComposicionND.setPorcentaje1(0.0);
								}
								
								if (innerObject4.get("porcentaje2") != null && innerObject4.get("porcentaje2").toString() != "") {
									clsComposicionND.setPorcentaje2(Double.valueOf(innerObject4.get("porcentaje2").toString()));
								} else {
									clsComposicionND.setPorcentaje2(0.0);
								}
								
								if (innerObject4.get("porcentaje3") != null && innerObject4.get("porcentaje3").toString() != "") {
									clsComposicionND.setPorcentaje3(Double.valueOf(innerObject4.get("porcentaje3").toString()));
								} else {
									clsComposicionND.setPorcentaje3(0.0);
								}
								
								if (innerObject4.get("porcentaje4") != null && innerObject4.get("porcentaje4").toString() != "") {
									clsComposicionND.setPorcentaje4(Double.valueOf(innerObject4.get("porcentaje4").toString()));
								} else {
									clsComposicionND.setPorcentaje4(0.0);
								}
								
								if (innerObject4.get("porcentaje5") != null && innerObject4.get("porcentaje5").toString() != "") {
									clsComposicionND.setPorcentaje5(Double.valueOf(innerObject4.get("porcentaje5").toString()));
								} else {
									clsComposicionND.setPorcentaje5(0.0);
								}
								
								if (innerObject4.get("porcentaje6") != null && innerObject4.get("porcentaje6").toString() != "") {
									clsComposicionND.setPorcentaje6(Double.valueOf(innerObject4.get("porcentaje6").toString()));
								} else {
									clsComposicionND.setPorcentaje6(0.0);
								}
								
								if (innerObject4.get("porcentaje7") != null && innerObject4.get("porcentaje7").toString() != "") {
									clsComposicionND.setPorcentaje7(Double.valueOf(innerObject4.get("porcentaje7").toString()));
								} else {
									clsComposicionND.setPorcentaje7(0.0);
								}
								
								if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
									clsComposicionND.setCod_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
								}
								
								listaComposicionND.add(clsComposicionND);
							}
							
						}
					}
				}
					
			}
			
			log.info("----------------------------------------------------------------------------------");
		}
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaComposicionND(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> ComposicionNDRestService : jsonEntradaListaComposicionND" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectResSolidos;
		JSONParser jsonParser = new JSONParser();
		
		clsComposicionND = new ClsComposicionND();
		clsGeneracion = new ClsGeneracion();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectResSolidos = (JSONObject) jsonObjectPrincipal.get("ComposicionND");
			
			if (jsonObjectResSolidos.get("generacion_id") != null && jsonObjectResSolidos.get("generacion_id") != "" &&
					jsonObjectResSolidos.get("generacion_id") != " ") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(jsonObjectResSolidos.get("generacion_id").toString()));
			}
			else
				clsGeneracion.setGeneracion_id(-1);
			
			clsComposicionND.setGeneracion(clsGeneracion);
			
			clsComposicionND.setTipors_id(0);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
