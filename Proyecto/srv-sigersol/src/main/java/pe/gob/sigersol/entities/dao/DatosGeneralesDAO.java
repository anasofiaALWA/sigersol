package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsDatosGenerales;
import pe.gob.sigersol.entities.ClsResultado;

public class DatosGeneralesDAO extends GenericoDAO{

	private static Logger log = Logger.getLogger(DatosGeneralesDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_datos_generales = "{call SIGERSOLBL.SGR_SP_LIST_DATOS_GENERALES(?,?)}";
	private final String sgr_sp_ins_datos_generales = "{call SIGERSOLBL.SGR_SP_INS_DATOS_GENERALES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_datos_generales = "{call SIGERSOLBL.SGR_SP_UPD_DATOS_GENERALES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	
	public ClsResultado obtener(ClsDatosGenerales clsDatosGenerales) {
		ClsResultado clsResultado = new ClsResultado();
		ClsDatosGenerales item = new ClsDatosGenerales();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_datos_generales;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDatosGenerales.getCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				try {
					item.setDatos_generales_id(rs.getInt("DATOS_GENERALES_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(cicloGestionResiduos);
					item.setNombre_alcalde(rs.getString("NOMBRE_ALCALDE"));
					item.setDireccion(rs.getString("DIRECCION"));
					item.setTelefono(rs.getString("TELEFONO"));
					item.setFax(rs.getString("FAX"));
					item.setEmail(rs.getString("EMAIL"));
					item.setClasif_municipalidad(rs.getString("CLASIF_MUNICIPALIDAD"));
					item.setTipo_municipalidad(rs.getString("TIPO_MUNICIPALIDAD"));
					item.setNombre_slp(rs.getString("NOMBRE_SLP"));
					item.setResponsable_slp(rs.getString("RESPONSABLE_SLP"));
					item.setTelefono_slp(rs.getString("TELEFONO_SLP"));
					item.setCelular_slp(rs.getString("CELULAR_SLP"));
					item.setEmail_slp(rs.getString("EMAIL_SLP"));
					item.setNombre_sigersol(rs.getString("NOMBRE_SIGERSOL"));
					item.setTelefono_sigersol(rs.getString("TELEFONO_SIGERSOL"));
					item.setCelular_sigersol(rs.getString("CELULAR_SIGERSOL"));
					item.setEmail_sigersol(rs.getString("EMAIL_SIGERSOL"));
					item.setDireccion_sigersol(rs.getString("DIRECCION_SIGERSOL"));
					item.setPoblacion_urbana(rs.getInt("POBLACION_URBANA"));
				    item.setPoblacion_rural(rs.getInt("POBLACION_RURAL"));
				    item.setTotal_viviendas(rs.getInt("TOTAL_VIVIENDAS"));

				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsDatosGenerales clsDatosGenerales) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_datos_generales;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDatosGenerales.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setString(2, clsDatosGenerales.getNombre_alcalde());
			cs.setString(3, clsDatosGenerales.getDireccion());
			cs.setString(4, clsDatosGenerales.getTelefono());
			cs.setString(5, clsDatosGenerales.getFax());
			cs.setString(6, clsDatosGenerales.getEmail());
			cs.setString(7, clsDatosGenerales.getClasif_municipalidad());
			cs.setString(8, clsDatosGenerales.getTipo_municipalidad());
			cs.setString(9, clsDatosGenerales.getNombre_slp());
			cs.setString(10, clsDatosGenerales.getResponsable_slp());
			cs.setString(11, clsDatosGenerales.getTelefono_slp());
			cs.setString(12, clsDatosGenerales.getCelular_slp());
			cs.setString(13, clsDatosGenerales.getEmail_slp());
			cs.setString(14, clsDatosGenerales.getNombre_sigersol());
			cs.setString(15, clsDatosGenerales.getTelefono_sigersol());
			cs.setString(16, clsDatosGenerales.getCelular_sigersol());
			cs.setString(17, clsDatosGenerales.getEmail_sigersol());
			cs.setString(18, clsDatosGenerales.getDireccion_sigersol());
			cs.setInt(19, clsDatosGenerales.getPoblacion_urbana());
			cs.setInt(20, clsDatosGenerales.getPoblacion_rural());
			cs.setInt(21, clsDatosGenerales.getTotal_viviendas());
			cs.setInt(22, clsDatosGenerales.getCodigo_usuario());
			cs.registerOutParameter(23, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(23);
			
			log.info("DatosGeneralesDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsDatosGenerales clsDatosGenerales) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_datos_generales;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDatosGenerales.getDatos_generales_id());
			cs.setInt(2, clsDatosGenerales.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setString(3, clsDatosGenerales.getNombre_alcalde());
			cs.setString(4, clsDatosGenerales.getDireccion());
			cs.setString(5, clsDatosGenerales.getTelefono());
			cs.setString(6, clsDatosGenerales.getFax());
			cs.setString(7, clsDatosGenerales.getEmail());
			cs.setString(8, clsDatosGenerales.getClasif_municipalidad());
			cs.setString(9, clsDatosGenerales.getTipo_municipalidad());
			cs.setString(10, clsDatosGenerales.getNombre_slp());
			cs.setString(11, clsDatosGenerales.getResponsable_slp());
			cs.setString(12, clsDatosGenerales.getTelefono_slp());
			cs.setString(13, clsDatosGenerales.getCelular_slp());
			cs.setString(14, clsDatosGenerales.getEmail_slp());
			cs.setString(15, clsDatosGenerales.getNombre_sigersol());
			cs.setString(16, clsDatosGenerales.getTelefono_sigersol());
			cs.setString(17, clsDatosGenerales.getCelular_sigersol());
			cs.setString(18, clsDatosGenerales.getEmail_sigersol());
			cs.setString(19, clsDatosGenerales.getDireccion_sigersol());
			cs.setInt(20, clsDatosGenerales.getPoblacion_urbana());
			cs.setInt(21, clsDatosGenerales.getPoblacion_rural());
			cs.setInt(22, clsDatosGenerales.getTotal_viviendas());
			cs.setInt(23, clsDatosGenerales.getCodigo_usuario());
			cs.registerOutParameter(24, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(24);

			log.info("DatosGeneralesDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
