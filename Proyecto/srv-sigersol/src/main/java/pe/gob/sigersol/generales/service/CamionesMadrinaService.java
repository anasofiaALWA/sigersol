package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCamionesMadrina;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CamionesMadrinaDAO;

public class CamionesMadrinaService {
	
	private static Logger log = Logger.getLogger(CamionesMadrinaService.class);
	CamionesMadrinaDAO camionesMadrinaDAO = new CamionesMadrinaDAO();
	
	public ClsResultado listarCamionesMadrina(ClsCamionesMadrina clsCamionesMadrina){
		ClsResultado clsResultado = null;
		try {
			clsResultado = camionesMadrinaDAO.listarCamionesMadrina(clsCamionesMadrina);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCamionesMadrina clsCamionesMadrina) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = camionesMadrinaDAO.insertar(clsCamionesMadrina);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsCamionesMadrina clsCamionesMadrina) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = camionesMadrinaDAO.actualizar(clsCamionesMadrina);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
