package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCobertura;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoManejo;
import pe.gob.sigersol.generales.service.TipoManejoService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase TipoManejoRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de disposicion final permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/tipoManejo")
@RequestScoped
public class TipoManejoRestService {

	private static Logger log = Logger.getLogger(TipoManejoRestService.class.getName());
	private ClsTipoManejo clsTipoManejo;
	private ClsCobertura clsCobertura;
	private ClsDisposicionFinalAdm clsDisposicionFinalAdm;
	private TipoManejoService tipoManejoService;
	private List<ClsTipoManejo> listaTipoManejo;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarManejo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarManejo(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			tipoManejoService = new TipoManejoService();
			
			jsonEntradaListaTipoManejo(paramJson);
			
			log.info("**Obtener Tipo Manejo**");
			
			clsResultado = tipoManejoService.listarManejo(clsTipoManejo);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstManejo\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
		// Aplica para inserción y actualización

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		tipoManejoService = new TipoManejoService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaTipoManejo(paramJson);

			for (ClsTipoManejo clsTipoManejo : listaTipoManejo) {
				clsResultado = tipoManejoService.insertar(clsTipoManejo);
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		tipoManejoService = new TipoManejoService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaTipoManejo(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsTipoManejo clsTipoManejo : listaTipoManejo) {
			clsResultado = tipoManejoService.actualizar(clsTipoManejo);
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de TipoManejo
	 * 
	 * */
	private void jsonEntradaTipoManejo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> GeneracionNoDomiciliarRestService :	 jsonEntradaNoDomiciliar" + jsonEntrada);

		JSONObject jsonObjectCobertura;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaTipoManejo = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsTipoManejo = new ClsTipoManejo();
			clsCobertura = new ClsCobertura();
			clsDisposicionFinalAdm = new ClsDisposicionFinalAdm();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("disposicion_final_id") != null && innerObject.get("disposicion_final_id").toString() != "") {
				clsDisposicionFinalAdm.setDisposicion_final_id(Integer.parseInt(innerObject.get("disposicion_final_id").toString()));
			}
			
			clsTipoManejo.setDisposicion_final_adm(clsDisposicionFinalAdm);
			
			if (innerObject.get("cobertura") != null && innerObject.get("cobertura").toString() != "") {
				jsonObjectCobertura  = (JSONObject) innerObject.get("cobertura");
				clsCobertura.setCobertura_id(Integer.parseInt(jsonObjectCobertura.get("cobertura_id").toString()));
			}
			clsTipoManejo.setCobertura(clsCobertura);
			
			if (innerObject.get("espesor_cobertura") != null && innerObject.get("espesor_cobertura").toString() != "") {
				clsTipoManejo.setEspesor_cobertura(Integer.parseInt(innerObject.get("espesor_cobertura").toString()));
			} else {
				clsTipoManejo.setEspesor_cobertura(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsTipoManejo.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaTipoManejo.add(clsTipoManejo);	
		}
	
		log.info("----------------------------------------------------------------------------------");
	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaTipoManejo(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaPersona" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectTipoManejo;
		JSONParser jsonParser = new JSONParser();
		
		clsTipoManejo = new ClsTipoManejo();
		clsCobertura = new ClsCobertura();
		clsDisposicionFinalAdm = new ClsDisposicionFinalAdm();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectTipoManejo = (JSONObject) jsonObjectPrincipal.get("Manejo");
			
			if (jsonObjectTipoManejo.get("disposicion_final_id") != null && jsonObjectTipoManejo.get("disposicion_final_id") != "" &&
					jsonObjectTipoManejo.get("disposicion_final_id") != " ") {
				clsDisposicionFinalAdm.setDisposicion_final_id(Integer.parseInt(jsonObjectTipoManejo.get("disposicion_final_id").toString()));
			}
			else
				clsDisposicionFinalAdm.setDisposicion_final_id(-1);
			
			clsTipoManejo.setDisposicion_final_adm(clsDisposicionFinalAdm);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
