package pe.gob.sigersol.entities;

public class ClsAdmFinanzas {

	private Integer adm_finanzas_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private Integer recaudacion_arbitrios;
	private Integer cant_predios;
	private Integer predios_afectos;
	private Integer flag_ambiental;
	private String nombre_ambiental;
	private Integer flag_limpieza;
	private Integer flag_rentas;
	private Integer flag_poi;
	private Integer carga_poi;
	private Integer flag_pp;
	private String codigo_pp;
	private Integer monto_programado;
	private String codigo_programado;
	private Integer monto_ejecutado;
	private String codigo_ejecutado;
	private Integer flag_pmi;
	private Integer programado_inv;
	private Integer ejecutado_inv;
	private Integer flag_proyecto;
	private String codigo_proyecto;
	private Integer fuente_financiamiento;
	private Integer inv_no_proyectos;
	private Integer flag_manejo;
	private Integer flag_pigars;
	private Integer flag_instrumento; 
	private Integer flag_licencia;
	private Integer flag_exp_tecnico;
	private Integer flag_ordenanza;
	private Integer flag_tributario;
	private Integer codigo_usuario;
	
	
	public Integer getAdm_finanzas_id() {
		return adm_finanzas_id;
	}
	public void setAdm_finanzas_id(Integer adm_finanzas_id) {
		this.adm_finanzas_id = adm_finanzas_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public Integer getRecaudacion_arbitrios() {
		return recaudacion_arbitrios;
	}
	public void setRecaudacion_arbitrios(Integer recaudacion_arbitrios) {
		this.recaudacion_arbitrios = recaudacion_arbitrios;
	}
	public Integer getCant_predios() {
		return cant_predios;
	}
	public void setCant_predios(Integer cant_predios) {
		this.cant_predios = cant_predios;
	}
	public Integer getPredios_afectos() {
		return predios_afectos;
	}
	public void setPredios_afectos(Integer predios_afectos) {
		this.predios_afectos = predios_afectos;
	}
	public Integer getFlag_ambiental() {
		return flag_ambiental;
	}
	public void setFlag_ambiental(Integer flag_ambiental) {
		this.flag_ambiental = flag_ambiental;
	}
	public String getNombre_ambiental() {
		return nombre_ambiental;
	}
	public void setNombre_ambiental(String nombre_ambiental) {
		this.nombre_ambiental = nombre_ambiental;
	}
	public Integer getFlag_limpieza() {
		return flag_limpieza;
	}
	public void setFlag_limpieza(Integer flag_limpieza) {
		this.flag_limpieza = flag_limpieza;
	}
	public Integer getFlag_rentas() {
		return flag_rentas;
	}
	public void setFlag_rentas(Integer flag_rentas) {
		this.flag_rentas = flag_rentas;
	}
	public Integer getFlag_poi() {
		return flag_poi;
	}
	public void setFlag_poi(Integer flag_poi) {
		this.flag_poi = flag_poi;
	}
	public Integer getCarga_poi() {
		return carga_poi;
	}
	public void setCarga_poi(Integer carga_poi) {
		this.carga_poi = carga_poi;
	}
	public Integer getFlag_pp() {
		return flag_pp;
	}
	public void setFlag_pp(Integer flag_pp) {
		this.flag_pp = flag_pp;
	}
	public String getCodigo_pp() {
		return codigo_pp;
	}
	public void setCodigo_pp(String codigo_pp) {
		this.codigo_pp = codigo_pp;
	}
	public Integer getMonto_programado() {
		return monto_programado;
	}
	public void setMonto_programado(Integer monto_programado) {
		this.monto_programado = monto_programado;
	}
	public String getCodigo_programado() {
		return codigo_programado;
	}
	public void setCodigo_programado(String codigo_programado) {
		this.codigo_programado = codigo_programado;
	}
	public Integer getMonto_ejecutado() {
		return monto_ejecutado;
	}
	public void setMonto_ejecutado(Integer monto_ejecutado) {
		this.monto_ejecutado = monto_ejecutado;
	}
	public String getCodigo_ejecutado() {
		return codigo_ejecutado;
	}
	public void setCodigo_ejecutado(String codigo_ejecutado) {
		this.codigo_ejecutado = codigo_ejecutado;
	}
	public Integer getFlag_pmi() {
		return flag_pmi;
	}
	public void setFlag_pmi(Integer flag_pmi) {
		this.flag_pmi = flag_pmi;
	}
	public Integer getProgramado_inv() {
		return programado_inv;
	}
	public void setProgramado_inv(Integer programado_inv) {
		this.programado_inv = programado_inv;
	}
	public Integer getEjecutado_inv() {
		return ejecutado_inv;
	}
	public void setEjecutado_inv(Integer ejecutado_inv) {
		this.ejecutado_inv = ejecutado_inv;
	}
	public Integer getFlag_proyecto() {
		return flag_proyecto;
	}
	public void setFlag_proyecto(Integer flag_proyecto) {
		this.flag_proyecto = flag_proyecto;
	}
	public String getCodigo_proyecto() {
		return codigo_proyecto;
	}
	public void setCodigo_proyecto(String codigo_proyecto) {
		this.codigo_proyecto = codigo_proyecto;
	}
	public Integer getFuente_financiamiento() {
		return fuente_financiamiento;
	}
	public void setFuente_financiamiento(Integer fuente_financiamiento) {
		this.fuente_financiamiento = fuente_financiamiento;
	}
	public Integer getInv_no_proyectos() {
		return inv_no_proyectos;
	}
	public void setInv_no_proyectos(Integer inv_no_proyectos) {
		this.inv_no_proyectos = inv_no_proyectos;
	}
	public Integer getFlag_manejo() {
		return flag_manejo;
	}
	public void setFlag_manejo(Integer flag_manejo) {
		this.flag_manejo = flag_manejo;
	}
	public Integer getFlag_pigars() {
		return flag_pigars;
	}
	public void setFlag_pigars(Integer flag_pigars) {
		this.flag_pigars = flag_pigars;
	}
	public Integer getFlag_instrumento() {
		return flag_instrumento;
	}
	public void setFlag_instrumento(Integer flag_instrumento) {
		this.flag_instrumento = flag_instrumento;
	}
	public Integer getFlag_licencia() {
		return flag_licencia;
	}
	public void setFlag_licencia(Integer flag_licencia) {
		this.flag_licencia = flag_licencia;
	}
	public Integer getFlag_exp_tecnico() {
		return flag_exp_tecnico;
	}
	public void setFlag_exp_tecnico(Integer flag_exp_tecnico) {
		this.flag_exp_tecnico = flag_exp_tecnico;
	}
	public Integer getFlag_ordenanza() {
		return flag_ordenanza;
	}
	public void setFlag_ordenanza(Integer flag_ordenanza) {
		this.flag_ordenanza = flag_ordenanza;
	}
	public Integer getFlag_tributario() {
		return flag_tributario;
	}
	public void setFlag_tributario(Integer flag_tributario) {
		this.flag_tributario = flag_tributario;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
