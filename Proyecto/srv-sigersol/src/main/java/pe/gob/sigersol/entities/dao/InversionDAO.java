package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsInversion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoInversion;

public class InversionDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(InversionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_inversion = "{call SIGERSOLBL.SGR_SP_INS_INVERSION(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_inversion = "{call SIGERSOLBL.SGR_SP_UPD_INVERSION(?,?,?,?,?,?)}";
	private final String sgr_sp_list_inversion = "{call SIGERSOLBL.SGR_SP_LIST_INVERSION(?,?)}";  
	
	
	public ClsResultado listarInversion(ClsInversion clsInversion){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsInversion> lista = new ArrayList<ClsInversion>();
		con = null;
		String sql = this.sgr_sp_list_inversion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsInversion.getAdm_finanzas().getAdm_finanzas_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsInversion item = new ClsInversion();
				ClsTipoInversion tipoInversion = new ClsTipoInversion();
				
				try {
					tipoInversion.setTipo_inversion_id(rs.getInt("TIPO_INVERSION_ID"));
					tipoInversion.setTipo_inversion(rs.getString("TIPO_INVERSION"));
					tipoInversion.setProceso(rs.getString("PROCESO"));
					item.setTipo_inversion(tipoInversion);
					if(clsInversion.getAdm_finanzas().getAdm_finanzas_id() != -1){
						item.setMonto_inv(rs.getInt("MONTO_INV"));
						item.setFuente_financ(rs.getInt("FUENTE_FINANC"));
					}else{
						item.setMonto_inv(null);
						item.setFuente_financ(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsInversion clsInversion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_inversion;
		
		Integer cont_contenedor = 1;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsInversion.getAdm_finanzas().getAdm_finanzas_id());
			cs.setInt(2, clsInversion.getTipo_inversion().getTipo_inversion_id());
			cs.setInt(3, clsInversion.getMonto_inv());
			cs.setInt(4, clsInversion.getFuente_financ());
			cs.setInt(5, clsInversion.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			cont_contenedor++;
			
			log.info("InversionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsInversion clsInversion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_inversion;

		Integer cont_contenedor = 1;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsInversion.getAdm_finanzas().getAdm_finanzas_id());
			cs.setInt(2, clsInversion.getTipo_inversion().getTipo_inversion_id());
			cs.setInt(3, clsInversion.getMonto_inv());
			cs.setInt(4, clsInversion.getFuente_financ());
			cs.setInt(5, clsInversion.getCodigo_usuario());
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(9);

			cont_contenedor++;
			
			log.info("InversionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
