package pe.gob.sigersol.entities;

public class ClsOtroMunicipales {

	private Integer otro_municipales_id;
	private ClsRecRcdOm recRcdOm;
	private Integer flag_municipal;
	private Integer adm_serv_id;
	private Integer costo;
	private String descrip_residuos_otros;
	private Integer usuario_atendido_otros;
	private Integer cantidad_otros;
	private Integer codigo_usuario;
	
	public Integer getOtro_municipales_id() {
		return otro_municipales_id;
	}
	public void setOtro_municipales_id(Integer otro_municipales_id) {
		this.otro_municipales_id = otro_municipales_id;
	}
	public ClsRecRcdOm getRecRcdOm() {
		return recRcdOm;
	}
	public void setRecRcdOm(ClsRecRcdOm recRcdOm) {
		this.recRcdOm = recRcdOm;
	}
	public Integer getFlag_municipal() {
		return flag_municipal;
	}
	public void setFlag_municipal(Integer flag_municipal) {
		this.flag_municipal = flag_municipal;
	}
	public Integer getAdm_serv_id() {
		return adm_serv_id;
	}
	public void setAdm_serv_id(Integer adm_serv_id) {
		this.adm_serv_id = adm_serv_id;
	}
	public Integer getCosto() {
		return costo;
	}
	public void setCosto(Integer costo) {
		this.costo = costo;
	}
	public String getDescrip_residuos_otros() {
		return descrip_residuos_otros;
	}
	public void setDescrip_residuos_otros(String descrip_residuos_otros) {
		this.descrip_residuos_otros = descrip_residuos_otros;
	}
	public Integer getUsuario_atendido_otros() {
		return usuario_atendido_otros;
	}
	public void setUsuario_atendido_otros(Integer usuario_atendido_otros) {
		this.usuario_atendido_otros = usuario_atendido_otros;
	}
	public Integer getCantidad_otros() {
		return cantidad_otros;
	}
	public void setCantidad_otros(Integer cantidad_otros) {
		this.cantidad_otros = cantidad_otros;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
