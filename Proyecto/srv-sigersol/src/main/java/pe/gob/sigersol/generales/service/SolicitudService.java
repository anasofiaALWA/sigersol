package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSolicitud;
import pe.gob.sigersol.entities.dao.SolicitudDAO;

public class SolicitudService {
	private static Logger log = Logger.getLogger(SolicitudService.class);
	SolicitudDAO solicitudDAO = new SolicitudDAO();
	
	public ClsResultado obtener(){
		ClsResultado clsResultado = null;
		try {
			clsResultado = solicitudDAO.obtener();
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsSolicitud clsSolicitud) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = solicitudDAO.insertar(clsSolicitud);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsSolicitud clsSolicitud) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = solicitudDAO.actualizar(clsSolicitud);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
