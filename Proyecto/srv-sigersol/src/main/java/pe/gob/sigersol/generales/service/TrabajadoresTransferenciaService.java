package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTrabajadores;
import pe.gob.sigersol.entities.dao.TrabajadoresTransferenciaDAO;

public class TrabajadoresTransferenciaService {

	private static Logger log = Logger.getLogger(TrabajadoresService.class);
	TrabajadoresTransferenciaDAO trabajadoresTransferenciaDAO = new TrabajadoresTransferenciaDAO();

	public ClsResultado listarTrabajadores(ClsTrabajadores clsTrabajadores){
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresTransferenciaDAO.listarTrabajadores(clsTrabajadores);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTrabajadores clsTrabajadores) {
		
		ClsResultado clsResultado = null;
		try {
			clsResultado = trabajadoresTransferenciaDAO.insertar(clsTrabajadores);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
