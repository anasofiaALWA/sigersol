package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsFuncion;
import pe.gob.sigersol.entities.ClsResultado;

public class FuncionDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(FuncionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String ep_mg_list_funcion = "{call COPEREBL.EP_MG_LIST_FUNCION(?)}";
	private final String ep_mg_list_funcion_sin_asig = "{call COPEREBL.EP_MG_LIST_FUNCION_SIN_ASIG(?,?,?)}";
	
	public ClsResultado obtener_lista_funcion() {
		ClsResultado clsResultado = new ClsResultado();
		ClsFuncion item = null;
		List<ClsFuncion> lista = new ArrayList<ClsFuncion>();
		con = null;
		String sql = this.ep_mg_list_funcion;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				item = new ClsFuncion();
				try {
					item.setFuncion_id(rs.getInt("FUNCION_ID"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setFuncion_nombre(rs.getString("FUNCION_NOMBRE"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setCodigo_estado(rs.getInt("CODIGO_ESTADO"));
				} catch (Exception e) {
					// TODO: handle exception
				}

				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_lista_funcion: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_lista_funcion: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje(ex.getMessage());
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_lista_funcion: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje(ex.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_lista_funcion: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje(exception.getMessage());
			}
		}
		return clsResultado;
	}
	
	public ClsResultado obtener_lista_funcion_sin_asignar(String perfil_id, String funcion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsFuncion item = null;
		List<ClsFuncion> lista = new ArrayList<ClsFuncion>();
		con = null;
		String sql = this.ep_mg_list_funcion_sin_asig;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, perfil_id);
			if (funcion == null || funcion.trim().equals("")) {
				log.info("parametro funcion vacio");
				cs.setString(2, "%");
			} else {
				cs.setString(2, "%" + funcion + "%");
			}
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(3);
			
			while (rs.next()) {
				item = new ClsFuncion();
				try {
					item.setFuncion_id(rs.getInt("FUNCION_ID"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setFuncion_nombre(rs.getString("FUNCION_NOMBRE"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					item.setCodigo_estado(rs.getInt("CODIGO_ESTADO"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					item.setCodigo_padre(rs.getInt("CODIGO_PADRE"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					item.setNivel(rs.getInt("NIVEL"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					item.setOrden(rs.getInt("ORDEN"));
				} catch (Exception e) {
					// TODO: handle exception
				}

				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_lista_funcion: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_lista_funcion: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje(ex.getMessage());
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_lista_funcion: " + ex.getMessage() + ex);
			clsResultado.setObjeto(null);
			clsResultado.setExito(false);
			clsResultado.setMensaje(ex.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_lista_funcion: " + exception.getMessage() + exception);
				clsResultado.setObjeto(null);
				clsResultado.setExito(false);
				clsResultado.setMensaje(exception.getMessage());
			}
		}
		return clsResultado;
	}
}
