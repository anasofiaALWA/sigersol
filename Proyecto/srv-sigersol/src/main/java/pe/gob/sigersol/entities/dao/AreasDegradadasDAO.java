package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsAreasDegradadas;
import pe.gob.sigersol.entities.ClsDisposicionFinal;
import pe.gob.sigersol.entities.ClsDisposicionFinalAdm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsSitioDisposicion;

public class AreasDegradadasDAO extends GenericoDAO{

	private static Logger log = Logger.getLogger(AreasDegradadasDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_admin_sdf2 = "{call SIGERSOLBL.SGR_SP_LIST_ADMIN_SDF2(?,?)}";
	private final String sgr_sp_ins_admin_sdf2 = "{call SIGERSOLBL.SGR_SP_INS_ADMIN_SDF2(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_admin_sdf2 = "{call SIGERSOLBL.SGR_SP_UPD_ADMIN_SDF2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		ClsResultado clsResultado = new ClsResultado();
		ClsDisposicionFinalAdm item = new ClsDisposicionFinalAdm();
		ClsSitioDisposicion sitioDisposicion = new ClsSitioDisposicion();
		con = null;
		String sql = this.sgr_sp_list_admin_sdf2;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinalAdm.getSitio_disposicion_id().getSitio_disposicion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setDisposicion_final_id(rs.getInt("ADMIN_SDF_ID"));
					sitioDisposicion.setSitio_disposicion_id(rs.getInt("SITIO_DISPOSICION_ID"));
					item.setSitio_disposicion_id(sitioDisposicion);
					item.setAnio_inicio(rs.getInt("ANIO_INICIO"));
					item.setCant_personal(rs.getInt("CANT_PERSONAL"));
					item.setAdmin_servicio_id(rs.getString("ADMIN_SERVICIO_ID"));
					item.setSituacion_terreno_id(rs.getString("SITUACION_TERRENO_ID"));
					item.setDireccion(rs.getString("DIRECCION"));
					item.setRef_direccion(rs.getString("REF_DIRECCION"));
					item.setArea_total_terreno(rs.getInt("AREA_TOTAL_TERRENO"));
					item.setArea_utilizada_terreno(rs.getInt("AREA_UTILIZADA_TERRENO"));
					item.setAltura_rs_ss(rs.getString("ALTURA_RS_SS"));
					item.setTipo_manejo_cob_id(rs.getString("TIPO_MANEJO_COB_ID"));
					item.setAltura_nf(rs.getInt("ALTURA_NF"));
					
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_admin_sdf2;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinalAdm.getSitio_disposicion_id().getSitio_disposicion_id());
			cs.setInt(2, clsDisposicionFinalAdm.getAnio_inicio());
			cs.setInt(3, clsDisposicionFinalAdm.getCant_personal());
			cs.setString(4, clsDisposicionFinalAdm.getAdmin_servicio_id());
			cs.setString(5, clsDisposicionFinalAdm.getSituacion_terreno_id());
			cs.setString(6, clsDisposicionFinalAdm.getDireccion());
			cs.setString(7, clsDisposicionFinalAdm.getRef_direccion());
			cs.setInt(8, clsDisposicionFinalAdm.getArea_total_terreno());
			cs.setInt(9, clsDisposicionFinalAdm.getArea_utilizada_terreno());
			cs.setString(10, clsDisposicionFinalAdm.getAltura_rs_ss());
			cs.setString(11, clsDisposicionFinalAdm.getTipo_manejo_cob_id());
			cs.setInt(12, clsDisposicionFinalAdm.getAltura_nf());			
			cs.setInt(13, clsDisposicionFinalAdm.getCodigo_usuario());
			cs.registerOutParameter(14, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(14);
			
			log.info("AreasDegradadasDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsDisposicionFinalAdm clsDisposicionFinalAdm) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_admin_sdf2;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsDisposicionFinalAdm.getDisposicion_final_id());
			cs.setInt(2, clsDisposicionFinalAdm.getSitio_disposicion_id().getSitio_disposicion_id());
			cs.setInt(3, clsDisposicionFinalAdm.getAnio_inicio());
			cs.setInt(4, clsDisposicionFinalAdm.getCant_personal());
			cs.setString(5, clsDisposicionFinalAdm.getAdmin_servicio_id());
			cs.setString(6, clsDisposicionFinalAdm.getSituacion_terreno_id());
			cs.setString(7, clsDisposicionFinalAdm.getDireccion());
			cs.setString(8, clsDisposicionFinalAdm.getRef_direccion());
			cs.setInt(9, clsDisposicionFinalAdm.getArea_total_terreno());
			cs.setInt(10, clsDisposicionFinalAdm.getArea_utilizada_terreno());
			cs.setString(11, clsDisposicionFinalAdm.getAltura_rs_ss());
			cs.setString(12, clsDisposicionFinalAdm.getTipo_manejo_cob_id());
			cs.setInt(13, clsDisposicionFinalAdm.getAltura_nf());			
			cs.setInt(14, clsDisposicionFinalAdm.getCodigo_usuario());
			cs.registerOutParameter(15, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(15);

			log.info("AreasDegradadasDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
