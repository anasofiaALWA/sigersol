package pe.gob.sigersol.entities;

public class ClsMunicipalidad {
	
	private Integer municipalidad_id;
	private String codigo_ubigeo_departamento;
	private String codigo_ubigeo_provincia;
	private String codigo_ubigeo_distrito;
	private String ruc;
	private String nombre_alcalde;
	private String nombre_gerencia;
	private String responsable_limpieza;
	private String direccion;
	private String telefono;
	private String fax;
	private Integer clasificacion_municipalidad;
	private Integer tipo_municipalidad;
	private Integer poblacion_urbana;
	private String poblacion_urbana_fuente;
	private Integer poblacion_rural;
	private String poblacion_rural_fuente;
	private Integer codigo_estado;
	private Integer codigo_usuario;
	private ClsUbigeo ubigeo;
	
	public Integer getMunicipalidad_id() {
		return municipalidad_id;
	}
	public void setMunicipalidad_id(Integer municipalidad_id) {
		this.municipalidad_id = municipalidad_id;
	}
	public String getCodigo_ubigeo_departamento() {
		return codigo_ubigeo_departamento;
	}
	public void setCodigo_ubigeo_departamento(String codigo_ubigeo_departamento) {
		this.codigo_ubigeo_departamento = codigo_ubigeo_departamento;
	}
	public String getCodigo_ubigeo_provincia() {
		return codigo_ubigeo_provincia;
	}
	public void setCodigo_ubigeo_provincia(String codigo_ubigeo_provincia) {
		this.codigo_ubigeo_provincia = codigo_ubigeo_provincia;
	}
	public String getCodigo_ubigeo_distrito() {
		return codigo_ubigeo_distrito;
	}
	public void setCodigo_ubigeo_distrito(String codigo_ubigeo_distrito) {
		this.codigo_ubigeo_distrito = codigo_ubigeo_distrito;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getNombre_alcalde() {
		return nombre_alcalde;
	}
	public void setNombre_alcalde(String nombre_alcalde) {
		this.nombre_alcalde = nombre_alcalde;
	}
	public String getNombre_gerencia() {
		return nombre_gerencia;
	}
	public void setNombre_gerencia(String nombre_gerencia) {
		this.nombre_gerencia = nombre_gerencia;
	}
	public String getResponsable_limpieza() {
		return responsable_limpieza;
	}
	public void setResponsable_limpieza(String responsable_limpieza) {
		this.responsable_limpieza = responsable_limpieza;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public Integer getClasificacion_municipalidad() {
		return clasificacion_municipalidad;
	}
	public void setClasificacion_municipalidad(Integer clasificacion_municipalidad) {
		this.clasificacion_municipalidad = clasificacion_municipalidad;
	}
	public Integer getTipo_municipalidad() {
		return tipo_municipalidad;
	}
	public void setTipo_municipalidad(Integer tipo_municipalidad) {
		this.tipo_municipalidad = tipo_municipalidad;
	}
	public Integer getPoblacion_urbana() {
		return poblacion_urbana;
	}
	public void setPoblacion_urbana(Integer poblacion_urbana) {
		this.poblacion_urbana = poblacion_urbana;
	}
	public String getPoblacion_urbana_fuente() {
		return poblacion_urbana_fuente;
	}
	public void setPoblacion_urbana_fuente(String poblacion_urbana_fuente) {
		this.poblacion_urbana_fuente = poblacion_urbana_fuente;
	}
	public Integer getPoblacion_rural() {
		return poblacion_rural;
	}
	public void setPoblacion_rural(Integer poblacion_rural) {
		this.poblacion_rural = poblacion_rural;
	}
	public String getPoblacion_rural_fuente() {
		return poblacion_rural_fuente;
	}
	public void setPoblacion_rural_fuente(String poblacion_rural_fuente) {
		this.poblacion_rural_fuente = poblacion_rural_fuente;
	}
	public Integer getCodigo_estado() {
		return codigo_estado;
	}
	public void setCodigo_estado(Integer codigo_estado) {
		this.codigo_estado = codigo_estado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	public ClsUbigeo getUbigeo() {
		return ubigeo;
	}
	public void setUbigeo(ClsUbigeo ubigeo) {
		this.ubigeo = ubigeo;
	}
	
	
	
}
