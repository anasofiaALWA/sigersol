package pe.gob.sigersol.entities;

public class ClsCantidadResiduos {

	private Integer cantidad_residuos_id;
	private ClsRecRcdOm recRcdOm;
	private ClsTipoResiduo tipoResiduos;
	private Integer usuarioAtendido;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getCantidad_residuos_id() {
		return cantidad_residuos_id;
	}
	public void setCantidad_residuos_id(Integer cantidad_residuos_id) {
		this.cantidad_residuos_id = cantidad_residuos_id;
	}
	public ClsRecRcdOm getRecRcdOm() {
		return recRcdOm;
	}
	public void setRecRcdOm(ClsRecRcdOm recRcdOm) {
		this.recRcdOm = recRcdOm;
	}
	public ClsTipoResiduo getTipoResiduos() {
		return tipoResiduos;
	}
	public void setTipoResiduos(ClsTipoResiduo tipoResiduos) {
		this.tipoResiduos = tipoResiduos;
	}
	public Integer getUsuarioAtendido() {
		return usuarioAtendido;
	}
	public void setUsuarioAtendido(Integer usuarioAtendido) {
		this.usuarioAtendido = usuarioAtendido;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
