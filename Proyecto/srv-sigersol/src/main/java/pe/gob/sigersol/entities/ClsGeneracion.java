package pe.gob.sigersol.entities;

public class ClsGeneracion {
	
	private Integer generacion_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private Integer flag_generacion;
	private Integer poblacion_urbana;
	private Integer poblacion_rural;
	private Integer densidad;
	private Integer cantidad_viviendas;
	private Double gpc_diario;
	private String descripcion;
	private Double porcentaje;
	private Integer total_no_domiciliar;
	private Integer total_composicion;
	private Integer total_residuos_especiales;
	private Double generacion_total_rsd;
	private Double generacion_total_rsnd;
	private Double generacion_total_rcd_om;
	private Integer codigo_usuario;
	
	public Integer getGeneracion_id() {
		return generacion_id;
	}
	public void setGeneracion_id(Integer generacion_id) {
		this.generacion_id = generacion_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public Integer getFlag_generacion() {
		return flag_generacion;
	}
	public void setFlag_generacion(Integer flag_generacion) {
		this.flag_generacion = flag_generacion;
	}
	public Integer getPoblacion_urbana() {
		return poblacion_urbana;
	}
	public void setPoblacion_urbana(Integer poblacion_urbana) {
		this.poblacion_urbana = poblacion_urbana;
	}
	public Integer getPoblacion_rural() {
		return poblacion_rural;
	}
	public void setPoblacion_rural(Integer poblacion_rural) {
		this.poblacion_rural = poblacion_rural;
	}
	public Integer getDensidad() {
		return densidad;
	}
	public void setDensidad(Integer densidad) {
		this.densidad = densidad;
	}
	public Integer getCantidad_viviendas() {
		return cantidad_viviendas;
	}
	public void setCantidad_viviendas(Integer cantidad_viviendas) {
		this.cantidad_viviendas = cantidad_viviendas;
	}
	public Double getGpc_diario() {
		return gpc_diario;
	}
	public void setGpc_diario(Double gpc_diario) {
		this.gpc_diario = gpc_diario;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public Integer getTotal_no_domiciliar() {
		return total_no_domiciliar;
	}
	public void setTotal_no_domiciliar(Integer total_no_domiciliar) {
		this.total_no_domiciliar = total_no_domiciliar;
	}
	public Integer getTotal_composicion() {
		return total_composicion;
	}
	public void setTotal_composicion(Integer total_composicion) {
		this.total_composicion = total_composicion;
	}
	public Integer getTotal_residuos_especiales() {
		return total_residuos_especiales;
	}
	public void setTotal_residuos_especiales(Integer total_residuos_especiales) {
		this.total_residuos_especiales = total_residuos_especiales;
	}
	public Double getGeneracion_total_rsd() {
		return generacion_total_rsd;
	}
	public void setGeneracion_total_rsd(Double generacion_total_rsd) {
		this.generacion_total_rsd = generacion_total_rsd;
	}
	public Double getGeneracion_total_rsnd() {
		return generacion_total_rsnd;
	}
	public void setGeneracion_total_rsnd(Double generacion_total_rsnd) {
		this.generacion_total_rsnd = generacion_total_rsnd;
	}
	public Double getGeneracion_total_rcd_om() {
		return generacion_total_rcd_om;
	}
	public void setGeneracion_total_rcd_om(Double generacion_total_rcd_om) {
		this.generacion_total_rcd_om = generacion_total_rcd_om;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
