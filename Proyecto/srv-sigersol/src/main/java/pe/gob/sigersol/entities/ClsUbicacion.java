package pe.gob.sigersol.entities;

public class ClsUbicacion {

	private Integer ubicacion_id;
	private ClsLugarAprovechamiento lugar_aprovechamiento;
	private String zona_coordenada;
	private String norte_coordenada;
	private String sur_coordenada;
	private String div_politica_dep;
	private String div_politica_prov;
	private String div_politica_dist;
	private String direccion;
	private String referencia;
	private Integer codigo_usuario;
	
	public Integer getUbicacion_id() {
		return ubicacion_id;
	}
	public void setUbicacion_id(Integer ubicacion_id) {
		this.ubicacion_id = ubicacion_id;
	}
	public ClsLugarAprovechamiento getLugar_aprovechamiento() {
		return lugar_aprovechamiento;
	}
	public void setLugar_aprovechamiento(ClsLugarAprovechamiento lugar_aprovechamiento) {
		this.lugar_aprovechamiento = lugar_aprovechamiento;
	}
	public String getZona_coordenada() {
		return zona_coordenada;
	}
	public void setZona_coordenada(String zona_coordenada) {
		this.zona_coordenada = zona_coordenada;
	}
	public String getNorte_coordenada() {
		return norte_coordenada;
	}
	public void setNorte_coordenada(String norte_coordenada) {
		this.norte_coordenada = norte_coordenada;
	}
	public String getSur_coordenada() {
		return sur_coordenada;
	}
	public void setSur_coordenada(String sur_coordenada) {
		this.sur_coordenada = sur_coordenada;
	}
	public String getDiv_politica_dep() {
		return div_politica_dep;
	}
	public void setDiv_politica_dep(String div_politica_dep) {
		this.div_politica_dep = div_politica_dep;
	}
	public String getDiv_politica_prov() {
		return div_politica_prov;
	}
	public void setDiv_politica_prov(String div_politica_prov) {
		this.div_politica_prov = div_politica_prov;
	}
	public String getDiv_politica_dist() {
		return div_politica_dist;
	}
	public void setDiv_politica_dist(String div_politica_dist) {
		this.div_politica_dist = div_politica_dist;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
