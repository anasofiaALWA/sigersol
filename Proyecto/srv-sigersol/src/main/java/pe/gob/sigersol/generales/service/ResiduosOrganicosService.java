package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResiduosOrganicos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ResiduosOrganicosDAO;

public class ResiduosOrganicosService {

	private static Logger log = Logger.getLogger(ResiduosOrganicosService.class);
	ResiduosOrganicosDAO residuosOrganicosDAO = new ResiduosOrganicosDAO();
	
	public ClsResultado listarResiduosOrganicos(ClsResiduosOrganicos clsResiduosOrganicos){
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosOrganicosDAO.listarResiduosOrganicos(clsResiduosOrganicos);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsResiduosOrganicos clsResiduosOrganicos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosOrganicosDAO.insertar(clsResiduosOrganicos);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsResiduosOrganicos clsResiduosOrganicos) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosOrganicosDAO.actualizar(clsResiduosOrganicos);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
