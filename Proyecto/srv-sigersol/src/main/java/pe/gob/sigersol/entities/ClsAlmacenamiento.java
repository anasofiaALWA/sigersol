package pe.gob.sigersol.entities;

public class ClsAlmacenamiento {
	
	private Integer almacenamiento_id;
	private ClsCicloGestionResiduos cicloGestionResiduos;
	private Integer costo_anual;
	private Integer capacidad_papeleras;
	private Integer metal;
	private Integer fibra_vidrio;
	private Integer plastico;
	private Integer mixto;
	private String descripcion_mixto;
	private Double total_recolectado;
	private Integer codigo_usuario;
	
	public Integer getAlmacenamiento_id() {
		return almacenamiento_id;
	}
	public void setAlmacenamiento_id(Integer almacenamiento_id) {
		this.almacenamiento_id = almacenamiento_id;
	}
	public ClsCicloGestionResiduos getCicloGestionResiduos() {
		return cicloGestionResiduos;
	}
	public void setCicloGestionResiduos(ClsCicloGestionResiduos cicloGestionResiduos) {
		this.cicloGestionResiduos = cicloGestionResiduos;
	}
	public Integer getCosto_anual() {
		return costo_anual;
	}
	public void setCosto_anual(Integer costo_anual) {
		this.costo_anual = costo_anual;
	}
	public Integer getCapacidad_papeleras() {
		return capacidad_papeleras;
	}
	public void setCapacidad_papeleras(Integer capacidad_papeleras) {
		this.capacidad_papeleras = capacidad_papeleras;
	}
	public Integer getMetal() {
		return metal;
	}
	public void setMetal(Integer metal) {
		this.metal = metal;
	}
	public Integer getFibra_vidrio() {
		return fibra_vidrio;
	}
	public void setFibra_vidrio(Integer fibra_vidrio) {
		this.fibra_vidrio = fibra_vidrio;
	}
	public Integer getPlastico() {
		return plastico;
	}
	public void setPlastico(Integer plastico) {
		this.plastico = plastico;
	}
	public Integer getMixto() {
		return mixto;
	}
	public void setMixto(Integer mixto) {
		this.mixto = mixto;
	}
	public String getDescripcion_mixto() {
		return descripcion_mixto;
	}
	public void setDescripcion_mixto(String descripcion_mixto) {
		this.descripcion_mixto = descripcion_mixto;
	}
	public Double getTotal_recolectado() {
		return total_recolectado;
	}
	public void setTotal_recolectado(Double total_recolectado) {
		this.total_recolectado = total_recolectado;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
