package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsMes;
import pe.gob.sigersol.entities.ClsMuniDispDf;
import pe.gob.sigersol.entities.ClsResultado;

public class MuniDispDfDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(MuniDispDfDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_rs_dispuesto = "{call SIGERSOLBL.SGR_SP_INS_RS_DISPUESTO(?,?,?,?,?,?)}";
	private final String sgr_sp_upd_rs_dispuesto = "{call SIGERSOLBL.SGR_SP_UPD_RS_DISPUESTO(?,?,?,?,?,?)}";
	private final String sgr_sp_list_rs_dispuesto = "{call SIGERSOLBL.SGR_SP_LIST_RS_DISPUESTO(?,?)}";  
	
	public ClsResultado listarMuniDisp(ClsMuniDispDf clsMuniDispDf){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsMuniDispDf> lista = new ArrayList<ClsMuniDispDf>();
		con = null;
		String sql = this.sgr_sp_list_rs_dispuesto;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsMuniDispDf.getDisposicionFinal().getDisposicion_final_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsMuniDispDf item = new ClsMuniDispDf();
				ClsMes mes = new ClsMes();
				
				try {
					mes.setMes_id(rs.getInt("MES_ID"));
					mes.setNombre_mes(rs.getString("NOMBRE_MES"));
					item.setMes(mes);
					if(clsMuniDispDf.getDisposicionFinal().getDisposicion_final_id() != -1){
						item.setCantidad(rs.getInt("CANTIDAD"));
						item.setMetodo_usado(rs.getInt("METODO_USADO"));
					}
					else{
						item.setCantidad(null);
						item.setMetodo_usado(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsMuniDispDf clsMuniDispDf) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_rs_dispuesto;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsMuniDispDf.getDisposicionFinal().getDisposicion_final_id());
			cs.setInt(2, clsMuniDispDf.getMes().getMes_id());
			cs.setInt(3, clsMuniDispDf.getCantidad());
			cs.setInt(4, clsMuniDispDf.getMetodo_usado());
			cs.setInt(5, clsMuniDispDf.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);
			
			log.info("MuniDispDfDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsMuniDispDf clsMuniDispDf) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_rs_dispuesto;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsMuniDispDf.getDisposicionFinal().getDisposicion_final_id());
			cs.setInt(2, clsMuniDispDf.getMes().getMes_id());
			cs.setInt(3, clsMuniDispDf.getCantidad());
			cs.setInt(4, clsMuniDispDf.getMetodo_usado());
			cs.setInt(5, clsMuniDispDf.getCodigo_usuario());
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(6);

			log.info("MuniDispDfDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
