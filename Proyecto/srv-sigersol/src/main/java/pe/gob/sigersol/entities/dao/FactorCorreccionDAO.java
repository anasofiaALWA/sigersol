package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsFactorCorreccion;
import pe.gob.sigersol.entities.ClsResultado;

public class FactorCorreccionDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(FactorCorreccionDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_cc_sel_factor_correccion = "{call SIGERSOLBL.SGR_SP_CC_SEL_FACTOR_CORREC(?,?)}";

	public ClsResultado listarFactor(Integer disposicionFinalId) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsFactorCorreccion> lista = new ArrayList<ClsFactorCorreccion>();
		ClsFactorCorreccion item = null;
		con = null;
		String sql = this.sgr_sp_cc_sel_factor_correccion;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);

			cs.setInt(1, disposicionFinalId);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				item = new ClsFactorCorreccion();
				try {
					item.setFactorCorreccionId(rs.getInt("FACTOR_CORRECCION_ID"));
				} catch (Exception e) {

				}
				try {
					item.setCondicionId(rs.getString("CONDICION"));
				} catch (Exception e) {

				}
				try {
					item.setDescripcion(rs.getString("APL_CONDICION"));
				} catch (Exception e) {

				}
				try {
					item.setValor_phi(rs.getDouble("VALOR_PHI"));
				} catch (Exception e) {

				}

				lista.add(item);
			}
			clsResultado.setExito(true);
			clsResultado.setObjeto(lista);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 listarFactor: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listarFactor: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listarFactor: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listarFactor: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;

	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}

}
