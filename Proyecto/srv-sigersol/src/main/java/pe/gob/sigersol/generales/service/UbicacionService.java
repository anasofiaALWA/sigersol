package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCostoReferencial;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUbicacion;
import pe.gob.sigersol.entities.dao.UbicacionDAO;

public class UbicacionService {
	
	private static Logger log = Logger.getLogger(UbicacionService.class);
	UbicacionDAO ubicacionDAO = new UbicacionDAO();
	
	public ClsResultado listarUbicacion(ClsUbicacion clsUbicacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = ubicacionDAO.listarUbicacion(clsUbicacion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado listarCoordenada(ClsUbicacion clsUbicacion){
		ClsResultado clsResultado = null;
		try {
			clsResultado = ubicacionDAO.listarCoordenada(clsUbicacion);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsUbicacion clsUbicacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = ubicacionDAO.insertar(clsUbicacion);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsUbicacion clsUbicacion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = ubicacionDAO.actualizar(clsUbicacion);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
