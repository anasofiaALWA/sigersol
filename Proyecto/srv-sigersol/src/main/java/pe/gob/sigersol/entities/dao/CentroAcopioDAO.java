package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCentroAcopio;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;

public class CentroAcopioDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(CentroAcopioDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_centro_acopio = "{call SIGERSOLBL.SGR_SP_LIST_SGR_CENTRO_ACOPIO(?,?)}";
	private final String sgr_sp_ins_centro_acopio = "{call SIGERSOLBL.SGR_SP_INS_CENTRO_ACOPIO(?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_centro_acopio = "{call SIGERSOLBL.SGR_SP_UPD_CENTRO_ACOPIO(?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsCentroAcopio clsCentroAcopio) {
		ClsResultado clsResultado = new ClsResultado();
		ClsCentroAcopio item = new ClsCentroAcopio();
		ClsValorizacion clsValorizacion = new ClsValorizacion();
		con = null;
		String sql = this.sgr_sp_list_sgr_centro_acopio;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCentroAcopio.getValorizacion().getValorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setCentro_acopio_id(rs.getInt("CENTRO_ACOPIO_ID"));
					clsValorizacion.setValorizacion_id(rs.getInt("VALORIZACION_ID"));
					item.setValorizacion(clsValorizacion);
					item.setAdm_propia(rs.getInt("ADM_PROPIA"));
					item.setTercerizado_eor(rs.getInt("TERCERIZADO_EOR"));
					item.setTercerizado_asoc_recic(rs.getInt("TERCERIZADO_ASOC_RECIC"));
					item.setMixto(rs.getInt("MIXTO"));
					item.setNumero_recicladores(rs.getInt("NUMERO_RECICLADORES"));
							
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsCentroAcopio clsCentroAcopio) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_centro_acopio;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCentroAcopio.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsCentroAcopio.getAdm_propia());
			cs.setInt(3, clsCentroAcopio.getTercerizado_eor());
			cs.setInt(4, clsCentroAcopio.getTercerizado_asoc_recic());
			cs.setInt(5, clsCentroAcopio.getMixto());
			cs.setInt(6, clsCentroAcopio.getNumero_recicladores());
			cs.setInt(7, clsCentroAcopio.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);
			
			log.info("CentroAcopioDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsCentroAcopio clsCentroAcopio) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_centro_acopio;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCentroAcopio.getCentro_acopio_id());
			cs.setInt(2, clsCentroAcopio.getValorizacion().getValorizacion_id());
			cs.setInt(3, clsCentroAcopio.getAdm_propia());
			cs.setInt(4, clsCentroAcopio.getTercerizado_eor());
			cs.setInt(5, clsCentroAcopio.getTercerizado_asoc_recic());
			cs.setInt(6, clsCentroAcopio.getMixto());
			cs.setInt(7, clsCentroAcopio.getNumero_recicladores());
			cs.setInt(8, clsCentroAcopio.getCodigo_usuario());
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(9);

			log.info("CentroAcopioDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
