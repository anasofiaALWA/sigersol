package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsPropia;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.PropiaDAO;

public class PropiaService {

	private static Logger log = Logger.getLogger(PropiaService.class);
	PropiaDAO propiaDAO = new PropiaDAO();
	
	public ClsResultado obtener(ClsPropia clsPropia){
		ClsResultado clsResultado = null;
		try {
			clsResultado = propiaDAO.obtener(clsPropia);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsPropia clsPropia) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = propiaDAO.insertar(clsPropia);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsPropia clsPropia) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = propiaDAO.actualizar(clsPropia);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
}
