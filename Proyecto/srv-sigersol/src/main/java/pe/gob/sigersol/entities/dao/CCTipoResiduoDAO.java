package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCCTipoResiduo;
import pe.gob.sigersol.entities.ClsResultado;

public class CCTipoResiduoDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(CCTipoResiduoDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_cc_sel_doc_tipoResiduo = "{call SIGERSOLBL.SGR_SP_CC_SEL_DOC_TIPO_RESIDUO(?)}";
	private final String sgr_sp_cc_sel_tasa_tipoResiduo = "{call SIGERSOLBL.SGR_SP_CC_SEL_TIPO_RES_TASA(?,?,?,?)}";

	public ClsResultado listarDOC() {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCCTipoResiduo> lista = new ArrayList<ClsCCTipoResiduo>();
		ClsCCTipoResiduo item = null;
		con = null;
		String sql = this.sgr_sp_cc_sel_doc_tipoResiduo;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);

			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(1);

			while (rs.next()) {
				item = new ClsCCTipoResiduo();
				try {
					item.setTipoResiduoId(rs.getInt("CC_TIPO_RESIDUO_ID"));
				} catch (Exception e) {

				}
				try {
					item.setDescripcion(rs.getString("DESCRIPCION"));
				} catch (Exception e) {

				}
				try {
					item.setValorCondHumeda(rs.getDouble("COND_HUM_DOC"));
				} catch (Exception e) {

				}
				try {
					item.setValorCondSeca(rs.getDouble("COND_SECA_DOC"));
				} catch (Exception e) {

				}

				lista.add(item);
			}
			clsResultado.setExito(true);
			clsResultado.setObjeto(lista);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 listarDOC: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listarDOC: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listarDOC: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listarDOC: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;

	}

	public ClsResultado listarTasaDesc(String ubigeoId, String condicionId) {
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCCTipoResiduo> lista = new ArrayList<ClsCCTipoResiduo>();
		ClsCCTipoResiduo item = null;
		con = null;
		String sql = this.sgr_sp_cc_sel_tasa_tipoResiduo;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setString(1, ubigeoId);
			cs.setString(2, condicionId);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(4);
			
			try {
				String tipoClima = rs.getString(3);
			} catch (Exception e) {
				// TODO: handle exception
			}

			while (rs.next()) {
				item = new ClsCCTipoResiduo();
				try {
					item.setTipoResiduoId(rs.getInt("CC_TIPO_RESIDUO_ID"));
				} catch (Exception e) {

				}
				try {
					item.setDescripcion(rs.getString("DESCRIPCION"));
				} catch (Exception e) {

				}
				try {
					item.setTasaDescomposicion(rs.getDouble("TASA"));
				} catch (Exception e) {

				}
				

				lista.add(item);
			}
			clsResultado.setExito(true);
			clsResultado.setObjeto(lista);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 listarTasaDesc: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 listarTasaDesc: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 listarTasaDesc: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 listarTasaDesc: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;

	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}

}
