package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsPersona;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTrabajadores;

public class TrabajadoresTransferenciaDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(TrabajadoresTransferenciaDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_trabajadores = "{call SIGERSOLBL.SGR_SP_LIST_TRABAJADORES3(?,?)}";
	private final String sgr_sp_ins_trabajadores = "{call SIGERSOLBL.SGR_SP_INS_TRABAJADORES3(?,?,?,?,?,?,?,?)}";
	
	public ClsResultado listarTrabajadores(ClsTrabajadores clsTrabajadores){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsTrabajadores> lista = new ArrayList<ClsTrabajadores>();
		con = null;
		String sql = this.sgr_sp_list_trabajadores;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsTrabajadores.getTransferencia().getTransferencia_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsTrabajadores item = new ClsTrabajadores();
				ClsPersona persona = new ClsPersona();
				
				try {
					persona.setDni(rs.getInt("DNI"));
					persona.setNom_apellidos(rs.getString("NOM_APELLIDOS"));
					persona.setSexo(rs.getString("SEXO"));
					persona.setRango_edad(rs.getString("RANGO_EDAD"));
					item.setPersona(persona);
					item.setCargo(rs.getString("CARGO"));
					item.setCondicion_laboral(rs.getString("CONDICION_LABORAL"));
					item.setHorario_trabajo(rs.getString("HORARIO_TRABAJO"));
					item.setDias_trabajados(rs.getInt("DIAS_TRABAJADOS"));
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsTrabajadores clsTrabajadores) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_trabajadores;
		
		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsTrabajadores.getTransferencia().getTransferencia_id());
			cs.setInt(2, clsTrabajadores.getPersona().getPersona_id());
			cs.setString(3, clsTrabajadores.getCargo());
			cs.setString(4, clsTrabajadores.getCondicion_laboral());
			cs.setString(5, clsTrabajadores.getHorario_trabajo());
			cs.setInt(6, clsTrabajadores.getDias_trabajados());
			cs.setInt(7, clsTrabajadores.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);
				
			log.info("TrabajadoresRecoleccionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
