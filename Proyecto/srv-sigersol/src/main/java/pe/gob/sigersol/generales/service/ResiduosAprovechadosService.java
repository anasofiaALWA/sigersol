package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsResiduosAprovechados;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.ResiduosAprovechadosDAO;

public class ResiduosAprovechadosService {
	
	private static Logger log = Logger.getLogger(ResiduosAprovechadosService.class);
	ResiduosAprovechadosDAO residuosAprovechadosDAO = new ResiduosAprovechadosDAO();
	
	public ClsResultado listarResiduosAprovechados(ClsResiduosAprovechados clsResiduosAprovechados){
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosAprovechadosDAO.listarResiduosAprovechados(clsResiduosAprovechados);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsResiduosAprovechados clsResiduosAprovechados) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosAprovechadosDAO.insertar(clsResiduosAprovechados);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsResiduosAprovechados clsResiduosAprovechados) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = residuosAprovechadosDAO.actualizar(clsResiduosAprovechados);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
