package pe.gob.sigersol.entities;

public class ClsAccionTipo {

	private Integer accion_tipo_id;
	private String nombre;
	
	public Integer getAccion_tipo_id() {
		return accion_tipo_id;
	}
	public void setAccion_tipo_id(Integer accion_tipo_id) {
		this.accion_tipo_id = accion_tipo_id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
