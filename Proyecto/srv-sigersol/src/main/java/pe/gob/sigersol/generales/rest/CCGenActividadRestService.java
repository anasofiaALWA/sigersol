package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCCGenActividad;
import pe.gob.sigersol.entities.ClsCCGenCombustible;
import pe.gob.sigersol.entities.ClsCCGenComposicion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.CCGenActividadService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/ccGenActividad")
@RequestScoped
public class CCGenActividadRestService {
	private static Logger log = Logger.getLogger(CCGenActividadRestService.class.getName());
	private CCGenActividadService ccGenActividadService;

	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		ccGenActividadService = new CCGenActividadService();
		ClsCCGenActividad genActividad = jsonEntradaInsertar(paramJson);
		if (genActividad.getCc_genActividadId() == -1) {
			clsResultado = ccGenActividadService.insertar(genActividad);
		} else {
			clsResultado = ccGenActividadService.actualizar(genActividad);

		}
		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();
	}

	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) throws ParseException {
		RestStatusProcess lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		ccGenActividadService = new CCGenActividadService();
		Integer ccGeneracionId = null;
		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(paramJson);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("Filtro");

			if (jsonObject.get("ccGeneracionId") != null && jsonObject.get("ccGeneracionId") != "") {
				ccGeneracionId = Integer.valueOf(jsonObject.get("ccGeneracionId").toString());
			}

			clsResultado = ccGenActividadService.getGenActividad(ccGeneracionId);
		} catch (Exception e) {
			log.warn("Error al obtener parametros de entrada en /obtener " + e.getMessage());
		}

		lRespuesta.setExito(clsResultado.isExito());
		lRespuesta.setDatosAdicionales(StringUtility.convertToJSON(clsResultado.getObjeto()));
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

		return lRespuesta.toJSon();
	}

	private ClsCCGenActividad jsonEntradaInsertar(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> CCGenActividadRestService :	jsonEntradaInsertar" + jsonEntrada);
		ClsCCGenActividad ccGenActividad = new ClsCCGenActividad();

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("ccGenActividad");

			if (jsonObject.get("ccGeneracionId") != null && jsonObject.get("ccGeneracionId") != "") {
				ccGenActividad.setCc_GeneracionId(Integer.valueOf(jsonObject.get("ccGeneracionId").toString()));
			}
			if (jsonObject.get("id") != null && jsonObject.get("id") != "") {
				ccGenActividad.setCc_genActividadId(Integer.valueOf(jsonObject.get("id").toString()));
			}

			if (jsonObject.get("cantMetano") != null && jsonObject.get("cantMetano") != "") {
				ccGenActividad.setMetano_quemado(Double.valueOf(jsonObject.get("cantMetano").toString()));
			}
			if (jsonObject.get("monitoreo") != null && jsonObject.get("monitoreo") != "") {
				ccGenActividad.setMonitoreo(Integer.valueOf(jsonObject.get("monitoreo").toString()));
			}
			if (jsonObject.get("horasFunc") != null && jsonObject.get("horasFunc") != "") {
				ccGenActividad.setHoras_quemador(Double.valueOf(jsonObject.get("horasFunc").toString()));
			}
			if (jsonObject.get("eficienciaQuemador") != null && jsonObject.get("eficienciaQuemador") != "") {
				ccGenActividad.setEficiencia_quemador(Double.valueOf(jsonObject.get("eficienciaQuemador").toString()));
			}
			if (jsonObject.get("total_gas") != null && jsonObject.get("total_gas") != "") {
				ccGenActividad.setGas_total(Double.valueOf(jsonObject.get("total_gas").toString()));
			}
			if (jsonObject.get("gas_quemado") != null && jsonObject.get("gas_quemado") != "") {
				ccGenActividad.setGas_quemado(Double.valueOf(jsonObject.get("gas_quemado").toString()));
			}
			if (jsonObject.get("gas_generado") != null && jsonObject.get("gas_generado") != "") {
				ccGenActividad.setGas_generacion(Double.valueOf(jsonObject.get("gas_generado").toString()));
			}
			if (jsonObject.get("ptj_metano_lfg") != null && jsonObject.get("ptj_metano_lfg") != "") {
				ccGenActividad.setP_metanoEnGas(Double.valueOf(jsonObject.get("ptj_metano_lfg").toString()));
			}
			if (jsonObject.get("densidad_metano") != null && jsonObject.get("densidad_metano") != "") {
				ccGenActividad.setDensidad_metano(Double.valueOf(jsonObject.get("densidad_metano").toString()));
			}
			if (jsonObject.get("eficienciaCaptura") != null && jsonObject.get("eficienciaCaptura") != "") {
				ccGenActividad.setEficienciaCaptura(Double.valueOf(jsonObject.get("eficienciaCaptura").toString()));
			}

			if (jsonObject.get("sesionId") != null && jsonObject.get("sesionId") != "") {
				ccGenActividad.setSesionId(Integer.valueOf(jsonObject.get("sesionId").toString()));
			}

			org.json.simple.JSONArray jsonListCombustible = (org.json.simple.JSONArray) jsonObject
					.get("listCombustible");
			java.util.Iterator i = jsonListCombustible.iterator();
			List<ClsCCGenCombustible> listCombustible = new ArrayList<ClsCCGenCombustible>();
			while (i.hasNext()) {
				org.json.simple.JSONObject objCombustible = (org.json.simple.JSONObject) i.next();
				ClsCCGenCombustible item = new ClsCCGenCombustible();

				if (objCombustible.get("ccGenCombustibleId") != null
						&& objCombustible.get("ccGenCombustibleId") != "") {
					item.setCcGenCombustibleId(Integer.parseInt(objCombustible.get("ccGenCombustibleId").toString()));
				}
				if (objCombustible.get("cc_tipoCombustibleId") != null
						&& objCombustible.get("cc_tipoCombustibleId") != "") {
					item.setCc_tipoCombustibleId(
							Integer.parseInt(objCombustible.get("cc_tipoCombustibleId").toString()));
				}
				if (objCombustible.get("cantidad") != null && objCombustible.get("cantidad") != "") {
					item.setCantidad(Double.valueOf(objCombustible.get("cantidad").toString()));
				}
				item.setSesionId(ccGenActividad.getSesionId());

				listCombustible.add(item);

			}

			ccGenActividad.setListCombustible(listCombustible);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ccGenActividad;
	}

}
