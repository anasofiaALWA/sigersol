package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCCGeneracion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CCGeneracionDAO;

public class CCGeneracionService {
	private static Logger log = Logger.getLogger(CCGeneracionService.class.getName());

	CCGeneracionDAO dao = new CCGeneracionDAO();
	CCGenComposicionService ccGenComposicionService = new CCGenComposicionService();

	public ClsResultado insertar(ClsCCGeneracion ccGeneracion) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.insertar(ccGeneracion);
			if (clsResultado.isExito()) {
				Integer ccGeneacionId = (Integer) clsResultado.getObjeto();
				clsResultado = ccGenComposicionService.insertar(ccGeneacionId, ccGeneracion.getListComposicion());
				if (clsResultado.isExito()) {
					clsResultado.setObjeto(ccGeneacionId);
				}
			}
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}

		return clsResultado;
	}

	public ClsResultado calcularTool4(String ubigeoId, Integer municipalidadId, Integer ccGeneracionId,
			String condicionId,Integer anioCGRS) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.calcularTool4(ubigeoId, municipalidadId, ccGeneracionId, condicionId, anioCGRS);

		} catch (Exception e) {
			log.info("message calcularTool4 >> service >> " + e.getMessage());
		}

		return clsResultado;
	}
}
