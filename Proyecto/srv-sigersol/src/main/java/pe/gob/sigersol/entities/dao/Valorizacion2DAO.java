package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsValorizacion;

public class Valorizacion2DAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(Valorizacion2DAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_valorizacion = "{call SIGERSOLBL.SGR_SP_LIST_SGR_VALORIZACION2(?,?)}";
	private final String sgr_sp_ins_valorizacion = "{call SIGERSOLBL.SGR_SP_INS_VALORIZACION(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_valorizacion = "{call SIGERSOLBL.SGR_SP_UPD_VALORIZACION1(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsValorizacion clsValorizacion) {
		ClsResultado clsResultado = new ClsResultado();
		ClsValorizacion item = new ClsValorizacion();
		ClsCicloGestionResiduos clsCicloGestionResiduos = new ClsCicloGestionResiduos();
		con = null;
		String sql = this.sgr_sp_list_sgr_valorizacion;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsValorizacion.getCicloGestionResiduos().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				try {
					item.setValorizacion_id(rs.getInt("VALORIZACION_ID"));
					clsCicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCicloGestionResiduos(clsCicloGestionResiduos);
					item.setResiduo_inorganico_otro(rs.getString("RESIDUO_INORGANICO_OTRO"));
					item.setEnero_otro(rs.getInt("ENERO_OTRO"));
					item.setFebrero_otro(rs.getInt("FEBRERO_OTRO"));
					item.setMarzo_otro(rs.getInt("MARZO_OTRO"));
					item.setAbril_otro(rs.getInt("ABRIL_OTRO"));
					item.setMayo_otro(rs.getInt("MAYO_OTRO"));
					item.setJunio_otro(rs.getInt("JUNIO_OTRO"));
					item.setJulio_otro(rs.getInt("JULIO_OTRO"));
					item.setAgosto_otro(rs.getInt("AGOSTO_OTRO"));
					item.setSetiembre_otro(rs.getInt("SETIEMBRE_OTRO"));
					item.setOctubre_otro(rs.getInt("OCTUBRE_OTRO"));
					item.setNoviembre_otro(rs.getInt("NOVIEMBRE_OTRO"));
					item.setDiciembre_otro(rs.getInt("DICIEMBRE_OTRO"));
					item.setCompostaje(rs.getInt("COMPOSTAJE"));
					item.setLombricultura(rs.getInt("LOMBRICULTURA"));
					item.setBiodigestion(rs.getInt("BIODIGESTION"));
					item.setDescrip_otros(rs.getString("DESCRIP_OTROS"));
					item.setOtros_actividades(rs.getInt("OTROS_ACTIVIDADES"));
					item.setDescrip_productos(rs.getString("DESCRIP_PRODUCTOS"));
					item.setCantidad_productos(rs.getInt("CANTIDAD_PRODUCTOS"));
					item.setNumero_ciclos(rs.getInt("NUMERO_CICLOS"));
					item.setCantidad_residuos1(rs.getInt("CANTIDAD_RESIDUOS1"));
					item.setCantidad_residuos2(rs.getInt("CANTIDAD_RESIDUOS2"));
					item.setCantidad_residuos3(rs.getInt("CANTIDAD_RESIDUOS3"));
					item.setCantidad_residuos4(rs.getInt("CANTIDAD_RESIDUOS4"));
					item.setCantidad_residuos5(rs.getInt("CANTIDAD_RESIDUOS5"));
			
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsValorizacion clsValorizacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_valorizacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsValorizacion.getCicloGestionResiduos().getCiclo_grs_id());
			cs.setInt(2, clsValorizacion.getFlag_acopio());
			cs.setInt(3, clsValorizacion.getFlag_acopio2());
			cs.setInt(4, clsValorizacion.getFlag_valorizacion());
			cs.setInt(5, clsValorizacion.getFlag_valorizacion2());
			cs.setString(6, clsValorizacion.getResiduo_inorganico_otro());
			cs.setInt(7, clsValorizacion.getEnero_otro());
			cs.setInt(8, clsValorizacion.getFebrero_otro());
			cs.setInt(9, clsValorizacion.getMarzo_otro());
			cs.setInt(10, clsValorizacion.getAbril_otro());
			cs.setInt(11, clsValorizacion.getMayo_otro());
			cs.setInt(12, clsValorizacion.getJunio_otro());
			cs.setInt(13, clsValorizacion.getJulio_otro());
			cs.setInt(14, clsValorizacion.getAgosto_otro());
			cs.setInt(15, clsValorizacion.getSetiembre_otro());
			cs.setInt(16, clsValorizacion.getOctubre_otro());
			cs.setInt(17, clsValorizacion.getNoviembre_otro());
			cs.setInt(18, clsValorizacion.getDiciembre_otro());
			cs.setInt(19, clsValorizacion.getCompostaje());
			cs.setInt(20, clsValorizacion.getLombricultura());
			cs.setInt(21, clsValorizacion.getBiodigestion());
			cs.setString(22, clsValorizacion.getDescrip_otros());
			cs.setInt(23, clsValorizacion.getOtros_actividades());
			cs.setString(24, clsValorizacion.getDescrip_productos());
			cs.setInt(25, clsValorizacion.getCantidad_productos());
			cs.setInt(26, clsValorizacion.getNumero_ciclos());
			cs.setInt(27, clsValorizacion.getNumero_acopio());
			cs.setInt(28, clsValorizacion.getNumero_valorizacion());
			cs.setInt(29, clsValorizacion.getCodigo_usuario());
			cs.registerOutParameter(30, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(30);
			
			log.info("ValorizacionDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsValorizacion clsValorizacion) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_valorizacion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsValorizacion.getValorizacion_id());
			cs.setString(2, clsValorizacion.getResiduo_inorganico_otro());
			cs.setInt(3, clsValorizacion.getEnero_otro());
			cs.setInt(4, clsValorizacion.getFebrero_otro());
			cs.setInt(5, clsValorizacion.getMarzo_otro());
			cs.setInt(6, clsValorizacion.getAbril_otro());
			cs.setInt(7, clsValorizacion.getMayo_otro());
			cs.setInt(8, clsValorizacion.getJunio_otro());
			cs.setInt(9, clsValorizacion.getJulio_otro());
			cs.setInt(10, clsValorizacion.getAgosto_otro());
			cs.setInt(11, clsValorizacion.getSetiembre_otro());
			cs.setInt(12, clsValorizacion.getOctubre_otro());
			cs.setInt(13, clsValorizacion.getNoviembre_otro());
			cs.setInt(14, clsValorizacion.getDiciembre_otro());
			cs.setInt(15, clsValorizacion.getCompostaje());
			cs.setInt(16, clsValorizacion.getLombricultura());
			cs.setInt(17, clsValorizacion.getBiodigestion());
			cs.setString(18, clsValorizacion.getDescrip_otros());
			cs.setInt(19, clsValorizacion.getOtros_actividades());
			cs.setString(20, clsValorizacion.getDescrip_productos());
			cs.setInt(21, clsValorizacion.getCantidad_productos());
			cs.setInt(22, clsValorizacion.getNumero_ciclos());
			cs.setInt(23, clsValorizacion.getCantidad_residuos1());
			cs.setInt(24, clsValorizacion.getCantidad_residuos2());
			cs.setInt(25, clsValorizacion.getCantidad_residuos3());
			cs.setInt(26, clsValorizacion.getCantidad_residuos4());
			cs.setInt(27, clsValorizacion.getCantidad_residuos5());
			cs.setInt(28, clsValorizacion.getCodigo_usuario());
			cs.registerOutParameter(29, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(29);

			log.info("ValorizacionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
