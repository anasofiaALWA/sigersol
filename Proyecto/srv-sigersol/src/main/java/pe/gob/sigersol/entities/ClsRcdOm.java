package pe.gob.sigersol.entities;

public class ClsRcdOm {

	private Integer rcd_om_id;
	private ClsGeneracion generacion;
	private ClsTipoConstruccion tipo_construccion;
	private Integer cantidad;
	private Integer codigo_usuario;
	
	public Integer getRcd_om_id() {
		return rcd_om_id;
	}
	public void setRcd_om_id(Integer rcd_om_id) {
		this.rcd_om_id = rcd_om_id;
	}
	public ClsGeneracion getGeneracion() {
		return generacion;
	}
	public void setGeneracion(ClsGeneracion generacion) {
		this.generacion = generacion;
	}
	public ClsTipoConstruccion getTipo_construccion() {
		return tipo_construccion;
	}
	public void setTipo_construccion(ClsTipoConstruccion tipo_construccion) {
		this.tipo_construccion = tipo_construccion;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
