package pe.gob.sigersol.generales.rest;


import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsUnicodeParametro;
import pe.gob.sigersol.generales.service.UnicodeParametroService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/unicodeParametroX")
@RequestScoped
public class UnicodeParametroRestService {
	
	private static Logger log = Logger.getLogger(UnicodeParametroRestService.class);
	private ClsUnicodeParametro clsUnicodeParametro;
	UnicodeParametroService unicodeParametroService = new UnicodeParametroService();
	
	
	@POST
	@Path("/obtenerLista")
	@Produces(MediaType.APPLICATION_JSON)
	public String obtenerLista(@FormParam("nombre_tabla") String nombre_tabla,
			@Context HttpServletRequest _request,@Context HttpServletResponse _reponse){
		
		RestStatusProcess lRespuesta = new RestStatusProcess();
		List<ClsUnicodeParametro> lstUnicodeParametro = new ArrayList<ClsUnicodeParametro>();
		
		log.info("** nombre_tabla : " + nombre_tabla);
		
		lstUnicodeParametro = unicodeParametroService.obtener_lista_parametros(nombre_tabla);
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"Parametro\":"+ StringUtility.convertToJSON(lstUnicodeParametro)+"}");
		
		//log.info("Json : " + lRespuesta.toJSon());
		
		return lRespuesta.toJSon();
		
	}
	
	@POST
	@Path("/obtenerListaTablas")
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener_lista_tabla(@Context HttpServletRequest _request,@Context HttpServletResponse _reponse){
		
		String json_nombre_tabla = "";
		Gson gson = new Gson();
		
		try {
			json_nombre_tabla = gson.toJson(unicodeParametroService.obtener_lista_tablas());
			log.info("json_nombre_tabla : " + json_nombre_tabla);
		} catch (Exception e) {
			log.info(" ERROR! 1 >> " + e.getMessage());
		}
		
		return json_nombre_tabla;
		
	}
	
	@POST
	@Path("/insertarTabla")
	@Produces(MediaType.APPLICATION_JSON)
	public String insertar_tabla(@FormParam("paramJson") String paramJson, @Context HttpServletRequest _request, @Context  HttpServletResponse _response){
		RestStatusProcess lRespuesta = null;
	
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
		
			//log.info("paramJson" + paramJson);
			jsonEntradaParametro(paramJson);
			
			clsResultado = unicodeParametroService.insertar_tabla(clsUnicodeParametro);
		
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto())+ "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(false);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		
		return lRespuesta.toJSon();
	}

	@POST
	@Path("/actualizar_tabla")
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar_tabla(@FormParam("tabla_anterior") String tabla_anterior,@FormParam("tabla_nueva") String tabla_nueva , @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		// Aplica para inserción y actualización
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			log.info("tabla_anterior: " + tabla_anterior);
			log.info("tabla_nueva: " + tabla_nueva);
			
			clsResultado = unicodeParametroService.actualizar_tabla(tabla_anterior,tabla_nueva);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
			return lRespuesta.toJSon();
		} catch (Exception e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}
	
	@POST
	@Path("/actualizarParametro")
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar_parametro(@FormParam("paramJson") String paramJson, @Context HttpServletRequest _request, @Context  HttpServletResponse _response){
		RestStatusProcess lRespuesta = null;
	
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			//log.info("paramJson" + paramJson);
			jsonEntradaParametro(paramJson);
			clsResultado = unicodeParametroService.actualizar_parametro(clsUnicodeParametro);
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto())+ "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(false);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		
		return lRespuesta.toJSon();
	}
	
	
	
	private void jsonEntradaParametro(String jsonEntrada) throws ParseException {
		log.info("----------------------------------------------------------------------------------");
		log.info("** InicioScriptJson INGRESO>> CursoRestService : jsonEntradaParametro>>");
		clsUnicodeParametro = new ClsUnicodeParametro();
		//log.info("** InicioScriptJson INGRESO>> jsonReclamo : " + jsonEntrada);
		JSONObject jsonObjectPrincipal;
		JSONObject jsonObject;
		JSONParser jsonParser = new JSONParser();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObject = (JSONObject) jsonObjectPrincipal.get("TablaIngr");
				
			    clsUnicodeParametro.setUnicode_parametro_id(Integer.parseInt(jsonObject.get("id_parametro").toString()));
				clsUnicodeParametro.setNombre_tabla(jsonObject.get("nombre_tabla").toString());
				clsUnicodeParametro.setCodigo_parametro(jsonObject.get("codigo_parametro").toString());
				clsUnicodeParametro.setValor_parametro(jsonObject.get("valor_parametro").toString());
				clsUnicodeParametro.setCodigo_estado(Integer.parseInt(jsonObject.get("codigo_estado").toString()));
				clsUnicodeParametro.setEditable(Integer.parseInt(jsonObject.get("editable").toString()));

		//	log.info("clsUnicodeParametro: " + clsUnicodeParametro.toJson());

		} catch (ParseException e) {
			e.printStackTrace();
		}

		//log.info("** FinScriptJson SALIDA>> jsonEntradaParametro : " + clsUnicodeParametro.toJson());
		log.info("----------------------------------------------------------------------------------");
	}	

}
