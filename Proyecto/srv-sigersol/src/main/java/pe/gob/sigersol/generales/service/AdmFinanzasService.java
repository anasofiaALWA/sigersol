package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsAdmFinanzas;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.AdmFinanzasDAO;

public class AdmFinanzasService {

	private static Logger log = Logger.getLogger(AdmFinanzasService.class);
	AdmFinanzasDAO admFinanzasDAO = new AdmFinanzasDAO();
	
	
	public ClsResultado obtener(ClsAdmFinanzas clsAdmFinanzas){
		ClsResultado clsResultado = null;
		try {
			clsResultado = admFinanzasDAO.obtener(clsAdmFinanzas);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsAdmFinanzas clsAdmFinanzas) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = admFinanzasDAO.insertar(clsAdmFinanzas);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsAdmFinanzas clsAdmFinanzas) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = admFinanzasDAO.actualizar(clsAdmFinanzas);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
