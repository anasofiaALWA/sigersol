package pe.gob.sigersol.entities;

public class ClsSeguro {
	
	private Integer persona_militar_id;
	private Integer seguro_id;
	private Integer seguro_tipo_id;
	private String seguro;
	private String seguro_particular;
	private String usuario_creacion;
	public Integer getPersona_militar_id() {
		return persona_militar_id;
	}
	public void setPersona_militar_id(Integer persona_militar_id) {
		this.persona_militar_id = persona_militar_id;
	}
	public Integer getSeguro_id() {
		return seguro_id;
	}
	public void setSeguro_id(Integer seguro_id) {
		this.seguro_id = seguro_id;
	}
	public Integer getSeguro_tipo_id() {
		return seguro_tipo_id;
	}
	public void setSeguro_tipo_id(Integer seguro_tipo_id) {
		this.seguro_tipo_id = seguro_tipo_id;
	}
	public String getSeguro() {
		return seguro;
	}
	public void setSeguro(String seguro) {
		this.seguro = seguro;
	}
	public String getSeguro_particular() {
		return seguro_particular;
	}
	public void setSeguro_particular(String seguro_particular) {
		this.seguro_particular = seguro_particular;
	}
	public String getUsuario_creacion() {
		return usuario_creacion;
	}
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}
	
	
}
