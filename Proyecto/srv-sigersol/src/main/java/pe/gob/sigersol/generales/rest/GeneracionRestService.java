package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsGeneracion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.generales.service.GeneracionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

/*
 * Clase GeneracionRestService
 * 	
 * Es el servicio Rest que permite la conexión con la vista de Generación, permite recuperar,
 * insertar o actualizar los datos ingresados 
 * 
 * Autor: Fredy Arévalo Delgado - Alwa S.A.C
 * Version: 1.0
 * Fecha: 11/12/2017
 * 
 * */

@Path("/generacion")
@RequestScoped
public class GeneracionRestService {
	
	private static Logger log = Logger.getLogger(GeneracionRestService.class.getName());
	private ClsGeneracion clsGeneracion;
	private ClsCicloGestionResiduos clsCicloGestionResiduos;
	private GeneracionService generacionService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		generacionService = new GeneracionService();
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			
			log.info("**obtener generacion**");
			
			jsonEntradaGeneracion(paramJson);
			
			clsResultado = generacionService.obtener(clsGeneracion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"generacion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		generacionService = new GeneracionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaGeneracion(paramJson);
			clsResultado = generacionService.insertar(clsGeneracion);
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		generacionService = new GeneracionService();
		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();

		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaGeneracion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = generacionService.actualizar(clsGeneracion);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Generación  
	 * 
	 * */
	private void jsonEntradaGeneracion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> SolicitudRestService :	 jsonEntradaGeneracion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectGeneracion;
		JSONParser jsonParser = new JSONParser();
		
		clsGeneracion = new ClsGeneracion();
		clsCicloGestionResiduos = new ClsCicloGestionResiduos();

		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectGeneracion = (JSONObject) jsonObjectPrincipal.get("Generacion");
			
			if (jsonObjectGeneracion.get("generacion_id") != null && jsonObjectGeneracion.get("generacion_id") != "") {
				clsGeneracion.setGeneracion_id(Integer.parseInt(jsonObjectGeneracion.get("generacion_id").toString()));
			}
			
			if (jsonObjectGeneracion.get("ciclo_grs_id") != null && jsonObjectGeneracion.get("ciclo_grs_id") != "") {
				clsCicloGestionResiduos.setCiclo_grs_id(Integer.parseInt(jsonObjectGeneracion.get("ciclo_grs_id").toString()));
			} 
			else
				clsCicloGestionResiduos.setCiclo_grs_id(-1);
			
			clsGeneracion.setCicloGestionResiduos(clsCicloGestionResiduos);
			
			if (jsonObjectGeneracion.get("flag_generacion") != null && jsonObjectGeneracion.get("flag_generacion") != "") {
				clsGeneracion.setFlag_generacion(Integer.parseInt(jsonObjectGeneracion.get("flag_generacion").toString()));
			}
			
			if (jsonObjectGeneracion.get("densidad") != null && jsonObjectGeneracion.get("densidad") != "") {
				clsGeneracion.setDensidad(Integer.parseInt(jsonObjectGeneracion.get("densidad").toString()));
			}
			
			if (jsonObjectGeneracion.get("gpc_diario") != null && jsonObjectGeneracion.get("gpc_diario") != "") {
				clsGeneracion.setGpc_diario(Double.valueOf(jsonObjectGeneracion.get("gpc_diario").toString()));
			}
			
			if (jsonObjectGeneracion.get("generacion_total_rsd") != null && jsonObjectGeneracion.get("generacion_total_rsd") != "") {
				clsGeneracion.setGeneracion_total_rsd(Double.valueOf(jsonObjectGeneracion.get("generacion_total_rsd").toString()));
			}
			
			if (jsonObjectGeneracion.get("generacion_total_rsnd") != null && jsonObjectGeneracion.get("generacion_total_rsnd") != "") {
				clsGeneracion.setGeneracion_total_rsnd(Double.valueOf(jsonObjectGeneracion.get("generacion_total_rsnd").toString()));
			}
			
			if (jsonObjectGeneracion.get("generacion_total_rcd_om") != null && jsonObjectGeneracion.get("generacion_total_rcd_om") != "") {
				clsGeneracion.setGeneracion_total_rcd_om(Double.valueOf(jsonObjectGeneracion.get("generacion_total_rcd_om").toString()));
			}
			
			if (jsonObjectGeneracion.get("cod_usuario") != null && jsonObjectGeneracion.get("cod_usuario") != "") {
				clsGeneracion.setCodigo_usuario(Integer.parseInt(jsonObjectGeneracion.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
