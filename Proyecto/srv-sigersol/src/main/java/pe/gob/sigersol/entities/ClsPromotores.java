package pe.gob.sigersol.entities;

public class ClsPromotores {

	private Integer promotores_id;
	private ClsEducacionAmbiental educacion_ambiental;
	private ClsTipoPromotor tipo_promotor;
	private Integer promotor_hombres;
	private Integer promotor_mujeres;
	private Integer codigo_usuario;
	
	public Integer getPromotores_id() {
		return promotores_id;
	}
	public void setPromotores_id(Integer promotores_id) {
		this.promotores_id = promotores_id;
	}
	public ClsEducacionAmbiental getEducacion_ambiental() {
		return educacion_ambiental;
	}
	public void setEducacion_ambiental(ClsEducacionAmbiental educacion_ambiental) {
		this.educacion_ambiental = educacion_ambiental;
	}
	public ClsTipoPromotor getTipo_promotor() {
		return tipo_promotor;
	}
	public void setTipo_promotor(ClsTipoPromotor tipo_promotor) {
		this.tipo_promotor = tipo_promotor;
	}
	public Integer getPromotor_hombres() {
		return promotor_hombres;
	}
	public void setPromotor_hombres(Integer promotor_hombres) {
		this.promotor_hombres = promotor_hombres;
	}
	public Integer getPromotor_mujeres() {
		return promotor_mujeres;
	}
	public void setPromotor_mujeres(Integer promotor_mujeres) {
		this.promotor_mujeres = promotor_mujeres;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
