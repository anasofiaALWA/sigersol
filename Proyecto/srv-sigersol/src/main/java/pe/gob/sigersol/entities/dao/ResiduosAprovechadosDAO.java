package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsResiduosAprovechados;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoResiduoSolido;
import pe.gob.sigersol.entities.ClsValorizacion;

public class ResiduosAprovechadosDAO extends GenericoDAO {

	private static Logger log = Logger.getLogger(ResiduosAprovechadosDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_res_aprovechados = "{call SIGERSOLBL.SGR_SP_INS_RES_APROVECHADOS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_res_aprovechados = "{call SIGERSOLBL.SGR_SP_UPD_RES_APROVECHADOS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	private final String sgr_sp_list_res_aprovechados = "{call SIGERSOLBL.SGR_SP_LIST_RES_APROVECHADOS(?,?)}";  
	
	public ClsResultado listarResiduosAprovechados(ClsResiduosAprovechados clsResiduosAprovechados){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsResiduosAprovechados> lista = new ArrayList<ClsResiduosAprovechados>();
		con = null;
		String sql = this.sgr_sp_list_res_aprovechados;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosAprovechados.getValorizacion().getValorizacion_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsResiduosAprovechados item = new ClsResiduosAprovechados();
				ClsTipoResiduoSolido tipoResiduoSolido = new ClsTipoResiduoSolido();
				try {
					tipoResiduoSolido.setTipo_residuo_id(rs.getInt("TIPO_RESIDUO_ID"));
					tipoResiduoSolido.setNombre(rs.getString("NOMBRE_TIPO_RESIDUO"));
					tipoResiduoSolido.setInformacion(rs.getString("INFORMACION"));
					item.setTipo_residuo_solido(tipoResiduoSolido);
					if(clsResiduosAprovechados.getValorizacion().getValorizacion_id() == -1){
						item.setEnero(null);
						item.setFebrero(null);
						item.setMarzo(null);
						item.setAbril(null);
						item.setMayo(null);
						item.setJunio(null);
						item.setJulio(null);
						item.setAgosto(null);
						item.setSetiembre(null);
						item.setOctubre(null);
						item.setNoviembre(null);
						item.setDiciembre(null);
					} else{
						item.setEnero(rs.getInt("ENERO"));
						item.setFebrero(rs.getInt("FEBRERO"));
						item.setMarzo(rs.getInt("MARZO"));
						item.setAbril(rs.getInt("ABRIL"));
						item.setMayo(rs.getInt("MAYO"));
						item.setJunio(rs.getInt("JUNIO"));
						item.setJulio(rs.getInt("JULIO"));
						item.setAgosto(rs.getInt("AGOSTO"));
						item.setSetiembre(rs.getInt("SETIEMBRE"));
						item.setOctubre(rs.getInt("OCTUBRE"));
						item.setNoviembre(rs.getInt("NOVIEMBRE"));
						item.setDiciembre(rs.getInt("DICIEMBRE"));
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsResiduosAprovechados clsResiduosAprovechados) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_res_aprovechados;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosAprovechados.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsResiduosAprovechados.getTipo_residuo_solido().getTipo_residuo_id());
			cs.setInt(3, clsResiduosAprovechados.getEnero());
			cs.setInt(4, clsResiduosAprovechados.getFebrero());
			cs.setInt(5, clsResiduosAprovechados.getMarzo());
			cs.setInt(6, clsResiduosAprovechados.getAbril());
			cs.setInt(7, clsResiduosAprovechados.getMayo());
			cs.setInt(8, clsResiduosAprovechados.getJunio());
			cs.setInt(9, clsResiduosAprovechados.getJulio());
			cs.setInt(10, clsResiduosAprovechados.getAgosto());
			cs.setInt(11, clsResiduosAprovechados.getSetiembre());
			cs.setInt(12, clsResiduosAprovechados.getOctubre());
			cs.setInt(13, clsResiduosAprovechados.getNoviembre());
			cs.setInt(14, clsResiduosAprovechados.getDiciembre());
			cs.setInt(15, clsResiduosAprovechados.getCodigo_usuario());
			cs.registerOutParameter(16, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(16);
			
			log.info("ResiduosAprovechadosDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsResiduosAprovechados clsResiduosAprovechados) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_res_aprovechados;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsResiduosAprovechados.getValorizacion().getValorizacion_id());
			cs.setInt(2, clsResiduosAprovechados.getTipo_residuo_solido().getTipo_residuo_id());
			cs.setInt(3, clsResiduosAprovechados.getEnero());
			cs.setInt(4, clsResiduosAprovechados.getFebrero());
			cs.setInt(5, clsResiduosAprovechados.getMarzo());
			cs.setInt(6, clsResiduosAprovechados.getAbril());
			cs.setInt(7, clsResiduosAprovechados.getMayo());
			cs.setInt(8, clsResiduosAprovechados.getJunio());
			cs.setInt(9, clsResiduosAprovechados.getJulio());
			cs.setInt(10, clsResiduosAprovechados.getAgosto());
			cs.setInt(11, clsResiduosAprovechados.getSetiembre());
			cs.setInt(12, clsResiduosAprovechados.getOctubre());
			cs.setInt(13, clsResiduosAprovechados.getNoviembre());
			cs.setInt(14, clsResiduosAprovechados.getDiciembre());
			cs.setInt(15, clsResiduosAprovechados.getCodigo_usuario());
			cs.registerOutParameter(16, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(16);

			log.info("ResiduosAprovechadosDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
