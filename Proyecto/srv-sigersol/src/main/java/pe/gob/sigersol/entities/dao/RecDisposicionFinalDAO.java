package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCicloGestionResiduos;
import pe.gob.sigersol.entities.ClsRecDisposicionFinal;
import pe.gob.sigersol.entities.ClsResultado;

public class RecDisposicionFinalDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(RecDisposicionFinalDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_list_sgr_rec_disp = "{call SIGERSOLBL.SGR_SP_LIST_SGR_REC_DISP(?,?,?,?)}";
	private final String sgr_sp_ins_rec_disposicion = "{call SIGERSOLBL.SGR_SP_INS_REC_DISPOSICION(?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_rec_disp = "{call SIGERSOLBL.SGR_SP_UPD_REC_DISP(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public ClsResultado obtener(ClsRecDisposicionFinal clsRecDisposicionFinal) {
		ClsResultado clsResultado = new ClsResultado();
		ClsRecDisposicionFinal item = new ClsRecDisposicionFinal();
		ClsCicloGestionResiduos cicloGestionResiduos = new ClsCicloGestionResiduos();
		
		String[] s; 
		
		con = null;
		String sql = this.sgr_sp_list_sgr_rec_disp;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecDisposicionFinal.getCiclo_grs_id().getCiclo_grs_id());
			cs.registerOutParameter(2, OracleTypes.DOUBLE);
			cs.registerOutParameter(3, OracleTypes.DOUBLE);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(4);

			try {
				Double total_rec_almacenamiento = cs.getDouble(2);
				item.setTotal_rec_almacenamiento(total_rec_almacenamiento);
			} catch (Exception e) {
				
			}

			try {
				Double total_rec_barrido = cs.getDouble(3);
				item.setTotal_rec_barrido(total_rec_barrido);

			} catch (Exception e) {
				
			}
			
			while (rs.next()) {
				try {
					item.setRec_disposicion_final_id(rs.getInt("REC_DISPOSICION_FINAL_ID"));
					cicloGestionResiduos.setCiclo_grs_id(rs.getInt("CICLO_GRS_ID"));
					item.setCiclo_grs_id(cicloGestionResiduos);
					item.setAdm_serv_id(rs.getString("ADM_SERV_ID"));
					item.setCosto_anual(rs.getInt("COSTO_ANUAL"));
					item.setFrecuencia(rs.getString("FRECUENCIA"));
					
					s = item.getFrecuencia().split("/");
					item.setFr1(Integer.parseInt(s[0]));
					item.setFr2(Integer.parseInt(s[1]));
					
					item.setCantidad_residuos(rs.getInt("CANTIDAD_RESIDUOS"));
					item.setOtros_descripcion(rs.getString("OTROS_DESCRIPCION"));
					item.setOtros_antig_promedio(rs.getInt("OTROS_ANTIG_PROMEDIO"));
					item.setOtros_capacidad(rs.getInt("OTROS_CAPACIDAD"));
					item.setOtros_recoleccion_anual(rs.getInt("OTROS_RECOLECCION_ANUAL"));
					item.setOtros_numero_unidades(rs.getInt("OTROS_NUMERO_UNIDADES"));
					item.setCodigo_usuario(rs.getInt("COD_USUARIO_CREACION"));
				} catch (Exception e) {

				}
			}
			clsResultado.setObjeto(item);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;
	}

	public ClsResultado insertar(ClsRecDisposicionFinal clsRecDisposicionFinal) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_rec_disposicion;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecDisposicionFinal.getCiclo_grs_id().getCiclo_grs_id());
			cs.setString(2, clsRecDisposicionFinal.getAdm_serv_id());
			cs.setInt(3, clsRecDisposicionFinal.getCosto_anual());
			cs.setString(4, clsRecDisposicionFinal.getFrecuencia());
			cs.setInt(5, clsRecDisposicionFinal.getCantidad_residuos());
			cs.setString(6, clsRecDisposicionFinal.getOtros_descripcion());
			cs.setInt(7, clsRecDisposicionFinal.getOtros_antig_promedio());
			cs.setInt(8, clsRecDisposicionFinal.getOtros_capacidad());
			cs.setInt(9, clsRecDisposicionFinal.getOtros_recoleccion_anual());
			cs.setInt(10, clsRecDisposicionFinal.getOtros_numero_unidades());
			cs.setInt(11, clsRecDisposicionFinal.getCodigo_usuario());
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(12);
			
			log.info("RecDisposicionFinalDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsRecDisposicionFinal clsRecDisposicionFinal) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_rec_disp;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsRecDisposicionFinal.getRec_disposicion_final_id());
			cs.setInt(2, clsRecDisposicionFinal.getCiclo_grs_id().getCiclo_grs_id());
			cs.setString(3, clsRecDisposicionFinal.getAdm_serv_id());
			cs.setInt(4, clsRecDisposicionFinal.getCosto_anual());
			cs.setString(5, clsRecDisposicionFinal.getFrecuencia());
			cs.setInt(6, clsRecDisposicionFinal.getCantidad_residuos());
			cs.setString(7, clsRecDisposicionFinal.getOtros_descripcion());
			cs.setInt(8, clsRecDisposicionFinal.getOtros_antig_promedio());
			cs.setInt(9, clsRecDisposicionFinal.getOtros_capacidad());
			cs.setInt(10, clsRecDisposicionFinal.getOtros_recoleccion_anual());
			cs.setInt(11, clsRecDisposicionFinal.getOtros_numero_unidades());
			cs.setInt(12, clsRecDisposicionFinal.getCodigo_usuario());
			cs.registerOutParameter(13, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(13);

			log.info("RecoleccionDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
