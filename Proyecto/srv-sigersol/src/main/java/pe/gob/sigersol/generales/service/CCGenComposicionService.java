package pe.gob.sigersol.generales.service;

import java.util.List;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsCCGenComposicion;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.CCGenComposicionDAO;

public class CCGenComposicionService {

	private static Logger log = Logger.getLogger(CCGenComposicionService.class);
	CCGenComposicionDAO dao = new CCGenComposicionDAO();

	public ClsResultado obtenerGenComposicion(Integer disposicionFinalId) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = dao.getGenComposicion(disposicionFinalId);
		} catch (Exception e) {
			log.info("message listCC_GenComposicion >> service >> " + e.getMessage());
		}

		return clsResultado;

	}

	public ClsResultado insertar(Integer ccGeneracionId, List<ClsCCGenComposicion> ListGenComposicion) {
		ClsResultado clsResultado = null;
		try {
			for (ClsCCGenComposicion clsCCGenComposicion : ListGenComposicion) {
				clsResultado = dao.insertar(ccGeneracionId, clsCCGenComposicion);
			}

		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}

		return clsResultado;

	}

}
