package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsOferta;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.OfertaDAO;

public class OfertaService {

	private static Logger log = Logger.getLogger(OfertaService.class);
	OfertaDAO ofertaDAO = new OfertaDAO();
	
	public ClsResultado obtener(ClsOferta clsOferta){
		ClsResultado clsResultado = null;
		try {
			clsResultado = ofertaDAO.obtener(clsOferta);
		} catch (Exception e) {
			log.info("message obtener >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado listarOferta(ClsOferta clsOferta){
		ClsResultado clsResultado = null;
		try {
			clsResultado = ofertaDAO.listarOferta(clsOferta);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsOferta clsOferta) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = ofertaDAO.insertar(clsOferta);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsOferta clsOferta) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = ofertaDAO.actualizar(clsOferta);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
