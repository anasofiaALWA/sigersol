package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCantPapeleras;
import pe.gob.sigersol.entities.ClsEstado;
import pe.gob.sigersol.entities.ClsResultado;

public class CantPapelerasDAO extends GenericoDAO {
	
	private static Logger log = Logger.getLogger(CantVehiculosDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;
	
	private final String sgr_sp_ins_cant_papeleras = "{call SIGERSOLBL.SGR_SP_INS_CANT_PAPELERAS(?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_cant_papeleras = "{call SIGERSOLBL.SGR_SP_UPD_CANT_PAPELERAS(?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_list_cant_papeleras = "{call SIGERSOLBL.SGR_SP_LIST_CANT_PAPELERAS(?,?)}";  
	
	public ClsResultado listarCantPapeleras(ClsCantPapeleras clsCantPapeleras){
		
		ClsResultado clsResultado = new ClsResultado();
		List<ClsCantPapeleras> lista = new ArrayList<ClsCantPapeleras>();
		con = null;
		String sql = this.sgr_sp_list_cant_papeleras;
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCantPapeleras.getAlmacenamiento().getAlmacenamiento_id());
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ClsCantPapeleras item = new ClsCantPapeleras();
				ClsEstado estado = new ClsEstado();
				
				try {
					estado.setEstado_id(rs.getInt("ESTADO_ID"));
					estado.setNombre_estado(rs.getString("NOMBRE_ESTADO"));
					item.setEstado(estado);
					if(clsCantPapeleras.getAlmacenamiento().getAlmacenamiento_id() != -1){
						item.setMercados(rs.getInt("MERCADOS"));
						item.setParques(rs.getInt("PARQUES"));
						item.setVias(rs.getInt("VIAS"));
						item.setOtros(rs.getInt("OTROS"));
					}
					else{
						item.setMercados(null);
						item.setParques(null);
						item.setVias(null);
						item.setOtros(null);
					}
					
				} catch (Exception e) {

				}
				
				lista.add(item);
			}
			clsResultado.setObjeto(lista);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 obtener_filtro: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 obtener_filtro: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 obtener_filtro: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsCantPapeleras clsCantPapeleras) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_ins_cant_papeleras;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCantPapeleras.getAlmacenamiento().getAlmacenamiento_id());
			cs.setInt(2, clsCantPapeleras.getEstado().getEstado_id());
			cs.setInt(3, clsCantPapeleras.getMercados());
			cs.setInt(4, clsCantPapeleras.getParques());
			cs.setInt(5, clsCantPapeleras.getVias());
			cs.setInt(6, clsCantPapeleras.getOtros());
			cs.setInt(7, clsCantPapeleras.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);
			
			log.info("CantPapelerasDAO >> insertar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Inserción exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}

	public ClsResultado actualizar(ClsCantPapeleras clsCantPapeleras) {
		Integer resultado;
		ClsResultado clsResultado = new ClsResultado();
		String sql = this.sgr_sp_upd_cant_papeleras;

		try {
			con = getConnection();
		} catch (Exception e) {
			setMensajeError(clsResultado, e);
			return null;
		}

		try {
			cs = con.prepareCall(sql);
			cs.setInt(1, clsCantPapeleras.getAlmacenamiento().getAlmacenamiento_id());
			cs.setInt(2, clsCantPapeleras.getEstado().getEstado_id());
			cs.setInt(3, clsCantPapeleras.getMercados());
			cs.setInt(4, clsCantPapeleras.getParques());
			cs.setInt(5, clsCantPapeleras.getVias());
			cs.setInt(6, clsCantPapeleras.getOtros());
			cs.setInt(7, clsCantPapeleras.getCodigo_usuario());
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.executeUpdate();
			resultado = cs.getInt(8);

			log.info("CantPapelerasDAO >> actualizar() >> resultado :" + resultado);

			clsResultado.setObjeto(resultado);
			clsResultado.setExito(true);
			clsResultado.setMensaje("Actualización exitosa");

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				cerrarConexiones();
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}

		return clsResultado;
	}
	
	private void cerrarConexiones() throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (cs != null) {
			cs.close();
		}
		if (con != null) {
			con.close();
		}
	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}
}
