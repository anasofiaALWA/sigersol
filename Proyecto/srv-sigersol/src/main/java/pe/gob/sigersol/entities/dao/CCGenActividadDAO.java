package pe.gob.sigersol.entities.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import oracle.jdbc.internal.OracleTypes;
import pe.gob.sigersol.core.dao.GenericoDAO;
import pe.gob.sigersol.entities.ClsCCGenActividad;
import pe.gob.sigersol.entities.ClsCCGenCombustible;
import pe.gob.sigersol.entities.ClsResultado;

public class CCGenActividadDAO extends GenericoDAO {
	private static Logger log = Logger.getLogger(CCGenActividadDAO.class.getName());
	private Connection con = null;
	private ResultSet rs = null;
	private CallableStatement cs = null;

	private final String sgr_sp_ins_cc_gen_actividad = "{call SIGERSOLBL.SGR_SP_INS_CC_GEN_ACTIVIDAD(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_upd_cc_gen_actividad = "{call SIGERSOLBL.SGR_SP_UPD_CC_GEN_ACTIVIDAD(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	private final String sgr_sp_sel_cc_gen_actividad = "{call SIGERSOLBL.SGR_SP_CC_SEL_GEN_ACTIVIDAD(?,?)}";

	public ClsResultado insertar(ClsCCGenActividad ccGenActividad) {
		ClsResultado clsResultado = new ClsResultado();
		Integer ccActividadId;
		con = null;
		String sql = this.sgr_sp_ins_cc_gen_actividad;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, ccGenActividad.getCc_GeneracionId());
			cs.setInt(2, ccGenActividad.getMonitoreo());
			cs.setDouble(3, ccGenActividad.getMetano_quemado());
			try {
				cs.setDouble(4, ccGenActividad.getHoras_quemador());
			} catch (Exception e) {
				cs.setNull(4, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(5, ccGenActividad.getEficiencia_quemador());
			} catch (Exception e) {
				cs.setNull(5, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(6, ccGenActividad.getGas_total());
			} catch (Exception e) {
				cs.setNull(6, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(7, ccGenActividad.getGas_quemado());
			} catch (Exception e) {
				cs.setNull(7, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(8, ccGenActividad.getGas_generacion());
			} catch (Exception e) {
				cs.setNull(8, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(9, ccGenActividad.getP_metanoEnGas());
			} catch (Exception e) {
				cs.setNull(9, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(10, ccGenActividad.getDensidad_metano());
			} catch (Exception e) {
				cs.setNull(10, OracleTypes.DOUBLE);
			}

			cs.setDouble(11, ccGenActividad.getEficienciaCaptura());
			cs.setInt(12, ccGenActividad.getSesionId());
			cs.registerOutParameter(13, OracleTypes.INTEGER);

			cs.executeUpdate();
			ccActividadId = cs.getInt(13);

			log.info("CCGenActividadDAO >> insertar() >> resultado :" + ccActividadId);

			if (ccActividadId > 0) {
				clsResultado.setExito(true);
				clsResultado.setMensaje("Inserción exitosa");
			} else {
				clsResultado.setExito(false);
				clsResultado.setMensaje("Inserción fallida");
			}
			clsResultado.setObjeto(ccActividadId);

		} catch (SQLException ex) {
			log.error("ERROR! 1 insertar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 insertar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 insertar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;

	}
	
	public ClsResultado actualizar(ClsCCGenActividad ccGenActividad) {
		ClsResultado clsResultado = new ClsResultado();
		Integer ccActividadId;
		con = null;
		String sql = this.sgr_sp_upd_cc_gen_actividad;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);
			cs.setInt(1, ccGenActividad.getCc_genActividadId());
			cs.setInt(2, ccGenActividad.getMonitoreo());
			cs.setDouble(3, ccGenActividad.getMetano_quemado());
			try {
				cs.setDouble(4, ccGenActividad.getHoras_quemador());
			} catch (Exception e) {
				cs.setNull(4, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(5, ccGenActividad.getEficiencia_quemador());
			} catch (Exception e) {
				cs.setNull(5, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(6, ccGenActividad.getGas_total());
			} catch (Exception e) {
				cs.setNull(6, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(7, ccGenActividad.getGas_quemado());
			} catch (Exception e) {
				cs.setNull(7, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(8, ccGenActividad.getGas_generacion());
			} catch (Exception e) {
				cs.setNull(8, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(9, ccGenActividad.getP_metanoEnGas());
			} catch (Exception e) {
				cs.setNull(9, OracleTypes.DOUBLE);
			}
			try {
				cs.setDouble(10, ccGenActividad.getDensidad_metano());
			} catch (Exception e) {
				cs.setNull(10, OracleTypes.DOUBLE);
			}
			
			cs.setDouble(11, ccGenActividad.getEficienciaCaptura());			
			cs.setInt(12, ccGenActividad.getSesionId());
			cs.registerOutParameter(13, OracleTypes.INTEGER);

			cs.executeUpdate();
			ccActividadId = cs.getInt(13);

			log.info("CCGenActividadDAO >> actualizar() >> resultado :" + ccActividadId);

			if (ccActividadId > 0) {
				clsResultado.setExito(true);
				clsResultado.setMensaje("Actualización exitosa");
			} else {
				clsResultado.setExito(false);
				clsResultado.setMensaje("Actualización fallida");
			}
			clsResultado.setObjeto(ccActividadId);

		} catch (SQLException ex) {
			log.error("ERROR! 1 actualizar: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 actualizar: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 actualizar: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;

	}

	public ClsResultado getGenActividad(Integer ccGeneracionId) {
		ClsResultado clsResultado = new ClsResultado();
		ClsCCGenActividad ccGenActividad = null;		
		con = null;
		String sql = this.sgr_sp_sel_cc_gen_actividad;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			cs = con.prepareCall(sql);

			cs.setInt(1, ccGeneracionId);
			cs.registerOutParameter(2, OracleTypes.CURSOR);			
			cs.execute();
			rs = (ResultSet) cs.getObject(2);

			while (rs.next()) {
				ccGenActividad = new ClsCCGenActividad();
				ccGenActividad.setCc_genActividadId(rs.getInt("CC_GEN_ACTIVIDAD_ID"));
				ccGenActividad.setCc_GeneracionId(rs.getInt("CC_GENERACION_ID"));
				ccGenActividad.setMonitoreo(rs.getInt("REALIZA_MONITOREO"));
				ccGenActividad.setMetano_quemado(rs.getDouble("METANO_QUEMADO"));
				ccGenActividad.setHoras_quemador(rs.getDouble("HORAS_QUEMADOR"));
				ccGenActividad.setEficiencia_quemador(rs.getDouble("EFICIENCIA_QUEMADOR"));
				ccGenActividad.setGas_total(rs.getDouble("GAS_TOTAL"));
				ccGenActividad.setGas_quemado(rs.getDouble("GAS_QUEMADO"));
				ccGenActividad.setGas_generacion(rs.getDouble("GAS_GENERACION"));
				ccGenActividad.setP_metanoEnGas(rs.getDouble("P_METANO_EN_GAS"));
				ccGenActividad.setDensidad_metano(rs.getDouble("DENSIDAD_METANO"));
				ccGenActividad.setEficienciaCaptura(rs.getDouble("EFICIENCIA_CAPTURA_METANO"));
			}

		
			if (ccGenActividad != null) {
				clsResultado.setExito(true);
			} 
			clsResultado.setObjeto(ccGenActividad);
			clsResultado.setMensaje("Obtención exitosa");
		} catch (SQLException ex) {
			log.error("ERROR! 1 getGenActividad: " + new StringBuilder(ex.getMessage()).append(" ").append(sql));
			log.error("ERROR! 2 getGenActividad: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} catch (Exception ex) {
			log.error("ERROR! 3 getGenActividad: " + ex.getMessage() + ex);
			setMensajeError(clsResultado, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception exception) {
				log.error("ERROR! 4 getGenActividad: " + exception.getMessage() + exception);
				setMensajeError(clsResultado, exception);
			}
		}
		return clsResultado;

	}

	private void setMensajeError(ClsResultado clsResultado, Exception ex) {
		clsResultado.setObjeto(null);
		clsResultado.setExito(false);
		clsResultado.setMensaje(ex.getMessage());
	}

}
