package pe.gob.sigersol.entities;

public class ClsAreasDegradadasAbandonadas {

	private Integer areas_degradadas_abandonadas_id;
	private ClsDisposicionFinal disposicion_final;
	private Integer flag_areas_abandonadas;
	private Integer numero_areas;
	private Integer anio_inicio;
	private Integer anio_cierre;
	private Integer area_total;
	private String responsable; 
	private Integer altura;
	private Integer cantidad;
	private Integer metodo_usado;
	private Integer altura_napa;
	private Integer codigo_usuario;
	
	public Integer getAreas_degradadas_abandonadas_id() {
		return areas_degradadas_abandonadas_id;
	}
	public void setAreas_degradadas_abandonadas_id(Integer areas_degradadas_abandonadas_id) {
		this.areas_degradadas_abandonadas_id = areas_degradadas_abandonadas_id;
	}
	public ClsDisposicionFinal getDisposicion_final() {
		return disposicion_final;
	}
	public void setDisposicion_final(ClsDisposicionFinal disposicion_final) {
		this.disposicion_final = disposicion_final;
	}
	public Integer getFlag_areas_abandonadas() {
		return flag_areas_abandonadas;
	}
	public void setFlag_areas_abandonadas(Integer flag_areas_abandonadas) {
		this.flag_areas_abandonadas = flag_areas_abandonadas;
	}
	public Integer getNumero_areas() {
		return numero_areas;
	}
	public void setNumero_areas(Integer numero_areas) {
		this.numero_areas = numero_areas;
	}
	public Integer getAnio_inicio() {
		return anio_inicio;
	}
	public void setAnio_inicio(Integer anio_inicio) {
		this.anio_inicio = anio_inicio;
	}
	public Integer getAnio_cierre() {
		return anio_cierre;
	}
	public void setAnio_cierre(Integer anio_cierre) {
		this.anio_cierre = anio_cierre;
	}
	public Integer getArea_total() {
		return area_total;
	}
	public void setArea_total(Integer area_total) {
		this.area_total = area_total;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public Integer getAltura() {
		return altura;
	}
	public void setAltura(Integer altura) {
		this.altura = altura;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getMetodo_usado() {
		return metodo_usado;
	}
	public void setMetodo_usado(Integer metodo_usado) {
		this.metodo_usado = metodo_usado;
	}
	public Integer getAltura_napa() {
		return altura_napa;
	}
	public void setAltura_napa(Integer altura_napa) {
		this.altura_napa = altura_napa;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
}
