package pe.gob.sigersol.entities;

public class ClsPropia {

	private Integer propia_id;
	private ClsTransferencia transferencia;
	private String num_iga;
	private String infraest;
	private String autoridad_resolucion;
	private String fecha_emision;
	private String licencia;
	private String inicio_permiso;
	private String culminacion_permiso;
	private Integer codigo_usuario;
	
	public Integer getPropia_id() {
		return propia_id;
	}
	public void setPropia_id(Integer propia_id) {
		this.propia_id = propia_id;
	}
	public ClsTransferencia getTransferencia() {
		return transferencia;
	}
	public void setTransferencia(ClsTransferencia transferencia) {
		this.transferencia = transferencia;
	}
	public String getNum_iga() {
		return num_iga;
	}
	public void setNum_iga(String num_iga) {
		this.num_iga = num_iga;
	}
	public String getInfraest() {
		return infraest;
	}
	public void setInfraest(String infraest) {
		this.infraest = infraest;
	}
	public String getAutoridad_resolucion() {
		return autoridad_resolucion;
	}
	public void setAutoridad_resolucion(String autoridad_resolucion) {
		this.autoridad_resolucion = autoridad_resolucion;
	}
	public String getFecha_emision() {
		return fecha_emision;
	}
	public void setFecha_emision(String fecha_emision) {
		this.fecha_emision = fecha_emision;
	}
	public String getLicencia() {
		return licencia;
	}
	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}
	public String getInicio_permiso() {
		return inicio_permiso;
	}
	public void setInicio_permiso(String inicio_permiso) {
		this.inicio_permiso = inicio_permiso;
	}
	public String getCulminacion_permiso() {
		return culminacion_permiso;
	}
	public void setCulminacion_permiso(String culminacion_permiso) {
		this.culminacion_permiso = culminacion_permiso;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
}
