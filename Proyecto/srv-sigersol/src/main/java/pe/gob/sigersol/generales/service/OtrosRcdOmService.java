package pe.gob.sigersol.generales.service;

import org.jboss.logging.Logger;

import pe.gob.sigersol.entities.ClsOtrosRcdOm;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.dao.OtrosRcdOmDAO;

public class OtrosRcdOmService {

	private static Logger log = Logger.getLogger(OtrosRcdOmService.class);
	OtrosRcdOmDAO otrosRcdOmDAO = new OtrosRcdOmDAO();
	
	public ClsResultado listarOtrosRcdOm(ClsOtrosRcdOm clsOtrosRcdOm){
		ClsResultado clsResultado = null;
		try {
			clsResultado = otrosRcdOmDAO.listarOtrosRcdOm(clsOtrosRcdOm);
		} catch (Exception e) {
			log.info("message listar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado insertar(ClsOtrosRcdOm clsOtrosRcdOm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = otrosRcdOmDAO.insertar(clsOtrosRcdOm);
		} catch (Exception e) {
			log.info("message insertar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
	
	public ClsResultado actualizar(ClsOtrosRcdOm clsOtrosRcdOm) {
		ClsResultado clsResultado = null;
		try {
			clsResultado = otrosRcdOmDAO.actualizar(clsOtrosRcdOm);
		} catch (Exception e) {
			log.info("message actualizar >> service >> " + e.getMessage());
		}
		return clsResultado;
	}
}
