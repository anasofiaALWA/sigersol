package pe.gob.sigersol.generales.rest;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsPropia;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTransferencia;
import pe.gob.sigersol.generales.service.PropiaService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/propiaX")
@RequestScoped
public class PropiaRestService {
	
	private static Logger log = Logger.getLogger(PropiaRestService.class.getName());
	private ClsPropia clsPropia;
	private ClsTransferencia clsTransferencia;
	private PropiaService propiaService;

	/*
	 * Método que permite recuperar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/obtener")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String obtener(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			propiaService = new PropiaService();
			
			jsonEntradaTransferencia(paramJson);
			
			log.info("**obtener propia**");
			
			clsResultado = propiaService.obtener(clsPropia);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"propia\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {
	
		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		propiaService = new PropiaService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();

			jsonEntradaTransferencia(paramJson);
			clsResultado = propiaService.insertar(clsPropia);	
			idUsuario = (Integer) clsResultado.getObjeto();
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		propiaService = new PropiaService();
		
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaTransferencia(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		clsResultado = propiaService.actualizar(clsPropia);

		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de Transferencia  
	 * 
	 * */
	private void jsonEntradaTransferencia(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> TransferenciaRestService :jsonEntradaTransferencia" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectPropia;
		JSONParser jsonParser = new JSONParser();
		
		clsPropia = new ClsPropia();
		clsTransferencia = new ClsTransferencia();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectPropia = (JSONObject) jsonObjectPrincipal.get("Propia");
			
			if (jsonObjectPropia.get("propia_id") != null && jsonObjectPropia.get("propia_id") != "") {
				clsPropia.setPropia_id(Integer.parseInt(jsonObjectPropia.get("propia_id").toString()));
			}
			
			if (jsonObjectPropia.get("transferencia_id") != null && jsonObjectPropia.get("transferencia_id") != "") {
				clsTransferencia.setTransferencia_id(Integer.parseInt(jsonObjectPropia.get("transferencia_id").toString()));
			}
			else
				clsTransferencia.setTransferencia_id(-1);
			
			clsPropia.setTransferencia(clsTransferencia);
			
			
			if (jsonObjectPropia.get("num_iga") != null && jsonObjectPropia.get("num_iga") != "") {
				clsPropia.setNum_iga(jsonObjectPropia.get("num_iga").toString());
			}
			
			if (jsonObjectPropia.get("infraest") != null && jsonObjectPropia.get("infraest") != "") {
				clsPropia.setInfraest(jsonObjectPropia.get("infraest").toString());
			}
				
			if (jsonObjectPropia.get("autoridad_resolucion") != null && jsonObjectPropia.get("autoridad_resolucion") != "") {
				clsPropia.setAutoridad_resolucion(jsonObjectPropia.get("autoridad_resolucion").toString());
			}
			
			if (jsonObjectPropia.get("fecha_emision") != null && jsonObjectPropia.get("fecha_emision") != "") {
				clsPropia.setFecha_emision(jsonObjectPropia.get("fecha_emision").toString());
			}
			
			if (jsonObjectPropia.get("licencia") != null && jsonObjectPropia.get("licencia") != "") {
				clsPropia.setLicencia(jsonObjectPropia.get("licencia").toString());
			}
			
			if (jsonObjectPropia.get("inicio_permiso") != null && jsonObjectPropia.get("inicio_permiso") != "") {
				clsPropia.setInicio_permiso(jsonObjectPropia.get("inicio_permiso").toString());
			}
			
			if (jsonObjectPropia.get("culminacion_permiso") != null && jsonObjectPropia.get("culminacion_permiso") != "") {
				clsPropia.setCulminacion_permiso(jsonObjectPropia.get("culminacion_permiso").toString());
			}
			
			if (jsonObjectPropia.get("cod_usuario") != null && jsonObjectPropia.get("cod_usuario") != "") {
				clsPropia.setCodigo_usuario(Integer.parseInt(jsonObjectPropia.get("cod_usuario").toString()));
			}

			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
