package pe.gob.sigersol.entities;

public class ClsCCGenCombustible {
	
	private Integer ccGenCombustibleId;	
	private Integer cc_tipoCombustibleId;
	private String cc_tipoCombustible_desc;
	private Double cantidad;
	private Integer sesionId;
	
	
	public Integer getCcGenCombustibleId() {
		return ccGenCombustibleId;
	}
	public void setCcGenCombustibleId(Integer ccGenCombustibleId) {
		this.ccGenCombustibleId = ccGenCombustibleId;
	}
	
	public Integer getCc_tipoCombustibleId() {
		return cc_tipoCombustibleId;
	}
	public void setCc_tipoCombustibleId(Integer cc_tipoCombustibleId) {
		this.cc_tipoCombustibleId = cc_tipoCombustibleId;
	}
	public String getCc_tipoCombustible_desc() {
		return cc_tipoCombustible_desc;
	}
	public void setCc_tipoCombustible_desc(String cc_tipoCombustible_desc) {
		this.cc_tipoCombustible_desc = cc_tipoCombustible_desc;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getSesionId() {
		return sesionId;
	}
	public void setSesionId(Integer sesionId) {
		this.sesionId = sesionId;
	}
	
}
