package pe.gob.sigersol.generales.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pe.gob.sigersol.entities.ClsAccion;
import pe.gob.sigersol.entities.ClsEducacionAmbiental;
import pe.gob.sigersol.entities.ClsResultado;
import pe.gob.sigersol.entities.ClsTipoActividad;
import pe.gob.sigersol.generales.service.AccionService;
import pe.gob.sigersol.sesion.rest.RestStatusProcess;
import pe.gob.sigersol.util.StringUtility;

@Path("/accionX")
@RequestScoped
public class AccionRestService {

	private static Logger log = Logger.getLogger(AccionRestService.class.getName());
	private Integer accion = null;
	private ClsAccion clsAccion;
	private ClsEducacionAmbiental clsEducacionAmbiental;
	private ClsTipoActividad clsTipoActividad;
	private AccionService accionService;
	private List<ClsAccion> listaAccion;

	/*
	 * Método que permite listar los datos que habían sido ingresados anteriormente
	 * 
	 * */
	@POST
	@Path("/listarAccion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String listarAccion(String paramJson, @Context HttpServletRequest _request, @Context HttpServletResponse _response) {
		RestStatusProcess lRespuesta = null;
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			accionService = new AccionService();
			
			jsonEntradaListaAccion(paramJson);	
			log.info("**Obtener accion**");	
			clsResultado = accionService.listarAccion(clsAccion);

			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"LstAccion\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite insertar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String registrar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;
		String pc = _request.getRemoteHost();
		Integer idUsuario = 0;
		accionService = new AccionService();
		
		try {
			lRespuesta = new RestStatusProcess();
			ClsResultado clsResultado = new ClsResultado();
			jsonEntradaAccion(paramJson);

			for (ClsAccion clsAccion : listaAccion) {
				
				if(clsAccion.getTipo_actividad().getFlag_actividad() == 1){
					clsResultado = accionService.insertar(clsAccion);
				}
			}
			
			lRespuesta.setExito(true);
			lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
			lRespuesta.setMensajeUsuario(clsResultado.getMensaje());

			return lRespuesta.toJSon();
		} catch (ParseException e) {
			lRespuesta = new RestStatusProcess();
			lRespuesta.setExito(true);
			lRespuesta.setMensajeUsuario(e.getMessage());
		}
		return lRespuesta.toJSon();
	}

	/*
	 * Método que permite actualizar los datos ingresados en la vista
	 * 
	 * */
	@POST
	@Path("/actualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String actualizar(String paramJson, @Context HttpServletRequest _request,
			@Context HttpServletResponse _response) {

		RestStatusProcess lRespuesta = null;

		lRespuesta = new RestStatusProcess();
		ClsResultado clsResultado = new ClsResultado();
		accionService = new AccionService();
		
		log.info("paramJson: " + paramJson);
		try {
			jsonEntradaAccion(paramJson);
		} catch (Exception e) {
			// TODO: handle exception
		}

		for (ClsAccion clsAccion : listaAccion) {
			
			if(clsAccion.getTipo_actividad().getFlag_actividad() == 1){
				clsResultado = accionService.actualizar(clsAccion);
			}
		}
		
		lRespuesta.setExito(true);
		lRespuesta.setDatosAdicionales("{\"id\":" + StringUtility.convertToJSON(clsResultado.getObjeto()) + "}");
		lRespuesta.setMensajeUsuario(clsResultado.getMensaje());
		return lRespuesta.toJSon();
	}
	
	/*
	 * Método que permite separar el JSON de entrada y guardarlo en la Entidad de CombustibleVehiculos
	 * 
	 * */
	private void jsonEntradaAccion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AccionRestService : jsonEntradaAccion" + jsonEntrada);

		JSONObject jsonObjectTipoActividad;
		
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(jsonEntrada);		
		listaAccion = new ArrayList();
		
		Iterator i = jsonObject.iterator();
		
		while (i.hasNext()) {
			
			clsAccion = new ClsAccion();
			clsEducacionAmbiental = new ClsEducacionAmbiental();
			clsTipoActividad = new ClsTipoActividad();
			
			JSONObject innerObject = (JSONObject) i.next();
			
			if (innerObject.get("educacion_ambiental_id") != null && innerObject.get("educacion_ambiental_id").toString() != "") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(innerObject.get("educacion_ambiental_id").toString()));
			}
			else {
				clsEducacionAmbiental.setEducacion_ambiental_id(0);
			}
			
			clsAccion.setEducacion_ambiental(clsEducacionAmbiental);
			
			
			if (innerObject.get("tipo_actividad") != null && innerObject.get("tipo_actividad").toString() != "") {
				jsonObjectTipoActividad  = (JSONObject) innerObject.get("tipo_actividad");
				clsTipoActividad.setTipo_actividad_id(Integer.parseInt(jsonObjectTipoActividad.get("tipo_actividad_id").toString()));
				clsTipoActividad.setFlag_actividad(Integer.parseInt(jsonObjectTipoActividad.get("flag_actividad").toString()));
			}
			clsAccion.setTipo_actividad(clsTipoActividad);
			
			if (innerObject.get("tipo") != null && innerObject.get("tipo").toString() != "") {
				clsAccion.setTipo(Integer.parseInt(innerObject.get("tipo").toString()));
			} else{
				clsAccion.setTipo(0);
			}
			
			if (innerObject.get("participantes_hombres") != null && innerObject.get("participantes_hombres").toString() != "") {
				clsAccion.setParticipantes_hombres(Integer.parseInt(innerObject.get("participantes_hombres").toString()));
			} else{
				clsAccion.setParticipantes_hombres(0);
			}
			
			if (innerObject.get("participantes_mujeres") != null && innerObject.get("participantes_mujeres").toString() != "") {
				clsAccion.setParticipantes_mujeres(Integer.parseInt(innerObject.get("participantes_mujeres").toString()));
			} else{
				clsAccion.setParticipantes_mujeres(0);
			}
			
			if (innerObject.get("total_mujer_hombre") != null && innerObject.get("total_mujer_hombre").toString() != "") {
				clsAccion.setTotal_mujer_hombre(Integer.parseInt(innerObject.get("total_mujer_hombre").toString()));
			} else{
				clsAccion.setTotal_mujer_hombre(0);
			}
			
			if (innerObject.get("cod_usuario") != null && innerObject.get("cod_usuario").toString() != "") {
				clsAccion.setCodigo_usuario(Integer.parseInt(innerObject.get("cod_usuario").toString()));
			}
			
			listaAccion.add(clsAccion);	
		}
	
		log.info("----------------------------------------------------------------------------------");

	}
	
	/*
	 * Método que permite separar el JSON de entrada y obtener el id para obtener la Lista  
	 * 
	 * */
	private void jsonEntradaListaAccion(String jsonEntrada) throws ParseException {
		log.info("InicioScriptJson INGRESO>> AccionRestService : jsonEntradaListaAccion" + jsonEntrada);

		JSONObject jsonObjectPrincipal;
		JSONObject jsonObjectAccion;
		JSONParser jsonParser = new JSONParser();
		
		clsAccion = new ClsAccion();
		clsEducacionAmbiental = new ClsEducacionAmbiental();
		clsTipoActividad = new ClsTipoActividad();
		
		try {
			jsonObjectPrincipal = (JSONObject) jsonParser.parse(jsonEntrada);
			jsonObjectAccion = (JSONObject) jsonObjectPrincipal.get("Accion");
			
			if (jsonObjectAccion.get("educacion_ambiental_id") != null && jsonObjectAccion.get("educacion_ambiental_id") != "" &&
					jsonObjectAccion.get("educacion_ambiental_id") != " ") {
				clsEducacionAmbiental.setEducacion_ambiental_id(Integer.parseInt(jsonObjectAccion.get("educacion_ambiental_id").toString()));
			}
			else
				clsEducacionAmbiental.setEducacion_ambiental_id(-1);
			
			clsAccion.setEducacion_ambiental(clsEducacionAmbiental);
			
			log.info("----------------------------------------------------------------------------------");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
