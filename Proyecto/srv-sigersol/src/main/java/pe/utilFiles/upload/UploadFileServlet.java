package pe.utilFiles.upload;

// Import required java libraries
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import pe.gob.sigersol.entities.ClsUnicodeParametro;
import pe.gob.sigersol.entities.dao.UnicodeParametroDAO;
import pe.gob.sigersol.generales.service.UnicodeParametroService;

@WebServlet("/UploadFileServlet")
public class UploadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private long MX_FILE_SIZE = 500;
	private long MX_REQUEST_SIZE = 500;
	private static Logger log = Logger.getLogger(UploadFileServlet.class.getName());

	// upload settings
	private int MEMORY_THRESHOLD = 1024 * 1024 * 450; // 3MB
	private long MAX_FILE_SIZE = 1024 * 1024 * MX_FILE_SIZE; // 40MB
	private long MAX_REQUEST_SIZE = 1024 * 1024 * MX_REQUEST_SIZE; // 50MB

	/**
	 * Upon receiving file upload submission, parses the request to read upload
	 * data and saves the file on disk.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// checks if the request actually contains upload file

		this.MX_FILE_SIZE = 500;
		// this.obtenerMaxFileSizeDesdeUnicode();
		this.MX_REQUEST_SIZE = 500;
		// this.obtenerMaxRequestSizeDesdeUnicode();

		log.info("UploadFileServlet  MX_FILE_SIZE >> " + MX_FILE_SIZE);
		log.info("UploadFileServlet  MX_REQUEST_SIZE >> " + MX_REQUEST_SIZE);

		MAX_FILE_SIZE = 1024 * 1024 * MX_FILE_SIZE;
		MAX_REQUEST_SIZE = 1024 * 1024 * MX_REQUEST_SIZE;

		log.info("UploadFileServlet  MAX_FILE_SIZE>> " + MAX_FILE_SIZE);
		log.info("UploadFileServlet  MAX_REQUEST_SIZE>> " + MAX_REQUEST_SIZE);

		// Obtener desde Parametros
		UnicodeParametroService parametro = new UnicodeParametroService();
		String Directorio = parametro.obtener_parametros("UploadFile", "rutaFileServer");
		// "C:\\Home\\AnaSofia\\upload";

		log.info("Directorio >> " + Directorio);

		/*
		 * File folder = new File(Directorio);
		 * 
		 * if (!folder.isDirectory()) { folder.mkdirs(); log.info(
		 * "Si no Existe el directorio ahora si!! <<<<<<<<<"); } else {
		 * log.info("Existe el directorio <<<<<<<<<"); }
		 */

		if (!ServletFileUpload.isMultipartContent(request)) {
			// if not, we stop here
			PrintWriter writer = response.getWriter();
			writer.println("Error: Form must has enctype=multipart/form-data.");
			writer.flush();
			return;
		}
		String nombreArchivoCorto = "";
		long tamanoArchivo = 0;
		String nombreArchivoTemp = "";
		String filePath = "";
		String estadoRespuesta = "1";
		String respuesta = "";

		// configures upload settings
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// sets memory threshold - beyond which files are stored in disk
		factory.setSizeThreshold(MEMORY_THRESHOLD);
		// sets temporary location to store files
		factory.setRepository(new File(Directorio));

		ServletFileUpload upload = new ServletFileUpload(factory);

		// sets maximum size of upload file
		upload.setFileSizeMax(MAX_FILE_SIZE);

		// sets maximum size of request (include file + form data)
		upload.setSizeMax(MAX_REQUEST_SIZE);

		// constructs the directory path to store upload file
		// this path is relative to application's directory
		String uploadPath = Directorio;

		// creates the directory if it does not exist
		File uploadDir = new File(uploadPath);
		if (!uploadDir.exists()) {
			log.info("no existe ruta");
			uploadDir.mkdirs();
			log.info("se debe crear la nueva ruta");
		}

		try {
			// parses the request's content to extract file data
			@SuppressWarnings("unchecked")
			List<FileItem> formItems = upload.parseRequest(request);

			if (formItems != null && formItems.size() > 0) {
				// iterates over form's fields
				try {
					for (FileItem item : formItems) {
						// processes only fields that are not form fields
						if (!item.isFormField() && item.getSize() > 0) {

							String fileName = new File(item.getName()).getName();
							String fieldName = item.getFieldName();

							log.info("fieldName:" + fieldName);

							log.info("fileName: " + fileName + " >> " + item.getSize());
							fileName = fileName.replace("\\", "\\\\");
							log.info("fileName: " + fileName);
							String nombresDir[] = fileName.split("\\\\");
							nombreArchivoCorto = nombresDir[nombresDir.length - 1];
							tamanoArchivo = item.getSize();

							String formato = "yyyy-MM-dd-HHmmss";
							String strDate = "";
							try {
								DateFormat dateFormat = new SimpleDateFormat(formato);
								strDate = dateFormat.format(new Date());

							} catch (Exception e) {

							}
							// nombreArchivoFisico
							nombreArchivoTemp = strDate + "_" + nombreArchivoCorto;
							log.info("nombre archivo > " + nombreArchivoTemp);

							log.info("UploadFileServlet>> ultimo digito de la ruta >>>>> "
									+ Directorio.substring(Directorio.length() - 1));
							String ultimo_digito = Directorio.substring(Directorio.length() - 1);

							String ambienteSO = "";
							String carpetaArchivos = "";

							UnicodeParametroService unicode = new UnicodeParametroService();

							try {
								ambienteSO = unicode.obtener_parametros("Propiedades", "sistemaOperativo");
								// "Windows";
								carpetaArchivos = unicode.obtener_parametros("Propiedades", "carpetaArchivos");
								// "C:";

							} catch (Exception e) {
								log.warning("No se puede obtener el ambienteSO, carpetaArchivos.");
							}

							log.info("UploadFileServlet>> ambienteSO: " + ambienteSO + " carpetaArchivos: "
									+ carpetaArchivos);

							/** Espacio en disco en Windows */
							if (ambienteSO.compareTo("Windows") == 0) {
								if (!ultimo_digito.contains("/") || !ultimo_digito.contains("\\")) {
									Directorio = Directorio + "/";
								}

								filePath = Directorio + nombreArchivoTemp;
								File storeFile = new File(filePath);

								File FileValidatorWindows = new File(carpetaArchivos);
								long freeSpaceWindows = FileValidatorWindows.getFreeSpace();
								if (freeSpaceWindows < tamanoArchivo) {
									respuesta = fieldName + "<**>" + "0" + fieldName + "<**>" + "0";
								} else {
									item.write(storeFile);
									respuesta = respuesta + "<**>" + fieldName + "<**>" + nombreArchivoTemp + "<**>"
											+ fieldName + "<**>" + nombreArchivoCorto;
								}
							}

							/** Espacio en disco en Linux */
							if (ambienteSO.compareTo("Linux") == 0) {
								if (!ultimo_digito.contains("/") || !ultimo_digito.contains("\\")) {
									Directorio = Directorio + "/";
								}

								filePath = Directorio + nombreArchivoTemp;
								File storeFile = new File(filePath);
								File FileValidator = new File(carpetaArchivos);
								log.info("FileValidator >> " + FileValidator);
								long freeSpace = FileValidator.getFreeSpace();
								log.info("tamanoArchivo >> " + tamanoArchivo);
								log.info("freeSpace > root > " + freeSpace);
								if (freeSpace < tamanoArchivo) {
									respuesta = fieldName + "<**>" + "0" + fieldName + "<**>" + "0";
								} else {
									item.write(storeFile);
									respuesta = respuesta + "<**>" + fieldName + "<**>" + nombreArchivoTemp + fieldName
											+ "<**>" + nombreArchivoCorto;
								}
							}

							// saves the file on disk
						}
					}
				} catch (Exception e) {
					estadoRespuesta = "0";
					e.printStackTrace();
				}

				log.info("respuesta: " + respuesta);

				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/plain");
				// response.getWriter().write(respuesta);
			}
		} catch (Exception ex) {
			request.setAttribute("message", "There was an error: " + ex.getMessage());
		}

		response.getWriter().write(respuesta);

	}

	private Integer obtenerMaxFileSizeDesdeUnicode() {
		Integer retorno = 500;

		UnicodeParametroDAO dao = new UnicodeParametroDAO();
		List<ClsUnicodeParametro> lista = new ArrayList<ClsUnicodeParametro>();

		try {
			lista = dao.obtener_lista_parametros("UploadFile");
		} catch (Exception e) {
			log.info("ERROR! UploadFileServlet obtenerMaxFileSizeDesdeUnicode>> " + e);
		}

		if (lista.isEmpty()) {
			retorno = 0;
		} else {

			for (ClsUnicodeParametro item : lista) {
				log.info("UploadFileServlet obtenerMaxFileSizeDesdeUnicode>> " + item.getCodigo_parametro());
				if (item.getCodigo_parametro().contains("maxFileSize")) {
					retorno = Integer.parseInt(item.getValor_parametro());
					break;
				}
			}
		}

		return retorno;
	}

	private Integer obtenerMaxRequestSizeDesdeUnicode() {
		Integer retorno = 500;

		UnicodeParametroDAO dao = new UnicodeParametroDAO();
		List<ClsUnicodeParametro> lista = new ArrayList<ClsUnicodeParametro>();

		try {
			lista = dao.obtener_lista_parametros("UploadFile");
		} catch (Exception e) {
			log.info("ERROR! UploadFileServlet obtenerMaxRequestSizeDesdeUnicode>> " + e);
		}

		if (lista.isEmpty()) {
			retorno = 0;
		} else {

			for (ClsUnicodeParametro item : lista) {
				log.info("UploadFileServlet  obtenerMaxRequestSizeDesdeUnicode>> " + item.getCodigo_parametro());
				if (item.getCodigo_parametro().contains("maxRequestSize")) {
					retorno = Integer.parseInt(item.getValor_parametro());
					break;
				}
			}
		}

		return retorno;
	}

}