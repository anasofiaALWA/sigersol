package pe.jaspersoft.jrx.export;

public abstract interface IDisplayWidthProviderFactory
{
  public abstract IDisplayWidthProvider createDisplayWidthProvider();
}
