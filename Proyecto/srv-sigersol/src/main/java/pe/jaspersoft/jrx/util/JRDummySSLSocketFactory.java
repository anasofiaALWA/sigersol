package pe.jaspersoft.jrx.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public class JRDummySSLSocketFactory
  extends SSLSocketFactory
{
  private SSLSocketFactory factory;
  
  public JRDummySSLSocketFactory()
  {
    try
    {
      SSLContext sslcontext = null;
      try
      {
        sslcontext = SSLContext.getInstance("TLS");
      }
      catch (NoSuchAlgorithmException ex)
      {
        boolean found_a = false;
        sslcontext = SSLContext.getInstance("TLS");
      }
      sslcontext.init(null, new TrustManager[] { new JRDummyTrustManager() }, new SecureRandom());
      
      this.factory = sslcontext.getSocketFactory();
    }
    catch (Exception ex) {}
  }
  
  public static SocketFactory getDefault()
  {
    return new JRDummySSLSocketFactory();
  }
  
  public Socket createSocket(Socket socket, String s, int i, boolean flag)
    throws IOException
  {
    return this.factory.createSocket(socket, s, i, flag);
  }
  
  public Socket createSocket(InetAddress inaddr, int i, InetAddress inaddr1, int j)
    throws IOException
  {
    return this.factory.createSocket(inaddr, i, inaddr1, j);
  }
  
  public Socket createSocket(InetAddress inaddr, int i)
    throws IOException
  {
    return this.factory.createSocket(inaddr, i);
  }
  
  public Socket createSocket(String s, int i, InetAddress inaddr, int j)
    throws IOException
  {
    return this.factory.createSocket(s, i, inaddr, j);
  }
  
  public Socket createSocket(String s, int i)
    throws IOException
  {
    try
    {
      if (this.factory == null)
      {
        SSLContext sslcontext = null;
        
        sslcontext = SSLContext.getInstance("SSL");
        sslcontext.init(null, new TrustManager[] { new JRDummyTrustManager() }, new SecureRandom());
        
        this.factory = sslcontext.getSocketFactory();
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    Socket sock = this.factory.createSocket(s, i);
    
    return sock;
  }
  
  public String[] getDefaultCipherSuites()
  {
    return this.factory.getSupportedCipherSuites();
  }
  
  public String[] getSupportedCipherSuites()
  {
    return this.factory.getSupportedCipherSuites();
  }
}
