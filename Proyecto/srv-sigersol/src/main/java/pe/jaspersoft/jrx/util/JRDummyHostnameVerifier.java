package pe.jaspersoft.jrx.util;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class JRDummyHostnameVerifier
  implements HostnameVerifier
{
  public boolean verify(String urlHostName, SSLSession session)
  {
    return true;
  }
}
