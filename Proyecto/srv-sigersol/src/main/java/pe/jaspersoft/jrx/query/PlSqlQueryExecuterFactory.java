package pe.jaspersoft.jrx.query;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataset;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.query.JRQueryExecuter;
import net.sf.jasperreports.engine.query.JRQueryExecuterFactory;

public class PlSqlQueryExecuterFactory
  implements JRQueryExecuterFactory
{
  public static final String PROPERTY_JDBC_FETCH_SIZE = "net.sf.jasperreports.jdbc.fetch.size";
  public static final String PARAMETER_ORACLE_REF_CURSOR = "ORACLE_REF_CURSOR";
  private static final Object[] ORACLE_BUILT_IN_PARAMETERS = { "ORACLE_REF_CURSOR", ResultSet.class };
  public static final String QUERY_LANGUAGE_PLSQL = "plsql";
  private static final String[] queryParameterClassNames = { Object.class.getName(), Boolean.class.getName(), Byte.class.getName(), Double.class.getName(), Float.class.getName(), Integer.class.getName(), Long.class.getName(), Short.class.getName(), BigDecimal.class.getName(), String.class.getName(), Date.class.getName(), Timestamp.class.getName(), Time.class.getName(), ResultSet.class.getName() };
  
  static
  {
    Arrays.sort(queryParameterClassNames);
  }
  
  public JRQueryExecuter createQueryExecuter(JRDataset dataset, Map parameters)
    throws JRException
  {
    return new PlSqlQueryExecuter(dataset, parameters);
  }
  
  public Object[] getBuiltinParameters()
  {
    return ORACLE_BUILT_IN_PARAMETERS;
  }
  
  public boolean supportsQueryParameterType(String className)
  {
    return Arrays.binarySearch(queryParameterClassNames, className) >= 0;
  }
}
