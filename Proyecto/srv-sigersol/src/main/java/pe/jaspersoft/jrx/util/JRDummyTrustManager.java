package pe.jaspersoft.jrx.util;

import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

public class JRDummyTrustManager
  implements X509TrustManager
{
  public void checkClientTrusted(X509Certificate[] cert, String s) {}
  
  public void checkServerTrusted(X509Certificate[] cert, String s) {}
  
  public X509Certificate[] getAcceptedIssuers()
  {
    return new X509Certificate[0];
  }
}
