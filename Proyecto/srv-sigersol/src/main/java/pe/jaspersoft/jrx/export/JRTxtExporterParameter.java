package pe.jaspersoft.jrx.export;

import net.sf.jasperreports.engine.JRExporterParameter;

public class JRTxtExporterParameter
  extends JRExporterParameter
{
  protected JRTxtExporterParameter(String name)
  {
    super(name);
  }
  
  public static final JRTxtExporterParameter PAGE_ROWS = new JRTxtExporterParameter("Rows per page");
  public static final JRTxtExporterParameter PAGE_COLUMNS = new JRTxtExporterParameter("Columns per page");
  public static final JRTxtExporterParameter ADD_FORM_FEED = new JRTxtExporterParameter("Add FORM FEED");
  public static final JRTxtExporterParameter BIDI_PREFIX = new JRTxtExporterParameter("String used as BIDI prefix");
  public static final JRTxtExporterParameter DISPLAY_WIDTH_PROVIDER_FACTORY = new JRTxtExporterParameter("Display Width Provider Factory");
}
