package pe.jaspersoft.jrx.export;

public abstract interface IDisplayWidthProvider
{
  public abstract int getDisplayWidth(String paramString);
}
