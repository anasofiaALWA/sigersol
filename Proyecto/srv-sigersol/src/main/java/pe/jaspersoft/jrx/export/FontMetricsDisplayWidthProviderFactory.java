package pe.jaspersoft.jrx.export;

public class FontMetricsDisplayWidthProviderFactory
  implements IDisplayWidthProviderFactory
{
  public IDisplayWidthProvider createDisplayWidthProvider()
  {
    return new FontMetricsDisplayWidthProvider(null);
  }
}
