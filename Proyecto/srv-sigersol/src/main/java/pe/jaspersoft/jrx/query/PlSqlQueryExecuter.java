package pe.jaspersoft.jrx.query;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRDataset;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.fill.JRFillParameter;
import net.sf.jasperreports.engine.query.JRAbstractQueryExecuter;
import net.sf.jasperreports.engine.util.JRProperties;

public class PlSqlQueryExecuter
  extends JRAbstractQueryExecuter
{
  private static final Log log = LogFactory.getLog(PlSqlQueryExecuter.class);
  private static final int ORACLE_CURSOR_TYPE = -10;
  private Connection connection;
  private PreparedStatement statement;
  private ResultSet resultSet;
  private int cursorParameter = -1;
  private boolean isStoredProcedure = false;
  
  public PlSqlQueryExecuter(JRDataset dataset, Map parameters)
  {
    super(dataset, parameters);
    
    this.connection = ((Connection)getParameterValue("REPORT_CONNECTION"));
    if (this.connection == null) {
      if (log.isWarnEnabled()) {
        log.warn("The supplied java.sql.Connection object is null.");
      }
    }
    parseQuery();
  }
  
  protected String getParameterReplacement(String parameterName)
  {
    return "?";
  }
  
  public JRDataSource createDatasource()
    throws JRException
  {
    JRDataSource dataSource = null;
    
    createStatement();
    if (this.statement != null) {
      try
      {
        Integer reportMaxCount = (Integer)getParameterValue("REPORT_MAX_COUNT");
        if (reportMaxCount != null) {
          this.statement.setMaxRows(reportMaxCount.intValue());
        }
        if (this.isStoredProcedure)
        {
          CallableStatement cstmt = (CallableStatement)this.statement;
          cstmt.execute();
          if (this.cursorParameter > 0) {
            this.resultSet = ((ResultSet)cstmt.getObject(this.cursorParameter));
          }
        }
        else
        {
          this.resultSet = this.statement.executeQuery();
        }
        dataSource = new JRResultSetDataSource(this.resultSet);
      }
      catch (SQLException e)
      {
        throw new JRException("Error executing SQL statement for : " + this.dataset.getName(), e);
      }
    }
    return dataSource;
  }
  
  private boolean isOracleStoredProcedure(String queryString)
    throws SQLException
  {
    if (this.connection == null) {
      return false;
    }
    String dbVendor = this.connection.getMetaData().getDatabaseProductName().toLowerCase();
    if (!"oracle".equals(dbVendor)) {
      return false;
    }
    String sql = queryString.trim().toLowerCase();
    if ((sql.charAt(0) == '{') && (sql.substring(1).trim().startsWith("call "))) {
      return true;
    }
    return false;
  }
  
  private void createStatement()
    throws JRException
  {
    String queryString = getQueryString();
    if ((this.connection != null) && (queryString != null) && (queryString.trim().length() > 0)) {
      try
      {
        this.isStoredProcedure = isOracleStoredProcedure(queryString);
        if (this.isStoredProcedure) {
          this.statement = this.connection.prepareCall(queryString);
        } else {
          this.statement = this.connection.prepareStatement(queryString);
        }
        int fetchSize = JRProperties.getIntegerProperty(this.dataset.getPropertiesMap(), "net.sf.jasperreports.jdbc.fetch.size", 0);
        if (fetchSize > 0) {
          this.statement.setFetchSize(fetchSize);
        }
        List parameterNames = getCollectedParameterNames();
        if (!parameterNames.isEmpty()) {
          for (int i = 0; i < parameterNames.size(); i++)
          {
            String parameterName = (String)parameterNames.get(i);
            JRFillParameter parameter = getParameter(parameterName);
            Class clazz = parameter.getValueClass();
            Object parameterValue = parameter.getValue();
            if (clazz.equals(Object.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 2000);
              } else {
                this.statement.setObject(i + 1, parameterValue);
              }
            }
            else if (clazz.equals(Boolean.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, -7);
              } else {
                this.statement.setBoolean(i + 1, ((Boolean)parameterValue).booleanValue());
              }
            }
            else if (clazz.equals(Byte.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, -6);
              } else {
                this.statement.setByte(i + 1, ((Byte)parameterValue).byteValue());
              }
            }
            else if (clazz.equals(Double.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 8);
              } else {
                this.statement.setDouble(i + 1, ((Double)parameterValue).doubleValue());
              }
            }
            else if (clazz.equals(Float.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 6);
              } else {
                this.statement.setFloat(i + 1, ((Float)parameterValue).floatValue());
              }
            }
            else if (clazz.equals(Integer.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 4);
              } else {
                this.statement.setInt(i + 1, ((Integer)parameterValue).intValue());
              }
            }
            else if (clazz.equals(Long.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, -5);
              } else {
                this.statement.setLong(i + 1, ((Long)parameterValue).longValue());
              }
            }
            else if (clazz.equals(Short.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 5);
              } else {
                this.statement.setShort(i + 1, ((Short)parameterValue).shortValue());
              }
            }
            else if (clazz.equals(BigDecimal.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 3);
              } else {
                this.statement.setBigDecimal(i + 1, (BigDecimal)parameterValue);
              }
            }
            else if (clazz.equals(String.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 12);
              } else {
                this.statement.setString(i + 1, parameterValue.toString());
              }
            }
            else if (clazz.equals(java.util.Date.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 91);
              } else {
                this.statement.setDate(i + 1, new java.sql.Date(((java.util.Date)parameterValue).getTime()));
              }
            }
            else if (clazz.equals(Timestamp.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 93);
              } else {
                this.statement.setTimestamp(i + 1, (Timestamp)parameterValue);
              }
            }
            else if (clazz.equals(Time.class))
            {
              if (parameterValue == null) {
                this.statement.setNull(i + 1, 92);
              } else {
                this.statement.setTime(i + 1, (Time)parameterValue);
              }
            }
            else if (clazz.equals(ResultSet.class))
            {
              if (!this.isStoredProcedure) {
                throw new JRException("OUT paramater used in non-stored procedure call : " + parameterName + " class " + clazz.getName());
              }
              if (this.cursorParameter > 0) {
                throw new JRException("A stored procedure can have at most one cursor parameter : " + parameterName + " class " + clazz.getName());
              }
              ((CallableStatement)this.statement).registerOutParameter(i + 1, -10);
              this.cursorParameter = (i + 1);
            }
            else
            {
              throw new JRException("Parameter type not supported in query : " + parameterName + " class " + clazz.getName());
            }
          }
        }
      }
      catch (SQLException e)
      {
        throw new JRException("Error preparing statement for executing the report query : \n\n" + queryString + "\n\n", e);
      }
    }
  }
  
  public synchronized void close()
  {
    if (this.resultSet != null) {
      try
      {
        this.resultSet.close();
      }
      catch (SQLException e)
      {
        log.error("Error while closing result set.", e);
      }
      finally
      {
        this.resultSet = null;
      }
    }
    if (this.statement != null) {
      try
      {
        this.statement.close();
      }
      catch (SQLException e)
      {
        log.error("Error while closing statement.", e);
      }
      finally
      {
        this.statement = null;
      }
    }
  }
  
  public synchronized boolean cancelQuery()
    throws JRException
  {
    if (this.statement != null) {
      try
      {
        this.statement.cancel();
        return true;
      }
      catch (Throwable t)
      {
        throw new JRException("Error cancelling SQL statement", t);
      }
    }
    return false;
  }
}
