package pe.jaspersoft.jrx.export;

import java.awt.Font;
import java.awt.FontMetrics;
import java.io.InputStream;

import javax.swing.JLabel;

public class FontMetricsDisplayWidthProvider
  implements IDisplayWidthProvider
{
  private FontMetrics fm = null;
  private static int oneCharWidth = 0;
  private static String defaultChar = "N";
  
  public FontMetricsDisplayWidthProvider(String ttfFontPath)
  {
    init(ttfFontPath);
  }
  
  public FontMetricsDisplayWidthProvider()
  {
    init(null);
  }
  
  public void init(String fontName)
  {
    try
    {
      Font font = null;
      if (fontName == null) {
        fontName = System.getProperty("TXTRPT_ALIGNMENT_FONT");
      }
      if (fontName != null)
      {
        InputStream fontInputStream = FontMetricsDisplayWidthProvider.class.getClassLoader().getResourceAsStream(fontName);
        
        font = Font.createFont(0, fontInputStream);
      }
      if (font == null) {
        font = new Font("Courier New", 0, 12);
      }
      this.fm = new JLabel("").getFontMetrics(font);
      
      oneCharWidth = this.fm.stringWidth(defaultChar);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public int getDisplayWidth(String str)
  {
    if ((this.fm != null) && (oneCharWidth > 0)) {
      return this.fm.stringWidth(str) / oneCharWidth;
    }
    return str.length();
  }
}
