angular.module("app.directives", [])
  .directive('notification',function($timeout){
  return {
	  restrict: 'E',
	  replace: true,
	  scope: {
		  ngModel: '='
	  },
	  template:'<div class="alert alert-{{ngModel.type}}" bs-alert="ngModel" ng-hide="ngModel.isClosed"><button class="close missing" ng-click="ngModel.isClosed=true"><i class="icn icn-remove"></i></button>{{ngModel.message}}</div>',
	  link: function(scope, element, attrs) {
	      $timeout(function(){
	    	  if (scope.ngModel.autoclose){
	    		  element.remove();
	    		  scope.ngModel.isClosed=true;
	    	  }
	      }, 5000);
	  }
  }
  })
  .directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                	dateFormat:'dd/mm/yy',
                    onSelect:function (date) {
                    	scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    }
                });
            });
        }
    }
  })
  .directive('multipleEmails', function () {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ctrl) {
        ctrl.$parsers.unshift(function(viewValue) {
        	var emails = viewValue.split(';');
        	var re = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        	
        	var isValid=false;
          
        	angular.forEach(emails, function() {
        		var validityArr = emails.map(function(str){
        			return re.test(str.trim());
        		}); //sample return is [true, true, true, false, false, false]
            
        		var atLeastOneInvalid = false;
        		angular.forEach(validityArr, function(value) {
        			if(value === false){
        				atLeastOneInvalid = true;
        			}
        		});
        		isValid = atLeastOneInvalid;
        	})
		  
		  	if(isValid) {
				ctrl.$setValidity('multipleEmails', false);
			    return undefined;
			} else {
				// ^ all I need is to call the angular email checker here, I think.
			    ctrl.$setValidity('multipleEmails', true);
			    return viewValue;
			}
        });
      }
    };
  });