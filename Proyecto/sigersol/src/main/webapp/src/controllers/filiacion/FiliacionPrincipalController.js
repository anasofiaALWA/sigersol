function FiliacionPrincipalController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	//$scope.validarLogin();
	var urlParametros = $scope.urlRest
			+ "/rest/parametros/obtener_unicodeparametros";
	var urlFiliacion = $scope.urlRest + "/rest/filiacion/listar";

	$scope.id_criterio = "";
	$scope.id_operador = "";

	$scope.cargarCriterio = function() {
		$http(
				{
					method : 'POST',
					url : urlParametros,
					data : $.param({
						"nombre_tabla" : "CRITERIO_FILIACION",
						"accion" : 2
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			$scope.listaCriterio = data;
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.cargarOperador = function() {
		$http(
				{
					method : 'POST',
					url : urlParametros,
					data : $.param({
						"nombre_tabla" : "OPERADOR",
						"accion" : 2
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			$scope.listaOperador = data;
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.cargarCriterio();
	$scope.cargarOperador();

	$scope.cargarFiliacion = function() {
		$scope.tblFiliacion = new NgTableParams(
				{
					page : 1,
					count : 10,
					filter : {},
					sorting : {

					}
				},
				{
					counts : [ 10, 15, 20, 25 ],
					getData : function($defer, params) {
						var paramJson = {
							Filtro : {
								criterio : $scope.id_criterio,
								operador : $scope.id_operador,
								argumento : $scope.argumento
							}
						};
						$http(
								{
									method : 'POST',
									url : urlFiliacion,
									data : $.param({
										"filtroJson" : JSON
												.stringify(paramJson)
									}),
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									}
								})
								.success(
										function(data) {
											blockUI.start();
											var filtro = {};
											var NewData = [];
											// console.log(JSON.stringify(data));
											// for (var x = 0; x <
											// data.datosAdicionales.LstPerFiliacion.length;
											// x++) {
											//
											// var lista = {
											// "persona_id" :
											// data.datosAdicionales.LstPerFiliacion[x].persona_id,
											// "tipoPersona" :
											// data.datosAdicionales.LstPerFiliacion[x].tipoPersona,
											// "tipoDoc" :
											// data.datosAdicionales.LstPerFiliacion[x].tipoDoc.valor_parametro,
											// "numDoc" :
											// data.datosAdicionales.LstPerFiliacion[x].numDoc,
											// "cip" :
											// data.datosAdicionales.LstPerFiliacion[x].cip,
											// "grado_categ" :
											// data.datosAdicionales.LstPerFiliacion[x].grado_categ,
											// "arma_carrera" :
											// data.datosAdicionales.LstPerFiliacion[x].arma_carrera,
											// "apellNombre" :
											// data.datosAdicionales.LstPerFiliacion[x].apellNombre
											//
											// }
											// NewData.push(lista);
											// }
											var nData = data.datosAdicionales.LstPerFiliacion;
											params.total(nData.length);
											$scope.orderedData = params
													.sorting() ? $filter(
													'orderBy')(nData,
													params.orderBy()) : nData;
											var filterData = filtro ? $filter(
													'filter')(
													$scope.orderedData, filtro)
													: $scope.orderedData;
											params.total(filterData.length);
											$defer.resolve(filterData.slice(
													(params.page() - 1)
															* params.count(),
													params.page()
															* params.count()));

											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	// $scope.cargarFiliacion();

	// $scope.verDatosPersonal = function(e) {
	// $localStorage.PERSONAL_ID = e.persona_id;
	// $localStorage.ACCION = 0;
	// $localStorage.DATOS_PERSONA = e.grado_nombre_corto.toUpperCase() + " "
	// + e.arma_nombre_corto.toUpperCase() + " "
	// + e.nombres.toUpperCase();
	// $localStorage.CIP = e.numero_cip;
	// $location.path("/filiacionfamiliares/filiacionfamiliares");
	// }

	$scope.buscar = function() {
		if ($scope.id_criterio == "" || $scope.id_operador == ""
				|| $scope.argumento == "") {

			alertService
					.showWarning("Seleccione y/o ingrese valores de búsqueda.");
			return;
		}

		$scope.cargarFiliacion();
	}

	$scope.editar = function(e) {
		$localStorage.PERSONA_ID = e.persona_id;
		$localStorage.DATOS_PERSONA = e.grado_categ.toUpperCase() + " "
				+ e.arma_carrera.toUpperCase() + " "
				+ e.apellNombre.toUpperCase();
		$localStorage.CIP = e.cip;
		$localStorage.ACCION = 2;
		$localStorage.TIPO_PERSONA = e.tipoPersona;
		$location.path("/filiacion/datosFiliacion");
	}

	$scope.nuevo = function() {
		$localStorage.PERSONA_ID = 0;
		$localStorage.ACCION = 1;
		$localStorage.DATOS_PERSONA = "";
		$localStorage.CIP = "";
		$localStorage.TIPO_PERSONA = "";
		$location.path("/filiacion/datosFiliacion");
	}
}