function MasterFiliacionController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	//$scope.validarLogin();

	$(function() {
		$('#idContenedor').tabs();
		$('.ui-widget-header').css("background-color", "#eeeeee");
		$('.ui-tabs .ui-tabs-nav .ui-tabs-anchor').css("float", "none");
		$('.ui-widget-header').css("border", "0");
	});

	$('#lista').click(function(e) {
		var id = e.target.id;
		console.log("id tab >> " + id);
		switch (id) {
		case "idPersonal":
			break;
		case "idSeguro":
			$scope.cargaInicialSeguro();
			break;
		case "idImagen":
			$scope.cargaInicialImagen();
			break;
		case "idPosesionArma":
			$scope.cargaInicialArmto();
			break;
		}

	});

	$scope.paramFilter = {
		"persona_id" : $localStorage.PERSONA_ID,
		"datos_persona" : $localStorage.DATOS_PERSONA,
		"nro_cip" : $localStorage.CIP,
		"tipo_persona" : $localStorage.TIPO_PERSONA,
		"accion" : $localStorage.ACCION
	// 2:Editar //1:Nuevo
	};

	if ($scope.paramFilter.tipo_persona == "A"
			|| $scope.paramFilter.tipo_persona == "B") {
		$('#idPosesionArma').removeClass('disabledLink');
		$('#idPosesionArma').css('cursor', 'pointer');
	}

	$scope.activarTabs = function() {
		$('#idSeguro').removeClass('disabledLink');
		$('#idSeguro').css('cursor', 'pointer');
		$('#idImagen').removeClass('disabledLink');
		$('#idImagen').css('cursor', 'pointer');

	}

	// URL
	var urlUbigeo = $scope.urlRest + "/rest/ubigeo/obtener_ubigeo";
	var urlParametros = $scope.urlRest
			+ "/rest/parametros/obtener_unicodeparametros";
	var urlObtenerXid = $scope.urlRest + "/rest/filiacion/obtenerXid";
	var urlRegistrarPersona = $scope.urlRest + "/rest/filiacion/registrar";

	var urlLstSeguro = $scope.urlRest + "/rest/poliza/lista";
	var urlLstCarta = $scope.urlRest + "/rest/carta/obtener";
	var urlLstEmisor = $scope.urlRest + "/rest/emisor/lista";
	var urlRegistrarSeguro = $scope.urlRest + "/rest/poliza/registrar";
	var urlRegistrarCarta = $scope.urlRest + "/rest/carta/registrar";

	var urlRegistrarImagen = $scope.urlRest + "/rest/imagen/registrar";
	var urlLstImagen = $scope.urlRest + "/rest/imagen/lista"

	var urlLstArmamento = $scope.urlRest + "/rest/armamento/lista"
	var urlRegistrarArmamento = $scope.urlRest + "/rest/armamento/registrar"

	/** Tab Personal* */

	$scope.persona = {};
	$scope.fdisDirDom = false;

	$scope.lstDepartamento = [];
	$scope.lstProvinciaNac = [];
	$scope.lstDistritoNac = [];
	$scope.lstCentroPobladoNac = [];
	$scope.lstProvinciaDom = [];
	$scope.lstDistritoDom = [];
	$scope.lstCentroPobladoDom = [];

	$scope.lstTipoDocumento = [];
	$scope.lstEstadoCivil = [];
	$scope.lstGpoSangre = [];
	$scope.lstSexo = [];
	$scope.lstNucleoUrb = [];
	$scope.lstTipoVia = [];
	$scope.lstTipoInterno = [];
	$scope.lstDominio = [];
	$scope.lstBanco = [];

	$scope.calcularEdad = function() {
		var edad = "";

		var fechaActual = new Date();
		dia_actual = fechaActual.getDate();
		mes_actual = fechaActual.getMonth() + 1;
		ano_actual = fechaActual.getFullYear();

		var fecha_nac_seleccionada = $scope.persona.fechaNacimiento.split("/");
		dia_nacimiento = fecha_nac_seleccionada[0];
		mes_nacimiento = fecha_nac_seleccionada[1];
		ano_nacimiento = fecha_nac_seleccionada[2];

		if (mes_nacimiento > mes_actual) {
			edad = (ano_actual - ano_nacimiento) - 1;
		} else if (mes_nacimiento == mes_actual && dia_nacimiento > dia_actual) {
			edad = (ano_actual - ano_nacimiento) - 1;
		} else if (mes_nacimiento == mes_actual && dia_nacimiento >= dia_actual) {
			edad = ano_actual - ano_nacimiento;
		} else {
			edad = ano_actual - ano_nacimiento;
		}

		$scope.persona.edad = edad + " " + "años.";
	}

	$scope.cargaDepartamento = function() {
		$http(
				{
					method : 'POST',
					url : urlUbigeo,
					data : $.param({
						"tipo_ubg" : 1
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			$scope.lstDepartamento = data;
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.cargaProvincia = function(departamento, flag) {

		$http(
				{
					method : 'POST',
					url : urlUbigeo,
					data : $.param({
						"tipo_ubg" : 2,
						"cod_dep" : departamento.ubigeo_id
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			switch (flag) {
			case 1:
				$scope.lstProvinciaNac = data;
				break;
			case 2:
				$scope.lstProvinciaDom = data;
				break;
			}
		}).error(function(status) {
			alertService.showDanger();
		});

		return $scope.lstProvincia;
	}

	$scope.cargaDistrito = function(departamento, provincia, flag) {

		$http(
				{
					method : 'POST',
					url : urlUbigeo,
					data : $.param({
						"tipo_ubg" : 3,
						"cod_dep" : departamento.ubigeo_id,
						"cod_prov" : provincia.ubigeo_id
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			switch (flag) {
			case 1:
				$scope.lstDistritoNac = data;
				break;
			case 2:
				$scope.lstDistritoDom = data;
				break;
			}
		}).error(function(status) {
			alertService.showDanger();
		});

		return $scope.lstDistrito;
	}

	$scope.cargaCentroPoblado = function(departamento, provincia, distrito,
			flag) {

		$http(
				{
					method : 'POST',
					url : urlUbigeo,
					data : $.param({
						"tipo_ubg" : 4,
						"cod_dep" : departamento.ubigeo_id,
						"cod_prov" : provincia.ubigeo_id,
						"cod_dist" : distrito.ubigeo_id
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			switch (flag) {
			case 1:
				$scope.lstCentroPobladoNac = data;
				break;
			case 2:
				$scope.lstCentroPobladoDom = data;
				break;
			}
		}).error(function(status) {
			alertService.showDanger();
		});

		return $scope.lstCCPP;
	}

	$scope.getLstParametro = function(nombreTabla) {

		$http(
				{
					method : 'POST',
					url : urlParametros,
					data : $.param({
						"nombre_tabla" : nombreTabla,
						"accion" : 2
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					},
				}).success(function(data) {
			switch (nombreTabla) {
			case "TIPO_DOCUMENTO":
				$scope.lstTipoDocumento = data;
				break;
			case "ESTADO_CIVIL":
				$scope.lstEstadoCivil = data;
				break;
			case "TIPO_SANGRE":
				$scope.lstGpoSangre = data;
				break;
			case "SEXO":
				$scope.lstSexo = data;
			case "NUCLEO_URBANO":
				$scope.lstNucleoUrb = data;
				break;
			case "TIPO_VIA":
				$scope.lstTipoVia = data;
				break;
			case "TIPO_INTERNO":
				$scope.lstTipoInterno = data;
				break;
			case "DOMINIOS_EMAIL":
				$scope.lstDominio = data;
				break;
			case "ENTIDAD_FINANCIERA":
				$scope.lstBanco = data;
				break;
			case "TIPO_POLIZA":
				$scope.lstTipoPoliza = data;
				break;
			case "IMAGENES":
				$scope.lstTipoImagen = data;
				break;
			case "TIPO_ARMAMENTO":
				$scope.lstTipoArmto = data;
				break;
			case "TIPO_ARMAMENTO":
				$scope.lstTipoArmto = data;
				break;
			case "MARCA_ARMAMENTO":
				$scope.lstMarcaArmto = data;
				break;
			case "CONDICION_ARMAMENTO":
				$scope.lstCondicion = data;
				break;
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.cargaLstTipoDocumento = function() {
		$scope.getLstParametro('TIPO_DOCUMENTO');
	}

	$scope.cargaLstEstadoCivil = function() {
		$scope.getLstParametro('ESTADO_CIVIL');
	}

	$scope.cargaLstGpoSangre = function() {
		$scope.getLstParametro('TIPO_SANGRE');
	}

	$scope.cargaLstSexo = function() {
		$scope.getLstParametro('SEXO');
	}

	$scope.cargaLstNucleUrb = function() {
		$scope.getLstParametro('NUCLEO_URBANO');
	}

	$scope.cargaLstTipoVia = function() {
		$scope.getLstParametro('TIPO_VIA');
	}

	$scope.cargaLstTipoInterno = function() {
		$scope.getLstParametro('TIPO_INTERNO');
	}

	$scope.cargaLstDominio = function() {
		$scope.getLstParametro('DOMINIOS_EMAIL');
	}

	$scope.cargaLstBanco = function() {
		$scope.getLstParametro('ENTIDAD_FINANCIERA');
	}

	$scope.cargaProvinciaNac = function() {
		$scope.cargaProvincia($scope.persona.departamentoNac_selected, 1);
	}

	$scope.cargaDistritoNac = function() {
		$scope.cargaDistrito($scope.persona.departamentoNac_selected,
				$scope.persona.provinciaNac_selected, 1);
	}

	$scope.cargaCentroPobladoNac = function() {
		$scope.cargaCentroPoblado($scope.persona.departamentoNac_selected,
				$scope.persona.provinciaNac_selected,
				$scope.persona.distritoNac_selected, 1);
	}

	$scope.cargaProvinciaDom = function() {
		$scope.lstProvinciaDom = $scope.cargaProvincia(
				$scope.persona.departamentoDom_selected, 2);
	}

	$scope.cargaDistritoDom = function() {
		$scope.cargaDistrito($scope.persona.departamentoDom_selected,
				$scope.persona.provinciaDom_selected, 2);
	}

	$scope.cargaCentroPobladoDom = function() {
		$scope.cargaCentroPoblado($scope.persona.departamentoDom_selected,
				$scope.persona.provinciaDom_selected,
				$scope.persona.distritoDom_selected, 2);
	}

	$scope.obtenerPersona = function() {

		$scope.persona.tipo_persona = $scope.paramFilter.tipo_persona;

		var paramJson = {
			Filtro : {
				idPersona : $scope.paramFilter.persona_id
			}
		}

		$http(
				{
					method : 'POST',
					url : urlObtenerXid,
					data : $.param({
						"filtroJson" : JSON.stringify(paramJson)
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				})
				.success(
						function(data) {
							console
									.log("Persona : "
											+ JSON
													.stringify(data.datosAdicionales.Persona));
							$scope.persona.persona_id = data.datosAdicionales.Persona.persona_id;
							$scope.persona.apellidoPaterno = data.datosAdicionales.Persona.apellidoPaterno;
							$scope.persona.apellidoMaterno = data.datosAdicionales.Persona.apellidoMaterno;
							$scope.persona.nombres = data.datosAdicionales.Persona.nombres;
							$scope.persona.tipoDoc_selected = data.datosAdicionales.Persona.tipoDoc;
							$scope.persona.nroDoc = data.datosAdicionales.Persona.nroDoc;
							$scope.persona.estadoCivil_selected = data.datosAdicionales.Persona.estadoCivil;
							$scope.persona.cip = data.datosAdicionales.Persona.cip;
							$scope.persona.nroSerie = data.datosAdicionales.Persona.nroSerie;
							$scope.persona.gpoSangre_selected = data.datosAdicionales.Persona.gpoSangre;
							$scope.persona.sexo_selected = data.datosAdicionales.Persona.sexo;
							$scope.persona.fechaNacimiento = data.datosAdicionales.Persona.fechaNacimiento;
							$scope.persona.edad = data.datosAdicionales.Persona.edad;
							$scope.persona.telCelular = data.datosAdicionales.Persona.telCelular;
							$scope.persona.telCasa = data.datosAdicionales.Persona.telCasa;
							$scope.persona.email = data.datosAdicionales.Persona.email;
							$scope.persona.dominio_selected = data.datosAdicionales.Persona.dominio;

							if (data.datosAdicionales.Persona.ctaBanco != undefined) {

								$scope.persona.ctaBanco_id = data.datosAdicionales.Persona.ctaBanco.ctaBanco_id;
								$scope.persona.banco_selected = data.datosAdicionales.Persona.ctaBanco.banco;
								$scope.persona.numCuenta = data.datosAdicionales.Persona.ctaBanco.numCuenta;
								$scope.persona.numCCI = data.datosAdicionales.Persona.ctaBanco.numCCI;
							} else {
								$scope.persona.ctaBanco_id = -1;
							}
							if (data.datosAdicionales.Persona.direccionNac != undefined) {
								$scope.persona.direccionNac_id = data.datosAdicionales.Persona.direccionNac.direccion_id;
								$scope.persona.departamentoNac_selected = data.datosAdicionales.Persona.direccionNac.departamento;
								$scope.cargaProvinciaNac();
								$scope.persona.provinciaNac_selected = data.datosAdicionales.Persona.direccionNac.provincia;
								$scope.cargaDistritoNac();
								$scope.persona.distritoNac_selected = data.datosAdicionales.Persona.direccionNac.distrito;
								$scope.cargaCentroPobladoNac();
								$scope.persona.ccppNac_selected = data.datosAdicionales.Persona.direccionNac.ccpp;
							} else {
								$scope.persona.direccionNac_id = -1
							}
							if (data.datosAdicionales.Persona.direccionDom != undefined) {

								$scope.persona.direccionDom_id = data.datosAdicionales.Persona.direccionDom.direccion_id;
								$scope.persona.departamentoDom_selected = data.datosAdicionales.Persona.direccionDom.departamento;
								$scope.cargaProvinciaDom();
								$scope.persona.provinciaDom_selected = data.datosAdicionales.Persona.direccionDom.provincia;
								$scope.cargaDistritoDom();
								$scope.persona.distritoDom_selected = data.datosAdicionales.Persona.direccionDom.distrito;
								$scope.cargaCentroPobladoDom();
								$scope.persona.ccppDom_selected = data.datosAdicionales.Persona.direccionDom.ccpp;
								$scope.persona.nucleoUrb_selected = data.datosAdicionales.Persona.direccionDom.nucleoUrb;
								$scope.persona.nucleoUrb_nombre = data.datosAdicionales.Persona.direccionDom.nucleoUrb_nombre;
								$scope.persona.tipoVia_selected = data.datosAdicionales.Persona.direccionDom.tipoVia;
								$scope.persona.tipoVia_nombre = data.datosAdicionales.Persona.direccionDom.tipoVia_nombre;
								$scope.persona.tipoVia_numero = data.datosAdicionales.Persona.direccionDom.tipoVia_numero;
								$scope.persona.tipoInterno_selected = data.datosAdicionales.Persona.direccionDom.tipoInterno;
								$scope.persona.tipoInterno_nombre = data.datosAdicionales.Persona.direccionDom.tipoInterno_nombre;
								$scope.persona.referenciaDir = data.datosAdicionales.Persona.direccionDom.referenciaDir;
								$scope.persona.personaRef = data.datosAdicionales.Persona.direccionDom.personaRef;
								$scope.persona.telefonoRef = data.datosAdicionales.Persona.direccionDom.telefonoRef;
							} else {
								$scope.persona.direccionDom_id = -1;
							}
							alertService
									.showSuccess("Datos cargados correctamente");
						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.cargaInicialPersonal = function() {

		$scope.cargaLstTipoDocumento();
		$scope.cargaLstEstadoCivil();
		$scope.cargaLstGpoSangre();
		$scope.cargaLstSexo();
		$scope.cargaDepartamento();
		$scope.cargaLstNucleUrb();
		$scope.cargaLstTipoVia();
		$scope.cargaLstTipoInterno();
		$scope.cargaLstDominio();
		$scope.cargaLstBanco();

		if ($scope.paramFilter.accion == 2) {
			$scope.obtenerPersona();
			$scope.activarTabs();

		} else {
			$scope.paramFilter.tipo_persona = 'G';
		}
	}

	$scope.cargaInicialPersonal();

	$scope.grabarPersona = function() {

		var paramJson = {
			Persona : $scope.persona
		};

		console.log("Persona : " + JSON.stringify(paramJson));

		$http(
				{
					method : 'POST',
					url : urlRegistrarPersona,
					data : $.param({
						"jsonPersona" : JSON.stringify(paramJson),
						"accion" : $scope.paramFilter.accion,
						"usuarioLogin" : $localStorage.Nombre_Usuario
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				})
				.success(
						function(data) {
							var id, cip;
							if ($scope.paramFilter.accion == 1) {
								var resultado = data.datosAdicionales.id
										.split(":::");
								id = resultado[0];
								cip = resultado[1];
								$scope.activarTabs();
							} else {
								id = data.datosAdicionales.id
							}
							if (id > 0) {
								$scope.paramFilter.persona_id = id;
								$scope.paramFilter.datos_persona = $scope.persona.apellidoPaterno
										+ " "
										+ $scope.persona.apellidoMaterno
										+ " " + $scope.persona.nombres;
								$scope.paramFilter.accion = 2;
								$scope.persona.cip = cip;
								alertService.showSuccess("Registro exitoso.");
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.limpiarPersona = function() {
		$scope.persona.apellidoPaterno = "";
		$scope.persona.apellidoMaterno = "";
		$scope.persona.nombres = "";
		$scope.persona.tipoDoc_selected = "";
		$scope.persona.nroDoc = "";
		$scope.persona.estadoCivil_selected = "";
		$scope.persona.cip = "";
		$scope.persona.nroSerie = "";
		$scope.persona.gpoSangre_selected = "";
		$scope.persona.sexo_selected = "";
		$scope.persona.fechaNacimiento = "";
		$scope.persona.edad = "";
		$scope.persona.telCelular = "";
		$scope.persona.telCasa = "";
		$scope.persona.email = "";
		$scope.persona.dominio_selected = "";

		$scope.persona.banco_selected = "";
		$scope.persona.numCuenta = "";
		$scope.persona.numCCI = "";

		$scope.persona.departamentoNac_selected = "";
		$scope.persona.provinciaNac_selected = "";
		$scope.persona.distritoNac_selected = "";
		$scope.persona.ccppNac_selected = "";

		$scope.persona.departamentoDom_selected = "";
		$scope.persona.provinciaDom_selected = "";
		$scope.persona.distritoDom_selected = "";
		$scope.persona.ccppDom_selected = "";
		$scope.persona.nucleoUrb_selected = "";
		$scope.persona.nucleoUrb_nombre = "";
		$scope.persona.tipoVia_selected = "";
		$scope.persona.tipoVia_nombre = "";
		$scope.persona.tipoVia_numero = "";
		$scope.persona.tipoInterno_selected = "";
		$scope.persona.tipoInterno_nombre = "";
		$scope.persona.referenciaDir = "";
		$scope.persona.telCelular = "";
		$scope.persona.telCasa = "";
		$scope.persona.email = "";
		$scope.persona.dominio_selected = "";
		$scope.persona.personaRef = "";
		$scope.persona.telefonoRef = "";
	}

	$scope.limpiarCuentaBanco = function() {
		$scope.persona.ctaBanco_id = -1;
		$scope.persona.banco_selected = "";
		$scope.persona.numCuenta = "";
		$scope.persona.numCCI = "";
	}

	$scope.limpiarDomicilioActual = function() {
		$scope.persona.direccionDom_id = -1;
		$scope.persona.departamentoDom_selected = "";
		$scope.persona.provinciaDom_selected = "";
		$scope.persona.distritoDom_selected = "";
		$scope.persona.ccppDom_selected = "";
		$scope.persona.nucleoUrb_selected = "";
		$scope.persona.nucleoUrb_nombre = "";
		$scope.persona.tipoVia_selected = "";
		$scope.persona.tipoVia_nombre = "";
		$scope.persona.tipoVia_numero = "";
		$scope.persona.tipoInterno_selected = "";
		$scope.persona.tipoInterno_nombre = "";
		$scope.persona.referenciaDir = "";
		$scope.persona.telCelular = "";
		$scope.persona.telCasa = "";
		$scope.persona.email = "";
		$scope.persona.dominio_selected = "";
		$scope.persona.personaRef = "";
		$scope.persona.telefonoRef = "";
	}

	/** Tab Seguros* */

	$scope.seguro = {};
	$scope.carta = {};
	$scope.lstEmisor = [];
	$scope.lstTipoPoliza = [];

	$scope.cargaLstSeguro = function() {
		console.log("persona_id:" + $scope.paramFilter.persona_id);
		$scope.tblDetalleSeguro = new NgTableParams(
				{
					page : 1,
					count : 10,
					filter : {},
					sorting : {

					}
				},
				{
					counts : [ 10, 15, 20, 25 ],
					getData : function($defer, params) {
						var paramJson = {
							Filtro : {
								idPersona : $scope.paramFilter.persona_id
							}
						};
						$http(
								{
									method : 'POST',
									url : urlLstSeguro,
									data : $.param({
										"filtroJson" : JSON
												.stringify(paramJson)
									}),
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									}
								})
								.success(
										function(data) {
											console
													.log("LstPoliza"
															+ JSON
																	.stringify(data.datosAdicionales.LstPoliza));
											var filtro = {};
											var NewData = [];
											var nData = data.datosAdicionales.LstPoliza;
											blockUI.start();
											params.total(nData.length);
											$scope.orderedData = params
													.sorting() ? $filter(
													'orderBy')(nData,
													params.orderBy()) : nData;
											var filterData = filtro ? $filter(
													'filter')(
													$scope.orderedData, filtro)
													: $scope.orderedData;
											params.total(filterData.length);
											$defer.resolve(filterData.slice(
													(params.page() - 1)
															* params.count(),
													params.page()
															* params.count()));

											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.cargaLstCarta = function() {
		$scope.tblDetalleCarta = new NgTableParams(
				{
					page : 1,
					count : 10,
					filter : {},
					sorting : {

					}
				},
				{
					counts : [ 10, 15, 20, 25 ],
					getData : function($defer, params) {
						var paramJson = {
							Filtro : {
								idPersona : $scope.paramFilter.persona_id
							}
						};
						$http(
								{
									method : 'POST',
									url : urlLstCarta,
									data : $.param({
										"filtroJson" : JSON
												.stringify(paramJson)
									}),
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									}
								})
								.success(
										function(data) {
											$scope.carta = data.datosAdicionales.LstCarta;
											var nData = data.datosAdicionales.LstCarta;
											console
													.log("LstCarta"
															+ JSON
																	.stringify(data.datosAdicionales.LstCarta));
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.cargaInicialSeguro = function() {
		$scope.cargaLstSeguro();
		$scope.cargaLstCarta();
	}

	$scope.cargaLstEmisor = function() {

		$http(
				{
					method : 'POST',
					url : urlLstEmisor,
					data : $.param({}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			$scope.lstEmisor = data.datosAdicionales.LstEmisor;

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.cargaLstTipoPoliza = function() {
		$scope.getLstParametro('TIPO_POLIZA');
	}

	$scope.grabarSeguro = function() {

		var paramJson = {
			Seguro : $scope.seguro
		};

		console.log("Seguro : " + JSON.stringify(paramJson));

		$http(
				{
					method : 'POST',
					url : urlRegistrarSeguro,
					data : $.param({
						"jsonPoliza" : JSON.stringify(paramJson),
						"accion" : $scope.seguro.flagAccion,
						"usuarioLogin" : $localStorage.Nombre_Usuario
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.tblDetalleSeguro.reload();

			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

		$('#modalSeguro').modal('hide');
	}

	$scope.limpiarSeguro = function() {
		$scope.seguro.poliza_id = -1;
		$scope.seguro.emisor_selected = "";
		$scope.seguro.tipoPoliza_selected = "";
		$scope.seguro.cobertura = "";
		$scope.seguro.fechaIni = "";
		$scope.seguro.vigencia = "";
	}

	$scope.limpiarCarta = function() {
		$scope.carta.fechaIni = "";
		$scope.carta.fechaFin = "";
	}

	$scope.grabarCarta = function(result) {

		var mensajetxt = result.replace("<pre>", "").replace("</pre>", "");
		var objResultado = mensajetxt.split("&lt;**&gt;");
		console.log("mesajetxt > " + mensajetxt);
		console.log("objResultado > " + objResultado);

		var lengthObj = objResultado.length;
		console.log("lengthObj > " + lengthObj);
		var fieldName = "";
		var fileName = "";
		var fileNameRef = "";

		if (lengthObj == 5) {
			fieldName = objResultado[1];
			if (fieldName == "dataFile") {
				fileNameRef = objResultado[2];
				fileName = objResultado[4];
			}
		}

		$scope.carta.fileName = fileName;
		$scope.carta.fileNameRef = fileNameRef;

		console.log("fileNameResRef:" + fileNameRef + "/ fileNameRes :"
				+ fileName);

		var paramJson = {
			Carta : $scope.carta
		};

		console.log("Carta : " + JSON.stringify(paramJson));

		$http(
				{
					method : 'POST',
					url : urlRegistrarCarta,
					data : $.param({
						"jsonCarta" : JSON.stringify(paramJson),
						"accion" : $scope.carta.flagAccion,
						"usuarioLogin" : $localStorage.Nombre_Usuario
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				$scope.tblDetalleCarta.reload();
				alertService.showSuccess("Registro exitoso.");

			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

		$('#modalCarta').modal('hide');
	}

	// Modales

	$scope.nuevoSeguro = function() {
		$scope.limpiarSeguro();
		$scope.cargaLstEmisor();
		$scope.cargaLstTipoPoliza();

		$scope.seguro.persona_id = $scope.paramFilter.persona_id;
		$scope.seguro.flagAccion = 1;
		$scope.titulo = "Agregar Seguro";
		$('#modalSeguro').modal();
	}

	$scope.verCobertura = function(detCobertura) {
		$scope.titulo = "Ver Cobertura";
		$scope.cobertura = detCobertura;
		$('#modalCobertura').modal();
	}

	$scope.editarSeguro = function(item) {
		$scope.limpiarSeguro();
		$scope.seguro.persona_id = $scope.paramFilter.persona_id;
		$scope.seguro.poliza_id = item.poliza_id;
		$scope.cargaLstEmisor();
		$scope.seguro.emisor_selected = item.emisor;
		$scope.cargaLstTipoPoliza();
		$scope.seguro.tipoPoliza_selected = item.tipoPoliza;
		$scope.seguro.cobertura = item.cobertura;
		$scope.seguro.fechaIni = item.fechaIni;
		$scope.seguro.vigencia = item.vigencia == '1' ? true : false;

		$scope.seguro.flagAccion = 2;
		$scope.titulo = "Editar Seguro";
		$('#modalSeguro').modal();
	}

	$scope.nuevaCarta = function() {
		$scope.limpiarCarta();
		$scope.carta.persona_id = $scope.paramFilter.persona_id;
		$scope.carta.flagAccion = 1;

		$scope.titulo = "Nueva Carta Declaratoria";
		$('#modalCarta').modal();

	}

	/** Tab Imagenes* */

	$scope.imagen = {};
	$scope.lstTipoImagen = [];
	$scope.lstImagen = [];

	$scope.fdisabled = false;
	$scope.imagen.flagAccion = 1;

	$scope.cargaLstTipoImagen = function() {
		$scope.getLstParametro('IMAGENES');
	}

	$scope.cargaLstImagen = function() {
		$scope.tblImagen = new NgTableParams(
				{
					page : 1,
					count : 10,
					filter : {},
					sorting : {

					}
				},
				{
					counts : [ 10, 15, 20, 25 ],
					getData : function($defer, params) {
						var paramJson = {
							Filtro : {
								idPersona : $scope.paramFilter.persona_id
							}
						};
						$http(
								{
									method : 'POST',
									url : urlLstImagen,
									data : $.param({
										"filtroJson" : JSON
												.stringify(paramJson)
									}),
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									}
								})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstImagen;
											$scope.lstImagen = data.datosAdicionales.LstImagen;
											blockUI.start();
											params.total(nData.length);
											$scope.orderedData = params
													.sorting() ? $filter(
													'orderBy')(nData,
													params.orderBy()) : nData;
											var filterData = filtro ? $filter(
													'filter')(
													$scope.orderedData, filtro)
													: $scope.orderedData;
											params.total(filterData.length);
											$defer.resolve(filterData.slice(
													(params.page() - 1)
															* params.count(),
													params.page()
															* params.count()));

											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.cargaInicialImagen = function() {
		$scope.cargaLstTipoImagen();
		$scope.cargaLstImagen();
		$scope.imagen.persona_id = $scope.paramFilter.persona_id;
	}

	$scope.seleccionarImagen = function() {
		if ($scope.imagen.tipoImg_selected.codigo_parametro != "") {
			if ($scope.lstImagen.length > 0) {
				for (i = 0; i < $scope.lstImagen.length; i++) {
					if ($scope.lstImagen[i].tipoImg.codigo_parametro == $scope.imagen.tipoImg_selected.codigo_parametro) {
						alertService
								.showWarning("Este tipo de imagen ya se encuentra añadida, seleccione la opción editar para que pueda ser modificada.");

						document.getElementById("files").disabled = true;
						return;
					}
				}
			}
			document.getElementById("files").disabled = false;
		} else {
			$('#files').val("");
			$('#fileinput-Img').text("");
			document.getElementById("list").innerHTML = "";
			document.getElementById("files").disabled = true;
		}
	}

	$scope.editarImagen = function(item) {
		$scope.fdisabled = true;
		$scope.imagen.flagAccion = 2;
		$scope.imagen.imagen_id = item.imagen_id;
		$scope.imagen.tipoImg_selected = item.tipoImg;
		$scope.imagen.fileName = item.fileName;
		$scope.imagen.fileNameRef = item.fileNameRef;

		document.getElementById("files").disabled = false;
		document.getElementById("list").innerHTML = [
				'<img class="thumb" src="', item.bytesFile, '" title="',
				item.fileName, '" />' ].join('');
	}

	$scope.verImagen = function(e) {
		document.getElementById("list").innerHTML = [
				'<img class="thumb" src="', e.bytesFile, '" title="',
				e.fileName, '" />' ].join('');
	}

	$scope.filaSeleccionada == null;
	$scope.setColorFila = function(index) {
		$scope.filaSeleccionada = index;
	}

	$scope.grabarImagen = function(result) {

		var mensajetxt = result.replace("<pre>", "").replace("</pre>", "");
		var objResultado = mensajetxt.split("&lt;**&gt;");
		console.log("mesajetxt > " + mensajetxt);
		console.log("objResultado > " + objResultado);

		var lengthObj = objResultado.length;
		console.log("lengthObj > " + lengthObj);
		var fieldName = "";
		var fileName = "";
		var fileNameRef = "";

		if (lengthObj == 5) {
			fieldName = objResultado[1];
			if (fieldName == "files[]") {
				fileNameRef = objResultado[2];
				fileName = objResultado[4];
			}
		}

		$scope.imagen.fileName = fileName == "" ? $scope.imagen.fileName
				: fileName;
		$scope.imagen.fileNameRef = fileNameRef == "" ? $scope.imagen.fileNameRef
				: fileNameRef;

		console.log("fileNameResRef:" + fileNameRef + "/ fileNameRes :"
				+ fileName);

		var paramJson = {
			Imagen : $scope.imagen
		};

		$http(
				{
					method : 'POST',
					url : urlRegistrarImagen,
					data : $.param({
						"jsonImagen" : JSON.stringify(paramJson),
						"accion" : $scope.imagen.flagAccion,
						"usuarioLogin" : $localStorage.Nombre_Usuario
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.tblImagen.reload();

			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	/** Tab Posesion de Armamento */

	$scope.armamento = {};

	$scope.lstArmamento = [];
	$scope.lstTipoArmto = [];
	$scope.lstMarcaArmto = [];
	$scope.lstCondicion = [];

	$scope.cargaLstArmamento = function() {
		$scope.tblArmamento = new NgTableParams(
				{
					page : 1,
					count : 10,
					filter : {},
					sorting : {

					}
				},
				{
					counts : [ 10, 15, 20, 25 ],
					getData : function($defer, params) {
						var paramJson = {
							Filtro : {
								idPersona : $scope.paramFilter.persona_id
							}
						};
						$http(
								{
									method : 'POST',
									url : urlLstArmamento,
									data : $.param({
										"filtroJson" : JSON
												.stringify(paramJson)
									}),
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									}
								})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstArmamento;

											blockUI.start();
											params.total(nData.length);
											$scope.orderedData = params
													.sorting() ? $filter(
													'orderBy')(nData,
													params.orderBy()) : nData;
											var filterData = filtro ? $filter(
													'filter')(
													$scope.orderedData, filtro)
													: $scope.orderedData;
											params.total(filterData.length);
											$defer.resolve(filterData.slice(
													(params.page() - 1)
															* params.count(),
													params.page()
															* params.count()));

											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.grabarArmamento = function() {

		var paramJson = {
			Armamento : $scope.armamento
		};

		console.log("armamento:" + JSON.stringify(paramJson));
		$http(
				{
					method : 'POST',
					url : urlRegistrarArmamento,
					data : $.param({
						"jsonArmamento" : JSON.stringify(paramJson),
						"accion" : $scope.armamento.flagAccion,
						"usuarioLogin" : $localStorage.Nombre_Usuario
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
				}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.tblArmamento.reload();

			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

		$('#modalArmamento').modal('hide');
	}

	$scope.cargaLstTipoArmto = function() {
		$scope.getLstParametro('TIPO_ARMAMENTO');
	}

	$scope.cargaLstMarcaArmto = function() {
		$scope.getLstParametro('MARCA_ARMAMENTO');
	}

	$scope.cargaLstCondicion = function() {
		$scope.getLstParametro('CONDICION_ARMAMENTO');
	}

	$scope.cargaInicialArmto = function() {
		$scope.cargaLstArmamento();
	}

	// Modales
	$scope.nuevoArmamento = function() {
		$scope.cargaLstTipoArmto();
		$scope.cargaLstMarcaArmto();
		$scope.cargaLstCondicion();

		$scope.armamento.armamento_id = -1;
		$scope.armamento.persona_id = $scope.paramFilter.persona_id;
		$scope.armamento.flagAccion = 1;
		$scope.titulo = "Nuevo Armamento";
		$('#modalArmamento').modal();

	}

	$scope.editarArmamento = function(item) {
		$scope.armamento.persona_id = $scope.paramFilter.persona_id;
		$scope.armamento.armamento_id = item.armamento_id;
		$scope.armamento.nroLicencia = item.nroLicencia;
		$scope.armamento.fechaExp = item.fechaExp;
		$scope.armamento.fechaCad = item.fechaCad;
		$scope.cargaLstTipoArmto();
		$scope.armamento.tipoArmto_selected = item.tipoArmto;
		$scope.cargaLstMarcaArmto();
		$scope.armamento.marcaArmto_selected = item.marcaArmto
		$scope.cargaLstCondicion();
		$scope.armamento.condArmto_selected = item.condArmto;
		$scope.armamento.nroSerie = item.nroSerie;
		$scope.armamento.modelo = item.modelo;
		$scope.armamento.calibre = item.calibre;
		$scope.armamento.observacion = item.observacion;
		$scope.armamento.vigencia = item.vigencia == '1' ? true : false;
		$scope.armamento.flagAccion = 2;
		$scope.titulo = "Editar Armamento";
		$('#modalArmamento').modal();
	}

	// solo numeros
	$('#telCelular').keypress(function(e) {
		var key = e.charCode || e.keyCode || 0;
		if (key < 48 || key > 58) {
			return false;
		}
	});

	// solo letras
	$(
			'#id_apellido_paterno, #id_apellido_materno, #id_nombres, #id_persona_referencia')
			.keypress(function(e) {
				var key = e.charCode || e.keyCode || 0;
				if (key > 48 && key < 57) {
					return false;
				}
			});
}