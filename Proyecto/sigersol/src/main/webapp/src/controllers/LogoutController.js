function LogoutController($scope, $http, $routeParams, $location, $rootScope, $localStorage,
		$controller, CONFIG, OPCIONES) {
	$controller('funcGenerales', {
		$scope : $scope
	});

	$scope.urlLogout = $scope.urlRest + "/rest/session/logout";

	$scope.logout = function() {
		$http({
			method : 'GET',
			url : $scope.urlLogout,
			headers : {
				'Authorization' : ""
			}
		})
				.success(function() {
					//$localStorage.permisosL = "";
					$location.path("/login")
				})
				.error(
						function(data, status, headers, config) {
							console
									.info("Error intentando llamar al servicio de desloguear:"
											+ status);
						});
	};

	$scope.logout();
}