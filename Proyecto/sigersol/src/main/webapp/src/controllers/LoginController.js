function LoginController($scope, $http, $routeParams, $location, $rootScope,
		$filter, $controller, CONFIG, OPCIONES, $localStorage, alertService,
		$window) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlLogin = $scope.urlRest + "/rest/sesion/validarTemp";

	$scope.password = "";
	$scope.username_cc = "";
	$scope.password_actual = "";
	$scope.password_nuevo = "";
	$scope.repassword_nuevo = "";

	$scope.validarlogin = function() {
		var paramJson = {
			Login : {
				usuario : $scope.username,
				contrasenia : $scope.password
			}
		};
		$http({
			method : 'POST',
			url : urlLogin,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							console.log("data >" + JSON.stringify(data));
							if (data.exito) {
								$scope.usuarioSesion = data.datosAdicionales;
								$localStorage.SesionId = $scope.usuarioSesion.usuario_id;
								$localStorage.Departamento = $scope.usuarioSesion.municipalidad.ubigeo.departamento;
								$localStorage.Provincia = $scope.usuarioSesion.municipalidad.ubigeo.provincia;
								$localStorage.Distrito = $scope.usuarioSesion.municipalidad.ubigeo.distrito;
								$localStorage.UbigeoId = $scope.usuarioSesion.municipalidad.ubigeo.ubigeoId;
								$localStorage.MunicipalidadId = $scope.usuarioSesion.municipalidad.municipalidad_id;

								alertService
										.showSuccess('Inicio de sesión exitoso');
								$location.path("/gestion");
							} else {
								alertService
										.showDanger('Credenciales inválidas');
							}

						}).error(function(status) {
					alertService.showDanger();
				});
	};

	$scope.summit = function() {

		if ((String($scope.username).localeCompare("") == 0)
				|| (String($scope.username).localeCompare("undefined") == 0)
				|| (String($scope.username).localeCompare("null") == 0)) {

			return;
		}

		if ((String($scope.password).localeCompare("") == 0)
				|| (String($scope.password).localeCompare("null") == 0)
				|| (String($scope.password).localeCompare("undefined") == 0)) {
			return;
		}

		$scope.validarlogin();
	};

	$scope.ngEnter = function(keyEvent) {
		if (keyEvent.which == 13)
			$scope.summit();
	};
}