function AdministracionController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	// $scope.validarLogin();

	var urlRegistrarAdministracion = $scope.urlRest + "/rest/administracion/registrar";
	var urlActualizarAdministracion = $scope.urlRest + "/rest/administracion/actualizar";
	var urlListarAdministracion = $scope.urlRest + "/rest/administracion/obtener";
	
	$scope.administracion = {};
	$scope.lista = [];
	$scope.lstAdministracion = [];
	
	
	$scope.administracion.cod_usuario = '15';
	$scope.administracion.municipalidad_id = '1';

	$scope.grabarAdministracion = function() {
		
		var paramJson = {
			Administracion : $scope.administracion
		};

		console.log("Administracion : " + JSON.stringify(paramJson));

		$http({
			method : 'POST',
			url : urlRegistrarAdministracion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.actualizarAdministracion = function() {
		
		var paramJson = {
			Administracion : $scope.solicitud
		};
		
		console.log("Administracion : " + JSON.stringify(paramJson));
		
		$http({
			method : 'POST',
			url : urlActualizarAdministracion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
	}
	
	$scope.listarAdministracion = function() {
		
		$http({
			method : 'POST',
			url : urlListarAdministracion,
			data : "",
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstAdministracion = data.datosAdicionales.LstAdministracion;
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.cancelar = function() {
		$location.path("/panel");
	}
}