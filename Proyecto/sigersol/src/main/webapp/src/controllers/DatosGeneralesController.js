function DatosGeneralesController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlRegistrarDatosGenerales = $scope.urlRest + "/rest/datosGenerales/registrar";
	var urlActualizarDatosGenerales = $scope.urlRest + "/rest/datosGenerales/actualizar";
	var urlListarDatosGenerales = $scope.urlRest + "/rest/datosGenerales/obtener";
	
	
	$scope.datos = {};
	$scope.lista = [];
	$scope.lstDatosGenerales = [];
	
	$scope.flag = 0;
	
	$scope.datos.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.datos.cod_usuario = '15';
	
	$scope.guardar = function() {
		if($scope.flag == 0)
			$scope.grabarDatosGenerales();
		else
			$scope.actualizarDatosGenerales();
	}
	
	$scope.grabarDatosGenerales = function() {
		
		var paramJson = {
			DatosGenerales : $scope.datos
		};

		$http({
			method : 'POST',
			url : urlRegistrarDatosGenerales,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.datos.datos_generales_id = data.datosAdicionales.id;
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}	
	
	$scope.actualizarDatosGenerales = function() {
		
		var paramJson = {
			DatosGenerales : $scope.datos
		};
		
		$http({
			method : 'POST',
			url : urlActualizarDatosGenerales,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");	
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}
	
	$scope.listarDatosGenerales = function() {
		
		var paramJson = {
			DatosGenerales : $scope.datos
		};
		
		$http({
			method : 'POST',
			url : urlListarDatosGenerales,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {			
			$scope.datos = data.datosAdicionales.datosGenerales;
			if($scope.datos.datos_generales_id != undefined){	
				$scope.flag = 1;
			}
			$scope.datos.ciclo_grs_id = $localStorage.GRS_ID;
			$scope.datos.cod_usuario = '15';
		}).error(function(status) {
			alertService.showDanger();
		});
	}
		
	
	$scope.totalPoblacion = function() {
		
		var totalP = 0;
		if($scope.datos.poblacion_urbana != null && $scope.datos.poblacion_rural != null){
		
			totalP = $scope.datos.poblacion_urbana + $scope.datos.poblacion_rural;
		}
		
		return totalP;
	}
	
	
	$scope.volver = function() {
		$location.path("/gestion");
	}
	
	$scope.listarDatosGenerales();
}