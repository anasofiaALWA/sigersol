function RecoleccionController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlRegistrarRecoleccionDf = $scope.urlRest
			+ "/rest/recoleccionDf/registrar";
	var urlActualizarRecoleccionDf = $scope.urlRest
			+ "/rest/recoleccionDf/actualizar";
	var urlListarRecoleccionDf = $scope.urlRest + "/rest/recoleccionDf/obtener";

	var urlRegistrarRecoleccionVal = $scope.urlRest
			+ "/rest/recoleccionVal/registrar";
	var urlActualizarRecoleccionVal = $scope.urlRest
			+ "/rest/recoleccionVal/actualizar";
	var urlListarRecoleccionVal = $scope.urlRest
			+ "/rest/recoleccionVal/obtener";

	var urlRegistrarRecoleccionRcd = $scope.urlRest
			+ "/rest/recoleccionRcd/registrar";
	var urlActualizarRecoleccionRcd = $scope.urlRest
			+ "/rest/recoleccionRcd/actualizar";
	var urlListarRecoleccionRcd = $scope.urlRest
			+ "/rest/recoleccionRcd/obtener";

	var urlRegistrarRecoleccionVehiculo = $scope.urlRest
			+ "/rest/recoleccionVehicular/registrar";
	var urlActualizarRecoleccionVehiculo = $scope.urlRest
			+ "/rest/recoleccionVehicular/actualizar";
	var urlListarRecoleccionVehiculo = $scope.urlRest
			+ "/rest/recoleccionVehicular/listarVehiculo";

	var urlRegistrarRecoleccionVehiculo2 = $scope.urlRest
			+ "/rest/recoleccionVehicular2/registrar";
	var urlActualizarRecoleccionVehiculo2 = $scope.urlRest
			+ "/rest/recoleccionVehicular2/actualizar";
	var urlListarRecoleccionVehiculo2 = $scope.urlRest
			+ "/rest/recoleccionVehicular2/listarVehiculo";

	var urlRegistrarRecoleccionVehiculo3 = $scope.urlRest
			+ "/rest/recoleccionVehicular3/registrar";
	var urlActualizarRecoleccionVehiculo3 = $scope.urlRest
			+ "/rest/recoleccionVehicular3/actualizar";
	var urlListarRecoleccionVehiculo3 = $scope.urlRest
			+ "/rest/recoleccionVehicular3/listarVehiculo";

	var urlRegistrarCombustible = $scope.urlRest
			+ "/rest/combustible/registrar";
	var urlActualizarCombustible = $scope.urlRest
			+ "/rest/combustible/actualizar";
	var urlListarCombustible = $scope.urlRest
			+ "/rest/combustible/listarCombustible";

	var urlRegistrarCombustible2 = $scope.urlRest
			+ "/rest/combustible2/registrar";
	var urlActualizarCombustible2 = $scope.urlRest
			+ "/rest/combustible2/actualizar";
	var urlListarCombustible2 = $scope.urlRest
			+ "/rest/combustible2/listarCombustible";

	var urlRegistrarCombustible3 = $scope.urlRest
			+ "/rest/combustible3/registrar";
	var urlActualizarCombustible3 = $scope.urlRest
			+ "/rest/combustible3/actualizar";
	var urlListarCombustible3 = $scope.urlRest
			+ "/rest/combustible3/listarCombustible";

	var urlRegistrarOtroMunicipales = $scope.urlRest
			+ "/rest/otroMunicipales/registrar";
	var urlActualizarOtroMunicipales = $scope.urlRest
			+ "/rest/otroMunicipales/actualizar";
	var urlListarOtroMunicipales = $scope.urlRest
			+ "/rest/otroMunicipales/obtener";

	var urlRegistrarCantidadResiduos = $scope.urlRest
			+ "/rest/cantidadResiduos/registrar";
	var urlActualizarCantidadResiduos = $scope.urlRest
			+ "/rest/cantidadResiduos/actualizar";
	var urlListarCantidadResiduos = $scope.urlRest
			+ "/rest/cantidadResiduos/listarCantidadResiduos";

	var urlRegistrarOtrosRcdOm = $scope.urlRest + "/rest/otrosRcdOm/registrar";
	var urlActualizarOtrosRcdOm = $scope.urlRest
			+ "/rest/otrosRcdOm/actualizar";
	var urlListarOtrosRcdOm = $scope.urlRest
			+ "/rest/otrosRcdOm/listarOtrosRcdOm";

	var urlRegistrarVehiculo = $scope.urlRest
			+ "/rest/vehiculoConvencional/registrar";
	var urlActualizarVehiculo = $scope.urlRest
			+ "/rest/vehiculoConvencional/actualizar";
	var urlListarVehiculo = $scope.urlRest
			+ "/rest/vehiculoConvencional/listarVehiculoConvencional";

	var urlRegistrarVehiculo2 = $scope.urlRest
			+ "/rest/vehiculoConvencional2/registrar";
	var urlActualizarVehiculo2 = $scope.urlRest
			+ "/rest/vehiculoConvencional2/actualizar";
	var urlListarVehiculo2 = $scope.urlRest
			+ "/rest/vehiculoConvencional2/listarVehiculoConvencional";

	var urlRegistrarVehiculo3 = $scope.urlRest
			+ "/rest/vehiculoConvencional3/registrar";
	var urlActualizarVehiculo3 = $scope.urlRest
			+ "/rest/vehiculoConvencional3/actualizar";
	var urlListarVehiculo3 = $scope.urlRest
			+ "/rest/vehiculoConvencional3/listarVehiculoConvencional";

	var urlRegistrarTrabajadores = $scope.urlRest
			+ "/rest/trabajadoresRecoleccion/registrar";
	var urlActualizarTrabajadores = $scope.urlRest
			+ "/rest/trabajadoresRecoleccion/actualizar";
	var urlListarTrabajadores = $scope.urlRest
			+ "/rest/trabajadoresRecoleccion/listarTrabajadores";

	$scope.recoleccionvehiculos = {};
	$scope.recoleccionvehiculos2 = {};
	$scope.recoleccionvehiculos3 = {};

	$scope.combustible = {};
	$scope.combustible2 = {};
	$scope.combustible3 = {};

	$scope.recoleccion_df = {};
	$scope.recoleccion_val = {};
	$scope.recoleccion_rcd = {};
	$scope.listaDf = [];
	$scope.listaVal = [];
	$scope.listaRCD = [];

	$scope.lstVehiculo = [];
	$scope.lstVehiculo2 = [];
	$scope.lstVehiculo3 = [];

	$scope.lstCombustible = [];
	$scope.lstCombustible2 = [];
	$scope.lstCombustible3 = [];

	$scope.otro_municipales = {};
	$scope.lstOtroMunicipales = [];

	$scope.cantidadResiduos = {};
	$scope.lstCantidadResiduos = [];

	$scope.otrosRcdOm = {};
	$scope.lstOtrosRcdOm = [];

	$scope.vehiculoConvencional = {};
	$scope.lstVehiculoConvencional = [];

	$scope.vehiculoConvencional2 = {};
	$scope.lstVehiculoConvencional2 = [];

	$scope.vehiculoConvencional3 = {};
	$scope.lstVehiculoConvencional3 = [];

	$scope.lstVehiculoAux = [];
	$scope.lstVehiculoAux2 = [];
	$scope.lstVehiculoAux3 = [];

	$scope.vehic = {}
	$scope.lstVehic = [];
	$scope.comb = {};
	$scope.lstComb = [];
	$scope.recogidos = {};
	$scope.lstRecogidos = [];

	$scope.vehic2 = {}
	$scope.lstVehic2 = [];
	$scope.comb2 = {};
	$scope.lstComb2 = [];
	$scope.recogidos2 = {};
	$scope.lstRecogidos2 = [];

	$scope.vehic3 = {}
	$scope.lstVehic3 = [];
	$scope.comb3 = {};
	$scope.lstComb3 = [];
	$scope.recogidos3 = {};
	$scope.lstRecogidos3 = [];

	$scope.persona = {};

	$scope.trabajadores = {};
	$scope.lstTrabajador = [];
	$scope.lstTrabajadorAux = [];
	$scope.lstTrabajadorAux2 = [];
	$scope.lstTrabajadorAux3 = [];

	$scope.cantidadVehiculo = 0;
	$scope.cantidadVehiculo2 = 0;
	$scope.cantidadVehiculo3 = 0;

	$scope.flag = 0;
	$scope.flag2 = 0;
	$scope.flag3 = 0;

	$scope.recoleccion_df.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.recoleccion_df.cod_usuario = '12';

	$scope.recoleccion_val.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.recoleccion_val.cod_usuario = '12';

	$scope.recoleccion_rcd.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.recoleccion_rcd.cod_usuario = '12';

	$scope.otro_municipales.cod_usuario = '12';
	$scope.cantidadResiduos.cod_usuario = '12';

	$localStorage.RECOLECCION = '0';
	$localStorage.FLAG_RECOLECCION = 0;
	$scope.totalRecoleccion = 0;

	$scope.getTotalRecolectado = function() {
		
		var cantidad_residuos;
		
		if($scope.recoleccion_df.cantidad_residuos == null)
			cantidad_residuos = 0; 
		else
			cantidad_residuos = $scope.recoleccion_df.cantidad_residuos;
		
		$scope.barrido = $scope.recoleccion_df.total_rec_barrido * 365 * 0.2 / 1000;//$localStorage.totalBarrido
		$scope.almacenamiento = $scope.recoleccion_df.total_rec_almacenamiento * 0.2;//$localStorage.totalAlmacenamiento
			
		$scope.totalRecoleccion = $scope.barrido + $scope.almacenamiento
				+ cantidad_residuos;

		return $scope.totalRecoleccion;
	}

	$scope.guardar = function() {
		if ($scope.flag == 0) {
			$scope.grabarRecoleccion_df();
			$scope.grabarRecoleccion_val();
			$scope.grabarRecoleccion_rcd();
		} else {
			$scope.actualizarRecoleccion_df();
			$scope.actualizarRecoleccion_val();
			$scope.actualizarRecoleccion_rcd();
		}
	}
	
	$scope.guardar1 = function() {
		if ($scope.flag == 0) {
			$scope.grabarRecoleccion_df();
		} else {
			$scope.actualizarRecoleccion_df();
		}
	}

	$scope.guardar2 = function() {
		if ($scope.flag2 == 0) {
			$scope.grabarRecoleccion_val();
		} else {
			$scope.actualizarRecoleccion_val();
		}
	}
	
	$scope.guardar3 = function() {
		if ($scope.flag3 == 0) {
			$scope.grabarRecoleccion_rcd();
		} else {
			$scope.actualizarRecoleccion_rcd();
		}
	}
	
	$scope.grabarRecoleccion_df = function() {

		var paramJson = {
			RecoleccionDf : $scope.recoleccion_df
		};

		$http({
			method : 'POST',
			url : urlRegistrarRecoleccionDf,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

								$scope.recoleccionvehiculos.rec_disposicion_final_id = data.datosAdicionales.id;
								$scope.vehiculoConvencional.rec_disposicion_final_id = data.datosAdicionales.id;
								// $scope.combustible.rec_disposicion_final_id =
								// data.datosAdicionales.id;
								$scope.grabarVehiculoConvencional();
								$scope.grabarVehiculos();
								// $scope.grabarCombustible();

								//$location.path("/gestion");
								$localStorage.RECOLECCION = '1';
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarRecoleccion_df = function() {

		var paramJson = {
			RecoleccionDf : $scope.recoleccion_df
		};

		$http({
			method : 'POST',
			url : urlActualizarRecoleccionDf,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

								$scope.recoleccionvehiculos.rec_disposicion_final_id = data.datosAdicionales.id;
								$scope.vehiculoConvencional.rec_disposicion_final_id = data.datosAdicionales.id;
								// $scope.combustible.rec_disposicion_final_id =
								// data.datosAdicionales.id;
								$scope.actualizarVehiculos();
								$scope.actualizarVehiculoConvencional();
								// $scope.actualizarCombustible();

							} else {
								alertService
										.showDanger("No se realizó el registro1.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		//$location.path("/gestion");
	}

	$scope.listarRecoleccion_df = function() {

		var paramJson = {
			RecoleccionDf : $scope.recoleccion_df
		};

		$http({
			method : 'POST',
			url : urlListarRecoleccionDf,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {

							$scope.recoleccion_df = data.datosAdicionales.recoleccion_df;

							if ($scope.recoleccion_df.rec_disposicion_final_id != undefined) {
								$scope.flag = 1;
								$scope.recoleccionvehiculos.rec_disposicion_final_id = $scope.recoleccion_df.rec_disposicion_final_id;
								$scope.vehiculoConvencional.rec_disposicion_final_id = $scope.recoleccion_df.rec_disposicion_final_id;
								// $scope.combustible.rec_disposicion_final_id =
								// $scope.recoleccion_df.rec_disposicion_final_id;
							}
							$scope.recoleccion_df.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.recoleccion_df.cod_usuario = '12';
							$scope.actTablaVehiculos();
							$scope.actTablaVehiculoConvencional();
							// $scope.actTablaCombustible();

						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.grabarRecoleccion_val = function() {

		var paramJson = {
			RecoleccionVal : $scope.recoleccion_val
		};

		$http({
			method : 'POST',
			url : urlRegistrarRecoleccionVal,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

								$scope.recoleccionvehiculos2.rec_valorizacion_id = data.datosAdicionales.id;
								$scope.vehiculoConvencional2.rec_valorizacion_id = data.datosAdicionales.id;
								// $scope.combustible.rec_disposicion_final_id =
								// data.datosAdicionales.id;
								$scope.grabarVehiculoConvencional2();
								$scope.grabarVehiculos2();
								// $scope.grabarCombustible2();
								//$location.path("/gestion");
								$localStorage.RECOLECCION = '1';
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarRecoleccion_val = function() {

		var paramJson = {
			RecoleccionVal : $scope.recoleccion_val
		};

		$http({
			method : 'POST',
			url : urlActualizarRecoleccionVal,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

								$scope.recoleccionvehiculos2.rec_valorizacion_id = data.datosAdicionales.id;
								$scope.vehiculoConvencional2.rec_valorizacion_id = data.datosAdicionales.id;
								// $scope.combustible2.rec_valorizacion_id =
								// data.datosAdicionales.id;
								$scope.actualizarVehiculos2();
								// $scope.actualizarCombustible2();
								$scope.actualizarVehiculoConvencional2();

							} else {
								alertService
										.showDanger("No se realizó el registro1.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		//$location.path("/gestion");
	}

	$scope.listarRecoleccion_val = function() {

		var paramJson = {
			RecoleccionVal : $scope.recoleccion_val
		};

		$http({
			method : 'POST',
			url : urlListarRecoleccionVal,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {

							$scope.recoleccion_val = data.datosAdicionales.recoleccion_val;

							if ($scope.recoleccion_val.rec_valorizacion_id != undefined) {
								$scope.flag2 = 1;
								$scope.recoleccionvehiculos2.rec_valorizacion_id = $scope.recoleccion_val.rec_valorizacion_id;
								$scope.vehiculoConvencional2.rec_valorizacion_id = $scope.recoleccion_val.rec_valorizacion_id;
								// $scope.combustible2.rec_valorizacion_id =
								// $scope.recoleccion_val.rec_valorizacion_id;
							}
							$scope.recoleccion_val.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.recoleccion_val.cod_usuario = '12';
							$scope.actTablaVehiculos2();
							$scope.actTablaVehiculoConvencional2();
							// $scope.actTablaCombustible2();
						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.grabarRecoleccion_rcd = function() {

		var paramJson = {
			RecoleccionRcd : $scope.recoleccion_rcd
		};

		$http({
			method : 'POST',
			url : urlRegistrarRecoleccionRcd,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

								$scope.recoleccionvehiculos3.rec_rcd_om_id = data.datosAdicionales.id;
								// $scope.combustible3.rec_rcd_om_id =
								// data.datosAdicionales.id;
								// $scope.cantidadResiduos.rec_rcd_om_id =
								// data.datosAdicionales.id;
								$scope.vehiculoConvencional3.rec_rcd_om_id = data.datosAdicionales.id;
								$scope.grabarVehiculoConvencional3();
								$scope.otrosRcdOm.rec_rcd_om_id = data.datosAdicionales.id;
								$scope.trabajadores.rec_rcd_om_id = data.datosAdicionales.id;
								$scope.grabarVehiculos3();
								$scope.grabarOtrosRcdOm();
								// $scope.grabarCombustible3();
								// $scope.grabarOtroMunicipales();
								// $scope.grabarCantidadResiduos();
								
								if($scope.lstBarredor.length > 0)
									$scope.grabarTrabajadores();
								
								//$scope.grabarTrabajadores();
								//$location.path("/gestion");
								$localStorage.RECOLECCION = '1';
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarRecoleccion_rcd = function() {

		var paramJson = {
			RecoleccionRcd : $scope.recoleccion_rcd
		};

		$http({
			method : 'POST',
			url : urlActualizarRecoleccionRcd,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

								$scope.recoleccionvehiculos3.rec_rcd_om_id = data.datosAdicionales.id;
								$scope.vehiculoConvencional3.rec_rcd_om_id = data.datosAdicionales.id;
								// $scope.combustible3.rec_rcd_om_id =
								// data.datosAdicionales.id;
								$scope.otro_municipales.rec_rcd_om_id = data.datosAdicionales.id;
								// $scope.cantidadResiduos.rec_rcd_om_id =
								// data.datosAdicionales.id;
								$scope.otrosRcdOm.rec_rcd_om_id = data.datosAdicionales.id;
								$scope.trabajadores.rec_rcd_om_id = data.datosAdicionales.id;
								$scope.actualizarVehiculos3();
								$scope.actualizarOtrosRcdOm();
								$scope.actualizarTrabajadores();
								$scope.actualizarVehiculoConvencional3();
								// $scope.actualizarCombustible3();
								// $scope.actualizarOtroMunicipales();
								// $scope.actualizarCantidadResiduos();

							} else {
								alertService
										.showDanger("No se realizó el registro1.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		//$location.path("/gestion");
	}

	$scope.listarRecoleccion_rcd = function() {

		var paramJson = {
			RecoleccionRcd : $scope.recoleccion_rcd
		};

		$http({
			method : 'POST',
			url : urlListarRecoleccionRcd,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {

							$scope.recoleccion_rcd = data.datosAdicionales.recoleccion_rcd;

							if ($scope.recoleccion_rcd.rec_rcd_om_id != undefined) {
								$scope.flag3 = 1;
								$scope.recoleccionvehiculos3.rec_rcd_om_id = $scope.recoleccion_rcd.rec_rcd_om_id;
								$scope.vehiculoConvencional3.rec_rcd_om_id = $scope.recoleccion_rcd.rec_rcd_om_id;
								// $scope.combustible3.rec_rcd_om_id =
								// $scope.recoleccion_rcd.rec_rcd_om_id;
								// $scope.cantidadResiduos.rec_rcd_om_id =
								// $scope.recoleccion_rcd.rec_rcd_om_id;
								// $scope.otro_municipales.rec_rcd_om_id =
								// $scope.recoleccion_rcd.rec_rcd_om_id;
								$scope.otrosRcdOm.rec_rcd_om_id = $scope.recoleccion_rcd.rec_rcd_om_id;
								$scope.trabajadores.rec_rcd_om_id = $scope.recoleccion_rcd.rec_rcd_om_id;
							}
							$scope.recoleccion_rcd.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.recoleccion_rcd.cod_usuario = '12';
							$scope.actTablaVehiculos3();
							$scope.actTablaOtrosRcdOm();
							$scope.actTablaVehiculoConvencional3();
							// $scope.actTablaCombustible3();
							// $scope.listarOtroMunicipales();
							// $scope.actTablaCantidadResiduos();
							$scope.actTablaTrabajador();
						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.actTablaVehiculos = function() {

		$scope.tblVehiculos = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"vehiculo_recoleccion_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					RecoleccionVehiculos : $scope.recoleccionvehiculos
				};

				$http({
					method : 'POST',
					url : urlListarRecoleccionVehiculo,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstVehiculo;
					$scope.lstVehiculo = data.datosAdicionales.LstVehiculo;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarVehiculos = function() {

		for (var x = 0; x < $scope.lstVehiculo.length; x++) {
			$scope.lstVehiculo[x].rec_disposicion_final_id = $scope.recoleccionvehiculos.rec_disposicion_final_id;
			$scope.lstVehiculo[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarRecoleccionVehiculo,
			data : JSON.stringify($scope.lstVehiculo),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarVehiculos = function() {

		for (var x = 0; x < $scope.lstVehiculo.length; x++) {
			$scope.lstVehiculo[x].rec_disposicion_final_id = $scope.recoleccionvehiculos.rec_disposicion_final_id;
			$scope.lstVehiculo[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarRecoleccionVehiculo,
			data : JSON.stringify($scope.lstVehiculo),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaCombustible = function() {

		$scope.tblCombustible = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"combustible_vehiculo_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							Combustible : $scope.combustible
						};

						$http({
							method : 'POST',
							url : urlListarCombustible,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstCombustible;
											$scope.lstCombustible = data.datosAdicionales.LstCombustible;
											blockUI.start();
											params.total(nData.length);

											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.grabarCombustible = function() {

		for (var x = 0; x < $scope.lstCombustible.length; x++) {
			$scope.lstCombustible[x].rec_disposicion_final_id = $scope.combustible.rec_disposicion_final_id;
			$scope.lstCombustible[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCombustible,
			data : JSON.stringify($scope.lstCombustible),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarCombustible = function() {

		for (var x = 0; x < $scope.lstCombustible.length; x++) {
			$scope.lstCombustible[x].rec_disposicion_final_id = $scope.combustible.rec_disposicion_final_id;
			$scope.lstCombustible[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarCombustible,
			data : JSON.stringify($scope.lstCombustible),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaVehiculos2 = function() {

		$scope.tblVehiculos2 = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"vehiculo_recoleccion_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					RecoleccionVehiculos2 : $scope.recoleccionvehiculos2
				};

				$http({
					method : 'POST',
					url : urlListarRecoleccionVehiculo2,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstVehiculo2;
					$scope.lstVehiculo2 = data.datosAdicionales.LstVehiculo2;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarVehiculos2 = function() {

		for (var x = 0; x < $scope.lstVehiculo2.length; x++) {
			$scope.lstVehiculo2[x].rec_valorizacion_id = $scope.recoleccionvehiculos2.rec_valorizacion_id;
			$scope.lstVehiculo2[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarRecoleccionVehiculo2,
			data : JSON.stringify($scope.lstVehiculo2),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarVehiculos2 = function() {

		for (var x = 0; x < $scope.lstVehiculo2.length; x++) {
			$scope.lstVehiculo2[x].rec_valorizacion_id = $scope.recoleccionvehiculos2.rec_valorizacion_id;
			$scope.lstVehiculo2[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarRecoleccionVehiculo2,
			data : JSON.stringify($scope.lstVehiculo2),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaCombustible2 = function() {

		$scope.tblCombustible2 = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"combustible_vehiculo_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							Combustible2 : $scope.combustible2
						};

						$http({
							method : 'POST',
							url : urlListarCombustible2,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstCombustible2;
											$scope.lstCombustible2 = data.datosAdicionales.LstCombustible2;
											blockUI.start();
											params.total(nData.length);
											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.grabarCombustible2 = function() {

		for (var x = 0; x < $scope.lstCombustible.length; x++) {
			$scope.lstCombustible2[x].rec_valorizacion_id = $scope.combustible2.rec_valorizacion_id;
			$scope.lstCombustible2[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCombustible2,
			data : JSON.stringify($scope.lstCombustible2),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarCombustible2 = function() {

		for (var x = 0; x < $scope.lstCombustible2.length; x++) {
			$scope.lstCombustible2[x].rec_valorizacion_id = $scope.combustible2.rec_valorizacion_id;
			$scope.lstCombustible2[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarCombustible2,
			data : JSON.stringify($scope.lstCombustible2),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaVehiculos3 = function() {

		$scope.tblVehiculos3 = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"vehiculo_recoleccion_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					RecoleccionVehiculos3 : $scope.recoleccionvehiculos3
				};

				$http({
					method : 'POST',
					url : urlListarRecoleccionVehiculo3,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstVehiculo3;
					$scope.lstVehiculo3 = data.datosAdicionales.LstVehiculo3;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarVehiculos3 = function() {

		for (var x = 0; x < $scope.lstVehiculo3.length; x++) {
			$scope.lstVehiculo3[x].rec_rcd_om_id = $scope.recoleccionvehiculos3.rec_rcd_om_id;
			$scope.lstVehiculo3[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarRecoleccionVehiculo3,
			data : JSON.stringify($scope.lstVehiculo3),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarVehiculos3 = function() {

		for (var x = 0; x < $scope.lstVehiculo3.length; x++) {
			$scope.lstVehiculo3[x].rec_rcd_om_id = $scope.recoleccionvehiculos3.rec_rcd_om_id;
			$scope.lstVehiculo3[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarRecoleccionVehiculo3,
			data : JSON.stringify($scope.lstVehiculo3),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaCombustible3 = function() {

		$scope.tblCombustible3 = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"combustible_vehiculo_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							Combustible3 : $scope.combustible3
						};

						$http({
							method : 'POST',
							url : urlListarCombustible3,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstCombustible3;
											$scope.lstCombustible3 = data.datosAdicionales.LstCombustible3;
											blockUI.start();
											params.total(nData.length);
											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.grabarCombustible3 = function() {

		for (var x = 0; x < $scope.lstCombustible.length; x++) {
			$scope.lstCombustible3[x].rec_rcd_om_id = $scope.combustible3.rec_rcd_om_id;
			$scope.lstCombustible3[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCombustible3,
			data : JSON.stringify($scope.lstCombustible3),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarCombustible3 = function() {

		for (var x = 0; x < $scope.lstCombustible.length; x++) {
			$scope.lstCombustible3[x].rec_rcd_om_id = $scope.combustible3.rec_rcd_om_id;
			$scope.lstCombustible3[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarCombustible3,
			data : JSON.stringify($scope.lstCombustible3),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.grabarOtroMunicipales = function() {

		var paramJson = {
			OtroMunicipales : $scope.otro_municipales
		};

		$http({
			method : 'POST',
			url : urlRegistrarOtroMunicipales,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				//$location.path("/gestion");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarOtroMunicipales = function() {

		var paramJson = {
			OtroMunicipales : $scope.otro_municipales
		};

		$http({
			method : 'POST',
			url : urlActualizarOtroMunicipales,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro1.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

		//$location.path("/gestion");
	}

	$scope.listarOtroMunicipales = function() {

		var paramJson = {
			OtroMunicipales : $scope.otro_municipales
		};

		$http({
			method : 'POST',
			url : urlListarOtroMunicipales,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {

			$scope.otro_municipales = data.datosAdicionales.otroMunicipales;
			$scope.otro_municipales.cod_usuario = '12';
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.actTablaCantidadResiduos = function() {

		$scope.tblCantidadResiduos = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"cantidad_residuos_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							CantidadResiduos : $scope.cantidadResiduos
						};

						$http({
							method : 'POST',
							url : urlListarCantidadResiduos,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstCantidadResiduos;
											$scope.lstCantidadResiduos = data.datosAdicionales.LstCantidadResiduos;
											blockUI.start();
											params.total(nData.length);
											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.grabarCantidadResiduos = function() {

		for (var x = 0; x < $scope.lstCantidadResiduos.length; x++) {
			$scope.lstCantidadResiduos[x].rec_rcd_om_id = $scope.cantidadResiduos.rec_rcd_om_id;
			$scope.lstCantidadResiduos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCantidadResiduos,
			data : JSON.stringify($scope.lstCantidadResiduos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarCantidadResiduos = function() {

		for (var x = 0; x < $scope.lstCantidadResiduos.length; x++) {
			$scope.lstCantidadResiduos[x].rec_rcd_om_id = $scope.cantidadResiduos.rec_rcd_om_id;
			$scope.lstCantidadResiduos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarCantidadResiduos,
			data : JSON.stringify($scope.lstCantidadResiduos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaOtrosRcdOm = function() {

		$scope.tblOtrosRcdOm = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"otrosRcdOm_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					OtrosRcdOm : $scope.otrosRcdOm
				};

				$http({
					method : 'POST',
					url : urlListarOtrosRcdOm,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstOtrosRcdOm;
					$scope.lstOtrosRcdOm = data.datosAdicionales.LstOtrosRcdOm;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarOtrosRcdOm = function() {

		for (var x = 0; x < $scope.lstOtrosRcdOm.length; x++) {
			$scope.lstOtrosRcdOm[x].rec_rcd_om_id = $scope.otrosRcdOm.rec_rcd_om_id;
			$scope.lstOtrosRcdOm[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarOtrosRcdOm,
			data : JSON.stringify($scope.lstOtrosRcdOm),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarOtrosRcdOm = function() {

		for (var x = 0; x < $scope.lstOtrosRcdOm.length; x++) {
			$scope.lstOtrosRcdOm[x].rec_rcd_om_id = $scope.otrosRcdOm.rec_rcd_om_id;
			$scope.lstOtrosRcdOm[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarOtrosRcdOm,
			data : JSON.stringify($scope.lstOtrosRcdOm),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaVehiculoConvencional = function() {

		$scope.tblVehiculoConvencional = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"vehiculo_convencional_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							VehiculoConvencional : $scope.vehiculoConvencional
						};

						$http({
							method : 'POST',
							url : urlListarVehiculo,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstVehiculoConvencional;
											$scope.lstVehiculoConvencional = data.datosAdicionales.LstVehiculoConvencional;
											// $scope.lstComb =
											// data.datosAdicionales.LstVehiculoConvencional;
											// $scope.lstRecogidos =
											// data.datosAdicionales.LstVehiculoConvencional;

											$scope.agregarComb();
											$scope.agregarRecogidos();

											for (var x = 0; x < $scope.lstVehiculoConvencional.length; x++) {

												$scope.lstComb = $scope.lstCombAux;
												$scope.lstRecogidos = $scope.lstRecogidosAux;

												$scope.lstVehiculoConvencional[x].recorrido = $scope.lstVehiculoConvencional[x].recorrido_anual;
												$scope.lstVehiculoConvencional[x].fabricacion = $scope.lstVehiculoConvencional[x].anio_fabricacion;
												$scope.lstComb[x].placa = $scope.lstVehiculoConvencional[x].placa;
												$scope.lstComb[x].tipo = $scope.lstVehiculoConvencional[x].tipo_combustible;
												$scope.lstComb[x].cantidad = $scope.lstVehiculoConvencional[x].cantidad_combustible;
												$scope.lstComb[x].costo = $scope.lstVehiculoConvencional[x].costo_anual;
												$scope.lstComb[x].rendimiento = $scope.lstVehiculoConvencional[x].rendimiento_anual;
												$scope.lstRecogidos[x].placa = $scope.lstVehiculoConvencional[x].placa;
												$scope.lstRecogidos[x].capacidad = $scope.lstVehiculoConvencional[x].capacidad_viaje;
												$scope.lstRecogidos[x].efectividad = $scope.lstVehiculoConvencional[x].efectividad;
												$scope.lstRecogidos[x].pcv = $scope.lstVehiculoConvencional[x].promedio_viajes;
												$scope.lstRecogidos[x].efectividad = $scope.lstVehiculoConvencional[x].efectividad;
												$scope.lstRecogidos[x].pvd = $scope.lstVehiculoConvencional[x].promedio_turno;
												$scope.lstRecogidos[x].trabajados = $scope.lstVehiculoConvencional[x].dias_trabajados;
												$scope.lstRecogidos[x].rec_media = $scope.lstVehiculoConvencional[x].recoleccion_media;
												$scope.lstRecogidos[x].rec_anual = $scope.lstVehiculoConvencional[x].recoleccion_anual;

											}

											$scope.cantidadVehiculo = $scope.lstVehiculoConvencional.length;
											console.log("Cantidad Vehiculo: "
													+ $scope.cantidadVehiculo);

											blockUI.start();
											params.total(nData.length);
											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.grabarVehiculoConvencional = function() {

		console.log("RDF: "
				+ $scope.vehiculoConvencional.rec_disposicion_final_id);

		for (var x = 0; x < $scope.lstVehiculoConvencional.length; x++) {
			$scope.lstVehiculoConvencional[x].rec_disposicion_final_id = $scope.vehiculoConvencional.rec_disposicion_final_id;

			$scope.lstVehiculoConvencional[x].placa = $scope.lstVehiculoConvencional[x].placa;
			$scope.lstVehiculoConvencional[x].recorrido_anual = $scope.lstVehiculoConvencional[x].recorrido;
			$scope.lstVehiculoConvencional[x].tipo_vehiculo = $scope.lstVehiculoConvencional[x].tipo_vehiculo;
			$scope.lstVehiculoConvencional[x].anio_fabricacion = $scope.lstVehiculoConvencional[x].fabricacion;
			$scope.lstVehiculoConvencional[x].capacidad = $scope.lstVehiculoConvencional[x].capacidad;
			$scope.lstVehiculoConvencional[x].tipo_combustible = $scope.lstComb[x].tipo;
			$scope.lstVehiculoConvencional[x].cantidad_combustible = $scope.lstComb[x].cantidad;
			$scope.lstVehiculoConvencional[x].costo_anual = $scope.lstComb[x].costo;
			$scope.lstVehiculoConvencional[x].rendimiento_anual = $scope.lstComb[x].rendimiento;
			$scope.lstVehiculoConvencional[x].capacidad_viaje = $scope.lstRecogidos[x].capacidad;
			$scope.lstVehiculoConvencional[x].efectividad = $scope.lstRecogidos[x].efectividad;
			$scope.lstVehiculoConvencional[x].promedio_viajes = $scope.lstRecogidos[x].pcv;
			$scope.lstVehiculoConvencional[x].promedio_turno = $scope.lstRecogidos[x].pvd;
			$scope.lstVehiculoConvencional[x].dias_trabajados = $scope.lstRecogidos[x].trabajados;
			$scope.lstVehiculoConvencional[x].recoleccion_media = $scope.lstRecogidos[x].rec_media;
			$scope.lstVehiculoConvencional[x].recoleccion_anual = $scope.lstRecogidos[x].rec_anual;

			$scope.lstVehiculoConvencional[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarVehiculo,
			data : JSON.stringify($scope.lstVehiculoConvencional),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarVehiculoConvencional = function() {

		var i = $scope.cantidadVehiculo;
		var c = 0;

		for (var x = i; x < $scope.lstVehiculoConvencional.length; x++) {
			$scope.lstVehiculoConvencional[x].rec_disposicion_final_id = $scope.vehiculoConvencional.rec_disposicion_final_id;

			$scope.lstVehiculoConvencional[x].placa = $scope.lstVehiculoConvencional[x].placa;
			$scope.lstVehiculoConvencional[x].recorrido_anual = $scope.lstVehiculoConvencional[x].recorrido;
			$scope.lstVehiculoConvencional[x].tipo_vehiculo = $scope.lstVehiculoConvencional[x].tipo_vehiculo;
			$scope.lstVehiculoConvencional[x].anio_fabricacion = $scope.lstVehiculoConvencional[x].fabricacion;
			$scope.lstVehiculoConvencional[x].capacidad = $scope.lstVehiculoConvencional[x].capacidad;
			$scope.lstVehiculoConvencional[x].tipo_combustible = $scope.lstComb[x].tipo;
			$scope.lstVehiculoConvencional[x].cantidad_combustible = $scope.lstComb[x].cantidad;
			$scope.lstVehiculoConvencional[x].costo_anual = $scope.lstComb[x].costo;
			$scope.lstVehiculoConvencional[x].rendimiento_anual = $scope.lstComb[x].rendimiento;
			$scope.lstVehiculoConvencional[x].capacidad_viaje = $scope.lstRecogidos[x].capacidad;
			$scope.lstVehiculoConvencional[x].efectividad = $scope.lstRecogidos[x].efectividad;
			$scope.lstVehiculoConvencional[x].promedio_viajes = $scope.lstRecogidos[x].pcv;
			$scope.lstVehiculoConvencional[x].promedio_turno = $scope.lstRecogidos[x].pvd;
			$scope.lstVehiculoConvencional[x].dias_trabajados = $scope.lstRecogidos[x].trabajados;
			$scope.lstVehiculoConvencional[x].recoleccion_media = $scope.lstRecogidos[x].rec_media;
			$scope.lstVehiculoConvencional[x].recoleccion_anual = $scope.lstRecogidos[x].rec_anual;

			$scope.lstVehiculoConvencional[x].cod_usuario = '14';

			$scope.lstVehiculoAux[c].rec_disposicion_final_id = $scope.lstVehiculoConvencional[x].rec_disposicion_final_id;
			$scope.lstVehiculoAux[c].placa = $scope.lstVehiculoConvencional[x].placa;
			$scope.lstVehiculoAux[c].recorrido_anual = $scope.lstVehiculoConvencional[x].recorrido_anual;
			$scope.lstVehiculoAux[c].tipo_vehiculo = $scope.lstVehiculoConvencional[x].tipo_vehiculo;
			$scope.lstVehiculoAux[c].anio_fabricacion = $scope.lstVehiculoConvencional[x].anio_fabricacion;
			$scope.lstVehiculoAux[c].capacidad = $scope.lstVehiculoConvencional[x].capacidad;
			$scope.lstVehiculoAux[c].tipo_combustible = $scope.lstVehiculoConvencional[x].tipo_combustible;
			$scope.lstVehiculoAux[c].cantidad_combustible = $scope.lstVehiculoConvencional[x].cantidad_combustible;
			$scope.lstVehiculoAux[c].costo_anual = $scope.lstVehiculoConvencional[x].costo_anual;
			$scope.lstVehiculoAux[c].rendimiento_anual = $scope.lstVehiculoConvencional[x].rendimiento_anual;
			$scope.lstVehiculoAux[c].capacidad_viaje = $scope.lstVehiculoConvencional[x].capacidad_viaje;
			$scope.lstVehiculoAux[c].efectividad = $scope.lstVehiculoConvencional[x].efectividad;
			$scope.lstVehiculoAux[c].promedio_viajes = $scope.lstVehiculoConvencional[x].promedio_viajes;
			$scope.lstVehiculoAux[c].promedio_turno = $scope.lstVehiculoConvencional[x].promedio_turno;
			$scope.lstVehiculoAux[c].dias_trabajados = $scope.lstVehiculoConvencional[x].dias_trabajados;
			$scope.lstVehiculoAux[c].recoleccion_media = $scope.lstVehiculoConvencional[x].recoleccion_media;
			$scope.lstVehiculoAux[c].recoleccion_anual = $scope.lstVehiculoConvencional[x].recoleccion_anual;
			$scope.lstVehiculoAux[c].cod_usuario = $scope.lstVehiculoConvencional[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarVehiculo,
			data : JSON.stringify($scope.lstVehiculoAux),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaVehiculoConvencional2 = function() {

		$scope.tblVehiculoConvencional2 = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"vehiculo_convencional_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							VehiculoConvencional2 : $scope.vehiculoConvencional2
						};

						$http({
							method : 'POST',
							url : urlListarVehiculo2,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstVehiculoConvencional2;
											$scope.lstVehiculoConvencional2 = data.datosAdicionales.LstVehiculoConvencional2;
											$scope.agregarComb2();
											$scope.agregarRecogidos2();

											for (var x = 0; x < $scope.lstVehiculoConvencional2.length; x++) {

												$scope.lstComb2 = $scope.lstCombAux2;
												$scope.lstRecogidos2 = $scope.lstRecogidosAux2;

												$scope.lstVehiculoConvencional2[x].recorrido = $scope.lstVehiculoConvencional2[x].recorrido_anual;
												$scope.lstVehiculoConvencional2[x].fabricacion = $scope.lstVehiculoConvencional2[x].anio_fabricacion;
												$scope.lstComb2[x].placa = $scope.lstVehiculoConvencional2[x].placa;
												$scope.lstComb2[x].tipo = $scope.lstVehiculoConvencional2[x].tipo_combustible;
												$scope.lstComb2[x].cantidad = $scope.lstVehiculoConvencional2[x].cantidad_combustible;
												$scope.lstComb2[x].costo = $scope.lstVehiculoConvencional2[x].costo_anual;
												$scope.lstComb2[x].rendimiento = $scope.lstVehiculoConvencional2[x].rendimiento_anual;
												$scope.lstRecogidos2[x].placa = $scope.lstVehiculoConvencional2[x].placa;
												$scope.lstRecogidos2[x].capacidad = $scope.lstVehiculoConvencional2[x].capacidad_viaje;
												$scope.lstRecogidos2[x].efectividad = $scope.lstVehiculoConvencional2[x].efectividad;
												$scope.lstRecogidos2[x].pcv = $scope.lstVehiculoConvencional2[x].promedio_viajes;
												$scope.lstRecogidos2[x].efectividad = $scope.lstVehiculoConvencional2[x].efectividad;
												$scope.lstRecogidos2[x].pvd = $scope.lstVehiculoConvencional2[x].promedio_turno;
												$scope.lstRecogidos2[x].trabajados = $scope.lstVehiculoConvencional2[x].dias_trabajados;
												$scope.lstRecogidos2[x].rec_media = $scope.lstVehiculoConvencional2[x].recoleccion_media;
												$scope.lstRecogidos2[x].rec_anual = $scope.lstVehiculoConvencional2[x].recoleccion_anual;

											}

											$scope.cantidadVehiculo2 = $scope.lstVehiculoConvencional2.length;
											console.log("Cantidad Vehiculo2: "
													+ $scope.cantidadVehiculo2);

											blockUI.start();
											params.total(nData.length);
											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.grabarVehiculoConvencional2 = function() {

		console.log("RV: " + $scope.vehiculoConvencional2.rec_valorizacion_id);

		for (var x = 0; x < $scope.lstVehiculoConvencional2.length; x++) {
			$scope.lstVehiculoConvencional2[x].rec_valorizacion_id = $scope.vehiculoConvencional2.rec_valorizacion_id;

			$scope.lstVehiculoConvencional2[x].placa = $scope.lstVehiculoConvencional2[x].placa;
			$scope.lstVehiculoConvencional2[x].recorrido_anual = $scope.lstVehiculoConvencional2[x].recorrido;
			$scope.lstVehiculoConvencional2[x].tipo_vehiculo = $scope.lstVehiculoConvencional2[x].tipo_vehiculo;
			$scope.lstVehiculoConvencional2[x].anio_fabricacion = $scope.lstVehiculoConvencional2[x].fabricacion;
			$scope.lstVehiculoConvencional2[x].capacidad = $scope.lstVehiculoConvencional2[x].capacidad;
			$scope.lstVehiculoConvencional2[x].tipo_combustible = $scope.lstComb2[x].tipo;
			$scope.lstVehiculoConvencional2[x].cantidad_combustible = $scope.lstComb2[x].cantidad;
			$scope.lstVehiculoConvencional2[x].costo_anual = $scope.lstComb2[x].costo;
			$scope.lstVehiculoConvencional2[x].rendimiento_anual = $scope.lstComb2[x].rendimiento;
			$scope.lstVehiculoConvencional2[x].capacidad_viaje = $scope.lstRecogidos2[x].capacidad;
			$scope.lstVehiculoConvencional2[x].efectividad = $scope.lstRecogidos2[x].efectividad;
			$scope.lstVehiculoConvencional2[x].promedio_viajes = $scope.lstRecogidos2[x].pcv;
			$scope.lstVehiculoConvencional2[x].promedio_turno = $scope.lstRecogidos2[x].pvd;
			$scope.lstVehiculoConvencional2[x].dias_trabajados = $scope.lstRecogidos2[x].trabajados;
			$scope.lstVehiculoConvencional2[x].recoleccion_media = $scope.lstRecogidos2[x].rec_media;
			$scope.lstVehiculoConvencional2[x].recoleccion_anual = $scope.lstRecogidos2[x].rec_anual;

			$scope.lstVehiculoConvencional2[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarVehiculo2,
			data : JSON.stringify($scope.lstVehiculoConvencional2),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarVehiculoConvencional2 = function() {

		var i = $scope.cantidadVehiculo2;
		var c = 0;

		for (var x = i; x < $scope.lstVehiculoConvencional2.length; x++) {
			$scope.lstVehiculoConvencional2[x].rec_valorizacion_id = $scope.vehiculoConvencional2.rec_valorizacion_id;

			$scope.lstVehiculoConvencional2[x].placa = $scope.lstVehiculoConvencional2[x].placa;
			$scope.lstVehiculoConvencional2[x].recorrido_anual = $scope.lstVehiculoConvencional2[x].recorrido;
			$scope.lstVehiculoConvencional2[x].tipo_vehiculo = $scope.lstVehiculoConvencional2[x].tipo_vehiculo;
			$scope.lstVehiculoConvencional2[x].anio_fabricacion = $scope.lstVehiculoConvencional2[x].fabricacion;
			$scope.lstVehiculoConvencional2[x].capacidad = $scope.lstVehiculoConvencional2[x].capacidad;
			$scope.lstVehiculoConvencional2[x].tipo_combustible = $scope.lstComb2[x].tipo;
			$scope.lstVehiculoConvencional2[x].cantidad_combustible = $scope.lstComb2[x].cantidad;
			$scope.lstVehiculoConvencional2[x].costo_anual = $scope.lstComb2[x].costo;
			$scope.lstVehiculoConvencional2[x].rendimiento_anual = $scope.lstComb2[x].rendimiento;
			$scope.lstVehiculoConvencional2[x].capacidad_viaje = $scope.lstRecogidos2[x].capacidad;
			$scope.lstVehiculoConvencional2[x].efectividad = $scope.lstRecogidos2[x].efectividad;
			$scope.lstVehiculoConvencional2[x].promedio_viajes = $scope.lstRecogidos2[x].pcv;
			$scope.lstVehiculoConvencional2[x].promedio_turno = $scope.lstRecogidos2[x].pvd;
			$scope.lstVehiculoConvencional2[x].dias_trabajados = $scope.lstRecogidos2[x].trabajados;
			$scope.lstVehiculoConvencional2[x].recoleccion_media = $scope.lstRecogidos2[x].rec_media;
			$scope.lstVehiculoConvencional2[x].recoleccion_anual = $scope.lstRecogidos2[x].rec_anual;

			$scope.lstVehiculoConvencional2[x].cod_usuario = '14';

			$scope.lstVehiculoAux2[c].rec_valorizacion_id = $scope.lstVehiculoConvencional2[x].rec_valorizacion_id;

			$scope.lstVehiculoAux2[c].placa2 = $scope.lstVehiculoConvencional2[x].placa;
			$scope.lstVehiculoAux2[c].recorrido_anual2 = $scope.lstVehiculoConvencional2[x].recorrido_anual;
			$scope.lstVehiculoAux2[c].tpv2 = $scope.lstVehiculoConvencional2[x].tipo_vehiculo;
			$scope.lstVehiculoAux2[c].anio_fabricacion2 = $scope.lstVehiculoConvencional2[x].anio_fabricacion;
			$scope.lstVehiculoAux2[c].capacidad2 = $scope.lstVehiculoConvencional2[x].capacidad;
			$scope.lstVehiculoAux2[c].tipo_combustible2 = $scope.lstVehiculoConvencional2[x].tipo_combustible;
			$scope.lstVehiculoAux2[c].cantidad_combustible2 = $scope.lstVehiculoConvencional2[x].cantidad_combustible;
			$scope.lstVehiculoAux2[c].costo_anual2 = $scope.lstVehiculoConvencional2[x].costo_anual;
			$scope.lstVehiculoAux2[c].rendimiento_anual2 = $scope.lstVehiculoConvencional2[x].rendimiento_anual;
			$scope.lstVehiculoAux2[c].capacidad_viaje2 = $scope.lstVehiculoConvencional2[x].capacidad_viaje;
			$scope.lstVehiculoAux2[c].efectividad2 = $scope.lstVehiculoConvencional2[x].efectividad;
			$scope.lstVehiculoAux2[c].promedio_viajes2 = $scope.lstVehiculoConvencional2[x].promedio_viajes;
			$scope.lstVehiculoAux2[c].promedio_turno2 = $scope.lstVehiculoConvencional2[x].promedio_turno;
			$scope.lstVehiculoAux2[c].dias_trabajados2 = $scope.lstVehiculoConvencional2[x].dias_trabajados;
			$scope.lstVehiculoAux2[c].recoleccion_media2 = $scope.lstVehiculoConvencional2[x].recoleccion_media;
			$scope.lstVehiculoAux2[c].recoleccion_anual2 = $scope.lstVehiculoConvencional2[x].recoleccion_anual;

			$scope.lstVehiculoAux2[c].cod_usuario = $scope.lstVehiculoConvencional2[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarVehiculo2,
			data : JSON.stringify($scope.lstVehiculoAux2),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaVehiculoConvencional3 = function() {

		$scope.tblVehiculoConvencional3 = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"vehiculo_convencional_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							VehiculoConvencional3 : $scope.vehiculoConvencional3
						};

						$http({
							method : 'POST',
							url : urlListarVehiculo3,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstVehiculoConvencional3;
											$scope.lstVehiculoConvencional3 = data.datosAdicionales.LstVehiculoConvencional3;
											$scope.agregarComb3();
											$scope.agregarRecogidos3();

											for (var x = 0; x < $scope.lstVehiculoConvencional3.length; x++) {

												$scope.lstComb3 = $scope.lstCombAux3;
												$scope.lstRecogidos3 = $scope.lstRecogidosAux3;

												$scope.lstVehiculoConvencional3[x].recorrido = $scope.lstVehiculoConvencional3[x].recorrido_anual;
												$scope.lstVehiculoConvencional3[x].fabricacion = $scope.lstVehiculoConvencional3[x].anio_fabricacion;
												$scope.lstComb3[x].placa = $scope.lstVehiculoConvencional3[x].placa;
												$scope.lstComb3[x].tipo = $scope.lstVehiculoConvencional3[x].tipo_combustible;
												$scope.lstComb3[x].cantidad = $scope.lstVehiculoConvencional3[x].cantidad_combustible;
												$scope.lstComb3[x].costo = $scope.lstVehiculoConvencional3[x].costo_anual;
												$scope.lstComb3[x].rendimiento = $scope.lstVehiculoConvencional3[x].rendimiento_anual;
												$scope.lstRecogidos3[x].placa = $scope.lstVehiculoConvencional3[x].placa;
												$scope.lstRecogidos3[x].capacidad = $scope.lstVehiculoConvencional3[x].capacidad_viaje;
												$scope.lstRecogidos3[x].efectividad = $scope.lstVehiculoConvencional3[x].efectividad;
												$scope.lstRecogidos3[x].pcv = $scope.lstVehiculoConvencional3[x].promedio_viajes;
												$scope.lstRecogidos3[x].efectividad = $scope.lstVehiculoConvencional3[x].efectividad;
												$scope.lstRecogidos3[x].pvd = $scope.lstVehiculoConvencional3[x].promedio_turno;
												$scope.lstRecogidos3[x].trabajados = $scope.lstVehiculoConvencional3[x].dias_trabajados;
												$scope.lstRecogidos3[x].rec_media = $scope.lstVehiculoConvencional3[x].recoleccion_media;
												$scope.lstRecogidos3[x].rec_anual = $scope.lstVehiculoConvencional3[x].recoleccion_anual;

											}

											$scope.cantidadVehiculo3 = $scope.lstVehiculoConvencional3.length;
											console.log("Cantidad Vehiculo: "
													+ $scope.cantidadVehiculo3);

											blockUI.start();
											params.total(nData.length);
											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.grabarVehiculoConvencional3 = function() {

		console.log("RRCD: " + $scope.vehiculoConvencional3.rec_rcd_om_id);

		for (var x = 0; x < $scope.lstVehiculoConvencional3.length; x++) {
			$scope.lstVehiculoConvencional3[x].rec_rcd_om_id = $scope.vehiculoConvencional3.rec_rcd_om_id;

			$scope.lstVehiculoConvencional3[x].placa = $scope.lstVehiculoConvencional3[x].placa;
			$scope.lstVehiculoConvencional3[x].recorrido_anual = $scope.lstVehiculoConvencional3[x].recorrido;
			$scope.lstVehiculoConvencional3[x].tipo_vehiculo = $scope.lstVehiculoConvencional3[x].tipo_vehiculo;
			$scope.lstVehiculoConvencional3[x].anio_fabricacion = $scope.lstVehiculoConvencional3[x].fabricacion;
			$scope.lstVehiculoConvencional3[x].capacidad = $scope.lstVehiculoConvencional3[x].capacidad;
			$scope.lstVehiculoConvencional3[x].tipo_combustible = $scope.lstComb3[x].tipo;
			$scope.lstVehiculoConvencional3[x].cantidad_combustible = $scope.lstComb3[x].cantidad;
			$scope.lstVehiculoConvencional3[x].costo_anual = $scope.lstComb3[x].costo;
			$scope.lstVehiculoConvencional3[x].rendimiento_anual = $scope.lstComb3[x].rendimiento;
			$scope.lstVehiculoConvencional3[x].capacidad_viaje = $scope.lstRecogidos3[x].capacidad;
			$scope.lstVehiculoConvencional3[x].efectividad = $scope.lstRecogidos3[x].efectividad;
			$scope.lstVehiculoConvencional3[x].promedio_viajes = $scope.lstRecogidos3[x].pcv;
			$scope.lstVehiculoConvencional3[x].promedio_turno = $scope.lstRecogidos3[x].pvd;
			$scope.lstVehiculoConvencional3[x].dias_trabajados = $scope.lstRecogidos3[x].trabajados;
			$scope.lstVehiculoConvencional3[x].recoleccion_media = $scope.lstRecogidos3[x].rec_media;
			$scope.lstVehiculoConvencional3[x].recoleccion_anual = $scope.lstRecogidos3[x].rec_anual;

			$scope.lstVehiculoConvencional3[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarVehiculo3,
			data : JSON.stringify($scope.lstVehiculoConvencional3),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarVehiculoConvencional3 = function() {

		var i = $scope.cantidadVehiculo3;
		var c = 0;

		for (var x = i; x < $scope.lstVehiculoConvencional3.length; x++) {
			$scope.lstVehiculoConvencional3[x].rec_rcd_om_id = $scope.vehiculoConvencional3.rec_rcd_om_id;

			$scope.lstVehiculoConvencional3[x].placa = $scope.lstVehiculoConvencional3[x].placa;
			$scope.lstVehiculoConvencional3[x].recorrido_anual = $scope.lstVehiculoConvencional3[x].recorrido;
			$scope.lstVehiculoConvencional3[x].tipo_vehiculo = $scope.lstVehiculoConvencional3[x].tipo_vehiculo;
			$scope.lstVehiculoConvencional3[x].anio_fabricacion = $scope.lstVehiculoConvencional3[x].fabricacion;
			$scope.lstVehiculoConvencional3[x].capacidad = $scope.lstVehiculoConvencional3[x].capacidad;
			$scope.lstVehiculoConvencional3[x].tipo_combustible = $scope.lstComb3[x].tipo;
			$scope.lstVehiculoConvencional3[x].cantidad_combustible = $scope.lstComb3[x].cantidad;
			$scope.lstVehiculoConvencional3[x].costo_anual = $scope.lstComb3[x].costo;
			$scope.lstVehiculoConvencional3[x].rendimiento_anual = $scope.lstComb3[x].rendimiento;
			$scope.lstVehiculoConvencional3[x].capacidad_viaje = $scope.lstRecogidos3[x].capacidad;
			$scope.lstVehiculoConvencional3[x].efectividad = $scope.lstRecogidos3[x].efectividad;
			$scope.lstVehiculoConvencional3[x].promedio_viajes = $scope.lstRecogidos3[x].pcv;
			$scope.lstVehiculoConvencional3[x].promedio_turno = $scope.lstRecogidos3[x].pvd;
			$scope.lstVehiculoConvencional3[x].dias_trabajados = $scope.lstRecogidos3[x].trabajados;
			$scope.lstVehiculoConvencional3[x].recoleccion_media = $scope.lstRecogidos3[x].rec_media;
			$scope.lstVehiculoConvencional3[x].recoleccion_anual = $scope.lstRecogidos3[x].rec_anual;

			$scope.lstVehiculoConvencional3[x].cod_usuario = '14';

			$scope.lstVehiculoAux3[c].rec_rcd_om_id = $scope.lstVehiculoConvencional3[x].rec_rcd_om_id;

			$scope.lstVehiculoAux3[c].placa3 = $scope.lstVehiculoConvencional3[x].placa;
			$scope.lstVehiculoAux3[c].recorrido_anual3 = $scope.lstVehiculoConvencional3[x].recorrido_anual;
			// $scope.lstVehiculoAux3[x].tipo_vehiculo3 =
			// $scope.lstVehiculoConvencional3[x].tipo_vehiculo;
			$scope.lstVehiculoAux3[c].anio_fabricacion3 = $scope.lstVehiculoConvencional3[x].anio_fabricacion;
			$scope.lstVehiculoAux3[c].capacidad3 = $scope.lstVehiculoConvencional3[x].capacidad;
			$scope.lstVehiculoAux3[c].tipo_combustible3 = $scope.lstVehiculoConvencional3[x].tipo_combustible;
			$scope.lstVehiculoAux3[c].cantidad_combustible3 = $scope.lstVehiculoConvencional3[x].cantidad_combustible;
			$scope.lstVehiculoAux3[c].costo_anual3 = $scope.lstVehiculoConvencional3[x].costo_anual;
			$scope.lstVehiculoAux3[c].rendimiento_anual3 = $scope.lstVehiculoConvencional3[x].rendimiento_anual;
			$scope.lstVehiculoAux3[c].capacidad_viaje3 = $scope.lstVehiculoConvencional3[x].capacidad_viaje;
			$scope.lstVehiculoAux3[c].efectividad3 = $scope.lstVehiculoConvencional3[x].efectividad;
			$scope.lstVehiculoAux3[c].promedio_viajes3 = $scope.lstVehiculoConvencional3[x].promedio_viajes;
			$scope.lstVehiculoAux3[c].promedio_turno3 = $scope.lstVehiculoConvencional3[x].promedio_turno;
			$scope.lstVehiculoAux3[c].dias_trabajados3 = $scope.lstVehiculoConvencional3[x].dias_trabajados;
			$scope.lstVehiculoAux3[c].recoleccion_media3 = $scope.lstVehiculoConvencional3[x].recoleccion_media;
			$scope.lstVehiculoAux3[c].recoleccion_anual3 = $scope.lstVehiculoConvencional3[x].recoleccion_anual;
			$scope.lstVehiculoAux3[c].tpv = $scope.lstVehiculoConvencional3[x].tipo_vehiculo;
			$scope.lstVehiculoAux3[c].cod_usuario = $scope.lstVehiculoConvencional3[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarVehiculo3,
			data : JSON.stringify($scope.lstVehiculoAux3),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.totalResiduos = function() {

		var total = 0;

		if ($scope.recoleccion_val.cantidad_residuos_organicos != null
				&& $scope.recoleccion_val.cantidad_residuos_inorganicos != null) {

			total = $scope.recoleccion_val.cantidad_residuos_organicos
					+ $scope.recoleccion_val.cantidad_residuos_inorganicos;
		}

		return total;
	}

	$scope.listarRecoleccion = function() {
		$scope.listarRecoleccion_df();
		$scope.listarRecoleccion_val();
		$scope.listarRecoleccion_rcd();
	}

	$scope.agregarComb = function() {

		$scope.lstCombAux = [ {
			'tipo' : '',
			'cantidad' : null,
			'costo' : null,
			'rendimiento' : null
		} ];
		if ($scope.flag > 0) {
			var aux = $scope.lstVehiculoConvencional.length;
			for (var x = 0; x < aux - 1; x++) {
				$scope.lstCombAux.push({
					'placa' : '',
					'recorrido' : null,
					'tipo_vehiculo' : '',
					'fabricacion' : null,
					'capacidad' : null
				});
			}
		}
	}

	$scope.agregarRecogidos = function() {

		$scope.lstRecogidosAux = [ {
			'capacidad' : '',
			'pcv' : null,
			'pvd' : null,
			'trabajados' : null,
			'densidad' : null,
			'rec_media' : null,
			'rec_anual' : null
		} ];
		if ($scope.flag > 0) {
			var aux = $scope.lstVehiculoConvencional.length;
			for (var x = 0; x < aux - 1; x++) {
				$scope.lstRecogidosAux.push({
					'placa' : '',
					'recorrido' : null,
					'tipo_vehiculo' : '',
					'fabricacion' : null,
					'capacidad' : null
				});
			}
		}
	}

	$scope.agregarComb2 = function() {

		$scope.lstCombAux2 = [ {
			'tipo' : '',
			'cantidad' : null,
			'costo' : null,
			'rendimiento' : null
		} ];
		if ($scope.flag > 0) {
			var aux2 = $scope.lstVehiculoConvencional2.length;
			for (var x = 0; x < aux2 - 1; x++) {
				$scope.lstCombAux2.push({
					'placa' : '',
					'recorrido' : null,
					'tipo_vehiculo' : '',
					'fabricacion' : null,
					'capacidad' : null
				});
			}
		}
	}

	$scope.agregarRecogidos2 = function() {

		$scope.lstRecogidosAux2 = [ {
			'capacidad' : '',
			'pcv' : null,
			'pvd' : null,
			'trabajados' : null,
			'densidad' : null,
			'rec_media' : null,
			'rec_anual' : null
		} ];
		if ($scope.flag > 0) {
			var aux2 = $scope.lstVehiculoConvencional2.length;
			for (var x = 0; x < aux2 - 1; x++) {
				$scope.lstRecogidosAux2.push({
					'placa' : '',
					'recorrido' : null,
					'tipo_vehiculo' : '',
					'fabricacion' : null,
					'capacidad' : null
				});
			}
		}
	}

	$scope.agregarComb3 = function() {

		$scope.lstCombAux3 = [ {
			'tipo' : '',
			'cantidad' : null,
			'costo' : null,
			'rendimiento' : null
		} ];
		if ($scope.flag > 0) {
			var aux3 = $scope.lstVehiculoConvencional3.length;
			for (var x = 0; x < aux3 - 1; x++) {
				$scope.lstCombAux3.push({
					'placa' : '',
					'recorrido' : null,
					'tipo_vehiculo' : '',
					'fabricacion' : null,
					'capacidad' : null
				});
			}
		}
	}

	$scope.agregarRecogidos3 = function() {

		$scope.lstRecogidosAux3 = [ {
			'capacidad' : '',
			'pcv' : null,
			'pvd' : null,
			'trabajados' : null,
			'densidad' : null,
			'rec_media' : null,
			'rec_anual' : null
		} ];
		if ($scope.flag > 0) {
			var aux3 = $scope.lstVehiculoConvencional3.length;
			for (var x = 0; x < aux3 - 1; x++) {
				$scope.lstRecogidosAux3.push({
					'placa' : '',
					'recorrido' : null,
					'tipo_vehiculo' : '',
					'fabricacion' : null,
					'capacidad' : null
				});
			}
		}
	}

	/*
	 * $scope.lstPlaca = [ { 'placa':'' }, ];
	 * 
	 * $scope.lstVehic = [ { 'recorrido':null, 'tipo_vehiculo':'',
	 * 'fabricacion':null, 'capacidad':null }, ];
	 * 
	 * $scope.lstComb = [ { 'tipo':'', 'cantidad':null, 'costo':null,
	 * 'rendimiento':null } ];
	 * 
	 * $scope.lstRecogidos = [ { 'capacidad':'', 'pcv':null, 'pvd':null,
	 * 'trabajados':null, 'densidad':null, 'rec_media':null, 'rec_anual':null } ];
	 * 
	 * $scope.lstPlaca2 = [ { 'placa':'' }, ];
	 * 
	 * $scope.lstVehic2 = [ { 'recorrido':null, 'tipo_vehiculo':'',
	 * 'fabricacion':null, 'capacidad':null }, ];
	 * 
	 * $scope.lstComb2 = [ { 'tipo':'', 'cantidad':null, 'costo':null,
	 * 'rendimiento':null } ];
	 * 
	 * $scope.lstRecogidos2 = [ { 'capacidad':'', 'pcv':null, 'pvd':null,
	 * 'trabajados':null, 'densidad':null, 'rec_media':null, 'rec_anual':null } ];
	 * 
	 * $scope.lstPlaca3 = [ { 'placa':'' }, ];
	 * 
	 * $scope.lstVehic3 = [ { 'recorrido':null, 'tipo_vehiculo':'',
	 * 'fabricacion':null, 'capacidad':null }, ];
	 * 
	 * $scope.lstComb3 = [ { 'tipo':'', 'cantidad':null, 'costo':null,
	 * 'rendimiento':null } ];
	 * 
	 * $scope.lstRecogidos3 = [ { 'capacidad':'', 'pcv':null, 'pvd':null,
	 * 'trabajados':null, 'densidad':null, 'rec_media':null, 'rec_anual':null } ];
	 */

	$scope.agregar = function() {

		/*
		 * $scope.lstPlaca.push({ 'placa':'' });
		 */

		$scope.lstVehiculoConvencional.push({
			'placa' : '',
			'recorrido' : null,
			'tipo_vehiculo' : '',
			'fabricacion' : null,
			'capacidad' : null
		});

		$scope.lstComb.push({
			'placa' : '',
			'tipo' : '',
			'cantidad' : null,
			'costo' : null,
			'rendimiento' : null
		});

		$scope.lstRecogidos.push({
			'placa' : '',
			'capacidad' : '',
			'pcv' : null,
			'pvd' : null,
			'trabajados' : null,
			'densidad' : null,
			'rec_media' : null,
			'rec_anual' : null
		});

		if ($scope.flag > 0) {

			$scope.lstVehiculoAux.push({
				'placa' : null,
				'recorrido_anual' : null,
				'tipo_vehiculo' : null,
				'anio_fabricacion' : null,
				'capacidad' : null,
				'tipo_combustible' : null,
				'cantidad_combustible' : null,
				'costo_anual' : null,
				'rendimiento_anual' : null,
				'capacidad_viaje' : null,
				'efectividad' : null,
				'promedio_viajes' : null,
				'promedio_turno' : null,
				'dias_trabajados' : null,
				'densidad' : null,
				'recoleccion_media' : null,
				'recoleccion_anual' : null
			});

		}
	}

	$scope.agregar2 = function() {

		/*
		 * $scope.lstPlaca2.push({ 'placa':'' });
		 */

		$scope.lstVehiculoConvencional2.push({
			'placa' : '',
			'recorrido' : null,
			'tipo_vehiculo' : '',
			'fabricacion' : null,
			'capacidad' : null
		});

		$scope.lstComb2.push({
			'placa' : '',
			'tipo' : '',
			'cantidad' : null,
			'costo' : null,
			'rendimiento' : null
		});

		$scope.lstRecogidos2.push({
			'placa' : '',
			'capacidad' : '',
			'pcv' : null,
			'pvd' : null,
			'trabajados' : null,
			'densidad' : null,
			'rec_media' : null,
			'rec_anual' : null
		});

		if ($scope.flag > 0) {

			$scope.lstVehiculoAux2.push({
				'rec_valorizacion_id' : null,
				'tpv2' : null,
				'placa2' : null,
				'recorrido_anual2' : null,
				'anio_fabricacion2' : null,
				'capacidad2' : null,
				'tipo_combustible2' : null,
				'cantidad_combustible2' : null,
				'costo_anual2' : null,
				'rendimiento_anual2' : null,
				'capacidad_viaje2' : null,
				'efectividad2' : null,
				'promedio_viajes2' : null,
				'promedio_turno2' : null,
				'dias_trabajados2' : null,
				'densidad2' : null,
				'recoleccion_media2' : null,
				'recoleccion_anual2' : null
			});
		}
	}

	$scope.agregar3 = function() {

		/*
		 * $scope.lstPlaca3.push({ 'placa':'' });
		 */

		$scope.lstVehiculoConvencional3.push({
			'placa' : '',
			'recorrido' : null,
			'tipo_vehiculo' : '',
			'fabricacion' : null,
			'capacidad' : null
		});

		$scope.lstComb3.push({
			'placa' : '',
			'tipo' : '',
			'cantidad' : null,
			'costo' : null,
			'rendimiento' : null
		});

		$scope.lstRecogidos3.push({
			'placa' : '',
			'capacidad' : '',
			'pcv' : null,
			'pvd' : null,
			'trabajados' : null,
			'densidad' : null,
			'rec_media' : null,
			'rec_anual' : null
		})

		if ($scope.flag > 0) {
			console.log("Flag");
			$scope.lstVehiculoAux3.push({
				'rec_rcd_om_id' : null,
				'tpv' : null,
				'placa3' : null,
				'recorrido_anual3' : null,
				'anio_fabricacion3' : null,
				'capacidad3' : null,
				'tipo_combustible3' : null,
				'cantidad_combustible3' : null,
				'costo_anual3' : null,
				'rendimiento_anual3' : null,
				'capacidad_viaje3' : null,
				'efectividad3' : null,
				'promedio_viajes3' : null,
				'promedio_turno3' : null,
				'dias_trabajados3' : null,
				'densidad3' : null,
				'recoleccion_media3' : null,
				'recoleccion_anual3' : null
			});

		}
	}

	$scope.insertar = function() {
		$scope.lstTrabajador.push({
			'dni' : $scope.trabajadores.dni,
			'nom_apellido' : $scope.trabajadores.nom_apellido,
			'sexo' : $scope.trabajadores.sexo,
			'rango' : $scope.trabajadores.rango,
			'cargo' : $scope.trabajadores.cargo,
			'laboral' : $scope.trabajadores.laboral,
			'horario' : $scope.trabajadores.horario,
			'dias_trabajados' : $scope.trabajadores.dias_trabajados
		});

		$scope.trabajadores.dni = null;
		$scope.trabajadores.nom_apellido = null;
		$scope.trabajadores.sexo = null;
		$scope.trabajadores.rango = null;
		$scope.trabajadores.cargo = null;
		$scope.trabajadores.laboral = null;
		$scope.trabajadores.horario = null;
		$scope.trabajadores.dias_trabajados = null;

		if ($scope.flag > 0) {

			$scope.lstTrabajadorAux.push({
				'dni' : $scope.trabajadores.dni,
				'nom_apellido' : $scope.trabajadores.nom_apellido,
				'sexo' : $scope.trabajadores.sexo,
				'rango' : $scope.trabajadores.rango,
				'cargo' : $scope.trabajadores.cargo,
				'laboral' : $scope.trabajadores.laboral,
				'horario' : $scope.trabajadores.horario,
				'dias_trabajados' : $scope.trabajadores.dias_trabajados
			});
		}
	}

	$scope.borrar = function(index) {
		$scope.lstTrabajador.splice(index, 1);
	};

	app.directive('editableTd', [ function() {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				element.css("cursor", "pointer");
				element.attr('contenteditable', 'true'); // Referencia:
				// Inciso 1
				element.bind('blur keyup change', function() { // Referencia:
					// Inciso 2
					scope.lstTrabajador[attrs.row][attrs.field] = element
							.text();
				});
				element.bind('click', function() { // Referencia: Inciso 3
					document.execCommand('selectAll', false, null)
				});
			}
		};
	} ]);

	/*
	 * $scope.insertar = function() {
	 * 
	 * $scope.grabarTrabajadores(); }
	 */

	$scope.grabarPersona = function() {

		for (var x = 0; x < $scope.lstBarredor.length; x++) {
			$scope.lstTrabajador[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarPersona,
			data : JSON.stringify($scope.lstBarredor),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.grabarTrabajadores();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.grabarTrabajadores = function() {

		for (var x = 0; x < $scope.lstTrabajador.length; x++) {
			$scope.lstTrabajador[x].rec_rcd_om_id = $scope.trabajadores.rec_rcd_om_id;
			$scope.lstTrabajador[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarTrabajadores,
			data : JSON.stringify($scope.lstTrabajador),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.actualizarTrabajadores = function() {

		var i = $scope.cantidad;
		var c = 0;

		for (var x = i; x < $scope.lstTrabajador.length; x++) {
			$scope.lstTrabajador[x].rec_rcd_om_id = $scope.trabajadores.rec_rcd_om_id;
			$scope.lstTrabajador[x].cod_usuario = '14';

			$scope.lstTrabajadorAux[c].rec_rcd_om_id = $scope.lstTrabajador[x].rec_rcd_om_id;

			$scope.lstTrabajadorAux[c].dni = $scope.lstTrabajador[x].dni;
			$scope.lstTrabajadorAux[c].nom_apellido = $scope.lstTrabajador[x].nom_apellido;
			$scope.lstTrabajadorAux[c].sexo = $scope.lstTrabajador[x].sexo;
			$scope.lstTrabajadorAux[c].rango = $scope.lstTrabajador[x].rango;
			$scope.lstTrabajadorAux[c].cargo = $scope.lstTrabajador[x].cargo;
			$scope.lstTrabajadorAux[c].laboral = $scope.lstTrabajador[x].laboral;
			$scope.lstTrabajadorAux[c].horario = $scope.lstTrabajador[x].horario;
			$scope.lstTrabajadorAux[c].dias_trabajados = $scope.lstTrabajador[x].dias_trabajados;

			$scope.lstTrabajadorAux[c].cod_usuario = $scope.lstTrabajador[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarTrabajadores,
			data : JSON.stringify($scope.lstTrabajadorAux),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.actTablaTrabajador = function() {

		$scope.tblTrabajador = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"trabajadores_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							Trabajadores : $scope.trabajadores
						};

						$http({
							method : 'POST',
							url : urlListarTrabajadores,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstTrabajadores;
											$scope.lstTrabajador = data.datosAdicionales.LstTrabajadores;

											for (var x = 0; x < $scope.lstTrabajador.length; x++) {

												$scope.lstTrabajador[x].dni = $scope.lstTrabajador[x].persona.dni;
												$scope.lstTrabajador[x].nom_apellido = $scope.lstTrabajador[x].persona.nom_apellidos;
												$scope.lstTrabajador[x].sexo = $scope.lstTrabajador[x].persona.sexo;
												$scope.lstTrabajador[x].rango = $scope.lstTrabajador[x].persona.rango_edad;
												$scope.lstTrabajador[x].cargo = $scope.lstTrabajador[x].cargo;
												$scope.lstTrabajador[x].laboral = $scope.lstTrabajador[x].condicion_laboral;
												$scope.lstTrabajador[x].horario = $scope.lstTrabajador[x].horario_trabajo;
												$scope.lstTrabajador[x].dias_trabajados = $scope.lstTrabajador[x].dias_trabajados;
											}

											$scope.cantidad = $scope.lstTrabajador.length;
											console.log("Cantidad: "
													+ $scope.cantidad);

											blockUI.start();
											params.total(nData.length);
											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.func = function(index) {
		$scope.lstComb[index].placa = $scope.lstVehiculoConvencional[index].placa;
		$scope.lstRecogidos[index].placa = $scope.lstVehiculoConvencional[index].placa;
	}

	$scope.func2 = function(index) {
		$scope.lstComb2[index].placa = $scope.lstVehiculoConvencional2[index].placa;
		$scope.lstRecogidos2[index].placa = $scope.lstVehiculoConvencional2[index].placa;
	}

	$scope.func3 = function(index) {
		$scope.lstComb3[index].placa = $scope.lstVehiculoConvencional3[index].placa;
		$scope.lstRecogidos3[index].placa = $scope.lstVehiculoConvencional3[index].placa;
	}

	$scope.volver = function() {
		$location.path("/gestion");
	}

	$scope.setCapacidad = function(index) {
		$scope.lstRecogidos[index].capacidad = $scope.lstVehiculoConvencional[index].capacidad;
	}
	
	$scope.setCapacidad2 = function(index) {
		$scope.lstRecogidos2[index].capacidad = $scope.lstVehiculoConvencional2[index].capacidad;
	}
	
	$scope.setCapacidad3 = function(index) {
		$scope.lstRecogidos3[index].capacidad = $scope.lstVehiculoConvencional3[index].capacidad;
	}
	
	$scope.setRendimiento = function(index) {
		$scope.lstComb[index].rendimiento = $scope.lstVehiculoConvencional[index].recorrido
				/ $scope.lstComb[index].cantidad;
	}

	$scope.setRecoleccionMedia = function(index) {
		$scope.lstRecogidos[index].rec_media = ($scope.lstRecogidos[index].capacidad
				* ($scope.lstRecogidos[index].efectividad / 100)
				* $scope.lstRecogidos[index].pcv
				* $scope.lstRecogidos[index].pvd * $scope.lstRecogidos[index].trabajados) / 7;
		$scope.lstRecogidos[index].rec_anual = $scope.lstRecogidos[index].rec_media * 365;
	}

	$scope.setRendimiento2 = function(index) {
		$scope.lstComb2[index].rendimiento = $scope.lstVehiculoConvencional2[index].recorrido
				/ $scope.lstComb2[index].cantidad;
	}

	$scope.setRecoleccionMedia2 = function(index) {
		$scope.lstRecogidos2[index].rec_media = ($scope.lstRecogidos2[index].capacidad
				* ($scope.lstRecogidos2[index].efectividad / 100)
				* $scope.lstRecogidos2[index].pcv
				* $scope.lstRecogidos2[index].pvd * $scope.lstRecogidos2[index].trabajados) / 7;
		$scope.lstRecogidos2[index].rec_anual = $scope.lstRecogidos2[index].rec_media * 365;
	}

	$scope.setRendimiento3 = function(index) {
		$scope.lstComb3[index].rendimiento = $scope.lstVehiculoConvencional3[index].recorrido
				/ $scope.lstComb3[index].cantidad;
	}

	$scope.setRecoleccionMedia3 = function(index) {
		$scope.lstRecogidos3[index].rec_media = ($scope.lstRecogidos3[index].capacidad
				* ($scope.lstRecogidos3[index].efectividad / 100)
				* $scope.lstRecogidos3[index].pcv
				* $scope.lstRecogidos3[index].pvd * $scope.lstRecogidos3[index].trabajados) / 7;
		$scope.lstRecogidos3[index].rec_anual = $scope.lstRecogidos3[index].rec_media * 365;
	}

	$scope.listarRecoleccion();
}