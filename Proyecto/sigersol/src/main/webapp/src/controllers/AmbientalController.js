function AmbientalController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlRegistrarAmbiental = $scope.urlRest + "/rest/ambiental/registrar";
	var urlActualizarAmbiental = $scope.urlRest + "/rest/ambiental/actualizar";
	var urlListarAmbiental = $scope.urlRest + "/rest/ambiental/obtener";

	var urlRegistrarAcciones = $scope.urlRest + "/rest/accionesEducacion/registrar";
	var urlActualizarAcciones = $scope.urlRest + "/rest/accionesEducacion/actualizar";
	var urlListarAcciones = $scope.urlRest + "/rest/accionesEducacion/listarAccionesEducacion";
	
	var urlRegistrarPoi = $scope.urlRest + "/rest/accionPoi/registrar";
	var urlActualizarPoi = $scope.urlRest + "/rest/accionPoi/actualizar";
	var urlListarPoi = $scope.urlRest + "/rest/accionPoi/listarAccionPoi";
	
	var urlRegistrarPromotores = $scope.urlRest + "/rest/promotores/registrar";
	var urlActualizarPromotores = $scope.urlRest + "/rest/promotores/actualizar";
	var urlListarPromotores = $scope.urlRest + "/rest/promotores/listarPromotores";
	
	
	var urlRegistrarAccion = $scope.urlRest + "/rest/accion/registrar";
	var urlActualizarAccion = $scope.urlRest + "/rest/accion/actualizar";
	var urlListarAccion = $scope.urlRest + "/rest/accion/listarAccion";
	
	
	$scope.ambiental = {};
	
	$scope.acciones = {};
	$scope.lstAcciones = [];
	
	$scope.poi = {};
	$scope.lstPoi = [];
	
	$scope.promotores = {};
	$scope.lstPromotores = [];
	
	$scope.accion = {};
	$scope.lstAccion = [];
	
	$scope.flag = 0;
	
	$scope.ambiental.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.ambiental.cod_usuario = '15';
	
	$scope.anioAnterior = "2017";
	
	$scope.guardar = function() {
		if($scope.flag == 0)
			$scope.grabarAmbiental();
		else
			$scope.actualizarAmbiental();
	}
	
	$scope.grabarAmbiental = function() {
		
		var paramJson = {
			Ambiental : $scope.ambiental
		};

		$http({
				method : 'POST',
				url : urlRegistrarAmbiental,
				data : JSON.stringify(paramJson),
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function(data) {
				if (data.datosAdicionales.id > 0) {
					alertService.showSuccess("Registro exitoso.");
					$scope.ambiental.educacion_ambiental_id = data.datosAdicionales.id;
					
					//$scope.acciones.educacion_ambiental_id = data.datosAdicionales.id;
					$scope.accion.educacion_ambiental_id = data.datosAdicionales.id;
					$scope.poi.educacion_ambiental_id = data.datosAdicionales.id;
					//$scope.promotores.educacion_ambiental_id = data.datosAdicionales.id;
					
					console.log("Accion " + $scope.acciones.educacion_ambiental_id);
					
					//$scope.grabarAcciones();
					$scope.grabarAccion();
					$scope.grabarPoi();
					//$scope.grabarPromotores();
				} else {
					alertService.showDanger("No se realizó el registro.");
				}

			}).error(function(status) {
				alertService.showDanger();
			});
			
			$location.path("/gestion");	
	}	
	
	$scope.actualizarAmbiental = function() {
		
		var paramJson = {
			Ambiental : $scope.ambiental
		};
		
		$http({
			method : 'POST',
			url : urlActualizarAmbiental,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");	
				
				//$scope.acciones.educacion_ambiental_id = data.datosAdicionales.id;
				$scope.accion.educacion_ambiental_id = data.datosAdicionales.id;
				$scope.poi.educacion_ambiental_id = data.datosAdicionales.id;
				//$scope.promotores.educacion_ambiental_id = data.datosAdicionales.id;
				
				
				//$scope.actualizarAcciones();
				$scope.actualizarAccion();
				$scope.actualizarPoi();
				//$scope.actualizarPromotores();
				
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}
	
	$scope.listarAmbiental = function() {
		
		var paramJson = {
			Ambiental : $scope.ambiental
		};
		
		$http({
			method : 'POST',
			url : urlListarAmbiental,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.ambiental = data.datosAdicionales.ambiental;
			
			console.log("Ambiental: " + $scope.ambiental.educacion_ambiental_id);
			
			if($scope.ambiental.educacion_ambiental_id != undefined){	
				$scope.flag = 1;
				//$scope.acciones.educacion_ambiental_id = $scope.ambiental.educacion_ambiental_id;
				$scope.accion.educacion_ambiental_id = $scope.ambiental.educacion_ambiental_id;
				$scope.poi.educacion_ambiental_id = $scope.ambiental.educacion_ambiental_id;
				//$scope.promotores.educacion_ambiental_id = $scope.ambiental.educacion_ambiental_id;
			}
			
			$scope.ambiental.ciclo_grs_id = $localStorage.GRS_ID;
			$scope.ambiental.cod_usuario = '15';
			//$scope.acciones.cod_usuario = '15';
			$scope.accion.cod_usuario = '15';
			$scope.poi.cod_usuario = '15';
			//$scope.promotores.cod_usuario = '15';
			
			//$scope.actTablaAcciones();
			$scope.actTablaAccion();
			$scope.actTablaPoi();
			//$scope.actTablaPromotores();
			
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	
	$scope.actTablaAcciones = function() {

		$scope.tblAcciones = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"acciones_educacion_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					AccionesEducacion : $scope.acciones
				};

				$http({
					method : 'POST',
					url : urlListarAcciones,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstAcciones;
					$scope.lstAcciones = data.datosAdicionales.LstAcciones;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarAcciones = function() {

		for (var x = 0; x < $scope.lstAcciones.length; x++) {
			$scope.lstAcciones[x].educacion_ambiental_id = $scope.acciones.educacion_ambiental_id;
			$scope.lstAcciones[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarAcciones,
			data : JSON.stringify($scope.lstAcciones),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.actualizarAcciones = function() {

		for (var x = 0; x < $scope.lstAcciones.length; x++) {
			$scope.lstAcciones[x].educacion_ambiental_id = $scope.acciones.educacion_ambiental_id;
			$scope.lstAcciones[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarAcciones,
			data : JSON.stringify($scope.lstAcciones),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	
	$scope.actTablaPoi = function() {

		$scope.tblPoi = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"accion_poi_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					AccionPoi : $scope.poi
				};

				$http({
					method : 'POST',
					url : urlListarPoi,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstAccionPoi;
					$scope.lstPoi = data.datosAdicionales.LstAccionPoi;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarPoi = function() {

		for (var x = 0; x < $scope.lstPoi.length; x++) {
			$scope.lstPoi[x].educacion_ambiental_id = $scope.poi.educacion_ambiental_id;
			$scope.lstPoi[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarPoi,
			data : JSON.stringify($scope.lstPoi),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.actualizarPoi = function() {

		for (var x = 0; x < $scope.lstPoi.length; x++) {
			$scope.lstPoi[x].educacion_ambiental_id = $scope.poi.educacion_ambiental_id;
			$scope.lstPoi[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarPoi,
			data : JSON.stringify($scope.lstPoi),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	
	$scope.actTablaPromotores = function() {

		$scope.tblPromotores = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"promotores_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					Promotores : $scope.promotores
				};

				$http({
					method : 'POST',
					url : urlListarPromotores,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstPromotores;
					$scope.lstPromotores = data.datosAdicionales.LstPromotores;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarPromotores = function() {

		for (var x = 0; x < $scope.lstPromotores.length; x++) {
			$scope.lstPromotores[x].educacion_ambiental_id = $scope.promotores.educacion_ambiental_id;
			$scope.lstPromotores[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarPromotores,
			data : JSON.stringify($scope.lstPromotores),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.actualizarPromotores = function() {

		for (var x = 0; x < $scope.lstPromotores.length; x++) {
			$scope.lstPromotores[x].educacion_ambiental_id = $scope.promotores.educacion_ambiental_id;
			$scope.lstPromotores[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarPromotores,
			data : JSON.stringify($scope.lstPromotores),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	
	$scope.actTablaAccion = function() {

		$scope.tblAccion = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"accion_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					Accion : $scope.accion
				};

				$http({
					method : 'POST',
					url : urlListarAccion,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstAccion;
					$scope.lstAccion = data.datosAdicionales.LstAccion;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarAccion = function() {

		for (var x = 0; x < $scope.lstAccion.length; x++) {
			$scope.lstAccion[x].educacion_ambiental_id = $scope.accion.educacion_ambiental_id;
			$scope.lstAccion[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarAccion,
			data : JSON.stringify($scope.lstAccion),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.actualizarAccion = function() {

		for (var x = 0; x < $scope.lstAccion.length; x++) {
			$scope.lstAccion[x].educacion_ambiental_id = $scope.accion.educacion_ambiental_id;
			$scope.lstAccion[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarAccion,
			data : JSON.stringify($scope.lstAccion),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
		
	$scope.volver = function() {
		$location.path("/gestion");
	}
	
	$scope.listarAmbiental();
}