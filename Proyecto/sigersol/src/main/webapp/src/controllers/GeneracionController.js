function GeneracionController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, alertService, blockUI, $window, ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope

	});
	$scope.validarSesion();

	var urlRegistrarGeneracion = $scope.urlRest + "/rest/generacion/registrar";
	var urlActualizarGeneracion = $scope.urlRest
			+ "/rest/generacion/actualizar";
	var urlListarGeneracion = $scope.urlRest + "/rest/generacion/obtener";

	var urlListarGeneracion_No_Domiciliar = $scope.urlRest
			+ "/rest/generacionNoDomiciliar/listarGND";
	var urlActualizarGeneracion_No_Domiciliar = $scope.urlRest
			+ "/rest/generacionNoDomiciliar/actualizar";
	var urlRegistrarGeneracion_No_Domiciliar = $scope.urlRest
			+ "/rest/generacionNoDomiciliar/registrar";

	var urlListarResiduosEspeciales = $scope.urlRest
			+ "/rest/residuosEspeciales/listarResiduosEspeciales";
	var urlRegistrarResiduosEspeciales = $scope.urlRest
			+ "/rest/residuosEspeciales/registrar";
	var urlActualizarResiduosEspeciales = $scope.urlRest
			+ "/rest/residuosEspeciales/actualizar";

	var urlListarResSolidos = $scope.urlRest + "/rest/resSolidos/obtener";
	var urlRegistrarResiduosSolidos = $scope.urlRest
			+ "/rest/resSolidos/registrar";
	var urlActualizarResiduosSolidos = $scope.urlRest
			+ "/rest/resSolidos/actualizar";

	var urlListarComposicionND = $scope.urlRest + "/rest/composicionND/obtener";
	var urlRegistrarComposicionND = $scope.urlRest
			+ "/rest/composicionND/registrar";
	var urlActualizarComposicionND = $scope.urlRest
			+ "/rest/composicionND/actualizar";

	var urlListarRS_Especiales = $scope.urlRest + "/rest/rsEspeciales/obtener";
	var urlRegistrarRS_Especiales = $scope.urlRest
			+ "/rest/rsEspeciales/registrar";
	var urlActualizarRS_Especiales = $scope.urlRest
			+ "/rest/rsEspeciales/actualizar";

	var urlListarRcdOm = $scope.urlRest + "/rest/rcdOm/obtener";
	var urlRegistrarRcdOm = $scope.urlRest + "/rest/rcdOm/registrar";
	var urlActualizarRcdOm = $scope.urlRest + "/rest/rcdOm/actualizar";

	$scope.cantidad = 0;
	$scope.generacion = {};
	$scope.generacionnodomiciliar = {};
	$scope.lstGND = [];
	$scope.resespeciales = {}
	$scope.lstResEspeciales = [];
	$scope.resSolidos = {};
	$scope.lstResSolidos = [];

	$scope.composicion_nd = {};
	$scope.lstComposicionND = [];

	$scope.res_solidos_especiales = {};
	$scope.lstResSolidosEspeciales = [];

	$scope.flag = 0;

	$scope.rcd_om = {};
	$scope.lstRcdOm = [];

	$scope.generacion.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.generacion.cod_usuario = $localStorage.SesionId;

	$scope.generacionnodomiciliar.cod_usuario = $localStorage.SesionId;

	$localStorage.GENERACION = '0';

	$scope.composicionTotal = 0;
	$scope.gndTotal = 0;
	$scope.composicionNoDomiciliarTotal = 0;
	$scope.especialesTotal = 0;

	$scope.guardar = function() {
		if ($scope.flag == 0) {
			console.log("Flag: " + $scope.generacion.flag_generacion);
			$scope.grabarGeneracion();
		} else {
			$scope.actualizarGeneracion();
		}
	}

	$scope.guardarOtro = function() {
		if ($scope.flag == 0) {
			$scope.grabarGeneracion();
		} else {
			$scope.actualizarGeneracion();
		}
	}

	$scope.grabarGeneracion = function() {

		var paramJson = {
			Generacion : $scope.generacion
		};

		$http({
			method : 'POST',
			url : urlRegistrarGeneracion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

								if ($scope.generacion.flag_generacion == 1) {
									$scope.generacionnodomiciliar.generacion_id = data.datosAdicionales.id;
									$scope.resSolidos.generacion_id = data.datosAdicionales.id;
									$scope.resespeciales.generacion_id = data.datosAdicionales.id;

									$scope.composicion_nd.generacion_id = data.datosAdicionales.id;
									$scope.res_solidos_especiales.generacion_id = data.datosAdicionales.id;
									$scope.rcd_om.generacion_id = data.datosAdicionales.id;

									$scope.grabarGeneracionNoDomiciliar();
									$scope.grabarComposicion();

									$scope.grabarComposicionND()
									$scope.grabarResSolidosEspeciales();
									$scope.grabarRcdOm();
								}

								$location.path("/gestion");

								$localStorage.GENERACION = '1';
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarGeneracion = function() {

		var paramJson = {
			Generacion : $scope.generacion
		};

		$http({
			method : 'POST',
			url : urlActualizarGeneracion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								$scope.generacionnodomiciliar.generacion_id = data.datosAdicionales.id;
								$scope.resSolidos.generacion_id = data.datosAdicionales.id;
								$scope.resespeciales.generacion_id = data.datosAdicionales.id;

								$scope.composicion_nd.generacion_id = data.datosAdicionales.id;

								$scope.res_solidos_especiales.generacion_id = data.datosAdicionales.id;

								$scope.rcd_om.generacion_id = data.datosAdicionales.id;

								console
										.log("ID Composicion Generacion Composicion No Domiciliar: "
												+ $scope.composicion_nd.generacion_id);

								$scope.actualizarGeneracionNoDomiciliar();
								$scope.actualizarComposicion();

								// $scope.actualizarResiduosEspeciales();

								$scope.actualizarComposicionND();
								$scope.actualizarResSolidosEspeciales();

								$scope.actualizarRcdOm();

							} else {
								alertService
										.showDanger("No se realizó el registro1.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		$location.path("/gestion");
	}

	$scope.listarGeneracion = function() {

		var paramJson = {
			Generacion : $scope.generacion
		};

		$http({
			method : 'POST',
			url : urlListarGeneracion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.generacion = data.datosAdicionales.generacion;
							if ($scope.generacion.generacion_id != undefined) {
								blockUI.start();
								$scope.flag = 1;
								// console.log("Generacion Lista Id: "
								// + $scope.generacion.generacion_id);
								$scope.generacionnodomiciliar.generacion_id = $scope.generacion.generacion_id;
								$scope.resSolidos.generacion_id = $scope.generacion.generacion_id;
								// $scope.resespeciales.generacion_id =
								// $scope.generacion.generacion_id;
								$scope.composicion_nd.generacion_id = $scope.generacion.generacion_id;
								$scope.res_solidos_especiales.generacion_id = $scope.generacion.generacion_id;
								$scope.rcd_om.generacion_id = $scope.generacion.generacion_id;

							}
							$scope.generacion.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.generacion.cod_usuario = $localStorage.SesionId;

							$scope.actTablaNoDomiciliar();
							// /$scope.actTablaResiduosEspeciales();
							$scope.listarResSolidos();
							$scope.listarComposicionND();
							$scope.listarResSolidosEspeciales();
							$scope.actTablaRcdOm();
							blockUI.stop();
						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.actTablaNoDomiciliar = function() {

		$scope.tblGeneracion = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"generacion_no_domiciliar_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					GeneracionNoDomiciliar : $scope.generacionnodomiciliar
				};

				$http({
					method : 'POST',
					url : urlListarGeneracion_No_Domiciliar,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstGND;
					$scope.lstGND = data.datosAdicionales.LstGND;

					params.total(nData.length);

				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarGeneracionNoDomiciliar = function() {

		for (var x = 0; x < $scope.lstGND.length; x++) {
			$scope.lstGND[x].generacion_id = $scope.generacionnodomiciliar.generacion_id;
			$scope.lstGND[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarGeneracion_No_Domiciliar,
			data : JSON.stringify($scope.lstGND),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarGeneracionNoDomiciliar = function() {

		for (var x = 0; x < $scope.lstGND.length; x++) {
			$scope.lstGND[x].generacion_id = $scope.generacionnodomiciliar.generacion_id;
			$scope.lstGND[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarGeneracion_No_Domiciliar,
			data : JSON.stringify($scope.lstGND),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.volver = function() {
		$location.path("/gestion");
	}

	$scope.getTotalComposicion = function() {

		var totalComposicion = 0;
		for (var i = 0; i < $scope.lstResSolidos.length; i++) {
			var item = $scope.lstResSolidos[i];

			for (var j = 0; j < $scope.lstResSolidos[i].hijos.length; j++) {

				var item2 = $scope.lstResSolidos[i].hijos[j];

				if (item2.porcentaje == null) {
					var porcentaje = 0;
					totalComposicion += porcentaje;
				} else {
					totalComposicion += item2.porcentaje;
				}

				if ($scope.lstResSolidos[i].hijos[j].hijos != undefined) {

					for (var k = 0; k < $scope.lstResSolidos[i].hijos[j].hijos.length; k++) {

						if ($scope.lstResSolidos[i].hijos[j].hijos[k].hijos != undefined) {

							for (var l = 0; l < $scope.lstResSolidos[i].hijos[j].hijos[k].hijos.length; l++) {

								var item4 = $scope.lstResSolidos[i].hijos[j].hijos[k].hijos[l];

								if (item4.porcentaje == null) {
									var porcentaje3 = 0;
									totalComposicion += porcentaje3;
								} else {
									totalComposicion += item4.porcentaje;
								}
							}

						} else {

							var item3 = $scope.lstResSolidos[i].hijos[j].hijos[k];

							if (item3.porcentaje == null) {
								var porcentaje2 = 0;
								totalComposicion += porcentaje2;
							} else {
								totalComposicion += item3.porcentaje;
							}
						}
					}
				} else {
					console.log("Entra a este segmento 1 GD")
				}

			}
		}

		if ($scope.generacion.porcentaje == null) {
			var genporcentaje = 0;
			totalComposicion += genporcentaje;
		} else {
			totalComposicion += $scope.generacion.porcentaje;
		}

		$scope.generacion.total_composicion = totalComposicion;

		$scope.composicionTotal = totalComposicion;

		return totalComposicion;
	}

	$scope.getTotalGND = function() {

		var totalGND = 0;

		for (var i = 0; i < $scope.lstGND.length; i++) {
			var item = $scope.lstGND[i];

			if (item.cantidad == null) {
				var cantidad = 0;
				totalGND += cantidad;
			} else {
				totalGND += item.cantidad;
			}
		}

		$scope.generacion.total_no_domiciliar = totalGND;
		$scope.gndTotal = totalGND;

		return totalGND;
	}

	/*
	 * $scope.getTotalResEspeciales = function() {
	 * 
	 * var totalResEspeciales = 0;
	 * 
	 * for (var i = 0; i < $scope.lstResEspeciales.length; i++) { var item =
	 * $scope.lstResEspeciales[i];
	 * 
	 * if(item.cantidad == null){ var cantidad = 0; totalResEspeciales +=
	 * cantidad; } else{ totalResEspeciales += item.cantidad; } }
	 * 
	 * $scope.generacion.total_residuos_especiales = totalResEspeciales;
	 * 
	 * return totalResEspeciales; }
	 */

	$scope.getTotalComposicionNoDomiciliar = function() {

		var totalComposicionNoDomiciliar = 0;
		for (var i = 0; i < $scope.lstComposicionND.length; i++) {
			var item = $scope.lstComposicionND[i];

			for (var j = 0; j < $scope.lstComposicionND[i].hijos.length; j++) {

				var item2 = $scope.lstComposicionND[i].hijos[j];
				var porcentaje1 = 0;
				var porcentaje2 = 0;
				var porcentaje3 = 0;
				var porcentaje4 = 0;
				var porcentaje5 = 0;
				var porcentaje6 = 0;
				var porcentaje7 = 0;

				if (item2.porcentaje1 != null) {
					porcentaje1 = item2.porcentaje1;
				}

				if (item2.porcentaje2 != null) {
					porcentaje2 = item2.porcentaje2;
				}

				if (item2.porcentaje3 != null) {
					porcentaje3 = item2.porcentaje3;
				}

				if (item2.porcentaje4 != null) {
					porcentaje4 = item2.porcentaje4;
				}

				if (item2.porcentaje5 != null) {
					porcentaje5 = item2.porcentaje5;
				}

				if (item2.porcentaje6 != null) {
					porcentaje6 = item2.porcentaje6;
				}

				if (item2.porcentaje7 != null) {
					porcentaje7 = item2.porcentaje7;
				}

				totalComposicionNoDomiciliar += porcentaje1 + porcentaje2
						+ porcentaje3 + porcentaje4 + porcentaje5 + porcentaje6
						+ porcentaje7;

				if ($scope.lstComposicionND[i].hijos[j].hijos != undefined) {

					for (var k = 0; k < $scope.lstComposicionND[i].hijos[j].hijos.length; k++) {

						if ($scope.lstComposicionND[i].hijos[j].hijos[k].hijos != undefined) {

							for (var l = 0; l < $scope.lstComposicionND[i].hijos[j].hijos[k].hijos.length; l++) {

								var item4 = $scope.lstComposicionND[i].hijos[j].hijos[k].hijos[l];

								var porcentajes1 = 0;
								var porcentajes2 = 0;
								var porcentajes3 = 0;
								var porcentajes4 = 0;
								var porcentajes5 = 0;
								var porcentajes6 = 0;
								var porcentajes7 = 0;

								if (item4.porcentaje1 != null) {
									porcentajes1 = item4.porcentaje1;
								}

								if (item4.porcentaje2 != null) {
									porcentajes2 = item4.porcentaje2;
								}

								if (item4.porcentaje3 != null) {
									porcentajes3 = item4.porcentaje3;
								}

								if (item4.porcentaje4 != null) {
									porcentajes4 = item4.porcentaje4;
								}

								if (item4.porcentaje5 != null) {
									porcentajes5 = item4.porcentaje5;
								}

								if (item4.porcentaje6 != null) {
									porcentajes6 = item4.porcentaje6;
								}

								if (item4.porcentaje7 != null) {
									porcentajes7 = item4.porcentaje7;
								}

								totalComposicionNoDomiciliar += porcentajes1
										+ porcentajes2 + porcentajes3
										+ porcentajes4 + porcentajes5
										+ porcentajes6 + porcentajes7;
							}

						} else {

							var item3 = $scope.lstComposicionND[i].hijos[j].hijos[k];

							var porcent1 = 0;
							var porcent2 = 0;
							var porcent3 = 0;
							var porcent4 = 0;
							var porcent5 = 0;
							var porcent6 = 0;
							var porcent7 = 0;

							if (item3.porcentaje1 != null) {
								porcent1 = item3.porcentaje1;
							}

							if (item3.porcentaje2 != null) {
								porcent2 = item3.porcentaje2;
							}

							if (item3.porcentaje3 != null) {
								porcent3 = item3.porcentaje3;
							}

							if (item3.porcentaje4 != null) {
								porcent4 = item3.porcentaje4;
							}

							if (item3.porcentaje5 != null) {
								porcent5 = item3.porcentaje5;
							}

							if (item3.porcentaje6 != null) {
								porcent6 = item3.porcentaje6;
							}

							if (item3.porcentaje7 != null) {
								porcent7 = item3.porcentaje7;
							}

							totalComposicionNoDomiciliar += porcent1 + porcent2
									+ porcent3 + porcent4 + porcent5 + porcent6
									+ porcent7;
						}
					}
				} else {
					console.log("Entra a este segmento")
				}

			}
		}

		$scope.composicionNoDomiciliarTotal = totalComposicionNoDomiciliar;

		return totalComposicionNoDomiciliar;
	}

	$scope.getTotalResiduosEspeciales = function() {

		var totalResiduosEspeciales = 0;
		for (var i = 0; i < $scope.lstResSolidosEspeciales.length; i++) {
			var item = $scope.lstResSolidosEspeciales[i];

			for (var j = 0; j < $scope.lstResSolidosEspeciales[i].hijos.length; j++) {

				var item2 = $scope.lstResSolidosEspeciales[i].hijos[j];
				var porcentaje1 = 0;
				var porcentaje2 = 0;
				var porcentaje3 = 0;
				var porcentaje4 = 0;
				var porcentaje5 = 0;
				var porcentaje6 = 0;
				var porcentaje7 = 0;

				if (item2.porcentaje1 != null) {
					porcentaje1 = item2.porcentaje1;
				}

				if (item2.porcentaje2 != null) {
					porcentaje2 = item2.porcentaje2;
				}

				if (item2.porcentaje3 != null) {
					porcentaje3 = item2.porcentaje3;
				}

				if (item2.porcentaje4 != null) {
					porcentaje4 = item2.porcentaje4;
				}

				if (item2.porcentaje5 != null) {
					porcentaje5 = item2.porcentaje5;
				}

				if (item2.porcentaje6 != null) {
					porcentaje6 = item2.porcentaje6;
				}

				if (item2.porcentaje7 != null) {
					porcentaje7 = item2.porcentaje7;
				}

				totalResiduosEspeciales += porcentaje1 + porcentaje2
						+ porcentaje3 + porcentaje4 + porcentaje5 + porcentaje6
						+ porcentaje7;

				if ($scope.lstResSolidosEspeciales[i].hijos[j].hijos != undefined) {

					for (var k = 0; k < $scope.lstResSolidosEspeciales[i].hijos[j].hijos.length; k++) {

						if ($scope.lstResSolidosEspeciales[i].hijos[j].hijos[k].hijos != undefined) {

							for (var l = 0; l < $scope.lstResSolidosEspeciales[i].hijos[j].hijos[k].hijos.length; l++) {

								var item4 = $scope.lstResSolidosEspeciales[i].hijos[j].hijos[k].hijos[l];

								var porcentajes1 = 0;
								var porcentajes2 = 0;
								var porcentajes3 = 0;
								var porcentajes4 = 0;
								var porcentajes5 = 0;
								var porcentajes6 = 0;
								var porcentajes7 = 0;

								if (item4.porcentaje1 != null) {
									porcentajes1 = item4.porcentaje1;
								}

								if (item4.porcentaje2 != null) {
									porcentajes2 = item4.porcentaje2;
								}

								if (item4.porcentaje3 != null) {
									porcentajes3 = item4.porcentaje3;
								}

								if (item4.porcentaje4 != null) {
									porcentajes4 = item4.porcentaje4;
								}

								if (item4.porcentaje5 != null) {
									porcentajes5 = item4.porcentaje5;
								}

								if (item4.porcentaje6 != null) {
									porcentajes6 = item4.porcentaje6;
								}

								if (item4.porcentaje7 != null) {
									porcentajes7 = item4.porcentaje7;
								}

								totalResiduosEspeciales += porcentajes1
										+ porcentajes2 + porcentajes3
										+ porcentajes4 + porcentajes5
										+ porcentajes6 + porcentajes7;
							}

						} else {

							var item3 = $scope.lstResSolidosEspeciales[i].hijos[j].hijos[k];

							var porcent1 = 0;
							var porcent2 = 0;
							var porcent3 = 0;
							var porcent4 = 0;
							var porcent5 = 0;
							var porcent6 = 0;
							var porcent7 = 0;

							if (item3.porcentaje1 != null) {
								porcent1 = item3.porcentaje1;
							}

							if (item3.porcentaje2 != null) {
								porcent2 = item3.porcentaje2;
							}

							if (item3.porcentaje3 != null) {
								porcent3 = item3.porcentaje3;
							}

							if (item3.porcentaje4 != null) {
								porcent4 = item3.porcentaje4;
							}

							if (item3.porcentaje5 != null) {
								porcent5 = item3.porcentaje5;
							}

							if (item3.porcentaje6 != null) {
								porcent6 = item3.porcentaje6;
							}

							if (item3.porcentaje7 != null) {
								porcent7 = item3.porcentaje7;
							}

							totalResiduosEspeciales += porcent1 + porcent2
									+ porcent3 + porcent4 + porcent5 + porcent6
									+ porcent7;
						}
					}
				} else {
					console.log("Entra a este segmento")
				}

			}
		}

		$scope.especialesTotal = totalResiduosEspeciales;
	}

	$scope.listarResSolidos = function() {

		var paramJson = {
			ResSolidos : $scope.resSolidos
		};

		$http({
			method : 'POST',
			url : urlListarResSolidos,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstResSolidos = data.datosAdicionales.LstResSolidos;

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.grabarComposicion = function() {

		for (var x = 0; x < $scope.lstResSolidos.length; x++) {
			$scope.lstResSolidos[x].generacion_id = $scope.resSolidos.generacion_id;
			$scope.lstResSolidos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarResiduosSolidos,
			data : JSON.stringify($scope.lstResSolidos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.actualizarComposicion = function() {

		for (var x = 0; x < $scope.lstResSolidos.length; x++) {
			$scope.lstResSolidos[x].generacion_id = $scope.resSolidos.generacion_id;
			$scope.lstResSolidos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarResiduosSolidos,
			data : JSON.stringify($scope.lstResSolidos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.grabarResiduosEspeciales = function() {

		for (var x = 0; x < $scope.lstResEspeciales.length; x++) {
			$scope.lstResEspeciales[x].generacion_id = $scope.resespeciales.generacion_id;
			$scope.lstResEspeciales[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarResiduosEspeciales,
			data : JSON.stringify($scope.lstResEspeciales),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarResiduosEspeciales = function() {

		for (var x = 0; x < $scope.lstResEspeciales.length; x++) {
			$scope.lstResEspeciales[x].generacion_id = $scope.resespeciales.generacion_id;
			$scope.lstResEspeciales[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarResiduosEspeciales,
			data : JSON.stringify($scope.lstResEspeciales),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaResiduosEspeciales = function() {

		$scope.tblResEspeciales = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"residuos_especiales_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							ResiduosEspeciales : $scope.resespeciales
						};

						$http({
							method : 'POST',
							url : urlListarResiduosEspeciales,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstResEspeciales;
											$scope.lstResEspeciales = data.datosAdicionales.LstResEspeciales;

											params.total(nData.length);

										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	$scope.listarComposicionND = function() {

		var paramJson = {
			ComposicionND : $scope.composicion_nd
		};

		$http({
			method : 'POST',
			url : urlListarComposicionND,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstComposicionND = data.datosAdicionales.LstComposicionND;
			// console.log('lista $scope.lstComposicionND>>' +
			// JSON.stringify($scope.lstComposicionND));
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	/*
	 * $scope.actTablaComposicionND = function() {
	 * 
	 * $scope.tblComposicionND = new NgTableParams({ page : 1, count : 10,
	 * sorting : { "composicion_no_domiciliar_id" : "asc" } }, { getData :
	 * function($defer, params) {
	 * 
	 * var paramJson = { ComposicionND : $scope.composicion_nd };
	 * 
	 * $http({ method : 'POST', url : urlListarComposicionND, data :
	 * JSON.stringify(paramJson), headers : { 'Content-Type' :
	 * 'application/json' } }).success(function(data) { var filtro = {}; var
	 * nData = data.datosAdicionales.LstComposicionND; $scope.lstComposicionND =
	 * data.datosAdicionales.LstComposicionND; blockUI.start();
	 * params.total(nData.length); blockUI.stop(); }).error(function(status) {
	 * $scope.status = status; }); } }); }
	 */

	$scope.grabarComposicionND = function() {

		for (var x = 0; x < $scope.lstComposicionND.length; x++) {
			$scope.lstComposicionND[x].generacion_id = $scope.composicion_nd.generacion_id;
			$scope.lstComposicionND[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarComposicionND,
			data : JSON.stringify($scope.lstComposicionND),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarComposicionND = function() {

		for (var x = 0; x < $scope.lstComposicionND.length; x++) {
			$scope.lstComposicionND[x].generacion_id = $scope.composicion_nd.generacion_id;
			$scope.lstComposicionND[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarComposicionND,
			data : JSON.stringify($scope.lstComposicionND),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.listarResSolidosEspeciales = function() {

		var paramJson = {
			ResSolidosEspeciales : $scope.res_solidos_especiales
		};

		$http({
			method : 'POST',
			url : urlListarRS_Especiales,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.lstResSolidosEspeciales = data.datosAdicionales.LstResSolidosEspeciales;

						}).error(function(status) {
					alertService.showDanger();
				});
	}

	/*
	 * $scope.actTablaResSolidosEspeciales = function() {
	 * 
	 * $scope.tblResSolidosEspeciales = new NgTableParams({ page : 1, count :
	 * 10, sorting : { "res_solidos_especiales_id" : "asc" } }, { getData :
	 * function($defer, params) {
	 * 
	 * var paramJson = { ResSolidosEspeciales : $scope.res_solidos_especiales };
	 * 
	 * $http({ method : 'POST', url : urlListarRS_Especiales, data :
	 * JSON.stringify(paramJson), headers : { 'Content-Type' :
	 * 'application/json' } }).success(function(data) { var filtro = {}; var
	 * nData = data.datosAdicionales.LstResSolidosEspeciales;
	 * $scope.lstResSolidosEspeciales =
	 * data.datosAdicionales.LstResSolidosEspeciales; blockUI.start();
	 * params.total(nData.length); blockUI.stop(); }).error(function(status) {
	 * $scope.status = status; }); } }); }
	 */

	$scope.grabarResSolidosEspeciales = function() {

		for (var x = 0; x < $scope.lstResSolidosEspeciales.length; x++) {
			$scope.lstResSolidosEspeciales[x].generacion_id = $scope.res_solidos_especiales.generacion_id;
			$scope.lstResSolidosEspeciales[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarRS_Especiales,
			data : JSON.stringify($scope.lstResSolidosEspeciales),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarResSolidosEspeciales = function() {

		for (var x = 0; x < $scope.lstResSolidosEspeciales.length; x++) {
			$scope.lstResSolidosEspeciales[x].generacion_id = $scope.res_solidos_especiales.generacion_id;
			$scope.lstResSolidosEspeciales[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarRS_Especiales,
			data : JSON.stringify($scope.lstResSolidosEspeciales),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaRcdOm = function() {

		$scope.tblRcdOm = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"rcd_om_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					RcdOm : $scope.rcd_om
				};

				$http({
					method : 'POST',
					url : urlListarRcdOm,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstRcdOm;
					$scope.lstRcdOm = data.datosAdicionales.LstRcdOm;

					params.total(nData.length);

				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarRcdOm = function() {

		for (var x = 0; x < $scope.lstRcdOm.length; x++) {
			$scope.lstRcdOm[x].generacion_id = $scope.rcd_om.generacion_id;
			$scope.lstRcdOm[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarRcdOm,
			data : JSON.stringify($scope.lstRcdOm),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarRcdOm = function() {

		for (var x = 0; x < $scope.lstRcdOm.length; x++) {
			$scope.lstRcdOm[x].generacion_id = $scope.rcd_om.generacion_id;
			$scope.lstRcdOm[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarRcdOm,
			data : JSON.stringify($scope.lstRcdOm),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.listarGeneracion();

	$scope.getTotalRsSsDs = function() {
		$scope.totalRSD = $scope.generacion.gpc_diario * 71364 / 1000 * 365;

		$scope.generacion.generacion_total_rsd = $scope.totalRSD;

		return $scope.totalRSD;

	}

	$scope.getTotalRsSsNDs = function() {
		$scope.totalRSND = 0;

		for (var i = 0; i < $scope.lstGND.length; i++) {
			$scope.totalRSND += $scope.lstGND[i].cantidad;
		}

		$scope.totalRSND = ($scope.totalRSND * 365) / 1000;

		$scope.generacion.generacion_total_rsnd = $scope.totalRSND;

		return $scope.totalRSND;
	}

	$scope.getTotalRCDOM = function() {
		$scope.totalRCDOM = 0;

		for (var i = 0; i < $scope.lstRcdOm.length; i++) {
			$scope.totalRCDOM += $scope.lstRcdOm[i].cantidad;
		}
	}

	$scope.getTotalGeneracionRsEspeciales = function() {
		$scope.getTotalResiduosEspeciales();
		$scope.getTotalRCDOM();

		$scoper.generacion.total_rsespeciales = $scope.totalRCDOM
				+ $scope.especialesTotal;

	}

	$scope.getTotalRcdOm = function(){
		var total = 0;
		
		for (var x = 0; x < $scope.lstRcdOm.length; x++){
			if($scope.lstRcdOm[x].cantidad == null)
				total += 0;
			else
				total += $scope.lstRcdOm[x].cantidad;
		}
		
		$scope.generacion.generacion_total_rcd_om = total;
		
		return total;
	}
	
}