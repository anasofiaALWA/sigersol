function AlmacenamientoController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlRegistrarAlmacenamiento = $scope.urlRest
			+ "/rest/almacenamiento/registrar";
	var urlActualizarAlmacenamiento = $scope.urlRest
			+ "/rest/almacenamiento/actualizar";
	var urlListarAlmacenamiento = $scope.urlRest
			+ "/rest/almacenamiento/obtener";

	var urlRegistrarContenedor = $scope.urlRest + "/rest/contenedor/registrar";
	var urlActualizarContenedor = $scope.urlRest
			+ "/rest/contenedor/actualizar";
	var urlListarContenedor = $scope.urlRest
			+ "/rest/contenedor/listarContenedor";

	var urlRegistrarPapeleras = $scope.urlRest + "/rest/papeleras/registrar";
	var urlActualizarPapeleras = $scope.urlRest + "/rest/papeleras/actualizar";
	var urlListarPapeleras = $scope.urlRest + "/rest/papeleras/listarPapeleras";

	$scope.almacenamiento = {};
	$scope.lista = [];
	$scope.lstAlmacenamiento = [];

	$scope.contenedor = {};
	$scope.lstContenedor = [];

	$scope.papeleras = {};
	$scope.lstPapeleras = [];

	$scope.flag = 0;

	$scope.almacenamiento.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.almacenamiento.cod_usuario = '15';

	$localStorage.ALMACENAMIENTO = '0';

	$scope.guardar = function() {
		if ($scope.flag == 0)
			$scope.grabarAlmacenamiento();
		else
			$scope.actualizarAlmacenamiento();
	}

	$scope.grabarAlmacenamiento = function() {

		var paramJson = {
			Almacenamiento : $scope.almacenamiento
		};

		$http({
			method : 'POST',
			url : urlRegistrarAlmacenamiento,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$localStorage.ALMACENAMIENTO = '1';

				$scope.contenedor.almacenamiento_id = data.datosAdicionales.id;
				$scope.papeleras.almacenamiento_id = data.datosAdicionales.id;
				$scope.grabarContenedor();
				$scope.grabarPapeleras();

			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

		$location.path("/gestion");
	}

	$scope.actualizarAlmacenamiento = function() {

		var paramJson = {
			Almacenamiento : $scope.almacenamiento
		};

		$http({
			method : 'POST',
			url : urlActualizarAlmacenamiento,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				$scope.contenedor.almacenamiento_id = data.datosAdicionales.id;
				$scope.papeleras.almacenamiento_id = data.datosAdicionales.id;
				$scope.actualizarContenedor();
				$scope.actualizarPapeleras();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

		$location.path("/gestion");
	}

	$scope.listarAlmacenamiento = function() {

		var paramJson = {
			Almacenamiento : $scope.almacenamiento
		};

		$http({
			method : 'POST',
			url : urlListarAlmacenamiento,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.almacenamiento = data.datosAdicionales.almacenamiento;
							if ($scope.almacenamiento.almacenamiento_id != undefined) {
								$scope.flag = 1;
								if ($scope.almacenamiento.metal == 1) {
									$scope.almacenamiento.metal = true;
								}
								if ($scope.almacenamiento.fibra_vidrio == 1) {
									$scope.almacenamiento.fibra_vidrio = true;
								}
								if ($scope.almacenamiento.plastico == 1) {
									$scope.almacenamiento.plastico = true;
								}
								if ($scope.almacenamiento.mixto == 1) {
									$scope.almacenamiento.mixto = true;
								}
								$scope.contenedor.almacenamiento_id = $scope.almacenamiento.almacenamiento_id;
								$scope.papeleras.almacenamiento_id = $scope.almacenamiento.almacenamiento_id;
							}

							$scope.almacenamiento.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.almacenamiento.cod_usuario = '15';

							$scope.actTablaContenedor();
							$scope.actTablaPapeleras();

						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.actTablaContenedor = function() {

		$scope.tblContenedor = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"contenedor_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					Contenedor : $scope.contenedor
				};

				$http({
					method : 'POST',
					url : urlListarContenedor,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstContenedor;
					$scope.lstContenedor = data.datosAdicionales.LstContenedor;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarContenedor = function() {

		for (var x = 0; x < $scope.lstContenedor.length; x++) {
			$scope.lstContenedor[x].almacenamiento_id = $scope.contenedor.almacenamiento_id;
			$scope.lstContenedor[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarContenedor,
			data : JSON.stringify($scope.lstContenedor),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarContenedor = function() {

		for (var x = 0; x < $scope.lstContenedor.length; x++) {
			$scope.lstContenedor[x].almacenamiento_id = $scope.contenedor.almacenamiento_id;
			$scope.lstContenedor[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarContenedor,
			data : JSON.stringify($scope.lstContenedor),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actTablaPapeleras = function() {

		$scope.tblPapeleras = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"cant_papeleras_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					Papeleras : $scope.papeleras
				};

				$http({
					method : 'POST',
					url : urlListarPapeleras,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstPapeleras;
					$scope.lstPapeleras = data.datosAdicionales.LstPapeleras;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarPapeleras = function() {

		for (var x = 0; x < $scope.lstPapeleras.length; x++) {
			$scope.lstPapeleras[x].almacenamiento_id = $scope.papeleras.almacenamiento_id;
			$scope.lstPapeleras[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarPapeleras,
			data : JSON.stringify($scope.lstPapeleras),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarPapeleras = function() {

		for (var x = 0; x < $scope.lstPapeleras.length; x++) {
			$scope.lstPapeleras[x].almacenamiento_id = $scope.papeleras.almacenamiento_id;
			$scope.lstPapeleras[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarPapeleras,
			data : JSON.stringify($scope.lstPapeleras),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.getTotalAlmacenamiento = function() {
		var total = 0;
		var totalPapelera = 0;
		var totalContenedor = 0;

		var capacidad;
		
		if ($scope.lstPapeleras.length != null) {
			for (var i = 0; i < $scope.lstPapeleras.length; i++) {
				if ($scope.lstPapeleras[i].estado.estado_id != 3) {
					
					if($scope.lstPapeleras[i].mercados != null)
						totalPapelera += $scope.lstPapeleras[i].mercados;
						
					if($scope.lstPapeleras[i].parques != null)
						totalPapelera += $scope.lstPapeleras[i].mercados;
						
					if($scope.lstPapeleras[i].vias != null)
						totalPapelera += $scope.lstPapeleras[i].vias;
						
					if($scope.lstPapeleras[i].otros != null)
						totalPapelera += $scope.lstPapeleras[i].otros;
					
					/*totalPapelera = totalPapelera
							+ $scope.lstPapeleras[i].mercados
							+ $scope.lstPapeleras[i].parques
							+ $scope.lstPapeleras[i].vias
							+ $scope.lstPapeleras[i].otros;*/
				}
			}
		}

		if ($scope.lstContenedor.length != null) {
			for (var i = 0; i < $scope.lstContenedor.length; i++) {
				if ($scope.lstContenedor[i].tipo_contenedor.tipo_contenedor_id != 3
						|| $scope.lstContenedor[i].tipo_contenedor.tipo_contenedor_id != 6) {
					
					if($scope.lstContenedor[i].cantidad != null){
						totalContenedor += $scope.lstContenedor[i].cantidad;
					}
					
				}
			}
		}

		if($scope.almacenamiento.capacidad_papeleras == null){
			capacidad = 0;
		}
		else
			capacidad = $scope.almacenamiento.capacidad_papeleras;
		
		total = (totalPapelera * capacidad
				* 365 / 1000 * 0.75)
				+ (totalContenedor * 52);

		$scope.almacenamiento.total_recolectado = total;
		$localStorage.totalAlmacenamiento = total;
		return total;
	}

	$scope.volver = function() {
		$location.path("/gestion");
	}

	$scope.listarAlmacenamiento();
}