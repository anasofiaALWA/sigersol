function TransferenciaController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlRegistrarTransferencia = $scope.urlRest + "/rest/transferencia/registrar";
	var urlActualizarTransferencia = $scope.urlRest + "/rest/transferencia/actualizar";
	var urlListarTransferencia = $scope.urlRest + "/rest/transferencia/obtener";
	
	var urlRegistrarPropia = $scope.urlRest + "/rest/propia/registrar";
	var urlActualizarPropia = $scope.urlRest + "/rest/propia/actualizar";
	var urlListarPropia = $scope.urlRest + "/rest/propia/obtener";
	
	var urlRegistrarTerceros = $scope.urlRest + "/rest/terceros/registrar";
	var urlActualizarTerceros = $scope.urlRest + "/rest/terceros/actualizar";
	var urlListarTerceros = $scope.urlRest + "/rest/terceros/obtener";
	
	var urlRegistrarVehiculo = $scope.urlRest
		+ "/rest/vehiculoConvencional4/registrar";
	var urlActualizarVehiculo = $scope.urlRest
		+ "/rest/vehiculoConvencional4/actualizar";
	var urlListarVehiculo = $scope.urlRest
		+ "/rest/vehiculoConvencional4/listarVehiculoConvencional";

	//var urlRegistrarCamionesMadrina = $scope.urlRest + "/rest/camionesMadrina/registrar";
	//var urlActualizarCamionesMadrina = $scope.urlRest + "/rest/camionesMadrina/actualizar";
	//var urlListarCamionesMadrina = $scope.urlRest + "/rest/camionesMadrina/listarCamionesMadrina";
	
	
	var urlRegistrarTrabajadores = $scope.urlRest
		+ "/rest/trabajadoresTransferencia/registrar";
	var urlListarTrabajadores = $scope.urlRest
		+ "/rest/trabajadoresTransferencia/listarTrabajadores";
	var urlActualizarTrabajadores = $scope.urlRest 
		+ "/rest/trabajadoresTransferencia/actualizar";
	
	$scope.transferencia = {};
	
	//$scope.camiones_madrina = {};
	//$scope.lstCamionesMadrina = [];
	
	$scope.propia = {};
	
	$scope.terceros = {};

	$scope.vehiculoConvencional = {};
	$scope.lstVehiculoConvencional = [];
	
	$scope.lstVehiculoAux = [];
	
	$scope.trabajadores = {};
	$scope.lstTrabajador = [];
	
	$scope.lstTrabajadorAux = [];
	
	$scope.flag = 0;
	$scope.cantidad = 0;
	$scope.cantidadVehiculo = 0;
	
	$scope.transferencia.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.transferencia.cod_usuario = '15';
	
	$scope.terceros.municipalidad_id = $localStorage.MunicipalidadId;
	
	$scope.propia.cod_usuario = '15';
	$scope.terceros.cod_usuario = '15';
	
	//$scope.camiones_madrina.cod_usuario = '15';
	
	$localStorage.TRANSFERENCIA = '0';
	
	$scope.guardar = function() {
		if($scope.flag == 0)
			$scope.grabarTransferencia();
		else
			$scope.actualizarTransferencia();
	}
	
	$scope.grabarTransferencia = function() {
		
		var paramJson = {
			Transferencia : $scope.transferencia
		};

		$http({
			method : 'POST',
			url : urlRegistrarTransferencia,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$localStorage.TRANSFERENCIA = '1';
				
				$scope.propia.cod_usuario = '15';
				$scope.terceros.cod_usuario = '15';
				
				$scope.vehiculoConvencional.transferencia_id = data.datosAdicionales.id;
				$scope.trabajadores.transferencia_id = data.datosAdicionales.id;
				
				$scope.terceros.municipalidad_id = $localStorage.MunicipalidadId;
				
				if($scope.transferencia.adm_serv_id == 'TER'){	
					$scope.terceros.transferencia_id = data.datosAdicionales.id;
					$scope.grabarTerceros();
				}
				
				if($scope.lstVehiculoConvencional.length > 0)
					$scope.grabarVehiculoConvencional();
				if($scope.lstTrabajador.length > 0)
					$scope.grabarTrabajadores();
				console.log("Transferencia Flag: " + $scope.transferencia.adm_serv_id);
				
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}	
	
	$scope.actualizarTransferencia = function() {
		
		var paramJson = {
			Transferencia : $scope.transferencia
		};
		
		$http({
			method : 'POST',
			url : urlActualizarTransferencia,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				
				$scope.propia.cod_usuario = '15';
				$scope.terceros.cod_usuario = '15';
				
				/*if($scope.transferencia.adm_serv_id == 1){	
					$scope.propia.transferencia_id = data.datosAdicionales.id;
					$scope.actualizarPropia();
				}*/
				
				$scope.terceros.municipalidad_id = $localStorage.MunicipalidadId;
				
				if($scope.transferencia.adm_serv_id == 'TER'){	
					$scope.terceros.transferencia_id = data.datosAdicionales.id;
					$scope.actualizarTerceros();
				}
				
				$scope.trabajadores.transferencia_id = data.datosAdicionales.id;
				$scope.vehiculoConvencional.transferencia_id = data.datosAdicionales.id;
				if($scope.lstTrabajadorAux.length > 0)
					$scope.actualizarTrabajadores();
				if($scope.lstVehiculoAux.length > 0)
					$scope.actualizarVehiculoConvencional();
				//$scope.camiones_madrina.transferencia_id = data.datosAdicionales.id;
				//$scope.actualizarCamionesMadrina();	
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}
	
	$scope.listarTransferencia = function() {
		
		var paramJson = {
			Transferencia : $scope.transferencia
		};
		
		$http({
			method : 'POST',
			url : urlListarTransferencia,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.transferencia = data.datosAdicionales.transferencia;
			
			if($scope.transferencia.transferencia_id != undefined){	
				$scope.flag = 1;
				//$scope.propia.transferencia_id = $scope.transferencia.transferencia_id;
				$scope.terceros.transferencia_id = $scope.transferencia.transferencia_id;
				$scope.vehiculoConvencional.transferencia_id = $scope.transferencia.transferencia_id;
				$scope.trabajadores.transferencia_id = $scope.transferencia.transferencia_id;
			}
			
			$scope.transferencia.ciclo_grs_id = $localStorage.GRS_ID;
			$scope.transferencia.cod_usuario = '15';
			
			/*if($scope.transferencia.adm_serv_id == 2){
				$scope.listarTerceros();
			}*/
			
			//$scope.camiones_madrina.cod_usuario = '15';
			//$scope.actTablaCamionesMadrina();
			$scope.actTablaVehiculoConvencional();
			$scope.actTablaTrabajador();
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.grabarPropia = function() {
		
		var paramJson = {
			Propia : $scope.propia
		};

		$http({
			method : 'POST',
			url : urlRegistrarPropia,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}	
	
	$scope.actualizarPropia = function() {
		
		var paramJson = {
			Propia : $scope.propia
		};
		
		$http({
			method : 'POST',
			url : urlActualizarPropia,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}
	
	$scope.listarPropia = function() {
		
		var paramJson = {
			Propia : $scope.propia
		};
		
		$http({
			method : 'POST',
			url : urlListarPropia,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.propia = data.datosAdicionales.propia;
			$scope.propia.cod_usuario = '15';
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	
	
	$scope.grabarTerceros = function() {
		
		console.log("Fecha " + $scope.terceros.fecha_inicio_vigencia );
		
		var paramJson = {
			Terceros : $scope.terceros
		};

		$http({
			method : 'POST',
			url : urlRegistrarTerceros,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}	
	
	$scope.actualizarTerceros = function() {
		
		var paramJson = {
			Terceros : $scope.terceros
		};
		
		$http({
			method : 'POST',
			url : urlActualizarTerceros,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}
	
	$scope.listarTerceros = function() {
		
		var paramJson = {
			Terceros : $scope.terceros
		};
		
		$http({
			method : 'POST',
			url : urlListarTerceros,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.terceros = data.datosAdicionales.terceros;
			$scope.terceros.cod_usuario = '15';
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	
	$scope.agregar = function() {
			
		$scope.lstVehiculoConvencional.push({ 
			'placa':'',
            'tipo_vehiculo':'',
            'anio_fabricacion':null,
            'capacidad':null,
            'tipo_combustible':null,
            'recorrido_anual': null,
            'cantidad_combustible': null,
            'costo_anual': null
        });
		
		if($scope.flag > 0){
			 
			 $scope.lstVehiculoAux.push({ 
				'placa':'',
		        'tipo_vehiculo':'',
		        'anio_fabricacion':null,
		        'capacidad':null,
		        'tipo_combustible':null,
		        'recorrido_anual': null,
		        'cantidad_combustible': null,
		        'costo_anual': null
		     });
		 }
		
	}
	
	
	/*$scope.actTablaCamionesMadrina = function() {

		$scope.tblCamionesMadrina = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"contenedor_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					CamionesMadrina : $scope.camiones_madrina
				};

				$http({
					method : 'POST',
					url : urlListarCamionesMadrina,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstCamionesMadrina;
					$scope.lstCamionesMadrina = data.datosAdicionales.LstCamionesMadrina;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarCamionesMadrina = function() {

		for (var x = 0; x < $scope.lstCamionesMadrina.length; x++) {
			$scope.lstCamionesMadrina[x].transferencia_id = $scope.camiones_madrina.transferencia_id;
			$scope.lstCamionesMadrina[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCamionesMadrina,
			data : JSON.stringify($scope.lstCamionesMadrina),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.actualizarCamionesMadrina = function() {

		for (var x = 0; x < $scope.lstCamionesMadrina.length; x++) {
			$scope.lstCamionesMadrina[x].transferencia_id = $scope.camiones_madrina.transferencia_id;
			$scope.lstCamionesMadrina[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarCamionesMadrina,
			data : JSON.stringify($scope.lstCamionesMadrina),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}*/
	
	$scope.actTablaVehiculoConvencional = function() {

		$scope.tblVehiculoConvencional = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"vehiculo_convencional_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					VehiculoConvencional : $scope.vehiculoConvencional
				};

				$http({
					method : 'POST',
					url : urlListarVehiculo,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstVehiculoConvencional;
					$scope.lstVehiculoConvencional = data.datosAdicionales.LstVehiculoConvencional;
					//$scope.lstComb = data.datosAdicionales.LstVehiculoConvencional;
					//$scope.lstRecogidos = data.datosAdicionales.LstVehiculoConvencional;
					
					$scope.cantidadVehiculo = $scope.lstVehiculoConvencional.length;
					console.log("Cantidad Vehiculo: " + $scope.cantidadVehiculo);
					
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarVehiculoConvencional = function() {
		
		for (var x = 0; x < $scope.lstVehiculoConvencional.length; x++) {
			$scope.lstVehiculoConvencional[x].transferencia_id = $scope.vehiculoConvencional.transferencia_id;
			$scope.lstVehiculoConvencional[x].cod_usuario = '14';
		}
		
		$http({
			method : 'POST',
			url : urlRegistrarVehiculo,
			data : JSON.stringify($scope.lstVehiculoConvencional),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarVehiculoConvencional = function() {

		/*for (var x = 0; x < $scope.lstVehiculoConvencional.length; x++) {
			$scope.lstVehiculoConvencional[x].transferencia_id = $scope.vehiculoConvencional.transferencia_id;
			$scope.lstVehiculoConvencional[x].cod_usuario = '14';
		}*/
		
		var i = $scope.cantidadVehiculo;
		var c = 0;
		
		for (var x = i; x < $scope.lstVehiculoConvencional.length; x++) {
			$scope.lstVehiculoConvencional[x].transferencia_id = $scope.vehiculoConvencional.transferencia_id;
			$scope.lstVehiculoConvencional[x].cod_usuario = '14';
			
			$scope.lstVehiculoAux[c].transferencia_id = $scope.lstVehiculoConvencional[x].transferencia_id;
			
			$scope.lstVehiculoAux[c].placa = $scope.lstVehiculoConvencional[x].placa;
			$scope.lstVehiculoAux[c].tipo_vehiculo = $scope.lstVehiculoConvencional[x].tipo_vehiculo;
			$scope.lstVehiculoAux[c].anio_fabricacion = $scope.lstVehiculoConvencional[x].anio_fabricacion;
			$scope.lstVehiculoAux[c].capacidad = $scope.lstVehiculoConvencional[x].capacidad;
			$scope.lstVehiculoAux[c].tipo_combustible = $scope.lstVehiculoConvencional[x].tipo_combustible;
			$scope.lstVehiculoAux[c].recorrido_anual = $scope.lstVehiculoConvencional[x].recorrido_anual;
			$scope.lstVehiculoAux[c].cantidad_combustible = $scope.lstVehiculoConvencional[x].cantidad_combustible;
			$scope.lstVehiculoAux[c].costo_anual = $scope.lstVehiculoConvencional[x].costo_anual;
			
			$scope.lstVehiculoAux[c].cod_usuario = $scope.lstVehiculoConvencional[x].cod_usuario;
			c++;
		}
		
		$http({
			method : 'POST',
			url : urlActualizarVehiculo,
			data : JSON.stringify($scope.lstVehiculoAux),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.insertar = function(){
		 $scope.lstTrabajador.push({ 
	       	'dni': $scope.trabajadores.dni,
	        'nom_apellido': $scope.trabajadores.nom_apellido,
	        'sexo': $scope.trabajadores.sexo,
	        'rango': $scope.trabajadores.rango,
	        'cargo': $scope.trabajadores.cargo,
	        'laboral': $scope.trabajadores.laboral,
	        'horario': $scope.trabajadores.horario,
	        'dias_trabajados': $scope.trabajadores.dias_trabajados
	    });
		
		 $scope.trabajadores.dni = null;
		 $scope.trabajadores.nom_apellido = null;
		 $scope.trabajadores.sexo = null;
		 $scope.trabajadores.rango = null;
		 $scope.trabajadores.cargo = null;
		 $scope.trabajadores.laboral = null;
		 $scope.trabajadores.horario = null;
		 $scope.trabajadores.dias_trabajados = null;
		 
		 if($scope.flag > 0){
			 
			 $scope.lstTrabajadorAux.push({ 
		        'dni': $scope.trabajadores.dni,
		        'nom_apellido': $scope.trabajadores.nom_apellido,
		        'sexo': $scope.trabajadores.sexo,
		        'rango': $scope.trabajadores.rango,
		        'cargo': $scope.trabajadores.cargo,
		        'laboral': $scope.trabajadores.laboral,
		        'horario': $scope.trabajadores.horario,
		        'dias_trabajados': $scope.trabajadores.dias_trabajados
		     });
		 }
	}

	$scope.borrar = function(index){
		$scope.lstTrabajador.splice(index, 1);
	};

	app.directive('editableTd', [function() {
	   return {
	      restrict: 'A',
	      link: function(scope, element, attrs) {
	         element.css("cursor", "pointer");
	         element.attr('contenteditable', 'true'); // Referencia: Inciso 1
	         element.bind('blur keyup change', function() { // Referencia: Inciso 2
	            scope.lstTrabajador[attrs.row][attrs.field] = element.text();
	         });
	         element.bind('click', function() { // Referencia: Inciso 3
	            document.execCommand('selectAll', false, null)
	         });
	      }
	   };
	}]);


	/*$scope.insertar = function() {
		
		$scope.grabarTrabajadores();
	}*/
	
	$scope.grabarPersona = function() {
		
		for (var x = 0; x < $scope.lstBarredor.length; x++) {
			$scope.lstTrabajador[x].cod_usuario = '14';
		}
	
		$http({
			method : 'POST',
			url : urlRegistrarPersona,
			data : JSON.stringify($scope.lstBarredor),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.grabarTrabajadores();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
	
		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.grabarTrabajadores = function() {
		
		for (var x = 0; x < $scope.lstTrabajador.length; x++) {
			$scope.lstTrabajador[x].transferencia_id = $scope.trabajadores.transferencia_id;
			$scope.lstTrabajador[x].cod_usuario = '14';
		}
	
		$http({
			method : 'POST',
			url : urlRegistrarTrabajadores,
			data : JSON.stringify($scope.lstTrabajador),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
	
		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.actualizarTrabajadores = function() {
		
		var i = $scope.cantidad;
		var c = 0;
		
		for (var x = i; x < $scope.lstTrabajador.length; x++) {
			$scope.lstTrabajador[x].transferencia_id = $scope.trabajadores.transferencia_id;
			$scope.lstTrabajador[x].cod_usuario = '14';
			
			$scope.lstTrabajadorAux[c].transferencia_id = $scope.lstTrabajador[x].transferencia_id;
			
			$scope.lstTrabajadorAux[c].dni = $scope.lstTrabajador[x].dni;
			$scope.lstTrabajadorAux[c].nom_apellido = $scope.lstTrabajador[x].nom_apellido;
			$scope.lstTrabajadorAux[c].sexo = $scope.lstTrabajador[x].sexo;
			$scope.lstTrabajadorAux[c].rango = $scope.lstTrabajador[x].rango;
			$scope.lstTrabajadorAux[c].cargo = $scope.lstTrabajador[x].cargo;
			$scope.lstTrabajadorAux[c].laboral = $scope.lstTrabajador[x].laboral;
			$scope.lstTrabajadorAux[c].horario = $scope.lstTrabajador[x].horario;
			$scope.lstTrabajadorAux[c].dias_trabajados = $scope.lstTrabajador[x].dias_trabajados;
			
			$scope.lstTrabajadorAux[c].cod_usuario = $scope.lstTrabajador[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarTrabajadores,
			data : JSON.stringify($scope.lstTrabajadorAux),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.actTablaTrabajador = function() {
	
		$scope.tblTrabajador = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"trabajadores_id" : "asc"
			}
		}, {
			getData : function($defer, params) {
				
				var paramJson = {
					Trabajadores : $scope.trabajadores
				};
	
				$http({
					method : 'POST',
					url : urlListarTrabajadores,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstTrabajadores;
					$scope.lstTrabajador = data.datosAdicionales.LstTrabajadores;
					
					for (var x = 0; x < $scope.lstTrabajador.length; x++) {
						
						$scope.lstTrabajador[x].dni = $scope.lstTrabajador[x].persona.dni;
						$scope.lstTrabajador[x].nom_apellido = $scope.lstTrabajador[x].persona.nom_apellidos;
						$scope.lstTrabajador[x].sexo = $scope.lstTrabajador[x].persona.sexo;
						$scope.lstTrabajador[x].rango = $scope.lstTrabajador[x].persona.rango_edad;
						$scope.lstTrabajador[x].cargo = $scope.lstTrabajador[x].cargo;
						$scope.lstTrabajador[x].laboral = $scope.lstTrabajador[x].condicion_laboral;
						$scope.lstTrabajador[x].horario = $scope.lstTrabajador[x].horario_trabajo;
						$scope.lstTrabajador[x].dias_trabajados = $scope.lstTrabajador[x].dias_trabajados;
					}
					
					$scope.cantidad = $scope.lstTrabajador.length;
					console.log("Cantidad: " + $scope.cantidad);
					
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	$scope.volver = function() {
		$location.path("/gestion");
	}
	
	$scope.listarTransferencia();
	$scope.listarTerceros();
}