function paginaPrincipalController($scope, $http, $routeParams, $location, $rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES, $localStorage,
		APIservice, alertService, blockUI, $window) {

	$controller('funcGenerales', {
		$scope : $scope
	});
	
	
	$scope.reporteFichaAM = function() {
		var  urlreporteFichaAM = $scope.urlReporte + "/rest/reporte/reporteFichaAM";
		//var  urlreporteFichaAM = "http://localhost:8093/prj-srv-copere-agregados/rest/reporte/reporteFichaAM";
				
		$http(
				{
					method : 'POST',
					url : urlreporteFichaAM,
					data : $.param({
						"cip" : "113817000"//$scope.CIP
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					},
				}
		)
		.success(
			function(data) {
				if (String(data).indexOf(".pdf") != -1) {
					window.open($scope.urlBase + data);					
				} else {
					$scope.summitEvaluar(data);
				}
			}
		).error(
				//window.open("<html><head><title>Pagina de error</title><body><h2>Se produjo un error inesperado</h2></body></html>");	
				/*pw.println("<html>");
				pw.println("<head><title>Pagina de error</title></title>");
				pw.println("<body>");
				pw.println("<h2>Se produjo un error inesperado</h2>");
				pw.println("</body></html>");*/
		);

	}

	
}