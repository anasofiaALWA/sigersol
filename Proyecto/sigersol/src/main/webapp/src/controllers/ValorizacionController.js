function ValorizacionController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlRegistrarValorizacion = $scope.urlRest + "/rest/valorizacion/registrar";
	var urlActualizarValorizacion = $scope.urlRest + "/rest/valorizacion/actualizar";
	var urlListarValorizacion = $scope.urlRest + "/rest/valorizacion/obtener";

	var urlRegistrarValorizacion1 = $scope.urlRest + "/rest/valorizacion1/registrar";
	var urlListarValorizacion1 = $scope.urlRest + "/rest/valorizacion1/obtener";
	var urlActualizarValorizacion1 = $scope.urlRest + "/rest/valorizacion1/actualizar";
	
	var urlActualizarValorizacion2 = $scope.urlRest + "/rest/valorizacion2/actualizar";
	var urlListarValorizacion2 = $scope.urlRest + "/rest/valorizacion2/obtener";
	
	var urlListarResiduos_Aprovechados = $scope.urlRest + "/rest/residuosAprovechados/listarResiduosAprovechados";
	var urlActualizarResiduos_Aprovechados = $scope.urlRest + "/rest/residuosAprovechados/actualizar";
	var urlRegistrarResiduos_Aprovechados = $scope.urlRest + "/rest/residuosAprovechados/registrar";
		
	var urlListarResiduosOrganicos = $scope.urlRest + "/rest/residuosOrganicos/listarResiduosOrganicos";
	var urlActualizarResiduosOrganicos = $scope.urlRest + "/rest/residuosOrganicos/actualizar";
	var urlRegistrarResiduosOrganicos = $scope.urlRest + "/rest/residuosOrganicos/registrar";
	
	var urlListarProductosObtenidos = $scope.urlRest + "/rest/productosObtenidos/listarProductosObtenidos";
	var urlActualizarProductosObtenidos = $scope.urlRest + "/rest/productosObtenidos/actualizar";
	var urlRegistrarProductosObtenidos = $scope.urlRest + "/rest/productosObtenidos/registrar";
	
	var urlRegistrarAcopio = $scope.urlRest + "/rest/centroAcopio/registrar";
	var urlActualizarAcopio = $scope.urlRest + "/rest/centroAcopio/actualizar";
	var urlListarAcopio = $scope.urlRest + "/rest/centroAcopio/obtener";
	
	var urlRegistrarPlanta = $scope.urlRest + "/rest/plantaValorizacion/registrar";
	var urlActualizarPlanta = $scope.urlRest + "/rest/plantaValorizacion/actualizar";
	var urlListarPlanta = $scope.urlRest + "/rest/plantaValorizacion/obtener";
	
	
	var urlRegistrarLugarAprovechamiento = $scope.urlRest + "/rest/lugarAprovechamiento/registrar";
	var urlActualizarLugarAprovechamiento = $scope.urlRest + "/rest/lugarAprovechamiento/actualizar";
	var urlListarLugarAprovechamiento = $scope.urlRest + "/rest/lugarAprovechamiento/obtener";
	
	var urlRegistrarLugarAprovechamiento2 = $scope.urlRest + "/rest/lugarAprovechamiento2/registrar";
	var urlActualizarLugarAprovechamiento2 = $scope.urlRest + "/rest/lugarAprovechamiento2/actualizar";
	var urlListarLugarAprovechamiento2 = $scope.urlRest + "/rest/lugarAprovechamiento2/obtener";
	
	var urlActualizacionAprovechamiento = $scope.urlRest + "/rest/lugarAprovechamiento/actualizacion";
	
	var urlRegistrarUbicacion = $scope.urlRest + "/rest/ubicacion/registrar";
	var urlListarUbicacion = $scope.urlRest + "/rest/ubicacion/listarCoordenada";
	
	var urlRegistrarSegregacion = $scope.urlRest + "/rest/programaSegregacion/registrar";
	var urlActualizarSegregacion = $scope.urlRest + "/rest/programaSegregacion/actualizar";
	var urlListarSegregacion = $scope.urlRest + "/rest/programaSegregacion/obtener";

	
	var urlListarCostoReferencial = $scope.urlRest + "/rest/costoReferencial/listarCostoReferencial";
	var urlActualizarCostoReferencial = $scope.urlRest + "/rest/costoReferencial/actualizar";
	var urlRegistrarCostoReferencial = $scope.urlRest + "/rest/costoReferencial/registrar";
	
	
	var urlListarResiduosCiclo = $scope.urlRest + "/rest/residuosCiclo/obtener";
	var urlActualizarResiduosCiclo = $scope.urlRest + "/rest/residuosCiclo/actualizar";
	var urlRegistrarResiduosCiclo = $scope.urlRest + "/rest/residuosCiclo/registrar";
	
	var urlRegistrarTrabajadores = $scope.urlRest
		+ "/rest/trabajadoresValorizacion/registrar";
	var urlListarTrabajadores = $scope.urlRest
		+ "/rest/trabajadoresValorizacion/listarTrabajadores";
	var urlActualizarTrabajadores = $scope.urlRest 
		+ "/rest/trabajadoresValorizacion/actualizar";
	
	$scope.valorizacion = {};
	$scope.valorizacion1 = {};
	$scope.valorizacion2 = {};
	$scope.residuosAprovechados = {};
	$scope.segregacion = {};
	$scope.residuosOrganicos = {};
	$scope.productosObtenidos = {};
	$scope.acopio = {};
	$scope.planta = {};
	$scope.ubicacion = {};
	
	$scope.lstValorizacion = [];
	$scope.lstResiduosAprovechados = [];
	$scope.lstSegregacion = [];
	$scope.lstResiduosOrganicos = [];
	$scope.lstProductosObtenidos = [];
	
	$scope.costoReferencial = {};
	$scope.lstCostoReferencial = [];
	
	$scope.residuosCiclo = {};
	$scope.lstResiduosCiclo = [];
	
	$scope.trabajadores = {};
	$scope.lstTrabajador = [];
	
	$scope.lstTrabajadorAux = [];
	$scope.lstCoordenadas = [];
	
	$scope.flag = 0;
	$scope.flag_recoleccion = 0;

	$scope.flag1 = 0;
	$scope.flag2 = 0;
	$scope.flag3 = 0;
	$scope.flag4 = 0;
	$scope.flag5 = 0;
	$scope.flag6 = 0;
	$scope.flag7 = 0;
	$scope.flag8 = 0;
	
	$scope.valorizacion.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.valorizacion.cod_usuario = '12';
	$scope.valorizacion1.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.valorizacion1.cod_usuario = '12';
	$scope.valorizacion2.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.valorizacion2.cod_usuario = '12';
	$scope.residuosAprovechados.cod_usuario = '15';
	$scope.ubicacion.cod_usuario = '15';
	$scope.segregacion.cod_usuario = '15';
	$scope.trabajadores.cod_usuario = '15';
	
	$localStorage.VALORIZACION = '0';

	$scope.guardar = function() {
		if ($scope.flag == 0)
			$scope.grabarValorizacion();
		else
			$scope.actualizarValorizacion();
	}

	$scope.grabarValorizacion = function() {

		var paramJson = {
			Valorizacion : $scope.valorizacion
		};

		$http({
			method : 'POST',
			url : urlRegistrarValorizacion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.residuosAprovechados.valorizacion_id = data.datosAdicionales.id;
								$scope.residuosOrganicos.valorizacion_id = data.datosAdicionales.id;
								$scope.productosObtenidos.valorizacion_id = data.datosAdicionales.id;
								$scope.acopio.valorizacion_id = data.datosAdicionales.id;
								$scope.planta.valorizacion_id = data.datosAdicionales.id;
								$scope.costoReferencial.valorizacion_id = data.datosAdicionales.id;
								$scope.residuosCiclo.valorizacion_id = data.datosAdicionales.id;
								$scope.trabajadores.valorizacion_id = data.datosAdicionales.id;
								//$scope.segregacion.valorizacion_id = data.datosAdicionales.id;
								$scope.grabarResiduosAprovechados();
								$scope.grabarResiduosOrganicos();
								$scope.grabarProductosObtenidos();
								$scope.grabarAcopio();
								$scope.grabarPlanta();
								$scope.grabarCostoReferencial();
								//$scope.grabarResiduosCiclo();
								$scope.grabarTrabajadores();
								//$scope.grabarSegregacion();
								$localStorage.VALORIZACION = '1';
								$location.path("/gestion");

							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarValorizacion = function() {

		var paramJson = {
			Valorizacion : $scope.valorizacion
		};

		$http({
			method : 'POST',
			url : urlActualizarValorizacion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.residuosAprovechados.valorizacion_id = data.datosAdicionales.id;
								$scope.residuosOrganicos.valorizacion_id = data.datosAdicionales.id;
								$scope.productosObtenidos.valorizacion_id = data.datosAdicionales.id;
								$scope.acopio.valorizacion_id = data.datosAdicionales.id;
								$scope.planta.valorizacion_id = data.datosAdicionales.id;
								$scope.costoReferencial.valorizacion_id = data.datosAdicionales.id;
								$scope.residuosCiclo.valorizacion_id = data.datosAdicionales.id;
								$scope.trabajadores.valorizacion_id = data.datosAdicionales.id;
								//$scope.segregacion.valorizacion_id = data.datosAdicionales.id;	
								$scope.actualizarResiduosAprovechados();
								$scope.actualizarResiduosOrganicos();
								$scope.actualizarProductosObtenidos();
								$scope.actualizarAcopio();
								$scope.actualizarPlanta();
								$scope.actualizarCostoReferencial();
								$scope.actualizarTrabajadores();
								//$scope.actualizarResiduosCiclo();
								//$scope.actualizarSegregacion();
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		$location.path("/gestion");
	}

	$scope.listarValorizacion = function() {

		$scope.flag_recoleccion = $localStorage.FLAG_RECOLECCION;
		
		var paramJson = {
			Valorizacion : $scope.valorizacion
		};

		$http({
			method : 'POST',
			url : urlListarValorizacion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.valorizacion = data.datosAdicionales.valorizacion;
							if ($scope.valorizacion.valorizacion_id != undefined) {
								$scope.flag = 1;
								
								if($scope.valorizacion.compostaje == 1){
									$scope.valorizacion.compostaje = true;
								}
								if($scope.valorizacion.biodigestion == 1){
									$scope.valorizacion.biodigestion = true;
								}
								if($scope.valorizacion.lombricultura == 1){
									$scope.valorizacion.lombricultura = true;
								}
								if($scope.valorizacion.otros_actividades == 1){
									$scope.valorizacion.otros_actividades = true;
								}
								
								
								$scope.residuosAprovechados.valorizacion_id = $scope.valorizacion.valorizacion_id;
								$scope.residuosOrganicos.valorizacion_id = $scope.valorizacion.valorizacion_id;
								$scope.productosObtenidos.valorizacion_id = $scope.valorizacion.valorizacion_id;
								$scope.acopio.valorizacion_id = $scope.valorizacion.valorizacion_id;
								$scope.planta.valorizacion_id = $scope.valorizacion.valorizacion_id;
								$scope.costoReferencial.valorizacion_id = $scope.valorizacion.valorizacion_id;
								$scope.residuosCiclo.valorizacion_id = $scope.valorizacion.valorizacion_id;
								$scope.trabajadores.valorizacion_id = $scope.valorizacion.valorizacion_id;
								//$scope.segregacion.valorizacion_id = $scope.valorizacion.valorizacion_id;
							} else {
								
							}

							$scope.valorizacion.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.valorizacion.cod_usuario = '12';	
							$scope.trabajadores.cod_usuario = '15';
							$scope.actTablaResiduosAprovechados();
							$scope.actTablaResiduosOrganicos();
							$scope.actTablaProductosObtenidos();
							$scope.listarAcopio();
							$scope.listarPlanta();
							$scope.actTablaCostoReferencial();
							//$scope.listarResiduosCiclo();
							$scope.actTablaTrabajador();
							//$scope.listarSegregacion();
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	$scope.listarValorizacion1 = function() {

		$scope.flag_recoleccion = $localStorage.FLAG_RECOLECCION;
		
		var paramJson = {
			Valorizacion1 : $scope.valorizacion1
		};

		$http({
			method : 'POST',
			url : urlListarValorizacion1,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.valorizacion1 = data.datosAdicionales.valorizacion;
							if($scope.valorizacion1.valorizacion_id != undefined){
								$scope.flag1 = 1;
								$scope.valorizacion1.valorizacion_id = $scope.valorizacion1.valorizacion_id
								$scope.valorizacion2.valorizacion_id = $scope.valorizacion1.valorizacion_id
								$scope.acopio.valorizacion_id = $scope.valorizacion1.valorizacion_id;
								$scope.planta.valorizacion_id = $scope.valorizacion1.valorizacion_id;
								$scope.costoReferencial.valorizacion_id = $scope.valorizacion1.valorizacion_id;
								$scope.trabajadores.valorizacion_id = $scope.valorizacion1.valorizacion_id;
							}
							$scope.valorizacion1.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.valorizacion1.cod_usuario = '12';
							
							$scope.listarValorizacion2();
							//$scope.listarAcopio();
							//$scope.listarPlanta();
							$scope.listarLugarAprovechamiento();
							$scope.listarLugarAprovechamiento2();
							$scope.actTablaCostoReferencial();
							$scope.actTablaTrabajador();
							
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	$scope.listarValorizacion2 = function() {

		$scope.flag_recoleccion = $localStorage.FLAG_RECOLECCION;
		
		var paramJson = {
			Valorizacion2 : $scope.valorizacion2
		};

		$http({
			method : 'POST',
			url : urlListarValorizacion2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.valorizacion2 = data.datosAdicionales.valorizacion;
							if ($scope.valorizacion2.valorizacion_id != undefined) {
								$scope.flag = 1;
								
								if($scope.valorizacion2.compostaje == 1){
									$scope.valorizacion2.compostaje = true;
								}
								if($scope.valorizacion2.biodigestion == 1){
									$scope.valorizacion2.biodigestion = true;
								}
								if($scope.valorizacion2.lombricultura == 1){
									$scope.valorizacion2.lombricultura = true;
								}
								if($scope.valorizacion2.otros_actividades == 1){
									$scope.valorizacion2.otros_actividades = true;
								}
								
								
								$scope.residuosAprovechados.valorizacion_id = $scope.valorizacion2.valorizacion_id;
								$scope.residuosOrganicos.valorizacion_id = $scope.valorizacion2.valorizacion_id;
								$scope.productosObtenidos.valorizacion_id = $scope.valorizacion2.valorizacion_id;

							} else {
								
							}

							$scope.valorizacion2.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.valorizacion2.cod_usuario = '12';	
							$scope.actTablaResiduosAprovechados();
							$scope.actTablaResiduosOrganicos();
							$scope.actTablaProductosObtenidos();
						
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	$scope.grabarAcopio = function() {

		var paramJson = {
			Acopio : $scope.acopio
		};

		$http({
			method : 'POST',
			url : urlRegistrarAcopio,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarAcopio = function() {

		var paramJson = {
			Acopio : $scope.acopio
		};

		$http({
			method : 'POST',
			url : urlActualizarAcopio,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.listarAcopio = function() {

		console.log("Ingreso Acopio");
		
		
		var paramJson = {
			Acopio : $scope.acopio
		};

		$http({
			method : 'POST',
			url : urlListarAcopio,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.acopio = data.datosAdicionales.acopio;							
							$scope.acopio.cod_usuario = '15';	
						
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	
	
	
	$scope.grabarPlanta = function() {

		var paramJson = {
			Planta : $scope.planta
		};

		$http({
			method : 'POST',
			url : urlRegistrarPlanta,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarPlanta = function() {

		var paramJson = {
			Planta : $scope.planta
		};

		$http({
			method : 'POST',
			url : urlActualizarPlanta,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.listarPlanta = function() {

		console.log("Ingreso a planta");
		
		var paramJson = {
			Planta : $scope.planta
		};

		$http({
			method : 'POST',
			url : urlListarPlanta,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.planta = data.datosAdicionales.planta;							
							$scope.planta.cod_usuario = '15';	
						
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	
	$scope.actTablaResiduosAprovechados = function() {

		$scope.tblResiduosAprovechados = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"residuos_aprovechados_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					ResiduosAprovechados : $scope.residuosAprovechados
				};

				$http({
					method : 'POST',
					url : urlListarResiduos_Aprovechados,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstResiduosAprovechados;
					$scope.lstResiduosAprovechados = data.datosAdicionales.LstResiduosAprovechados;
					
					console.log("Valorizacion RA " + $scope.residuosAprovechados.valorizacion_id);
					
					if($scope.residuosAprovechados.valorizacion_id != undefined){
						$scope.flag2 = 1;
					}
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarResiduosAprovechados = function() {

		for (var x = 0; x < $scope.lstResiduosAprovechados.length; x++) {
			$scope.lstResiduosAprovechados[x].valorizacion_id = $scope.residuosAprovechados.valorizacion_id;
			$scope.lstResiduosAprovechados[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarResiduos_Aprovechados,
			data : JSON.stringify($scope.lstResiduosAprovechados),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarResiduosAprovechados = function() {

		for (var x = 0; x < $scope.lstResiduosAprovechados.length; x++) {
			$scope.lstResiduosAprovechados[x].valorizacion_id = $scope.residuosAprovechados.valorizacion_id;
			$scope.lstResiduosAprovechados[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarResiduos_Aprovechados,
			data : JSON.stringify($scope.lstResiduosAprovechados),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	
	
	
	$scope.actTablaResiduosOrganicos = function() {

		$scope.tblResiduosOrganicos = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"residuos_organicos_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					ResiduosOrganicos : $scope.residuosOrganicos
				};

				$http({
					method : 'POST',
					url : urlListarResiduosOrganicos,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstResiduosOrganicos;
					$scope.lstResiduosOrganicos = data.datosAdicionales.LstResiduosOrganicos;
					if($scope.residuosOrganicos.valorizacion_id != undefined){
						$scope.flag3 = 1;
					}
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarResiduosOrganicos = function() {

		for (var x = 0; x < $scope.lstResiduosOrganicos.length; x++) {
			$scope.lstResiduosOrganicos[x].valorizacion_id = $scope.residuosOrganicos.valorizacion_id;
			$scope.lstResiduosOrganicos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarResiduosOrganicos,
			data : JSON.stringify($scope.lstResiduosOrganicos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarResiduosOrganicos = function() {

		for (var x = 0; x < $scope.lstResiduosOrganicos.length; x++) {
			$scope.lstResiduosOrganicos[x].valorizacion_id = $scope.residuosOrganicos.valorizacion_id;
			$scope.lstResiduosOrganicos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarResiduosOrganicos,
			data : JSON.stringify($scope.lstResiduosOrganicos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	
	$scope.actTablaProductosObtenidos = function() {

		$scope.tblProductosObtenidos = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"productos_obtenidos_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					ProductosObtenidos : $scope.productosObtenidos
				};

				$http({
					method : 'POST',
					url : urlListarProductosObtenidos,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstProductosObtenidos;
					$scope.lstProductosObtenidos = data.datosAdicionales.LstProductosObtenidos;
					if($scope.productosObtenidos.valorizacion_id != undefined ){
						$scope.flag4 = 1;
					}
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarProductosObtenidos = function() {

		for (var x = 0; x < $scope.lstProductosObtenidos.length; x++) {
			$scope.lstProductosObtenidos[x].valorizacion_id = $scope.productosObtenidos.valorizacion_id;
			$scope.lstProductosObtenidos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarProductosObtenidos,
			data : JSON.stringify($scope.lstProductosObtenidos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarProductosObtenidos = function() {

		for (var x = 0; x < $scope.lstProductosObtenidos.length; x++) {
			$scope.lstProductosObtenidos[x].valorizacion_id = $scope.productosObtenidos.valorizacion_id;
			$scope.lstProductosObtenidos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarProductosObtenidos,
			data : JSON.stringify($scope.lstProductosObtenidos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	
	
	
	$scope.grabarSegregacion = function() {
		
		var paramJson = {
			Segregacion : $scope.segregacion
		};

		$scope.segregacion.cod_usuario = '15';
		
		$http({
			method : 'POST',
			url : urlRegistrarSegregacion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
		
		
	}	
	
	$scope.actualizarSegregacion = function() {
		
		var paramJson = {
			Segregacion : $scope.segregacion
		};
		
		$scope.segregacion.cod_usuario = '15';
		
		$http({
			method : 'POST',
			url : urlActualizarSegregacion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}
	
	$scope.listarSegregacion = function() {
		
		var paramJson = {
			Segregacion : $scope.segregacion
		};
		
		$http({
			method : 'POST',
			url : urlListarSegregacion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.segregacion = data.datosAdicionales.segregacion;
			$scope.segregacion.cod_usuario = '15';
		}).error(function(status) {
			alertService.showDanger();
		});
	}
		
	
	
	$scope.actTablaCostoReferencial = function() {

		$scope.tblCostoReferencial = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"costo_referencial_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					CostoReferencial : $scope.costoReferencial
				};

				$http({
					method : 'POST',
					url : urlListarCostoReferencial,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstCostoReferencial;
					$scope.lstCostoReferencial = data.datosAdicionales.LstCostoReferencial;
					console.log("lstCostoReferencial > " + JSON.stringify($scope.lstCostoReferencial));
					
					if($scope.costoReferencial.valorizacion_id != undefined){
						$scope.flag7 = 1;
					}
					
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarCostoReferencial = function() {

		for (var x = 0; x < $scope.lstCostoReferencial.length; x++) {
			$scope.lstCostoReferencial[x].valorizacion_id = $scope.costoReferencial.valorizacion_id;
			$scope.lstCostoReferencial[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCostoReferencial,
			data : JSON.stringify($scope.lstCostoReferencial),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarCostoReferencial = function() {

		for (var x = 0; x < $scope.lstCostoReferencial.length; x++) {
			$scope.lstCostoReferencial[x].valorizacion_id = $scope.costoReferencial.valorizacion_id;
			$scope.lstCostoReferencial[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarCostoReferencial,
			data : JSON.stringify($scope.lstCostoReferencial),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	$scope.grabarResiduosCiclo = function() {

		for (var x = 0; x < $scope.lstResiduosCiclo.length; x++) {
			$scope.lstResiduosCiclo[x].valorizacion_id = $scope.residuosCiclo.valorizacion_id;
			$scope.lstResiduosCiclo[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarResiduosCiclo,
			data : JSON.stringify($scope.lstResiduosCiclo),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarResiduosCiclo = function() {

		for (var x = 0; x < $scope.lstResiduosCiclo.length; x++) {
			$scope.lstResiduosCiclo[x].valorizacion_id = $scope.residuosCiclo.valorizacion_id;
			$scope.lstResiduosCiclo[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarResiduosCiclo,
			data : JSON.stringify($scope.lstResiduosCiclo),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.listarResiduosCiclo = function() {
		
		var paramJson = {
			ResiduosCiclo : $scope.residuosCiclo
		};
		
		$http({
			method : 'POST',
			url : urlListarResiduosCiclo,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstResiduosCiclo = data.datosAdicionales.LstResiduosCiclo;
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	
	
	$scope.insertar = function(){
		 $scope.lstTrabajador.push({ 
	       	'dni': $scope.trabajadores.dni,
	           'nom_apellido': $scope.trabajadores.nom_apellido,
	           'sexo': $scope.trabajadores.sexo,
	           'rango': $scope.trabajadores.rango,
	           'cargo': $scope.trabajadores.cargo,
	           'laboral': $scope.trabajadores.laboral,
	           'horario': $scope.trabajadores.horario,
	           'dias_trabajados': $scope.trabajadores.dias_trabajados
	    });
		
		 $scope.trabajadores.dni = null;
		 $scope.trabajadores.nom_apellido = null;
		 $scope.trabajadores.sexo = null;
		 $scope.trabajadores.rango = null;
		 $scope.trabajadores.cargo = null;
		 $scope.trabajadores.laboral = null;
		 $scope.trabajadores.horario = null;
		 $scope.trabajadores.dias_trabajados = null;
		 
		 if($scope.flag > 0){
			 
			 $scope.lstTrabajadorAux.push({ 
		        'dni': $scope.trabajadores.dni,
		        'nom_apellido': $scope.trabajadores.nom_apellido,
		        'sexo': $scope.trabajadores.sexo,
		        'rango': $scope.trabajadores.rango,
		        'cargo': $scope.trabajadores.cargo,
		        'laboral': $scope.trabajadores.laboral,
		        'horario': $scope.trabajadores.horario,
		        'dias_trabajados': $scope.trabajadores.dias_trabajados
		     });
		 }
	}

	$scope.borrar = function(index){
		$scope.lstTrabajador.splice(index, 1);
	};

	app.directive('editableTd', [function() {
	   return {
	      restrict: 'A',
	      link: function(scope, element, attrs) {
	         element.css("cursor", "pointer");
	         element.attr('contenteditable', 'true'); // Referencia: Inciso 1
	         element.bind('blur keyup change', function() { // Referencia: Inciso 2
	            scope.lstTrabajador[attrs.row][attrs.field] = element.text();
	         });
	         element.bind('click', function() { // Referencia: Inciso 3
	            document.execCommand('selectAll', false, null)
	         });
	      }
	   };
	}]);


	/*$scope.insertar = function() {
		
		$scope.grabarTrabajadores();
	}*/
	
	$scope.grabarPersona = function() {
		
		for (var x = 0; x < $scope.lstBarredor.length; x++) {
			$scope.lstTrabajador[x].cod_usuario = '14';
		}
	
		$http({
			method : 'POST',
			url : urlRegistrarPersona,
			data : JSON.stringify($scope.lstBarredor),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.grabarTrabajadores();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
	
		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.grabarTrabajadores = function() {
		
		for (var x = 0; x < $scope.lstTrabajador.length; x++) {
			$scope.lstTrabajador[x].valorizacion_id = $scope.trabajadores.valorizacion_id;
			$scope.lstTrabajador[x].cod_usuario = '14';
		}
	
		$http({
			method : 'POST',
			url : urlRegistrarTrabajadores,
			data : JSON.stringify($scope.lstTrabajador),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
	
		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.actualizarTrabajadores = function() {
		
		var i = $scope.cantidad;
		var c = 0;
		
		for (var x = i; x < $scope.lstTrabajador.length; x++) {
			$scope.lstTrabajador[x].valorizacion_id = $scope.trabajadores.valorizacion_id;
			$scope.lstTrabajador[x].cod_usuario = '14';
			
			$scope.lstTrabajadorAux[c].valorizacion_id = $scope.lstTrabajador[x].valorizacion_id;
			
			$scope.lstTrabajadorAux[c].dni = $scope.lstTrabajador[x].dni;
			$scope.lstTrabajadorAux[c].nom_apellido = $scope.lstTrabajador[x].nom_apellido;
			$scope.lstTrabajadorAux[c].sexo = $scope.lstTrabajador[x].sexo;
			$scope.lstTrabajadorAux[c].rango = $scope.lstTrabajador[x].rango;
			$scope.lstTrabajadorAux[c].cargo = $scope.lstTrabajador[x].cargo;
			$scope.lstTrabajadorAux[c].laboral = $scope.lstTrabajador[x].laboral;
			$scope.lstTrabajadorAux[c].horario = $scope.lstTrabajador[x].horario;
			$scope.lstTrabajadorAux[c].dias_trabajados = $scope.lstTrabajador[x].dias_trabajados;
			
			$scope.lstTrabajadorAux[c].cod_usuario = $scope.lstTrabajador[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarTrabajadores,
			data : JSON.stringify($scope.lstTrabajadorAux),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.actTablaTrabajador = function() {
	
		$scope.tblTrabajador = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"trabajadores_id" : "asc"
			}
		}, {
			getData : function($defer, params) {
				
				var paramJson = {
					Trabajadores : $scope.trabajadores
				};
	
				$http({
					method : 'POST',
					url : urlListarTrabajadores,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstTrabajadores;
					$scope.lstTrabajador = data.datosAdicionales.LstTrabajadores;
					
					for (var x = 0; x < $scope.lstTrabajador.length; x++) {
						
						$scope.lstTrabajador[x].dni = $scope.lstTrabajador[x].persona.dni;
						$scope.lstTrabajador[x].nom_apellido = $scope.lstTrabajador[x].persona.nom_apellidos;
						$scope.lstTrabajador[x].sexo = $scope.lstTrabajador[x].persona.sexo;
						$scope.lstTrabajador[x].rango = $scope.lstTrabajador[x].persona.rango_edad;
						$scope.lstTrabajador[x].cargo = $scope.lstTrabajador[x].cargo;
						$scope.lstTrabajador[x].laboral = $scope.lstTrabajador[x].condicion_laboral;
						$scope.lstTrabajador[x].horario = $scope.lstTrabajador[x].horario_trabajo;
						$scope.lstTrabajador[x].dias_trabajados = $scope.lstTrabajador[x].dias_trabajados;
					}
					
					$scope.cantidad = $scope.lstTrabajador.length;
					console.log("Cantidad Trabajadores " + $scope.cantidad);
					if($scope.cantidad > 0){
						$scope.flag8 = 1;
					}
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarValorizacion1 = function() {

		var paramJson = {
			Valorizacion1 : $scope.valorizacion1
		};

		$http({
			method : 'POST',
			url : urlRegistrarValorizacion1,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.valorizacion2.valorizacion_id = data.datosAdicionales.id;
								$scope.acopio.valorizacion_id = data.datosAdicionales.id;
								$scope.planta.valorizacion_id = data.datosAdicionales.id;
								$scope.trabajadores.valorizacion_id = data.datosAdicionales.id;
								$scope.costoReferencial.valorizacion_id = data.datosAdicionales.id;
								$localStorage.VALORIZACION = '1';
								//$location.path("/gestion");

							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}
	
	$scope.actualizarValorizacion1 = function() {

		var paramJson = {
			Valorizacion1 : $scope.valorizacion1
		};

		$http({
			method : 'POST',
			url : urlActualizarValorizacion1,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.valorizacion2.valorizacion_id = data.datosAdicionales.id;
								$scope.acopio.valorizacion_id = data.datosAdicionales.id;
								$scope.planta.valorizacion_id = data.datosAdicionales.id;
								$scope.costoReferencial.valorizacion_id = data.datosAdicionales.id;
								$scope.trabajadores.valorizacion_id = data.datosAdicionales.id;
					
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		//$location.path("/gestion");
	}
	
	$scope.actualizarValorizacion2 = function() {

		var paramJson = {
			Valorizacion2 : $scope.valorizacion2
		};

		$http({
			method : 'POST',
			url : urlActualizarValorizacion2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.acopio.valorizacion_id = data.datosAdicionales.id;
								$scope.residuosAprovechados.valorizacion_id = data.datosAdicionales.id;
								$scope.residuosOrganicos.valorizacion_id = data.datosAdicionales.id;
								$scope.productosObtenidos.valorizacion_id = data.datosAdicionales.id;
								$scope.trabajadores.valorizacion_id = data.datosAdicionales.id;
								if($scope.flag2 > 0)
									$scope.actualizarResiduosAprovechados();
								else
									$scope.grabarResiduosAprovechados();
								
								if($scope.flag3 > 0)
									$scope.actualizarResiduosOrganicos();
								else
									$scope.grabarResiduosOrganicos();
								
								if($scope.flag4 > 0)
									$scope.actualizarProductosObtenidos();
								else
									$scope.grabarProductosObtenidos();
							
								$localStorage.VALORIZACION = '1';
								//$location.path("/gestion");

							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}
	
	
	$scope.grabarLugarAprovechamiento = function() {

		var paramJson = {
			LugarAprovechamiento : $scope.acopio
		};

		$http({
			method : 'POST',
			url : urlRegistrarLugarAprovechamiento,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.acopio.lugar_aprovechamiento_id = data.datosAdicionales.id;
								$scope.ubicacion.lugar_aprovechamiento_id = data.datosAdicionales.id;
								$scope.grabarUbicacion();
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarLugarAprovechamiento = function() {
		
		var paramJson = {
			LugarAprovechamiento : $scope.acopio
		};

		$http({
			method : 'POST',
			url : urlActualizarLugarAprovechamiento,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.listarLugarAprovechamiento = function() {

		var paramJson = {
			LugarAprovechamiento : $scope.acopio
		};

		$http({
			method : 'POST',
			url : urlListarLugarAprovechamiento,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
		.success(
				function(data) {
					$scope.acopio = data.datosAdicionales.acopio;
					$scope.acopio.valorizacion_id = $scope.valorizacion1.valorizacion_id;
					$scope.acopio.cod_usuario = '15';	
					if ($scope.acopio.lugar_aprovechamiento_id != undefined) {
						$scope.flag = 1;
						$scope.flag5 = 1;
						//$scope.acopio.valorizacion_id = $scope.valorizacion1.valorizacion_id;
						console.log("Id de Valorizacion Aprovechamiento : " + $scope.acopio.valorizacion_id);
						$scope.ubicacion.lugar_aprovechamiento_id = $scope.acopio.lugar_aprovechamiento_id;
						console.log("Id de Ubicacion: " + $scope.ubicacion.lugar_aprovechamiento_id);
					} else {
						//$scope.acopio.valorizacion_id = $scope.valorizacion1.valorizacion_id;
					}

				}).error(function(status) {
				alertService.showDanger();
		});
	}
	
	
	$scope.grabarLugarAprovechamiento2 = function() {

		var paramJson = {
			LugarAprovechamiento2 : $scope.planta
		};

		$http({
			method : 'POST',
			url : urlRegistrarLugarAprovechamiento2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarLugarAprovechamiento2 = function() {
		
		var paramJson = {
			LugarAprovechamiento2 : $scope.planta
		};

		$http({
			method : 'POST',
			url : urlActualizarLugarAprovechamiento2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
			.success(
					function(data) {
						if (data.datosAdicionales.id > 0) {
							alertService.showSuccess("Registro exitoso.");
						} else {
							alertService
								.showDanger("No se realizó el registro.");
						}
					}).error(function(status) {
				alertService.showDanger();
			});

	}
	
	$scope.actualizacionAprovechamiento = function() {
		
		var paramJson = {
			LugarAprovechamiento : $scope.acopio
		};

			$http({
				method : 'POST',
				url : urlActualizacionAprovechamiento,
				data : JSON.stringify(paramJson),
				headers : {
					'Content-Type' : 'application/json'
				}
			})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
							} else {
								alertService
									.showDanger("No se realizó el registro.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});
	}

	
	$scope.listarLugarAprovechamiento2 = function() {

		var paramJson = {
			LugarAprovechamiento2 : $scope.planta
		};

		$http({
			method : 'POST',
			url : urlListarLugarAprovechamiento2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
		.success(
				function(data) {
					$scope.planta = data.datosAdicionales.planta;							
					$scope.planta.cod_usuario = '15';
					$scope.planta.valorizacion_id = $scope.valorizacion1.valorizacion_id;
					if ($scope.planta.lugar_aprovechamiento_id != undefined) {
						$scope.flag = 1;		
						$scope.flag6 = 1;
					} else {
						//$scope.planta.valorizacion_id = $scope.valorizacion1.valorizacion_id;
					}

				}).error(function(status) {
				alertService.showDanger();
		});
	}
	
	
	$scope.grabarUbicacion = function() {

		var paramJson = {
			Ubicacion : $scope.ubicacion
		};

		$http({
			method : 'POST',
			url : urlRegistrarUbicacion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");

							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}
	
	
	$scope.actTablaCoordenadas = function() {
		
		$scope.tblCoordenadas = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"ubicacion_id" : "asc"
			}
		}, {
			getData : function($defer, params) {
				
				var paramJson = {
					Ubicacion : $scope.ubicacion
				};
	
				$http({
					method : 'POST',
					url : urlListarUbicacion,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstCoordenadas;
					$scope.lstCoordenadas = data.datosAdicionales.LstCoordenadas;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	
	$scope.grabar1 = function() {
		if($scope.flag1 > 0)
			$scope.actualizarValorizacion1();
		else
			$scope.grabarValorizacion1();
	}
	
	$scope.grabar2 = function() {
		$scope.actualizarValorizacion2();
	}
	
	$scope.grabar3 = function() {
		if($scope.contaDetalle == 0){
			if($scope.flag5 > 0)
				$scope.actualizarLugarAprovechamiento();
			else
				$scope.grabarLugarAprovechamiento();
		}
		else{
			$scope.grabarUbicacion();
		}
		$scope.contaDetalle++;
	}
	
	$scope.grabar4 = function() {
		if($scope.flag7 > 0)
			$scope.actualizarCostoReferencial();			
		else
			$scope.grabarCostoReferencial();
	}
	
	$scope.grabar5 = function() {
		$scope.actualizacionAprovechamiento();
		if($scope.flag8 > 0)
			$scope.actualizarTrabajadores();
		else
			$scope.grabarTrabajadores();
	}
	
	$scope.grabar6 = function() {
		if($scope.flag6 > 0)
			$scope.actualizarLugarAprovechamiento2();
		else
			$scope.grabarLugarAprovechamiento2();
	}
	
	$scope.agregarDetalle = function() {
		$scope.detalle = $scope.detalle + 1;
		$scope.documento = null;
		$scope.ubicacion.zona_coordenada = null;
		$scope.ubicacion.norte_coordenada = null;
		$scope.ubicacion.sur_coordenada = null;
		$scope.ubicacion.div_politica_dep = null;
		$scope.ubicacion.div_politica_prov= null;
		$scope.ubicacion.div_politica_dist = null;
		$scope.ubicacion.direccion = null;
		$scope.ubicacion.referencia = null;
	}
	
	$scope.verDetalle = function() {
		$scope.coordenada++;
		$scope.actTablaCoordenadas();
	}
	
	$scope.number = 1;
	$scope.number2 = 1;
	
	$scope.detalle = 0;
	$scope.coordenada = 0;
	
	$scope.contaDetalle = 0;
	
	$scope.myFunc = function() {
		$scope.lstResiduosCiclo.length = $scope.valorizacion.numero_ciclos;
    };
    
    $scope.myFunc2 = function() {
		$scope.number2 = $scope.acopio.adm_propia;
    };
   
    $scope.getNumber2 = function(num2) {
        return new Array(num2);   
   }
	
	$scope.volver = function() {
		$location.path("/gestion");
	}

	//$scope.listarValorizacion();
	$scope.listarValorizacion1();
	
}