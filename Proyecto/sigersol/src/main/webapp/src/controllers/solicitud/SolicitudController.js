function SolicitudController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	// $scope.validarLogin();

	var urlRegistrarSolicitud = $scope.urlRest + "/rest/solicitud/registrar";
	var urlActualizarSolicitud = $scope.urlRest + "/rest/solicitud/actualizar";
	var urlListarSolicitud = $scope.urlRest + "/rest/solicitud/obtener";
	
	$scope.solicitud = {};
	$scope.lista = [];
	$scope.lstSolicitud = [];

	$scope.solicitud.cod_usuario = '14';
	
	$scope.grabarSolicitud = function() {

		var paramJson = {
			Solicitud : $scope.solicitud
		};

		console.log("Solicitud : " + JSON.stringify(paramJson));

		$http({
			method : 'POST',
			url : urlRegistrarSolicitud,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.actualizarSolicitud = function() {
		
		var paramJson = {
			Solicitud : $scope.solicitud
		};
		
		console.log("Solicitud : " + JSON.stringify(paramJson));
		
		$http({
			method : 'POST',
			url : urlActualizarSolicitud,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
	}
	
	$scope.listarSolicitud = function() {
		
		$http({
			method : 'POST',
			url : urlListarSolicitud,
			data : $.param({}),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstSolicitud = data.datosAdicionales.LstSolicitud;
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.tblAprobacion = new NgTableParams(
		{
			page : 1,
			count : 10,
			sorting : {
				"solicitud_id" : "asc"
			}
		},
		{
			counts : [ 10, 15, 20, 25 ],
			getData : function($defer, params) {

			$http(
					{
						method : 'POST',
						url : urlListarSolicitud,
						data : $.param({
						}),
						headers : {
							'Content-Type' : 'application/json'
						}
					})
					.success(
								function(data) {
										blockUI.start();
										var filtro = {};
										var NewData = [];
										for (var x = 0; x < data.datosAdicionales.LstSolicitud.length; x++) {
											var lista = {
												"solicitud_id" : data.datosAdicionales.LstSolicitud[x].solicitud_id,
												"codigo_ubigeo_departamento" : data.datosAdicionales.LstSolicitud[x].codigo_ubigeo_departamento,
												"codigo_ubigeo_provincia" : data.datosAdicionales.LstSolicitud[x].codigo_ubigeo_provincia,
												"codigo_ubigeo_distrito" : data.datosAdicionales.LstSolicitud[x].codigo_ubigeo_distrito,
												"ruc_municipalidad" : data.datosAdicionales.LstSolicitud[x].ruc_municipalidad,
											}
											NewData.push(lista);
										}
										
										var nData = NewData;
										params.total(nData.length);
										$scope.orderedData = params.sorting() ? $filter('orderBy')(nData, params.orderBy()) : nData;
										var filterData = filtro ? $filter('filter')($scope.orderedData, filtro) : $scope.orderedData;
										params.total(filterData.length);
										$defer.resolve(filterData.slice((params.page() - 1)* params.count(), params.page() * params.count()));

										blockUI.stop();
									 }).error(function(status) {
									 $scope.status = status;
							 });
						}
		  });
	
}