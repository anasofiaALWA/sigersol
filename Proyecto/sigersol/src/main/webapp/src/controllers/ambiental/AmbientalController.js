function AmbientalController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	// $scope.validarLogin();

	var urlRegistrarAmbiental = $scope.urlRest + "/rest/ambiental/registrar";
	var urlActualizarAmbiental = $scope.urlRest + "/rest/ambiental/actualizar";
	var urlListarAmbiental = $scope.urlRest + "/rest/ambiental/obtener";
	
	$scope.ambiental = {};
	$scope.lista = [];
	$scope.lstAmbiental = [];
	
	$scope.ambiental.municipalidad_id = '1';
	$scope.ambiental.cod_usuario = '14';
	
	
	


	$scope.grabarAmbiental = function() {

		var paramJson = {
			Ambiental : $scope.ambiental
		};

		console.log("Ambiental : " + JSON.stringify(paramJson));

		$http({
			method : 'POST',
			url : urlRegistrarAmbiental,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.actualizarAmbiental = function() {
		
		var paramJson = {
			Ambiental : $scope.ambiental
		};
		
		console.log("Ambiental : " + JSON.stringify(paramJson));
		
		$http({
			method : 'POST',
			url : urlActualizarAmbiental,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
	}
	
	$scope.listarAmbiental = function() {
		
		$http({
			method : 'POST',
			url : urlListarAmbiental,
			data : $.param({}),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstAmbiental = data.datosAdicionales.LstAmbiental;
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.cancelar = function() {
		$location.path("/panel");
	}
}