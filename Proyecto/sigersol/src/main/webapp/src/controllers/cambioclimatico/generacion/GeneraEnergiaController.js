function GeneraEnergiaController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage,  alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlGetUltAnioCicloGRS = $scope.urlRest
			+ "/rest/gestion/getUltAnioCicloGRS";
	var urlGetDatosGenerales = $scope.urlRest
			+ "/rest/ccGenComposicion/datosGenerales";
	var urlListarSDF = $scope.urlRest + "/rest/disposicion/listarSDF";
	var urlListarCombustible = $scope.urlRest + "/rest/ccGenCombustible/listar";
	var urlRegistrarActividad = $scope.urlRest
			+ "/rest/ccGenActividad/registrar";
	var urlObtenerActividad = $scope.urlRest + "/rest/ccGenActividad/obtener";
	var urlRegistrarGeneracion = $scope.urlRest
			+ "/rest/ccGeneracion/registrar";
	var urlListDepartamento = $scope.urlRest + "/rest/ubigeo/listDepartamento";
	var urlListProvincia = $scope.urlRest + "/rest/ubigeo/listProvincia";
	var urlListDistrito = $scope.urlRest + "/rest/ubigeo/listDistrito";
	var urlListVariable = $scope.urlRest + "/rest/ccVariable/listar";
	var urlListFactorCorreccion = $scope.urlRest
			+ "/rest/ccFactorCorrec/listar";
	var urlListTipoResiduoDOC = $scope.urlRest
			+ "/rest/ccTipoResiduo/listarDOC";
	var urlListTipoResiduoTasaD = $scope.urlRest
			+ "/rest/ccTipoResiduo/listarTasaDesc";
	var urlCalcularTool4 = $scope.urlRest + "/rest/ccGeneracion/calcularTool4";

	$scope.ccGeneracion = {};
	$scope.ccGenActividad = {};
	$scope.ccGenCalc = {};
	$scope.fDisabled = true;
	$scope.listSDF = [];
	$scope.listDep = [];
	$scope.listProv = [];
	$scope.listDist = [];
	$scope.ccGenActividad.eficienciaQuemador = 60;
	$scope.ccGenActividad.densidad_metano = 0.0007168;
	$scope.ccGenActividad.eficienciaCaptura = 50;
	$scope.fShowTab = false;
	$scope.ccGenCalc.Fch4bly = 0;
	$scope.ccGenCalc.LE = 0;
	$scope.ccGenCalc.PE = 0;
	$scope.ccGeneracion.id = -1;
	$scope.ccGenActividad.id = -1;
	$scope.fTabActLoad = false;
	$scope.fTabCalcLoad = false;

	$scope.lstDepartamento = function() {
		$http({
			method : 'POST',
			url : urlListDepartamento,
			data : "",
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			// console.log("data >" + JSON.stringify(data.datosAdicionales));
			$scope.listDep = data.datosAdicionales;
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.lstProvincia = function() {

		var paramJson = {
			Ubigeo : {
				CodDep : $scope.selDepartamento.departamento
			}
		};

		$http({
			method : 'POST',
			url : urlListProvincia,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			// console.log("data >" + JSON.stringify(data.datosAdicionales));
			$scope.listProv = data.datosAdicionales;
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.lstDistrito = function() {
		var paramJson = {
			Ubigeo : {
				CodDep : $scope.selDepartamento.departamento,
				CodProv : $scope.selProvincia.provincia
			}
		};

		$http({
			method : 'POST',
			url : urlListDistrito,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			// console.log("data >" + JSON.stringify(data.datosAdicionales));
			$scope.listDist = data.datosAdicionales;
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.getUltAnio = function() {

		var paramJson = {
			Ubigeo : {
				CodDist : $scope.selDistrito.ubigeoId
			}
		};

		$http({
			method : 'POST',
			url : urlGetUltAnioCicloGRS,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data.datosAdicionales));
			$scope.municipalidadId = data.datosAdicionales.municipalidad_id;
			$scope.ccGeneracion.anioReporte = data.datosAdicionales.anio;
			$scope.listarSDF();
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.listarSDF = function() {
		var paramJson = {
			Filtro : {
				municipalidadId : $scope.municipalidadId,
				anio : $scope.ccGeneracion.anioReporte
			}
		};

		$http({
			method : 'POST',
			url : urlListarSDF,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data.datosAdicionales));
			$scope.listSDF = data.datosAdicionales.ListSDF;

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.getDatosGenerales_SDF = function() {
		var paramJson = {
			Filtro : {
				DispFinalId : $scope.sdf_selected.disposicion_final_id
			}
		};

		$http({
			method : 'POST',
			url : urlGetDatosGenerales,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							console.log("data >"
									+ JSON.stringify(data.datosAdicionales));
							if (data.exito) {
								$scope.ccGeneracion.anioInicio = data.datosAdicionales.anioInicio_sdf;
								$scope.ccGeneracion.totalRRSS = data.datosAdicionales.generacionTotal_sdf;
								$scope.ccGeneracion.listComposicion = data.datosAdicionales.listComposicion;
								$scope.ccGeneracion.id = data.datosAdicionales.cc_generacionId;

								if ($scope.ccGeneracion.anioInicio === 0) {
									$scope.fDisabled = false;
								}
								alertService.showSuccess();

							} else {
								alertService.showDanger();
							}
						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.listarCombustible = function() {
		console.log("$scope.ccGenActividad.id >"
				+ JSON.stringify($scope.ccGenActividad.id));
		var paramJson = {
			Filtro : {
				ccGenActividadId : $scope.ccGenActividad.id
			}
		};

		$http({
			method : 'POST',
			url : urlListarCombustible,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data));
			$scope.ccGenActividad.listCombustible = data.datosAdicionales;

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.registrarGeneracion = function() {
		$scope.ccGeneracion.sesionId = 14;
		$scope.ccGeneracion.disposicionFinalId = $scope.sdf_selected.disposicion_final_id;

		console.log("$scope.ccGeneracion >"
				+ JSON.stringify($scope.ccGeneracion));
		var paramJson = {
			ccGeneracion : $scope.ccGeneracion

		};

		$http({
			method : 'POST',
			url : urlRegistrarGeneracion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data.datosAdicionales));
			if (data.exito) {
				$scope.ccGeneracion.id = data.datosAdicionales;
				alertService.showSuccess();
			} else {
				alertService.showDanger();
			}
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.registrarActividad = function() {
		$scope.ccGenActividad.sesionId = 14;
		$scope.ccGenActividad.ccGeneracionId = $scope.ccGeneracion.id;

		console.log("$scope.ccGenActividad >"
				+ JSON.stringify($scope.ccGenActividad));
		var paramJson = {
			ccGenActividad : $scope.ccGenActividad

		};

		$http({
			method : 'POST',
			url : urlRegistrarActividad,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data.datosAdicionales));
			if (data.exito) {
				$scope.ccGenActividad.id = data.datosAdicionales;
				alertService.showSuccess();

			} else {
				alertService.showDanger();
			}
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.lstDepartamento();

	/**
	 * *****************************************************************************
	 * Tab Cálculo
	 * *****************************************************************************
	 */

	$scope.setVariables = function() {

		var paramJson = {
			Filtro : {
				sitioDFId : $scope.sdf_selected.sitioDisposicion.sitio_disposicion_id
			}
		};

		$http({
			method : 'POST',
			url : urlListVariable,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data.datosAdicionales));
			$scope.variables = data.datosAdicionales;
			for (var i = 0; i < $scope.variables.length; i++) {
				switch ($scope.variables[i].nombreVariable) {
				case 'GWPCH4':
					$scope.ccGenCalc.potencial_cg = $scope.variables[i].valor;
					break;
				case 'OX':
					$scope.ccGenCalc.factor_ox = $scope.variables[i].valor;
					break;
				case 'e':
					$scope.ccGenCalc.euler = $scope.variables[i].valor;
					break;
				case 'DOCF':
					$scope.ccGenCalc.fraccion_doc = $scope.variables[i].valor;
					break;
				case 'MCF':
					$scope.ccGenCalc.factor_cm = $scope.variables[i].valor;
					break;
				case 'F':
					$scope.ccGenCalc.fraccion_lfg = $scope.variables[i].valor;
					break;
				default:
					break;
				}

			}
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.listarCalcCondicion = function() {
		var paramJson = {
			Filtro : {
				disposicionFinalId : $scope.sdf_selected.disposicion_final_id
			}
		};

		$http({
			method : 'POST',
			url : urlListFactorCorreccion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data.datosAdicionales));
			if (data.exito) {
				$scope.listCondicion = data.datosAdicionales;
			}
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.listDOCTipoResiduo = function() {
		console
				.log("ccGenCalc.selFactor.condicionId >> ccGenCalc.selFactor.condicionId");
		$http({
			method : 'POST',
			url : urlListTipoResiduoDOC,
			data : "",
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data.datosAdicionales));
			if (data.exito) {
				$scope.listTipoResiduoDOC = data.datosAdicionales;
			}
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.listTasaDescTipoResiduo = function() {
		var paramJson = {
			Filtro : {
				condicionId : $scope.ccGenCalc.selFactor.condicionId,
				ubigeoId : $scope.selDistrito.ubigeoId
			}
		};

		$http({
			method : 'POST',
			url : urlListTipoResiduoTasaD,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			console.log("data >" + JSON.stringify(data.datosAdicionales));
			if (data.exito) {
				$scope.listTipoResiduoTD = data.datosAdicionales;
			}
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.calcularTool4 = function() {
		var paramJson = {
			Filtro : {
				condicionId : $scope.ccGenCalc.selFactor.condicionId,
				ubigeoId : $scope.selDistrito.ubigeoId,
				municipalidadId : $scope.municipalidadId,
				ccGeneracionId : $scope.ccGeneracion.id,
				anioCGRS : $scope.ccGeneracion.anioReporte
			}
		};

		$http({
			method : 'POST',
			url : urlCalcularTool4,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							console.log("data >"
									+ JSON.stringify(data.datosAdicionales));
							if (data.exito) {
								$scope.ccGenCalc.total = data.datosAdicionales;
								$scope.ccGenCalc.Fch4pjy = $scope.ccGenCalc.total
										* ($scope.ccGenActividad.eficienciaCaptura / 100)
										/ $scope.ccGenCalc.potencial_cg;
								$scope.ccGenCalc.BEch4y = ((1 - $scope.ccGenCalc.factor_ox)
										* $scope.ccGenCalc.Fch4pjy - $scope.ccGenCalc.Fch4bly)
										* $scope.ccGenCalc.potencial_cg;
								$scope.ccGenCalc.BEy = $scope.ccGenCalc.BEch4y;
								$scope.ccGenCalc.ER = $scope.ccGenCalc.BEy
										- $scope.ccGenCalc.PE
										- $scope.ccGenCalc.LE;

							}
						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.setPhi = function() {
		console.log("data selFactor > "
				+ JSON.stringify($scope.ccGenCalc.selFactor));

		$scope.ccGenCalc.phi = $scope.ccGenCalc.selFactor.valor_phi;
		$scope.listDOCTipoResiduo();
		$scope.listTasaDescTipoResiduo();
		$scope.calcularTool4();
	}

	$scope.getActividad = function() {
		var paramJson = {
			Filtro : {
				ccGeneracionId : $scope.ccGeneracion.id
			}
		};

		$http({
			method : 'POST',
			url : urlObtenerActividad,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							console.log("data >"
									+ JSON.stringify(data.datosAdicionales));
							if (data.exito) {
								$scope.ccGenActividad.id = data.datosAdicionales.cc_genActividadId;
								$scope.ccGenActividad.cantMetano = data.datosAdicionales.metano_quemado;
								$scope.ccGenActividad.monitoreo = data.datosAdicionales.monitoreo;
								if ($scope.ccGenActividad.monitoreo === 1) {
									$scope.ccGenActividad.horasFunc = data.datosAdicionales.horas_quemador;
									$scope.ccGenActividad.eficienciaQuemador = data.datosAdicionales.eficiencia_quemador;
									$scope.ccGenActividad.total_gas = data.datosAdicionales.gas_total;
									$scope.ccGenActividad.gas_quemado = data.datosAdicionales.gas_quemado;
									$scope.ccGenActividad.gas_generado = data.datosAdicionales.gas_generacion;
									$scope.ccGenActividad.ptj_metano_lfg = data.datosAdicionales.p_metanoEnGas;
								} else {
									$scope.ccGenActividad.eficienciaCaptura = data.datosAdicionales.eficienciaCaptura;
								}

								alertService.showSuccess();
							}
							$scope.listarCombustible();

						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.irTabActividad = function() {
		if ($scope.ccGeneracion.id > 0) {
			if (!$scope.fTabActLoad) {
				$scope.getActividad();
			}
			$scope.numTab = 2;
			$scope.fTabActLoad = true;

		} else {
			alertService
					.showInfo("Debe guardar los Datos Generales del Tab para continuar.");
		}

	}

	$scope.irTabCalculo = function() {
		$scope.numTab = 3;
		if ($scope.ccGenActividad.id > 0) {
			if (!$scope.fTabCalcLoad) {
				$scope.setVariables();
				$scope.listarCalcCondicion();
			}
			$scope.numTab = 3;
			$scope.fTabCalcLoad = true;
		} else {
			alertService
					.showInfo("Debe completar los datos de Actividad del Proyecto");
		}
	}

}