function AdmFinanzasController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage,  alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlRegistrarAdmFinanzas = $scope.urlRest + "/rest/finanzas/registrar";
	var urlActualizarAdmFinanzas = $scope.urlRest + "/rest/finanzas/actualizar";
	var urlListarAdmFinanzas = $scope.urlRest + "/rest/finanzas/obtener";
	
	var urlRegistrarInversion = $scope.urlRest + "/rest/inversion/registrar";
	var urlActualizarInversion = $scope.urlRest + "/rest/inversion/actualizar";
	var urlListarInversion = $scope.urlRest + "/rest/inversion/listarInversion";
	
	$scope.finanzas = {};

	$scope.inversion = {};
	$scope.lstInversion = [];
	
	$scope.flag = 0;
	
	$scope.finanzas.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.finanzas.cod_usuario = '15';
	$scope.inversion.cod_usuario = '15';
	
	$scope.guardar = function() {
		if($scope.flag == 0)
			$scope.grabarAdmFinanzas();
		else
			$scope.actualizarAdmFinanzas();
	}
	
	$scope.grabarAdmFinanzas = function() {
		
		var paramJson = {
			AdmFinanzas : $scope.finanzas
		};

		$http({
			method : 'POST',
			url : urlRegistrarAdmFinanzas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.finanzas.adm_finanzas_id = data.datosAdicionales.id;
				$scope.inversion.adm_finanzas_id = data.datosAdicionales.id;
				$scope.grabarInversion();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}	
	
	$scope.actualizarAdmFinanzas = function() {
		
		var paramJson = {
			AdmFinanzas : $scope.finanzas
		};
		
		$http({
			method : 'POST',
			url : urlActualizarAdmFinanzas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");	
				
				$scope.inversion.adm_finanzas_id = data.datosAdicionales.id;
				$scope.actualizarContenedor();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		$location.path("/gestion");	
	}
	
	$scope.listarAdmFinanzas = function() {
		
		var paramJson = {
			AdmFinanzas : $scope.finanzas
		};
		
		$http({
			method : 'POST',
			url : urlListarAdmFinanzas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			
			$scope.finanzas = data.datosAdicionales.admFinanzas;	
			if($scope.finanzas.adm_finanzas_id != undefined){	
				$scope.flag = 1;
			}
			$scope.finanzas.ciclo_grs_id = $localStorage.GRS_ID;
			$scope.finanzas.cod_usuario = '15';
			$scope.inversion.adm_finanzas_id = $scope.finanzas.adm_finanzas_id;
			$scope.actTablaInversion();
		}).error(function(status) {
			alertService.showDanger();
		});
	}
		
	
	
	$scope.actTablaInversion = function() {

		$scope.tblInversion = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"inversion_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					Inversion : $scope.inversion
				};

				$http({
					method : 'POST',
					url : urlListarInversion,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstInversion;
					$scope.lstInversion = data.datosAdicionales.LstInversion;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarInversion = function() {

		for (var x = 0; x < $scope.lstInversion.length; x++) {
			$scope.lstInversion[x].adm_finanzas_id = $scope.finanzas.adm_finanzas_id;
			$scope.lstInversion[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarInversion,
			data : JSON.stringify($scope.lstInversion),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.actualizarContenedor = function() {

		for (var x = 0; x < $scope.lstInversion.length; x++) {
			$scope.lstInversion[x].adm_finanzas_id = $scope.finanzas.adm_finanzas_id;
			$scope.lstInversion[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarInversion,
			data : JSON.stringify($scope.lstInversion),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	$scope.volver = function() {
		$location.path("/gestion");
	}
	
	$scope.listarAdmFinanzas();
}