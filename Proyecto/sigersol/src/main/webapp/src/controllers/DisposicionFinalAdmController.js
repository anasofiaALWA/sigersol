function DisposicionFinalAdmController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	$scope.flag = 0;
	$scope.flg = 0;
	
	$scope.cantidadAreaAbandonada = 0;
	
	var urlRegistrarDisposicionAdm = $scope.urlRest 
			+ "/rest/disposicionAdm/registrar";
	var urlActualizarDisposicionAdm = $scope.urlRest
			+ "/rest/disposicionAdm/actualizar";
	var urlListarDisposicionAdm = $scope.urlRest 
			+ "/rest/disposicionAdm/obtener";

	
	$scope.disposicion_adm = {};
	
	var urlRegistrarDegradadas = $scope.urlRest 
		+ "/rest/degradadas/registrar";
	var urlActualizarDegradadas = $scope.urlRest
		+ "/rest/degradadas/actualizar";
	var urlListarDegradadas = $scope.urlRest 
		+ "/rest/degradadas/obtener";
	
	
	$scope.degradadas = {};
	$scope.lstDegradadas = [];
	
	var urlRegistrarAbandonadas = $scope.urlRest 
		+ "/rest/abandonadas/registrar";
	var urlActualizarAbandonadas = $scope.urlRest
		+ "/rest/abandonadas/actualizar";
	var urlListarAbandonadas = $scope.urlRest 
		+ "/rest/abandonadas/obtener";
	
	var urlRegistrarAreaAbandonada = $scope.urlRest 
		+ "/rest/areaAbandonada/registrar";
	var urlActualizarAreaAbandonada = $scope.urlRest 
		+ "/rest/areaAbandonada/actualizar";
	var urlListarAreaAbandonada = $scope.urlRest 
		+ "/rest/areaAbandonada/listar";
	
	$scope.abandonadas = {};
		
	var urlRegistrarManejo = $scope.urlRest 
		+ "/rest/tipoManejo/registrar";
	var urlActualizarManejo = $scope.urlRest
		+ "/rest/tipoManejo/actualizar";
	var urlListarManejo = $scope.urlRest 
		+ "/rest/tipoManejo/listarManejo";
	
	$scope.tipomanejo = {};
	$scope.lstManejo = [];
	
	var urlRegistrarCompMuniDf = $scope.urlRest 
		+ "/rest/compMuniDf/registrar";
	var urlActualizarCompMuniDf = $scope.urlRest
		+ "/rest/compMuniDf/actualizar";
	var urlListarCompMuniDf = $scope.urlRest 
		+ "/rest/compMuniDf/listarCompMuni";
	
	$scope.compmunidf = {};
	$scope.lstCompMuniDf = [];
	
	var urlRegistrarInfoCeldas = $scope.urlRest 
		+ "/rest/infoCeldas/registrar";
	var urlActualizarInfoCeldas = $scope.urlRest
		+ "/rest/infoCeldas/actualizar";
	var urlListarInfoCeldas = $scope.urlRest 
		+ "/rest/infoCeldas/listarCeldas";
	
	$scope.infoceldas = {};
	$scope.lstInfoCeldas = [];
	
	var urlRegistrarCantVehiculos = $scope.urlRest 
		+ "/rest/cantVehiculos/registrar";
	var urlActualizarCantVehiculos = $scope.urlRest
		+ "/rest/cantVehiculos/actualizar";
	var urlListarCantVehiculos = $scope.urlRest 
		+ "/rest/cantVehiculos/listarCantidadVehiculos";

	$scope.cantvehiculos = {};
	$scope.lstCantVehiculos = [];
		
	var urlRegistrarCompMuniDeg = $scope.urlRest 
		+ "/rest/compMuniDeg/registrar";
	var urlActualizarCompMuniDeg = $scope.urlRest
		+ "/rest/compMuniDeg/actualizar";
	var urlListarCompMuniDeg = $scope.urlRest 
		+ "/rest/compMuniDeg/listarCompMuni";

	$scope.compmunideg = {};
	$scope.lstCompMuniDeg = [];
	
	var urlRegistrarMuniDispDf = $scope.urlRest 
		+ "/rest/MuniDispDf/registrar";
	var urlActualizarMuniDispDf = $scope.urlRest
		+ "/rest/MuniDispDf/actualizar";
	var urlListarMuniDispDf = $scope.urlRest 
		+ "/rest/MuniDispDf/listarMuniDisp";

	$scope.munidispdf = {};
	$scope.lstMuniDispDf = [];
		
	var urlRegistrarMuniDispDeg = $scope.urlRest 
		+ "/rest/MuniDispDeg/registrar";
	var urlActualizarMuniDispDeg = $scope.urlRest
		+ "/rest/MuniDispDeg/actualizar";
	var urlListarMuniDispDeg = $scope.urlRest 
		+ "/rest/MuniDispDeg/listarMuniDisp";

	$scope.munidispdeg = {};
	$scope.lstMuniDispDeg = [];
	
	var urlRegistrarTrabajadores = $scope.urlRest
		+ "/rest/trabajadoresDisposicionFinal/registrar";
	var urlListarTrabajadores = $scope.urlRest
		+ "/rest/trabajadoresDisposicionFinal/listarTrabajadores";
	var urlActualizarTrabajadores = $scope.urlRest
		+ "/rest/trabajadoresDisposicionFinal/actualizar";
	
	var urlRegistrarTrabajadores2 = $scope.urlRest
		+ "/rest/trabajadoresDisposicionFinal2/registrar";
	var urlListarTrabajadores2 = $scope.urlRest
		+ "/rest/trabajadoresDisposicionFinal2/listarTrabajadores";
	var urlActualizarTrabajadores2 = $scope.urlRest
		+ "/rest/trabajadoresDisposicionFinal2/actualizar";
	
	var urlListarRellenoAdm = $scope.urlRest 
		+ "/rest/sitioDisposicion/obtenerAdm";
	
	var urlListarDegradadaAdm = $scope.urlRest 
		+ "/rest/sitioDisposicion/listarAdm";
	
	var urlListarCoordenada = $scope.urlRest 
		+ "/rest/sitioDisposicion/listarCoordenada";
	var urlListarCoordenada2 = $scope.urlRest 
		+ "/rest/sitioDisposicion/listarCoordenada2";

	
	var urlListUbigeo = $scope.urlRest + "/rest/ubigeo/listPorUbigeo";
	
	$scope.trabajadores = {};
	$scope.lstTrabajador = [];
	
	$scope.trabajadores2 = {};
	$scope.lstTrabajador2 = [];
	
	$scope.lstTrabajadorAux = [];
	$scope.lstTrabajadorAux2 = [];
	
	$scope.lstAreaAbandonadaAux = [];
	
	$scope.area_abandonada = {};
	$scope.lstAreaAbandonada = [];
	
	$scope.area_abandonada.municipalidad_id = 4;
	
	$scope.disposicion_adm.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.disposicion_adm.cod_usuario = '14';
	
	$scope.degradadas.cod_usuario =  '14';
	$scope.abandonadas.cod_usuario = '14';
	
	//$scope.disposicion_adm.selec_relleno = $localStorage.RELLENO;
	//$scope.degradadas.selec_degradada = $localStorage.DEGRADADA;
	
	$scope.rellenoAdm = {};
	$scope.lstRellenoAdm = [];
	
	$scope.degradadaAdm = {};
	$scope.lstDegradadaAdm = [];
	
	$scope.ubicacion = {};
	$scope.ubicacion2 = {};
	$scope.coordenadas = {};
	$scope.coordenadas2 = {};
	$scope.utm = {};
	$scope.utm2 = {};
	
	$scope.flag1 = 0
	$scope.flag2 = 0;
	$scope.flag3 = 0;
	
	//$scope.coordenadas.sitio_disposicion_id = $localStorage.RELLENO;
	//$scope.coordenadas2.sitio_disposicion_id = $localStorage.DEGRADADA;
	
	$scope.guardar = function() {
		if ($scope.flag == 0)
			$scope.grabarDisposicionAdm();
		else
			$scope.actualizarDisposicionAdm();
	}

	$scope.grabarDisposicionAdm = function() {

		var paramJson = {
			DisposicionAdm : $scope.disposicion_adm
		};

		$http({
			method : 'POST',
			url : urlRegistrarDisposicionAdm,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.flag = 1;
								//$scope.degradadas.disposicion_final_id = data.datosAdicionales.id;
								//$scope.abandonadas.disposicion_final_id = data.datosAdicionales.id;
								$scope.tipomanejo.disposicion_final_id = data.datosAdicionales.id;
								//$scope.compmunidf.disposicion_final_id = data.datosAdicionales.id;
								$scope.infoceldas.disposicion_final_id = data.datosAdicionales.id;
								$scope.cantvehiculos.disposicion_final_id = data.datosAdicionales.id;
								//$scope.munidispdf.disposicion_final_id = data.datosAdicionales.id;
								$scope.trabajadores.disposicion_final_id = data.datosAdicionales.id;
								$scope.area_abandonada.disposicion_final_id = data.datosAdicionales.id;
								
								//$scope.grabarDegradadas();
								//$scope.grabarAbandonadas();
								$scope.grabarTipoManejo();
								//$scope.grabarCompMuniDf();
								$scope.grabarInfoCeldas();
								$scope.grabarCantVehiculos();
								//$scope.grabarMuniDispDf();
								$scope.grabarTrabajadores();
								//$scope.grabarAreaAbandonada();
								
								//$location.path("/gestion");

							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarDisposicionAdm = function() {

		var paramJson = {
			DisposicionAdm : $scope.disposicion_adm
		};

		$http({
			method : 'POST',
			url : urlActualizarDisposicionAdm,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								
								$scope.tipomanejo.disposicion_final_id = data.datosAdicionales.id;
								$scope.infoceldas.disposicion_final_id = data.datosAdicionales.id;
								$scope.cantvehiculos.disposicion_final_id = data.datosAdicionales.id;
								$scope.trabajadores.disposicion_final_id = data.datosAdicionales.id;
								
								$scope.actualizarTipoManejo();
								$scope.actualizarInfoCeldas();
								$scope.actualizarCantVehiculos();
								$scope.actualizarTrabajadores();
	
							} else {
								alertService
										.showDanger("No se realizó el registro1.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		//$location.path("/gestion");
	}

	$scope.listarDisposicionAdm = function() {

		var aux = $scope.disposicion_adm.selec_relleno;
		$scope.coordenadas.sitio_disposicion_id = $scope.disposicion_adm.selec_relleno;
		
		var paramJson = {
			DisposicionAdm : $scope.disposicion_adm
		};

		$http({
			method : 'POST',
			url : urlListarDisposicionAdm,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.disposicion_adm = data.datosAdicionales.disposicion_adm;
							if ($scope.disposicion_adm.disposicion_final_id != undefined) {
								$scope.flag = 1;
								$scope.flag1 = 1;
								$scope.tipomanejo.disposicion_final_id = $scope.disposicion_adm.disposicion_final_id;
								$scope.infoceldas.disposicion_final_id = $scope.disposicion_adm.disposicion_final_id;
								$scope.cantvehiculos.disposicion_final_id = $scope.disposicion_adm.disposicion_final_id;
								$scope.trabajadores.disposicion_final_id = $scope.disposicion_adm.disposicion_final_id;
								
							} else {
								$scope.flag1 = 0;
								$scope.tipomanejo.disposicion_final_id = null;
								$scope.infoceldas.disposicion_final_id = null;
								$scope.cantvehiculos.disposicion_final_id = null;
								$scope.trabajadores.disposicion_final_id = null;
							}

							$scope.disposicion_adm.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.disposicion_adm.cod_usuario = '12';
			
							$scope.rellenoAdm.ubigeo_id = '150113';
							//$scope.relleno.ubigeo_id = '101010';
							
							$scope.actTablaManejo();
							$scope.actTablaInfoCeldas();
							$scope.actTablaCantVehiculos();
							
							$scope.listarRellenoAdm();
							$scope.listarCoordenadas();
							$scope.lstUbigeo();
							$scope.actTablaTrabajador();
						
							$scope.setAreaDisponible();
							
							$scope.disposicion_adm.selec_relleno = aux;
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	
	$scope.grabarDegradadas = function() {
		
		var paramJson = {
			Degradadas : $scope.degradadas
		};

		$scope.degradadas.cod_usuario = '15';

		$http({
			method : 'POST',
			url : urlRegistrarDegradadas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");	
				$scope.flg = 1;
				$scope.trabajadores2.disposicion_final_id = data.datosAdicionales.id;
				$scope.grabarTrabajadores2();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
			
	}	
	
	
	$scope.actualizarDegradadas = function() {
		
		var paramJson = {
			Degradadas : $scope.degradadas
		};
		
		$scope.degradadas.cod_usuario = '15';
		
		$http({
			method : 'POST',
			url : urlActualizarDegradadas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");		
			
				$scope.trabajadores2.disposicion_final_id = data.datosAdicionales.id;
				$scope.actualizarTrabajadores2();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		//$location.path("/gestion");	
	}
	
	
	$scope.listarDegradadas = function() {

		var aux = $scope.degradadas.selec_degradada;
		$scope.coordenadas2.sitio_disposicion_id = $scope.degradadas.selec_degradada;
		
		var paramJson = {
			Degradadas : $scope.degradadas
		};

		$http({
			method : 'POST',
			url : urlListarDegradadas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.degradadas = data.datosAdicionales.degradadas;
							
							if ($scope.degradadas.disposicion_final_id != undefined) {
								$scope.flag = 1;
								$scope.flg = 1;
								$scope.flag2 = 1;
								$scope.trabajadores2.disposicion_final_id = $scope.degradadas.disposicion_final_id;
							} else {
								$scope.flag2 = 0;
								$scope.trabajadores2.disposicion_final_id = null;
							}

							//$scope.degradada.ubigeo_id = '150113';
							
							$scope.actTablaTrabajador2();	
							$scope.degradadaAdm.ubigeo_id = '150113';
							
							$scope.listarCoordenadas2();
							$scope.listarDegradadaAdm();
							
							$scope.setAreaDisponible();
							
							$scope.degradadas.selec_degradada = aux;
							//$scope.degradadas.selec_degradada = $localStorage.DEGRADADA;
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	$scope.listarCoordenadas = function() {

		var paramJson = {
			Coordenadas : $scope.coordenadas
		};

		$http({
			method : 'POST',
			url : urlListarCoordenada,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.coordenadas = data.datosAdicionales.coordenada;

							if ($scope.coordenadas != undefined) {
								
								$scope.utm.zona = $scope.coordenadas.zona;
								$scope.utm.norte = $scope.coordenadas.coordenada_norte;
								$scope.utm.este = $scope.coordenadas.coordenada_este;
								
							} else {
								
							}

							
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	$scope.listarCoordenadas2 = function() {

		var paramJson = {
			Coordenadas2 : $scope.coordenadas2
		};

		$http({
			method : 'POST',
			url : urlListarCoordenada2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.coordenadas2 = data.datosAdicionales.coordenada2;

							if ($scope.coordenadas2 != undefined) {
								
								$scope.utm2.zona = $scope.coordenadas2.zona;
								$scope.utm2.norte = $scope.coordenadas2.coordenada_norte;
								$scope.utm2.este = $scope.coordenadas2.coordenada_este;
								
							} else {
								
							}

							
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	$scope.grabarAbandonadas = function() {
		
		var paramJson = {
			Abandonadas : $scope.abandonadas
		};

		$scope.abandonadas.cod_usuario = '15';
	
		$http({
			method : 'POST',
			url : urlRegistrarAbandonadas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
		
	}	
	
	
	$scope.actualizarAbandonadas = function() {
		
		var paramJson = {
			Abandonadas : $scope.abandonadas
		};
		
		$scope.abandonadas.cod_usuario = '15';
		
		$http({
			method : 'POST',
			url : urlActualizarAbandonadas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});
		
		//$location.path("/gestion");	
	}
	
	$scope.listarAbandonadas = function() {

		var paramJson = {
			Abandonadas : $scope.abandonadas
		};

		$http({
			method : 'POST',
			url : urlListarAbandonadas,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.abandonadas = data.datosAdicionales.abandonadas;
										
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	
	
	$scope.actTablaManejo = function() {

		$scope.tblManejo = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"tipo_manejo_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
						Manejo : $scope.tipomanejo
				};

				$http({
					method : 'POST',
					url : urlListarManejo,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstManejo;
					$scope.lstManejo = data.datosAdicionales.LstManejo;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarTipoManejo = function() {

		for (var x = 0; x < $scope.lstManejo.length; x++) {
			$scope.lstManejo[x].disposicion_final_id = $scope.tipomanejo.disposicion_final_id;
			$scope.lstManejo[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarManejo,
			data : JSON.stringify($scope.lstManejo),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarTipoManejo = function() {

		for (var x = 0; x < $scope.lstManejo.length; x++) {
			$scope.lstManejo[x].disposicion_final_id = $scope.tipomanejo.disposicion_final_id;
			$scope.lstManejo[x].cod_usuario = '14';
			
		}
		
		$http({
			method : 'POST',
			url : urlActualizarManejo,
			data : JSON.stringify($scope.lstManejo),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	$scope.actTablaCompMuniDf = function() {

		$scope.tblCompMuniDf = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"comp_muni_df_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					CompMuniDf : $scope.compmunidf
				};

				$http({
					method : 'POST',
					url : urlListarCompMuniDf,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstCompMuni;
					$scope.lstCompMuniDf = data.datosAdicionales.LstCompMuni;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarCompMuniDf = function() {

		for (var x = 0; x < $scope.lstCompMuniDf.length; x++) {
			$scope.lstCompMuniDf[x].disposicion_final_id = $scope.compmunidf.disposicion_final_id;
			$scope.lstCompMuniDf[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCompMuniDf,
			data : JSON.stringify($scope.lstCompMuniDf),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarCompMuniDf = function() {

		for (var x = 0; x < $scope.lstCompMuniDf.length; x++) {
			$scope.lstCompMuniDf[x].disposicion_final_id = $scope.compmunidf.disposicion_final_id;
			$scope.lstCompMuniDf[x].cod_usuario = '14';
			
		}

		$http({
			method : 'POST',
			url : urlActualizarCompMuniDf,
			data : JSON.stringify($scope.lstCompMuniDf),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	$scope.actTablaInfoCeldas = function() {

		$scope.tblInfoCeldas = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"info_celdas_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					InfoCeldas : $scope.infoceldas
				};

				$http({
					method : 'POST',
					url : urlListarInfoCeldas,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstCeldas;
					$scope.lstInfoCeldas = data.datosAdicionales.LstCeldas;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarInfoCeldas = function() {

		for (var x = 0; x < $scope.lstInfoCeldas.length; x++) {
			$scope.lstInfoCeldas[x].disposicion_final_id = $scope.infoceldas.disposicion_final_id;
			$scope.lstInfoCeldas[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarInfoCeldas,
			data : JSON.stringify($scope.lstInfoCeldas),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarInfoCeldas = function() {

		for (var x = 0; x < $scope.lstInfoCeldas.length; x++) {
			$scope.lstInfoCeldas[x].disposicion_final_id = $scope.infoceldas.disposicion_final_id;
			$scope.lstInfoCeldas[x].cod_usuario = '14';
			
		}

		$http({
			method : 'POST',
			url : urlActualizarInfoCeldas,
			data : JSON.stringify($scope.lstInfoCeldas),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	$scope.actTablaCantVehiculos = function() {

		$scope.tblCantVehiculos = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"info_celdas_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					CantVehiculos : $scope.cantvehiculos
				};

				$http({
					method : 'POST',
					url : urlListarCantVehiculos,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstCantVehiculos;
					$scope.lstCantVehiculos = data.datosAdicionales.LstCantVehiculos;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarCantVehiculos = function() {

		for (var x = 0; x < $scope.lstCantVehiculos.length; x++) {
			$scope.lstCantVehiculos[x].disposicion_final_id = $scope.cantvehiculos.disposicion_final_id;
			$scope.lstCantVehiculos[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCantVehiculos,
			data : JSON.stringify($scope.lstCantVehiculos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarCantVehiculos = function() {

		for (var x = 0; x < $scope.lstCantVehiculos.length; x++) {
			$scope.lstCantVehiculos[x].disposicion_final_id = $scope.cantvehiculos.disposicion_final_id;
			$scope.lstCantVehiculos[x].cod_usuario = '14';
			
		}

		$http({
			method : 'POST',
			url : urlActualizarCantVehiculos,
			data : JSON.stringify($scope.lstCantVehiculos),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	$scope.actTablaCompMuniDeg = function() {

		$scope.tblCompMuniDeg = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"comp_muni_deg_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					CompMuniDeg : $scope.compmunideg
				};

				$http({
					method : 'POST',
					url : urlListarCompMuniDeg,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstCompMuni;
					$scope.lstCompMuniDeg = data.datosAdicionales.LstCompMuni;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarCompMuniDeg = function() {

		for (var x = 0; x < $scope.lstCompMuniDeg.length; x++) {
			$scope.lstCompMuniDeg[x].areas_degradadas_id = $scope.compmunideg.areas_degradadas_id;
			$scope.lstCompMuniDeg[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarCompMuniDeg,
			data : JSON.stringify($scope.lstCompMuniDeg),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarCompMuniDeg = function() {

		for (var x = 0; x < $scope.lstCompMuniDeg.length; x++) {
			$scope.lstCompMuniDeg[x].areas_degradadas_id = $scope.compmunideg.areas_degradadas_id;
			$scope.lstCompMuniDeg[x].cod_usuario = '14';
			
		}

		$http({
			method : 'POST',
			url : urlActualizarCompMuniDeg,
			data : JSON.stringify($scope.lstCompMuniDeg),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.actTablaMuniDispDf = function() {

		$scope.tblMuniDispDf = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"muni_disp_df_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					MuniDispDf : $scope.munidispdf
				};

				$http({
					method : 'POST',
					url : urlListarMuniDispDf,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstMuniDispDf;
					$scope.lstMuniDispDf = data.datosAdicionales.LstMuniDisp;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarMuniDispDf = function() {

		for (var x = 0; x < $scope.lstMuniDispDf.length; x++) {
			$scope.lstMuniDispDf[x].disposicion_final_id = $scope.munidispdf.disposicion_final_id;
			$scope.lstMuniDispDf[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarMuniDispDf,
			data : JSON.stringify($scope.lstMuniDispDf),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarMuniDispDf = function() {

		for (var x = 0; x < $scope.lstMuniDispDf.length; x++) {
			$scope.lstMuniDispDf[x].disposicion_final_id = $scope.munidispdf.disposicion_final_id;
			$scope.lstMuniDispDf[x].cod_usuario = '14';
			
		}

		$http({
			method : 'POST',
			url : urlActualizarMuniDispDf,
			data : JSON.stringify($scope.lstMuniDispDf),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	$scope.actTablaMuniDispDeg = function() {

		$scope.tblMuniDispDeg = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"muni_disp_deg_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					MuniDispDeg : $scope.munidispdeg
				};

				$http({
					method : 'POST',
					url : urlListarMuniDispDeg,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstMuniDispDeg;
					$scope.lstMuniDispDeg = data.datosAdicionales.LstMuniDisp;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarMuniDispDeg = function() {

		for (var x = 0; x < $scope.lstMuniDispDeg.length; x++) {
			$scope.lstMuniDispDeg[x].areas_degradadas_id = $scope.munidispdeg.areas_degradadas_id;
			$scope.lstMuniDispDeg[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarMuniDispDeg,
			data : JSON.stringify($scope.lstMuniDispDeg),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarMuniDispDeg = function() {

		for (var x = 0; x < $scope.lstMuniDispDeg.length; x++) {
			$scope.lstMuniDispDeg[x].areas_degradadas_id = $scope.munidispdeg.areas_degradadas_id;
			$scope.lstMuniDispDeg[x].cod_usuario = '14';
			
		}

		$http({
			method : 'POST',
			url : urlActualizarMuniDispDeg,
			data : JSON.stringify($scope.lstMuniDispDeg),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.actTablaAreaAbandonada = function() {

		$scope.tblAreaAbandonada = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"area_abandonada_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					AreaAbandonada : $scope.area_abandonada
				};

				$http({
					method : 'POST',
					url : urlListarAreaAbandonada,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstAreaAbandonada;
					$scope.lstAreaAbandonada = data.datosAdicionales.LstAreaAbandonada;
					
					if($scope.lstAreaAbandonada.length > 0){
						$scope.abandonadas.flag_areas_abandonadas = 1;
						$scope.abandonadas.numero_areas = $scope.lstAreaAbandonada.length;
						$scope.flag3 = 1;
					}
					
					$scope.cantidadAreaAbandonada = $scope.lstAreaAbandonada.length;
					
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarAreaAbandonada = function() {

		for (var x = 0; x < $scope.lstAreaAbandonada.length; x++) {
			$scope.lstAreaAbandonada[x].municipalidad_id = $scope.area_abandonada.municipalidad_id;
			$scope.lstAreaAbandonada[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarAreaAbandonada,
			data : JSON.stringify($scope.lstAreaAbandonada),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarAreaAbandonada = function() {

	var i = $scope.cantidadAreaAbandonada;
	var c = 0;

	console.log("Cantidad de Area Abandonada " + i);
	console.log("Lista Area Abandonada " + $scope.lstAreaAbandonada.length);
	
		for (var x = i; x < $scope.lstAreaAbandonada.length; x++) {
			$scope.lstAreaAbandonadaAux[c].municipalidad_id = $scope.area_abandonada.municipalidad_id;
			
			$scope.lstAreaAbandonadaAux[c].anio_inicio = $scope.lstAreaAbandonada[x].anio_inicio;
			$scope.lstAreaAbandonadaAux[c].anio_cierre = $scope.lstAreaAbandonada[x].anio_cierre;
			$scope.lstAreaAbandonadaAux[c].area_total = $scope.lstAreaAbandonada[x].area_total;
			$scope.lstAreaAbandonadaAux[c].responsable = $scope.lstAreaAbandonada[x].responsable;
			$scope.lstAreaAbandonadaAux[c].altura_nf = $scope.lstAreaAbandonada[x].altura_nf;
			$scope.lstAreaAbandonadaAux[c].cantidad = $scope.lstAreaAbandonada[x].cantidad;
			$scope.lstAreaAbandonadaAux[c].metodo_usado = $scope.lstAreaAbandonada[x].metodo_usado;
			
			$scope.lstAreaAbandonadaAux[c].cod_usuario = '14';
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarAreaAbandonada,
			data : JSON.stringify($scope.lstAreaAbandonadaAux),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.insertar = function(){
		 $scope.lstTrabajador.push({ 
	       	'dni': $scope.trabajadores.dni,
	        'nom_apellido': $scope.trabajadores.nom_apellido,
	        'sexo': $scope.trabajadores.sexo,
	        'rango': $scope.trabajadores.rango,
	        'cargo': $scope.trabajadores.cargo,
	        'laboral': $scope.trabajadores.laboral,
	        'horario': $scope.trabajadores.horario,
	        'dias_trabajados': $scope.trabajadores.dias_trabajados
	    });
		
		 $scope.trabajadores.dni = null;
		 $scope.trabajadores.nom_apellido = null;
		 $scope.trabajadores.sexo = null;
		 $scope.trabajadores.rango = null;
		 $scope.trabajadores.cargo = null;
		 $scope.trabajadores.laboral = null;
		 $scope.trabajadores.horario = null;
		 $scope.trabajadores.dias_trabajados = null;
		 
		 if($scope.flag > 0){
			 
			 $scope.lstTrabajadorAux.push({ 
		        'dni': $scope.trabajadores.dni,
		        'nom_apellido': $scope.trabajadores.nom_apellido,
		        'sexo': $scope.trabajadores.sexo,
		        'rango': $scope.trabajadores.rango,
		        'cargo': $scope.trabajadores.cargo,
		        'laboral': $scope.trabajadores.laboral,
		        'horario': $scope.trabajadores.horario,
		        'dias_trabajados': $scope.trabajadores.dias_trabajados
		     });
		 }
	}
	
	$scope.insertar2 = function(){
		 $scope.lstTrabajador2.push({ 
	       	'dni': $scope.trabajadores2.dni,
	        'nom_apellido': $scope.trabajadores2.nom_apellido,
	        'sexo': $scope.trabajadores2.sexo,
	        'rango': $scope.trabajadores2.rango,
	        'cargo': $scope.trabajadores2.cargo,
	        'laboral': $scope.trabajadores2.laboral,
	        'horario': $scope.trabajadores2.horario,
	        'dias_trabajados': $scope.trabajadores2.dias_trabajados
	    });
		
		 $scope.trabajadores2.dni = null;
		 $scope.trabajadores2.nom_apellido = null;
		 $scope.trabajadores2.sexo = null;
		 $scope.trabajadores2.rango = null;
		 $scope.trabajadores2.cargo = null;
		 $scope.trabajadores2.laboral = null;
		 $scope.trabajadores2.horario = null;
		 $scope.trabajadores2.dias_trabajados = null;
		 
		 if($scope.flg > 0){
			 
			 $scope.lstTrabajadorAux2.push({ 
		        'dni': $scope.trabajadores2.dni,
		        'nom_apellido': $scope.trabajadores2.nom_apellido,
		        'sexo': $scope.trabajadores2.sexo,
		        'rango': $scope.trabajadores2.rango,
		        'cargo': $scope.trabajadores2.cargo,
		        'laboral': $scope.trabajadores2.laboral,
		        'horario': $scope.trabajadores2.horario,
		        'dias_trabajados': $scope.trabajadores2.dias_trabajados
		     });
		 }
	}

	$scope.borrar = function(index){
		$scope.lstTrabajador.splice(index, 1);
	};
	
	$scope.borrar2 = function(index){
		$scope.lstTrabajador2.splice(index, 1);
	};

	app.directive('editableTd', [function() {
	   return {
	      restrict: 'A',
	      link: function(scope, element, attrs) {
	         element.css("cursor", "pointer");
	         element.attr('contenteditable', 'true'); // Referencia: Inciso 1
	         element.bind('blur keyup change', function() { // Referencia: Inciso 2
	            scope.lstTrabajador[attrs.row][attrs.field] = element.text();
	         });
	         element.bind('click', function() { // Referencia: Inciso 3
	            document.execCommand('selectAll', false, null)
	         });
	      }
	   };
	}]);


	/*$scope.insertar = function() {
		
		$scope.grabarTrabajadores();
	}*/
	
	$scope.grabarPersona = function() {
		
		for (var x = 0; x < $scope.lstBarredor.length; x++) {
			$scope.lstTrabajador[x].cod_usuario = '14';
		}
	
		$http({
			method : 'POST',
			url : urlRegistrarPersona,
			data : JSON.stringify($scope.lstBarredor),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.grabarTrabajadores();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
	
		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.grabarTrabajadores = function() {
		
		for (var x = 0; x < $scope.lstTrabajador.length; x++) {
			$scope.lstTrabajador[x].disposicion_final_id = $scope.trabajadores.disposicion_final_id;
			$scope.lstTrabajador[x].cod_usuario = '14';
		}
	
		$http({
			method : 'POST',
			url : urlRegistrarTrabajadores,
			data : JSON.stringify($scope.lstTrabajador),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
	
		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.actualizarTrabajadores = function() {
		
		var i = $scope.cantidad;
		var c = 0;
		
		for (var x = i; x < $scope.lstTrabajador.length; x++) {
			$scope.lstTrabajador[x].disposicion_final_id = $scope.trabajadores.disposicion_final_id;
			$scope.lstTrabajador[x].cod_usuario = '14';
			
			$scope.lstTrabajadorAux[c].disposicion_final_id = $scope.lstTrabajador[x].disposicion_final_id;
			
			$scope.lstTrabajadorAux[c].dni = $scope.lstTrabajador[x].dni;
			$scope.lstTrabajadorAux[c].nom_apellido = $scope.lstTrabajador[x].nom_apellido;
			$scope.lstTrabajadorAux[c].sexo = $scope.lstTrabajador[x].sexo;
			$scope.lstTrabajadorAux[c].rango = $scope.lstTrabajador[x].rango;
			$scope.lstTrabajadorAux[c].cargo = $scope.lstTrabajador[x].cargo;
			$scope.lstTrabajadorAux[c].laboral = $scope.lstTrabajador[x].laboral;
			$scope.lstTrabajadorAux[c].horario = $scope.lstTrabajador[x].horario;
			$scope.lstTrabajadorAux[c].dias_trabajados = $scope.lstTrabajador[x].dias_trabajados;
			
			$scope.lstTrabajadorAux[c].cod_usuario = $scope.lstTrabajador[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarTrabajadores,
			data : JSON.stringify($scope.lstTrabajadorAux),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.actTablaTrabajador = function() {
	
		$scope.tblTrabajador = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"trabajadores_id" : "asc"
			}
		}, {
			getData : function($defer, params) {
				
				var paramJson = {
					Trabajadores : $scope.trabajadores
				};
	
				$http({
					method : 'POST',
					url : urlListarTrabajadores,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstTrabajadores;
					$scope.lstTrabajador = data.datosAdicionales.LstTrabajadores;
					
					for (var x = 0; x < $scope.lstTrabajador.length; x++) {
						
						$scope.lstTrabajador[x].dni = $scope.lstTrabajador[x].persona.dni;
						$scope.lstTrabajador[x].nom_apellido = $scope.lstTrabajador[x].persona.nom_apellidos;
						$scope.lstTrabajador[x].sexo = $scope.lstTrabajador[x].persona.sexo;
						$scope.lstTrabajador[x].rango = $scope.lstTrabajador[x].persona.rango_edad;
						$scope.lstTrabajador[x].cargo = $scope.lstTrabajador[x].cargo;
						$scope.lstTrabajador[x].laboral = $scope.lstTrabajador[x].condicion_laboral;
						$scope.lstTrabajador[x].horario = $scope.lstTrabajador[x].horario_trabajo;
						$scope.lstTrabajador[x].dias_trabajados = $scope.lstTrabajador[x].dias_trabajados;
					}
					
					$scope.cantidad = $scope.lstTrabajador.length;
					
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	$scope.grabarTrabajadores2 = function() {
		
		for (var x = 0; x < $scope.lstTrabajador2.length; x++) {
			$scope.lstTrabajador2[x].disposicion_final_id = $scope.trabajadores2.disposicion_final_id;
			$scope.lstTrabajador2[x].cod_usuario = '14';
		}
	
		$http({
			method : 'POST',
			url : urlRegistrarTrabajadores2,
			data : JSON.stringify($scope.lstTrabajador2),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
	
		}).error(function(status) {
			alertService.showDanger();
		});
	}	
	
	$scope.actualizarTrabajadores2 = function() {
		
		var i = $scope.cantidad2;
		var c = 0;
		
		for (var x = i; x < $scope.lstTrabajador2.length; x++) {
			$scope.lstTrabajador2[x].disposicion_final_id = $scope.trabajadores2.disposicion_final_id;
			$scope.lstTrabajador2[x].cod_usuario = '14';
			
			$scope.lstTrabajadorAux2[c].disposicion_final_id = $scope.lstTrabajador2[x].disposicion_final_id;
			
			$scope.lstTrabajadorAux2[c].dni = $scope.lstTrabajador2[x].dni;
			$scope.lstTrabajadorAux2[c].nom_apellido = $scope.lstTrabajador2[x].nom_apellido;
			$scope.lstTrabajadorAux2[c].sexo = $scope.lstTrabajador2[x].sexo;
			$scope.lstTrabajadorAux2[c].rango = $scope.lstTrabajador2[x].rango;
			$scope.lstTrabajadorAux2[c].cargo = $scope.lstTrabajador2[x].cargo;
			$scope.lstTrabajadorAux2[c].laboral = $scope.lstTrabajador2[x].laboral;
			$scope.lstTrabajadorAux2[c].horario = $scope.lstTrabajador2[x].horario;
			$scope.lstTrabajadorAux2[c].dias_trabajados = $scope.lstTrabajador2[x].dias_trabajados;
			
			$scope.lstTrabajadorAux2[c].cod_usuario = $scope.lstTrabajador2[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarTrabajadores2,
			data : JSON.stringify($scope.lstTrabajadorAux2),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.actTablaTrabajador2 = function() {
	
		$scope.tblTrabajador2 = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"trabajadores_id" : "asc"
			}
		}, {
			getData : function($defer, params) {
				
				var paramJson = {
					Trabajadores2 : $scope.trabajadores2
				};
	
				$http({
					method : 'POST',
					url : urlListarTrabajadores2,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstTrabajadores2;
					$scope.lstTrabajador2 = data.datosAdicionales.LstTrabajadores2;
					
					for (var x = 0; x < $scope.lstTrabajador2.length; x++) {
						
						$scope.lstTrabajador2[x].dni = $scope.lstTrabajador2[x].persona.dni;
						$scope.lstTrabajador2[x].nom_apellido = $scope.lstTrabajador2[x].persona.nom_apellidos;
						$scope.lstTrabajador2[x].sexo = $scope.lstTrabajador2[x].persona.sexo;
						$scope.lstTrabajador2[x].rango = $scope.lstTrabajador2[x].persona.rango_edad;
						$scope.lstTrabajador2[x].cargo = $scope.lstTrabajador2[x].cargo;
						$scope.lstTrabajador2[x].laboral = $scope.lstTrabajador2[x].condicion_laboral;
						$scope.lstTrabajador2[x].horario = $scope.lstTrabajador2[x].horario_trabajo;
						$scope.lstTrabajador2[x].dias_trabajados = $scope.lstTrabajador2[x].dias_trabajados;
					}
					
					$scope.cantidad2 = $scope.lstTrabajador2.length;
					
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	$scope.listarRellenoAdm = function() {
		
		var paramJson = {
			Relleno : $scope.rellenoAdm
		};
		
		$http({
			method : 'POST',
			url : urlListarRellenoAdm,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstRellenoAdm = data.datosAdicionales.LstRellenoAdm;
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.listarDegradadaAdm = function() {
		
		var paramJson = {
			Degradada : $scope.degradadaAdm
		};
		
		$http({
			method : 'POST',
			url : urlListarDegradadaAdm,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstDegradadaAdm = data.datosAdicionales.LstDegradadaAdm;
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.lstUbigeo = function() {

		var paramJson = {
			Ubigeo : {
				ubigeo_id : 150113 
			}
		};

		$http({
			method : 'POST',
			url : urlListUbigeo,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.ubicacion.departamento = data.datosAdicionales.departamento;
			$scope.ubicacion.provincia = data.datosAdicionales.provincia;
			$scope.ubicacion.distrito = data.datosAdicionales.distrito;
			$scope.ubicacion2.departamento = data.datosAdicionales.departamento;
			$scope.ubicacion2.provincia = data.datosAdicionales.provincia;
			$scope.ubicacion2.distrito = data.datosAdicionales.distrito;
			
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.func = function() {
		
		$scope.listarDisposicionAdm();
	}
	
	$scope.func2 = function() {
		
		$scope.listarDegradadas();
	
	}
	
	$scope.volver = function() {
		$location.path("/gestion");
	}
	
	$scope.listarDisposicionAdm();
	$scope.listarDegradadas();
	$scope.actTablaAreaAbandonada();
	
	$scope.grabar1 = function() {
		if($scope.flag1 > 0)
			$scope.actualizarDisposicionAdm();
		else
			$scope.grabarDisposicionAdm();
	}
	
	$scope.grabar2 = function() {
		if($scope.flag2 > 0)
			$scope.actualizarDegradadas();
		else
			$scope.grabarDegradadas();
	}
	
	$scope.grabar3 = function() {
		
		if($scope.flag3 > 0)
			$scope.actualizarAreaAbandonada();
		else
			$scope.grabarAreaAbandonada();
	}
	
	$scope.agregar = function(){
		
		if($scope.abandonadas.numero_areas == null)
			$scope.abandonadas.numero_areas = 0;
		
		$scope.lstAreaAbandonada.push({
			'anio_inicio' : null,
			'anio_cierre' : null,
			'area_total' : null,
			'responsable' : null,
			'altura_nf' : null,
			'cantidad' : null,
			'metodo_usado' : null
		});
		
		if($scope.flag3 > 0){
			
			$scope.lstAreaAbandonadaAux.push({ 
				   'anio_inicio': null,
				   'anio_cierre': null,
				   'area_total': null,
				   'responsable': null,
				   'altura_nf': null,
				   'cantidad': null,
				   'metodo_usado': null       
				});
		}

		$scope.abandonadas.numero_areas = $scope.abandonadas.numero_areas + 1;
	}
	
	$scope.setAreaDisponible = function(){
		$scope.disposicion_adm.area_disponible = $scope.disposicion_adm.area_total_terreno - $scope.disposicion_adm.area_utilizada_terreno;
		$scope.degradadas.area_disponible = $scope.degradadas.area_total_terreno - $scope.degradadas.area_utilizada_terreno;
	}
	
	$scope.setAreaDisponible();
	
}