function ParametroController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams,$sce, $timeout) {
	
	$scope.nombre_tabla = -1;
	
	$controller('funcGenerales', {
		$scope : $scope
	});
	
	$scope.eDefParametro = {};
	$scope.dataTabla = {};
	$scope.dato_anterior = "";
	$scope.dEditable = 0;
	$scope.accion = 0;
	$scope.id = 0;
	$scope.textoError = "";
	
	//url web service
	
	var urlObtenerFiltroParametro = $scope.urlRest + "/rest/unicodeParametro/obtenerLista";
	var urlObtenerListaTabla = $scope.urlRest + "/rest/unicodeParametro/obtenerListaTablas";
	var urlInsertarTabla = $scope.urlRest + "/rest/unicodeParametro/insertarTabla";
	var urlEditarTabla = $scope.urlRest + "/rest/unicodeParametro/actualizar_tabla";
	var urlEditarParametro = $scope.urlRest + "/rest/unicodeParametro/actualizarParametro";
	
	$scope.obtenerListaNombreTabla = function(){
		$http(
				{
					method : 'POST',
					url : urlObtenerListaTabla,
					data : $.param({
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					},
				}
			)
			.success(
				function(data) {
					$scope.listaTablas = data,
					$scope.dataTabla = data
				}
			)
			.error(
				function(status) {
					alertService.showDanger();
				}
			);
	}
	
	//list paramters table
	$scope.tblParametro = new NgTableParams(
			{
				page : 1,
				count : 10,
				sorting : {
					"codigo_parametro" : "asc"
				}
			},
			{
				counts : [10,15,20,25],
				getData : function($defer, params){
					var paramJsonSinParam = { UbigeoIngr : {} };
					$http(
						{
							method : 'POST',
							url : urlObtenerFiltroParametro,
							data : $.param({
								"nombre_tabla" : $scope.nombre_tabla
							}),
							headers : {
								'Content-type' : 'application/x-www-form-urlencoded; charset=UTF-8'
							}
						}	
					)
					.success(
						function(data){
							blockUI.start();
							var filtro = {};
							var NewData = [];
							for (var x = 0; x < data.datosAdicionales.Parametro.length; x++) {
								var lista = {
									"codigo_parametro" : data.datosAdicionales.Parametro[x].unicode_parametro_id,
									"codigo_parametro_dato" : data.datosAdicionales.Parametro[x].codigo_parametro,
									"valor_parametro" : data.datosAdicionales.Parametro[x].valor_parametro,
									"codigo_estado" : data.datosAdicionales.Parametro[x].codigo_estado == "1" ? true : false
								}
								NewData.push(lista);
							}

							var nData = NewData;
							params.total(nData.length);
							$scope.orderedData = params.sorting() ? $filter('orderBy')(nData, params.orderBy()) : nData;
							var filterData = filtro ? $filter('filter')($scope.orderedData, filtro): $scope.orderedData;
							params.total(filterData.length);
							$defer.resolve(filterData.slice((params.page() - 1)	* params.count(), params.page()	* params.count()));
							
							blockUI.stop();
						
					}
					)
					.error(function(status){
						$scope.status = status;
					});
				  }     
		    	}
			);
	
	$scope.obtenerListaNombreTabla();
	
	$scope.filtrarParametro = function(){
		$scope.tblParametro.reload();
		for(i=0 ; i<$scope.dataTabla.length ; i++){
			if($scope.dataTabla[i]['nombre_tabla'].trim() == $scope.nombre_tabla.trim()){
				$scope.dEditable = $scope.dataTabla[i]['editable'];
			}
		}		
	}
	
	$scope.limpiarCamposForm = function(){
		$scope.eDefParametro.nombre_tabla = "";
		$scope.eDefParametro.codigo_parametro = "";
		$scope.eDefParametro.valor_parametro = "";
		$scope.eDefParametro.bcodigo_estado = false;
	}
	
	$scope.limpiarCamposForm();
	
	$scope.abrirNuevoTabla = function(){
		$scope.accion = 0;
		$scope.titulo = "Nueva tabla";
		$('#modalParametro').modal();
		$scope.limpiarCamposForm();
		$scope.verElementos('block',1);
	}
	
	$scope.actualizarTabla = function(){
		
		if($scope.nombre_tabla == "" || $scope.nombre_tabla =="-1" || $scope.nombre_tabla == undefined){
			    $scope.textoError = "Seleccione una tabla."
				$('#modalError').modal();
		}else{
			if($scope.dEditable == 2){
				$scope.textoError = "La tabla seleccionada no es editable."
				$('#modalError').modal();
			}else{
				$scope.accion = 1;
				$scope.titulo = "Editar tabla"
					
				$('#modalParametro').modal();		
				$scope.limpiarCamposForm();
				$scope.eDefParametro.nombre_tabla = $scope.nombre_tabla.trim();
				$scope.dato_anterior = $scope.nombre_tabla.trim();
				$scope.verElementos('none',1);
			}
		}	
		
	}
	
	$scope.abrirNuevoParametro = function(){
		
		if($scope.nombre_tabla == "" || $scope.nombre_tabla =="-1" || $scope.nombre_tabla == undefined){
			$scope.textoError = "Seleccione una tabla."
			$('#modalError').modal();
		}else{
			$scope.accion = 2;
			$scope.titulo = "Agregar parámetro";
			$scope.limpiarCamposForm();
			$('#modalParametro').modal();
			$scope.verElementos('block',0);
			$scope.eDefParametro.nombre_tabla = $scope.nombre_tabla;
		}
		
	}
	
	$scope.editarParametro =  function(x){
		
		if($scope.dEditable == 2){
			$scope.textoError = "Registro no editable.";
			$('#modalError').modal();
		}else{
			$scope.accion = 3;
			$scope.titulo = "Editar parámetro";
			$scope.limpiarCamposForm();		
			$scope.verElementos('block',0);
			$scope.id = x.codigo_parametro;
			$scope.eDefParametro.nombre_tabla = $scope.nombre_tabla;
			$scope.eDefParametro.codigo_parametro = x.codigo_parametro_dato;
			$scope.eDefParametro.valor_parametro = x.valor_parametro
			
			if(x.codigo_estado == "1" ){
				$scope.eDefParametro.bcodigo_estado = true;
			}else{
				$scope.eDefParametro.bcodigo_estado = false;
			}
			$('#modalParametro').modal();
	   }		
	}
	
	$scope.verElementos = function(evento,num){
		document.getElementById('label_codigo_parametro').style.display = evento;
		document.getElementById('codigo_parametro').style.display = evento;
		document.getElementById('label_valor_parametro').style.display = evento;
		document.getElementById('valor_parametro').style.display = evento;
		document.getElementById('label_estado').style.display = evento;
		document.getElementById('estado').style.display = evento;      
		document.getElementById('label_activo').style.display = evento;
		if(num == 1){
			document.getElementById("id_nombre_tabla").disabled = false;
		}else{
			document.getElementById("id_nombre_tabla").disabled = true;
		}
	}
	
	$scope.grabarTabla = function(){
		var valido = true;
		
		   
		if($scope.accion == 1){
			$scope.grabarEditarTabla();
		}else{	
			if($scope.eDefParametro.nombre_tabla == "" || $scope.eDefParametro.nombre_tabla == undefined){
				$scope.textoError = "Ingrese nombre de tabla.";
				$('#modalError').modal();
				valido = false;
				return;
			}
			
			if($scope.eDefParametro.codigo_parametro == "" || $scope.eDefParametro.codigo_parametro == undefined){
				$scope.textoError = "Ingrese código de parámetro.";
				$('#modalError').modal();
				valido = false;
				return;
			}
			
			if($scope.eDefParametro.valor_parametro == "" || $scope.eDefParametro.valor_parametro == undefined){
				$scope.textoError = "Ingrese valor de parámetro.";
				$('#modalError').modal();
				valido = false;
				return;
			}
			
			if($scope.eDefParametro.bcodigo_estado == true){
				$scope.eDefParametro.codigo_estado = "1";
			}else{
				$scope.eDefParametro.codigo_estado = "0";
			}
			
			if(valido == true){
				if($scope.accion == 0 || $scope.accion == 2){				
					$scope.grabarNuevaTabla($scope.accion,urlInsertarTabla,-1,$scope.eDefParametro.nombre_tabla,
						$scope.eDefParametro.codigo_parametro,$scope.eDefParametro.valor_parametro,$scope.eDefParametro.codigo_estado);
				}else if($scope.accion == 3){
					$scope.grabarNuevaTabla($scope.accion,urlEditarParametro,$scope.id,$scope.eDefParametro.nombre_tabla,
							$scope.eDefParametro.codigo_parametro,$scope.eDefParametro.valor_parametro,$scope.eDefParametro.codigo_estado);
				}	
			}
		}
		
	}
	
	$scope.grabarNuevaTabla= function(scopeAction,url,id,tabla,codigo,valor,estado) {
		var paramJsonRegistrar = {
			TablaIngr : {
				id_parametro : id,
				nombre_tabla : tabla,
				codigo_parametro : codigo,
				valor_parametro : valor,
				codigo_estado : estado,
				editable : 1
			}
		};
		
		json = JSON.stringify(paramJsonRegistrar);
		
		$http(
			{
				method : 'POST',
				url : url,
				data : $.param({
					"paramJson" : json
				}),
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}
		).success(
			function(data){
				alertService.showSuccess('Cambios realizados con éxito.');
				$scope.tblParametro.reload();
				$scope.limpiarCamposForm();
				$("#modalParametro").modal('hide');
				if(scopeAction == 0){
					$scope.obtenerListaNombreTabla();
					$scope.nombre_tabla = -1;
					$scope.tblParametro.reload();
				}
			}
		).error(
			function(status) {
				alertService.showDanger();
			}
		);
	}	
	
	$scope.grabarEditarTabla = function() {
		var paramJsonEditar = { TablaIngr : {} };
		
		$http(
			{
				method : 'POST',
				url : urlEditarTabla,
				data : $.param({
					"tabla_anterior" : $scope.dato_anterior,
					"tabla_nueva" : $scope.eDefParametro.nombre_tabla
				}),
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}
		).success(
			function(data){
				alertService.showSuccess('Curso fue guardado.');
				$scope.tblParametro.reload();
				$scope.limpiarCamposForm();
				$("#modalParametro").modal('hide');
				$scope.obtenerListaNombreTabla();
				$scope.nombre_tabla = -1;
			}
		).error(
			function(status) {
				alertService.showDanger();
			}
		);
	}
		
	
	
}