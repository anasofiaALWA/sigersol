function MasterProfesionalController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	// $scope.validarLogin();

	$(function() {
		$('#idContenedor').tabs();
		$('.ui-widget-header').css("background-color", "#eeeeee");
		$('.ui-tabs .ui-tabs-nav .ui-tabs-anchor').css("float", "none");
		$('.ui-widget-header').css("border", "0");
	});

	$('#lista').click(function(e) {
		var id = e.target.id;
		console.log("id tab >> " + id);
		switch (id) {
		case "idAcademico":
			break;
		case "idOperativo":
			// $scope.cargaInicialSeguro();
			break;
		case "idApreciacion":
			// $scope.cargaInicialImagen();
			break;

		}

	});

	$scope.paramFilter = {
		"persona_id" : $localStorage.PERSONA_ID,
		"datos_persona" : $localStorage.DATOS_PERSONA,
		"nro_cip" : $localStorage.CIP,
		"tipo_persona" : $localStorage.TIPO_PERSONA,
		"accion" : $localStorage.ACCION
	// 2:Editar //1:Nuevo
	};

	/** Tab Antecedentes Academicos ** */

	$scope.flagDisabEM = true;
	$scope.flagMerito = false;
	$scope.flagNota = false;

	$scope.estMilitar = {};
	$scope.estNoMilitar = {};
	$scope.exaAscenso = {};
	$scope.demeritoAcad = {};

	// Declarando las listas

	$scope.lstTipoCurso = [];
	$scope.lstModalidad = [];
	$scope.lstPais = [];
	$scope.lstInstitucion = [];
	$scope.lstTipoVacante = [];
	$scope.lstApreciacion = [];
	$scope.lstNivelCurso = [];
	$scope.lstCategoria = [];
	$scope.lstNivelIdioma = [];
	$scope.lstCurso = [];
	$scope.lstInstitucionENM = [];
	$scope.lstClaseDemerito = [];
	$scope.lstInstitucionDA = [];

	// URL
	var urlListaTipoCurso = $scope.urlRest + "/rest/curso/listaTipo";
	var urlListalugar = $scope.urlRest + "/rest/pais/lista";
	var urlParametros = $scope.urlRest
			+ "/rest/parametros/obtener_unicodeparametros";
	var urlListaCursoXfiltro = $scope.urlRest + "/rest/curso/listaXfiltro";
	var urlListaInstitucion = $scope.urlRest + "/rest/institucion/lista";
	var urlListaPais = $scope.urlRest + "/rest/pais/listar";
	var urlGeneraQuinto = $scope.urlRest + "/rest/academico/generaQuinto";
	var urlRegistrarAcademico = $scope.urlRest + "/rest/academico/registrar";

	if ($scope.paramFilter.tipo_persona == "B") {
		$scope.flagDisabEM = false;
		$scope.cargaLstModalidad();
	}

	$scope.selectTipoCalificacion = function() {
		if (estMilitar.tipoCalif_selected.codigo_parametro == 1) {
			// Merito
			$scope.flagMerito = true;
		}
		if (estMilitar.tipoCalif_selected.codigo_parametro == 2) {
			// Nota
			$scope.flagNota = true;
		}
	}

	// /Modales
	$scope.nuevoEstudiosMilitares = function() {
		$scope.estMilitar.flagAccion = 1;
		$scope.titulo = "Nuevo Estudios Militares";
		$('#modalEstudiosMilitares').modal();

	}

	$scope.nuevoEstudiosNoMilitares = function() {
		$scope.estNoMilitar.flagAccion = 1;
		$scope.titulo = "Nuevo Estudios No Militares";
		$('#modalEstudiosNoMilitares').modal();
	}

	$scope.nuevoExamenAscenso = function() {
		$scope.exaAscenso.flagAccion = 1;
		$scope.titulo = "Nuevo Examen de Ascenso";
		$('#modalExamenAscenso').modal();
	}

	$scope.nuevoDemeritoAcademico = function() {
		$scope.demeritoAcad.flagAccion = 1;
		$scope.titulo = "Nuevo Demerito Academico";
		$('#modalDemeritoAcademico').modal();
	}

	// /accediendo a los REST

	$scope.cargaLstTipoCurso = function() {
		var paramJson = {
			Filtro : {
				idTipoPersona : $scope.paramFilter.tipo_persona
			}
		}

		$http({
			method : 'POST',
			url : urlListaTipoCurso,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							console
									.log("LstTipoCurso : "
											+ JSON
													.stringify(data.datosAdicionales.LstTipoCurso));
							$scope.lstTipoCurso = data.datosAdicionales.LstTipoCurso;
						}).error(function(status) {

				});
	}

	$scope.cargaLstPais = function() {

		$http({
			method : 'POST',
			url : urlListaPais,
			data : "",
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(
				function(data) {
					console.log("Pais : "
							+ JSON.stringify(data.datosAdicionales.LstPais));
					$scope.lstPais = data.datosAdicionales.LstPais;
				}).error(function(status) {

		});
	}

	$scope.getLstParametro = function(nombreTabla) {

		$http(
				{
					method : 'POST',
					url : urlParametros,
					data : $.param({
						"nombre_tabla" : nombreTabla,
						"accion" : 2
					}),
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					},
				}).success(function(data) {
			switch (nombreTabla) {
			case "TIPO_VACANTE":
				$scope.lstTipVacante = data;
				break;
			case "TIPO_CALIFICACION":
				$scope.lstApreciacion = data;
				break;
			case "MODALIDAD_CURSO":
				$scope.lstModalidad = data;
				break;

			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.cargaLstTipoVacante = function() {
		$scope.getLstParametro('TIPO_VACANTE');
	}

	$scope.cargaLstTipoCalificacion = function() {
		$scope.getLstParametro('TIPO_CALIFICACION');
	}

	$scope.cargaLstModalidad = function() {
		$scope.getLstParametro('MODALIDAD_CURSO');
	}

	$scope.cargaLstCurso = function() {
		var paramJson = {
			Filtro : {
				idCategoria : '-1',
				idTipo : $scope.estMilitar.tipoCurso_selected.codigo_parametro
			}
		}

		$http({
			method : 'POST',
			url : urlListaCursoXfiltro,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(
				function(data) {
					console.log("lstCurso : "
							+ JSON.stringify(data.datosAdicionales.LstCurso));
					$scope.lstCurso = data.datosAdicionales.LstCurso;
				}).error(function(status) {

		});

	}

	$scope.cargaLstInstitucion = function() {
		var paramJson = {
			Filtro : {
				idPais : $scope.estMilitar.pais_selected.pais_id
			}
		}

		$http({
			method : 'POST',
			url : urlListaInstitucion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(
				function(data) {
					console.log("lstCurso : "
							+ JSON.stringify(data.datosAdicionales.LstCurso));
					$scope.lstCurso = data.datosAdicionales.LstCurso;
				}).error(function(status) {

		});

	}

	$scope.generarQuinto = function() {
		var paramJson = {
			Filtro : {
				puesto : $scope.estMilitar.puesto,
				nota : $scope.estMilitar.valorCalif,
				totalAlumnos : $scope.estMilitar.totalAlumnos
			}
		}

		$http({
			method : 'POST',
			url : urlGeneraQuinto,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(
				function(data) {
					console.log("quinto : "
							+ JSON.stringify(data.datosAdicionales.Quinto));
					$scope.estMilitar.quinto = data.datosAdicionales.Quinto;
				}).error(function(status) {

		});
	}

	$scope.cargaInicialAcademico = function() {
		$scope.cargaLstTipoCalificacion();
		$scope.cargaLstTipoCurso();
		$scope.cargaLstPais();
		$scope.cargaLstTipoVacante();
	}

	$scope.cargaInicialAcademico();

}