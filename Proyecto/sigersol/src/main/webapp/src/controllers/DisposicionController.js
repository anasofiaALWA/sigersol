function DisposicionController($scope, $http, $routeParams, $location,
		$rootScope, $filter, NgTableParams, $controller, CONFIG, OPCIONES,
		$localStorage, APIservice, alertService, blockUI, $window,
		ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	$scope.flag = 0;
	$scope.anioAnterior ="2017";
	
	var urlRegistrarDisposicion = $scope.urlRest 
			+ "/rest/disposicion/registrar";
	var urlActualizarDisposicion = $scope.urlRest
			+ "/rest/disposicion/actualizar";
	var urlListarDisposicion = $scope.urlRest 
			+ "/rest/disposicion/obtener";
	
	var urlRegistrarDisposicion2 = $scope.urlRest 
		+ "/rest/disposicion2/registrar";
	var urlActualizarDisposicion2 = $scope.urlRest
		+ "/rest/disposicion2/actualizar";
	var urlListarDisposicion2 = $scope.urlRest 
		+ "/rest/disposicion2/obtener";

	
	$scope.disposicion = {};
	$scope.disposicion2 = {};
	
	var urlRegistrarDegradadas = $scope.urlRest 
		+ "/rest/degradadas/registrar";
	var urlActualizarDegradadas = $scope.urlRest
		+ "/rest/degradadas/actualizar";
	var urlListarDegradadas = $scope.urlRest 
		+ "/rest/degradadas/obtener";
	
	
	$scope.degradadas = {};
	$scope.lstDegradadas = [];
	
	var urlRegistrarAbandonadas = $scope.urlRest 
		+ "/rest/abandonadas/registrar";
	var urlActualizarAbandonadas = $scope.urlRest
		+ "/rest/abandonadas/actualizar";
	var urlListarAbandonadas = $scope.urlRest 
		+ "/rest/abandonadas/obtener";
	
	$scope.abandonadas = {};
		
	var urlRegistrarManejo = $scope.urlRest 
		+ "/rest/tipoManejo/registrar";
	var urlActualizarManejo = $scope.urlRest
		+ "/rest/tipoManejo/actualizar";
	var urlListarManejo = $scope.urlRest 
		+ "/rest/tipoManejo/listarManejo";
	
	$scope.tipomanejo = {};
	$scope.lstManejo = [];
	
	var urlRegistrarCompMuniDf = $scope.urlRest 
		+ "/rest/compMuniDf/registrar";
	var urlActualizarCompMuniDf = $scope.urlRest
		+ "/rest/compMuniDf/actualizar";
	var urlListarCompMuniDf = $scope.urlRest 
		+ "/rest/compMuniDf/listarCompMuni";
	
	$scope.compmunidf = {};
	$scope.lstCompMuniDf = [];
	
	var urlRegistrarInfoCeldas = $scope.urlRest 
		+ "/rest/infoCeldas/registrar";
	var urlActualizarInfoCeldas = $scope.urlRest
		+ "/rest/infoCeldas/actualizar";
	var urlListarInfoCeldas = $scope.urlRest 
		+ "/rest/infoCeldas/listarCeldas";
	
	$scope.infoceldas = {};
	$scope.lstInfoCeldas = [];
	
	var urlRegistrarCantVehiculos = $scope.urlRest 
		+ "/rest/cantVehiculos/registrar";
	var urlActualizarCantVehiculos = $scope.urlRest
		+ "/rest/cantVehiculos/actualizar";
	var urlListarCantVehiculos = $scope.urlRest 
		+ "/rest/cantVehiculos/listarCantidadVehiculos";

	$scope.cantvehiculos = {};
	$scope.lstCantVehiculos = [];
		
	var urlRegistrarCompMuniDeg = $scope.urlRest 
		+ "/rest/compMuniDeg/registrar";
	var urlActualizarCompMuniDeg = $scope.urlRest
		+ "/rest/compMuniDeg/actualizar";
	var urlListarCompMuniDeg = $scope.urlRest 
		+ "/rest/compMuniDeg/listarCompMuni";

	$scope.compmunideg = {};
	$scope.lstCompMuniDeg = [];
	
	var urlRegistrarMuniDispDf = $scope.urlRest 
		+ "/rest/MuniDispDf/registrar";
	var urlActualizarMuniDispDf = $scope.urlRest
		+ "/rest/MuniDispDf/actualizar";
	var urlListarMuniDispDf = $scope.urlRest 
		+ "/rest/MuniDispDf/listarMuniDisp";

	$scope.munidispdf = {};
	$scope.lstMuniDispDf = [];
		
	var urlRegistrarMuniDispDeg = $scope.urlRest 
		+ "/rest/MuniDispDeg/registrar";
	var urlActualizarMuniDispDeg = $scope.urlRest
		+ "/rest/MuniDispDeg/actualizar";
	var urlListarMuniDispDeg = $scope.urlRest 
		+ "/rest/MuniDispDeg/listarMuniDisp";

	$scope.munidispdeg = {};
	$scope.lstMuniDispDeg = [];
	
	$scope.relleno = {};
	$scope.lstRelleno = [];
	
	$scope.degradada = {};
	$scope.lstDegradada = [];
	
	var urlListarRelleno = $scope.urlRest 
		+ "/rest/sitioDisposicion/obtener";
	
	var urlListarDegradada = $scope.urlRest 
		+ "/rest/sitioDisposicion/listar";
	
	$scope.flag1 = 0;
	$scope.flag2 = 0;
	
	$scope.disposicion.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.disposicion2.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.disposicion.cod_usuario = '14';
	$scope.disposicion2.cod_usuario = '14';
	
	$localStorage.RELLENO = 0;
	$localStorage.DEGRADADA = 0;
	
	$scope.degradadas.cod_usuario =  '14';
	$scope.abandonadas.cod_usuario = '14';
	
	$scope.guardar = function() {
		if ($scope.flag == 0){
			$scope.grabarDisposicion();
		}
		else{
			$scope.actualizarDisposicion();
		}
	}

	$scope.grabarDisposicion = function() {

		var paramJson = {
			Disposicion : $scope.disposicion
		};

		$http({
			method : 'POST',
			url : urlRegistrarDisposicion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.flag = 1;
							
								$scope.munidispdf.selec_relleno = $scope.disposicion.selec_relleno;
								$scope.munidispdf.disposicion_final_id = data.datosAdicionales.id;
								//$scope.munidispdeg.disposicion_final_id = data.datosAdicionales.id;
								
								$scope.grabarMuniDispDf();
								//$scope.grabarMuniDispDeg();
								
								//$location.path("/gestion");

								$localStorage.GENERACION = '1';
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarDisposicion = function() {

		var paramJson = {
			Disposicion : $scope.disposicion
		};

		$http({
			method : 'POST',
			url : urlActualizarDisposicion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								
								$scope.munidispdf.selec_relleno = $scope.disposicion.selec_relleno;
								$scope.munidispdf.disposicion_final_id = data.datosAdicionales.id;
								//$scope.munidispdeg.disposicion_final_id = data.datosAdicionales.id;
									
								$scope.actualizarMuniDispDf();
								//$scope.actualizarMuniDispDeg();
							} else {
								alertService
										.showDanger("No se realizó el registro1.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		//$location.path("/gestion");
	}

	$scope.listarDisposicion = function() {

		var aux = $scope.disposicion.selec_relleno;
		
		var paramJson = {
			Disposicion : $scope.disposicion
		};

		$http({
			method : 'POST',
			url : urlListarDisposicion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.disposicion = data.datosAdicionales.disposicion;
							if ($scope.disposicion.disposicion_final_id != undefined) {
								$scope.flag = 1;				
								$scope.flag1 = 1;
								
								if($scope.disposicion.flag_df == 1){
									$scope.disposicion.flag_df = true;
								}
								if($scope.disposicion.flag_df2 == 1){
									$scope.disposicion.flag_df2 = true;
								}
	
								$scope.munidispdf.disposicion_final_id = $scope.disposicion.disposicion_final_id;
								//$scope.munidispdeg.disposicion_final_id = $scope.disposicion.disposicion_final_id;
																
							} else {
								$scope.flag1 = 0;
								$scope.munidispdf.disposicion_final_id = null;
							}

							$scope.relleno.ubigeo_id = '150113'; /*JESUS MARIA*/
							//$scope.relleno.ubigeo_id = '101010';
							//$scope.degradada.municipalidad_id = 4;
							$scope.disposicion.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.disposicion.cod_usuario = '12';
			
							$scope.actTablaMuniDispDf();
							//$scope.actTablaMuniDispDeg();
							$scope.listarRelleno();
							//$scope.listarDegradada();
							
							$scope.disposicion.selec_relleno = aux;
							$localStorage.RELLENO = $scope.disposicion.selec_relleno;
							
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	
	$scope.grabarDisposicion2 = function() {

		var paramJson = {
			Disposicion2 : $scope.disposicion2
		};

		$http({
			method : 'POST',
			url : urlRegistrarDisposicion2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								$scope.flag = 1;
							
								$scope.munidispdeg.selec_degradada = $scope.disposicion2.selec_degradada;
								//$scope.munidispdf.disposicion_final_id = data.datosAdicionales.id;
								//$scope.relleno.ubigeo_id = '150113';
								$scope.degradada.ubigeo_id = '101010';
								$scope.munidispdeg.disposicion_final_id = data.datosAdicionales.id;
								
								//$scope.grabarMuniDispDf();
								$scope.grabarMuniDispDeg();
								
								//$location.path("/gestion");

								$localStorage.GENERACION = '1';
							} else {
								alertService
										.showDanger("No se realizó el registro.");
							}

						}).error(function(status) {
					alertService.showDanger();
				});

	}

	$scope.actualizarDisposicion2 = function() {

		var paramJson = {
			Disposicion2 : $scope.disposicion2
		};

		$http({
			method : 'POST',
			url : urlActualizarDisposicion2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							if (data.datosAdicionales.id > 0) {
								alertService.showSuccess("Registro exitoso.");
								
								$scope.munidispdeg.selec_degradada = $scope.disposicion2.selec_degradada;
								//$scope.munidispdf.disposicion_final_id = data.datosAdicionales.id;
								$scope.munidispdeg.disposicion_final_id = data.datosAdicionales.id;
								
								//$scope.actualizarMuniDispDf();
								$scope.actualizarMuniDispDeg();
							} else {
								alertService
										.showDanger("No se realizó el registro1.");
							}
						}).error(function(status) {
					alertService.showDanger();
				});

		//$location.path("/gestion");
	}

	$scope.listarDisposicion2 = function() {

		var aux = $scope.disposicion2.selec_degradada;
		
		var paramJson = {
			Disposicion2 : $scope.disposicion2
		};

		$http({
			method : 'POST',
			url : urlListarDisposicion2,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.disposicion2 = data.datosAdicionales.disposicion2;
							if ($scope.disposicion2.disposicion_final_id != undefined) {
								$scope.flag = 1;				
								$scope.flag2 = 1;
								
								$scope.munidispdeg.selec_degradada = $scope.disposicion2.selec_degradada;
								$scope.munidispdeg.disposicion_final_id = $scope.disposicion2.disposicion_final_id;		
								
							} else {
								$scope.flag2 = 0;
								$scope.munidispdeg.disposicion_final_id = null;
							}

							//$scope.degradada.municipalidad_id = 4;
							$scope.disposicion2.ciclo_grs_id = $localStorage.GRS_ID;
							$scope.disposicion2.cod_usuario = '12';
							$scope.degradada.ubigeo_id = '150113';
							//$scope.actTablaMuniDispDf();
							$scope.actTablaMuniDispDeg();
							$scope.listarDegradada();
							$scope.disposicion2.selec_degradada = aux;
							$localStorage.DEGRADADA = $scope.disposicion2.selec_degradada;
							
						}).error(function(status) {
					alertService.showDanger();
				});
	}
	
	
	$scope.actTablaMuniDispDf = function() {

		$scope.tblMuniDispDf = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"muni_disp_df_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					MuniDispDf : $scope.munidispdf
				};

				$http({
					method : 'POST',
					url : urlListarMuniDispDf,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstMuniDisp;
					$scope.lstMuniDispDf = data.datosAdicionales.LstMuniDisp;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarMuniDispDf = function() {

		for (var x = 0; x < $scope.lstMuniDispDf.length; x++) {
			$scope.lstMuniDispDf[x].disposicion_final_id = $scope.munidispdf.disposicion_final_id;
			$scope.lstMuniDispDf[x].selec_relleno = $scope.munidispdf.selec_relleno;
			$scope.lstMuniDispDf[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarMuniDispDf,
			data : JSON.stringify($scope.lstMuniDispDf),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarMuniDispDf = function() {

		for (var x = 0; x < $scope.lstMuniDispDf.length; x++) {
			$scope.lstMuniDispDf[x].disposicion_final_id = $scope.munidispdf.disposicion_final_id;
			$scope.lstMuniDispDf[x].selec_relleno = $scope.munidispdf.selec_relleno;
			$scope.lstMuniDispDf[x].cod_usuario = '14';
			
		}

		$http({
			method : 'POST',
			url : urlActualizarMuniDispDf,
			data : JSON.stringify($scope.lstMuniDispDf),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	
	$scope.actTablaMuniDispDeg = function() {

		$scope.tblMuniDispDeg = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"muni_disp_deg_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					MuniDispDeg : $scope.munidispdeg
				};

				$http({
					method : 'POST',
					url : urlListarMuniDispDeg,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstMuniDisp;
					$scope.lstMuniDispDeg = data.datosAdicionales.LstMuniDisp;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}
	
	
	$scope.grabarMuniDispDeg = function() {

		for (var x = 0; x < $scope.lstMuniDispDeg.length; x++) {
			$scope.lstMuniDispDeg[x].disposicion_final_id = $scope.munidispdeg.disposicion_final_id;
			$scope.lstMuniDispDeg[x].selec_degradada = $scope.munidispdeg.selec_degradada;
			$scope.lstMuniDispDeg[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarMuniDispDeg,
			data : JSON.stringify($scope.lstMuniDispDeg),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarMuniDispDeg = function() {

		for (var x = 0; x < $scope.lstMuniDispDeg.length; x++) {
			$scope.lstMuniDispDeg[x].disposicion_final_id = $scope.munidispdeg.disposicion_final_id;
			$scope.lstMuniDispDeg[x].selec_degradada = $scope.munidispdeg.selec_degradada;
			$scope.lstMuniDispDeg[x].cod_usuario = '14';
			
		}

		$http({
			method : 'POST',
			url : urlActualizarMuniDispDeg,
			data : JSON.stringify($scope.lstMuniDispDeg),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}
	
	$scope.listarRelleno = function() {
		
		var paramJson = {
			Relleno : $scope.relleno
		};
		
		$http({
			method : 'POST',
			url : urlListarRelleno,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstRelleno = data.datosAdicionales.LstRelleno;
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.listarDegradada = function() {
		
		var paramJson = {
			Degradada : $scope.degradada
		};
		
		$http({
			method : 'POST',
			url : urlListarDegradada,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.lstDegradada = data.datosAdicionales.LstDegradada;
		}).error(function(status) {
			alertService.showDanger();
		});
	}
	
	$scope.func = function() {
		
		$scope.disposicion.relleno = null;
		$scope.disposicion.costo = null;
		
		for (var x = 0; x < $scope.lstMuniDispDf.length; x++) {
			$scope.lstMuniDispDf[x].cantidad = null;
			$scope.lstMuniDispDf[x].metodo_usado = null;	
		}
		
		$scope.munidispdf.selec_relleno = $scope.disposicion.selec_relleno;
		
		$scope.listarDisposicion();
		//$scope.actTablaMuniDispDf();
		
	}
	
	$scope.func2 = function() {
		
		$scope.disposicion2.degradada = null;
		$scope.disposicion2.costo_degradadas = null;
		
		for (var x = 0; x < $scope.lstMuniDispDeg.length; x++) {
			$scope.lstMuniDispDeg[x].cantidad = null;
			$scope.lstMuniDispDeg[x].metodo_usado = null;	
		}
		
		$scope.munidispdeg.selec_degradada = $scope.disposicion2.selec_degradada;
		
		$scope.listarDisposicion2();
		//$scope.actTablaMuniDispDeg();
	
	}
	
	$scope.grabar = function() {
		if($scope.flag1 > 0)
			$scope.actualizarDisposicion();
		else
			$scope.grabarDisposicion();
	}
	
	$scope.grabar2 = function() {
		if($scope.flag2 > 0)
			$scope.actualizarDisposicion2();
		else
			$scope.grabarDisposicion2();
	}
	
	$scope.volver = function() {
		$location.path("/gestion");
	}
	
	$scope.listarDisposicion();
	$scope.listarDisposicion2();
}