function BarridoController($scope, $http, $routeParams, $location, $rootScope,
		$filter, NgTableParams, $controller, CONFIG, OPCIONES, $localStorage,
		APIservice, alertService, blockUI, $window, ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});

	var urlRegistrarBarrido = $scope.urlRest + "/rest/barrido/registrar";
	var urlActualizarBarrido = $scope.urlRest + "/rest/barrido/actualizar";
	var urlListarBarrido = $scope.urlRest + "/rest/barrido/obtener";

	var urlRegistrarOferta = $scope.urlRest + "/rest/oferta/registrar";
	var urlActualizarOferta = $scope.urlRest + "/rest/oferta/actualizar";
	var urlListarOferta = $scope.urlRest + "/rest/oferta/listarOferta";

	var urlRegistrarOfertaBarrido = $scope.urlRest
			+ "/rest/ofertaBarrido/registrar";
	var urlActualizarOfertaBarrido = $scope.urlRest
			+ "/rest/ofertaBarrido/actualizar";
	var urlListarOfertaBarrido = $scope.urlRest + "/rest/ofertaBarrido/obtener";

	var urlRegistrarEquipo = $scope.urlRest + "/rest/equipo/registrar";
	var urlActualizarEquipo = $scope.urlRest + "/rest/equipo/actualizar";
	var urlListarEquipo = $scope.urlRest + "/rest/equipo/listarEquipo";

	var urlRegistrarProteccion = $scope.urlRest + "/rest/proteccion/registrar";
	var urlActualizarProteccion = $scope.urlRest
			+ "/rest/proteccion/actualizar";
	var urlListarProteccion = $scope.urlRest
			+ "/rest/proteccion/listarProteccion";

	var urlRegistrarPersona = $scope.urlRest + "/rest/persona/registrar";
	var urlRegistrarTrabajadores = $scope.urlRest
			+ "/rest/trabajadores/registrar";
	var urlActualizarTrabajadores = $scope.urlRest
			+ "/rest/trabajadores/actualizar";
	var urlListarTrabajadores = $scope.urlRest
			+ "/rest/trabajadores/listarTrabajadores";

	$scope.barrido = {};

	$scope.oferta = {};
	$scope.lstOferta = [];

	$scope.ofertaBarrido = {};

	$scope.equipo = {};
	$scope.lstEquipo = [];

	$scope.proteccion = {};
	$scope.lstProteccion = [];

	$scope.flag = 0;

	$scope.cantidad = 0;

	$scope.trabajadores = {};

	$scope.lstBarredor = [];
	$scope.lstTrabajadores = [];

	$scope.lstBarredorAux = [];

	$scope.barrido.ciclo_grs_id = $localStorage.GRS_ID;
	$scope.barrido.cod_usuario = '15';

	$localStorage.BARRIDO = '0';

	$scope.guardar = function() {
		if ($scope.flag == 0)
			$scope.grabarBarrido();
		else
			$scope.actualizarBarrido();
	}

	$scope.grabarBarrido = function() {

		var paramJson = {
			Barrido : $scope.barrido
		};

		$http({
			method : 'POST',
			url : urlRegistrarBarrido,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {

				$localStorage.BARRIDO = '1';
				console.log("Cantidad de barredores: " + $scope.lstBarredor.length);
				$scope.ofertaBarrido.barrido_id = data.datosAdicionales.id;
				$scope.oferta.barrido_id = data.datosAdicionales.id;
				$scope.equipo.barrido_id = data.datosAdicionales.id;
				$scope.trabajadores.barrido_id = data.datosAdicionales.id;
				$scope.grabarOfertaBarrido();
				$scope.grabarOferta();
				
				if($scope.lstBarredor.length > 0)
					$scope.grabarTrabajadores();
				// $scope.grabarEquipo();
				// $scope.grabarProteccion();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

		$location.path("/gestion");
	}

	$scope.actualizarBarrido = function() {

		var paramJson = {
			Barrido : $scope.barrido
		};

		$http({
			method : 'POST',
			url : urlActualizarBarrido,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {

				$scope.ofertaBarrido.barrido_id = data.datosAdicionales.id;
				$scope.oferta.barrido_id = data.datosAdicionales.id;
				$scope.equipo.barrido_id = data.datosAdicionales.id;
				$scope.trabajadores.barrido_id = data.datosAdicionales.id;
				$scope.actualizarOfertaBarrido();
				$scope.actualizarOferta();
				$scope.actualizarTrabajadores();
				// $scope.actualizarEquipo();
				// $scope.actualizarProteccion();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

		$location.path("/gestion");
	}

	$scope.listarBarrido = function() {

		var paramJson = {
			Barrido : $scope.barrido
		};

		$http({
			method : 'POST',
			url : urlListarBarrido,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {

			$scope.barrido = data.datosAdicionales.barrido;

			if ($scope.barrido.barrido_id != undefined) {
				$scope.flag = 1;
				$scope.ofertaBarrido.barrido_id = $scope.barrido.barrido_id;
				$scope.oferta.barrido_id = $scope.barrido.barrido_id;
				$scope.equipo.barrido_id = $scope.barrido.barrido_id;
				$scope.trabajadores.barrido_id = $scope.barrido.barrido_id;

			}

			$scope.barrido.ciclo_grs_id = $localStorage.GRS_ID;
			$scope.barrido.cod_usuario = '15';
			$scope.listarOfertaBarrido();
			$scope.actTablaOferta();
			$scope.actTablaBarredor();
			// $scope.actTablaEquipo();
			// $scope.actTablaProteccion();

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.actTablaOferta = function() {

		$scope.tblOferta = new NgTableParams({
			page : 1,
			count : 10,
			sorting : {
				"oferta_id" : "asc"
			}
		}, {
			getData : function($defer, params) {

				var paramJson = {
					Oferta : $scope.oferta
				};

				$http({
					method : 'POST',
					url : urlListarOferta,
					data : JSON.stringify(paramJson),
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function(data) {
					var filtro = {};
					var nData = data.datosAdicionales.LstOferta;
					$scope.lstOferta = data.datosAdicionales.LstOferta;
					blockUI.start();
					params.total(nData.length);
					blockUI.stop();
				}).error(function(status) {
					$scope.status = status;
				});
			}
		});
	}

	$scope.grabarOferta = function() {

		for (var x = 0; x < $scope.lstOferta.length; x++) {
			$scope.lstOferta[x].barrido_id = $scope.oferta.barrido_id;
			$scope.lstOferta[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarOferta,
			data : JSON.stringify($scope.lstOferta),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarOferta = function() {

		for (var x = 0; x < $scope.lstOferta.length; x++) {
			$scope.lstOferta[x].barrido_id = $scope.oferta.barrido_id;
			$scope.lstOferta[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlActualizarOferta,
			data : JSON.stringify($scope.lstOferta),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.grabarOfertaBarrido = function() {

		var paramJson = {
			OfertaBarrido : $scope.ofertaBarrido
		};

		$scope.ofertaBarrido.cod_usuario = '15';

		$http({
			method : 'POST',
			url : urlRegistrarOfertaBarrido,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarOfertaBarrido = function() {

		var paramJson = {
			OfertaBarrido : $scope.ofertaBarrido
		};

		$scope.ofertaBarrido.cod_usuario = '15';

		$http({
			method : 'POST',
			url : urlActualizarOfertaBarrido,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

		$location.path("/gestion");
	}

	$scope.listarOfertaBarrido = function() {

		var paramJson = {
			OfertaBarrido : $scope.ofertaBarrido
		};

		$http({
			method : 'POST',
			url : urlListarOfertaBarrido,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			$scope.ofertaBarrido = data.datosAdicionales.ofertaBarrido;
			$scope.ofertaBarrido.cod_usuario = '15';
		}).error(function(status) {
			alertService.showDanger();
		});
	}

	/*
	 * $scope.actTablaEquipo = function() {
	 * 
	 * $scope.tblEquipo = new NgTableParams({ page : 1, count : 10, sorting : {
	 * "oferta_id" : "asc" } }, { getData : function($defer, params) {
	 * 
	 * var paramJson = { Equipo : $scope.equipo };
	 * 
	 * $http({ method : 'POST', url : urlListarEquipo, data :
	 * JSON.stringify(paramJson), headers : { 'Content-Type' :
	 * 'application/json' } }).success(function(data) { var filtro = {}; var
	 * nData = data.datosAdicionales.LstEquipo; $scope.lstEquipo =
	 * data.datosAdicionales.LstEquipo; blockUI.start();
	 * params.total(nData.length); blockUI.stop(); }).error(function(status) {
	 * $scope.status = status; }); } }); }
	 * 
	 * 
	 * $scope.grabarEquipo = function() {
	 * 
	 * for (var x = 0; x < $scope.lstEquipo.length; x++) {
	 * $scope.lstEquipo[x].barrido_id = $scope.equipo.barrido_id;
	 * $scope.lstEquipo[x].cod_usuario = '14'; }
	 * 
	 * $http({ method : 'POST', url : urlRegistrarEquipo, data :
	 * JSON.stringify($scope.lstEquipo), headers : { 'Content-Type' :
	 * 'application/json' } }).success(function(data) { if
	 * (data.datosAdicionales.id > 0) { alertService.showSuccess("Registro
	 * exitoso."); } else { alertService.showDanger("No se realizó el
	 * registro."); }
	 * 
	 * }).error(function(status) { alertService.showDanger(); });
	 *  }
	 * 
	 * $scope.actualizarEquipo = function() {
	 * 
	 * for (var x = 0; x < $scope.lstEquipo.length; x++) {
	 * $scope.lstEquipo[x].barrido_id = $scope.equipo.barrido_id;
	 * $scope.lstEquipo[x].cod_usuario = '14'; }
	 * 
	 * $http({ method : 'POST', url : urlActualizarEquipo, data :
	 * JSON.stringify($scope.lstEquipo), headers : { 'Content-Type' :
	 * 'application/json' } }).success(function(data) { if
	 * (data.datosAdicionales.id > 0) { alertService.showSuccess("Registro
	 * exitoso."); } else { alertService.showDanger("No se realizó el
	 * registro."); } }).error(function(status) { alertService.showDanger(); });
	 *  }
	 * 
	 * 
	 * 
	 * 
	 * $scope.actTablaProteccion = function() {
	 * 
	 * $scope.tblProteccion = new NgTableParams({ page : 1, count : 10, sorting : {
	 * "proteccion_id" : "asc" } }, { getData : function($defer, params) {
	 * 
	 * var paramJson = { Proteccion : $scope.proteccion };
	 * 
	 * $http({ method : 'POST', url : urlListarProteccion, data :
	 * JSON.stringify(paramJson), headers : { 'Content-Type' :
	 * 'application/json' } }).success(function(data) { var filtro = {}; var
	 * nData = data.datosAdicionales.LstProteccion; $scope.lstProteccion =
	 * data.datosAdicionales.LstProteccion; blockUI.start();
	 * params.total(nData.length); blockUI.stop(); }).error(function(status) {
	 * $scope.status = status; }); } }); }
	 * 
	 * 
	 * $scope.grabarProteccion = function() {
	 * 
	 * for (var x = 0; x < $scope.lstProteccion.length; x++) {
	 * $scope.lstProteccion[x].barrido_id = $scope.proteccion.barrido_id;
	 * $scope.lstProteccion[x].cod_usuario = '14'; }
	 * 
	 * $http({ method : 'POST', url : urlRegistrarProteccion, data :
	 * JSON.stringify($scope.lstProteccion), headers : { 'Content-Type' :
	 * 'application/json' } }).success(function(data) { if
	 * (data.datosAdicionales.id > 0) { alertService.showSuccess("Registro
	 * exitoso."); } else { alertService.showDanger("No se realizó el
	 * registro."); }
	 * 
	 * }).error(function(status) { alertService.showDanger(); });
	 *  }
	 * 
	 * $scope.actualizarProteccion = function() {
	 * 
	 * for (var x = 0; x < $scope.lstProteccion.length; x++) {
	 * $scope.lstProteccion[x].barrido_id = $scope.proteccion.barrido_id;
	 * $scope.lstProteccion[x].cod_usuario = '14'; }
	 * 
	 * $http({ method : 'POST', url : urlActualizarProteccion, data :
	 * JSON.stringify($scope.lstProteccion), headers : { 'Content-Type' :
	 * 'application/json' } }).success(function(data) { if
	 * (data.datosAdicionales.id > 0) { alertService.showSuccess("Registro
	 * exitoso."); } else { alertService.showDanger("No se realizó el
	 * registro."); } }).error(function(status) { alertService.showDanger(); });
	 *  }
	 */

	$scope.getTotal25 = function() {
		var total = 0;

		if ($scope.barrido.cr_25 != null && $scope.barrido.cpr_25 != null) {
			total = $scope.barrido.cr_25 * $scope.barrido.cpr_25;
			$scope.barrido.total_25 = total;
		}

		return total;
	}

	$scope.getTotal2565 = function() {
		var total = 0;

		if ($scope.barrido.cr_25_65 != null && $scope.barrido.cpr_25_65 != null) {

			total = $scope.barrido.cr_25_65 * $scope.barrido.cpr_25_65;
			$scope.barrido.total_25_65 = total;
		}

		return total;
	}

	$scope.getTotal65 = function() {
		var total = 0;

		if ($scope.barrido.cr_65 != null && $scope.barrido.cpr_65 != null) {
			total = $scope.barrido.cr_65 * $scope.barrido.cpr_65;
			$scope.barrido.total_65 = total;
		}

		return total;
	}

	$scope.getTotal = function() {
		var total = 0;

		total = $scope.getTotal25() + $scope.getTotal2565() + $scope.getTotal65();

		$scope.barrido.total_recolectado = total;
		$localStorage.totalBarrido = total;

		return total;
	}

	$scope.agregar = function() {

		$scope.lstBarredor.push({
			'dni' : $scope.trabajadores.dni,
			'nom_apellido' : $scope.trabajadores.nom_apellido,
			'sexo' : $scope.trabajadores.sexo,
			'rango' : $scope.trabajadores.rango,
			'cargo' : $scope.trabajadores.cargo,
			'laboral' : $scope.trabajadores.laboral,
			'horario' : $scope.trabajadores.horario,
			'dias_trabajados' : $scope.trabajadores.dias_trabajados
		});

		$scope.trabajadores.dni = null;
		$scope.trabajadores.nom_apellido = null;
		$scope.trabajadores.sexo = null;
		$scope.trabajadores.rango = null;
		$scope.trabajadores.cargo = null;
		$scope.trabajadores.laboral = null;
		$scope.trabajadores.horario = null;
		$scope.trabajadores.dias_trabajados = null;

		if ($scope.flag > 0) {

			$scope.lstBarredorAux.push({
				'dni' : $scope.trabajadores.dni,
				'nom_apellido' : $scope.trabajadores.nom_apellido,
				'sexo' : $scope.trabajadores.sexo,
				'rango' : $scope.trabajadores.rango,
				'cargo' : $scope.trabajadores.cargo,
				'laboral' : $scope.trabajadores.laboral,
				'horario' : $scope.trabajadores.horario,
				'dias_trabajados' : $scope.trabajadores.dias_trabajados
			});
		}
	}

	$scope.borrar = function(index) {
		$scope.lstBarredor.splice(index, 1);
	};

	app.directive('editableTd', [ function() {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				element.css("cursor", "pointer");
				element.attr('contenteditable', 'true'); // Referencia:
															// Inciso 1
				element.bind('blur keyup change', function() { // Referencia:
																// Inciso 2
					scope.lstBarredor[attrs.row][attrs.field] = element.text();
				});
				element.bind('click', function() { // Referencia: Inciso 3
					document.execCommand('selectAll', false, null)
				});
			}
		};
	} ]);

	$scope.grabarPersona = function() {

		for (var x = 0; x < $scope.lstBarredor.length; x++) {
			$scope.lstBarredor[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarPersona,
			data : JSON.stringify($scope.lstBarredor),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$scope.grabarTrabajadores();
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.grabarTrabajadores = function() {

		for (var x = 0; x < $scope.lstBarredor.length; x++) {
			$scope.lstBarredor[x].barrido_id = $scope.trabajadores.barrido_id;
			$scope.lstBarredor[x].cod_usuario = '14';
		}

		$http({
			method : 'POST',
			url : urlRegistrarTrabajadores,
			data : JSON.stringify($scope.lstBarredor),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.actualizarTrabajadores = function() {

		var i = $scope.cantidad;
		var c = 0;

		for (var x = i; x < $scope.lstBarredor.length; x++) {
			$scope.lstBarredor[x].barrido_id = $scope.trabajadores.barrido_id;
			$scope.lstBarredor[x].cod_usuario = '14';

			$scope.lstBarredorAux[c].barrido_id = $scope.lstBarredor[x].barrido_id;

			$scope.lstBarredorAux[c].dni = $scope.lstBarredor[x].dni
			$scope.lstBarredorAux[c].nom_apellido = $scope.lstBarredor[x].nom_apellido;
			$scope.lstBarredorAux[c].sexo = $scope.lstBarredor[x].sexo;
			$scope.lstBarredorAux[c].rango = $scope.lstBarredor[x].rango;
			$scope.lstBarredorAux[c].cargo = $scope.lstBarredor[x].cargo;
			$scope.lstBarredorAux[c].laboral = $scope.lstBarredor[x].laboral;
			$scope.lstBarredorAux[c].horario = $scope.lstBarredor[x].horario;
			$scope.lstBarredorAux[c].dias_trabajados = $scope.lstBarredor[x].dias_trabajados;

			$scope.lstBarredorAux[c].cod_usuario = $scope.lstBarredor[x].cod_usuario;
			c++;
		}

		$http({
			method : 'POST',
			url : urlActualizarTrabajadores,
			data : JSON.stringify($scope.lstBarredorAux),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});
	}

	$scope.actTablaBarredor = function() {

		$scope.tblBarredor = new NgTableParams(
				{
					page : 1,
					count : 10,
					sorting : {
						"barredor_id" : "asc"
					}
				},
				{
					getData : function($defer, params) {

						var paramJson = {
							Trabajadores : $scope.trabajadores
						};

						$http({
							method : 'POST',
							url : urlListarTrabajadores,
							data : JSON.stringify(paramJson),
							headers : {
								'Content-Type' : 'application/json'
							}
						})
								.success(
										function(data) {
											var filtro = {};
											var nData = data.datosAdicionales.LstTrabajadores;
											$scope.lstBarredor = data.datosAdicionales.LstTrabajadores;

											for (var x = 0; x < $scope.lstBarredor.length; x++) {

												$scope.lstBarredor[x].dni = $scope.lstBarredor[x].persona.dni;
												$scope.lstBarredor[x].nom_apellido = $scope.lstBarredor[x].persona.nom_apellidos;
												$scope.lstBarredor[x].sexo = $scope.lstBarredor[x].persona.sexo;
												$scope.lstBarredor[x].rango = $scope.lstBarredor[x].persona.rango_edad;
												$scope.lstBarredor[x].cargo = $scope.lstBarredor[x].cargo;
												$scope.lstBarredor[x].laboral = $scope.lstBarredor[x].condicion_laboral;
												$scope.lstBarredor[x].horario = $scope.lstBarredor[x].horario_trabajo;
												$scope.lstBarredor[x].dias_trabajados = $scope.lstBarredor[x].dias_trabajados;
											}

											$scope.cantidad = $scope.lstBarredor.length;
											console.log("Cantidad: "
													+ $scope.cantidad);

											blockUI.start();
											params.total(nData.length);
											blockUI.stop();
										}).error(function(status) {
									$scope.status = status;
								});
					}
				});
	}

	/*
	 * $scope.lstBarredor = [ { 'dni':null, 'sexo':'', 'edad':'' }, ];
	 * 
	 * $scope.lstBarredor2 = [ { 'cargo':'', 'laboral':'', 'horario':'',
	 * 'dias':null }, ];
	 * 
	 * $scope.agregar = function(){ $scope.lstBarredor.push({ 'dni':null,
	 * 'sexo':'', 'edad':'' }); $scope.lstBarredor2.push({ 'cargo':'',
	 * 'laboral':'', 'horario':'', 'dias':null }); };
	 */

	$scope.volver = function() {
		$location.path("/gestion");
	}

	$scope.listarBarrido();

}