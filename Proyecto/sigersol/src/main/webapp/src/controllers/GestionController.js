function GestionController($scope, $http, $routeParams, $location, $rootScope,
		$filter, NgTableParams, $controller, CONFIG, OPCIONES, $localStorage,
		APIservice, alertService, blockUI, $window, ngTableParams) {

	$controller('funcGenerales', {
		$scope : $scope
	});
	
	$scope.validarSesion();

	$scope.departamento = $localStorage.Departamento;
	$scope.provincia = $localStorage.Provincia
	$scope.distrito = $localStorage.Distrito

	$scope.gestion = {};
	$scope.flag = {}

	$scope.date = new Date();
	console.log("anio  >> " + $scope.date.getFullYear());
	$scope.currentYear = $scope.date.getFullYear();
	$scope.anioReporte = $scope.currentYear - 1;
	console.log("$localStorage.MunicipalidadId;  >> "
			+ $localStorage.MunicipalidadId);

	$scope.gestion.municipalidad_id = $localStorage.MunicipalidadId;
	$scope.gestion.cod_usuario = $localStorage.SesionId;

	var urlValidarGestion = $scope.urlRest + "/rest/gestion/validar";
	var urlRegistrarGestion = $scope.urlRest + "/rest/gestion/registrar";
	var urlActualizarGestion = $scope.urlRest + "/rest/gestion/actualizar";

	$scope.flagGeneracion = 0;
	$scope.flagBarrido = 0;
	$scope.flagAlmacenamiento = 0;
	$scope.flagRecoleccion = 0;
	$scope.flagValorizacion = 0;
	$scope.flagTransferencia = 0;
	$scope.flagDisposicionFinal = 0;

	$scope.validar = function() {

		var paramJson = {
			Gestion : $scope.gestion
		};

		$http({
			method : 'POST',
			url : urlValidarGestion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		})
				.success(
						function(data) {
							$scope.gestion = data.datosAdicionales.cicloGRS;
							console
									.log("gestion>>"
											+ JSON
													.stringify($scope.gestion.ciclo_grs_id))

							if ($scope.gestion.ciclo_grs_id == undefined) {
								$scope.gestion.ciclo_grs_id = -1;

							} else {
								$scope.flagGeneracion = $scope.gestion.generacion;
								$scope.flagBarrido = $scope.gestion.barrido;
								$scope.flagAlmacenamiento = $scope.gestion.almacenamiento;
								$scope.flagRecoleccion = $scope.gestion.recoleccion;
								$scope.flagValorizacion = $scope.gestion.valorizacion;
								$scope.flagTransferencia = $scope.gestion.transferencia;
								$scope.flagDisposicionFinal = $scope.gestion.disposicion_final;
							}

							$localStorage.GRS_ID = $scope.gestion.ciclo_grs_id;
							$localStorage.FLAG_RECOLECCION = $scope.gestion.flag_recoleccion;
							$scope.gestion.municipalidad_id = $localStorage.MunicipalidadId;
							$scope.gestion.cod_usuario = $localStorage.SesionId;
						}).error(function(status) {
					alertService.showDanger();
				});
	}

	$scope.grabarGestion = function() {
		$('#modalConfirmacionCiclo').modal('hide');
		var paramJson = {
			Gestion : $scope.gestion
		};

		console.log("Gestion : " + JSON.stringify(paramJson));

		$http({
			method : 'POST',
			url : urlRegistrarGestion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
				$localStorage.GRS_ID = data.datosAdicionales.id;
				if ($scope.flag == '0')
					$location.path("/generacion");
				if ($scope.flag == '1')
					$location.path("/almacenamiento");
				if ($scope.flag == '2')
					$location.path("/barrido");
				if ($scope.flag == '3')
					$location.path("/recoleccionSelectiva");
				if ($scope.flag == '4')
					$location.path("/transferencia");
				if ($scope.flag == '5')
					$location.path("/valorizacion");
				if ($scope.flag == '6')
					$location.path("/disposicionFinal");
				if ($scope.flag == '7')
					$location.path("/datosGenerales");
				if ($scope.flag == '8')
					$location.path("/ambiental");
				if ($scope.flag == '9')
					$location.path("/finanzas");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}

		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.actualizarGestion = function() {

		var paramJson = {
			Gestion : $scope.gestion
		};

		$http({
			method : 'POST',
			url : urlActualizarGestion,
			data : JSON.stringify(paramJson),
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data) {
			if (data.datosAdicionales.id > 0) {
				alertService.showSuccess("Registro exitoso.");
			} else {
				alertService.showDanger("No se realizó el registro.");
			}
		}).error(function(status) {
			alertService.showDanger();
		});

	}

	$scope.validar();

	$scope.almacenamiento = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '1';
			// $scope.grabarGestion();
		} else {
			$location.path("/almacenamiento");
		}
	}

	$scope.generacion = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '0';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/generacion");
		}
	}

	$scope.barrido = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '2';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/barrido");
		}
	}

	$scope.recoleccion = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '3';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/recoleccion");
		}
	}

	$scope.transferencia = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '4';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/transferencia");
		}
	}

	$scope.valorizacion = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '5';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/valorizacion");
		}
	}

	$scope.disposicion = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '6';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/disposicionFinal");
		}
	}

	$scope.datos = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '7';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/datosGenerales");
		}
	}

	$scope.ambiental = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '8';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/ambiental");
		}
	}

	$scope.finanzas = function() {
		if ($localStorage.GRS_ID == -1) {
			$scope.flag = '9';
			$('#modalConfirmacionCiclo').modal();
			// $scope.grabarGestion();
		} else {
			$location.path("/finanzas");
		}
	}

	var urlreporte = $scope.urlRest + "/rest/reporte/resumenGral";

	$scope.exportarPDF = function() {

		$scope.descargarArchivo("SGRS_resumenGral.pdf", "SGRS_resumenGral.pdf");

		var paramJson = {
			Filtro : {}
		};

		// console.log("exportarPDF" + JSON.stringify(paramJson));

		// $http({
		// method : 'POST',
		// url : urlreporte,
		// data : JSON.stringify(paramJson),
		// headers : {
		// 'Content-Type' : 'application/json'
		// },
		// }).success(function(data) {
		// if (String(data).indexOf(".pdf") != -1) {
		// console.log("exportarPDF: " + data);
		// // window.open($scope.urlBase + data);
		// $scope.descargarArchivo("SGRS_resumenGral.pdf",
		// "SGRS_resumenGral.pdf");
		//
		// } else {
		//				
		// }
		// }).error(function(status) {
		//
		// });

	}

}