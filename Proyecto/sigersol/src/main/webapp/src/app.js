'use strict';
angular
		.module(
				'app',
				[ 'ngRoute', 'ngTable', 'ngResource', 'ngStorage', 'blockUI',
						'app.services', 'app.directives', 'app.util.alerts',
						'uiGmapgoogle-maps', 'angularTreeview', 'autocomplete',
						'ua.pivottable', 'wt.responsive', 'ui.tree' ])

		.constant('CONFIG', {
			USERNAME : undefined,
			ROL_CURRENT_USER : undefined,
			PRUEBA_CONFIG : "00"
		})
		// definicion de Opciones de Menu
		.constant('OPCIONES', {
			OPC_NOLOGIN : {
				ID : 0,
				PATH : "/nologin",
				NAME : "No login"
			},
			OPC_PANEL : {
				ID : 1,
				PATH : "/panel",
				NAME : "Panel de Inicio"
			},
			OPC_CICLO_GESTION_RS : {
				ID : 1,
				PATH : "/gestion",
				NAME : "Ciclo de la gestión de residuos sólidos"
			},
		})
		.config(
				[
						'$routeProvider',
						'$locationProvider',
						'$httpProvider',
						function($routes, $locationProvider, $httpProvider,
								CONFIG, $rootScope) {

							$routes
									.when('/login', {
										templateUrl : 'src/views/login.html',
										controller : LoginController
									})
									.when('/logout', {
										templateUrl : 'src/views/nologin.html',
										controller : LogoutController
									})
									.when(
											'/panel',
											{
												templateUrl : 'src/views/paginaPrincipal.html',
												controller : LoginController
											})

									.when(
											'/datosGenerales',
											{
												templateUrl : 'src/views/datosGenerales.html',
												controller : DatosGeneralesController
											})
									.when('/login', {
										templateUrl : 'src/views/login.html',
										controller : LoginController
									})
									.when(
											'/registro',
											{
												templateUrl : 'src/views/registro.html',
												controller : LoginController
											})
									.when(
											'/solicitudUsuario',
											{
												templateUrl : 'src/views/solicitudUsuario.html',
												controller : SolicitudController
											})
									.when(
											'/bandejaUsuario',
											{
												templateUrl : 'src/views/bandejaUsuario.html'
											})
									.when(
											'/parametrosGenerales',
											{
												templateUrl : 'src/views/parametrosGenerales.html'
											})
									.when(
											'/aprobacionUsuario',
											{
												templateUrl : 'src/views/aprobacionUsuario.html',
												controller : SolicitudController
											})
									.when(
											'/finanzas',
											{
												templateUrl : 'src/views/administracionFinanzas.html',
												controller : AdmFinanzasController
											})
									.when(
											'/ambiental',
											{
												templateUrl : 'src/views/educacionAmbiental.html',
												controller : AmbientalController

											})
									.when(
											'/preguntas',
											{
												templateUrl : 'src/views/preguntasFrecuentes.html',
												controller : GestionController
											})
									.when(
											'/gestion',
											{
												templateUrl : 'src/views/gestionResiduos.html',
												controller : GestionController
											})
									.when(
											'/generacion',
											{
												templateUrl : 'src/views/generacion.html',
												controller : GeneracionController
											})
									.when(
											'/almacenamiento',
											{
												templateUrl : 'src/views/almacenamiento.html',
												controller : AlmacenamientoController
											})
									.when('/barrido', {
										templateUrl : 'src/views/barrido.html',
										controller : BarridoController
									})
									.when(
											'/recoleccion',
											{
												templateUrl : 'src/views/recoleccionSelectiva.html',
												controller : RecoleccionController
											})
									.when(
											'/transferencia',
											{
												templateUrl : 'src/views/transferencia.html',
												controller : TransferenciaController
											})
									.when(
											'/valorizacion',
											{
												templateUrl : 'src/views/valorizacion.html',
												controller : ValorizacionController
											})
									.when(
											'/disposicionFinalAdm',
											{
												templateUrl : 'src/views/disposicionFinalAdm.html',
												controller : DisposicionFinalAdmController
											})
									.when(
											'/disposicionFinal',
											{
												templateUrl : 'src/views/disposicionFinal.html',
												controller : DisposicionController
											})

									.when(
											'/cambioClimatico/generacion',
											{
												templateUrl : 'src/views/cambioclimatico/generacion/generacionSDF.html',
												controller : GeneraEnergiaController
											}).otherwise({
										redirectTo : '/panel'
									});

						} ])

		.run(function($rootScope) {

		})
		// funciones generales
		.controller(
				'funcGenerales',
				function($log, $location, $scope, OPCIONES, CONFIG, $rootScope,
						$localStorage, $http, alertService) {

					$scope.urlBaseLocation = "http://" + $location.host() + ":"
							+ $location.port();
					$scope.urlBase = String($location.absUrl()).replace('/#',
							'').replace(String($location.path()), "");
					$scope.urlName = $scope.urlBase.replace(
							$scope.urlBaseLocation, '');
					$scope.urlRest = $scope.urlBaseLocation + "/srv-sigersol";
					$scope.urlReporte = $scope.urlBaseLocation
							+ "/srv-sigersol";

					$scope.logout = function() {
						// Limpiando variables locales
						$localStorage.SesionId = "";
						$localStorage.Departamento = "";
						$localStorage.Provincia = "";
						$localStorage.Distrito = "";
						$localStorage.UbigeoId = "";
						$localStorage.MunicipalidadId = -1;

						console.log("Cerrando session");

						$location.path("/login");
						alertService.showSuccess('Sesión terminada.');
					}

					$scope.validarSesion = function() {
						console.log("Validando session ");
						$scope.leerSession();
						/*
						 * var sesionId = $localStorage.SesionId;
						 * 
						 * if ((String(sesionId).localeCompare("") == 0) ||
						 * (String(sesionId).localeCompare("undefined") == 0) ||
						 * (String(sesionId).localeCompare("null") == 0)) {
						 * alertService.showInfo('Sesión terminada');
						 * 
						 * //$location.path("/login"); }
						 */
					};

					var urlSession = $scope.urlRest
							+ "/rest/sesion/revisarSesion";
					$scope.leerSession = function() {

						$http(
								{
									method : 'POST',
									url : urlSession,
									data : $.param({}),
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									},
								}).success(function(data) {
							console.log("data >>" + JSON.stringify(data));
							if (data) {
								console.log("Tiene sesion  abierta");
							} else {

								console.log("No hay datos del usuario");
								$location.path("/login");
							}
						}).error(function(status) {
							$scope.status = status.mensaje;
						});
					};

					var urlDescarga = $scope.urlRest
							+ "/rest/descarga/getFile?";

					$scope.descargarArchivo = function(fileNameRef, fileName) {
						console.log("archivo Ref:" + fileNameRef
								+ "- fileName:" + fileName);
						window.open(urlDescarga + "fileNameRef=" + fileNameRef
								+ "&fileName=" + fileName, "_blank");
					}

				})
