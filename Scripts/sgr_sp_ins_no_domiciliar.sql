create or replace procedure SIGERSOLBL.SGR_SP_INS_NO_DOMICILIAR(
    IN_GENERACION_ID NUMBER, 
    IN_TIPO_GENERACION_ID NUMBER,
    IN_CANTIDAD_T_X_DIA NUMBER,
    IN_COD_USUARIO NUMBER,
    OUT_CODIGO OUT NUMBER) 
AS 
BEGIN
      INSERT INTO DEVSIGERSOL.SGR_GENERACION_NO_DOMICILIAR
        ( 
          GENERACION_ID, 
          TIPO_GENERACION_ID,
          CANTIDAD_T_X_DIA,
          CODIGO_ESTADO,
          COD_USUARIO_CREACION,
          FECHA_CREACION     
        )
        VALUES
        (
          IN_GENERACION_ID, 
          IN_TIPO_GENERACION_ID,
          IN_CANTIDAD_T_X_DIA,
            1,
          IN_COD_USUARIO,
          SYSDATE     
        )
     RETURNING GENERACION_NO_DOMICILIAR_ID INTO OUT_CODIGO;
      EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo insertarse el registro en la tabla SGR_GENERACION_NO_DOMICILIAR');	   
         OUT_CODIGO :=-1;
        RETURN; 
END;