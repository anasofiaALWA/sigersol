CREATE SEQUENCE DEVSIGERSOL.VALORIZACION_SEQ
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

DROP TABLE DEVSIGERSOL.SGR_VALORIZACION;

CREATE TABLE DEVSIGERSOL.SGR_VALORIZACION (
  VALORIZACION_ID NUMBER DEFAULT DEVSIGERSOL.VALORIZACION_SEQ.NEXTVAL,
  CICLO_GRS_ID NUMBER,
  CANT_REC_FORMALIZADOS NUMBER, 
  MONTO_INCENTIVOS NUMBER,
  RESIDUO_INORGANICO_OTRO VARCHAR2(100),
  ENERO_OTRO NUMBER,
  FEBRERO_OTRO NUMBER,
  MARZO_OTRO NUMBER,
  ABRIL_OTRO NUMBER,
  MAYO_OTRO NUMBER,
  JUNIO_OTRO NUMBER,
  JULIO_OTRO NUMBER,
  AGOSTO_OTRO NUMBER,
  SETIEMBRE_OTRO NUMBER,
  OCTUBRE_OTRO NUMBER,
  NOVIEMBRE_OTRO NUMBER,
  DICIEMBRE_OTRO NUMBER,
  TOTAL_OTRO NUMBER,
  RESIDUOS_ORGANICOS NUMBER,
  CODIGO_ESTADO NUMBER,
  COD_USUARIO_CREACION NUMBER,
  FECHA_CREACION DATE,
  COD_USUARIO_MODIFICACION NUMBER,
  FECHA_MODIFICACION DATE,
  PRIMARY KEY (VALORIZACION_ID)
);

GRANT SELECT ON DEVSIGERSOL.VALORIZACION_SEQ TO SIGERSOLBL;  
GRANT INSERT,UPDATE, DELETE, SELECT ON DEVSIGERSOL.SGR_VALORIZACION TO SIGERSOLBL;

ALTER TABLE DEVSIGERSOL.SGR_VALORIZACION
ADD FOREIGN KEY (CICLO_GRS_ID) REFERENCES DEVSIGERSOL.SGR_CICLO_GESTION_RESIDUOS(CICLO_GRS_ID);