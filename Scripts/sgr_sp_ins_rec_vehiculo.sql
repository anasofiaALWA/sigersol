create or replace procedure SIGERSOLBL.SGR_SP_INS_REC_VEHICULO(
    IN_RECOLECCION_ID NUMBER, 
    IN_TIPO_VEHICULO_ID NUMBER,
    IN_CANTIDAD NUMBER,
    IN_NUMERO_LLANTAS NUMBER,
    IN_COSTO_LLANTA NUMBER,
    IN_COD_USUARIO NUMBER,
    OUT_CODIGO OUT NUMBER) 
AS 
BEGIN
      INSERT INTO DEVSIGERSOL.SGR_RECOLECCION_VEHICULO
        ( 
          RECOLECCION_ID, 
          TIPO_VEHICULO_ID,
          CANTIDAD,
          NUMERO_LLANTAS,
          COSTO_LLANTA,
          CODIGO_ESTADO,
          COD_USUARIO_CREACION,
          FECHA_CREACION     
        )
        VALUES
        (
          IN_RECOLECCION_ID, 
          IN_TIPO_VEHICULO_ID,
          IN_CANTIDAD,
          IN_NUMERO_LLANTAS,
          IN_COSTO_LLANTA,
            1,
          IN_COD_USUARIO,
          SYSDATE     
        )
     RETURNING RECOLECCION_VEHICULO_ID INTO OUT_CODIGO;
      EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo insertarse el registro en la tabla SGR_RECOLECCION_VEHICULO');	   
         OUT_CODIGO :=-1;
        RETURN; 
END;