create or replace procedure SIGERSOLBL.SGR_SP_INS_VALORIZACION(
    IN_CICLO_GRS_ID NUMBER, 
    IN_CANT_REC_FORMALIZADOS NUMBER, 
    IN_MONTO_INCENTIVOS NUMBER,
    IN_RESIDUO_INORGANICO_OTRO VARCHAR2,
    IN_ENERO_OTRO NUMBER,
    IN_FEBRERO_OTRO NUMBER,
    IN_MARZO_OTRO NUMBER,
    IN_ABRIL_OTRO NUMBER,
    IN_MAYO_OTRO NUMBER,
    IN_JUNIO_OTRO NUMBER,
    IN_JULIO_OTRO NUMBER,
    IN_AGOSTO_OTRO NUMBER,
    IN_SETIEMBRE_OTRO NUMBER,
    IN_OCTUBRE_OTRO NUMBER,
    IN_NOVIEMBRE_OTRO NUMBER,
    IN_DICIEMBRE_OTRO NUMBER,
    IN_TOTAL_OTRO NUMBER,
    IN_RESIDUOS_ORGANICOS NUMBER,
    IN_COD_USUARIO NUMBER,
    OUT_CODIGO OUT NUMBER) 
AS
BEGIN
    INSERT INTO DEVSIGERSOL.SGR_VALORIZACION
        ( 
          CICLO_GRS_ID,
          CANT_REC_FORMALIZADOS, 
          MONTO_INCENTIVOS,
          RESIDUO_INORGANICO_OTRO,
          ENERO_OTRO,
          FEBRERO_OTRO,
          MARZO_OTRO,
          ABRIL_OTRO,
          MAYO_OTRO,
          JUNIO_OTRO,
          JULIO_OTRO,
          AGOSTO_OTRO,
          SETIEMBRE_OTRO,
          OCTUBRE_OTRO,
          NOVIEMBRE_OTRO,
          DICIEMBRE_OTRO,
          TOTAL_OTRO,
          RESIDUOS_ORGANICOS,
          CODIGO_ESTADO,
          COD_USUARIO_CREACION,
          FECHA_CREACION     
        )
        VALUES
        (
          IN_CICLO_GRS_ID,
          IN_CANT_REC_FORMALIZADOS, 
          IN_MONTO_INCENTIVOS,
          IN_RESIDUO_INORGANICO_OTRO,
          IN_ENERO_OTRO,
          IN_FEBRERO_OTRO,
          IN_MARZO_OTRO,
          IN_ABRIL_OTRO,
          IN_MAYO_OTRO,
          IN_JUNIO_OTRO,
          IN_JULIO_OTRO,
          IN_AGOSTO_OTRO,
          IN_SETIEMBRE_OTRO,
          IN_OCTUBRE_OTRO,
          IN_NOVIEMBRE_OTRO,
          IN_DICIEMBRE_OTRO,
          IN_TOTAL_OTRO,
          IN_RESIDUOS_ORGANICOS,
            1,
          IN_COD_USUARIO,
          SYSDATE     
        )
        RETURNING VALORIZACION_ID INTO OUT_CODIGO;
            EXCEPTION   
            WHEN OTHERS THEN
            dbms_output.put_line ('No pudo insertarse el registro en la tabla SGR_VALORIZACION');	   
            OUT_CODIGO :=-1;
            RETURN;
END;