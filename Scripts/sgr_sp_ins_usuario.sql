create or replace procedure SIGERSOLBL.SGR_SP_INS_USUARIO(
    IN_USUARIO_LOGIN VARCHAR2,
    IN_CLAVE_LOGIN VARCHAR2,
    IN_LLAVE_ENCRYPT VARCHAR2,
    IN_EMAIL VARCHAR2,
    IN_COD_USUARIO NUMBER,
    OUT_CODIGO OUT NUMBER
) 
AS
BEGIN
  INSERT INTO DEVSIGERSOL.SGR_USUARIO
    ( 
      USUARIO_LOGIN,
      CLAVE_LOGIN,
      LLAVE_ENCRYPT,
      EMAIL,
      CODIGO_ESTADO,
      COD_USUARIO_CREACION,
      FECHA_CREACION
    )
    VALUES
    (
      IN_USUARIO_LOGIN,
      IN_CLAVE_LOGIN,
      IN_LLAVE_ENCRYPT,
      IN_EMAIL,
      1,
      IN_COD_USUARIO,
      SYSDATE   
    )
    RETURNING USUARIO_ID INTO OUT_CODIGO;
    EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo insertarse el registro en la tabla SGR_USUARIO');	   
        OUT_CODIGO :=-1;
        RETURN;
END;