create or replace procedure SIGERSOLBL.SGR_SP_UPD_NO_DOMICILIAR(
    IN_GENERACION_ID NUMBER, 
    IN_TIPO_GENERACION_ID NUMBER,
    IN_CANTIDAD_T_X_DIA NUMBER,
    IN_COD_USUARIO_MODIFICACION NUMBER,
    OUT_CODIGO OUT NUMBER
) 
AS
BEGIN
   UPDATE DEVSIGERSOL.SGR_GENERACION_NO_DOMICILIAR
   SET 
      CANTIDAD_T_X_DIA = IN_CANTIDAD_T_X_DIA,
      COD_USUARIO_MODIFICACION = IN_COD_USUARIO_MODIFICACION,
      FECHA_MODIFICACION = SYSDATE
   WHERE GENERACION_ID = IN_GENERACION_ID AND TIPO_GENERACION_ID = IN_TIPO_GENERACION_ID
   RETURNING GENERACION_NO_DOMICILIAR_ID INTO OUT_CODIGO;
    EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo actualizarse el registro en la tabla SGR_GENERACION_NO_DOMICILIAR');	   
        OUT_CODIGO :=-1;
        RETURN;
END;