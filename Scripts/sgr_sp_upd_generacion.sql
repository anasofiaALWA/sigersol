create or replace procedure SIGERSOLBL.SGR_SP_UPD_GENERACION(
    IN_GENERACION_ID  NUMBER,
    IN_CICLO_GRS_ID NUMBER, 
    IN_POBLACION_URBANA NUMBER,
    IN_POBLACION_RURAL NUMBER,
    IN_DENSIDAD_T_X_M3 NUMBER,
    IN_CANTIDAD_VIVIENDAS NUMBER,
    IN_GPC_DIARIO_KG_X_HAB NUMBER,
    IN_DESCRIPCION_OTRO VARCHAR2,
    IN_PORCENTAJE  NUMBER,
    IN_TOTAL_NO_DOMICILIAR NUMBER,   
    IN_TOTAL_COMPOSICION NUMBER,
    IN_COD_USUARIO_MODIFICACION NUMBER,
    OUT_CODIGO OUT NUMBER
) 
AS
BEGIN
   UPDATE DEVSIGERSOL.SGR_GENERACION
   SET 
      CICLO_GRS_ID = IN_CICLO_GRS_ID, 
      POBLACION_URBANA = IN_POBLACION_URBANA,
      POBLACION_RURAL = IN_POBLACION_RURAL,
      DENSIDAD_T_X_M3 = IN_DENSIDAD_T_X_M3,
      GPC_DIARIO_KG_X_HAB = IN_GPC_DIARIO_KG_X_HAB,
      DESCRIPCION_OTRO = IN_DESCRIPCION_OTRO,
      PORCENTAJE = IN_PORCENTAJE,
      TOTAL_NO_DOMICILIAR = IN_TOTAL_NO_DOMICILIAR,
      TOTAL_COMPOSICION = IN_TOTAL_COMPOSICION,
      COD_USUARIO_MODIFICACION = IN_COD_USUARIO_MODIFICACION,
      FECHA_MODIFICACION = SYSDATE
   WHERE GENERACION_ID = IN_GENERACION_ID
   RETURNING GENERACION_ID INTO OUT_CODIGO;
    EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo actualizarse el registro en la tabla SGR_GENERACION');	   
        OUT_CODIGO :=-1;
        RETURN;
END;