create or replace procedure SIGERSOLBL.SGR_SP_INS_COMPOSICION(
    IN_GENERACION_ID NUMBER, 
    IN_TIPO_RESIDUO_ID NUMBER,
    IN_PORCENTAJE NUMBER,
    IN_COD_USUARIO NUMBER,
    OUT_CODIGO OUT NUMBER) 
AS 

BEGIN
    INSERT INTO DEVSIGERSOL.SGR_COMPOSICION
        ( 
            GENERACION_ID, 
            TIPO_RESIDUO_ID,
            PORCENTAJE,
            CODIGO_ESTADO,
            COD_USUARIO_CREACION,
            FECHA_CREACION     
        )
        VALUES
        (
          IN_GENERACION_ID, 
          IN_TIPO_RESIDUO_ID,
          IN_PORCENTAJE,
            1,
          IN_COD_USUARIO,
          SYSDATE     
        )
  RETURNING COMPOSICION_ID INTO OUT_CODIGO;
   EXCEPTION   
      WHEN OTHERS THEN
        dbms_output.put_line ('No pudo insertarse el registro en la tabla SGR_COMPOSICION');	   
        OUT_CODIGO :=-1;
       RETURN; 
END;