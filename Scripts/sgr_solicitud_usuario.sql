CREATE SEQUENCE DEVSIGERSOL.SOLICITUD_SEQ
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

CREATE TABLE DEVSIGERSOL.SGR_SOLICITUD_USUARIO (
  SOLICITUD_ID NUMBER DEFAULT DEVSIGERSOL.SOLICITUD_SEQ.NEXTVAL,
  COD_UBIGEO_DEP VARCHAR2(5), 
  COD_UBIGEO_PROV VARCHAR2(5),
  COD_UBIGEO_DIST VARCHAR2(5),
  RUC_MUNICIPALIDAD VARCHAR2(11),
  NOMBRE_ALCALDE VARCHAR2(50),
  DIRECCION VARCHAR2(50),
  TELEFONO VARCHAR2(20),
  FAX   VARCHAR2(20),
  EMAIL VARCHAR2(30),
  CODIGO_ESTADO NUMBER,
  COD_USUARIO_CREACION NUMBER,
  FECHA_CREACION DATE,
  COD_USUARIO_MODIFICACION NUMBER,
  FECHA_MODIFICACION DATE,
  PRIMARY KEY (SOLICITUD_ID)
);

GRANT SELECT ON DEVSIGERSOL.TABLA_SEQ TO ESQUEMA_PROCEDURES;  
GRANT INSERT,UPDATE,SELECT ON DEVSIGERSOL.SGR_SOLICITUD_USUARIO TO ESQUEMA_PROCEDURES;