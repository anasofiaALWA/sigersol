create or replace procedure  SIGERSOLBL.SGR_SP_INS_GENERACION(
    IN_CICLO_GRS_ID NUMBER, 
    IN_POBLACION_URBANA NUMBER,
    IN_POBLACION_RURAL NUMBER,
    IN_DENSIDAD_T_X_M3 NUMBER,
    IN_CANTIDAD_VIVIENDAS NUMBER,
    IN_GPC_DIARIO_KG_X_HAB NUMBER,
    IN_DESCRIPCION_OTRO VARCHAR2,
    IN_PORCENTAJE  NUMBER,
    IN_TOTAL_NO_DOMICILIAR NUMBER,   
    IN_TOTAL_COMPOSICION NUMBER,
    IN_COD_USUARIO NUMBER,
    OUT_CODIGO OUT NUMBER) 
AS
BEGIN
BEGIN
    INSERT INTO DEVSIGERSOL.SGR_GENERACION
        ( 
          CICLO_GRS_ID, 
          POBLACION_URBANA,
          POBLACION_RURAL,
          DENSIDAD_T_X_M3,
          CANTIDAD_VIVIENDAS,
          GPC_DIARIO_KG_X_HAB,
          DESCRIPCION_OTRO,
          PORCENTAJE,
          TOTAL_NO_DOMICILIAR,
          TOTAL_COMPOSICION,
          CODIGO_ESTADO,
          COD_USUARIO_CREACION,
          FECHA_CREACION     
        )
        VALUES
        (
          IN_CICLO_GRS_ID, 
          IN_POBLACION_URBANA,
          IN_POBLACION_RURAL,
          IN_DENSIDAD_T_X_M3,
          IN_CANTIDAD_VIVIENDAS,
          IN_GPC_DIARIO_KG_X_HAB,
          IN_DESCRIPCION_OTRO,
          IN_PORCENTAJE,
          IN_TOTAL_NO_DOMICILIAR,
          IN_TOTAL_COMPOSICION,
            1,
          IN_COD_USUARIO,
          SYSDATE     
        )
        RETURNING GENERACION_ID INTO OUT_CODIGO;
            EXCEPTION   
            WHEN OTHERS THEN
            dbms_output.put_line ('No pudo insertarse el registro en la tabla SGR_GENERACION');	   
            OUT_CODIGO :=-1;
            RETURN;
END;
BEGIN
        UPDATE DEVSIGERSOL.SGR_CICLO_GESTION_RESIDUOS
           SET 
                GENERACION = 1       
           WHERE  CICLO_GRS_ID = IN_CICLO_GRS_ID  ;
END;
END;