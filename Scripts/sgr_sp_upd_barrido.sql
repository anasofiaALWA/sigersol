create or replace procedure  SIGERSOLBL.SGR_SP_UPD_BARRIDO(
    IN_BARRIDO_ID NUMBER,
    IN_CICLO_GRS_ID NUMBER, 
    IN_ADM_SERV_ID NUMBER,
    IN_COSTO_ANUAL NUMBER,
    IN_DEMANDA NUMBER,
    IN_OFERTA NUMBER,
    IN_HOMBRE_25 NUMBER,
    IN_MUJER_25 NUMBER,
    IN_HOMBRE_25_65 NUMBER,
    IN_MUJER_25_65 NUMBER,
    IN_HOMBRE_65 NUMBER,
    IN_MUJER_65 NUMBER,
    IN_COD_USUARIO_MODIFICACION NUMBER,
    OUT_CODIGO OUT NUMBER
) 
AS
BEGIN
   UPDATE DEVSIGERSOL.SGR_BARRIDO
   SET 
      CICLO_GRS_ID = IN_CICLO_GRS_ID, 
      ADM_SERV_ID = IN_ADM_SERV_ID,
      COSTO_ANUAL = IN_COSTO_ANUAL,
      DEMANDA = IN_DEMANDA,
      OFERTA = IN_OFERTA,
      HOMBRE_25 = IN_HOMBRE_25,
      MUJER_25 = IN_MUJER_25,
      HOMBRE_25_65 = IN_HOMBRE_25_65,
      MUJER_25_65 = IN_MUJER_25_65,
      HOMBRE_65 = IN_HOMBRE_65,
      MUJER_65 = IN_MUJER_65,
      COD_USUARIO_MODIFICACION = IN_COD_USUARIO_MODIFICACION,
      FECHA_MODIFICACION = SYSDATE
   WHERE BARRIDO_ID = IN_BARRIDO_ID
   RETURNING BARRIDO_ID INTO OUT_CODIGO;
    EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo actualizarse el registro en la tabla SGR_BARRIDO');	   
        OUT_CODIGO :=-1;
        RETURN;
END;