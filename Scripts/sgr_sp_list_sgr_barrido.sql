create or replace procedure SIGERSOLBL.SGR_SP_LIST_SGR_BARRIDO(
    IN_CICLO_GRS_ID NUMBER,
    PS_CURSOR OUT sys_refcursor
   )
AS
BEGIN
    OPEN PS_CURSOR FOR SELECT BARRIDO_ID,
    CICLO_GRS_ID, 
    ADM_SERV_ID,
    COSTO_ANUAL,
    DEMANDA,
    OFERTA,
    HOMBRE_25,
    MUJER_25,
    HOMBRE_25_65,
    MUJER_25_65,
    HOMBRE_65,
    MUJER_65
    FROM DEVSIGERSOL.SGR_BARRIDO
    WHERE CICLO_GRS_ID = IN_CICLO_GRS_ID;
END;