create or replace procedure SIGERSOLBL.SGR_SP_UPD_USUARIO(
    IN_USUARIO_ID NUMBER,
    IN_USUARIO_LOGIN VARCHAR2,
    IN_CLAVE_LOGIN VARCHAR2,
    IN_LLAVE_ENCRYPT VARCHAR2,
    IN_EMAIL VARCHAR2,
    IN_COD_USUARIO_MODIFICACION NUMBER,
    OUT_CODIGO OUT NUMBER
) 
AS
BEGIN
   UPDATE DEVSIGERSOL.SGR_USUARIO
   SET 
      USUARIO_LOGIN = IN_USUARIO_LOGIN,
      CLAVE_LOGIN = IN_CLAVE_LOGIN,
      LLAVE_ENCRYPT = IN_LLAVE_ENCRYPT,
      EMAIL = IN_EMAIL,
      COD_USUARIO_MODIFICACION = IN_COD_USUARIO_MODIFICACION,
      FECHA_MODIFICACION = SYSDATE
   WHERE USUARIO_ID = IN_USUARIO_ID
   RETURNING USUARIO_ID INTO OUT_CODIGO;
    EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo actualizarse el registro en la tabla SGR_USUARIO');	   
        OUT_CODIGO :=-1;
        RETURN;
END;