create or replace procedure  SIGERSOLBL.SGR_SP_INS_TRANSFERENCIA(
    IN_CICLO_GRS_ID NUMBER, 
    IN_ADM_SERV_ID NUMBER,
    IN_COSTO_ANUAL NUMBER,
    IN_TONELADAS NUMBER,
    IN_NUMERO_CAMIONES NUMBER,
    IN_COMBUSTIBLE NUMBER,
    IN_RECORRIDO NUMBER,
    IN_COD_USUARIO NUMBER,
    OUT_CODIGO OUT NUMBER) 
AS
BEGIN
    INSERT INTO DEVSIGERSOL.SGR_TRANSFERENCIA
        ( 
          CICLO_GRS_ID, 
          ADM_SERV_ID,
          COSTO_ANUAL,
          TONELADAS,
          NUMERO_CAMIONES,
          COMBUSTIBLE,
          RECORRIDO,
          CODIGO_ESTADO,
          COD_USUARIO_CREACION,
          FECHA_CREACION     
        )
        VALUES
        (
          IN_CICLO_GRS_ID,
          IN_ADM_SERV_ID,
          IN_COSTO_ANUAL,
          IN_TONELADAS,
          IN_NUMERO_CAMIONES,
          IN_COMBUSTIBLE,
          IN_RECORRIDO,
            1,
          IN_COD_USUARIO,
          SYSDATE     
        )
        RETURNING TRANSFERENCIA_ID INTO OUT_CODIGO;
            EXCEPTION   
            WHEN OTHERS THEN
            dbms_output.put_line ('No pudo insertarse el registro en la tabla SGR_TRANSFERENCIA');	   
            OUT_CODIGO :=-1;
            RETURN;
END;