create or replace procedure  SIGERSOLBL.SGR_SP_INS_RES_APROVECHADOS(
    IN_VALORIZACION_ID NUMBER, 
    IN_TIPO_RESIDUO_ID NUMBER,
    IN_ENERO NUMBER,
    IN_FEBRERO NUMBER,
    IN_MARZO NUMBER,
    IN_ABRIL NUMBER,
    IN_MAYO NUMBER,
    IN_JUNIO NUMBER,
    IN_JULIO NUMBER,
    IN_AGOSTO NUMBER,
    IN_SETIEMBRE NUMBER,
    IN_OCTUBRE NUMBER,
    IN_NOVIEMBRE NUMBER,
    IN_DICIEMBRE NUMBER,
    IN_TOTAL NUMBER,
    IN_COD_USUARIO NUMBER,
    OUT_CODIGO OUT NUMBER) 
AS 
BEGIN
      INSERT INTO DEVSIGERSOL.SGR_RESIDUOS_APROVECHADOS
        ( 
          VALORIZACION_ID,
          TIPO_RESIDUO_ID,
          ENERO,
          FEBRERO,
          MARZO,
          ABRIL,
          MAYO,
          JUNIO,
          JULIO,
          AGOSTO,
          SETIEMBRE,
          OCTUBRE,
          NOVIEMBRE,
          DICIEMBRE,
          TOTAL,
          CODIGO_ESTADO,
          COD_USUARIO_CREACION,
          FECHA_CREACION     
        )
        VALUES
        (
          IN_VALORIZACION_ID,
          IN_TIPO_RESIDUO_ID,
          IN_ENERO,
          IN_FEBRERO,
          IN_MARZO,
          IN_ABRIL,
          IN_MAYO,
          IN_JUNIO,
          IN_JULIO,
          IN_AGOSTO,
          IN_SETIEMBRE,
          IN_OCTUBRE,
          IN_NOVIEMBRE,
          IN_DICIEMBRE,
          IN_TOTAL,
            1,
          IN_COD_USUARIO,
          SYSDATE     
        )
     RETURNING RESIDUOS_APROVECHADOS_ID INTO OUT_CODIGO;
      EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo insertarse el registro en la tabla SGR_RESIDUOS_APROVECHADOS');	   
         OUT_CODIGO :=-1;
        RETURN; 
END;