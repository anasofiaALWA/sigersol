create or replace procedure SIGERSOLBL.SGR_SP_UPD_TRANSFERENCIA(
    IN_TRANSFERENCIA_ID  NUMBER,
    IN_CICLO_GRS_ID NUMBER, 
    IN_ADM_SERV_ID NUMBER,
    IN_COSTO_ANUAL NUMBER,
    IN_TONELADAS NUMBER,
    IN_NUMERO_CAMIONES NUMBER,
    IN_COMBUSTIBLE NUMBER,
    IN_RECORRIDO NUMBER,
    IN_COD_USUARIO_MODIFICACION NUMBER,
    OUT_CODIGO OUT NUMBER
) 
AS
BEGIN
   UPDATE DEVSIGERSOL.SGR_TRANSFERENCIA
   SET 
      CICLO_GRS_ID = IN_CICLO_GRS_ID, 
      ADM_SERV_ID = IN_ADM_SERV_ID,
      COSTO_ANUAL = IN_COSTO_ANUAL,
      TONELADAS = IN_TONELADAS,
      NUMERO_CAMIONES = IN_NUMERO_CAMIONES,
      COMBUSTIBLE = IN_COMBUSTIBLE,
      RECORRIDO = IN_RECORRIDO,
      COD_USUARIO_MODIFICACION = IN_COD_USUARIO_MODIFICACION,
      FECHA_MODIFICACION = SYSDATE
   WHERE TRANSFERENCIA_ID = IN_TRANSFERENCIA_ID
   RETURNING TRANSFERENCIA_ID INTO OUT_CODIGO;
    EXCEPTION   
        WHEN OTHERS THEN
        dbms_output.put_line ('No pudo actualizarse el registro en la tabla SGR_TRANSFERENCIA');	   
        OUT_CODIGO :=-1;
        RETURN;
END;