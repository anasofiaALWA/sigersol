create or replace procedure SIGERSOLBL.SGR_SP_LIST_SGR_TRANSFERENCIA(
    IN_CICLO_GRS_ID NUMBER,
    PS_CURSOR OUT sys_refcursor
   )
AS
BEGIN
    OPEN PS_CURSOR FOR SELECT TRANSFERENCIA_ID,
    CICLO_GRS_ID,
    ADM_SERV_ID,
    COSTO_ANUAL,
    TONELADAS,
    NUMERO_CAMIONES,
    COMBUSTIBLE,
    RECORRIDO
    FROM DEVSIGERSOL.SGR_TRANSFERENCIA
    WHERE CICLO_GRS_ID = IN_CICLO_GRS_ID;
END;